var MMM_static;
//When using the packaging tool for production, '<%{ENV_NAME}%>' will be replaced by 'prod'
if ('<%{ENV_NAME}%>' === 'prod') {
    //PROD config
    MMM_static = {
        "config": {
            "name": "bpmv-prod",
            "description": "prod environment for unified app",
            "timestamp": "2011-08-05T05:30:19UTC",
            "services": {
                "lg-movies": "http://bptv.bigpond.com/iptvapi",//"http://cache.samsung.bigpond.com/iptvapi",
                "lg-movies-secured": "https://downloads.bigpondmovies.com/iptvapi",
                "lg-movies-resizer": "http://bpimagedownload.ngcdn.telstra.com/",
                "lg-movies-drm": "https://wvlicense.vos.bigpond.com/widevine-license-proxy/drmkey/",
                "lg-movies-drm-trailer": "https://wvlicense.vos.bigpond.com/widevine-license-proxy/trailerkey/",
                "metering-api": "https://services.bigpond.com/rest/v2",
                "datetime-offsets": "https://epg.cache.iptv.bigpond.com/bptvlive/datetimeoffsets.php",
                "right-now": "http://bptv.bigpond.com/cgi-bin/bigpond.cfg/php/xml_api/parse.php?sec_string=helpcenter",
                "device-logger": "https://iptvlogger.bigpond.com/iptvapi/clientlogger",
                "image-resizer": "http://epg.cache.iptv.bigpond.com/"
            },
            "omniture_server": "info.telstra.com",
            "omniture_un": "telstrabplgprd,telstrabpiptvprd",
            "omniture_suite": "telstrabplgprd",
            "assets": {}
        },
        useProxy: false
    };
} else if ('<%{ENV_NAME}%>' === 'stg') {
    // STAGING config
    MMM_static = {
        "config": {
            "name": "bpmv-dev",
            "description": "dev environment for lg movies",
            "timestamp": "2011-08-05T05:30:19UTC",
            "services": {
                "lg-movies": "http://bptvtest.bigpond.com/iptvapi",//"http://downloads.testmovies01.bigpond.com/iptvapi",
                "lg-movies-secured": "https://downloads.testmovies01.bigpond.com/iptvapi",
                "lg-movies-resizer": "http://epg-dev.tlab.bigpond.com/",
                "lg-movies-drm": "https://wvlicense.vosm.bigpond.com/widevine-license-proxy/drmkey/",
                "lg-movies-drm-trailer": "https://wvlicense.vosm.bigpond.com/widevine-license-proxy/trailerkey/",
                "metering-api": "https://servicesit.bigpond.com/rest/v2",
                "datetime-offsets": "https://epg.cache.iptv.bigpond.com/bptvlive/datetimeoffsets.php",
                "right-now": "http://bptvtest.bigpond.com/cgi-bin/bigpond.cfg/php/xml_api/parse.php?sec_string=helpcenter",
                "device-logger": "https://iptvloggertest.bigpond.com/iptvapi/clientlogger",
                "image-resizer": "http://epg-dev.tlab.bigpond.com/"
            },
            "omniture_server": "scdebugger.com",
            "omniture_un": "telstrabplgdev,telstrabpiptvdev",
            "omniture_suite": "telstrabplgdev",
            "assets": {}
        },
        useProxy: false
    };
} else {
	//dev config
	MMM_static = {
        "config": {
            "name": "bpmv-dev",
            "description": "dev environment for lg movies",
            "timestamp": "2011-08-05T05:30:19UTC",
            "services": {
                "lg-movies": "http://downloads.testmovies01.bigpond.com/iptvapi",
                "lg-movies-secured": "https://downloads.testmovies01.bigpond.com/iptvapi",
                "lg-movies-resizer": "http://epg-dev.tlab.bigpond.com/",
                "lg-movies-drm": "https://wvlicense.vosm.bigpond.com/widevine-license-proxy/drmkey/",
                "lg-movies-drm-trailer": "https://wvlicense.vosm.bigpond.com/widevine-license-proxy/trailerkey/",
                "metering-api": "https://servicesit.bigpond.com/rest/v2",
                "datetime-offsets": "https://epg.cache.iptv.bigpond.com/bptvlive/datetimeoffsets.php",
                "right-now": "http://bptvtest.bigpond.com/cgi-bin/bigpond.cfg/php/xml_api/parse.php?sec_string=helpcenter",
                "device-logger": "https://iptvloggertest.bigpond.com/iptvapi/clientlogger",
                "image-resizer": "http://epg-dev.tlab.bigpond.com/"
            },
            "omniture_server": "scdebugger.com",
            "omniture_un": "telstrabplgdev,telstrabpiptvdev",
            "omniture_suite": "telstrabplgdev",
            "assets": {}
        },
        useProxy: true
    };
}
