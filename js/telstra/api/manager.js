/**
 * @fileOverview API Manager. Provides methods returning datasources that refects data
 *  fetched on the servers of a 3rd-party (= Telstra), or triggering callbacks
 * @example var ds = telstra.api.manager.epg.getBouquet(); ds.load()
 * @author <a href="mailto:gregory.desfour@accedobroadband.com">Gregory Desfour</a>
 */
      
accedo.define('telstra.api.manager',
    ['lib.xml2json',
    'lib.xmlobjectifier',
    'lib.md5',
    'accedo.data.ds',
    'accedo.device.manager',
    'accedo.utils.ajax',
    'accedo.utils.object',
    'accedo.utils.string',
    'accedo.utils.array',
    'accedo.events.dispatcher',
    'telstra.sua.module.bpvod.utils',
    'telstra.sua.core.error',
    'telstra.sua.core.components.loadingOverlay',
    'telstra.sua.core.controllers.popupHelpController'],
    function() {

        //PRIVATE (closure)
    
        var X_TM_DEVICE, MOVIES_SERVICE_URL, MOVIES_SECURED_URL, MOVIES_RESIZER_URL, METERING_URL, RIGHTNOW_URL, DEVICELOGGER_URL,
        TIME_DST_URL, MAINTENANCE_URLS, RESIZER_URL,
        VERSION = '1.0',
        X_TM_IDENTITY_TYPE = "Serial",
        DEFAULT_SIZE = 6,
        //SECRET_STRING = "SamsungVOD",
        SECRET_STRING = "LGE",	
        RESIZER_REMOTE_PATH = 'ImageSizer/RemoteImage/',
        RESIZER_LOCAL_PATH = 'ImageSizer/LocalImage/',
        //Request headers needed for EPG and Time/DST calls (same so far, could change)
        TIME_DST_HEADERS = {
            Authorization: 'Basic Y21zeG1sYXV0aEBtc2guYmlncG9uZC5jb206VGVsc3RyYTEyMw=='
        },
        PROXY_URL = './php/ajaxRequest.php?link=',
        USE_PROXY = MMM_static.useProxy,
        
        //Simulated response when the maintenance service is down (provided by Telstra). Do not modify, clone it if needed.
        maintenance_default_response = {
            "status_modes":[["Online","The app is online"]],
            "current_status" : 0,
            "force_exit" : "false",
            "config" : {
                "name" : "bpmv-dev",
                "description" : "dev environment for lg movies",
                "timestamp" : "2011-08-05T05:30:19UTC",
                "services" : {
                    "lg-movies" : "http://cache.samsung.bigpond.com/iptvapi",
                    "lg-movies-secured" : "https://downloads.bigpondmovies.com/iptvapi",
                    "metering-api" : "https://servicesit.bigpond.com/usr/DeviceManagement/v2",
                    "datetime-offsets" : "http://epg.cache.iptv.bigpond.com/bptvlive/datetimeoffsets.php",
                    "right-now" : "http://bptvtest.bigpond.com/cgi-bin/bigpond.cfg/php/xml_api/parse.php?sec_string=helpcenter",
                    "device-logger" : "https://iptvloggertest.bigpond.com/iptvapi/clientlogger",
                    "image-resizer" : "http://bpmoviedownload.ngcdn02.telstra.net/"
                },
                "assets" : {}
            }
        },
        maintenance_default_response_mini = {
            "status_modes":[["Online","The app is online"]],
            "current_status" : 0,
            "force_exit" : "false"
        },
        
        cache = {},
        //This is a sample content of the cache!
        /* {
            'movies': {
                'getCategory': [
                    {
                        _EXP_TIMESTAMP: 1307932466, //UNIX timestamp in ms
                        args: ['/category/Movies/Comedy', null, null, 1],
                        data: [
                            {
                                cache: 'test'
                            },
                            {
                                cache: 'test2'
                            }
                        ],
                        totalItems: 15,
                        lastPageLoaded: 1
                    }
                ]
            },
            weather: {},
            epg: {}
        }, */
        
        //Default cache lifetime in minutes
        CACHE_DEFAULT_LIFETIME = 10,
        
        //Server time, to be synchronized regularly with the server, and updated by timeouts in between.
        //Expressed in milliseconds since Jan 1 1970
        serverTime,
        
        //Id of the currently used interval launching the time increment
        intervalIncrement,
        
        //Id of the interval launching the time sync with the server
        intervalSynchronize,
        
        //Currently ongoing requests
        activeRequests = [],
        
        //Array of APIs whose requests we never want to be aborted
        unabortableRequests = ['time', 'telstra_metering', 'weather'],
        
        //stores the device identification
        xTmDevice = null,
        
        //Region/Postcode Entered by the users
        userPostcode, userRegion, userTimezoneOffset,
        
        /**
         * Called every minute to increment the latest synchronized time by... a minute.
         * @private
         */
        incrementTime = function() {
            serverTime += 60000;
            obj.dispatchEvent('telstra:time:update', serverTime);
        },
        
        /**
         * Sets the server time. To be done after an API call to a server that gives us the current server time!
         * @param {Number} timeNow Current time in milliseconds
         * @private
         */
        setTimeNow = function(timeNow) {
            serverTime = timeNow;
            obj.dispatchEvent('telstra:time:update', serverTime);
            if (intervalIncrement) {
                clearInterval(intervalIncrement);
            }
            //Increment the time every minute
            intervalIncrement = setInterval(incrementTime, 60000);
        },
        
        /**
         * Updates the various Telstra API URLs
         * @param {Object} services
         * @private
         */
        updateServiceURLs = function(services) {
            TIME_DST_URL = services['datetime-offsets'];
            MOVIES_SERVICE_URL = services['lg-movies'];
            MOVIES_SECURED_URL = services['lg-movies-secured'];
            MOVIES_RESIZER_URL = services['lg-movies-resizer'];
            METERING_URL = services['metering-api'];
            RIGHTNOW_URL = services['right-now'];
            DEVICELOGGER_URL = services['device-logger'];
            RESIZER_URL = services['image-resizer'];
        },
        /**
         * Adds data to the cache.
         * @parameter {String} api The API id
         * @parameter {String} fn Name of the function called
         * @parameter {Array} args Array of arguments applied to the above function
         * @parameter {Object} data Response object obtained from the server
         * @parameter {Integer} totalItems Total number of items available on the server
         * @parameter {Integer} lastPageLoaded Number of the response page that included this data
         * @parameter {Number} cacheLifeTime Lifetime of this cached data in minutes
         *   addToCache("movies", "getCategory", ['/category/Movies/Featured', 7], {dataitem: 'myResponse'}, 15, 1, 10);
         * @private
         */
        addToCache = function(api, fn, args, data, totalItems, lastPageLoaded, cacheLifeTime) {    
            //totally disabled
            // return;
            if (serverTime) {
                //Create the structure if needed: cache > api > fn > [objects]
                if (!cache[api]) {
                    cache[api] = {};
                }
                if (!cache[api][fn]) {
                    cache[api][fn] = [];
                }
                                
                cache[api][fn].push({
                    _EXP_TIMESTAMP: serverTime + (cacheLifeTime || CACHE_DEFAULT_LIFETIME) * 60000,
                    args:           args,
                    data:           data,
                    totalItems:     totalItems,
                    lastPageLoaded: lastPageLoaded
                });
            }
        },
        
        /**
         * Gets data from cache if data is present.
         * @example getFromCache("movies", "getCategory", [5]);
         * @private
         */
        getFromCache = function(api, fn, args) {
            /**
            Check if cache[api][fn] is set.
            If so, go through the cache and find the element with same 'args' as well as _EXP_TIMESTAMP <= CURRENT_TIMESTAMP
                    If no such Item found - return null.
                    */
            var i, isSimilar, cacheItem = null;
            
            /* We need to know the current time in the following function, and anyway we won't use the
            cache if we cannt test its expiry date against the server time. */
            if (serverTime && cache[api] && cache[api][fn]) {
            
                accedo.utils.array.each(cache[api][fn], function(iCache, position) {
                    if (iCache.args.length == args.length) {
                        //test that the arguments are the same
                        i = args.length;
                        isSimilar = true;
                        
                        while (i--) {
                            if (args[i] !== iCache.args[i]) {
                                isSimilar = false;
                                break;
                            }
                        }
                        if (isSimilar) {
                            //Check whether the cached data is recent enough
                            if (iCache._EXP_TIMESTAMP > serverTime) {
                                cacheItem = iCache;
                            } else {
                                //cached data has expired, delete the entry
                                accedo.console.info('Cache expired for: '+api+' > '+fn+' > '+args);
                                accedo.utils.array.remove(cache[api][fn], position);
                            }
                            throw accedo.$break;
                        }
                    }
                });
            }

            return cacheItem;
        },
        
        /**
         * Purges the cache of a given API/fn couple
         * @private
         */
        purgeCache = function(api, fn) {
            if (cache[api]) {
                delete cache[api][fn];
            }
        },
        
        launchCacheCallbacks = function(eventHandler, data) {
            //Pass the cached data object to the provided callback
            if (eventHandler) {
                if (accedo.utils.object.isFunction(eventHandler.onSuccess)) {
                    eventHandler.onSuccess(data);
                }
                if (accedo.utils.object.isFunction(eventHandler.onComplete)) {
                    eventHandler.onComplete();
                }
            }
        },

        /**
         * Returns an Array built from the arguments Object (which is not a real, fully-featured Array)
         * @param {arguments Object mimicking an Array}
         * @returns {Array}
         */
        getArgsArray = function(args) {
            return Array.prototype.slice.call(args);
        },

        /**
         * Cleans a responseText by replacing appropriately some ':' by '_'
         * @param {String} text a responseText obtained by AJAX
         * @returns {String} the cleaned responseText
         */
        cleanResponseText = function(text){
            return text.replace(/v11:/g,'').
            replace(/v1:/g,'').
            replace(/soapenv:/g,'').
            replace(/xmlns:/g,'').
            replace(/xsi:nil=/g, "xsi_nil=").
            replace(/xmlns:xsi=/g, "xmlns_xsi=").
            replace(/xsi:noNamespaceSchemaLocation=/g, "xsi_noNamespaceSchemaLocation=");
        },
    
        /**
         * Fills a datasource with given data
         */
        fillDataSource = function(ds, data, totalItems, lastPageLoaded, pageSize) {
            ds.setTotalItems(totalItems);
            ds.setLastPageLoaded(lastPageLoaded);
            if (pageSize){
                ds.appendData(data, (lastPageLoaded -1)*pageSize);
            }else{
                ds.appendData(data);
            }
        },
    
        /**
         * Gateway to all AJAX requests
         * @param {String} api The API id
         * @param {String} fn Name of the function called
         * @param {String} url
         * @param {Object} params
         * @param {post|get|put} params.method (default:post)
         * @param {accedo.utils.hash} params.parameters Additional parameters (POST params or GET querystring params)
         * @param {String} params.postBody
         * @param {Number} params.authtoken
         * @param {Object} params.requestHeaders RequestHeaders to be added to the default ones
         * @param {Object} eventHandler (with onSuccess, onFailure, etc... properties)
         * @param {Number} retries How many times to retry on failure before propagating the onFailure callback (default 0)
         * @private
         */
        callService = function(api, fn, url, params, eventHandler, addEIUSN, retries) {
        	
        	/*accedo.console.log("callService "+url);
        	if (params && params.postBody)
        		accedo.console.log("postBody "+params.postBody);*/
    
            if (typeof eventHandler != "object") {
                eventHandler = {};
            }
    
            if (typeof retries != 'number') {
                retries = 0;
            }

            /**
             * Removes the current request from the activeRequests array.
             * This will be called later in onSuccess, onFailure when retries has reached 0, and onException
             * @private
             */
            
			//Juergen: 20130415 moved over WD version to get around header issue on LG 2011
            if (USE_PROXY){
            	url = PROXY_URL + escape(url);
            }

            var popRequest = function() {

                if(!telstra.sua.core.main.isOnline){
                    return;
                }

                activeRequests = accedo.utils.array.filter(activeRequests, function(item) {
                    return item.request !== request;
                });
            },

            //Keep a ref to the request so we can resend it
            request = accedo.utils.ajax.request(
                url,
                {
                    method:         params.method || "post",
                    async:         	params.async,
                    parameters:     params.parameters,
                    postBody:       params.postBody,
                    contentType:    'application/xml',
                    useProxy:		USE_PROXY,
                    requestHeaders : accedo.utils.object.extend(
                        (function(){
                            if(api == 'movies'){
                                var obj = {
                                    "x-tm-device":          X_TM_DEVICE,
                                    "x-tm-api-version":     VERSION,
                                    "x-tm-identity":        accedo.device.manager.identification.getUniqueID(),
                                    "x-tm-identity-type":   X_TM_IDENTITY_TYPE,
                                    "x-tm-authtoken":       params.authtoken || telstra.api.manager.movies.authToken || null
                                };
                            }
                            else if (api == 'time' && accedo.device.manager.identification.getDeviceId() == "lg") {
                                var obj = {
                                    "x-tm-origin": document.location.protocol + "//" + document.location.hostname
                                };	
                            }
                            else {
                                return {};
                            }
							
							if (addEIUSN){
                                var sdi_id = accedo.device.manager.identification.getWidevineSDI_ID();
                            	//accedo.console.log("sdi_id: " + sdi_id);
                            	if (sdi_id){
                            		var index2010 = sdi_id.indexOf("2010");
                            		if (index2010 == -1){
                            			obj["x-tm-eiusn"] = sdi_id;
                            		}
                            	}
                            }
                            
                            /*for(var i in obj)
                            	accedo.console.log(i+":"+obj[i]);*/

                            if(params.method === "put"){
                                obj["Content-type"] = "application/xml";
                                obj["Content-Length"] = params.postBody.length;
                            }
                            return obj;
                        })(),
                        params.requestHeaders || {}
                        ),
                    onSuccess: function(response) {
                        //remove from the array of ongoing requests
                        popRequest();
                            
                        var responseText,
                        json = response.responseJSON;
                        
                        //accedo.console.log('onSuccess');
                        
                            
                        //If the response is not in JSON format, assume it's XML and parse it to JSON
                        //@todo: see if responseXML could be more efficiently parsed, using for instance:
                        //http://www.terracoder.com/index.php/xml-objectifier/xml-objectifier-documentation
                        if (!json) {
                            responseText = cleanResponseText(response.responseText);
                                
                            //Temporary conditional to move API from xml2json to xmlobjectifier, as xmlobjectifier is much faster than xml2json
                            if (api == 'movies'){
                                json = lib.xmlobjectifier.xmlToJSON(response.responseXML, {
                                    outputType: 1
                                });

                                //As the tags converted by this new library is not the same as the previous library,
                                //We need to make a change to the feed
                                json = telstra.sua.module.bpvod.utils.migrateXmlJson(json);
                            }else{
                                // accedo.console.info('MIGRATING TO XMLOBJECTIFIER SOON! CHECK IT WILL STILL WORK! '+api+'/'+fn);
                                json = lib.xml2json.parser(responseText);
                            }
                        }
                        if (accedo.utils.object.isFunction(eventHandler.onSuccess)) {
                            eventHandler.onSuccess(json, response);
                        }
                    },

                    onFailure: function(response) {
                        if (retries--) {
                            accedo.console.log('Retrying... ' + retries + ' more retries left before giving up');
                            request.resend();
                            return;
                        }
                            
                        //remove from the array of ongoing requests
                        popRequest();
                            
                        accedo.console.info('Ajax call failure on ' + api + '/' + fn);
                            
                        var responseText = cleanResponseText(response.responseText);
                        accedo.console.info('responseText: ' + responseText);
                            
                        if (response.status == 400) {
                            var json = lib.xml2json.parser(responseText);
                        }
                        else if (response.status != 200) {
                            var json = {
                                error: {
                                    errorcode:response.status
                                }
                            }
                        }

                        if (accedo.utils.object.isFunction(eventHandler.onFailure)) {
                            eventHandler.onFailure(json);
                        }
                    },

                    onException: function(request, exception) {
                        accedo.console.log("onException: " + accedo.utils.object.toJSON(exception));
                            
                        //remove from the array of ongoing requests
                        popRequest();
                            
                        if (accedo.utils.object.isFunction(eventHandler.onFailure)) {
                            eventHandler.onFailure();
                        }
                    },

                    onComplete: function() {
                        if (accedo.utils.object.isFunction(eventHandler.onComplete)) {
                            eventHandler.onComplete();
                        }
                    },

                    onAbort: function() {
                        //No need to remove from the array of ongoing requests, it is done manually when aborting
                            
                        if (accedo.utils.object.isFunction(eventHandler.onAbort)) {
                            eventHandler.onAbort();
                        }
                    }
                });
            
            //Push this request to the active requests array
            activeRequests.push({
                api: api,
                fn: fn,
                request: request
            });
        },
        
        /**
         * Builds a default event handler object for use with accedo.utils.ajax with a user-defined eventHandler.
         * Provides default event handler behaviour. Update its properties to customise it as you need
         * (See in movies.getMediaItem for an example).
         * @param {Object} eventHandler (optional) An object containing onSuccess, onFailure, etc. functions
         * @returns an event handler with onSuccess, onFailure etc. functions with default behaviour and
         *          calling the argument's corresponding function, if present
         */
        getDefaultEventHandler = function(eventHandler) {
            if (typeof(eventHandler) != "object" || eventHandler === null) {
                eventHandler = {};
            }
            return {
                onSuccess: function(json) {
                    //Pass the response object to the provided callback
                    if (accedo.utils.object.isFunction(eventHandler.onSuccess)) {
                        eventHandler.onSuccess(json);
                    }
                },
                onFailure:function(json) {
                    //Pass the Error object to the provided callback (pass the response if no Error object is inside)
                    if (accedo.utils.object.isFunction(eventHandler.onFailure)) {
                        eventHandler.onFailure(  (!accedo.utils.object.isUndefined(json) ? (json.error || json) : null) );
                    }else{
                        telstra.sua.core.error.show(telstra.sua.core.error.CODE.GLOBAL_API_FAILED);
                    }
                },
                onException:function(json) {
                    if (accedo.utils.object.isFunction(eventHandler.onException)) {
                        eventHandler.onException(json);
                    }
                },
                onComplete:function(json) {
                    if (accedo.utils.object.isFunction(eventHandler.onComplete)) {
                        eventHandler.onComplete(json);
                    }
                },
                onAbort:function(json) {
                    if (accedo.utils.object.isFunction(eventHandler.onAbort)) {
                        eventHandler.onAbort(json);
                    }
                }
            }
        },
        
        
        
        /**
         * assigns the correct x-tm-device value according to identity and model name
         * @private
         * @returns {String}  the value for x-tm-device
         */
        checkHeader = function(){
        	accedo.console.log("checkHeader:5:00pm");
        	
        	//temp
        	//xTmDevice = 'LGNetcastTV2013SDI';
        	//temp

        	if(xTmDevice !== null){
        		return xTmDevice;
        	}
        	
        	var ua = navigator.userAgent;
        	accedo.console.log("ua:"+ua);
        	var modelName = accedo.device.manager.identification.getModelName();
        	accedo.console.log("modelName:"+modelName);
        	
        	if (ua.indexOf("LG NetCast.TV-2013") !== -1) {
        		xTmDevice = 'LGNetcastTV2013SDI';
        		SECRET_STRING = 'SamsungVOD';
        	} else if (ua.indexOf("LG NetCast.TV-2012") !== -1) {
        		xTmDevice = 'LGNetcast2012TVSDI';
        	} else if (ua.indexOf("LG NetCast.TV-2011") !== -1) {
        		xTmDevice = 'LGNetcastTVSDI';
        	} else if (ua.indexOf("LG NetCast.TV-2010") !== -1) {
        		xTmDevice = 'LGNetcastTV';
        	} else if (ua.indexOf("LG NetCast.Media-2012") !== -1) {
        		switch (modelName) {
    				case "Media/BP730":
        				xTmDevice = 'LGNetcastBD2013SDI';
                		SECRET_STRING = 'SamsungVOD';
    					break;
        			case "Media/BH9430":
        				xTmDevice = 'LGNetcastHT2013SDI';
                		SECRET_STRING = 'SamsungVOD';
        				break;
        			default:
        				xTmDevice = 'LGNetcastTV';
        		}
        		SECRET_STRING = 'SamsungVOD';
        	} else if (ua.indexOf("LG NetCast.Media-2011") !== -1) {
        		switch (modelName) {
        			case "Media/BP530":
        			case "Media/HR935":		//model 936
        				xTmDevice = 'LGNetcastBD2013SDI';
                		SECRET_STRING = 'SamsungVOD';
        				break;
        			case "Media/BH6830":	//model 6530
        				xTmDevice = 'LGNetcastHT2013SDI';
                		SECRET_STRING = 'SamsungVOD';
        				break;
        			case "Media/BH7520":
        				xTmDevice = 'LGNetcast2012HTSDI';
        				break;
        			case "Media/HR920":		//model 925
        			case "Media/BP520":		//model 620
        			case "Media/BP220":		//model 320
        				xTmDevice = 'LGNetcast2012BDSDI';
        				break;
        			case "Media/BD670":
        				xTmDevice = 'LGNetcastBDSDI';
        				break;
        			case "Media/HB906":
        				xTmDevice = 'LGNetcastHTSDI';
        				break;
        			case "Media/ST600":
        				xTmDevice = 'LGNetcastSuSDI';
        				break;
        			default:
        				xTmDevice = 'LGNetcastTV';
        		}
        	} else if (ua.indexOf("LG NetCast.Media-2010") !== -1) {
        		switch (modelName) {
	    			case "Media/BD570":
	    				xTmDevice = 'LGNetcastBD';
	    				break;
	    			case "Media/HB905":
	    			case "Media/HB965":
	    				xTmDevice = 'LGNetcastHT';
	    				break;
	    			default:
	    				xTmDevice = 'LGNetcastTV';
	    		}
	    	} else {
	        		xTmDevice = 'LGNetcastTV';
        	}
        	
        	accedo.console.log("xTmDevice "+xTmDevice);
        	accedo.console.log("SECRET_STRING "+SECRET_STRING);
        	
        	return xTmDevice;
        },
        

        /**
         * generate and return a secret key to be used for demo license
         * @returns {string} secret key
         */
        generateSecretKey = function(){
            var deviceType =  X_TM_DEVICE;
            var secretString = SECRET_STRING;
            var identifier = accedo.device.manager.identification.getUniqueID();

            accedo.console.log("deviceType: " + deviceType + " secretString: " + secretString + " identifier: " + identifier);
	
			var preHash = deviceType + secretString + identifier;
			accedo.console.log("preHash: " + preHash);
			var hash = lib.md5.b64_md5( deviceType + secretString + identifier );
			accedo.console.log("hash: " + hash);
            var demoauth = "demoauth" + hash + "==";
			//accedo.console.log("demoauth: " + demoauth);
            //var demoauth = "demoauthd3kgufeK2l3h1EwpkYmhRw==";
            accedo.console.log("generateSecretKey: " + demoauth);
            return demoauth;
        },
        
        /**
         * Returns a string representing the date, in the yyyymmdd format, using UTC (GMT timezone)
         * @param {Date} d 
         * @returns {String} yyyymmdd string version of the date
         * @private
         */
        formatDate = function(d) {
            var curr_date = d.getUTCDate();
            var curr_month = d.getUTCMonth() + 1; //months are zero based
            var curr_year = d.getUTCFullYear();
            return '' + curr_year + (curr_month > 9 ? curr_month : '0' + curr_month) + (curr_date > 9 ? curr_date : '0' + curr_date);
        };

        switch (accedo.device.manager.identification.getDeviceId()) {
            case 'workstation':
            case 'lg':            
            default:
                X_TM_DEVICE = checkHeader();
            	accedo.console.log('X_TM_DEVICE '+X_TM_DEVICE);
                MAINTENANCE_URLS = {
                    'core'  :'http://mmm.iptv-apps-cache.tlab.bigpond.com/ping/samsung-unified-app.json?token=82462e34188e0e598900e5962b126b01&env=sua-dev',
                    'ga'    :'http://mmm.iptv-apps-cache.tlab.bigpond.com/ping/feed-engine.json?token=4cba5a1e4b14ae17676bd90306d45474',
                    'bpvod' :'http://mmm.iptv-apps-cache.tlab.bigpond.com/ping/samsung-bigpond-movies.json?token=0334b38847b03a4ea64bdfee1efbe945',
                    'bptv'  :'http://mmm.iptv-apps-cache.tlab.bigpond.com/ping/samsung-live-tv.json?token=80530d48126261c2dda952ceb16aa8fd'
                };
            
                //Apply the static MMM config if present - only on TVs, as workstations need Apache reverse proxy settings
                //NB: the config comes from the config.js file
                if (MMM_static && MMM_static.config && MMM_static.config.services) {
                    updateServiceURLs(MMM_static.config.services);
                } else {
                    accedo.console.info('~~ CRITICAL ERROR - no static MMM config found ! ~~');
                }
                break;
        }
        
        accedo.console.info('X_TM_DEVICE '+X_TM_DEVICE);

        //Extend dispatcher - we sent events on time update
        var obj = accedo.utils.object.extend(accedo.events.dispatcher(), {

            // expose header function to other classes (3rd party requirement to use same device ID's)
            checkHeader: function() {
                return checkHeader();
            },
            /**
         * Aborts requests that are not related to the given API, and update the activeRequests array
         * @param {String} api
         * @public
         */
            abortOtherRequests: function(api) {
                //These 3 use the same EPG APIs
                if (api == 'fta' || api == 'bptv') {
                    api = 'epg';
                }
                activeRequests = accedo.utils.array.filter(activeRequests, function(item) {
                    if (item.api != api && -1 == accedo.utils.array.indexOf(unabortableRequests, item.api)) {
                        item.request.abort();
                        return false;
                    }
                    return true;
                });
            },
        
            /**
         * Aborts requests that match the given API/fn couple
         * @param {String} api
         * @param {String} fn
         * @public
         */
            abortRequests: function(api, fn) {
                activeRequests = accedo.utils.array.filter(activeRequests, function(item) {
                    if (item.api == api && item.fn == fn) {
                        // accedo.console.info('Aborting '+item.api+'/'+item.fn);
                        item.request.abort();
                        return false;
                    }
                    return true;
                });
            },
        
            /**
             * BP Movies API
             */
            movies: {
        
                /**
                 * Movies-specific fields
                 */
                email : null,
                accountId : null,
                authToken : null,
                accountInfo : {},
                isProduction : ('<%{ENV_NAME}%>' === 'prod'), //state which server is using

                /**
                 * Logs the user in according to the parameters.
                 * @param {String} username
                 * @param {String} password
                 * @param {Object} eventHandler (optional) An object containing onSuccess, onFailure, etc. functions
                 * @param {Boolean} validate - save the token after login? if not, this api is just for validating an account
                 * @example telstra.api.manager.movies.login("bpmtest1@accedobroadband.com", "aaaaaa", {
                 *  onFailure: function(error){accedo.console.log('ERROR, code is: '+error.errorcode)},
                 *  onSuccess: function(resp){accedo.console.log('User logged in, account id: '+resp.accountid)}
                 * })
                 */
                login: function(username, password, eventHandler, validate) {
            
                    if (!username || !password) {
                        accedo.console.info('username and password are compulsory');
                        return;
                    }
                
                    if (typeof(eventHandler) != "object") {
                        eventHandler = {};
                    };
                
                    var api = "movies",
                    fn  = "login",
                    base64 = lib.md5.b64_md5(password) + "==",
                    postBody = "<authRequest><email>" + username + "</email><password>" + base64 + "</password></authRequest>",
                    myEventHandler = getDefaultEventHandler(eventHandler),
                    defaultOnSuccess = myEventHandler.onSuccess;
                    //accedo.console.log("login: postBody: " + postBody);
                    //Customise the default event handler for this response's specificities
                    myEventHandler.onSuccess = function(json) {
                        accedo.console.log('User logged in');

                        telstra.api.manager.movies.email = username;

                        //Get the new accoundId and authToken values
                        if(!validate){
                            if (json.accountid) {
                                telstra.api.manager.movies.accountId = json.accountid;
                            }
                            
                            if (json.authtoken) {
                                telstra.api.manager.movies.authToken = json.authtoken;
                            }
                        }
                    
                        defaultOnSuccess(json);
                    };
                    
                    callService(api, fn,
                        MOVIES_SECURED_URL + '/account/login',
                        {
                            method:     'post',
                            postBody:   postBody
                        },
                        myEventHandler,
                        true
                        );
                },

                /*
                 * login api but for verifying user only
                 * @param username [string] username string
                 * @param password [string] password string
                 * @param _eventHandler [json] contain callback function for possible result
                 */
                verifyUser:function(username, password, eventHandler){
                    var api = "movies",
                    fn  = "login",
                    base64 = lib.md5.b64_md5(password) + "==",
                    postBody = "<authRequest><email>" + username + "</email><password>" + base64 + "</password></authRequest>",
                    myEventHandler = getDefaultEventHandler(eventHandler),
                    defaultOnSuccess = myEventHandler.onFailure;

                    //accedo.console.log("postBody: " + postBody);

                    myEventHandler.onFailure = function(json) {
                        accedo.console.log('Fail');
                        defaultOnSuccess(json);
                    };

                    callService(api, fn,
                        MOVIES_SECURED_URL + '/account/login',
                        {
                            method:     'post',
                            postBody:   postBody
                        },
                        myEventHandler,
                        true
                        );
                },
            
                /**
             * Logs the user out
             * @param {Object} eventHandler (optional) An object containing onSuccess, onFailure, etc. functions
             * @example telstra.api.manager.movies.logout("bpmtest1@accedobroadband.com", "aaaaaa", {
             *  onFailure: function(error){accedo.console.log('ERROR, code is: '+error.errorcode)}, 
             *  onSuccess: function(resp){accedo.console.log('User logged out')}
             * })
             */
                logout: function(eventHandler) {
                    var api = "movies",
                    fn  = "logout",
                    myEventHandler = getDefaultEventHandler(eventHandler),
                    defaultOnSuccess = myEventHandler.onSuccess;
                
                    //Customise the default event handler for this response's specificities
                    myEventHandler.onSuccess = function(json) {
                        accedo.console.info('User logged out');
                        telstra.api.manager.movies.email = null;
                        telstra.api.manager.movies.accountId = null;
                        telstra.api.manager.movies.authToken = null;
                        telstra.api.manager.movies.accountInfo = {};

                        defaultOnSuccess(json);
                    };
                
                    callService(api, fn,
                        MOVIES_SECURED_URL + '/account/logout',
                        {
                            method:     'post'
                        },
                        myEventHandler,
                        false
                        );
                },
            
                /**
             * Fetches the currently logged-in user account. Use callbacks (onSuccess, etc.) to do something with it.
             * Needs to be logged in, otherwise an error object will be passed to the callback's onFailure method (if any)
             * @param {Object} eventHandler (optional) An object containing onSuccess, onFailure, etc. functions
             * @example telstra.api.manager.movies.getAccount({
             *              onFailure: function(error){accedo.console.log('ERROR, code: '+error.errorcode)},
             *              onSuccess: function(account){accedo.console.log('Account found, id: '+account.accountid)}
             *          })
             */
                getAccount: function(eventHandler) {

                    var api = "movies",
                    fn  = "getAccount",
                    myEventHandler = getDefaultEventHandler(eventHandler),
                    defaultOnSuccess = myEventHandler.onSuccess;
                
                    //Customise the default event handler for this response's specificities
                    myEventHandler.onSuccess = function(json) {
                        //store data in accountInfo
                        telstra.api.manager.movies.accountInfo = json;

                        //accedo.console.log(accedo.utils.object.toJSON(telstra.api.manager.movies.accountInfo));

                        var deviceList = telstra.api.manager.movies.accountInfo.devicelist.device;

                        if (!accedo.utils.object.isArray(deviceList)){
                            telstra.api.manager.movies.accountInfo.devicename = deviceList.devicename;
                            telstra.api.manager.movies.accountInfo.deviceid = deviceList.deviceid;
                        } else {
                            for (var i = 0; i < deviceList.length; i ++){
                                if (deviceList[i].deviceid.toUpperCase() == accedo.device.manager.identification.getUniqueID().toUpperCase()) {
                                    accedo.console.log("found device: " + deviceList[i].devicename + " id: " + deviceList[i].deviceid);
                                    telstra.api.manager.movies.accountInfo.devicename = deviceList[i].devicename;
                                    telstra.api.manager.movies.accountInfo.deviceid = deviceList[i].deviceid;
                                    break;
                                }
                            }
                        }

                        //Pass the Account object to the provided callback
                        defaultOnSuccess(json);
                    };
                
                    callService(api, fn,
                        MOVIES_SECURED_URL + '/account',
                        {
                            method:     'get',
                            async:		false
                        },
                        myEventHandler,
                        false
                        );
                },

                /**
             * Gives access to subchannel, promotion, myrental & account objects (of the main BPMovie channel)
             * @param {Boolean} noCache Specify use cache or not. Required after login / logout
             * @param {Function} eventHandler Specify the function to pass in
             */
                getChannel: function(noCache, eventHandler) {
                	//accedo.console.log("getChannel "+noCache+' /'+eventHandler);
            
                    var api = "movies",
                    fn  = "getChannel",
                    myEventHandler = getDefaultEventHandler(eventHandler),
                    defaultOnSuccess = myEventHandler.onSuccess,
                    loadData,
                    ds = accedo.data.ds(),
                    utils = telstra.sua.module.bpvod.utils;
                
                    //Customise the default event handler for this response's specificities
                    myEventHandler.onSuccess = function(resp) {
                    	//accedo.console.log("getChannel onSuccess: "+JSON.stringify(resp));
                        var data = resp.menuitem,
                        dataItem, e, promoimage, rentalDetails,
                        dataLen = data.length,
                        imgBase = 'images/module/bpvod/',
                        i = 0,
                        isSignedIn = telstra.sua.module.bpvod.session.isSignedIn,
                        topLevelMenuItem = [];
						

                        for (; i < dataLen; i++){
                            dataItem = data[i];
                            if ('promotion' in dataItem){
                                if (isSignedIn){
                                    promoimage = dataItem.promotion.promoimage;
                                    topLevelMenuItem.push({
                                        subchannelname: dataItem.promotion.promoname,
                                        url: "/category/" + dataItem.promotion.subchannelname + "/" + dataItem.promotion.categoryname,
                                        subchannelimage: obj.resizer(promoimage,216,126),
                                        subchannelimagebig: obj.resizer(promoimage,300,174),
                                        template: 'promotion',
                                        targetctrller: 'movieDetailsController',
                                        shift: (dataItem.promotion.subchannelname == "Television" ? 0 : parseInt(dataItem.promotion.mediaitemposition) - 1),
                                        selected_ch_id: (dataItem.promotion.subchannelname == "Television" ? dataItem.promotion.mediaitemid : null),
                                        currentCategoryName: dataItem.promotion.categoryname,
                                        contentlabel: ''
                                    });
                                }
                            }else if ('subchannel' in dataItem){
                                switch(dataItem.subchannel.subchannelname) {
                                    case 'Television':
                                        topLevelMenuItem.push({
                                            subchannelname: dataItem.subchannel.subchannelname,
                                            url: dataItem.subchannel.url,
                                            subchannelimage: imgBase + 'topMenu_Icon01_tvShow_small.png',
                                            subchannelimagebig: imgBase + 'topMenu_Icon01_tvShow_big.png',
                                            template: 'television',
                                            mediatype: 'Television',
                                            mediaindex: 1,
                                            targetctrller: 'movieCategoryListingController',
                                            contentlabel: ''
                                        });
                                        break;
                                    case 'Movies':
                                        topLevelMenuItem.push({
                                            subchannelname: dataItem.subchannel.subchannelname,
                                            url: dataItem.subchannel.url,
                                            subchannelimage: imgBase + 'topMenu_Icon02_movie_small.png',
                                            subchannelimagebig: imgBase + 'topMenu_Icon02_movie_big.png',
                                            template: 'movies',
                                            mediatype: 'Movies',
                                            mediaindex: 1,
                                            targetctrller: 'movieCategoryListingController',
                                            contentlabel: ''
                                        });
                                        if (!isSignedIn) {
                                            topLevelMenuItem.unshift({
                                                subchannelname: 'Free Movies',
                                                url: dataItem.subchannel.url,
                                                subchannelimage: imgBase + 'topMenu_Icon07_freeMovie_small.png',
                                                subchannelimagebig: imgBase + 'topMenu_Icon07_freeMovie_big.png',
                                                template: 'freemovies',
                                                mediatype: 'Movies',
                                                selectedCatName: "Free Trial Movies",
                                                targetctrller: 'movieCategoryListingController',
                                                contentlabel: ''
                                            });
                                        }
                                        break;
                                }
                            }else if ('myrental' in dataItem){
                                if (isSignedIn) {
                                    rentalDetails = utils.countRentedItem();
                                    topLevelMenuItem.push({
                                        subchannelname: 'My Rentals',
                                        url: '/subchannel/My-Rentals',
                                        subchannelimage: imgBase + 'topMenu_Icon03_myRental_small.png',
                                        subchannelimagebig: imgBase + 'topMenu_Icon03_myRental_big.png',
                                        template: 'myrental',
                                        targetctrller: 'myRentalController',
                                        contentlabel: '<span  style="line-height:150%;">View your current ' +
                                        'rentals</span><br/>' + rentalDetails.rented + ' Ready to ' +
                                        'watch<br/>' + rentalDetails.aboutToExpired + ' About to expire'
                                    });
                                }
                                topLevelMenuItem.push({
                                    subchannelname: 'Search',
                                    url: '/subchannel/Search',
                                    subchannelimage: imgBase + 'topMenu_Icon04_search_small.png',
                                    subchannelimagebig: imgBase + 'topMenu_Icon04_search_big.png',
                                    template: 'search',
                                    targetctrller: 'searchPageController',
                                    contentlabel: 'Search for Movies and TV shows by title, actor or director'
                                });
                                if (!isSignedIn){
                                    topLevelMenuItem.push({
                                        subchannelname: 'Sign in or Join',
                                        url: '/subchannel/Sign-in-or-Join',
                                        subchannelimage: imgBase + 'topMenu_Icon05_SignIn_or_join_small.png',
                                        subchannelimagebig: imgBase + 'topMenu_Icon05_SignIn_or_join_big.png',
                                        template: 'signin',
                                        targetctrller: 'signInORjoinNowController',
                                        contentlabel: 'Sign in or Join now to rent from a world of movies and TV shows'
                                    });
                                }
                                if (isSignedIn) {
                                    topLevelMenuItem.push({
                                        subchannelname: 'My Account',
                                        url: '/subchannel/My-Account',
                                        subchannelimage: imgBase + 'topMenu_Icon06_myAccount_small.png',
                                        subchannelimagebig: imgBase + 'topMenu_Icon06_myAccount_big.png',
                                        template: 'myaccount',
                                        targetctrller: 'myAccountPageController',
                                        contentlabel: 'View your account information and purchase ' +
                                        'history, or redeem a voucher.<br/>Available credit: ' +
                                        telstra.api.manager.movies.accountInfo.vouchercredit
                                    });
                                }
                            }
                        }
                        ds.appendData(topLevelMenuItem);
                    
                        addToCache(api, fn,
                            [isSignedIn],
                            topLevelMenuItem, null, null);
                    
                        defaultOnSuccess(topLevelMenuItem);
                    };
                
                    loadData = function(){
                        //Get the data from cache if possible
                        var cacheItem = getFromCache(api, fn, [telstra.sua.module.bpvod.session.isSignedIn]);

                        if(noCache || cacheItem == null) {

                            //Do server request
                            callService(api, fn,
								//'http://192.168.0.114/dev/telstra/bpmovies/channel.xml',	//JUERGEN: FOR DEV ONLY
                                MOVIES_SERVICE_URL + '/channel',
                                {
                                    method:     'get'
                                },
                                myEventHandler,
                                false
                                );
                        
                            return;
                        }
                        ds.appendData(cacheItem.data);
                    };
                
                    ds.addEventListener("accedo:datasource:load", loadData);
                    return ds;
                },
            
                /**
             * Gives access to categories relative to a given subChannel
             * @param {String} subChannelUrl, e.g. 'Movies'
             * @returns {accedo.data.ds}
             */
                getSubChannel: function(subChannelUrl) {
                    if (!subChannelUrl) {
                        accedo.console.log('subChannelUrl is compulsory');
                        return null;
                    }
                
                    var ds = accedo.data.ds(),
                    api = "movies",
                    fn = "getSubChannel",
                    myEventHandler = getDefaultEventHandler(),
                    defaultOnSuccess = myEventHandler.onSuccess,
                    loadData;
                
                    //Customise the default event handler for this response's specificities
                    myEventHandler.onSuccess = function(resp) {
                        //We manually add a Search field here.
                        var data = [{
                            categoryname: 'Search'
                        }].concat(resp.category);
                    
                        //Fill the DS with the new info
                        fillDataSource(ds, data, null, null);
                    
                        //This API method is cacheable, let's save the response.
                        addToCache(api, fn, [subChannelUrl], data, null, null);
                        
                        defaultOnSuccess(data);
                    };
                    
                    //Load data handler
                    loadData = function () {
                
                        //Get the data from cache if possible
                        var cacheItem = getFromCache(api, fn, [subChannelUrl]);

                        if(cacheItem == null) {
                            //Do server request
                            callService(api, fn,
								//'http://192.168.0.114/dev/telstra/bpmovies/subchannel.xml', //JUERGEN: FOR DEV ONLY
                                MOVIES_SERVICE_URL + subChannelUrl,
                                {
                                    method:     'get'
                                },
                                myEventHandler,
                                false
                                );

                            return;
                        }
                    
                        //Fill the DS with the cached info
                        fillDataSource(ds, cacheItem.data, cacheItem.totalItems, cacheItem.lastPageLoaded);
                    };

                    ds.addEventListener("accedo:datasource:load", loadData);
                
                    return ds;
                },
            
                /**
             * Gives access to movies relative to a given category
             * @param {String} categoryID, e.g. '/category/Movies/Comedy'
             * @param {Object} opts (optionnal) Options object with pageSize, page, and sort properties
             * @returns {accedo.data.ds}
             */
                getCategory: function(categoryID, opts, eventHandler) {
					accedo.console.log("---------------getCategory -----------------:"+categoryID);
                    if (!categoryID) {
                        accedo.console.log('categoryID is compulsory');
                        return null;
                    }
                
                    //Remove the parameter's HTML encoding, then encode it for JS.
                    //Converts 'Children &amp; Family' to 'Children%20%26%20Family'
                    categoryID = escape(accedo.utils.string.unescapeHTML(categoryID));
                
                    var ds = accedo.data.ds({
                        hasPagination: true
                    }),
                    api = "movies",
                    fn = "getCategory",
                    myEventHandler = getDefaultEventHandler(eventHandler),
                    defaultOnSuccess = myEventHandler.onSuccess,
                    loadData, contextArgs,
                    prependMediaItem_done = false,
                    utils = telstra.sua.module.bpvod.utils;
                    
                    //Build a contextArgs object that reflects all the options used
                    // (sets them to the default value if not present)
                    contextArgs = accedo.utils.object.extend({
                        pageSize :  6,
                        sort:       'most recent'
                    }, opts);

                    //Customise the default event handler for this response's specificities
                    myEventHandler.onSuccess = function(resp) {
                        var totalItems, totalItemsLimit,
                        lastPageLoaded,
                        data,
                        i;
                    
                        if (!accedo.utils.object.isEmpty(resp.mediaitems)){
                            data = resp.mediaitems.mediaitem;
                        }else{
                            data = [];
                        }
                        if (!accedo.utils.object.isArray(data)){
                            data = [data];
                        }
                        
                        accedo.console.log("getCategory success: ");
						//accedo.console.log("resp: "+JSON.stringify(resp));
                
                        totalItems = parseInt(resp.totalitems);
                        /*
                     * totalItemsLimit is for outputting a datasource which has a manually set limit
                     * It's used in SUA home bpvod menu "new releases" movie list
                     * We'll fill datasource with this totalItemsLimit, however, for caching we still have to use the 
                     * original totalItems in order to to mess it up
                     */
                        if (contextArgs.totalItemsLimit){
                            totalItemsLimit = contextArgs.totalItemsLimit;
                        }else{
                            totalItemsLimit = totalItems;
                        }
                        lastPageLoaded = parseInt(resp.currentpage);
                    
                        //This API method is cacheable, let's save the response.
                        addToCache(api, fn,
                            [categoryID, contextArgs.pageSize, contextArgs.sort, lastPageLoaded],
                            data, totalItems, lastPageLoaded);
                        
                    
                        if (!accedo.utils.object.isUndefined(telstra.sua.module.bpvod.session)){
                            data = telstra.sua.module.bpvod.utils.updateVideoDataRented(data);
                        }
                        if (contextArgs.additionalData){
                            data = data.concat(contextArgs.additionalData);
                        }
                        if (contextArgs.prependData) {
                            data = [contextArgs.prependData].concat(data);
                        }
                    
                        //Fill the DS with the new info
                        fillDataSource(ds, data, totalItemsLimit, lastPageLoaded, contextArgs.pageSize);
                    
                        defaultOnSuccess(data);
                    };
                    
                    
                    //Load data handler
                    loadData = function (loadParams) {
                        accedo.console.info('Received load event in getCategory, now in loadData');
                        var page;
                        if ('dsIndex' in loadParams){
                            page = Math.floor(loadParams.dsIndex / contextArgs.pageSize)+1;
                        }else{
                            page = ds.getLastPageLoaded() ? ds.getLastPageLoaded() + 1 : 1
                        }
                    
                        //Get the data from cache if possible
                        var cacheItem = getFromCache(api, fn,
                            [categoryID, contextArgs.pageSize, contextArgs.sort, page]);

                        if(cacheItem == null) {
                            //Do server request
                            callService(api, fn,
								//'http://192.168.0.114/dev/telstra/bpmovies/movies.xml',	//JUERGEN: FOR DEV ONLY
                                MOVIES_SERVICE_URL + categoryID,
                                {
                                    method:     'get',
                                    parameters: accedo.utils.hash(
                                    {
                                        view:       'detail-longsynopsis',
                                        page:       page,
                                        pagesize:   contextArgs.pageSize,
                                        sort:       contextArgs.sort
                                    }
                                    )
                                },
                                myEventHandler,
                                false
                                );

                            return;
                        }
                        //defer the fill datasource, as it'll look like the same behaviour as ajax call success
                        accedo.utils.fn.defer(function(){
                            var newData = telstra.sua.module.bpvod.utils.updateVideoDataRented(cacheItem.data);
                    
                            if (contextArgs.prependData) {
                                newData = [contextArgs.prependData].concat(newData);
                            }
                
                            //Fill the DS with the cached info
                            fillDataSource(ds, newData, cacheItem.totalItems, cacheItem.lastPageLoaded, contextArgs.pageSize);
                        });
                    };
            
                    //Special case when we need to prepend a secific mediaItem at the beginning of a category (used for promos)
                    if ('prependMediaItem' in contextArgs) {
                        ds.addEventListener("accedo:datasource:load", function(evt){
                    
                            if (!prependMediaItem_done) {
                                prependMediaItem_done = true;
                                accedo.console.info('Received load event in getCategory with prependMediaItem');
                        
                                obj.movies.getMediaItem(contextArgs.prependMediaItem, {
                                    onSuccess: function(resp){
                                        contextArgs.prependData = resp;
                                        // ds.appendData([resp]);
                                        loadData(evt);
                                    },
                                    onFailure: function(){
                                        eventHandler && eventHandler.onFailure && eventHandler.onFailure();
                                    }
                                });
                            } else {
                                loadData(evt);
                            }
                    
                        });
                    } else {
                        ds.addEventListener("accedo:datasource:load", loadData);
                    }
            
                    return ds;
                },
        
                /**
             * Fetches the details of a given media item. Use callbacks (onSuccess, etc.) to do something with it.
             * @param {String} mediaItemID, e.g. 'mi11354294'
             * @param {Object} eventHandler (optional) An object containing onSuccess, onFailure, etc. functions
             * @example telstra.api.manager.movies.getMediaItem('mi11354294', {
             *              onFailure: function(error){accedo.console.log('ERROR, code: '+error.errorcode)},
             *              onSuccess: function(mediaItem){accedo.console.log('MediaItem found, title: '+mediaItem.title)}
             *          })
             */
                getMediaItem: function(mediaItemID, eventHandler) {
                    if (!mediaItemID) {
                        accedo.console.log('mediaItemID is compulsory');
                        return null;
                    }
                
                    var api = "movies",
                    fn = "getMediaItem",
                    //Get the data from cache if possible
                    cacheItem = getFromCache(api, fn, [mediaItemID]),
                    myEventHandler = getDefaultEventHandler(eventHandler),
                    defaultOnSuccess = myEventHandler.onSuccess;
                
                
                    if(cacheItem == null) {
                    
                        //Customise the default event handler for this response's specificities
                        myEventHandler.onSuccess = function(resp) {
                            //This API method is cacheable, let's save the response.
                            addToCache(api, fn,
                                [mediaItemID],
                                resp, null, null);
                            
                            defaultOnSuccess(resp);
                        };
                    
                        //Do server request
                        callService(api, fn,
                            MOVIES_SERVICE_URL + '/mediaitem/' + mediaItemID,
                            {
                                method:     'get',
                                parameters: accedo.utils.hash({
                                    view:   'detail-longsynopsis'
                                })
                            },
                            myEventHandler,
                            false
                            );

                        return;
                    }
                
                    //Pass the cached data object to the provided callback
                    launchCacheCallbacks(eventHandler, cacheItem.data);
                },
            
                /**
             * Fetches the details of TV feed episode for a given TV season. Use callbacks (onSuccess, etc.) to do something with it.
             * @param {String} seasonItemID, e.g. 'si10436521'
             * @param {Object} opts (optionnal) Options object with pageSize and page properties
             * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
             * @example telstra.api.manager.movies.getSeasonFeed('si10436521', {})
             * @returns {accedo.data.ds}
             */
                getSeasonFeed : function(seasonItemID, opts) {
                    if (!seasonItemID) {
                        accedo.console.log('seasonItemID is compulsory');
                        return null;
                    }

                    var ds = accedo.data.ds({
                        hasPagination: true
                    }),
                    api = "movies",
                    fn = "getSeasonFeed",
                    myEventHandler = getDefaultEventHandler(),
                    defaultOnSuccess = myEventHandler.onSuccess,
                    loadData, contextArgs;
                    
                    //Build a contextArgs object that reflects all the options used
                    // (sets them to the default value if not present)
                    contextArgs = accedo.utils.object.extend({
                        pageSize :  DEFAULT_SIZE
                    }, opts);
                    
                    //Customise the default event handler for this response's specificities
                    myEventHandler.onSuccess = function(resp) {
                        var data,
                        totalItems,
                        lastPageLoaded;

                        if (!accedo.utils.object.isEmpty(resp.mediaitems)){
                            data = resp.mediaitems.mediaitem;
                        }else{
                            data = [];
                        }
                        if (!accedo.utils.object.isArray(data)){
                            data = [data];
                        }
                        
                        totalItems = parseInt(resp.totalitems);
                        lastPageLoaded = parseInt(resp.currentpage);

                        //This API method is cacheable, let's save the response.
                        addToCache(api, fn,
                            [seasonItemID, contextArgs.pageSize, lastPageLoaded],
                            data, totalItems, lastPageLoaded);
                        
                        if (!accedo.utils.object.isUndefined(telstra.sua.module.bpvod.session)){
                            data = telstra.sua.module.bpvod.utils.updateVideoDataRented(data);
                        }
                        //Fill the DS with the new info
                        fillDataSource(ds, data, totalItems, lastPageLoaded, contextArgs.pageSize);
                
                        defaultOnSuccess(data);
                    };

                    //Load data handler
                    loadData = function (loadParams) {
                        var page;
                        if ('dsIndex' in loadParams){
                            page = Math.floor(loadParams.dsIndex / contextArgs.pageSize)+1;
                        }else{
                            page = ds.getLastPageLoaded() ? ds.getLastPageLoaded() + 1 : 1
                        }

                        //Get the data from cache if possible
                        var cacheItem = getFromCache(api, fn,
                            [seasonItemID, contextArgs.pageSize, page]);

                        if(cacheItem == null) {
                            //Do server request
                            callService(api, fn,
                                MOVIES_SERVICE_URL + '/season/' + seasonItemID,
                                {
                                    method:     'get',
                                    parameters: accedo.utils.hash(
                                    {
                                        page:       page,
                                        pagesize:   contextArgs.pageSize
                                    }
                                    )
                                },
                                myEventHandler,
                                false
                                );

                            return;
                        }
                        //defer the fill datasource, as it'll look like the same behaviour as ajax call success
                        accedo.utils.fn.defer(function(){
                            var newData = telstra.sua.module.bpvod.utils.updateVideoDataRented(cacheItem.data);
                            //Fill the DS with the cached info
                            fillDataSource(ds, newData, cacheItem.totalItems, cacheItem.lastPageLoaded, contextArgs.pageSize);
                        });
                    };

                    ds.addEventListener("accedo:datasource:load", loadData);

                    return ds;
                },

                /**
             * Search the mediaItem by its title
             * @param {String} title, e.g. 'Inkheart'
             * @param {String} subChannel, e.g. 'Movies'
             * @param {Object} opts (optionnal) Options object with pageSize, adult and page properties
             * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
             * @example telstra.api.manager.movies.searchTitle('Inkheart', 'Movies', {})
             * @returns {accedo.data.ds}
             */
                searchTitle : function(title, subChannel, opts, eventHandler){
                    if (!title) {
                        accedo.console.log('title is compulsory');
                        return null;
                    }

                    if (!subChannel) {
                        accedo.console.log('subChannel is compulsory');
                        return null;
                    }

                    var ds = accedo.data.ds({
                        hasPagination: true
                    }),
                    api = "movies",
                    fn = "searchTitle",
                    myEventHandler = getDefaultEventHandler(eventHandler),
                    defaultOnSuccess = myEventHandler.onSuccess,
                    loadData, contextArgs;
                    
                    //Build a contextArgs object that reflects all the options used
                    // (sets them to the default value if not present)
                    contextArgs = accedo.utils.object.extend({
                        title: title + "*",
                        subchannel: subChannel,
                        view: 'detail',
                        adult: opts.adult ? opts.adult : false, //not sure if this is still needed
                        pageSize :  opts.pageSize || DEFAULT_SIZE
                    }, opts);
                        
                    //Customise the default event handler for this response's specificities
                    myEventHandler.onSuccess = function(resp) {
                        var data,
                        totalItems,
                        lastPageLoaded;
                    
                        data = resp.mediaitems ? resp.mediaitems.mediaitem : [];
                        if (!accedo.utils.object.isArray(data)){
                            data = [data];
                        }
                        totalItems = parseInt(resp.totalitems);
                        lastPageLoaded = parseInt(resp.currentpage) || 1;
                    


                        //This API method is cacheable, let's save the response.
                        addToCache(api, fn,
                            [title, subChannel, contextArgs.adult, contextArgs.pageSize, lastPageLoaded],
                            data, totalItems, lastPageLoaded);
                    
                        data = telstra.sua.module.bpvod.utils.updateVideoDataRented(data);
                    
                        //Fill the DS with the new info
                        fillDataSource(ds, data, totalItems, lastPageLoaded);
                        defaultOnSuccess(data);
                    };

                    //Load data handler
                    loadData = function () {

                        if (accedo.utils.object.isUndefined(contextArgs.page)){
                            page = ds.getLastPageLoaded() ? ds.getLastPageLoaded() + 1 : 1
                        }else{
                            page = contextArgs.page;
                        }

                        //Get the data from cache if possible
                        var cacheItem = getFromCache(api, fn,
                            [title, subChannel, contextArgs.pageSize, contextArgs.page]);

                        if(cacheItem == null) {
                            //Do server request
                            callService(api, fn,
                            	//'http://192.168.0.114/dev/telstra/bpmovies/searchtitle.xml',	 //JUERGEN: FOR DEV ONLY
                                MOVIES_SERVICE_URL + '/searchtitle/',
                                {
                                    method:     'get',
                                    parameters: accedo.utils.hash(
                                    {
                                        title:      contextArgs.title,
                                        subchannel: contextArgs.subchannel,
                                        view:       contextArgs.view,
                                        adult:      contextArgs.adult,
                                        page:       page,
                                        pagesize:   contextArgs.pageSize
                                    }
                                    )
                                },
                                myEventHandler,
                                false
                                );

                            return;
                        }
                    
                        //Fill the DS with the cached info
                        fillDataSource(ds, telstra.sua.module.bpvod.utils.updateVideoDataRented(cacheItem.data),
                            cacheItem.totalItems, cacheItem.lastPageLoaded);
                    };

                    ds.addEventListener("accedo:datasource:load", loadData);

                    return ds;
                },

                /**
             * Search the mediaItem by its director/actors
             * @param {String} name, e.g. 'Paul Bettany'
             * @param {String} subChannel, e.g. 'Movies'
             * @param {Object} opts (optionnal) Options object with pageSize, adult and page properties
             * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
             * @example telstra.api.manager.movies.searchPeople('Paul Bettany', 'Movies', {})
             * @returns {accedo.data.ds}
             */
                searchPeople : function(name, subChannel, opts){
                    if (!name) {
                        accedo.console.log('name is compulsory');
                        return null;
                    }

                    if (!subChannel) {
                        accedo.console.log('subChannel is compulsory');
                        return null;
                    }

                    var ds = accedo.data.ds({
                        hasPagination: true
                    }),
                    api = "movies",
                    fn = "searchPeople",
                    myEventHandler = getDefaultEventHandler(),
                    defaultOnSuccess = myEventHandler.onSuccess,
                    loadData,
                    contextArgs;
                
                    //Customise the default event handler for this response's specificities
                    myEventHandler.onSuccess = function(resp) {
                        var data,
                        totalItems = resp.totalitems,
                        lastPageLoaded = resp.currentpage;
                    
                        data = resp.people ? resp.people.person : [];
                    
                        if (!accedo.utils.object.isArray(data)){
                            data = [data];
                        }
                        //Fill the DS with the new info
                        fillDataSource(ds, data, totalItems, lastPageLoaded);

                        //This API method is cacheable, let's save the response.
                        addToCache(api, fn,
                            [name, subChannel, contextArgs.adult, contextArgs.pageSize, contextArgs.page],
                            data, totalItems, lastPageLoaded);
                
                        defaultOnSuccess(data);
                    };

                    //Load data handler
                    loadData = function () {

                        //Build a contextArgs object that reflects all the options used
                        // (sets them to the default value if not present)
                        contextArgs = accedo.utils.object.extend({
                            name: name + "*",
                            subchannel: subChannel,
                            view: 'detail',
                            adult: opts.adult ? opts.adult : false, //not sure if this is still needed
                            pageSize :  opts.pageSize || DEFAULT_SIZE,
                            page:       ds.getLastPageLoaded() ? ds.getLastPageLoaded() + 1 : 1
                        }, opts);


                        //Get the data from cache if possible
                        var cacheItem = getFromCache(api, fn,
                            [name, subChannel, contextArgs.pageSize, contextArgs.page]);

                        if(cacheItem == null) {
                            //Do server request
                            callService(api, fn,
                            	//'http://192.168.0.114/dev/telstra/bpmovies/searchpeople.xml',	 //JUERGEN: FOR DEV ONLY
                                MOVIES_SERVICE_URL + '/searchpeople/',
                                {
                                    method:     'get',
                                    parameters: accedo.utils.hash(
                                    {
                                        name:      contextArgs.name,
                                        subchannel: contextArgs.subchannel,
                                        view:       contextArgs.view,
                                        adult:      contextArgs.adult,
                                        page:       contextArgs.page,
                                        pagesize:   contextArgs.pageSize
                                    }
                                    )
                                },
                                myEventHandler,
                                false
                                );

                            return;
                        }

                        //Fill the DS with the cached info
                        fillDataSource(ds, cacheItem.data, cacheItem.totalItems, cacheItem.lastPageLoaded);
                    };

                    ds.addEventListener("accedo:datasource:load", loadData);

                    return ds;
                },

                /**
             * Get details for an artist
             * @param {String} url the url returned from search People functions, e.g. '/people/actor/Movies/Paul.Bettany'
             * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
             * @example telstra.api.manager.movies.peopleDetails('/people/actor/Movies/Paul.Bettany');
             * @returns {accedo.data.ds}
             */
                peopleDetails : function(url){
                    if (!url) {
                        accedo.console.log('url is compulsory');
                        return null;
                    }

                    var ds  = accedo.data.ds({
                        hasPagination: false
                    }),
                    api = "movies",
                    fn  = "peopleDetails",
                    myEventHandler = getDefaultEventHandler(),
                    defaultOnSuccess = myEventHandler.onSuccess,
                    loadData;
                
                    //Customise the default event handler for this response's specificities
                    myEventHandler.onSuccess = function(resp) {
                        var data = resp.mediaitems.mediaitem;

                        //Make data an array (if there is only one element it is an object)
                        //If no data was received, leave data as is
                        if (data && !accedo.utils.object.isArray(data)) {
                            data = [data];
                        }
                        //This API method is cacheable, let's save the response.
                        addToCache(api, fn,
                            [url],
                            data, null, null);

                        data = telstra.sua.module.bpvod.utils.updateVideoDataRented(data);
                        fillDataSource(ds, data, null, null);

                
                        defaultOnSuccess(data);
                    };

                    loadData = function() {

                        //Get the data from cache if possible
                        var cacheItem = getFromCache(api, fn, [url]);

                        if(cacheItem == null) {
                            //Do server request
                            callService(api, fn,
                                MOVIES_SERVICE_URL + url,
                                {
                                    method:     'get',
                                    parameters: accedo.utils.hash({
                                        view:   'detail-longsynopsis'
                                    })
                                },
                                myEventHandler,
                                false
                                );

                            return;
                        }
                        //Fill the DS with the cached info
                        fillDataSource(ds, telstra.sua.module.bpvod.utils.updateVideoDataRented(cacheItem.data),
                            null, null);
                    };

                    ds.addEventListener("accedo:datasource:load", loadData);

                    return ds;
                },

                /**
             * Logs the user in according to the parameters.
             * @param {String} productID
             * @param {Object} eventHandler (optional) An object containing onSuccess, onFailure, etc. functions
             * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
             * @example telstra.api.manager.movies.purchaseCheck("mi12121728", {
             *      onFailure: function(error){accedo.console.log('ERROR, code is: '+error.errorcode)},
             *      onSuccess: function(resp){accedo.console.log('purchse check: '+resp)}
             * });
             */
                purchaseCheck: function(productID, eventHandler){
                	accedo.console.info('purchaseCheck '+productID);

                    if (!productID) {
                        accedo.console.info('productID are compulsory');
                        return;
                    }

                    var api = 'movies',
                    fn  = 'purchaseCheck',
                
                    myEventHandler = getDefaultEventHandler(eventHandler);
                
                    callService(api, fn,
                        MOVIES_SECURED_URL + '/purchase',
                        {
                            method:     'get',
                            parameters: accedo.utils.hash(
                            {
                                productID : productID
                            }
                            )
                        },
                        myEventHandler,
                        false
                        );
                },

                /**
             * perform the purchase action
             * @param {String} productID
             * @param {Object} eventHandler (optional) An object containing onSuccess, onFailure, etc. functions
             * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
             * @example telstra.api.manager.movies.purchase("mi12121728",{
                                onFailure: function(error){
                                    accedo.console.log('ERROR, code is: '+error.errorcode);
                                },
                                onSuccess: function(data){
                                    accedo.console.log('puchase success: '+ data.purchaseid);
                                }
                            });
             */
                purchase: function(productID, eventHandler){
                	accedo.console.info('purchase '+productID);

                    if (!productID) {
                        accedo.console.info('productID are compulsory');
                        return;
                    }

                    var api = 'movies',
                    fn  = 'purchase',
                    widevineESN = accedo.device.manager.identification.getUniqueID();
                    
                    if(!widevineESN){
                        accedo.console.info('widevineESN not found');
                        return;
                    }

                    var myEventHandler = getDefaultEventHandler(eventHandler),
                    defaultOnSuccess = myEventHandler.onSuccess;
                
                    //Customise the default event handler for this response's specificities
                    myEventHandler.onSuccess = function(resp) {
                        telstra.api.manager.movies.accountInfo.vouchercredit = resp.vouchercreditafterpurchase;

                        //Parse the license info
                        var xml=lib.xmlobjectifier.textToXML(resp.license);
                        var json = lib.xmlobjectifier.xmlToJSON(xml, {
                            outputType: 1
                        });
                        
                        //As the tags converted by this new library is not the same as the previous library,
                        //We need to make a change to the feed
                        json = telstra.sua.module.bpvod.utils.migrateXmlJson(json);

                        resp.license = json.license;
                        accedo.console.log(json.license);

                        defaultOnSuccess(resp);
                    };

                    var postBody = "<purchaseReq><clientID><![CDATA[" + widevineESN + "]]></clientID></purchaseReq>";

                    callService(api, fn,
                        MOVIES_SECURED_URL + '/purchase',
                        {
                            method:     'post',
                            parameters: accedo.utils.hash(
                            {
                                productID : productID
                            }
                            ),
                            postBody:   postBody
                        },
                        myEventHandler,
                        true
                        );
                },

                /**
             * register a voucher
             * @param {String} voucherPin
             * @param {Object} eventHandler (optional) An object containing onSuccess, onFailure, etc. functions
             * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
             * @example telstra.api.manager.movies.registerVoucher("9XW4KLFX4XPMMD",{
                                onFailure: function(error){
                                    accedo.console.log('ERROR, code is: '+error.errorcode);
                                },
                                onSuccess: function(data){
                                    accedo.console.log('register voucher success: '+ data.voucher.vouchercredit );
                                }
                            });
             */
                registerVoucher: function(voucherPin, eventHandler){

                    if (!voucherPin) {
                        accedo.console.info('voucherPin is compulsory');
                        return;
                    }

                    var api = 'movies',
                    fn = 'registerVoucher',
                    myEventHandler = getDefaultEventHandler(eventHandler),
                    defaultOnSuccess = myEventHandler.onSuccess,
                    defaultOnFailure = myEventHandler.onFailure,
                    defaultOnException = myEventHandler.onException;
                
                    //Customise the default event handler for this response's specificities
                    myEventHandler.onSuccess = function(resp) {
                        //@todo: or resp.voucher? need voucher to test..
                        defaultOnSuccess(resp);
                    };

                    myEventHandler.onFailure = function(resp) {
                        //@todo: or resp.voucher? need voucher to test..
                        defaultOnFailure(resp);
                    };

                    myEventHandler.onException = function(resp) {
                        //@todo: or resp.voucher? need voucher to test..
                        defaultOnException(resp);
                    };

                    callService(api, fn,
                        MOVIES_SECURED_URL + '/vouchercredit',
                        {
                            method:     'post',
                            parameters: accedo.utils.hash(
                            {
                                voucherPin : voucherPin
                            }
                            )
                        },
                        myEventHandler,
                        false
                        );

                },

                /**
             * get amount of voucher credit
             * @param {Object} eventHandler (optional) An object containing onSuccess, onFailure, etc. functions
             * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
             * @example telstra.api.manager.movies.getVoucher({
                                onFailure: function(error){
                                    accedo.console.log('ERROR, code is: '+error.errorcode);
                                },
                                onSuccess: function(data){
                                    accedo.console.log('get voucher success: '+ data.voucher.vouchercredit);
                                }
                            });
             */
                getVoucher: function(eventHandler){

                    var api = 'movies',
                    fn = 'getVoucher',
                    myEventHandler = getDefaultEventHandler(eventHandler),
                    defaultOnSuccess = myEventHandler.onSuccess;
                
                    //Customise the default event handler for this response's specificities
                    myEventHandler.onSuccess = function(resp) {
                        //update account info
                        telstra.api.manager.movies.accountInfo.vouchercredit = resp.vouchercredit;

                        defaultOnSuccess(resp);
                    };
                
                    callService(api, fn,
                        MOVIES_SECURED_URL + '/vouchercredit',
                        {
                            method:     'get'
                        },
                        myEventHandler,
                        false
                        );
                },


                /**
             * get the purchase history
             * @param {Object} opts (optionnal) Options object with pageSize and page properties
             * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
             */
                getAccountPurchaseInfo : function(opts, eventHandler){

                    var ds = accedo.data.ds({
                        hasPagination: true
                    }),
                    api = "movies",
                    fn = "getAccountPurchaseInfo",
                    myEventHandler = getDefaultEventHandler(eventHandler),
                    defaultOnSuccess = myEventHandler.onSuccess,
                    defaultOnFailure = myEventHandler.onFailure;
                
                    //Customise the default event handler for this response's specificities
                    myEventHandler.onSuccess = function(resp) {
                        var data;

                        if (!accedo.utils.object.isEmpty(resp.purchaseitems)) {
                            data = resp.purchaseitems.purchaseitem;
                        } else {
                            data = [];
                        }
                        if (!accedo.utils.object.isArray(data)) {
                            data = [data];
                        }

                        //This API method is cacheable, let's save the response.
                        addToCache(api, fn, [], data);

                        defaultOnSuccess(data);
                    };

                    //Get the data from cache if possible
                    var cacheItem = getFromCache(api, fn, []);

                    if(cacheItem == null) {
                        //Do server request
                        callService(api, fn,
                            MOVIES_SECURED_URL + '/purchasehistory/',
                            {
                                method:     'get',
                                parameters: accedo.utils.hash(
                                {
                                    page:       opts.page,
                                    pagesize:   opts.pageSize
                                }
                                )
                            },
                            myEventHandler,
                            false
                            );

                        return;
                    }

                    defaultOnSuccess(cacheItem.data);
                },

                /**
             * get the demo license for free movice
             * @param {String} productID - Id of the free video Item
             * @param {Object} eventHandler (optional) An object containing onSuccess, onFailure, etc. functions
             * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
             * @example telstra.api.manager.movies.getDemoLicense("mi12121734",{
                                onFailure: function(error){
                                    accedo.console.log('ERROR, code is: '+error.errorcode);
                                },
                                onSuccess: function(data){
                                    accedo.console.log('get DemoLicense: '+ data.demolicense.demolicenseID);
                                }
                            });
             *
             */
                getDemoLicense: function(productID, eventHandler){
                	accedo.console.info('getDemoLicense '+productID);

                    if (!productID) {
                        accedo.console.info('productID is compulsory');
                        return;
                    }

                    var api = 'movies',
                    fn = 'getDemoLicense',
                    myEventHandler = getDefaultEventHandler(eventHandler),
                	/*
                    postBody = "<demoLicenseReq><clientinfo><![CDATA["
                    + accedo.device.manager.identification.getWidevineESN() +
                    "]]></clientinfo></demoLicenseReq>";
                    */
                    postBody = "<demoLicenseReq><clientID><![CDATA["
                    + accedo.device.manager.identification.getUniqueID() +
                    "]]></clientID></demoLicenseReq>";
                    
                    //accedo.console.log("postBody: "+postBody);

                    callService(api, fn,
                        MOVIES_SECURED_URL + '/demolicense',
                        {
                            method:     'post',
                            parameters: accedo.utils.hash(
                            {
                                productID : productID
                            }
                            ),
                            postBody : postBody,
                            authtoken : generateSecretKey()
                        },
                        myEventHandler,
                        false
                        );

                },

                /**
             * update an user profile item (a key, value pair) to the server
             * @param {String} key - key of the item
             * @param {String} value - value of the item
             * @param {Object} eventHandler (optional) An object containing onSuccess, onFailure, etc. functions
             * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
             * @example telstra.api.manager.movies.updateDevUserProfile("testingKey", "testingValue",{
                                onFailure: function(error){
                                    accedo.console.log('ERROR, code is: '+error.errorcode);
                                },
                                onSuccess: function(data){
                                    accedo.console.log('update success');
                                }
                            });
             *
             */
                updateDevUserProfile : function(key, value, eventHandler){

                    if (!key) {
                        accedo.console.info('key is compulsory');
                        return;
                    }

                    if (!value) {
                        accedo.console.info('value is compulsory');
                        return;
                    }

                    var api = 'movies',
                    fn = 'updateDevUserProfile',
                    myEventHandler = getDefaultEventHandler(eventHandler);
                

                    var postBody = "<profileAttribute>"
                    + "<key>" + key + "</key>"
                    + "<value><![CDATA["
                    + value
                    + "]]></value>"
                    + "</profileAttribute>";

                    callService(api, fn,
                        MOVIES_SECURED_URL + '/userdeviceprofile/attribute',
                        {
                            method:     'post',
                            postBody : postBody
                        },
                        myEventHandler,
                        true
                        );
                },

                /**
             * get an value of user profile item from the server
             * @param {String} key - key of the item
             * @param {Object} eventHandler (optional) An object containing onSuccess, onFailure, etc. functions
             * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
             * @example telstra.api.manager.movies.getDevUserProfile("testingKey", {
                                onFailure: function(error){
                                    accedo.console.log('ERROR, code is: '+error.errorcode);
                                },
                                onSuccess: function(data){
                                    accedo.console.log('getDevUserProfile success: ');
                                    accedo.console.log(data);
                                }
                            });
             *
             */
                getDevUserProfile : function(key, eventHandler){

                    if (!key) {
                        accedo.console.info('key is compulsory');
                        return;
                    }

                    var api = 'movies',
                    fn = 'getDevUserProfile',
                    myEventHandler = getDefaultEventHandler(eventHandler);

                    callService(api, fn,
                        MOVIES_SECURED_URL + '/userdeviceprofile/attribute',
                        {
                            method:     'get',
                            parameters: accedo.utils.hash(
                            {
                                key : key
                            }
                            )
                        },
                        myEventHandler,
                        true
                        );
                },

                /**
             * delete an user profile item from the server
             * @param {String} key - key of the item
             * @param {Object} eventHandler (optional) An object containing onSuccess, onFailure, etc. functions
             * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
             * @example telstra.api.manager.movies.delDevUserProfile("testingKey", {
                                onFailure: function(error){
                                    accedo.console.log('ERROR, code is: '+error.errorcode);
                                },
                                onSuccess: function(data){
                                    accedo.console.log('delDevUserProfile success: ');
                                }
                            });
             *
             */
                delDevUserProfile : function(key, eventHandler){

                    if (!key) {
                        accedo.console.info('key is compulsory');
                        return;
                    }

                    var api = 'movies',
                    fn = 'delDevUserProfile',
                    myEventHandler = getDefaultEventHandler(eventHandler);

                    callService(api, fn,
                        MOVIES_SECURED_URL + '/userdeviceprofile/attribute',
                        {
                            method:     'delete',
                            parameters: accedo.utils.hash(
                            {
                                key : key
                            }
                            )
                        },
                        myEventHandler,
                        false
                        );
                },


                /**
             * register a user account
             * @param {Object} info, a JSON object contain all the required and optional info
             * @param {String} info.email (Required) account email
             * @param {String} info.pwd (Required) account password
             * @param {String} info.accountPin (Required) account pin
             * @param {Boolean} info.useAccountPin (Required) true for using account pin for purchase
             * @param {Boolean} info.rceiveNews (Required) true for agreeing to receive newsletter
             * @param {String} info.firstName (Required) firstName
             * @param {String} info.surName (Required) surName
             * @param {String} info.deviceType (Required) Type of the device id (e.g. MAC)
             * @param {String} info.cardNumber (Required) credit Card Number
             * @param {String} info.cardType (Required) credit Card Type
             * @param {String} info.expiryDate (Required) expiryDate of the credit card
             * @param {String} info.securityCode (Required) securityCode of the credit card
             * @param {String} info.deviceId(Required) unique id of the device
             * @param {String} info.deviceName (Required) name of the device
             * @param {String} info.bigPondId (Optional) BigPond Unique identifier
             * @param {String} info.payPrefer (Optional) prefered payment method (e.g. cc)
             * @param {Boolean} info.telstraBill (Optional) true for user having an existing telstra billing
             * @param {String} info.birth (Optional) user's birthday
             * @param {String} info.gender (Optional) user's gender (male or female)
             * @param {String} info.phoneNum (Optional) user's phone Number
             * @param {String} info.mobile (Optional) user's mobile phone Number
             * @param {String} info.postCode (Optional) user's postCode
             * @param {String} info.nickName (Optional) user's nickName
             * @param {String} info.referredBy (Optional) media source refering the user to service (magazine or tv)
             *
             * @param {Object} eventHandler (optional) An object containing onSuccess, onFailure, etc. functions
             * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
             * @example telstra.api.manager.movies.registerMoviesAccount({
                                    email : "dgp@dgp.dgp",
                                    pwd: "dgpdgp",
                                    accountPin : "1111",
                                    useAccountPin:  true,
                                    receiveNews:  true,
                                    firstName: "Accedo",
                                    surName: "Testing",
                                    deviceType: "MAC",
                                    deviceId: "112233445566",
                                    deviceName: "testing",
                                    cardNumber: "5163201000012969",
                                    cardType: "mastercard",
                                    expiryDate: "2015-08",
                                    securityCode: "000"
                                },{
                                onFailure: function(error){
                                    accedo.console.log('ERROR, code is: '+error.errorcode);
                                },
                                onSuccess: function(data){
                                    accedo.console.log('register success');
                                }
                            });
             *
             */
                registerMoviesAccount : function(info, eventHandler){

                    if (!info) {
                        accedo.console.info('info is compulsory');
                        return;
                    }

                    if(!(info.email) || !(info.pwd)
                        || !(info.accountPin) || (info.useAccountPin == null)
                        || (info.receiveNews == null) || !(info.firstName)
                        || !(info.surName) || !(info.deviceType)
                        || !(info.cardNumber) || !(info.cardType)
                        || !(info.expiryDate) || !(info.securityCode)
                        || !(info.deviceId) || !(info.deviceName)){
                        accedo.console.info("error: registerMoviesAccount parameter is missing");
                        return;
                    }

                    var api = 'movies',
                    fn = 'registerMoviesAccount',
                    bigPondId = "",
                    payPrefer = "",
                    telstraBill = "",
                    birth = "",
                    gender = "",
                    phoneNum = "",
                    mobile = "",
                    postCode = "",
                    nickName = "",
                    referredBy = "";

                    if(info.bigPondId){
                        bigPondId = "<bigPondID><![CDATA["+info.bigPondId+"]]></bigPondID>";
                    }

                    if(info.payPrefer){
                        payPrefer = "<paymentPreference><![CDATA["+info.payPrefer+"]]></paymentPreference>";
                    }

                    if(info.telstraBill){
                        telstraBill = info.telstraBill? 1:0;
                        telstraBill = "<hasTelstraBilling><![CDATA["+telstraBill+"]]></hasTelstraBilling>";
                    }

                    if(info.birth){
                        birth = "<dob><![CDATA["+info.birth+"]]></dob>";
                    }

                    if(info.gender){
                        gender = "<gender><![CDATA["+info.gender+"]]></gender>";
                    }

                    if(info.phoneNum){
                        phoneNum = "<phoneNumber><![CDATA["+info.phoneNum+"]]></phoneNumber>";
                    }

                    if(info.mobile){
                        mobile = "<mobile><![CDATA["+info.mobile+"]]></mobile>";
                    }

                    if(info.postCode){
                        postCode = "<postCode><![CDATA["+info.postCode+"]]></postCode>";
                    }

                    if(info.nickName){
                        nickName = "<nickname><![CDATA["+info.nickName+"]]></nickname>";
                    }

                    if(info.referredBy){
                        referredBy = "<referredBy><![CDATA["+info.referredBy+"]]></referredBy>";
                    }

                    var myEventHandler = getDefaultEventHandler(eventHandler);

                    var useAccountPin = info.useAccountPin ? 1 : 0;
                    var receiveNews = info.receiveNews ? 1 : 0;
                    var postBody = "<account>"
                    + "<email>" + info.email + "</email>"
                    + "<password>" + (lib.md5.b64_md5(info.pwd) +"==") + "</password>"
                    + bigPondId + payPrefer + telstraBill
                    + "<accountPin>" + info.accountPin + "</accountPin>"
                    + "<useAccountPin>" + useAccountPin + "</useAccountPin>"
                    + "<receiveNewsletter>" + receiveNews + "</receiveNewsletter>"
                    + "<firstname>" + info.firstName + "</firstname>"
                    + "<surname>" + info.surName + "</surname>"
                    + birth + gender + phoneNum + mobile
                    + postCode + nickName + referredBy
                    + "<device type='" + checkHeader() + "'>"
                    + "<deviceID type=" + "'" + info.deviceType +"'"+ ">" + info.deviceId + "</deviceID>"
                    + "<deviceName><![CDATA[" + info.deviceName + "]]></deviceName></device>"
                    + "<creditCard><cardNumber>" + info.cardNumber + "</cardNumber>"
                    + "<cardType>" + info.cardType + "</cardType>"
                    + "<expiryDate>" + info.expiryDate + "</expiryDate>"
                    + "<securityCode>" + info.securityCode + "</securityCode>"
                    +"</creditCard>"
                    + "</account>";

                    callService(api, fn,
                        MOVIES_SECURED_URL + '/account',
                        {
                            method:     'post',
                            postBody :  postBody
                        },
                        myEventHandler,
                        false
                        );
                },


                /**
             * update a user account, note: credit card info is not needed there
             * @param {Object} info, a JSON object contain all the required and optional info
             * @param {String} info.email (Required) account email
             * @param {String} info.pwd (Required) account password
             * @param {String} info.accountPin (Required) account pin
             * @param {Boolean} info.useAccountPin (Required) true for using account pin for purchase
             * @param {Boolean} info.rceiveNews (Required) true for agreeing to receive newsletter
             * @param {String} info.firstName (Required) firstName
             * @param {String} info.surName (Required) surName
             * @param {String} info.deviceId(Required) unique id of the device
             * @param {String} info.deviceName (Required) name of the device
             * @param {String} info.bigPondId (Optional) BigPond Unique identifier
             * @param {String} info.payPrefer (Optional) prefered payment method (e.g. cc)
             * @param {Boolean} info.telstraBill (Optional) true for user having an existing telstra billing
             * @param {String} info.birth (Optional) user's birthday
             * @param {String} info.gender (Optional) user's gender (male or female)
             * @param {String} info.phoneNum (Optional) user's phone Number
             * @param {String} info.mobile (Optional) user's mobile phone Number
             * @param {String} info.postCode (Optional) user's postCode
             * @param {String} info.nickName (Optional) user's nickName
             * @param {String} info.referredBy (Optional) media source refering the user to service (magazine or tv)
             *
             * @param {Object} eventHandler (optional) An object containing onSuccess, onFailure, etc. functions
             * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
             * @example telstra.api.manager.movies.updateMoviesAccount({
                                    email : "dgp@dgp.dgp",
                                    pwd: "dgpdgp",
                                    accountPin : "1111",
                                    useAccountPin:  true,
                                    receiveNews:  true,
                                    firstName: "Accedo2",
                                    surName: "Testing2",
                                    deviceId: "112233445566",
                                    deviceName: "testing2"
                                },{
                                onFailure: function(error){
                                    accedo.console.log('ERROR, code is: '+error.errorcode);
                                },
                                onSuccess: function(data){
                                    accedo.console.log('update success');
                                }
                            });
             *
             */
                updateMoviesAccount : function(info, eventHandler){

                    if (!info) {
                        accedo.console.info('info is compulsory');
                        return;
                    }

                    if ((info.email == null) || (info.accountpin == null)
                        || (info.useaccountpin == null) || (info.receivenewsletter == null)
                        || (info.firstname == null) || (info.surname == null)
                        || (info.deviceid == null) || (info.devicename == null)){
                        accedo.console.info("error: updateMoviesAccount parameter is missing");
                        return;
                    }

                    var api = 'movies',
                    fn = 'updateMoviesAccount',
                    password = "",
                    bigPondId = "",
                    payPrefer = "",
                    telstraBill = "",
                    birth = "",
                    gender = "",
                    phonenum = "",
                    mobile = "",
                    postcode = "",
                    nickname = "";
                    
                    if(info.pwd){
                        password = "<password>" + (lib.md5.b64_md5(info.pwd) +"==") + "</password>";
                    }

                    if(info.bigPondId){
                        bigPondId = "<bigPondID><![CDATA["+info.bigPondId+"]]></bigPondID>";
                    }

                    if(info.payPrefer){
                        payPrefer = "<paymentPreference><![CDATA["+info.payPrefer+"]]></paymentPreference>";
                    }

                    if(info.telstraBill){
                        telstraBill = info.telstraBill? 1:0;
                        telstraBill = "<hasTelstraBilling><![CDATA["+telstraBill+"]]></hasTelstraBilling>";
                    }

                    if(info.birth){
                        birth = "<dob><![CDATA["+info.birth+"]]></dob>";
                    }

                    if(info.gender){
                        gender = "<gender><![CDATA["+info.gender+"]]></gender>";
                    }

                    if(info.phonenum){
                        phonenum = "<phoneNumber><![CDATA["+info.phonenum+"]]></phoneNumber>";
                    }

                    if(info.mobile){
                        mobile = "<mobile><![CDATA["+info.mobile+"]]></mobile>";
                    }

                    if(info.postcode){
                        postcode = "<postcode><![CDATA["+info.postcode+"]]></postcode>";
                    }

                    if(info.nickname){
                        nickname = "<nickname><![CDATA["+info.nickname+"]]></nickname>";
                    }

                    var myEventHandler = getDefaultEventHandler(eventHandler);

                    var postBody = "<account>"
                    + "<email>" + info.email + "</email>"
                    + password + bigPondId + payPrefer + telstraBill
                    + "<accountPin>" + info.accountpin + "</accountPin>"
                    + "<useAccountPin>" + info.useaccountpin + "</useAccountPin>"
                    + "<receiveNewsletter>" + info.receivenewsletter + "</receiveNewsletter>"
                    + "<firstname>" + info.firstname + "</firstname>"
                    + "<surname>" + info.surname + "</surname>"
                    + birth + gender + phonenum + mobile + postcode + nickname
                    + "<deviceName deviceID='" + info.deviceid + "'><![CDATA[" + info.devicename + "]]></deviceName>"
                    + "</account>";

                    callService(api, fn,
                        MOVIES_SECURED_URL + '/account',
                        {
                            method:     'put',
                            postBody :  postBody
                        },
                        myEventHandler,
                        false
                        );
                },

                /**
             * register a credit card
             * @param {Object} info, a JSON object contain all the required and optional info
             * @param {String} info.cardNumber (Required) credit Card Number
             * @param {String} info.cardType (Required) credit Card Type
             * @param {String} info.expiryDate (Required) expiryDate of the credit card
             * @param {String} info.securityCode (Required) securityCode of the credit card
             *
             * @param {Object} eventHandler (optional) An object containing onSuccess, onFailure, etc. functions
             * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
             * @example telstra.api.manager.movies.registerCreditCard({
                                    cardNumber: "5163201000012969",
                                    cardType: "mastercard",
                                    expiryDate: "2015-08",
                                    securityCode: "000"
                                },{
                                onFailure: function(error){
                                    accedo.console.log('ERROR, code is: '+error.errorcode);
                                },
                                onSuccess: function(data){
                                    accedo.console.log('register success');
                                }
                            });
             *
             */
                registerCreditCard : function(info, eventHandler){

                    if (!info) {
                        accedo.console.info('info is compulsory');
                        return;
                    }

                    if(!(info.cardNumber) || !(info.cardType)
                        || !(info.expiryDate) || !(info.securityCode)){
                        accedo.console.info("error: registerCreditCard parameter is missing");
                        return;
                    }

                    var api = 'movies',
                    fn = 'registerCreditCard',
                    myEventHandler = getDefaultEventHandler(eventHandler);

                    var postBody = "<creditCard>"
                    + "<cardNumber>" + info.cardNumber + "</cardNumber>"
                    + "<cardType>" + info.cardType + "</cardType>"
                    + "<expiryDate>" + info.expiryDate + "</expiryDate>"
                    + "<securityCode>" + info.securityCode + "</securityCode>"
                    + "</creditCard>";

                    callService(api, fn,
                        MOVIES_SECURED_URL + '/creditcard',
                        {
                            method:     'post',
                            postBody :  postBody
                        },
                        myEventHandler,
                        false
                        );
                }

            },
        
            /**
             * Time and DST
             */
            time: {
                /**
                 * Gets the current EPG time and the timezone offsets for Australian states
                 * @param {Object} eventHandler
                 * @param {boolean} skipCache True to skip the cache and force an AJAX request
                 * @returns {accedo.data.ds}
                 * @public
                 */
                getTimeAndDST: function(eventHandler, skipCache) {
                    var ds = accedo.data.ds({
                        hasPagination: false
                    }),
                    api = "time",
                    fn = "getTimeAndDST",
                    myEventHandler = getDefaultEventHandler(eventHandler),
                    defaultOnSuccess = myEventHandler.onSuccess,
                    loadData;
                
                    //Temporary code to allow the app continue running even time api is down
                    if (!myEventHandler.onFailure){
                        myEventHandler.onFailure = function(){};
                    }
                
                    //Customise the default event handler for this response's specificities
                    myEventHandler.onSuccess = function(resp) {
                        accedo.console.log('getTimeAndDST onSuccess');
                
                        //Use this code to specify the time / time offsets, for tests
                        // resp = JSON.parse('{"document":{"server":{"datetime":"2011-10-13T02:57:40Z"},"countries":{"country":{"name":"Australia","region":[{"name":"WA","offset":8},{"name":"NT","offset":9.5},{"name":"SA","offset":10.5},{"name":"NSW","offset":11},{"name":"QLD","offset":10},{"name":"VIC","offset":11},{"name":"TAS","offset":11},{"name":"ACT","offset":11}]}}}}');
                
                        var data = resp.document.countries.country.region;

                        //If there is a known region for the user...
                        if (userRegion) {
                            accedo.utils.array.each(data, function(item) {
                                //Find the user's region
                                if (item.name === userRegion.name) {
                                    //If the time offset has changed, update the region
                                    if (item.offset !== userRegion.offset) {
                                        obj.time.setRegion(item, true);
                                    }
                                    throw accedo.$break;
                                }
                            });
                        }

                        //Synchronize the current time with the server
                        setTimeNow(obj.time.parseDate(resp.document.server.datetime).getTime());

                        // Fill the DS with the new info (timezones only, not the time)
                        fillDataSource(ds, data, null, null);
                    
                        // This API method is cacheable, let's save the response.
                        addToCache(api, fn, [], data, null, null);
                        
                        defaultOnSuccess(data);
                    };
                    
                    //Load data handler
                    loadData = function () {
                        ds.removeEventListener("accedo:datasource:load", loadData);
                
                        var cacheItem = getFromCache(api, fn, []);

                        /* Get the data from cache if possible and not specified otherwise */
                        if(skipCache || cacheItem == null) {
                            //Do server request
                            callService(api, fn,
                                TIME_DST_URL,
                                {
                                    method:         'get',
                                    requestHeaders: TIME_DST_HEADERS
                                },
                                myEventHandler,
                                false
                                );

                            return;
                        }

                        //Fill the DS with the cached info
                        fillDataSource(ds, cacheItem.data, cacheItem.totalItems, cacheItem.lastPageLoaded);
                    
                        //Pass the cached data object to the provided callback
                        launchCacheCallbacks(eventHandler, cacheItem.data);
                    
                        ds = null;
                    };

                    ds.addEventListener("accedo:datasource:load", loadData);
                
                    return ds;
                },

                /**
                 * Function called every X minutes (see inside) to synchronize the time with telstra's servers
                 * @public
                 */
                synchronizeTime: function() {
                    //skip getTimeAndDST's cache!
                    var ds = obj.time.getTimeAndDST(null, true);
                    ds.load();
                    ds = null;
            
                    //Synchronize the time with telstra's server every 20 minutes (interval to be initialized once only)
                    if (!intervalSynchronize) {
                        intervalSynchronize = setInterval(obj.time.synchronizeTime, 1200000);
                    }
                },
            
                /**
                 * Fix for some devices as they can't compute some string formats on new Date()
                 * @param {String} stringDate A UTC date in string format, this way: "2011-07-27T09:22Z" or "2011-07-27T09:22:37Z"
                 * @returns {Date}
                 * @public
                 */
                parseDate: function(stringDate) {
                    var timeData = stringDate.split("T"),
                    dateInfo = timeData[0].split("-"), // gives us ['2011', '07', '27']
                    timeInfo = timeData[1].split(":"); // gives us ['09', '22Z'] or ['09', '22', '37Z']

                    timeInfo[timeInfo.length-1] = timeInfo[timeInfo.length-1].substr(0,2);

                    //UTC is important!!
                    return new Date(Date.UTC(dateInfo[0], dateInfo[1]-1, dateInfo[2], timeInfo[0], timeInfo[1]));
                },

                /*
                 * Returns the current time in milliseconds
                 * @param {Function} onReady Callback which will be launched with the server time as argument
                 * @returns {Number} Time in milliseconds
                 * @private
                 * @example telstra.api.manager.time.getTimeNow(function(t){accedo.console.log(new Date(t))})
                 */
                getTimeNow: function(onReady) {
                    if (serverTime) {
                        onReady && onReady(serverTime);
                        return;
                    }

                    var ds = obj.time.getTimeAndDST({
                        onSuccess: function() {
                            onReady && onReady(serverTime); // serverTime is set automatically by the getTimeAndDST API
                        }
                    }, true); //skip getTimeAndDST's cache of course!

                    ds.load();
                },

                /*
                 * Returns the current time in milliseconds without ajax request and callback
                 * @returns {Number} Time in milliseconds
                 * @private
                 * @example telstra.api.manager.time.getServerTime()
                 */
                getServerTime : function(){
                    if (serverTime) {
                        return serverTime;
                    }else{
                        return false;
                    }
                },

                /*
                 * Returns the current date without ajax request and callback
                 * @returns {Number} From 0 to 6, representing the day of the week
                 * @private
                 * @example telstra.api.manager.time.getDate()
                 */
                getDate : function(){
                    var date = new Date(serverTime);

                    return date.getDay();
                },

                /**
                 * Get the postcode of the user
                 * @return {String} postcode
                 * @public
                 */
                getPostcode: function() {
                    return userPostcode;
                },

                /**
                 * Get the region of the user
                 * @return {Object} region
                 * @public
                 */
                getRegion: function() {
                    return userRegion;
                },
                
                /**
                 * Sets the region selected by the user.
                 * Also sets the time offset, in milliseconds, between the user's timezone and the GMT.
                 * If the region's offset is unknown assume he lives in the GMT+10 timezone.
                 * @param {Object} region
                 * @param {String} region.name
                 * @param {Number} region.offset
                 * @param {boolean} silent   If true, do not launch the time update notification event
                 * @public
                 */
                setRegion: function(region, silent) {
                    if (region) {
                        var previousTZ = userTimezoneOffset;
                        userRegion = region;
                
                        if (typeof region.offset != 'number'){
                            region.offset = 10; //10 hours - Sydney is usually GMT+10 (can be +11 though)
                        }
                        userTimezoneOffset = region.offset * 3600000;
                    
                        //If the TZ has changed, send the time update event
                        if (!silent && serverTime && userTimezoneOffset !== previousTZ) {
                            obj.dispatchEvent('telstra:time:update', serverTime);
                        }
                    }
                },
            
                /**
                 * Returns the offset, in milliseconds, between the user's timezone and the GMT.
                 * If the user's timezone is unknown assume he lives in the GMT+10 timezone.
                 * TZO = Time Zone Offset
                 * @returns {Number} Time offset in milliseconds
                 * @public
                 */
                getTZO: function() {
                    if (typeof userTimezoneOffset != 'number') {
                        return 10 * 3600000; //10 hours - Sydney is usually GMT+10
                    }
                    return userTimezoneOffset; //10 hours - Sydney is usually GMT+10
                },
        
                /**
                 * Is the user region's timezone offset known (true), or does the default apply (false) ?
                 * @returns {boolean}
                 * @public
                 */
                isSetTZO: function() {
                    return typeof userTimezoneOffset == 'number';
                }
            },
        
            /**
             *  meter and unmetering API
             */
            telstra_metering:{
                onNetFlag : null,
         
                check: function(eventHandler){
                    var self = this,
                    myEventHandler = getDefaultEventHandler(eventHandler),
                    defaultOnSuccess = myEventHandler.onSuccess,
                    defaultOnFailure = myEventHandler.onFailure;

                    if(self.onNetFlag != null){
                        defaultOnSuccess(self.onNetFlag);
                        return;
                    }
                    
                    var script = document.createElement('script');

                    script.type = 'text/javascript';
                    script.src = METERING_URL + "/RetrieveNetworkInfo?requestApplicationLabel=BPTV_WD&username=wdBPMovies&password=" + (obj.movies.isProduction? "sdjdfowh567MAOdG2" : "sdjdfsdf567MAOdG2") + "&callback=metering_callback";

                    window["metering_callback"] = function(resp) {
                        self.onNetFlag = resp.RetrieveNetworkInfo.OnNetFlag
                        accedo.console.log("onNetFlag: " + self.onNetFlag);

                        if (self.onNetFlag != "true" && self.onNetFlag !="false") {
                            accedo.console.log("telstra_metering failure");
                            defaultOnFailure();
                        }
                        else {
                            if(self.onNetFlag == "true"){
                                self.onNetFlag = true;
                                defaultOnSuccess(self.onNetFlag);
                            }
                            if(self.onNetFlag == "false"){
                                self.onNetFlag = false;
                                defaultOnFailure();
                            }
                        }

                        //remove the tag
                        document.getElementsByTagName('head')[0].removeChild(script);
                        script = null;
                        //remove the global function
                        delete window["metering_callback"];
                    }
            
                    document.getElementsByTagName('head')[0].appendChild(script);
                }
            },

            /*
             * rightNow API
             */
            rightNowAPI : {
                /**
                 * get the help text from server
                 * @param {Integer} code
                 * @param {Object} eventHandler (optional) An object containing onSuccess, onFailure, etc. functions
                 */
                helpRequest: function(code, eventHandler){

                    var postContent = 'xml_doc=<connector><function name="ans_get"><parameter name="args" type="pair"><pair name="id" type="integer">'+code+'</pair></parameter></function></connector>',
                    myEventHandler = getDefaultEventHandler(eventHandler),
                    defaultOnSuccess = myEventHandler.onSuccess;
                    
                    //Customise the default event handler for this response's specificities
                    myEventHandler.onSuccess = function(resp) {
                        //accedo.console.log("callHelp: onSuccess: " + resp);
                        var sourceText = resp.responseText;
                        var content = false;
                        
                        //that's bad for manipulating text..
                        try{
                            content = sourceText.split('<pair name="solution" type="string">')[1];
                            content = content.split('</pair>')[0];
                            content = content.replace(/&lt;/g,"<");
                            content = content.replace(/&gt;/g,">");
                            content = content.replace(/&amp;/g,"&");
                            content = content.replace(/&nbsp;/g," ");
                        }catch(e){
                            content = false;
                        }

                        defaultOnSuccess(content);
                    };
                
                    if(!telstra.sua.core.main.isOnline){
                        accedo.console.log("callHelp error: !isOnline");
                        loading.turnOffLoading();
                        return;
                    }

					var url = '';
					if (USE_PROXY){
						url = PROXY_URL + escape(RIGHTNOW_URL);
					}
					else{
						url = RIGHTNOW_URL;
					}
					accedo.utils.ajax.request(url,
                        accedo.utils.object.extend(myEventHandler, {
                            method:			'post',
                            postBody:		postContent,
                            encoding:		"UTF-8",
                            contentType:	"application/x-www-form-urlencoded",
                            useProxy:		USE_PROXY
                        })
                   );
                },
                
                callHelp : function(module, rightNowCode, title, callback, rightNowCodeArray){
                	accedo.console.log('callHelp for rightNowCode: ' + rightNowCode);
                    if (!title) {
                        title = 'Help';
                    }

                    var loading = telstra.sua.core.components.loadingOverlay;
                
                    loading.turnOnLoading();
            
                    if(!telstra.sua.core.main.isOnline){
                        accedo.console.log("callHelp error: is NOT Online");
                        loading.turnOffLoading();
                        telstra.sua.core.controllers.popupHelpController({
                            innerText : "[help text should come from server]<br/> rightNowCode : " + rightNowCode,
                            headerText : title,
                            callback : callback
                        });
                    }
                    else {
                        telstra.api.manager.rightNowAPI.helpRequest(rightNowCode,{
                            onSuccess: function(content){
                                loading.turnOffLoading();
                                if(content == false){
                                    accedo.console.log("callHelp error");
                                    // add a message for testing in case of server error
                                    telstra.sua.core.controllers.popupHelpController({
                                        innerText : "Sorry, there is a server error.<br/>Help text will come from server.<br/>Help code : " + rightNowCode,
                                        headerText : title,
                                        callback : callback
                                    });
                                }else{
                                    telstra.sua.core.controllers.popupHelpController({
                                        innerText : content,
                                        headerText : title,
                                        callback : callback
                                    });
                                }
                            },
                            onFailure: function(){
                                loading.turnOffLoading();
                                telstra.sua.core.error.show(telstra.sua.core.error.CODE.GLOBAL_API_FAILED);
                            }
                        });
                    }
                }
            },

            /*
             * deviceLogger API
             */
            deviceLogger : {

                version:"1.0",
                identityType:"Serial",
                manufacturer:"LG",
                firmware:"STATIC",

                /**
             * send the log to Telstra's server for error logging
             * @param {Object} logInfo - log info object containing info required to send the request
             * @param {Object} eventHandler (optional) An object containing onSuccess, onFailure, etc. functions
             * @example telstra.api.manager.deviceLogger.log({
                            frontendapp : "bigPondMovies",
                            component : "General",
                            logMessage : "Unify App API testing"
                        },{
                            onFailure: function(error){
                                outputConsole.setText('ERROR sending device logger');
                            },
                            onSuccess: function(data){
                                outputConsole.setText('device logger send success');
                            }
                        });
             */
                log : function(logInfo, eventHandler){

                    var isUndefined = accedo.utils.object.isUndefined;
                    if(isUndefined(logInfo)){
                        accedo.console.info('missing parameter');
                        return;
                    }

                    if( isUndefined(logInfo.component)
                        || isUndefined(logInfo.logMessage)){
                        accedo.console.info('missing parameter component or logMessage');
                        return;
                    }
                
                    if (isUndefined(logInfo.frontendapp)){
                        logInfo.frontendapp = "SUA";
                    }

                    if(isUndefined(logInfo.accountid) || logInfo.accountid == null
                        || isUndefined(logInfo.email) || logInfo.email == null){
                        logInfo.accountid = "notSignedIn";
                        logInfo.email = "notSignedIn";
                    }

                    if(isUndefined(logInfo.logLevel)
                        || (logInfo.logLevel != "warn" && logInfo.logLevel != "info" && logInfo.logLevel != "error") ){
                        logInfo.logLevel = "error";
                    }

                    /* log content format
                <log>
                    <manufacturer>DEVICE_MANUFACTURER_STR</manufacturer>
                    <firmware>DEVICE_FIRMWARE_STR</firmware>
                    <frontendapp>CLIENT_APP_VERSION</frontendapp>
                    <component>CLIENT_COMPONENT_STR</component>
                    <deviceTime>DEVICE_CONFIGURED_DATETIME</deviceTime>
                    <logLevel>CLIENT_LOG_LEVEL_STR</logLevel>
                    <logMessage>CLIENT_LOG_MESSAGE_STR</logMessage>
                </log>

                */

                    /* log sample
                    <log>
                        <manufacturer>Mozilla</manufacturer>
                        <firmware>STATIC</firmware>
                        <frontendapp>STATIC</frontendapp>
                        <component>STATIC:STATIC</component>
                        <deviceTime>STATIC</deviceTime>
                        <logLevel>error</logLevel>
                        <logMessage><![CDATA[STATIC]]></logMessage>
                    </log>

                */

                    //add in general info
                    logInfo.logMessage += "|userId:"+logInfo.accountid
                    +"|userEmail:"+logInfo.email
                    +"|serialNumber:"+accedo.device.manager.identification.getUniqueID() || "112233445566";
                    +"|MACAddress:"+accedo.device.manager.identification.getMac() || "112233445566";

                    var logContent = "<log>"
                    + "<manufacturer>"+ this.manufacturer +"</manufacturer>"
                    + "<model>"+ accedo.device.manager.identification.getModelName() +"</model>"
                    + "<firmware>"+ this.firmware +"</firmware>"
                    + "<frontendapp>"+ logInfo.frontendapp +"</frontendapp>"
                    + "<component>"+ logInfo.component +"</component>"
                    + "<deviceTime><![CDATA["+ (new Date()) +"]]></deviceTime>"
                    + "<logLevel>"+ logInfo.logLevel +"</logLevel>" //error, warn, info
                    + "<logMessage><![CDATA["+ logInfo.logMessage +"]]></logMessage>"
                    + "</log>";

                    //accedo.console.log(logContent);

                    var myEventHandler = getDefaultEventHandler(eventHandler),
                    defaultOnSuccess = myEventHandler.onSuccess;

                    //Customise the default event handler for this response's specificities
                    myEventHandler.onSuccess = function(resp) {

                        accedo.console.log("=============== deviceLogger success");
                        if(resp.status == 201){
                            defaultOnSuccess();
                        } else{
                            myEventHandler.onFailure();
                        }
                    };

                    if(!telstra.sua.core.main.isOnline){
                        return;
                    }

                    accedo.utils.ajax.request(DEVICELOGGER_URL,
                        accedo.utils.object.extend({
                            method:			'post',
                            postBody:		logContent,
                            contentType:	"application/xml",
                            requestHeaders:	{
                                "x-tm-device":      X_TM_DEVICE,
                                "x-tm-authtoken":   "3f211c8faf7607495e849f96cce97b58",  //applicable for BPMV
                                "x-tm-identity":    accedo.device.manager.identification.getUniqueID()
                            },
                            useProxy:		USE_PROXY
                        },
                        myEventHandler)
                        );
                }
            },

            /**
             * @namespace Maintenance Module Manager API
             */
            maintenance: {
                /**
             * Checks if a specific module is up.
             * As per requirement, an error of status code 500 or no response should be threated as OK.
             * @param {String} module Name of the module. Currently supported values are:
                'core', 'ga', 'movies', 'live'
             * @eventHandler {Object} Object for handling standard events.
             * @memberOf telstra.api.manager.maintentance#
             */
                check : function(module, eventHandler) {
                    var api = 'maintenance',
                    fn = 'check',
                
                    myEventHandler = getDefaultEventHandler(eventHandler),
                    defaultOnSuccess = myEventHandler.onSuccess,
                    
                    cacheItem;
                
                    //No MMM for this module - launch the callback with positive response
                    if (!(module in MAINTENANCE_URLS)) {
                        var data = accedo.utils.object.clone(maintenance_default_response_mini, true);
                        launchCacheCallbacks(eventHandler, data);
                        return;
                    }
                
                    cacheItem = getFromCache(api, fn, [module]);
                
                    if(cacheItem == null) {
                    
                        //Customise the default event handler for this response's specificities
                        myEventHandler.onSuccess = function(data) {
                            if (!data || !('current_status' in data)) {
                                //Simulated response on ajax call failure (provided by Telstra)
                                data = accedo.utils.object.clone(module === 'core' ? maintenance_default_response : maintenance_default_response_mini, true);
                            }
                        
                            //Cached for 10 minutes (but 1 minute only when the system was not ok)
                            var cache_ttl = data.current_status === 0 ? 10 : 1;
                        
                            if (module === 'core' && data.config && data.config.services) {
                                updateServiceURLs(data.config.services);
                            }
                        
                            //Delete what's not needed any more
                            delete data.config;
                        
                            //This API method is cacheable, let's save the response.
                            addToCache(api, fn, [module], data, null, null, cache_ttl);
                        
                            defaultOnSuccess(data);
                        };
                    
                        myEventHandler.onFailure = function() {
                            accedo.console.log("MMM API ("+module+"): Maintenance check failed, force run");
                        
                            //Simulated response on ajax call failure (provided by Telstra)
                            var data = accedo.utils.object.clone(module === 'core' ? maintenance_default_response : maintenance_default_response_mini, true);
                        
                            if (module === 'core' && data.config && data.config.services) {
                                updateServiceURLs(data.config.services);
                            }
                        
                            //Delete what's not needed any more
                            delete data.config;
                        
                            //This API method is cacheable, let's save the response.
                            //Cached only for 1 minute as the system didn't reply
                            addToCache(api, fn, [module], data, null, null, 1);
                        
                            //Launch onSuccess with the simulated response
                            defaultOnSuccess(data);
                        };
                    
                        myEventHandler.onAbort = myEventHandler.onFailure;
                    
                        //Do server request
                        callService(api, fn, MAINTENANCE_URLS[module], {}, myEventHandler, false);
                        setTimeout(function() {
                    
                            }, 2000);

                        return;
                    }
                
                    //Pass the cached data object to the provided callback
                    launchCacheCallbacks(eventHandler, cacheItem.data);
                }
            },
                
            /**
             * Fn called automatically every X minute to purge the expired data
             * @public
             */
            autoPurgeCache: function() {
                var count = 0;
                if (!serverTime) {
                    return;
                }
                for (var api in cache) {
                    for (var fn in cache[api]) {
                        accedo.utils.array.each(cache[api][fn], function(item, index) {
                            if (serverTime > item._EXP_TIMESTAMP) {
                                count++;
                                accedo.utils.array.remove(cache[api][fn], index--);
                            }
                        });
                    }
                }
                accedo.console.info('Deleted expired data in the cache: ' + count);
            },
        
            /**
             * Data persistency: saving and loading selected API data on exiting / loading the Unified App
             */
            persistence: {
                save: function() {
                    try {
                        var store = accedo.device.manager.system.storage();
                        if(store){
                            store.initialize("API_data", "WD_BPMV_API.txt");

                            try {
                                userRegion && store.set("REGION", userRegion);
                            } catch (e) {
                                accedo.console.info('Error setting REGION in persistence.save: ' + e);
                            }
                        
                            try {
                                userPostcode && store.set("POSTCODE", userPostcode);
                            } catch (e) {
                                accedo.console.info('Error setting POSTCODE in persistence.save: ' + e);
                            }
                        
                            store.save();
                        } else {
                            accedo.console.info('no store');
                        }
                    } catch (e) {
                        accedo.console.info('Error in persistence.save: ' + e);
                    }
                },
                        
                load: function() {
                    try {
                        //To create a storage handler
                        var store = accedo.device.manager.system.storage();
                        if (store) {
                            store.initialize("API_data", "WD_BPMV_API.txt");

                            //load the file
                            var hasFile = store.load(),
                            data;

                            if(hasFile) {
                                try {
                                    data = store.get("REGION");
                                    if (data && data.name && data.offset) {
                                        obj.time.setRegion(data);
                                    }
                                } catch (e) {
                                    accedo.console.info('Error getting REGION in persistence.load: ' + e);
                                }
                            
                                try {
                                    data = store.get("POSTCODE");
                                    if (data) {
                                        userPostcode = data;
                                        obj.weather.getInfo(null, {
                                            onFailure:function(){
                                            //ignore the errors as weather is nothing critical
                                            }
                                        });
                                    }
                                } catch (e) {
                                    accedo.console.info('Error getting POSTCODE in persistence.load: ' + e);
                                }
                           
                                data = null;
                            } else {
                                accedo.console.info('No previous API data found in persistency layer');
                            }
                        }
                    } catch (e) {
                        accedo.console.info('Error in persistence.load: ' + e);
                    }
                }
            },
        
            /**
             * Image resizer
             * @param {String} image url
             * @param {Number} width (in pixels)
             * @param {Number} height (in pixels)
             * @returns {String} the URL to use for the resized image
             * @public
             */
            resizer : function(url, width, height, local) {
                if (!url) {
                    return 'images/core/empty.png';
                }
                /**
                 * Remote Image resizer
                 */
                if (!local){
                    return RESIZER_URL + RESIZER_REMOTE_PATH + width + 'x' + height + '/' + url.replace(/^http(s?):\/\//, '');
                }
        
                /**
                 * Handling of local image resizer
                 * (As far as I know it's only used for movies)
                 */
                var localUrl = url.replace(/^http(s?):\/\/[^/]+\/(newdrm3|drm5)\//, '');
                /**
                 * Warning! This is a temporary measure to override the gobal resizer url as it's not working for different environment
                 */
        
                return MOVIES_RESIZER_URL + RESIZER_LOCAL_PATH + width + 'x' + height + '/' + localUrl;
            },
        
            /**
             * Image resizer, with width only (keeping similar proportions)
             */
            resizerW : function(url, width) {
                if (!url) {
                    return 'images/core/empty.png';
                }
                return RESIZER_URL + RESIZER_REMOTE_PATH + width + 'w/' + url.replace(/^http(s?):\/\//, '');
            },
        
            /**
             * Image resizer, with height only (keeping similar proportions)
             */
            resizerH : function(url, height) {
                if (!url) {
                    return 'images/core/empty.png';
                }
                return RESIZER_URL + RESIZER_REMOTE_PATH + height + 'h/' + url.replace(/^http(s?):\/\//, '');
            }
        });
    
        return obj;
    });