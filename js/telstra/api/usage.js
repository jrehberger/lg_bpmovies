/**
 * @fileOverview omniture logging for all the modules
 *  
 * @example 
 * @author <a href="mailto:henry.leung@accedobroadband.com">Henry</a>
 */
      
accedo.define('telstra.api.usage', ['accedo.device.manager'], function() {
          
    return {

        data: null,// gets filled in this.once
        startTime: null,
        eVar: new Array(49),
        prop: new Array(49),

        init: function () {
            this.eVar[0] = "&v1=panels";
            this.eVar[6] = "&v7=western digital";
            this.eVar[7] = "&v8=stand alone";
            this.eVar[22] = "&v23=" + accedo.device.manager.identification.getFirmware();
            this.eVar[23] = "&v24=" + escape("D=vid");
            this.eVar[25] = "&v26="+ (telstra.sua.core.main.onOffNet ? "unmetered" : "unmetered");
            this.eVar[40] = "&v41=" + telstra.api.manager.time.getDate();
            this.eVar[42] = "&v43=nsw";

            this.prop[0] = "&c1=" + escape("D=v1");
            this.prop[6] = "&c7=" + escape("D=v7");
            this.prop[7] = "&c8=" + escape("D=v8");
            this.prop[22] = "&c23=" + escape("D=v23");
            this.prop[24] = "&c24=" + escape("D=v24");
            this.prop[26] = "&c26=" + escape("D=v26");
            this.prop[41] = "&c41=" + escape("D=v41");
            this.prop[43] = "&c43=" + escape("D=v43");
            
            this.setTime();
            telstra.api.manager.removeEventListener('telstra:time:update', telstra.api.usage.init);
        },
        
        log: function (moduleId, pageId, params) {
            var  products = "", events = "", message = "", eVar = new Array(49), prop = new Array(49);
            
            switch (moduleId) {
                case "overlay":
                    message += "&v10=" + params.logMessage + "&c10=" + escape("D=v10") + "&pe=o&pev2=" + escape("D=v10");
                    break;

                case "error":
                    message += "&v28=" + params.description.toLowerCase() + "&c28=" + escape("D=v28") + "&events=event8&pe=o&pev2=" + escape("D=v28");
                    break;

                case 'bpvod':
                    if(params && params.logMessage) {
                        params.logMessage = params.logMessage.replace(/&amp;/g,"&").replace(/\,/g,"");

                        if(params.products && params.products != null){
                            products = params.products.replace(/&amp;/g,"&").replace(/\,/g,"");
                        }

                        if(params.events && params.events != null){
                            events = params.events.replace(/&amp;/g,"&").replace(/\,/g,"");
                        }

                        // Split out pageHierarchy into the appropriate props and vars
                        var pageHierarchy = params.logMessage.split(':');

                        for (var i = 0; i < pageHierarchy.length; i++) {
                            switch (i) {
                                case 0:
                                    eVar[1] = "&v2=" + pageHierarchy[i];
                                    prop[1] = "&c2=" + escape("D=v2");
                                    break;
                                case 1:
                                    eVar[2] = "&v3=" + pageHierarchy[i];
                                    prop[2] = "&c3=" + escape("D=v3");
                                    break;
                                case 2:
                                    eVar[3] = "&v4=" + pageHierarchy[i];
                                    prop[3] = "&c4=" + escape("D=v4");
                                    break;
                                case 3:
                                    eVar[4] = "&v5=" + pageHierarchy[i];
                                    prop[4] = "&c5=" + escape("D=v5");
                                    break;
                            }
                        }
                    }

                    //extra variables
                    if (pageId == "browse_catalogue") {
                        eVar[11] = "&v12=" + pageHierarchy[3];
                        prop[11] = "&c12=" + escape("D=v12");

                        eVar[12] = "&v13=" + pageHierarchy[2];
                        prop[12] = "&c13=" + escape("D=v13");

                        eVar[14] = "&v15=" + pageHierarchy[1].split(" ")[0];
                        prop[14] = "&c15=" + escape("D=v15");
                    }

                    if (pageId == "search_catalogue_success") {
                        eVar[14] = "&v15=" + pageHierarchy[1];
                        prop[14] = "&c15=" + escape("D=v15");

                        eVar[15] = "&v16=men";
                        prop[15] = "&c16=" + escape("D=v16");
                    }

                    if (pageId == "search_catalogue_success_no_result") {
                        eVar[14] = "&v15=" + pageHierarchy[1];
                        prop[14] = "&c15=" + escape("D=v15");

                        eVar[15] = "&v16=null:men";
                        prop[15] = "&c16=" + escape("D=v16");
                    }

                    if (pageId == "movies_synopsis" || pageId == "tv_shows_synopsis" ) {
                        eVar[12] = (pageId == "movies_synopsis" ? "&v13=movies" : "&v13=tv shows");
                        prop[12] = "&c13=" + escape("D=v13");

                        eVar[14] = "&v15=search";
                        prop[14] = "&c15=" + escape("D=v15");

                        eVar[48] = "&v49=" + escape("D=products");
                        prop[48] = "&c49=" + escape("D=products");
                    }

                    if (pageId == "movies_related" || pageId == "tv_shows_related" ) {
                        eVar[10] = "&v11=" + params.actor;
                        prop[10] = "&c11=" + escape("D=v11");

                        eVar[14] = "&v15=related";
                        prop[14] = "&c15=" + escape("D=v15");
                    }
                    
                    if (pageId == "purchase_flow_complete") {
                        eVar[13] = "&v14=" + params.paymentType;
                        prop[13] = "&c14=" + escape("D=v14");
                    }

                    message += "&gn=" + params.logMessage.toLowerCase() + eVar.join("").toLowerCase().replace("&v49=d", "&v49=D") + products.toLowerCase() + events + prop.join("");
                    break;
                    
                case "playback":
                    events = params.events.replace(/&amp;/g,"&").replace(/\,/g,"");
                    
                    if (pageId == "start") {
                        eVar[1] = "&v2=bigpond movies";
                        prop[1] = "&c2=" + escape("D=v2");
                        
                        eVar[36] = "&v37=vod";
                        prop[36] = "&c37=" + escape("D=v37");
                        
                        eVar[37] = "&v38=full screen";
                        prop[37] = "&c38=" + escape("D=v38");
                        
                        eVar[48] = "&v49=" + params.contentID;
                        prop[48] = "&c49=" + escape("D=v49");

                        message += "&gn=" + params.logMessage.toLowerCase() + eVar.join("").toLowerCase() + events + prop.join("");
                    }

                    if (pageId == "stop") {
                        message += events + "&pe=o&pev2=asset complete";
                    }
                    break;
                    
                default:
                    return;
            }

            this.beacon(message);
        },

        beacon: function (message) {
            this.eVar[24] = "&v25=" + (telstra.sua.module.bpvod.session.isSignedIn ? "logged in" : "logged out"),
            this.eVar[39] = "&v40=" + this.getCurrentTime();
            this.prop[39] = "&c40=" + escape("D=v40");
            
            this.data = [
            accedo.device.manager.identification.getUniqueID(),
            this.eVar.join(""),
            this.prop.join(""),
            ].join("");
            
            var host = ["/b/ss/", MMM_static.config.omniture_suite, "/0"].join(""),
            segments = [this.data, message].join(""),
            url = ["http://", MMM_static.config.omniture_server, host, "?AQB=1&vid=", encodeURI(segments).replace(/252Cevent4725/g, "%2Cevent47%"), "&AQE=1"].join("");

            //in model we won't call omniture as it breaks due to cross domain
            if (MMM_static.config.omniture_server.indexOf("scdebugger.com") !== -1)
            	return;
            
            accedo.console.log("Omniture URL -> " + url);
            accedo.utils.ajax.request(url,
                accedo.utils.object.extend({}, {
                    method:    'GET'
                }));
        },

        setTime: function() {
            this.startTime = new Date().getTime();
        },

        getCurrentTime: function() {
            var currentTime = telstra.api.manager.time.getServerTime();
            var date = new Date(currentTime);
            
            return this.formatTime(date);
        },

        formatTime: function(date) {
            var hr = date.getHours();
            var min = date.getMinutes();

            if (hr < 10) {
                hr = "0" + hr;
            }

            if (min < 10) {
                min = "0" + min;
            }

            return hr + ":" + min;
        }
    };
});