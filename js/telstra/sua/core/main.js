//Onload bootstrap
onload = function() {

    //Load the proper device configuration before anything else
    accedo.define(
        null,
        ['accedo.device.loader'],
        function(){
        
            //Now that the page is loaded, let's bootstrap everything!
            accedo.define("telstra.sua.core.main",
                [
                'accedo.app',
                'telstra.api.manager',
                'telstra.api.usage',
                'telstra.sua.core.error',
                'telstra.sua.core.config',
                'telstra.sua.core.views.startView',
                'telstra.sua.core.components.imagePreloader',
                'telstra.sua.core.components.navigationBar',
                'telstra.sua.core.components.infoMessageBar',
                'accedo.utils.object',
                'accedo.ui.popupOk',
                'accedo.ui.popupYesNo',
                'accedo.focus.manager',
                'accedo.focus.managerMouse',
                'telstra.sua.module.bpvod.utils',
                'telstra.sua.core.controllers.startController',
                'telstra.sua.core.controllers.popupOkInfoController'
                ],
                function() {
                    return (function(){
                        return {

                            isOnline : true,
                            connectionStatusCounter : null,
                            utils : null,

                            /**
                     *  init the apps , Define global function and property that needed
                     */
                            init: function () {
                                var self = this;

                                //Load some API data from the persistency layer
                                //telstra.api.manager.persistence.load();
                                //Preload some images
                                telstra.sua.core.components.imagePreloader.preload(telstra.sua.core.config.imageList);
                                //Launch the time server sync
                                telstra.api.manager.time.synchronizeTime();
                                telstra.api.manager.addEventListener('telstra:time:update', accedo.utils.fn.bind(telstra.api.usage.init, telstra.api.usage));
                                utils  = telstra.sua.module.bpvod.utils;
                                //Init some parameters
                                this.setParameters();
                                //Check the metering status
                                this.meteringCheck();

                                //Init XDK app
                                accedo.app.setApp({
                                    controllers: {
                                        'startScreen': {
                                            controller:     telstra.sua.core.controllers.startController,
                                            title:          'Samsung SUA App main View',
                                            main:           true
                                        }
                                    }
                                });
                            
                                this.setAppDefaultKeys();
                                this.connectionStatusCheck();
                            
                                //Launch the XDK app
                                accedo.app.main();

                                this.mainNavBar = telstra.sua.core.components.navigationBar();
                                this.mainNavBar.init();

                                telstra.sua.core.components.infoMessageBar.init();
                                telstra.sua.core.error.init();

                                //initialize autoPurgeData calls - every 3 minutes
                                //window.setInterval(telstra.api.manager.autoPurgeCache, 3 * 60000);

                                return this;
                            },
                        
                            /**
                             *  init the parameters that can share between all the modules
                             */
                            setParameters : function () {
                                this.onOffNet = -1;   //  true or false or -2(fail to get from api) or -1 (havn't call api yet)
                                this.keyboardShown = false;
                            },
                       
                            /**
                             *  De-init the program during remove, attached to window.onUnload during init
                             */
                            deinit:function () {
                                accedo.console.log("Core apps deinit");
                                var currentModule = telstra.sua.core.modulemanager.getCurrentModuleId(),
                                main = telstra.sua.module[currentModule].main;
                                
                                if (main && accedo.utils.object.isFunction(main.deinit)) {
                                    try {
                                        main.deinit();
                                    } catch(e) {
                                        accedo.console.log("Warning: " + e);
                                    }
                                }
                                accedo.console.log("leaving the core apps");
                            },
                        
                            /**
                             *  register key event for all the pages in this module
                             */
                            setAppDefaultKeys: function () {
                                accedo.device.manager.setAppDefaultKeys({
									"red": function() {
										var controller = accedo.app.getCurrentController().get("coreSubController");
                                        controller = controller ? controller.getInstance() : false;
                                        controller && controller.onKeyRed && controller.onKeyRed();
									},
									"green": function() {
										//apply FF functionality to the green button -> rent movies
										var controller = accedo.app.getCurrentController().get("coreSubController");
                                        controller = controller ? controller.getInstance() : false;
                                        controller && controller.onKeyGreen && controller.onKeyGreen();
									},
									"yellow": function() {
										//apply FF functionality to the green button -> rent movies
										var controller = accedo.app.getCurrentController().get("coreSubController");
                                        controller = controller ? controller.getInstance() : false;
                                        controller && controller.onKeyYellow && controller.onKeyYellow();
									},
									"blue": function() {
                                        var controller = accedo.app.getCurrentController().get("coreSubController");
                                        controller = controller ? controller.getInstance() : false;
                                        
                                        if (controller && controller.onKeyBlue){
                                        	accedo.console.log('onKeyBlue defined in controller');
                                        	controller.onKeyBlue();
                                        } else {
                                        	//Movies Home as default action
                                        	accedo.console.log('onKeyBlue use default - backToMovieHome');
                                        	utils.backToMovieHome(controller);
                                        }
									},
                                    "play": function() {
                                        var controller = accedo.app.getCurrentController().get("coreSubController");
                                        controller = controller ? controller.getInstance() : false;
                                        controller && controller.onKeyPlay && controller.onKeyPlay();
                                        telstra.sua.core.main.clearFirebugPanel();
                                    },
									"pause": function() {
                                        var controller = accedo.app.getCurrentController().get("coreSubController");
                                        controller = controller ? controller.getInstance() : false;
                                        controller && controller.onKeyPause && controller.onKeyPause();
                                        telstra.sua.core.main.toggleFirebugPanel();
                                    },
                                    "stop": function() {
                                        var controller = accedo.app.getCurrentController().get("coreSubController");
                                        controller = controller ? controller.getInstance() : false;
                                        controller && controller.onKeyStop && controller.onKeyStop();
                                    },
                                    "ff": function() {
                                        var controller = accedo.app.getCurrentController().get("coreSubController");
                                        controller = controller ? controller.getInstance() : false;
                                        controller && controller.onKeyFF && controller.onKeyFF();
                                    },
                                    "rw": function() {
                                        var controller = accedo.app.getCurrentController().get("coreSubController");
                                        controller = controller ? controller.getInstance() : false;
                                        controller && controller.onKeyRW && controller.onKeyRW();
                                    },
                                    "back": function() {
                                        var cc = accedo.app.getCurrentController();
                                        if(cc.isPopup){
                                            cc.onKeyBack && cc.onKeyBack();
                                            return;
                                        }

                                        var controller = cc.get("coreSubController");
                                        controller = controller ? controller.getInstance() : false;
                                        controller && controller.onKeyBack && controller.onKeyBack();
                                    },
                                    "home" : function() {
                                    	var controller = accedo.app.getCurrentController().get("coreSubController");
                                        controller = controller ? controller.getInstance() : false;
                                        if (controller && controller.onKeyHome){
                                        	controller.onKeyHome();
                                        } else {
                                        	//Call Device Home as default action
                                        	telstra.sua.core.main.moveBackToMyApps();
                                        }
                                    },
                                    "exit" : function() {
                                        var cc = accedo.app.getCurrentController();
                                        
                                        if(cc.isPopup){
                                            var controller = cc.backgroundController.get("coreSubController");
                                        }else{
                                            var controller = cc.get("coreSubController");
                                        }
                               
                                        controller = controller ? controller.getInstance() : false;
                                        if (controller) {
                                            var exitFunction = function(onCloseCallback,onConfirmCallback){
                                                if(typeof(telstra.sua.core.main.onExitPopup) === 'undefined' || telstra.sua.core.main.onExitPopup === false){
                                                    telstra.sua.core.main.onExitPopup = true;
                                                    var popupYesNo_exit = accedo.ui.popupYesNo({
                                                        popup_css: 'bpmvPopup exitPopup',
                                                        headline_css: 'header',
                                                        headline_txt: 'Exit',
                                                        msg_css: 'exitPopup_msg',
                                                        msg_txt: 'Are you sure you want to exit the application?',
                                                        yesBtn_css: 'button',
                                                        yesBtn_txt: 'Yes',
                                                        noBtn_css: 'button',
                                                        noBtn_txt: 'No',
                                                        root_css: 'exitBg'
                                                    });
                                                    telstra.api.manager.dispatchEvent('accedo:appExit:onPopup');
                                                    popupYesNo_exit.addEventListener('accedo:popupYesNo:onConfirm',function(){
                                                        telstra.api.manager.dispatchEvent('accedo:appExit:onConfirm');
														
                                                        if(onConfirmCallback){
                                                            onConfirmCallback();
                                                        }
                                        
                                                        telstra.sua.core.main.exitUnifiedApp();
                                                    });
                                                    popupYesNo_exit.addEventListener('accedo:popupYesNo:onClose',function(){
                                                        telstra.sua.core.main.onExitPopup = false;
                                                        telstra.api.manager.dispatchEvent('accedo:appExit:onClose');

                                                        if(onCloseCallback){
                                                            onCloseCallback();
                                                        }
                                                    });
                                                    
                                                    popupYesNo_exit.onKeyBack = popupYesNo_exit.exitPopup;
                                                }
                                            };
                                        
                                            if (accedo.utils.object.isFunction(controller.onExit)){
                                                controller.onExit(exitFunction);
                                            } else {
                                                exitFunction();
                                            }
                                        }
                                    }
                                });
                            
                                accedo.device.manager.resetKeyEvent();
                            },
                        
                            /**
                             * function to handle the exit and back event
                             */
                            exitUnifiedApp:function(){
                                //Save some API data on the device's persistence layer
                                //telstra.api.manager.persistence.save();
                                //accedo.device.manager.system.exit();
                                window.NetCastExit();
                            },
                            
                            moveBackToMyApps:function(){
                                //Save some API data on the device's persistence layer
                                //telstra.api.manager.persistence.save();
                                //accedo.device.manager.system.exit();
                                window.NetCastBack();
                            },
                        
                            /**
                             *   perform meter and unmetering check
                             *   call the metering api
                             */
                            meteringCheck: function(callback) {
                                telstra.api.manager.telstra_metering.check({
                                    onSuccess: function(result) {
                                        accedo.console.log("unmetered");
                                        telstra.sua.core.main.onOffNet = result;
                                        callback && callback(telstra.sua.core.main.onOffNet);
                                    },
                                    onFailure: function() {
                                        accedo.console.log("metered");
                                        telstra.sua.core.main.onOffNet = false; //-2; //or other response?
                                        callback && callback(telstra.sua.core.main.onOffNet);
                                    }
                                });
                            },

                            connectionStatusCheck : function(){
                                var self = this;

                                if( this.connectionStatusCounter == null){
                                    this.connectionStatusCounter = setInterval(function(){
                                        var online = accedo.device.manager.identification.checkConnection();
                                        if(!online && self.isOnline){
                                            self.isOnline = false;
                                            clearInterval(self.connectionStatusCounter);
                                            // it's not working on LG TVs - when network disconnect, we got TV's message
                                            telstra.sua.core.error.show(telstra.sua.core.error.CODE.GLOBAL_NETWORK_DISCONNECT);
                                        }
                                    },5000);
                                }
                            },
                            
                            /**
                             * First time launch? Returns true or false. Uses _isFirstTimeLaunch as a cached result
                             * @returns {boolean}
                             * @public
                             */
                            isFirstTimeLaunch: function() {
                                //Reuse the previous value if this function was already called
                                if ('_isFirstTimeLaunch' in this) {
                                    return this._isFirstTimeLaunch;
                                }
                                
                                //Otherwise, find out the result, store it and return it
                                var store = accedo.device.manager.system.storage();
                                if(store) {
                                    store.initialize("FirstTimeLaunch", "AccedoXDK_1stTime");

                                    var hasFile = store.load();

                                    this._isFirstTimeLaunch = !hasFile;
                                    accedo.console.info('First time launch? ' + !hasFile);
                                    
                                    if (!hasFile) {
                                        store.save();
                                    }
                                    
                                    return this._isFirstTimeLaunch;
                                } else {
                                    //No storage - return false by default
                                    this._isFirstTimeLaunch = false;
                                    return false;
                                }
                            },
                            
                            /**
                             * Shows some info on a popup: version number, firmware used, etc
                             * @public
                             */
                            showInfo: function() {
                                var id = accedo.device.manager.identification,
                                info = (('<%{ENV_NAME}%>' === 'prod') ? "Production " : "Development ") + "Version: " + telstra.sua.core.config.version;
                                info += "<br/><br/> Model name: " + accedo.device.manager.identification.getModelName();
                                info += "<br/> Firmware: " + id.getFirmware();
                                info += "<br/> MAC: " + id.getMac();
                                info += "<br/> DUID: " + id.getUniqueID();
                                info += "<br/> WidevineESN: " + id.getWidevineESN();
                                
                                //Redefine this function (so we don't have to get all the info again) - 'lazy function definition'
                                telstra.sua.core.main.showInfo = function() {
                                    telstra.sua.core.controllers.popupOkInfoController({
                                        headerText: "Application Info",
                                        innerText:  info
                                    });
                                };
                                //Launch it (for the first call)
                                telstra.sua.core.main.showInfo();
                            },
                            
                            toggleFirebugPanel: function(){
                            	if ('<%{ENV_NAME}%>' !== 'prod'){
                                	if (firebug && firebug.el && firebug.el.main && firebug.el.main.environment){
										if (firebug.el.main.environment.getStyle("display") == "none"){
											firebug.win.show();
										}
										else{
											firebug.win.hide();	
										}
									}
                                }
                            },
                            
                            clearFirebugPanel: function(){
                            	if ('<%{ENV_NAME}%>' !== 'prod'){
                                	if (firebug && firebug.d && firebug.d.console && firebug.d.console.clear){
										firebug.d.console.clear()
									}
                                }
                            }
                        };
                    })().init();
                });
        });
};
