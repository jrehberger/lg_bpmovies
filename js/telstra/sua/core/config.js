/**
 * @fileOverview Config object for the app and modules
 * @returns {Object} the config object
 * @author <a href="mailto:gregory.desfour@accedobroadband.com">Gregory Desfour</a>
 */
accedo.define('telstra.sua.core.config', ['telstra.sua.module.bpvod.controllers.mainController'], function() {

    return {

        version : '<%{VERSION}%>',
        
        homePage: telstra.sua.module.bpvod.controllers.mainController,
        
        imageList:[
        //Popup
        "images/core/trans_bkg.png",
        "images/core/exit_bg.png",
        "images/module/bpvod/popup_option_small.png",
        "images/module/bpvod/popup_option_middle.png",
        "images/module/bpvod/popup_option_big.png",
        "images/module/bpvod/pages/SS-ACC-1_0_1_3_7_MyAccountSignOutOverlay_Delete/SignOutOverlay_bg.png",
            
        //Message strap
        "images/core/info/msgBar_blue.png",
        "images/core/info/msgBar_green.png",
        "images/core/info/msgBar_green_big.png",
        "images/core/info/msgBar_red.png",

        //Loading icon
        "images/core/loadingOverlay/loadingOverlayBg.png",
        "images/core/loadingOverlay/loadingOverlay_01.png",
        "images/core/loadingOverlay/loadingOverlay_02.png",
        "images/core/loadingOverlay/loadingOverlay_03.png",
        "images/core/loadingOverlay/loadingOverlay_04.png",
        "images/core/loadingOverlay/loadingOverlay_05.png",
        "images/core/loadingOverlay/loadingOverlay_06.png",
        "images/core/loadingOverlay/loadingOverlay_07.png",
        "images/core/loadingOverlay/loadingOverlay_08.png",

        //Background
        "images/module/bpvod/background3.jpg",
        "images/core/bg.jpg"
        ]
    };
});