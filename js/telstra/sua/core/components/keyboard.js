/*
 * Telstra Unify Apps inputField, it's base on the inputField2 in BPMovie 1.1
 *
 * This inputField have 2 mode, fullscreen mode or NumPad mode 
 * 
 * remark:  all parameters are optional
 * 
 * @example                    {
                                    type:           telstra.sua.core.components.keyboard,
                                    id:             'searchInputField2',
                                    css:            'searchInput2',
                                    nextUp:         'searchInputField',
                                    nextDown:       'searchButton',
                                    
                                    params:{
                                             isPassword : true,
                                            width:400,
                                            title:"Password",
                                            defaultStr: "aaaaaaa"
                                            numericalInput:true,
                                            defaultCapitalInput: false,
                                            keyboardPosition: {x:250, y:405},
                                            maxLengthCallback: function (){}
                                            zeroLengthCallback:function (){}
                                            maxlength: 40,
                                            isPopup: false
                                    } 
                                }, 
 
                  // get the result      
 *                   var searchInputField = self.get('searchInputField');
 *                   var searchInput = searchInputField.getCleanedResultStr()
 *                   
 *               // active or deactive the keyboard
 *                        var searchInputField = self.get('searchInputField');   
 *                        searchInputField.activate();
 *                        searchInputField.deactivate();  

Creator: Henry
Date: 2011-08-01

Note:


*/


/**
 *  inputField with Full Screen keyboard
 *  @ param (string) classes
 *  @ param (object) params : {     // all params are optional
 *                     isPopup:(boolean),      // whether the keyboard is on popup page or not    
 *                     isPassword : (boolean), // mask the output with * or not
 *                     width (number),         // auto adust the length of the inputfield in the page
                       title (string),                  // title in fullscreen Keyboard
                       defaultStr (string),             //pre-fill string
                       numericalInput (boolean),         // "true" for NumPad mode, "false" for fullscreen mode 
                       defaultCapitalInput (boolean)     //   set the default to capital letter in fullscreen mode   
                                                                  or maxlength is reach in num pad mode
                       zeroLengthCallback (function),           // callback function when delete is pressed in NumPad mode with no string exist
                       maxlength (number),              // max length of word in input field,
                       keyboardPosition: {x:(number), y:(number)} // position of the numPad keyboard
                       resetOnOverflow (boolean)        //if true, reset the input when a key was pressed and the max length was already reached
 *                  }
 */

accedo.define("telstra.sua.core.components.keyboard",["accedo.utils.dom","accedo.ui.label","accedo.focus.managerMouse"],
    function(){
    
    
        /**
     *  Create the class body of the telstra.sua.core.components.keyboard
     *  @param (object) opts 
     */
        var _class = function(opts) {

            opts.root = accedo.utils.dom.element('div');
            opts.root.addClass('accedo-ui-keyboard');
            opts.focusable  = true;
            var params = opts.params || {};
            
            var keyboard = accedo.utils.object.extend(accedo.ui.component(opts), {
                initialized : false,

                /**
                 *  controller the flow from beginning
                 */
                initialize : function()
                {   
                    var self = this;

                    this.setParameter();
                    this.createPageInputField();

                    if (params.defaultStr) {
                        this.setString(params.defaultStr);
                    }

                    //Listen for browsers click event, for keyboard
                    this.addEventListener('click',function(){
                        self.onClick();
                    });
                },
                
                /**
                 * Initial the value of parameters
                 */
                setParameter : function ()
                {
                    this.keyboardBase = _class.BASE;
                    
                    this.keyBoardHidden = true;
                    
                    this.cursor = "&nbsp;";
                    this.cursor_copy = "<span class='cursor_copy'>|</span>";
                    this.cursPos = 0;
                    
                    this.data = _class.keyStrokeData;
                    
                    this.keyBoardLayout = null;
                    this.keyBoardLayoutArray = null;
                  
                    this.screenSize = null;
                    this.currentMode = null;

                    this.currentFocus = [0,0];              //position of the currentFocus
                    this.currentMouseFocus = [-1,-1];       //position of the currentMouseFocus
                    
                    this.oldStr = "";
                    this.resultStr = "";
                    
                    this.isPassword =  params.isPassword || false;
                    this.hidePWtimeout = null;
                    
                    params = params||{};
                            
                    if (!accedo.utils.object.isUndefined(params.maxlength) && params.maxlength!=null)
                        this.maxlength=params.maxlength;
                    else this.maxlength =_class.MAXLENGTH;
                    
                    this.isPopup = params.isPopup || false;
                    this.numericalInput = params.numericalInput || false;
                    this.defaultCapitalInput = params.defaultCapitalInput || false;
                    this.pageInputFieldwidth = params.width || _class.DEFAULTWIDTH;
                    this.outputTitle = params.title || "Input";
                    this.maxLengthCallback = params.maxLengthCallback || null;
                    this.zeroLengthCallback = params.zeroLengthCallback || null;
                    this.keyBoardPos = params.keyboardPosition || null;
                    this.resetOnOverflow = params.resetOnOverflow || true;
                },
                
                /**
                 *  create the small inputField that is shown in normal interface 
                 *  @supportMode (for both full / numPad)
                 */
                createPageInputField : function ()
                {
                    accedo.console.log("createPageInputField");
                    this.pageInputField = _class.inputField({
                        css:"pageInputField",
                        parentKeyboard: keyboard
                    });
                    this.root.appendChild(this.pageInputField.root);

                    accedo.console.log("this.numericalInput: " + this.numericalInput );
                    if (this.numericalInput!=true)
                    {
                        this.pageInputField.setText(_class.DEFAULTINPUT);
                    }else{
                        this.pageInputField.setText("");
                    }
                    
                    this.pageInputField.setWidth(this.pageInputFieldwidth);
                },
                
                /**
                 *  set the position of the NumPad keyboard
                 *  @param (object) dimension{ x,y}
                 *  
                 */
                setNumPadPosition : function (dimension)
                {
                    this.keyBoardPos = dimension;
                },

                createElement: function()
                {
                    this.createFullKbElement();
                    this.createEmptyKeyboard("fullKeyBoardLayout");
                    this.refactorFullKeyboard();

                    this.fillInLetterKey("number"); // number should go first
                    this.fillInLetterKey("smallCap");
                    this.fillSpecialKey("other_full");

                    // pre-enter the past input if any exist
                    if (this.oldStr.length>0)
                    {
                        this.cursPos = this.oldStr.length;
                        this.updateOutField(this.oldStr, this.cursPos);
                    }

                    this.capKey = this.keyBoardLayoutArray[4][0];
                    this.symbolKey = this.keyBoardLayoutArray[2][10];

                    if (this.defaultCapitalInput)
                    {
                        this.switchCaps();
                    }

                    this.requestKeyFocus([0,0]);
                },

                /**
                 *  create the keyboard with no keys
                 *  @supportMode (for both full / numPad)
                 *  @param (string) name, css name of the key
                 *
                 */
                createEmptyKeyboard : function (name)
                {
                    var self = this;
                    this.keyBoardLayout =  accedo.utils.dom.element('div');
                    this.keyBoardLayout.addClass(name);
                    this.keyBoardLayoutArray = [];

                    for (var row=0, rowlimit = this.data[this.screenSize].row, key;  row<rowlimit; row++)
                    {
                        this.keyBoardLayoutArray.push([]);

                        for (var col=0, collimit = this.data[this.screenSize].col[row], key;  col<collimit; col++)
                        {
                            key = _class.key({
                                row: row,
                                col: col,
                                parentKeyboard: keyboard
                            });
                            key.onFocus = function (){
                                // update the current Focus
                                self.currentFocus = [this.getOption("row"), this.getOption("col")];
                            };

                            key.onClick = function (){
                                self.onKeyPressed();
                            };

                            this.keyBoardLayout.appendChild(key.root);
                            this.keyBoardLayoutArray[row].push(key);
                        }

                        this.keyBoardLayoutArray[row][0].root.addClass("newline");
                    }

                    this.keyBoardContainer.appendChild(this.keyBoardLayout);
                },
                
                /**
                 *  create the outputField of fullscreen Keyboard
                 *  @supportMode (full)
                 */
                createFullKbElement : function()
                {
                    var self = this;

                    var title = accedo.utils.dom.element('div');
                    title.addClass("title");
                    title.setText(this.outputTitle);
                    this.keyBoardContainer.appendChild(title);

                    var outputField = accedo.utils.dom.element('div');
                    outputField.addClass("outputField");
                    this.keyBoardContainer.appendChild(outputField);

                    this.outputText_copy = accedo.utils.dom.element("div");
                    this.outputText_copy.addClass("outputText_copy");
                    this.outputText_copy.setText(this.cursor_copy);
                    outputField.appendChild(this.outputText_copy);

                    this.outputText = accedo.utils.dom.element('div');
                    this.outputText.addClass("outputText greytxt");
                    this.outputText.setText(this.cursor);
                    outputField.appendChild(this.outputText);
                },
                
                /**
                 *  create the outputField of NumPad Keyboard
                 *  @supportMode (numPad)
                 */
                createNumKbBgElement : function()
                {
                    var bg = accedo.utils.dom.element('div');
                    bg.addClass("numbg");
                    this.keyBoardContainer.appendChild(bg);                 
                },

                /**
                 *  Create and display the fullscreen Keyboard
                 *  @supportMode (full)
                 */
                showFullScreenKeyboard : function ()
                {
                    var self = this;

                    this.oldStr = this.resultStr;

                    this.keyBoardHidden = false;
                    this.screenSize = "fullScreen";
                    this.addShadeBg();

                    this.keyBoardContainer = accedo.utils.dom.element('div');
                    this.keyBoardContainer.addClass("accedo-ui-keyboard keyboardContainer full");

                    var bg = accedo.utils.dom.element('img', {
                        'id': 'keyboard_bg',
                        'src': ''
                    });
                    //bg.addClass("bg");
                    this.keyBoardContainer.appendChild(bg);

                    var onImageLoad = function (event) {
                        accedo.console.log('Keyboard bg loaded');
                        bg.getHTMLElement().removeEventListener('load', onImageLoad, false);
                        self.createElement();
                    };
                    bg.getHTMLElement().addEventListener('load', onImageLoad, false);

                    bg.setAttributes({
                        src : 'images/core/keyboard/fullBg.png'
                    });

                    if (this.isPopup == true)
                    {
                        this.keyBoardContainer.addClass("popup");
                    }
                    this.keyboardBase.appendChild(this.keyBoardContainer);
                },

                /**
                 *  create the Num pad keyboard
                 *  @supportMode (numPad)
                 */
                showNumKeyboard : function ()
                {
                    this.oldStr = this.resultStr;
                    this.keyBoardHidden = false;
                    this.screenSize = "smallScreen";

                    this.keyBoardContainer =  accedo.utils.dom.element('div');
                    this.keyBoardContainer.addClass("accedo-ui-keyboard keyboardContainer");

                    if (this.isPopup == true)
                    {
                        this.keyBoardContainer.addClass("popup");
                    }

                    this.keyboardBase.appendChild(this.keyBoardContainer);

                    if (this.keyBoardPos!=null)
                    {
                        this.keyBoardContainer.setStyle({
                            left:this.keyBoardPos.x+"px",
                            top : this.keyBoardPos.y+"px"
                        });
                    }

                    this.createNumKbBgElement();
                    this.createEmptyKeyboard("numPadKeyBoardLayout");
                    this.fillInLetterKey("numPad_number");
                    this.fillSpecialKey("other_num");

                    // pre-enter the past input if any exist
                    if (this.oldStr.length>0)
                    {
                        this.cursPos = this.oldStr.length;
                    }

                    this.requestKeyFocus([0,0]);
                },

                /**
                 *  hide the fullscreen Keyboard
                 *  @supportMode (full)
                 */
                hideFullScreenKeyboard : function ()
                {

                    this.keyBoardHidden = true;
                    this.capKey =null;
                    this.symbolKey =null;
                    this.removeShadeBg();

                    this.keyBoardLayout = null;
                    this.keyBoardLayoutArray = null;
                    this.outputText = null;
                    this.outputText_copy = null;
                    this.currentMode = null;
                    this.currentFocus = [0,0];
                    this.currentMouseFocus = [-1,-1];
                    this.oldStr = "";
                    this.cursPos = 0;
                    this.keyBoardContainer.remove();
                    this.keyBoardContainer = null;

                    accedo.device.manager.popAllKeyEvent();
                    accedo.focus.manager.requestFocus(this);
                    telstra.sua.core.main.keyboardShown = false;
                },
                
                /**
                 *  remove the Num pad keyboard
                 *  @supportMode (numPad)
                 */
                hideNumKeyboard : function ()
                {
                    this.keyBoardHidden = true;
                    
                    this.keyBoardLayout = null;
                    this.keyBoardLayoutArray = null;
                    
                    this.outputText = null;
                    this.outputText_copy = null;
                    this.currentMode = null;
                    this.currentFocus = [0,0];
                    this.currentMouseFocus = [-1,-1];
                    this.oldStr = "";
                    this.cursPos = 0;
                    if (this.keyBoardContainer!=null )
                        this.keyBoardContainer.remove();
                    this.keyBoardContainer = null;
                    
                    //this.clearNumPadKeyHandling();
                    accedo.device.manager.popAllKeyEvent();
                    //accedo.focus.manager.requestFocus(this);
                },
                
                /**
                 *  create the color and different size
                 *  @supportMode (full)
                 */
                refactorFullKeyboard : function ()
                {
                    var specialConfig = this.data[this.screenSize].special,
                    greyKeyConfig = specialConfig.grey,
                    greenKeyConfig = specialConfig.green,
                    width2Config = specialConfig.width2,
                    width3Config = specialConfig.width3;
                    
                    // paint key grey
                    for (var i = 0, limit = greyKeyConfig.length; i< limit; i++ ){
                        this.keyBoardLayoutArray[greyKeyConfig[i][0]][greyKeyConfig[i][1]].root.addClass("grey");
                    }
                    
                    //paint key green 
                    this.keyBoardLayoutArray[greenKeyConfig[0]][greenKeyConfig[1]].root.addClass("green");
                    
                    //set the width 2 key
                    for (var i = 0, limit = width2Config.length; i< limit; i++ ){
                        this.keyBoardLayoutArray[width2Config[i][0]][width2Config[i][1]].root.addClass("width2");
                    }
                    
                    //set the width 3 key (space)
                    this.keyBoardLayoutArray[width3Config[0]][width3Config[1]].root.addClass("width3");
                    
                    //set Left Margin
                    for (var i = 0,  rightMargin = this.data[this.screenSize].special.rightMargin, limit = rightMargin.length ; i<limit; i++)
                    {
                        this.keyBoardLayoutArray[rightMargin[i][0]][rightMargin[i][1]].root.addClass("rightMargin");
                    }
                },
                
                /**
                 * fill in "smallCap" or "bigCap" or "symbol" or "number" input into the keyboard
                 * @supportMode (for both full / numPad)
                 * @param (string) type , "smallCap" or "bigCap" or "symbol" or "number"
                 */
                fillInLetterKey : function(type)
                {
                    var letterData = this.data[type];
                    
                    if (letterData)
                    {
                        this.currentMode = type;
                        for (var row= letterData.startPos[0],
                            letterPos = 0,
                            rowlimit = row + letterData.height,
                            key,content,
                            data_Max = letterData.content.length; row <rowlimit ; row++)
                            {
                            for (var col= letterData.startPos[1],
                                collimit = col + letterData.width; col < collimit && letterPos < data_Max  ; col++, letterPos++)
                                {
                                key = this.keyBoardLayoutArray[row][col];
                                content = letterData.content[letterPos];
                                
                                if (content === 0){
                                    key.setVisible(true);
                                }else if (content ==null){
                                    key.setVisible(false);
                                }else{
                                    key.setOutput(content);
                                    key.setText(content);
                                    key.type = letterData.type;
                                    key.addTextClass(letterData.css);
                                    key.setVisible(true);
                                }
                            }
                        }
                    }else
                    {
                        accedo.console.log("incorrect type");
                    }
                },
                
                /**
                 * fill in other input into the keyboard
                 * @supportMode (for both full / numPad)
                 * 
                 */
                fillSpecialKey : function(listName)
                {
                    var letterData = this.data[listName];
                    for (var i=0, key , data , limit = letterData.length; i<limit; i++)
                    {
                        data = letterData[i];
                        key = this.keyBoardLayoutArray[data.pos[0]][data.pos[1]];
                        
                        if (data.content !=null)
                        {
                            key.type = data.type;
                            
                            if (key.type == _class.SP_CHAR || 
                                key.type == _class.FUNC) 
                                {
                                key.setOutput(data.output);
                            }else
                            {
                                key.setOutput(data.content);
                            }
                            
                            key.setText(data.content);                
                            key.addTextClass(data.css);
                            key.addClass(data.keyCss);
                            key.setVisible(true);
                        }
                        else

                        {
                            key.setVisible(false);
                        }
                    }
                },
                
                /**
                 *  set the focus to new position , focus manage will blur the old one
                 *  @supportMode (for both full / numPad)
                 *  @param (array) position, eg. [0,0]
                 */
                requestKeyFocus : function (newPosition)
                {
                    try{
                        if (newPosition && this.keyBoardLayoutArray!=null)
                        {
                            var keyFocused = this.keyBoardLayoutArray[this.currentFocus[0]][this.currentFocus[1]];
                            if (keyFocused){
                            	keyFocused.blur();
                            }
                            
                            var key = this.keyBoardLayoutArray[newPosition[0]][newPosition[1]];
                            if (key){
                                key.focus();
                            }
                            //remove mouse-focus if any
                            if (this.currentMouseFocus != [-1,-1]){
                            	accedo.focus.managerMouse.releaseFocus();
                            }
                        }
                    }catch(e){
                        accedo.console.log(e);
                    }
                },
                
                /**
                 *  calculate the next focus position
                 *  @supportMode (for both full / numPad)
                 *  @param (number) direction,  direction index
                 */
                getNextFocusPosition : function (direction)
                {
                    var newRow = this.currentFocus[0],
                    newCol = this.currentFocus[1],
                    config = this.data[this.screenSize];
                    
                    if (this.numericalInput)
                    {
                        //NumPad Keyboard's focus handling
                        switch (direction)
                        {
                            case _class.UP:
                                if (newRow>0){
                                    newRow--;
                                }
                                break;
                            case _class.DOWN:
                                if (newRow < config.row - 1){
                                    if (newRow == 2 && newCol == 0){
                                    	break;
                                    }else{
                                    	newRow++;
                                    }
                                }
                                break;
                            case _class.LEFT:
                                if (newCol > 0){
                                    if (newRow == 3 && newCol == 1){
                                    	break;
                                    }else{
                                    	newCol--;
                                    }
                                }
                                break;
                            case _class.RIGHT:
                                if (newRow == 0 && newCol == -1 ){
                                    newCol++;
                                    this.handle.removeClassName("focus");
                                }else if (newCol < config.col[newRow]-1){
                                    newCol++;
                                }
                                break;
                            default:
                                ;
                        }
                    }
                    else
                    {
                        //fullscreen Keyboard's focus handling
                        if (this.currentMode == "symbol")
                        {
                            // symbol direction handling
                            switch (direction)
                            {
                                case _class.UP:
                                    if (newRow>0){
                                        newRow--;
                                    }
                                    
                                    switch(newRow)
                                    {   
                                        case 0:
                                            if (newCol == 11) newCol = 10;
                                            break;
                                        case 2:
                                            switch(newCol)
                                            {
                                                case 8:
                                                    newCol = 9;
                                                    break;
                                                case 9:
                                                    newCol = 10;
                                                    break; 
                                                default:
                                                    ;
                                            }
                                            break;
                                        case 3:
                                            switch(newCol)
                                            {
                                                case 0:
                                                case 1:
                                                    newRow = 2;
                                                    break;
                                                case 2:
                                                    newRow = 2;
                                                    newCol = 4;
                                                    break;
                                                case 3:
                                                    newRow = 2;
                                                    newCol = 5;
                                                    break;
                                                case 4: 
                                                case 5:
                                                    newCol = 7;
                                                    break; 
                                                case 6:
                                                    newCol = 8;
                                                    break; 
                                                default:
                                                    ;
                                            }
                                            break;
                                    }
                                    break;
                                case _class.DOWN:
                                    if (newRow < config.row - 1){
                                        newRow++;
                                    }
                                    
                                    switch(newRow)
                                    {   
                                        case 2:
                                            switch(newCol)
                                            {
                                                case 6:
                                                    newRow = 4;
                                                    newCol = 3;
                                                    break;
                                                case 11:
                                                    newCol = 10;
                                                    break;
                                                default:
                                                    ;
                                            }
                                            break;
                                        case 3:
                                            switch(newCol)
                                            {
                                                case 0:
                                                case 1:
                                                    newRow = 4;
                                                    break;
                                                case 2:
                                                case 3:
                                                    newRow = 4;
                                                    newCol = 1;
                                                    break;
                                                case 4:
                                                    newRow = 4;
                                                    newCol = 2;
                                                    break;
                                                case 5:
                                                    newRow = 4;
                                                    newCol = 3;
                                                    break;
                                                case 8:
                                                    newCol = 7;
                                                    break;
                                                case 9:
                                                    newCol = 8;
                                                    break; 
                                                case 10:
                                                    newCol = 9;
                                                    break; 
                                                default:
                                                    ;
                                            }
                                            break;
                                        case 4:
                                            switch(newCol)
                                            {
                                                case 7:
                                                    newCol = 4;
                                                    break; 
                                                case 8:  
                                                case 9:
                                                    newCol = 6;
                                                    break; 
                                                default:
                                                    ;
                                            }
                                            break;
                                    }
                                    break;
                                case _class.LEFT:
                                    if (newCol>0){
                                        newCol--;
                                    }
                                    
                                    if (newRow ==2 && newCol==6){
                                        newCol=5;
                                    }else if (newRow ==3 && newCol==6){
                                        newCol=7;
                                    }
                                        
                                    break;
                                case _class.RIGHT:
                                    if (newCol < config.col[newRow] - 1){
                                        newCol++;
                                    }
                                    
                                    if (newRow ==2 && newCol==6){
                                        newCol=7;
                                    }
                                    
                                    break;
                                default:
                                    ;
                            }
                        }else    // Big cap, small cap direction handling
                        {
                            switch (direction)
                            {
                                case _class.UP:
                                    if (newRow>0){
                                        newRow--;
                                    }
                                    
                                    switch(newRow)
                                    {   
                                        case 0:
                                            if (newCol == 11) newCol = 10;
                                            break;
                                        case 2:
                                            switch(newCol)
                                            {
                                                case 8:
                                                    newCol = 9;
                                                    break;
                                                case 9:
                                                    newCol = 10;
                                                    break; 
                                                default:
                                                    ;
                                            }
                                            break;
                                        case 3:
                                            switch(newCol)
                                            {
                                                case 2:
                                                    newCol = 4;
                                                    break;
                                                case 3:
                                                    newCol = 5;
                                                    break;
                                                case 4: 
                                                case 5:
                                                    newCol = 7;
                                                    break; 
                                                case 6:
                                                    newCol = 8;
                                                    break; 
                                                default:
                                                    ;
                                            }
                                            break;
                                    }
                                    break;
                                case _class.DOWN:
                                    if (newRow < config.row - 1){
                                        newRow++;
	                                    switch(newRow)
	                                    {   
	                                        case 2:
	                                            if (newCol == 11) newCol = 10;
	                                            break;
	                                        case 3:
	                                            switch(newCol)
	                                            {
	                                                case 8:
	                                                    newCol = 7;
	                                                    break;
	                                                case 9:
	                                                    newCol = 8;
	                                                    break; 
	                                                case 10:
	                                                    newCol = 9;
	                                                    break; 
	                                                default:
	                                                    ;
	                                            }
	                                            break;
	                                        case 4:
	                                            switch(newCol)
	                                            {
	                                                case 2:
	                                                case 3:
	                                                    newCol = 1;
	                                                    break;
	                                                case 4:
	                                                    newCol = 2;
	                                                    break; 
	                                                case 5:
	                                                case 6:
	                                                    newCol = 3;
	                                                    break; 
	                                                case 7:
	                                                    newCol = 4;
	                                                    break; 
	                                                case 8:
	                                                case 9:
	                                                    newCol = 6;
	                                                    break; 
	                                                default:
	                                                    ;
	                                            }
	                                            break;
	                                    	}
	                                   }
                                    break;
                                case _class.LEFT:
                                    if (newCol>0){
                                        newCol--;
                                    }
                                    break;
                                case _class.RIGHT:
                                    if (newCol < config.col[newRow] - 1){
                                        newCol++;
                                    }
                                    break;
                                default:
                                    ;
                            }
                        }
                    }
                    return [newRow, newCol];
                },
                
                /**
                 *  switch between big cap and small cap
                 *  @supportMode (full)
                 */
                switchCaps : function ()
                {
                    switch (this.currentMode)
                    {
                        case "bigCap":
                            this.fillInLetterKey("smallCap");  
                            this.capKey.setText('CAPS');
                            break;
                            
                        case "symbol":
                            this.symbolKey.setText("#+=");
                        case "smallCap":
                            this.fillInLetterKey("bigCap"); 
                            this.capKey.setText('caps');
                            break;
                        default:
                            accedo.console.log("SwitchCaps Error");
                    }
                },
                
                /**
                 *  switch between symbol and small/big cap
                 *  @supportMode (full)
                 */
                switchSymbol : function ()
                {
                    switch (this.currentMode)
                    {
                        case "bigCap":
                        case "smallCap":
                            this.fillInLetterKey("symbol");  
                            this.symbolKey.setText("abc");
                            this.capKey.setText('CAPS');
                            break;
                            
                        case "symbol":
                            this.fillInLetterKey("smallCap"); 
                            this.symbolKey.setText("#+=");
                            this.capKey.setText('caps');
                            break;
                        default:
                            accedo.console.log("switchSymbol Error");
                    }
                },
                
                /**
                 *  delete a character before cursor
                 *  @supportMode (full)
                 */  
                backspace : function(updateItself)
                {
                    if (this.cursPos > 0)
                    {
                        this.oldStr =  this.oldStr.substring(0, this.cursPos-1)+ this.oldStr.substring(this.cursPos);
                        this.cursPos--;
                        this.updateOutField(this.oldStr, this.cursPos);
                    }
                },
                
                /**
                 *  delete the last character in NumPad
                 *  @supportMode (numPad)
                 */  
                numPadBackspace : function ()
                {       
                    if (this.resultStr.length>0)
                    {
                        this.resultStr = this.resultStr.substring(0, this.resultStr.length-1);
                        
                        if (this.isPassword)
                        {
                            this.pageInputField.setText(this.resultStr.replace(/./g,"*"));
                        }else
                        {
                            this.pageInputField.setText(this.resultStr);
                        }
                    }else
                    {
                        if (typeof this.zeroLengthCallback == "function")
                        {
                            this.zeroLengthCallback();
                        }
                    }
                },
                
                /**
                 * Clear all the current content
                 * @public
                 */
                clear: function() {
                    this.resultStr = '';
                    this.pageInputField.setText('');
                },
                
                /**
                 *  Move the cursor to the Left
                 *  @supportMode (full)
                 */
                moveCursorLeft : function ()
                {
                    if (this.cursPos>0)
                    {
                        this.cursPos--;
                        this.updateOutField (this.oldStr, this.cursPos);
                    } 
                },
                
                /**
                 *  Move the cursor to the Right
                 *  @supportMode (full)
                 */
                moveCursorRight : function()
                {
                    if (this.cursPos <= this.oldStr.length)
                    {
                        this.cursPos++;
                        this.updateOutField (this.oldStr, this.cursPos);
                    } 
                },
                
                /**
                 *  Display the text result with cursor update 
                 *  @supportMode (full)
                 *  @param (string) text  , text to be display
                 *  @param (number) cursPos, cursor's position 
                 *  @param (boolean)  newChar, determine whether display  the last character or not
                 */
                updateOutField : function (text, cursPos, newChar)
                {
                    var self = this;
                    // clear the auto hide of password
                    if ( this.hidePWtimeout !=null)
                    {
                        clearTimeout(this.hidePWtimeout);
                        this.hidePWtimeout = null;
                    }
                    
                    if (this.outputText == null){
                    	return; //error protection for timeout
                    }
                    
                    if (typeof cursPos != "undefined" && cursPos < text.length)
                    {
                        if (this.isPassword)
                        {
                            if (newChar === true)
                            {
                                //hide all the letters except the last one
                                this.outputText.setText(text.substring(0, cursPos-1).replace(/./g,"*") + (cursPos > 1 ? text[cursPos-1]:"") + this.cursor +
                                    text.substring(cursPos).replace(/./g,"*"));
                                this.outputText_copy.setText(text.substring(0, cursPos-1).replace(/./g,"*") + (cursPos > 1 ? text[cursPos-1]:"") + this.cursor_copy +
                                    text.substring(cursPos).replace(/./g,"*"));
                              
                                //set up the auto hide of password
                                this.hidePWtimeout = setTimeout(function(){
                                    self.updateOutField(text, cursPos);
                                },500);
                            }else
                            {
                                //hide all the letters
                                this.outputText.setText( text.substring(0, cursPos).replace(/./g,"*") + this.cursor +
                                    text.substring(cursPos).replace(/./g,"*"));
                                this.outputText_copy.setText(text.substring(0, cursPos).replace(/./g,"*") + this.cursor_copy +
                                    text.substring(cursPos).replace(/./g,"*"));
                            }

                        }else
                        {  
                            this.outputText.setText( text.substring(0, cursPos).replace(/&/g,"&amp;").replace(/ /g,"&nbsp;") + this.cursor +
                                text.substring(cursPos).replace(/&/g,"&amp;").replace(/ /g,"&nbsp;"));
                            this.outputText_copy.setText(text.substring(0, cursPos).replace(/&/g,"&amp;").replace(/ /g,"&nbsp;") + this.cursor_copy +
                                text.substring(cursPos).replace(/&/g,"&amp;").replace(/ /g,"&nbsp;"));
                        }
                    }else
                    {
                        if (this.isPassword)
                        {
                            if (newChar === true)
                            {
                                this.outputText.setText(text.substring(0, text.length-1).replace(/./g,"*") + text[text.length-1]+ this.cursor);
                                this.outputText_copy.setText(text.substring(0, text.length-1).replace(/./g,"*") + text[text.length-1]+ this.cursor_copy);
                           
                                //set up the auto hide of password
                                this.hidePWtimeout = setTimeout(function(){
                                    self.updateOutField(text, cursPos);
                                },500);
                            }else
                            {
                                this.outputText.setText(text.replace(/./g,"*") + this.cursor);
                                this.outputText_copy.setText(text.replace(/./g,"*") + this.cursor_copy);
                            }
                        }else
                        {   
                            this.outputText.setText(text.replace(/&/g,"&amp;").replace(/ /g,"&nbsp;") + this.cursor);
                            this.outputText_copy.setText(text.replace(/&/g,"&amp;").replace(/ /g,"&nbsp;") + this.cursor_copy);
                        }
                    }
                    
                },
                
                /**
                 * save the result of the outputField and close the keyboard
                 * @supportMode (full)
                 */
                saveOutputField : function ()
                {
                    accedo.console.log("saveOutputField");
                    this.resultStr = this.oldStr;
                    if (this.resultStr.length>0)
                    {
                        this.pageInputField.root.addClass("fill");
                        
                        if (this.isPassword)
                        {
                            this.pageInputField.setText(this.resultStr.replace(/./g,"*"));
                        }else
                        {
                            this.pageInputField.setText(this.resultStr);
                        }
                        
                    }else
                    {
                        this.pageInputField.root.removeClass("fill");
                        accedo.console.log("this.numericalInput: " + this.numericalInput );
                        if (this.numericalInput!=true)
                        {
                            this.pageInputField.setText(_class.DEFAULTINPUT);
                        }else{
                            this.pageInputField.setText("");
                        }
                         
                    }
                    this.hideFullScreenKeyboard();
                    
                    if (typeof this.maxLengthCallback == "function" )
                    {
                        this.maxLengthCallback();
                    }
                },
                
                /**
                 *  update the pageInputField with the user's input
                 *  @supportMode (numPad)
                 *  @param (string) newStr
                 */
                displayNumPadInput : function (newStr, newChar)
                {
                    var self = this;
                    // clear the auto hide of password
                    if (this.hidePWtimeout != null)
                    {
                        clearTimeout(this.hidePWtimeout);
                        this.hidePWtimeout = null;
                    }
                    
                    //maxLength already reached - reset the previous input ?
                    if (this.resetOnOverflow && this.resultStr.length + newStr.length > this.maxlength) {
                        this.clear();
                    }
                    
                    if (this.resultStr.length + newStr.length <= this.maxlength)
                    {     
                        this.resultStr += newStr;
                        
                        if (this.isPassword)
                        {
                            if (newChar)
                            {
                                this.pageInputField.setText(this.resultStr.substring(0, this.resultStr.length-1).replace(/./g,"*") + this.resultStr[this.resultStr.length-1]);
                                
                                //set up the auto hide of password
                                this.hidePWtimeout = setTimeout(function(){
                                    self.displayNumPadInput("");
                                },500);
                                
                            }else
                            {
                                this.pageInputField.setText(this.resultStr.replace(/./g,"*"));
                            }
                        }else
                        {
                            this.pageInputField.setText(this.resultStr);
                        }
                    }
                    
                    if (this.resultStr.length >= this.maxlength)
                    {
                        if (typeof this.maxLengthCallback == "function")
                        {
                            this.hideNumKeyboard();
                            this.maxLengthCallback();
                        }
                    }  
                },
                
                /**
                 *  update the outputField on FullScreen Keyboard with the user's input
                 *  @supportMode (numPad)
                 *  @param (string) newStr
                 */
                displayInput : function (newStr)
                {
					if (this.currentMode != "numPad_number"){
	                   	this.oldStr = this.oldStr.substring(0, this.cursPos)+ newStr.replace(/&amp;/g,"&") + this.oldStr.substring(this.cursPos);
	                    this.cursPos += newStr.length;
	                    
	                    if (this.oldStr.length > this.maxlength)
	                    {           
	                        this.oldStr  = this.oldStr .substring(0,this.maxlength);
	                        
	                        // check the location of cursor 
	                        if (this.cursPos > this.maxlength)
	                        {
	                            this.cursPos = this.maxlength;
	                        }
	                    }
                    	this.updateOutField(this.oldStr, this.cursPos, true);	
                    }
                    else{
                    	this.numTyped(newStr);
                    }
                },
                
                /**
                 *  Operation to be done when num key is pressed from remote
                 *  @supportMode (for both full / numPad)
                 *  @param (number) letter, 0-9 from remote, delete = -1
                 */
                numTyped : function (letter)
                {
                    if (this.numericalInput)
                    {
                        if (letter == -1 )
                        {
                            this.numPadBackspace();             
                        }else{
                            this.displayNumPadInput(letter,true);
                        }
                    }else
                    {
                        if (letter != -1 )
                        {
                            this.displayInput(letter);
                        }
                    }
                },
                
                /**
                 *  Flash the key when the key is pressed
                 *  @param (number) key
                 */
                numPadflashKey : function (key)
                {
                    if (this.keyBoardLayoutArray != null)
                    {
                        var coord = [], key;
                        switch(key)
                        {
                            case "1":
                                coord = [0,0];
                                break;
                            case "2":
                                coord = [0,1];
                                break;
                            case "3":
                                coord = [0,2];
                                break;
                            case "4":
                                coord = [1,0];
                                break;
                            case "5":
                                coord = [1,1];
                                break;
                            case "6":
                                coord = [1,2];
                                break;
                            case "7":
                                coord = [2,0];
                                break;
                            case "8":
                                coord = [2,1];
                                break;
                            case "9":
                                coord = [2,2];
                                break;
                            case "0":
                                coord = [3,1];
                                break;
                            case -1:
                                coord = [3,2];
                                break;
                            default:
                                return;
                        }   
                        
                        key = this.keyBoardLayoutArray[coord[0]][coord[1]];
                        if (typeof key != "undefined")
                        {
                            var keyFocused = this.keyBoardLayoutArray[this.currentFocus[0]][this.currentFocus[1]];
                            if(keyFocused){
                            	keyFocused.blur();
                            }
                            key.focus();
                            /*setTimeout(function(){
                                key.blur();
                            },_class.keyFlashInterval);*/
                        }
                    }
                },
                
                /**
                 *  Operation to be done when key is pressed
                 *  @supportMode (for full screen mode)
                 */
                onKeyPressed : function (pressedRow, pressedCol)
                {
                    if (!pressedRow){
                    	pressedRow = this.currentFocus[0];
                    }
                    if (!pressedCol){
                    	pressedCol = this.currentFocus[1];
                    }
                    var keyPressed = this.keyBoardLayoutArray[pressedRow][pressedCol];
                    
                    try{        
                        switch (keyPressed.type)
                        {
                            case _class.FUNC:
                                var func_Name = keyPressed.getOutput();
                                if (typeof this[func_Name] == "function")
                                {
                                    this[func_Name]();
                                }
                                break;
                            case _class.CHAR:
                            case _class.STR:
                            case _class.SP_CHAR:
                                var newStr = keyPressed.getOutput(); 

                                this.displayInput(newStr);
                                break;
                            default:
                                ;
                        }
                    }catch (e)
                    {
                        accedo.console.log (e);
                    }
                },
                
                /**
                 * Handle the click event for inputField in normal page
                 * @support full screen mode
                 */
                onClick : function (parent)
                {
                    if (opts.focusable){
                    	if (!this.numericalInput) {
	                        this.showFullScreenKeyboard();
	                        this.createFullScreenKeyHandling();
	                        telstra.sua.core.main.keyboardShown = true;
	                    }
	                    else{
	                    	accedo.focus.manager.requestFocus(this);
						}
                    }
                },
                
                /**
                 *  create the key Event Handling for the Full screen Keyboard
                 *  @supportMode (for both full / numPad)
                 */
                createFullScreenKeyHandling: function ()
                {
                    accedo.device.manager.pushAllKeyEvent();
                    var self = this;
                    
                    accedo.device.manager.registerKey("up", function(){
                        var nextPos = self.getNextFocusPosition (_class.UP);
                        self.requestKeyFocus(nextPos);
                    });
                    accedo.device.manager.registerKey("down", function(){
                        var nextPos = self.getNextFocusPosition (_class.DOWN);
                        self.requestKeyFocus(nextPos);
                    });
                    accedo.device.manager.registerKey("left", function(){
                        var nextPos = self.getNextFocusPosition (_class.LEFT);
                        self.requestKeyFocus(nextPos);
                    });
                    accedo.device.manager.registerKey("right", function(){
                        var nextPos = self.getNextFocusPosition (_class.RIGHT);
                        self.requestKeyFocus(nextPos);
                    });
                    
                    accedo.device.manager.registerKey("enter", function(){
                        self.onKeyPressed();
                    });
                    
                    accedo.device.manager.registerKey("back", function(){
                        self.hideFullScreenKeyboard();
                    });
                },
                
                /**
                 *  create the key Event Handling for the numpad screen Keyboard
                 *  @supportMode (for numPad)
                 */
                createNumPadKeyHandling: function ()
                {
                    var self = this;

                    accedo.utils.fn.delay(function() {
                        accedo.device.manager.registerKey("up", function(){
	                        var nextPos = self.getNextFocusPosition(_class.UP);
	                        self.requestKeyFocus(nextPos);
	                    });
	                    accedo.device.manager.registerKey("down", function(){
	                        var nextPos = self.getNextFocusPosition(_class.DOWN);
	                        self.requestKeyFocus(nextPos);
	                    });
	                    accedo.device.manager.registerKey("left", function(){
	                        var nextPos = self.getNextFocusPosition(_class.LEFT);
	                        self.requestKeyFocus(nextPos);
	                    });
	                    accedo.device.manager.registerKey("right", function(){
	                        var nextPos = self.getNextFocusPosition(_class.RIGHT);
	                        self.requestKeyFocus(nextPos);
	                    });
	
	                    accedo.device.manager.registerKey("enter", function(){
	                        self.onKeyPressed();
	                    });
	                    
	                    accedo.device.manager.registerKey("back", function(){
	                        self.hideNumKeyboard();
	                    });
                        
                        accedo.device.manager.registerKey("0", function(){
                            self.numPadflashKey("0");
                            self.numTyped("0");
                        });
                        accedo.device.manager.registerKey("1", function(){
                            self.numPadflashKey("1");
                            self.numTyped("1");
                        });
                        accedo.device.manager.registerKey("2", function(){
                            self.numPadflashKey("2");
                            self.numTyped("2");
                        });
                        accedo.device.manager.registerKey("3", function(){
                            self.numPadflashKey("3");
                            self.numTyped("3");
                        });
                        accedo.device.manager.registerKey("4", function(){
                            self.numPadflashKey("4");
                            self.numTyped("4");
                        });
                        accedo.device.manager.registerKey("5", function(){
                            self.numPadflashKey("5");
                            self.numTyped("5");
                        });
                        accedo.device.manager.registerKey("6", function(){
                            self.numPadflashKey("6");
                            self.numTyped("6");
                        });
                        accedo.device.manager.registerKey("7", function(){
                            self.numPadflashKey("7");
                            self.numTyped("7");
                        });
                        accedo.device.manager.registerKey("8", function(){
                            self.numPadflashKey("8");
                            self.numTyped("8");
                        });
                        accedo.device.manager.registerKey("9", function(){
                            self.numPadflashKey("9");
                            self.numTyped("9");
                        });
                    }, 1);
                },
                
                /**
                 *  remove the key Event Handling for the numpad screen Keyboard
                 *  @supportMode (for numPad)
                 */
                clearNumPadKeyHandling : function ()
                {
                    for (var i = 0; i < 10; i++)
                    {
                        accedo.device.manager.unregisterKey(i+"");
                    }
                    
                    accedo.device.manager.unregisterKey("eject");
                },
                
                /**
                 *  add a shade background to the keyboard_base
                 *  @supportMode (for both full / numPad)
                 */
                addShadeBg : function ()
                {
                    this.keyboardBase.addClass("shade");
                },

                /**
                 *  remove a shade background in the keyboard_base
                 *  @supportMode (for both full / numPad)
                 */
                removeShadeBg : function ()
                {
                    this.keyboardBase.removeClass("shade");
                },

                /**
                 *  remove a class to the pageInputField
                 *  @supportMode (for both full / numPad)
                 *  @param (string) name
                 */
                removeInputFieldClass : function(name) {
                    this.pageInputField.removeInputFieldClass();
                },
                
                /**
                 *  register the key handling when focus
                 *  @supportMode (for both full / numPad)
                 */
                onFocus : function(){
                    this.pageInputField.focus();
 
                    if (this.numericalInput && opts.focusable)
                    {
                        this.createNumPadKeyHandling();
                        this.showNumKeyboard();
                    }

                    this.dispatchEvent('accedo:keyboard:onFocus');
                },
                
                /**
                 *  unregister the key handling when blur
                 *  @supportMode (for both full / numPad)
                 */
                onBlur : function(){
                    this.pageInputField.blur();
                    
                    if (this.numericalInput && opts.focusable)
                    {
                        this.clearNumPadKeyHandling();
                        this.hideNumKeyboard();
                    }
                },
                
                /**
                 *  activate the inputField
                 */
                activate : function(){
                    opts.focusable = true;
                    this.pageInputField.activate();
                },

                /**
                 *  deactivate the inputField
                 */
                deactivate : function(){
                    opts.focusable = false;
                    this.pageInputField.deactivate();
                },

                /**
                 *  set the initial input of the inputField
                 *  @supportMode (for both full / numPad)
                 *  @ param (string) str, the input string
                 */
                setString : function(str){
                    accedo.console.log("setString: " + str);
                    str = str || "";       
                    this.resultStr = str;

                    if (this.resultStr.length>0)
                    {
                        this.pageInputField.root.addClass("fill");
                        
                        if (this.isPassword)
                        {
                            this.pageInputField.setText( this.resultStr.replace(/./g,"*"));
                        }else
                        {
                            this.pageInputField.setText( this.resultStr);
                        }
                    }else
                    {
                        this.pageInputField.root.removeClass("fill");
                        accedo.console.log("this.numericalInput: " + this.numericalInput );
                        if (this.numericalInput!=true)
                        {
                            this.pageInputField.setText(_class.DEFAULTINPUT);
                        }else{
                            this.pageInputField.setText("");
                        }
                    }      
                },
                
                /**
                 *  Get a clean result string from the input Field.
                 *  @supportMode (for both full / numPad)
                 *  @return (string)
                 */
                getCleanedResultStr : function ()
                {
                    if (this.resultStr) {
                        return this.resultStr.replace(/&amp;/g,"&").replace(/&nbsp;/g," ");
                    } else {
                        return '';
                    }
                },

                /**
                 *  Get the InputField in the keyboard.
                 *  @supportMode (for both full / numPad)
                 *  @return (object)
                 */
                getInputField : function()
                {
                    return this.pageInputField;
                }
            });
            
            keyboard.initialize();
            return keyboard;           
        };
    
        /**
     * create a InputField in the keyboard
     * @param (object) opts
     */
        _class.inputField = function (opts)
        {
            opts = opts || {};
            opts.text = opts.text || '';
            opts.css = opts.css || '';
        
            var pageInputFieldTxt, pageInputField, pageInputFieldBg_M;
            /**
         * This function creates a div object.
         */
            var create = function(opts) {
                var pageInputField,
                pageInputFieldBg_L,
                pageInputFieldBg_R;
            
                pageInputField= accedo.utils.dom.element('div');
                pageInputField.addClass(opts.css);

                pageInputFieldBg = accedo.utils.dom.element('div');
                pageInputFieldBg_L = accedo.utils.dom.element('div');
                pageInputFieldBg_M = accedo.utils.dom.element('div');
                pageInputFieldBg_R = accedo.utils.dom.element('div');
            
                pageInputFieldBg.addClass("pageInputFieldBg");
                pageInputFieldBg_L.addClass("left");
                pageInputFieldBg_M.addClass("middle");
                pageInputFieldBg_R.addClass("right");
            
                pageInputField.appendChild(pageInputFieldBg);
                pageInputFieldBg.appendChild(pageInputFieldBg_L);
                pageInputFieldBg.appendChild(pageInputFieldBg_M);
                pageInputFieldBg.appendChild(pageInputFieldBg_R);
 
                pageInputFieldTxt = accedo.ui.label({
                    parent: pageInputField,
                    css:"pageInputFieldTxt"
                });
                return pageInputField;
            };
        
            opts.root = create(opts);
            opts.focusable = true;
        
            /**
         * Set-up inheritance.
         * @scope accedo.ui.button
         */
            var inputField = accedo.utils.object.extend(accedo.ui.component(opts), {
            	/**
	             * This function dispatches click, mouseover and mouseout events
	             * @private
	             */
	            dispatchClickEvent: function() {
	                opts.parentKeyboard.onClick();
	            },
	            dispatchMouseOverEvent: function() {
	                accedo.focus.manager.requestFocus(opts.parentKeyboard);
	                accedo.focus.managerMouse.requestFocus(opts.root);
	            },
	            dispatchMouseOutEvent: function() {
	                accedo.focus.managerMouse.releaseFocus();
	            },
                        
                /**
             *   for setting the width of the input Field in normal screen
             *   @supportMode (for both full / numPad)
             *   @param ( number ) width  , in px
             */
                setWidth : function (width)
                {
                    this.root.setStyle({
                        width : width+"px"
                    });
                    pageInputFieldBg.setStyle({
                        width : width+"px"
                    });
                    pageInputFieldBg_M.setStyle({
                        width : (width-16)+"px"
                    });
                },
      
                /**
             * This function sets the text label of the button.
             * @param {String} text The text label
             * @public
             */
                setText: function(text) {
                    pageInputFieldTxt.setText(text || '');
                },
            
                /**
             * This function gets the text label of the button.
             * @param NA
             * @public
             */
                getText: function() {
                    return pageInputFieldTxt.getText();
                },
            
                /**
             *  activate the inputField
             */
                activate : function(){
                    opts.focusable = true;
                    this.root.removeClass("inactive");
                },

                /**
             *  deactivate the inputField
             */
                deactivate : function(){
                    opts.focusable = false;
                    this.root.addClass("inactive");
                },

                /**
             *  add a class to the pageInputField
             *  @supportMode (for both full / numPad)
             *  @param (string) className
             */
                addInputFieldClass : function(className) {
                    pageInputFieldTxt.getRoot().addClass(className);
                },

                /**
             *  remove a class to the pageInputField
             *  @supportMode (for both full / numPad)
             *  @param (string) className
             */
                removeInputFieldClass : function(className) {
                    pageInputFieldTxt.getRoot().removeClass(className);
                }
        
            });
        
            inputField.setText(opts.text);
            opts.root.addEventListener('click', inputField.dispatchClickEvent, false);
	        opts.root.addEventListener('mouseover', inputField.dispatchMouseOverEvent, false);
	        opts.root.addEventListener('mouseout', inputField.dispatchMouseOutEvent, false);
            return inputField;
        };

        /**
     * create a key in the keyboard
     * @param (object) opts
     */
        _class.key =  function(opts) {
            opts = opts || {};
            opts.text = opts.text || '';
            var keyText, keyOutput;

            /**
         * This function creates a div object.
         */
            var create = function() {
                var element = accedo.utils.dom.element('div');
                element.addClass('key');

                keyText = accedo.ui.label({
                    parent: element,
                    css: "key_txt"
                });
                return element;
            };
            opts.root = create(opts);
            opts.focusable = true;
        
            /**
         * Set-up inheritance.
         */
            var key = accedo.utils.object.extend(accedo.ui.component(opts), {

                /**
             *  init the varable
             */
                initialize : function ()
                {
                    var self = this;
                
                    if (opts.text) this.setText(opts.text);
                    if (opts.css) this.addClass(opts.css);
                
                    this.output = null;
                },
            
                /**
             * This function dispatches a click event
             */
                dispatchClickEvent: function() {
	                opts.parentKeyboard.onKeyPressed(opts.row, opts.col);
	            },
	            dispatchMouseOverEvent: function() {
	                if(opts.parentKeyboard.numericalInput && opts.row == 3 && opts.col == 0){
	                	return;
	                }
	                
	                var currentFocus = opts.parentKeyboard.currentFocus;
	                if (currentFocus && currentFocus.length == 2){
	                	var keyInFocus = opts.parentKeyboard.keyBoardLayoutArray[currentFocus[0]][currentFocus[1]];
	                	if (keyInFocus){
	                		keyInFocus.blur();
	                	}
	                }
	                key.focus();
	                
	                accedo.focus.managerMouse.requestFocus(opts.root);
	                opts.parentKeyboard.currentMouseFocus = [opts.row, opts.col];
	            },
	            dispatchMouseOutEvent: function() {
	                accedo.focus.managerMouse.releaseFocus();
	            },

                /**
             *  set the text of the button
             */
                setText: function(text) {
                    keyText.setText(text || '');
                },
            
                /**
             *  get the text of the button
             */
                getText : function ()
                {
                    return keyText.getText() ;
                },
            
                /**
             *  set the output of the button
             *  @param (string or function) output
             */
                setOutput : function (output)
                {
                    keyOutput = output;
                },
            
                /**
             *  get the output of the button
             */
                getOutput : function()
                {
                    return keyOutput;
                },
            
                /**
             *  add css class to the text name
             */
                addTextClass : function (className)
                {
                    keyText.root.addClass(className || "");
                },
                
                addClass : function (className)
                {
                    key.root.addClass(className || "");
                },
            
                /**
             *  key onclick event
             */
                onClick: function (){}
            });
  
            key.initialize();
            opts.root.addEventListener('click', key.dispatchClickEvent, false);
	        opts.root.addEventListener('mouseover', key.dispatchMouseOverEvent, false);
	        opts.root.addEventListener('mouseout', key.dispatchMouseOutEvent, false);
            return key;
        };

        // create a base div for all the keyboard
        _class.BASE = accedo.utils.dom.element('div');
        _class.BASE.addClass("accedo-ui-keyboard keyboard_base");
        accedo.app.getRoot().appendChild(_class.BASE);
   
   
        //Default inputtext that tell user to Press enter
        _class.DEFAULTINPUT = "Press enter to input";
        
        //Maximum number of character in the output Field
        _class.MAXLENGTH = 45;

        //default width of the page inputfield
        _class.DEFAULTWIDTH = 100;

        // type of direction
        _class.UP = 0;
        _class.DOWN = 1;
        _class.LEFT = 2;
        _class.RIGHT = 3;

        // type of the data
        _class.CHAR = 1;                // character
        _class.STR = 2;                 //string
        _class.FUNC = 3;                //function
        _class.SP_CHAR = 4;                  //special character

        // interval of key flash
        _class.keyFlashInterval = 200;

        /**
     *  the fullscreen letter config
     */
        _class.keyStrokeData = {
            fullScreen: {
                row: 5,                 
                col: [11,12,11,10,7],                                   //each row have diff col size
                special:{
                    grey:   [[0,10],[1,10],[1,11],[2,10],[3,9],[4,0]],     // [row,col] coord of the grey key
                    green:  [4,3],                                        //  [row,col] coord of the green key
                    width2: [[0,10],[2,10],[3,7],[3,9],[4,3]],                  //  [row,col] coord of the width 2 key
                    width3: [4,1],                                            //  [row,col] coord of the width 3 key   
                    rightMargin : [[0,6],[1,6],[2,6],[3,6],[4,3],[0,9],[1,9],[2,9],[3,8]] // col that need to add right margin
                }
            },
            
            smallScreen: {
                row: 4,                 
                col: [3,3,3,3],                                   //each row have diff col size
                special:{
                    deleteKey:   [3,2]                    //position
                }
            },
            
            bigCap: {
                content: ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",0,0],
                type:     _class.CHAR,
                width:    7,
                height:   4,  
                startPos:  [0,0],                                       //position to start fill in the letter
                css:     "bigCap greytxt"
            },
            
            smallCap: {
                content:  ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",0,0],
                type:     _class.CHAR,
                width:    7,
                height:   4, 
                startPos:  [0,0],
                css:     "smallCap greytxt"
            },
            
            symbol: {
                content:  ["!","#","$","%","^","&amp;","*","(",")","-","_","+","=","|","/","\\",":","'",'"',"?",null,null,null,null,null,null,null,null],
                type:     _class.CHAR,
                width:    7,
                height:   4, 
                startPos:  [0,0],
                css:     "symbol greytxt"
            },
            
            number: {
                content:  ["1","2","3","4","5","6","7","8","9",0,"0"],
                type:     _class.CHAR,
                width:    3,
                height:   4, 
                startPos:  [0,7],                                                           //position [row, col] to start fill in the letter
                css:     "number greytxt"
            },
            
            numPad_number: {
                content:  ["1","2","3","4","5","6","7","8","9",0,"0"],
                type:     _class.CHAR,
                width:    3,
                height:   4, 
                startPos:  [0,0],                                                           //position [row, col] to start fill in the letter
                css:     "number greytxt"
            },
            
            other_full: [{
                content:  "CAPS",
                type:     _class.FUNC,
                output:   "switchCaps",
                css:     "caps whitetxt",
                pos:     [4,0]                                                           //pos [row, col]
            },{
                content:  "Space",
                output:   " ",
                type:     _class.SP_CHAR,
                css:     "space greytxt",
                pos:     [4,1]
            },{
                content:  "@",
                type:     _class.CHAR,
                css:     "at greytxt",
                pos:     [4,2]
            },{
                content:  "Done",
                type:     _class.FUNC,
                output:   "saveOutputField",
                css:     "done whitetxt",
                pos:     [4,3]
            },{
                content:  "",
                type:     _class.FUNC,
                css:     "backspace",
                output:   "backspace",
                pos:     [0,10]
            },{
                content:  "",
                type:     _class.FUNC,
                css:     "back",
                output:   "moveCursorLeft",
                pos:     [1,10]
            },{
                content:  "",
                type:     _class.FUNC,
                css:     "next",
                output:   "moveCursorRight",
                pos:     [1,11]
            },{
                content:  "#+=",
                output:   "switchSymbol",
                type:     _class.FUNC,
                css:     "tab whitetxt",
                pos:     [2,10]
            },{
                content:  "Cancel",
                type:     _class.FUNC,
                output: "hideFullScreenKeyboard",
                css:     "Cancel",
                pos:     [3,9]
            },{
                content:  ".com",
                type:     _class.STR,
                css:     "com greytxt",
                pos:     [3,5]
            },{
                content:  ".au",
                type:     _class.STR,
                css:     "au greytxt",
                pos:     [3,6]
            },{
                content:  "bigpond.com",
                type:     _class.STR,
                css:     "bpCom greytxt",
                pos:     [3,7]
            },{
                content:  ".",
                type:     _class.CHAR,
                css:     "symbol2 greytxt",
                pos:     [4,4]
            },{
                content:  ",",
                type:     _class.CHAR,
                css:     "symbol2 greytxt",
                pos:     [4,5]
            },{
                content:  ";",
                type:     _class.CHAR,
                css:     "symbol2 greytxt",
                pos:     [4,6]
            }
            ],
            
            other_num: [{
                content:  "del",
                type:     _class.FUNC,
                css:     "delete greytxt",
                keyCss:     "delete",
                output:  "numPadBackspace",
                pos:     [3,2]
            }]
        };
        return _class;
    });

