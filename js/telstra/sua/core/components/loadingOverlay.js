
/**
 * loading bar
 */
accedo.define("telstra.sua.core.components.loadingOverlay",["accedo.ui.image","accedo.ui.label","accedo.app", "accedo.device.manager"],
    function(){
        return (function() {
    
            var base = null, circle, loopTimeOut, 
            currentPos =0,  loaderCount = 0;
            const INTERVAL = 150; //500, (ms per frame)
    
            return {
            
                /**
                 *   init the loading Bar. Only need to call once after root is set.
                 */
                init : function() {
                    if (base === null) {
                        base = accedo.ui.label({
                            css : 'loadingOverlayBg',
                            text:""
                        });
        
                        circle = accedo.ui.image({
                            parent: base.getRoot(),
                            divImg: false,
                            css : 'loadingCircle'
                        });
                   
        
                        accedo.app.getRoot().appendChild(base.getRoot());
                        base.setVisible(false);
                    } else {
                        accedo.console.log("Loading Overlay already exist");
                    }
                },
    
                /**
                *  Turn on and show the Loading Bar 
                *  @param (boolean) keyBlockOpt , determine whether block key or not.   false: not block key
                *  @param (boolean) hideBgOpt   ,  if true, hide the background of loading layer
                *  @returns {boolean} true if the loading was really turned on, false if it was already on before
                */
                turnOnLoading : function(keyBlockOpt, hideBgOpt) {
                    accedo.console.log("Calling loading turn ON");
                    loaderCount++;
                    accedo.console.log("loaderCount: "+loaderCount);
                    
                    if (loaderCount === 1) {
                        accedo.console.log("Force-blocking keys");
                        accedo.device.manager.blockAllKeyEvent(true);
                        accedo.console.log("Loading is turned on");
                        base.setVisible(true);
                        base.root.removeClass("hidden");
                        this.loadingAction();
                        return true;
                    } else {
                        //accedo.console.log("Loading Already: ", loaderCount);
                        return false;
                    }
                },
    
                /**
                *  Turn off and hide the loading bar
                *  @param (boolean) resetLoadingCount,  for special case like video buffering 
                *  @returns {boolean} true if the loading was really turned off, false otherwise
                */
                turnOffLoading : function(resetLoadingCount) {
                    accedo.console.log("Calling loading turn OFF");
                    
                    if (resetLoadingCount) {
                        loaderCount = 0;
                    } else {
                        loaderCount--;
                    }
                    
                    accedo.console.log("loaderCount: "+loaderCount);
                    
                    if (loaderCount === 0) {
                        accedo.console.log("Loading is turned off");
                        base.setVisible(false);
                        clearTimeout(loopTimeOut);
                        loopTimeOut = null;
                        accedo.utils.fn.defer(function(){
                            accedo.device.manager.blockAllKeyEvent(false);
                        });
                        return true;
                    } else if(loaderCount < 0) {
                        loaderCount = 0;
                    } else {
                        accedo.console.log("Still have more loading requests: ", loaderCount);
                    }
                    
                    return false;
                },
    
                /**
                 * loop the loading action
                 */
                loadingAction : function() {
                    var target = telstra.sua.core.components.loadingOverlay;
                    if (loaderCount < 1) {
                        clearTimeout(loopTimeOut);
                        return;
                    } else {

                        clearTimeout(loopTimeOut);
                        loopTimeOut = null;
                        circle.setSrc("images/core/loadingOverlay/loadingOverlay_0" + (currentPos+1) + ".png");
                    
                        if(currentPos < 7) {
                            currentPos++;
                        } else {
                            currentPos = 0;
                        }

                        loopTimeOut = setTimeout(target.loadingAction, INTERVAL);
                    }
                    return false;
                },
            
                /**
                 *  return the status of loading
                 *  @return (bool) Loading status
                 */
                isLoadingOn :function() {
                    return loaderCount > 0;
                },

                addClass : function(className){
                    base.getRoot().addClass(className);
                },

                removeClass : function(className){
                    base.getRoot().removeClass(className);
                }
            };
        })();
    });