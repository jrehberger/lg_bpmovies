/**
 * @fileOverview Telstra information strip Bar
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
 */

accedo.define("telstra.sua.core.components.infoMessageBar", [
    'accedo.utils.dom',
    'accedo.ui.component',
    'accedo.utils.object',
    'accedo.ui.layout.linear',
    'accedo.app'],

    function() {

        var SHOWTIME = 7,
        colorSpectrum = {
            "green" : {
                css : "greenBar"
            },
            "greenBig" : {
                css : "greenBigBar"
            },
            "blue" : {
                css : "blueBar"
            },
            "red" : {
                css : "redBar"
            }
        },
        //Keep track of the current bars, so we can display each type of bar once only (by defining its ID)
        currentBars = {},
        
        barObj = null,
        messageBarItem = function(opts){

            var create = function() {
                var element = accedo.utils.dom.element('div');
                element.addClass('infoMessageBar');

                if(opts.colour && colorSpectrum[opts.colour]){
                    element.addClass(colorSpectrum[opts.colour].css);
                }else{
                    element.addClass(colorSpectrum["blue"].css);
                }

                var table = accedo.utils.dom.element('table');
                var tr = accedo.utils.dom.element('tr');
                var td = accedo.utils.dom.element('td');

                accedo.ui.label({
                    parent: td,
                    text: opts.text
                });

                tr.appendChild(td);
                table.appendChild(tr);
                element.appendChild(table);

                return element;
            };
            opts.root = create(opts);
            opts.focusable = false;

            var obj = accedo.utils.object.extend(accedo.ui.component(opts), {

                });

            return obj;
        },

        container =  {
            type:           accedo.ui.layout.linear,
            widthStyle:     accedo.ui.component.WRAP_CONTENT,
            heightStyle:    accedo.ui.component.WRAP_CONTENT,
            orientation:    accedo.ui.layout.linear.VERTICAL,
            id:             '#infoMessageBarStore',
            children:       []
        },

        removeBar = function(ref){
            if (ref.getId()) {
                //Note that bar is not currently displayed any more
                delete currentBars[ref.getId()];
            }
            barObj.remove(ref);
        };

        return {

            initialized : false,
        
            init : function(){
            
                if(this.initialized){
                    return;
                }

                //initialize the bar store
                var baseDiv = accedo.utils.dom.element('div');
                var self = this;

                barObj = container.type(container);
                baseDiv.appendChild(barObj.root);

                accedo.app.getRoot().appendChild(baseDiv);

                accedo.device.manager.getKeyEventDispatcher().addEventListener("accedo:keyEventDispatcher:dispatchKey", function(e){
                    //accedo.console.log("lulu: " + e);
                    switch(e){
                        case "up":
                        case "down":
                        case "left":
                        case "right":
                        case "enter":
                            self.clearAllBar();
                            break;
                    }    
                });

                this.initialized = true;
                
            },

            addBar : function(opts){
                if ('id' in opts && opts.id in currentBars) {
                    //Bar with the given ID is already displayed
                    return;
                }
                //Note that bar is currently displayed
                currentBars[opts.id] = true;
                
                var barToAdd = new messageBarItem(opts);
                barObj.append(barToAdd);
                var time = SHOWTIME;
                if(opts && opts.time){
                    time = opts.time;
                }
                accedo.utils.fn.delay(removeBar, time, barToAdd);
            },

            clearAllBar : function(){
                currentBars = {};
                barObj.removeAll();
            }
        }
    });