
/**
 *   Simple image preloader 
 */
accedo.define("telstra.sua.core.components.imagePreloader", ["accedo.utils.object"], function() {
    return (function() {

        var imageList = {};

        return {
            preload : function(images) {

                var i = images.length,
                    src, image;
                
                while (i--) {
                    src = images[i];

                    if (src && !(src in imageList)) {
                        image = document.createElement('img'); // createElement() instead of new Image() to prevent memory leaks (Samsung)
                        image.src = src;
                        imageList[src] = image;
                    }
                }
            }
        };
    })();
});