/**
 * @fileOverview Navigation bar to be used in Western Digital platform for BigPond Movies
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
 */

accedo.define("telstra.sua.core.components.navigationBar", [
    'accedo.utils.dom',
    'accedo.utils.object',
    'accedo.utils.fn',
    'accedo.ui.layout.linear',
    'accedo.ui.component',
    'accedo.app',
    'telstra.api.manager'],

    function() {
		
        ITEM = {
			"RED": {
				css: "navigationItemRed",
				name: "red"
			},
			"GREEN": {
				css: "navigationItemGreen",
				name: "green"
			},
			"YELLOW": {
				css: "navigationItemYellow",
				name: "yellow"
			},
			"BLUE": {
				css: "navigationItemBlue",
				name: "blue"
			},
			"HOME": {
				css: "navigationItemHome", 
				title: 'Home',
				name: "home"
			},
            "BACK" : {
				css: "navigationItemBack", 
				title: 'Exit',
				name: "back"
            }
        },

		ITEM_OPTIONAL = {},

        containerLayout = {
            type:           accedo.ui.layout.linear,
            widthStyle:     accedo.ui.component.WRAP_CONTENT,
            heightStyle:    accedo.ui.component.FIT_PARENT,
            orientation:    accedo.ui.layout.linear.HORIZONTAL,
            id:             'navigationBar',

            children:       []
        };
        

        return function(){

            var baseDiv;
           
            return {

                menuBtnObj : null,
                barObj : null,
                optionsList : [],
                initialized : false,

                init: function(){

                    if(!this.initialized){

                        baseDiv = accedo.utils.dom.element('div');
                        baseDiv.addClass("navigationBar");
                        accedo.app.getRoot().appendChild(baseDiv);

                        var baseDivInner = accedo.utils.dom.element('div');
                        baseDivInner.addClass("navigationBarBody");
                        baseDiv.appendChild(baseDivInner);

                        var container = containerLayout.type(containerLayout);
                        baseDivInner.appendChild(container.getRoot());

                        container.getRoot().setStyle({
                            position: "absolute",
                            left : "0px"
                        })

                        this.menuBtnObj = container;

						var redBtn = this.navigationItem(ITEM["RED"]);
                        this.menuBtnObj.append(redBtn);

						var greenBtn = this.navigationItem(ITEM["GREEN"]);
                        this.menuBtnObj.append(greenBtn);

						var yellowBtn = this.navigationItem(ITEM["YELLOW"]);
                        this.menuBtnObj.append(yellowBtn);

						var blueBtn = this.navigationItem(ITEM["BLUE"]);
						this.menuBtnObj.append(blueBtn);

						//check if firmware year is less than 2011 - make HOME btn as Netcast
						/*accedo.console.log("//// FirmwareYear::: " + accedo.device.manager.identification.getFirmwareYear());
                        if (accedo.device.manager.identification.getFirmwareYear() <= "2011"){
                        	ITEM["HOME"].css += " netcast";
                        	ITEM["HOME"].title = "Netcast";
                        }*/
						this.homeBtn = this.navigationItem(ITEM["HOME"]);
                        this.menuBtnObj.append(this.homeBtn);
                        
                        this.backBtn = this.navigationItem(ITEM["BACK"]);
                        this.menuBtnObj.append(this.backBtn);

						ITEM_OPTIONAL = {
							"red": redBtn,
							"green": greenBtn,
							"yellow": yellowBtn,
							"blue": blueBtn,
							"home": this.homeBtn
						};

                        this.initialized = true;
                    }
                },

                navigationItem : function(opts, hidden){
                	var element = accedo.ui.button({
                        id: 'btn-' + opts.name,
                        css: 'navigationItem ' + opts.css,
                        text: opts.title ? opts.title : ""
                   });
                   element.addEventListener('click', function() {
                    	accedo.console.log(opts.name + ' clicked!');
                    	accedo.device.manager.dispatchKey(opts.name);
                    });
						
                    return element;
                },

				/*
                navigationItem : function(opts, hidden){
                    var create = function() {
                        var element = accedo.utils.dom.element('div');
                        element.addClass('navigationItem');
						element.setStyle({
							'background': 'url('+opts.path+')', 
							'width': opts.width
						});
						
						//element.setText('Back');
            			
                        var label = accedo.ui.label({
                            parent: element,
                            text: opts.text
                        });
                        

                        return element;
                    };
                    opts.root = create(opts);
                    opts.focusable = false;

                    var obj = accedo.utils.object.extend(accedo.ui.component(opts), {});
                    return obj;
                },

/*
                navigationItem : function(opts){
                    var create = function() {
						var element = accedo.utils.dom.element('div');
                        element.addClass('navigationItem');

                        var item = accedo.utils.dom.element('div', {id:'test', style:'width:50px;'});
						element.appendChild(item);
						
                        var label = accedo.ui.label({
                            parent: element,
                            text: opts.text
                        });
						

                        return element;
                    };
                    opts.root = create(opts);
                    opts.focusable = false;

                    var obj = accedo.utils.object.extend(accedo.ui.component(opts), {});
                    return obj;
                },
*/

                hide : function(){
                    baseDiv.hide();
                },

                show : function(){
                    baseDiv.show();
                },

                hideOptionBtn: function(){
                    //this.optionBtn.hide();
                },

                showOptionBtn: function(){
                    //this.optionBtn.show();
                },

                addClass : function(className){
                    baseDiv.addClass(className);
                },

                removeClass : function(className){
                    baseDiv.removeClass(className);
                },

				setMenuButtons: function(buttons) {
					accedo.console.log('setMenuButtons');
					accedo.console.log(buttons);
					
					var button, title;
					
					for (var i = 0; i < buttons.length; i++) {
						button = ITEM_OPTIONAL[buttons[i]["type"]];
						title = buttons[i]["title"];
						
						if (title){
							button.setText(title);	
						}
						button.root.setStyle({
							"visibility": "visible"
						});
					}
				},

                setMenuButton : function(title) {
					accedo.console.log('setMenuButton ' + title);
					//REMOVED AND REPLACED WITH setMenuButtons
					/*
					ITEM_OPTIONAL[title].root.setStyle({
						"visibility": "visible"
					});
					*/
                },

                resetMenuButton : function(){
                	accedo.console.log('resetMenuButton ');
                	
					for (var i in ITEM_OPTIONAL) {
						//set the Home Movies as default for blue
						if (i==='blue') {
							ITEM_OPTIONAL['blue'].setText('Movies Home');
						} else {
							ITEM_OPTIONAL[i].root.setStyle({
								"visibility": "hidden"
							});
						}
						
					}
					
					this.homeBtn.root.setStyle({
						"visibility": "visible"
					});				
            	
                	//set back as default
                	this.backBtn.setText('Back');
                	this.backBtn.root.addClass('back');
                	this.backBtn.root.setStyle({
						"visibility": "visible"
					});
                },

                hasMenuButton : function(){
                    return (this.menuBtnObj.getChildren().length > 0 ? true : false);
                }, 
                
                setExitButton: function() {                	
                	this.backBtn.setText('Exit');
                	this.backBtn.root.removeClass('back');
                	this.backBtn.root.setStyle({
						"visibility": "visible"
					});
                }
            }
        }
    });