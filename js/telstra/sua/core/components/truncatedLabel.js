/**
 * @fileOverview
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 */

//= require <accedo/utils/object>
//= require <accedo/utils/dom>
//= require <accedo/ui/component>

accedo.define("telstra.sua.core.components.truncatedLabel", [
    "accedo.ui.label",
    "accedo.utils.object",
    "accedo.utils.dom",
    "accedo.ui.component"
    ], function() {

        /**
     * Creates a new label instance.
     * @class A label implementation
     * @extends accedo.ui.label
     * @constructor
     * @param {Object} opts The options object
     * @param {String} opts.text The text label
     * @param {CSS classes} opts.innerLabel_css The innerLabel css
     */
        return function(opts) {

            /**
         * Internal storage for label text.
         * @field
         * @private
         */
            var labelText = opts.text = opts.text || '';
            var innerLabel, element;

            /**
         * This function creates a button object.
         * @private
         * @function
         */
            var create = function() {
                element = accedo.utils.dom.element('div');
                element.addClass('accedo-ui-label');

                innerLabel = accedo.utils.dom.element('div');
                innerLabel.addClass(opts.innerLabel_css);
                element.appendChild(innerLabel);
                
                return element;
            };
            //if(opts && !opts.root){
                opts.root = create(opts);
            //}

            /**
         * Set-up inheritance.
         * @scope accedo.ui.label
         */
            //var obj = accedo.utils.object.extend(accedo.ui.label(opts), {
            var obj = accedo.utils.object.extend(accedo.ui.component(opts), {

                /**
             * Sets the text of the label.
             * @param {String} text The text to set
             * @memberOf accedo.ui.label#
             * @public
             */
                setText: function(text,height) {

                    accedo.utils.fn.delay(function(){                       
                        labelText = text || '';
                        var textToTrim = labelText;
                        var innerLabelObj = innerLabel.getHTMLElement(),
                        elementObj = element.getHTMLElement();
                        var count = 0; //to prevent latch up

						if(height){
							innerLabel.setStyle({'visibility' : 'hidden'});
							innerLabel.setText(labelText);
							while(count < 100 && (innerLabelObj.offsetHeight > height)) {
								if(labelText.length == 0){
									break;
								}
								textToTrim = textToTrim.substr(0,textToTrim.length-3);
								innerLabel.setText(textToTrim + "...");
								count ++;
							}
							innerLabel.setStyle({'visibility' : 'visible'});
						}else{
							innerLabel.setText(labelText);
							while(count < 100 && (innerLabelObj.offsetWidth > elementObj.offsetWidth || innerLabelObj.offsetHeight > elementObj.offsetHeight)) {
								if(labelText.length == 0){
									break;
								}
								textToTrim = textToTrim.substr(0,textToTrim.length-3);
								innerLabel.setText(textToTrim + "...");
								count ++;
							}
						}

                        innerLabel.setStyle({
                            "letterSpacing":"0.03em"
                        });
						
                    },0.1);

                },

                /**
             * Gets the text of the label.
             * @memberOf accedo.ui.label#
             * @public
             * @return {String} Text of the label
             */
                getText: function() {
                    return labelText;
                }


            });

            //Set initial text
            obj.setText(opts.text);
            //accedo.utils.fn.delay(obj.setText, 0.1, opts.text);

            return obj;
        };
    });
