accedo.define('telstra.sua.core.views.startView', 
    [
        'accedo.ui.layout.normal', 
        'accedo.ui.image',
        'telstra.sua.core.config'
    ],  function() {

    return {

        type:           accedo.ui.layout.normal,
        id:             'coreLayout',
        css:            'coreApps',

        children:       [
            {
                id:             'coreSubController',
                controller:     telstra.sua.core.config.homePage,
                widthStyle:     accedo.ui.component.FIT_PARENT,
                heightStyle:    accedo.ui.component.FIT_PARENT
            }
            
            
        ]
    };
});