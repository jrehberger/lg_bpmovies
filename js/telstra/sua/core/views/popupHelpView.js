/**
 * @fileOverview popupHelpView
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 */
accedo.define('telstra.sua.core.views.popupHelpView',
    ['accedo.ui.layout.normal',
    'accedo.ui.layout.absolute',
    "accedo.ui.label",
    "accedo.ui.image",
    "accedo.ui.button",
    "accedo.ui.scrollbar"],
    function() {

        var label   = accedo.ui.label,
        layout      = accedo.ui.layout,
        button      = accedo.ui.fluidButton,
        scrollbar   = accedo.ui.scrollbar;

        return {

            type:layout.absolute,
            id:'popupBg',
            css : 'accedo-ui-popupcontent',
            children: [

            {
                type: layout.absolute,
                id : 'popupMain',
                css:'bpmvPopup panel popup focus helpPopup',
                children:[

                {
                    type : label,
                    id : 'header',
                    css : 'header label',
                    text : ''
                },
                {
                    type:layout.normal,
                    id: 'labelContainer',
                    css : 'labelContainer',
                    children : [
                    {
                        type : label,
                        id : 'innerLabel',
                        css : 'scrollLabel label',
                        text : ''
                    }
                    ]
                },
                {
                    type: layout.absolute,
                    id : 'buttonPanel',
                    css:'buttonPanel panel vertical focus',
                    children:[
                    {
                        type : button,
                        id : 'okButton',
                        css : 'button',
                        text : 'OK'
                    }
                    ]
                },
                {
                    type: accedo.ui.layout.absolute,
                    id:   'scrollerContainer',
                    css:  'upDownScrollerKey',
                    children: [
                    {
                        type: accedo.ui.button,
                        id:   'upScroller',
                        css:  'upDownElement up'
                    },
                    {
	                    type : scrollbar,
	                    css : "verticalScrollbar",
	                    id : "scrollbar",
	                    trackTopping : false
	                },
                    {
                        type: accedo.ui.button,
                        id:   'downScroller',
                        css:  'upDownElement down'
                    }
                    ]
                }
                ]
            }
            ]
        }
    });