/**
 * @fileOverview popupOkInfoView
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 */
accedo.define('telstra.sua.core.views.popupOkInfoView',
    ['accedo.ui.layout.normal',
     'accedo.ui.layout.absolute',
    "accedo.ui.label",
    "accedo.ui.image",
    "accedo.ui.button"],
    function() {

        var label   = accedo.ui.label,
        layout      = accedo.ui.layout,
        button      = accedo.ui.fluidButton;

        return {

            type:layout.absolute,
            id:'popupBg',
            css : 'accedo-ui-popupcontent',
            children: [
            {
                type: layout.absolute,
                id : 'popupMain',
                css: 'bpmvPopup panel popup focus',
                children:[
                    {
                        type : label,
                        id : 'header',
                        css : 'header label',
                        text : ''
                    },
                    {
                        type : label,
                        id : 'innerLabel',
                        css : 'innerLabel label',
                        text : ''
                    },
                    {
                            type: layout.absolute,
                            id : 'buttonPanel',
                            css:'buttonPanel panel vertical focus',
                            children:[
                                {
                                    type : button,
                                    id : 'okButton',
                                    css : 'button',
                                    text : 'OK'
                                }
                            ]
                        }

                ]
            }
            ]
        }
    });