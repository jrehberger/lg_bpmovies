/**
 * @fileOverview Error manager, consolidation of error handling in different moduels and show up a 
 *               popup/message strap with proper message
 * @author <a href="mailto:roy.chan@accedobroadband.com">Roy Chan</a>
 */
accedo.define(
    "telstra.sua.core.error",
    [
    "telstra.sua.core.controllers.popupOkInfoController",
    "telstra.sua.core.components.loadingOverlay",
    "accedo.device.manager"
    ],
    function(){
        /*
        100# Global Errors
        200# SUA Home Page
        300# On Now
        400# TV Guide
        500# BigPond Movies
        600# BigPond TV
        700# Game Analysers
        */
        var CODE = {
            GLOBAL_NETWORK_DISCONNECT: 1000,
            GLOBAL_API_FAILED: 1001,
            GLOBAL_VIDEO_FAILED: 1002,
            BPVOD_MEDIAITEM_NOT_FOUND: 5001
        },
        error_map = {},
        default_popup_callback = function(){
            var proxy = accedo.app.getCurrentController().get("coreSubController");
            
            if (proxy && proxy.getInstance){
                var controller = proxy.getInstance();
                controller && controller.onKeyBack && controller.onKeyBack();
            }else{
                //There would be some case that the error happens in between the switching of controller
                accedo.device.manager.dispatchKey("exit");
            }
        },
        currentErrorPopupCode = null,
        clearErrorPopup = function(code){
            if (currentErrorPopupCode == code){
                currentErrorPopupCode = null;
            }
        };
        
        return {
            /**
             * CODE contains the codes for different error scenario
             */
            CODE: CODE,
            /**
             * Initialize error manager
             */
            init: function(){
                error_map[CODE.GLOBAL_NETWORK_DISCONNECT] = {
                    title: 'Network Connection Fail',
                    text: "Network Disconnected, please Check your internet connection.<br/><br/> Press OK to Exit",
                    callback: function(){
                        //Stop the video (fixes bugs if BPMovie/GA was playing) then exit without any other processing (not saving the postcode, etc)
                        accedo.device.manager.system.exit();
                    }
                };
                error_map[CODE.GLOBAL_API_FAILED] = {
                    title: 'Network Connection Fail',
                    text: 'API: The network connection has timed out. Please check your network connection and try again.'
                };
                error_map[CODE.GLOBAL_VIDEO_FAILED] = {
                    title: 'Network Connection Fail',
                    text: 'Video: The network connection has timed out. Please check your network connection and try again.',
                    callback: function(){}
                };
                error_map[CODE.BPVOD_MEDIAITEM_NOT_FOUND] = {
                    title: 'System comes across an error',
                    text: 'Reference code: INVALID_MEDIAITEMID'
                };
            },
            /**
             * Show the error popup with a callback function
             * @param {Number} code in telstra.sua.core.error.CODE.XXXX , please add it when necessary
             * @param {Function} callback (optional) overrides the callbck function when 'OK' button on the error popup is pressed
             * @param {Function} after (optional) a callback to launch after the error popup was created
             */
            show: function(code, callback, after){
                accedo.console.log('Error occurs, code:'+code);
                
                if (!error_map[code]){
                    accedo.console.log("Error manager: Error code cannot be mapped - probably missing error definition. code:"+code);
                    return;
                }
                
                if (currentErrorPopupCode){
                    //popup already showing
                    return;
                }
                
                currentErrorPopupCode = code;
                var error_item = error_map[code];
                
                var popupError = telstra.sua.core.controllers.popupOkInfoController({
                    headerText:  error_item.title,
                    innerText : error_item.text,
                    buttonText: error_item.button || 'OK',
                    callback: function(){
                        var targetCallback = error_item.callback;
                        clearErrorPopup(code);
                        targetCallback();
                        currentErrorPopupCode = null;
                    }
                });

                telstra.api.usage.log("error", "", {
                    description: error_item.title
                });
                
                return popupError;
            } 
        };
    });