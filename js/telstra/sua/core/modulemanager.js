/**
 * @fileOverview Module Manager. Lazily loads controllers on demand,
 *  and enables loading each module's menuController
 *  according to configuration. Also listens to the loaded controllers' events
 *  such as 'telstra:core:loadController' and 'telstra:core:exitModule'
 * @author <a href="mailto:gregory.desfour@accedobroadband.com">Gregory Desfour</a>
 */
accedo.define("telstra.sua.core.modulemanager", [
    'accedo.utils.dom',
    'accedo.utils.array',
    'accedo.utils.fn',
    'accedo.ui.controllerProxy',
    'accedo.ui.component',
    'accedo.focus.manager',
    'accedo.focus.managerMouse',
    'accedo.device.manager',
    'telstra.api.manager',
    'telstra.sua.core.components.infoMessageBar',
    'telstra.sua.core.components.loadingOverlay'
    ],  function() {
    
        return (function(){
    
            var moduleProxy,        //Proxy containing the current module's active controller (or the home page)
            currentModuleId,        //Current module indicator
            currentController,      //Current controller indicator
            history = [],           //History of navigation
            cachedMenu = [],        //Cached of menu controllers in Home
            isLoading = false,      //When we are loading a new controller, ignore other loading requests

            /**
             * Push the current state (or the given one) to the history array
             * @param   {Object} e                  Event object
             * @param   {Object} e.fromContext      Object used to represent the current context, that will be used when going back
             *                                      in history (for instance what element is focused, what list child is selected)
             * @param   {String} e.push_moduleId    (Optional) module ID to push. By default the current module ID is pushed
             * @param   {String} e.push_moduleController   (Optional) controller name to push. By default the current controller name is pushed
             * @private
             */
            historyPush = function(e) {
                e = e || {};
                // accedo.console.info('pushing history -> new size: ' + (history.length + 1));
                history.push({
                    moduleId:   ('push_moduleId' in e) ? e.push_moduleId : currentModuleId,
                    controller: ('push_moduleController' in e) ? e.push_moduleController : currentController,
                    context:    e.fromContext
                });
            },
            
            /**
             * Clear the history
             * @returns {Object} the first element in history before it was cleared
             * @private
             */
            historyClear = function() {
                var firstHistory = null;
                
                if (history.length) {
                    firstHistory = history[0];
                }
                history = [];
                //Nothing else to do it seems...
                return firstHistory;
            },
            
            /**
             * Pop the latest available state in the history array
             * If the contoller/module couple is provided but unfound in history, it will go back to the Core module (!)
             * @param   {Object} e                      Optional, event object
             * @param   {Object} e.moduleController     Optional, controller to go back to.
             * @param   {Object} e.moduleId             Optional, module to go back to.
             * @private
             */
            historyBack = function(e) {
                if (isLoading) {
                    accedo.console.info('Another loading request is currently processing - ignoring this one');
                    return;
                }
                
                var targetController,
                targetModule,
                latest = history.pop();
                
                //No history -> the user may want to quit the app (used for standalone Movies)
                if (!latest) {
                    telstra.sua.core.main.moveBackToMyApps();
                    //accedo.device.manager.dispatchKey("exit");
                    return;
                }
                
                //There is some history, set the loading flag and hide the navBar then process
                isLoading = true;
                //telstra.sua.core.main.mainNavBar.hide();
                
                if (e && 'moduleController' in e && 'moduleId' in e) {
                    targetController = e.moduleController;
                    targetModule = e.moduleId;
                }
                
                // go back in history as long as targetController is not found
                if (targetController && targetModule) {
                    while (latest && !(latest.controller === targetController && latest.moduleId === targetModule)) {
                        latest = history.pop();
                    }
                }
                
                if (latest) {
                    //Add the 'fromHistory' indicator to the context
                    latest.context = latest.context || {};
                    latest.context._fromHistory = true;

                    accedo.console.log(latest.context);
                    
                    if (latest.moduleId && latest.controller) {
                        loadController.call(this, {
                            moduleId:           latest.moduleId,
                            moduleController:   latest.controller,
                            context:            latest.context,
                            skipHistory:        true
                        }, true);
                    } else {
                        exitModule.call(this, null, latest.context, true);
                    }
                }
            },

            /**
             * Clear the history untill reaching a particular module
             * If the contoller/module couple is provided but unfound in history, it will go back to the Core module (!)
             * @param   {Object} e                      Optional, event object
             * @param   {Object} e.moduleController     Optional, controller to go back to.
             * @param   {Object} e.moduleId             Optional, module to go back to.
             * @private
             */
            clearTillLast = function(e){
                var targetController,
                targetModule,
                latest = history.pop();
                
                if (e && 'moduleController' in e && 'moduleId' in e) {
                    targetController = e.moduleController;
                    targetModule = e.moduleId;
                }
                
                // go back in history as long as targetController is not found
                while (latest && !(latest.controller === targetController && latest.moduleId === targetModule) && history.length >0) {
                    latest = history.pop();
                }

                //put it back
                history.push(latest);
            },

            /**
             * Load menuControllers and add them to a layout then trigger a callback
             * @param {accedo.ui.container} subLayout container to which new controllers will be appended
             * @param {Function} onReady callback triggered when all the new controllers have been loaded
             */
            loadMenuFn = function(subLayout, onReady) {
                
                if (cachedMenu.length > 0){
                    var lastChild = subLayout.getChildren()[1];
                    subLayout.remove(lastChild);

                    accedo.utils.array.each(cachedMenu, function(menu){
                        subLayout.append(menu);
                    });

                    subLayout.append(lastChild);
                    onReady(telstra.sua.core.config.modulesList);
                    return;
                }
                
                
                var loadingController,
                readyCount = 0,
                
                //When all menuControllers have been loaded...
                signalReady = function() {
                    if (++readyCount == telstra.sua.core.config.modulesList.length) {
                        //Launch the callback function with modulesList param so that the core's list can be created
                        subLayout.append(lastChild);
                        onReady(telstra.sua.core.config.modulesList);
                    }
                };
                
                //Don't block the app if there is no module
                if (! telstra.sua.core.config.modulesList.length) {
                    onReady();
                    return;
                }

                var lastChild = subLayout.getChildren()[1];
                subLayout.remove(lastChild);
                
                //For each module, load its associated menuController
                accedo.utils.array.each(telstra.sua.core.config.modulesList, function(moduleConfig){
                
                    if (moduleConfig.hasMenuController !== false) {
                        loadingController = 'telstra.sua.module.'+moduleConfig.id+'.controllers.menuController';
                        
                        accedo.define(null, [loadingController], function() {

                            //Create the module's menuController
                            var newController = accedo.ui.controllerProxy({
                                'controller':     telstra.sua.module[moduleConfig.id].controllers.menuController,
                                'id':             moduleConfig.id + obj.MC_SUFFIX,
                                'widthStyle':     accedo.ui.component.FIT_PARENT
                            });

                            //Listen to some of the controller's events (NOT its proxy)...
                            addMMMEvents(newController.getInstance())

                            //Append it hidden, as a sub-controller
                            newController.getRoot().setStyle({
                                zIndex: 1,
                                display: 'none'
                            });
                            cachedMenu.push(newController);
                            subLayout.append(newController);

                            //This menuController is ready
                            signalReady();
                        });
                    } else {
                        //No menuController, we're ready
                        signalReady();
                    }
                });
            },
            
            /**
             * Loads a module's controller if not already present in memory, passes this new controller to the controllerProxy
             * @param {Object} e Object sent with the 'telstra:core:loadController' event
             * @param {String} e.moduleId the root namespace of this module under telstra.sua.module, e.g. 'bpmovies'
             * @param {String} e.moduleController the name of the controller we want
             *      ( = JS file name without the suffix), e.g. 'mainController'
             * @param {Object} e.context Context object that will be added in the options object to the
             *      controller
             * @param {Object} e.fromContext Object used to represent the current context,
             *      that will be used when going back in history
             *      (for instance what element is focused, what list child is selected)
             * @param {Object} (optional) e.skipHistory When true, do not push the current state into history
             * @param {boolean} ignoreIsLoading (optional) If true, continue the processing even if another controller loading was supposedly processing

             */
            loadController = function(e, ignoreIsLoading) {
                if (isLoading && !ignoreIsLoading) {
                    accedo.console.info('Another loading request is currently processing - ignoring this one');
                    return;
                }
                
                isLoading = true;
            
                // accedo.console.log("LoadController >> skipHistory: " + e.skipHistory);
                var self = this;
                
                //Necessary variables (let's not lazyload a file that does not exist !)
                if (moduleProxy && e.moduleId && e.moduleController) {
                    
                    //------Fullscreen tweaks - When going from home/epg to bptv
                    if (e.moduleId === 'bptv') {                        
                        var videoFS = document.createElement('div'),
                        subLayout = document.getElementById('Core_subLayout') ||
                        document.getElementById('EPG_channelHome') ||
                        document.getElementById('EPG_mainView');
                        
                        videoFS.id = "videoFS";
                        subLayout && subLayout.parentNode.appendChild(videoFS);
                        
                        subLayout = null;
                        videoFS = null;

                        telstra.sua.core.components.video.setFullscreen(telstra.sua.core.components.video.playingFTA);
                    }
                    
                    //Remove current focus
                    accedo.focus.manager.requestFocus(null);
                    
                    //Hide the navbar during loading
                    //telstra.sua.core.main.mainNavBar.hide();
                
                    //Timeout for the FS tweak to work visually on Samsung TV
                    setTimeout(function(){
                        //Abort requests not useful to this module
                        telstra.api.manager.abortOtherRequests(e.moduleId);
                        _doLoadController.call(self, e);
                    }, 100);
                }
            },
            
            /**
             * Keep loading the controller if the MMM's reponse is OK
             * @param {Object} e (see above) 
             * @private
             */
            _doLoadController = function(e) {
                var self = this;
                
                //Actually we dont want to unload BPTV when its redirecting to the EPG and the user wants to go back...!
                //Unload objects of other modules
                // unloadOtherModules(e.moduleId);

                telstra.sua.core.main.mainNavBar.resetMenuButton();
                telstra.sua.core.components.infoMessageBar.clearAllBar();

                accedo.define(null, ['telstra.sua.module.'+e.moduleId+'.controllers.'+e.moduleController], function() {
                    
                    if (!e.skipHistory) {
                        historyPush({
                            fromContext: e.fromContext
                        });
                    }
                                        
                    //Reset all key events to the XDK default + the UA default
                    if (e.moduleId !== currentModuleId) {
                        accedo.device.manager.resetKeyEvent();
                    }
                    //Update the 'current...' indicators
                    currentModuleId = e.moduleId;
                    currentController = e.moduleController;
                
                    var newController = telstra.sua.module[e.moduleId].controllers[e.moduleController];
                    
                    //Blur the current focus as XDK's focus manager is not doing it.
                    accedo.focus.manager.requestFocus(null);
                
                    //Listen to some of this new controller's events
                    // newController.getInstance().addEventListener('telstra:core:exitModule', exitModule)
                    
                    /* The above doesn't work because newController is not a controller but a function
                    that takes an argument to create a controller. As a workaround I created and use here a callback
                    on the navigate event, see below */
                    
                    //Navigate to this controller with provided options
                    moduleProxy.getInstance().dispatchEvent(
                        'navigate',
                        {
                            controller: newController,
                            context:    e.context || {},
                            callback:   function(ctl) {
                                //show the navbar after loading
                                telstra.sua.core.main.mainNavBar.show();
                                //Reenable controller loading
                                isLoading = false;
                                //add MMM events support
                                addMMMEvents(ctl);
                            }
                        }
                        );
                });
            },
            
            /**
             * Function triggered by a controller's 'telstra:core:exitModule' event
             * Navigates to the Core Module's starting controller.
             * If toContext is not provided, the original context saved in history will be used.
             * @param {Object} e (optional) Will not be used. Object passed by the dispatcher (as it passes {} if nothing was sent currently)
             * @param {Object} toContext (optional) Context to pass to the new controller
             * @param {boolean} ignoreIsLoading (optional) If true, continue the processing even if another controller loading was supposedly processing
             */
            exitModule = function(e, toContext, ignoreIsLoading) {
                
                if (isLoading && !ignoreIsLoading) {
                    accedo.console.info('Another loading request is currently processing - ignoring this one');
                    return;
                }
                
                //Hide the navbar while loading
                //telstra.sua.core.main.mainNavBar.hide();
                //Disable controller loading
                isLoading = true;
                                
                //Blur the current focus as XDK's focus manager is not doing it.
                accedo.focus.manager.requestFocus(null);
                
                //Delete the objects created by this module
                // unloadModule(currentModuleId);
                
                currentModuleId = null;
                currentController = null;

                //Reset all key events to the XDK default + the UA default
                accedo.device.manager.resetKeyEvent();
                
                //Ho: reset of nav bar also
                telstra.sua.core.main.mainNavBar.removeClass('bpmv');
                telstra.sua.core.main.mainNavBar.resetMenuButton();
                telstra.sua.core.components.infoMessageBar.clearAllBar();
                                
                toContext = toContext || {};
                toContext._exitModule = true; //currently not used any more
                        
                moduleProxy.getInstance().dispatchEvent(
                    'navigate',
                    {
                        controller: telstra.sua.core.config.homePage,
                        context:    toContext || null,
                        callback: function(ctl) {
                            //show the navbar after loading
                            telstra.sua.core.main.mainNavBar.show();
                            //Reenable controller loading
                            isLoading = false;
                            //Add basic MMM events support
                            addMMMEvents(ctl);
                            //Reset history
                            historyClear();
                        }
                    }
                    );
                
                toContext = null;
            },
            
            /**
             * Unloads module-specific objects and updates XDK's 'loadedScripts' object
             * @param {String} moduleId Module to unload
             * @private
             */
            unloadModule = function(moduleId) {
                //delete the objects
                telstra.sua.module[moduleId] = null;
                delete telstra.sua.module[moduleId];
                //remove them from the loadedScripts list so they can be loaded again on demand
                accedo.undefine(new RegExp('^telstra\.sua\.module\.' + moduleId + '\.'));
            },
            
            /**
             * Unloads module-specific objects and updates XDK's 'loadedScripts' object
             * (all modules BUT the one whose id is given) 
             * @param {String} moduleId Module NOT to unload (unload all the others)
             * @private
             */
            unloadOtherModules = function(moduleId) {
                //delete the objects
                for (var mod in telstra.sua.module) {
                    if (mod != moduleId) {
                        telstra.sua.module[mod] = null;
                        delete telstra.sua.module[mod];
                    }
                }
                //remove them from the loadedScripts list so they can be loaded again on demand
                accedo.undefine(new RegExp('^telstra\.sua\.module\.(?!' + moduleId + '\.)'));
            },
            
            /**
             * Adds MMM events support to a controller
             * @param {accedo.ui.controller} ctl    Controller 
             * @private
             */
            addMMMEvents = function(ctl) {
                // Add support for some of the MM specific events to the starting page
                ctl.addEventListener('telstra:core:loadController', accedo.utils.fn.bind(loadController, ctl));
                ctl.addEventListener('telstra:core:exitModule', accedo.utils.fn.bind(exitModule, ctl));
                ctl.addEventListener('telstra:core:historyBack', accedo.utils.fn.bind(historyBack, ctl));
                ctl.addEventListener('telstra:core:clearTillLast', accedo.utils.fn.bind(clearTillLast, ctl));
                ctl.addEventListener('telstra:core:historyPush', accedo.utils.fn.bind(historyPush, ctl));
            },
            
            obj = {
                
                /**
                 * Register the main controller proxy for future use
                 * @param {accedo.ui.controllerProxy} proxy
                 */
                registerModuleProxy: function(proxy) {
                    moduleProxy = proxy;
                    
                    //Add basic MMM events support to the home page
                    addMMMEvents(proxy.getInstance());
                },
                
                /**
                 * Get the main controller proxy
                 * @returns {accedo.ui.controller}
                 * @public
                 */
                getModuleProxy: function() {
                    return moduleProxy;
                },

                /**
                 * return the previousController
                 */
                getPreviousController : function () {
                    accedo.console.log(history);
                    return history[history.length - 1].controller;
                },

                /**
                 * set the previousContext
                 */
                setPreviousContext : function (context) {
                    var previousContext = history[history.length - 1].context;
                    
                    if(previousContext){
                        previousContext = accedo.utils.object.extend(previousContext, context, true);
                    }else{
                        previousContext = context;
                    }
                },
                
                /**
                 * return the currentModuleId
                 */ 
                getCurrentModuleId : function () {
                    return currentModuleId;
                },
                
                //Public access for loadMenuFn
                loadMenu : loadMenuFn,
                
                MC_SUFFIX : 'MenuController',       //Suffix used in every MenuController's ID,
                
                /**
                 * Returns the current isLoading state
                 * @returns {boolean}
                 * @public
                 */
                isLoading: function() {
                    return isLoading;
                },
                /** 
                 * Search in history and see if history contains the wanted controller
                 * @param {Object} e contains the controller and module information which needs to be search,
                 *                 e.g. {moduleController : 'mainController', moduleId: 'bpvod'}
                 * @returns {boolean}
                 * @public
                 */
                hasHistory: function(e){
                    var targetController, targetModule, i, historyLen = history.length;
                    if (e && 'moduleController' in e && 'moduleId' in e) {
                        targetController = e.moduleController;
                        targetModule = e.moduleId;
                    }else{
                        return false;
                    }
                    
                    
                    for (i=0; i<historyLen;i++){
                        if (history[i].controller === targetController && history[i].moduleId === targetModule){
                            return true;
                        }
                    }
                    return false;
                }
            };
        
            return obj;

        })();
    });