/**
 * @fileOverview popupHelpController
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
 */

accedo.define("telstra.sua.core.controllers.popupHelpController", [
    'telstra.sua.core.views.popupHelpView',
    "accedo.utils.object"],
    function() {

        /**
         * @class
         * @extends accedo.ui.popup
         */
        return function(opts) {

            var myPubObj,
            self,
            textContainer,textContent,scrollbar,
            scrollerContainer,upScrollerBtn,downScrollerBtn,
            headerText = opts.headerText || "Help",
            innerText = opts.innerText || "",
            innerTextCSS = opts.innerTextCSS || "",
            buttonText = opts.buttonText || "OK",
            callback = opts.callback,
            popupMain = null,
            okButton = null;

            myPubObj = accedo.utils.object.extend(accedo.ui.popup(opts), {

                init : function(){
                    self = this;
                    //setView
                    this.setView(telstra.sua.core.views.popupHelpView);

                    popupMain = this.get('popupBg').getById('popupMain');
                    popupMain.getById('header').setText(headerText);
                    textContainer = popupMain.getById('labelContainer');
                    textContent = textContainer.getById('innerLabel');
                    textContent.getRoot().addClass(innerTextCSS);
                    textContent.setText(innerText);
                    okButton = popupMain.getById('buttonPanel').getById('okButton');
                    okButton.setText(buttonText);
                    scrollbar = popupMain.getById('scrollbar');
                    scrollerContainer = popupMain.getById('scrollerContainer');
                    upScrollerBtn = this.get('upScroller');
                    downScrollerBtn = this.get('downScroller');

                    var scrollText = function(direction){ //0 = not move, 1 = up, 2 = down
                        var containerHeight = textContainer.getRoot().getHTMLElement().offsetHeight;
                        var currentHeight  = textContent.getRoot().getHTMLElement().scrollHeight + 100;
                        var currentTop = textContent.getRoot().getHTMLElement().offsetTop;
                        
                        if(currentHeight -100 <= containerHeight){
                            scrollerContainer.hide();
                            return;
                        }

                        accedo.console.log("srollText: containerHeight: " + containerHeight + " currentHeight: " + currentHeight + " currentTop: " + currentTop);

                        var targetTop;

                        if(direction == 1){
                            targetTop = currentTop + 95;
                            if(targetTop > 0){
                                targetTop = 0;
                            }
                        }else if (direction == 2){
                            targetTop = currentTop - 95;
                            if(targetTop < -1 * (currentHeight - containerHeight) ){
                                targetTop = -1 * (currentHeight - containerHeight);
                            }
                        }

                        if(direction != 0){
                            currentTop = targetTop;
                            textContent.getRoot().setStyle({
                                top: targetTop + "px"
                            });
                        }


                        scrollbar.update(-1* currentTop, containerHeight, currentHeight);
                    }

                    okButton.setOption("nextUp",function(){
                        scrollText(1);
                    });

                    okButton.setOption("nextDown",function(){
                        scrollText(2);
                    });
                    
                    upScrollerBtn.addEventListener('click',function(evt){
                        scrollText(1);
                    });
                    
                    downScrollerBtn.addEventListener('click',function(evt){
                        scrollText(2);
                    });
                    
                    scrollbar.addEventListener('click',function(evt){
                        scrollText(2);
                    });

                    accedo.utils.fn.defer(scrollText, 1, 0);

                    okButton.addEventListener('click', function() {
                        self.close();
                        callback && callback();
                    });

                    accedo.focus.manager.requestFocus(okButton);
                },

                onKeyBack : function(){
                    self.close();
                    callback && callback();
                }
            });

            myPubObj.init();
            return myPubObj;
        }
    });