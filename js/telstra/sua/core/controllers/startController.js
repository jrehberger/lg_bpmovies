/**
 *  core app's controller
 */
accedo.define("telstra.sua.core.controllers.startController", [
    'accedo.utils.object',
    'accedo.ui.controller',
    'telstra.sua.core.modulemanager',
    'telstra.sua.core.views.startView'],

function() {

    return function(opts) {
                
        return accedo.utils.object.extend(accedo.ui.controller(opts), {
        
            onCreate: function(context) {

                // Set the view
                this.setView(telstra.sua.core.views.startView);
                
                //Register the main controller proxy for the app (will contain the home page or any module's controller)
                telstra.sua.core.modulemanager.registerModuleProxy(this.get('coreSubController'));
            }
        });
    };
});