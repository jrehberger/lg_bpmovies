/**
 * @fileOverview popupOkInfoController
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
 */

accedo.define("telstra.sua.core.controllers.popupOkInfoController", [
    'telstra.sua.core.views.popupOkInfoView',
    "accedo.utils.object"],
    function() {

        /**
         * @class
         * @extends accedo.ui.popup
         */
        return function(opts) {

            var myPubObj,
            self,
            headerText = opts.headerText || "Header",
            innerText = opts.innerText || "innerText",
            buttonText = opts.buttonText || "OK",
            callback = opts.callback || function(){},
            popupMain = null,
            okButton = null;

            myPubObj = accedo.utils.object.extend(accedo.ui.popup(opts), {

                init : function(){

                    self = this;
                    //setView
                    this.setView(telstra.sua.core.views.popupOkInfoView);

                    popupMain = this.get('popupBg').getById('popupMain');
                    popupMain.getById('header').setText(headerText);
                    popupMain.getById('innerLabel').setText(innerText);
                    okButton = popupMain.getById('buttonPanel').getById('okButton');
                    okButton.setText(buttonText);

                    okButton.addEventListener('click', function() {
                        self.close();
                        callback();
                    });
                    
                    accedo.focus.manager.requestFocus(okButton);
                },

                onKeyBack : function(){
                    self.close();
                    callback();
                }
            });

            myPubObj.init();
            return myPubObj;
        }
    });