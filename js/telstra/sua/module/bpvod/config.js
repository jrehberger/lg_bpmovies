accedo.define(
    "telstra.sua.module.bpvod.config",
    null,
    function(){
        
        return {
            imageList : [
            //footer
            "images/core/navigationHelp_lg/navbar_red.png",
            "images/core/navigationHelp_lg/navbar_red_hover.png",
			"images/core/navigationHelp_lg/navbar_green.png",
			"images/core/navigationHelp_lg/navbar_green_hover.png",
			"images/core/navigationHelp_lg/navbar_yellow.png",
			"images/core/navigationHelp_lg/navbar_yellow_hover.png",
			"images/core/navigationHelp_lg/navbar_blue.png",
			"images/core/navigationHelp_lg/navbar_blue_hover.png",
			"images/core/navigationHelp_lg/navbar_lghome.png",
			"images/core/navigationHelp_lg/navbar_lghome_hover.png",
			"images/core/navigationHelp_lg/navbar_netcast.png",
			"images/core/navigationHelp_lg/navbar_netcast_hover.png",
			"images/core/navigationHelp_lg/navbar_exit.png",
			"images/core/navigationHelp_lg/navbar_exit_hover.png",
			"images/core/navigationHelp_lg/navbar_back.png",
			"images/core/navigationHelp_lg/navbar_back_hover.png",
			
			//buttons
			"images/module/bpvod/button/small_buttons.png",
			"images/module/bpvod/button/small_buttons_middle.png",
			"images/module/bpvod/button/stripButtons_mini.png",
			"images/module/bpvod/button/stripButtons_mini2.png",
			"images/module/bpvod/button/stripButtons_small.png",
			"images/module/bpvod/button/stripButtons_star_small.png",
			"images/module/bpvod/button/play.png",
			"images/module/bpvod/button/play_focus.png",
			"images/module/bpvod/button/restart.png",
			"images/module/bpvod/button/restart_focus.png",
            
            //keyboard images
            "images/core/keyboard/fullBg.png",
            "images/core/keyboard/keys.png",
            "images/core/keyboard/outputField.png",
            "images/core/keyboard/smallbg.png",
            
            //login/join
            "images/module/bpvod/pages/joinNow/signInBT_blur.png",
            "images/module/bpvod/pages/joinNow/signInBT_focus.png",
            "images/module/bpvod/pages/joinNow/joinNowBT_blur.png",
            "images/module/bpvod/pages/joinNow/joinNowBT_focus.png",
            "images/module/bpvod/pages/joinNow/unmerteredBT.png",
            "images/module/bpvod/pages/joinNow/unmerteredBTShadow.png",
            "images/module/bpvod/pages/joinNow/star1.png",
            "images/module/bpvod/pages/joinNow/star2.png",
            "images/module/bpvod/pages/joinNow/star3.png",
            
            //home page
            "images/module/bpvod/topMenu_bigPane.png",
            "images/module/bpvod/topMenu_bigPane_mirror.png",
            "images/module/bpvod/topMenu_mediumPane.png",
            "images/module/bpvod/topMenu_mediumPane_mirror.png",
            "images/module/bpvod/topMenu_smallPane.png",
            "images/module/bpvod/topMenu_smallPane_mirror.png",
            
            "images/module/bpvod/topMenu_Icon01_tvShow_big.png",
            "images/module/bpvod/topMenu_Icon01_tvShow_small.png",
            "images/module/bpvod/topMenu_Icon02_movie_big.png",
            "images/module/bpvod/topMenu_Icon02_movie_small.png",
            "images/module/bpvod/topMenu_Icon03_myRental_big.png",
            "images/module/bpvod/topMenu_Icon03_myRental_small.png",
            "images/module/bpvod/topMenu_Icon04_search_big.png",
            "images/module/bpvod/topMenu_Icon04_search_small.png",
            "images/module/bpvod/topMenu_Icon05_SignIn_or_join_big.png",
            "images/module/bpvod/topMenu_Icon05_SignIn_or_join_small.png",
            "images/module/bpvod/topMenu_Icon06_myAccount_big.png",
            "images/module/bpvod/topMenu_Icon06_myAccount_small.png",
            "images/module/bpvod/topMenu_Icon07_freeMovie_big.png",
            "images/module/bpvod/topMenu_Icon07_freeMovie_small.png",
            
            "images/module/bpvod/button/arrow-left.png",
            "images/module/bpvod/button/arrow-right.png",

            //panel images
            "images/module/bpvod/movieDetailsLeftPanel.png",
            "images/module/bpvod/previewBoxBackground.png",
            "images/module/bpvod/pages/joinNow/steps/stepBG.png",
            "images/module/bpvod/pages/joinNow/steps/stepBG_terms.png",
            "images/module/bpvod/pages/new_inputFieldPage/ACC30_bg_new_290910.png",
            "images/module/bpvod/pages/new_inputFieldPage/popup_bg02_new_290910.png",
            "images/module/bpvod/pages/SS-REN-1_0_My_Rentals/MyRentals_bg01.png",
            "images/module/bpvod/pages/SS-MS-4_0_PurchaseOverlayConfirmRental/popup_bg01.png",
            "images/module/bpvod/pages/SS-MS-4_0_PurchaseOverlayConfirmRental/popup_bg01_new_111010.png",
            "images/module/bpvod/pages/SS-ACC-2_0_MyAccountMainMenu/ACC101_bg.png",
            "images/module/bpvod/pages/SS-ACC-3_6_MyAccountPurchaseHistory/ACC36_bg.png",
            
            "images/module/bpvod/pages/SS-CL-1_0_ConnectionInfo/SS-CL1.0_bg_short.png",
            "images/module/bpvod/pages/SS-CL-1_0_ConnectionInfo/ConnectiBar_Cal.png",
            "images/module/bpvod/pages/SS-CL-1_0_ConnectionInfo/SS-CL1.0_bg_long.png",
            
            //popup images
            "images/module/bpvod/popup_classification_background.png",
            "images/module/bpvod/freeMovieTermsAndConditionBackground.png",
            "images/module/bpvod/pages/SS-MS-4_5_SoftExitDecisionPoint/MS4.5_bg.png",
            "images/module/bpvod/pages/SS-REN-1_1_MyRentalsPlaybackOverlay/Playback_bg01.png",
            "images/module/bpvod/popup_sortBy_background.png",
            
            //trailer
            "images/module/bpvod/playTrailerIcon.png",
            "images/module/bpvod/playTrailerIconHover.png"
            ],
                
            /**
             * init the parameters
             */
            init_parameter : function() {
                this.videoPlayer = null;

                //================================
                //Config
                //================================
                this.isProduction = ('<%{ENV_NAME}%>' === 'prod');//is it for production? true to get those developer page hidden.
                this.keypadSelectAsFocus = true; // true: keypad would be selected when it is focused, false: a "enter" needed to be pressed on the focused field to get it selected
                this.testExpiry = false; // true = all purchased movie will be expired in two minutes
                this.sdi_id = true; // use GetESN as ID for 2011 version and onwards?
                this.pinFailLimit = 3; //number of fail attempt allowed for Pin entry

                this.speedSlowOverlayLimit = 1.2; //when it is lower than this speed. Video slow overlay shown
                this.speedTestRedLimit = 1; //max value for red zone
                this.speedTestYellowStart = 2; // min value for yellow zone
                this.speedTestYellowLimit = 2.5; // max value for yellow zone
                this.speedTestSDLimit = 3; // position of recommended SD
                this.speedTestHDLimit = 4.5; //position of recommended HD
                this.speedTestMaxLimit = 6; //position of max HD

                this.systemMainteinanceAPI = "http://bptvtest.bigpond.com/iptvapi/testmaintenance"; //to test the system with "systemMaintenance"
                this.systemComingSoonAPI = "http://bptvtest.bigpond.com/iptvapi/testcomingsoon"; //to test the system with "coming soon"

                this.speedTestVideoUrl = "http://lgtvdownload.bigpond.com/drm5/OnlineMovies/Speedtest/V8_10mins_V2_16x9_1.6M.wmv";
                this.speedTestDrmUrl = "https://wvlicense.vos.bigpond.com/widevine-license-proxy/speedtest";
                this.speedTestVideoUrlSIT1 = "http://moviesvod.vosm.bigpond.com/speedtest/V8SpeedTest11Bitrates.wvm";
                this.speedTestDrmUrlSIT1 = "https://wvlicense.vosm.bigpond.com/widevine-license-proxy/speedtest";

                this.secretString = "LGE";

                //================================
                //operational variable;
                //================================

                this.currentYear = null;
                this.currentMonth = null;

                //Information for Join Now
                this.joinNowInfo = {
                    email: null,
                    pwd: null,
                    firstName: null,
                    surName: null,
                    receiveNews: null,
                    useAccountPin: null,
                    accountPin: null,
                    cardType: null,
                    cardNumber: null,
                    expiryDate: null,
                    securityCode: null,
                    deviceName: null,
                    deviceType: telstra.api.manager.deviceLogger.identityType,
                    deviceId: accedo.device.manager.identification.getUniqueID()
                }

                this.creditCardInfo = {
                    cardType: null,
                    cardNumber: {
                        cc1: null,
                        cc2: null,
                        cc3: null,
                        cc4: null
                    },
                    expiryDate: {
                        month: null,
                        year: null
                    },
                    securityCode: null
                }
        
                this.termsTxt = '<h2>BigPond Movies Terms of Use</h2>'

            + '<p>Please read these Terms carefully so that you understand them.  If you are reading on a TV screen and would like to read on your computer or print a copy, please visit  <span style="color: rgb(0, 147, 193);">http:\/\/downloads.bigpondmovies.com</span> and follow the links for an online copy to the Terms.</p>'

            + '<p>We may change these Terms at any time in accordance with clause 7, and you can always find the latest version by visiting Our Website.  If we consider a change to these terms will make you worse off, we will give you notice of the change and you may choose to cancel your Account as set out below.</p>'

            + '<p>Capitalised words (such as Authorised Electronic Device) have special meanings, which are explained in the Dictionary at the end of these Terms.</p>'

            + '<h3><strong>1. Membership requirements</strong></h3><p class="subheading">Your account, and who can use it</p><p><strong>1.1.</strong> You declare you are over 18 years of age and have legal power to agree to these Terms.</p><p><strong>1.2.</strong> You must ensure your username and password are kept secure and secret.  You are responsible to us for any action or omission by any person using your Account and must ensure any person using your Account complies with these terms.</p><p><strong>1.3.</strong> If you connect to the Internet using BigPond Internet Access, you can use the same username and password for both accounts.  Any person to whom you provide your username and password will then have access to both accounts.  You remain responsible for use made of either account by such person. If you choose use the same username and password, you will need to make sure you register for BigPond Movies.  If you use a different username and password to register for BigPond Movies, you will need to continue signing in to BigPond Movies using that different username and password combination.</p><p><strong>1.4.</strong> You declare, and must ensure, that any Video accessed using the Service is under the control of a person over 18 years of age at all times.</p><p class="subheading">Communicating with you</p><p><strong>1.5.</strong> You must provide us with a valid e-mail address which you regularly check, and promptly tell us if you change that address.  We will use e-mail to inform you of account issues. If you have agreed to receive promotional communications, we will use this e-mail address to send such communications.</p>             <p><strong>1.6.</strong> You can change your contact details on the \'Account Management\' section of Our Website or via your account settings section of your Authorised Electronic Device.</p>' 
        
            + '<h3><strong>2. The Service</strong></h3><p class="subheading">Important limitations to your use</p><p><strong>2.1.</strong> You must make sure you do not use the Service in a way that would result in you, us, or any other person affected by your actions, breaching any law (including by infringing any person\'s copyright or breaching these Terms).</p><p><strong>2.2.</strong> You may only view the Video in accordance with Content Terms applicable to the particular Video.  You must not use the Video in any manner inconsistent with the rights or restrictions set out in the Content Terms.  In particular, unless the Content Terms allow you to do so, you must not do any of the following:</p><ol style="list-style-type: none;">	<li>a) display or make the Video available to the public or in a public space;</li>	<li>b) re-supply or re-transmit the Video to another person;</li>	<li>c) transfer the Video from your Computer or Authorised Electronic Device to any other device or computer (except to the extent normal operation results in transfer to a computer monitor or television screen); or</li>	<li>d) remove the Computer or Authorised Electronic Device from Australia while it contains any Video.</li></ol><p class="subheading">How you can access the Service</p><p><strong>2.3.</strong> You must only access the Service from your Computer or an Authorised Electronic Device, and it must be located in Australia.</p><p><strong>2.4.</strong> To access the Service from an Authorised Electronic Device, it must be connected to the Internet. Some Authorised Electronic Devices must be connected to the Internet through BigPond Internet Access in order to access the Service - to find out if this applies to your Authorised Electronic Device, check the terms that came with it or contact us.</p><p><strong>2.5.</strong> You must not interfere with, or allow interference with, the proper functioning of the Authorised Electronic Device (including with the DRM Software and any other digital rights management and copy protection technology).</p><p><strong>2.6.</strong> To access the Service on your Computer:</p><ol style="list-style-type: none;">	<li>a) you must be able to access Our Website;</li>	<li>b) the Computer must meet the minimum requirements set out on Our Website;</li>	<li>c) the Computer must be running a properly functioning version of the DRM Software; and</li>	<li>d) you must not interfere with, or allow interference with, the proper functioning of the DRM Software.</li></ol><p><strong>2.7.</strong>  DRM Software required to use the Service on your Computer can be downloaded from Our Website without charge (though normal connection and data charges may apply).  You will have to agree to the licence terms and conditions associated with the DRM Software.</p><p><strong>2.8.</strong>  You acknowledge the DRM Software may delete Videos from your Computer or Authorised Electronic Device after the viewing period set out in the Content Terms has expired.</p><p class="subheading">More about Internet access and device requirements</p><p><strong>2.9.</strong>  You are responsible for arranging and paying for a Broadband Connection that connects your Computer or Authorised Electronic Device to the Internet, allowing it to access the Service. The Broadband Connection must meet the minimum requirements specified from time to time on Our Website.</p><p><strong>2.10.</strong> You may be charged data-based download charges for using the Service Broadband Connections in addition to the charge you pay to access the Video through the Service.  Or your Broadband Connection may be "shaped" (or intentionally slowed down) due to the volume of Video accessed through the Service. </p><p><strong>2.11.</strong>  Most BigPond Internet Access plans do not take into account Video accessed through the Service for the purpose of calculating excess data charges or shaping - the Video is "unmetered".  The exceptions are hourly plans including dial-up and satellite plans and international roaming.  You can check whether the Service is unmetered on your BigPond Internet Access plan by checking the terms of your plan.</p><p><strong>2.12.</strong>  Videos typically constitute a large amount of data. For that reason you must ensure:</p><ol style="list-style-type: none;">	<li>a) your Computer or Authorised Electronic Device has sufficient space to store the Video; and</li>	<li>b) your Broadband Connection can download the Video (for example, your service provider will not shape your Broadband Connection in a way that stops you being able to complete the download).</li></ol><p><strong>2.13.</strong> You acknowledge that: </p><ol style="list-style-type: none;">	<li>a) the experience of downloading and playing the Video (including speed and smoothness) partly depends on your Broadband Connection, other devices and users connected to your Broadband Connection and other tasks you are using your Computer or Authorised Electronic Device for at the same time; andd</li>	<li>b) you may not be able to immediately view a Video after purchasing or renting the Video - it may take some time for a sufficient portion of the Video to be downloaded before it can be displayed.</li></ol><p class="subheading">Further limitations on your use</p><p><strong>2.14.</strong> You must not use the Service or Our Website to do any of the following:</p><ol style="list-style-type: none;">	<li>a) defame, abuse, harass or otherwise violate the legal rights of another person;</li>	<li>b) publish, post, upload, e-mail, distribute or disseminate (that is, "transmit") any inappropriate, profane, defamatory, obscene, indecent or unlawful content (including that which infringes another person\'s intellectual property);</li>	<li>c) transmit files that contain viruses, corrupted files or any similar software that may damage or adversely affect the operation of another person\'s computer, Our Website or the Service;</li>	<li>d) advertise or offer to sell any goods or services for any commercial purpose;</li>	<li>e) transmit surveys, contests, pyramid schemes, spam, unsolicited advertising or promotional materials or chain letters;</li>	<li>f) falsify or delete author attributions, legal or other proper notices, or proprietary designations or labels which specify the origin or source of any material;</li>	<li>g) collect or store personal information about another person; or</li>	<li>h) engage in any illegal activities.</li></ol>'

            + '<h3><strong>3. Charges</strong></h3><p><strong>3.1.</strong> Charges are set out on Our Website.  Where you have to pay a charge for a Video, the charge will be shown before you choose to rent or purchase the Video.  All charges are GST inclusive unless otherwise stated.</p><p><strong>3.2.</strong> Monthly charges are payable in advance.  Other charges are payable at the time of purchase or rental unless stated otherwise.</p><p><strong>3.3.</strong> All charges will be:</p><ol style="list-style-type: none; list-style-image: none; list-style-position: outside;">	<li>a) billed to your nominated credit card where you select to pay by credit card (we accept Mastercard, Visa, American Express and Diners Club only);</li>	<li>b) billed to your Telstra bill if this option is available and you nominate this option (only available for selected services at this time); or</li>	<li>c) debited from a prepaid card or voucher issued by us and redeemed by you which you have nominated (any credit must be used by the expiry date shown on the card or voucher, or else it will be forfeited).</li></ol><p><strong>3.4.</strong> If you provide us with a credit card for the purpose of billing or to verify your age, it must be a valid credit card issued in Australia which you are authorised to use.</p><p><strong>3.5.</strong> If you use a credit card for charges, you agree that we may:</p><ol style="list-style-type: none; list-style-image: none; list-style-position: outside;">	<li>a) bill all charges related to the Service (including cancellation charges where applicable) to that credit card;</li>	<li>b) disclose details of the credit card and information about you as set out in the section "Your Privacy", below.</li></ol><p><strong>3.6.</strong> If you choose to charge BigPond Movies to a Telstra bill, you must be the Telstra account holder for that bill or have the authority of the account holder.</p><p class="subheading">Unpaid charges</p><p><strong>3.7.</strong> If you provide us with credit card details and nominate another payment method, but we are unable to use that other payment method to collect charges, we may charge your credit card.</p><p><strong>3.8.</strong> Unpaid debt can be a real problem for us.  For that reason, if any amount is unpaid after the due date, we may do any or all of the following:</p><ul style="list-style-type: none; list-style-image: none; list-style-position: outside;">	<li>a) charge you interest on the unpaid amount from the due date to the date on which we receive full payment (at an annual rate up to the official cash rate set by the Reserve Bank of Australia plus 5% - you can check the cash rate at http: \/\/www.rba.gov.au); and</li>	<li>b) cancel or suspend your Account (provided we comply with clause 8) and, where you are charging to a Telstra bill, suspend any other service linked with that Telstra bill.</li></ul>'
            
            + '<h3><strong>4. Special Offers and Trial Plans</strong></h3><p><strong>4.1.</strong> Clauses 3.4 and 3.5 of these Terms apply to you even while you are accessing the Service under a special offer or trial plan.  This is because there may be circumstances in which you agree to a charge for Video or other content not included in the special offer or trial plan, we may need to check your details to transition you to another plan at the end of the special offer or trial period, and because we need to verify your age even if no charge will apply.</p><p><strong>4.2.</strong> If your Account is suspended during a special offer or trial period, that special offer or trial stops applying to you from the time of that suspension.</p><p><strong>4.3.</strong> Additional terms and conditions may apply to a special offer or trial plan.  Please check when you take up the offer or trial.</p>'

            + '<h3><strong>5.</strong>	Your Privacy</h3><p><strong>5.1.</strong> Please read our "Protecting Your Privacy" statement at http: \/\/www.telstra.com.au/privacy.  It sets out how we and our related companies collect, use and disclose your personal information (including for the purpose of marketing to  you) and your rights to access and correct that information.</p><p><strong>5.2.</strong> In certain circumstances we may need to disclose information about you to others.  We ask, and you agree, that we may subject to the Privacy Act 1988 (Cth):</p><p><strong>a)</strong>	disclose information about you to, and obtain information from, any financial institution or card issuer to verify your details and details of the credit card;</p> <p><strong>b)</strong> take steps to verify there is sufficient credit on your credit card to meet any charges we consider might become due in relation to the Service.</p><p><strong>c)</strong>	disclose information about you (including information relating to conduct of your account) to:</p><ul style="list-style-type: none; list-style-image: none; list-style-position: outside;">	<li>i)	credit reporting agencies to obtain and maintain a Credit Information file about you;</li>	<li>ii)	credit providers or collection agents to collect overdue payments which you owe us and to notify any default by you; and</li>	<li>iii)	our contractors or agents who are involved in the supply of the Service; and</li></ul><p><strong>d)</strong> obtain and use information about your creditworthiness (including consumer or commercial credit reports) from credit reporting agencies, credit providers or other businesses that report on creditworthiness for the purpose of assessing any application by you or collecting overdue payments.</p><p><strong>5.3.</strong> We ask and you agree that we may, in accordance with our "Protecting your Privacy" statement and clause 10.2, disclose Personal Information about you (including sensitive information about you) to Video content providers and licensors for the purpose of assisting them to enforce their intellectual property rights.</p><p><strong>5.4.</strong> Our Website uses cookies.  Cookies are pieces of information stored on your Computer which allows us to track your use of Our Website and allows us to customise information you see, improving the experience of using our Website.  Most web browsers are pre-set to accept and store cookies.  You may choose to turn this functionality off, or set your browser to warn you when a cookie is being stored on your Computer.  For more information, use the "Help" menu in your web browser or contact the company that makes your web browser.</p>'

            + '<h3><strong>6. Content on the Service</strong></h3><p class="subheading">Others\' commercial products</p><p><strong>6.1.</strong> The Service may contain information about products and services offered by third parties, including product details, pricing, availability, performance and editorial commentary.  We are not responsible for any reliance that you place on any of this content (including, but not limited to reviews and movie descriptions).</p><p class="subheading">User generated material</p><p><strong>6.2.</strong> We cannot guarantee the accuracy, integrity or quality of content which other users post on or in relation to the Service, including in user opinion, message board or feedback areas.  We have the right, but not the obligation, to:</p><ol style="list-style-type: none;">	<li>a) review such material before it is made available to other users; and</li>	<li>b) remove any content that we, in our sole discretion, consider objectionable (on the basis of language, content, relevance, appropriateness or likely offense to others).</li></ol><p><strong>6.3.</strong> You agree to use any communication facility which we make available (for example, chat, forum or review services) only to send and receive messages and material properly related to that facility, legal, considerate of other users of the facility and which does not carry any risk to our reputation as a result of facilitating the communication.</p><p><strong>6.4.</strong> You are solely responsible for all material that you upload, post, e-mail, transmit or otherwise make available to us and other users ("Your Content").  You certify you own all intellectual property rights in Your Content or have been licensed to use it as you have (including in relation to your sub-license to us below).</p><p><strong>6.5.</strong> You grant to us, our agents, and contractors a worldwide, irrevocable, royalty-free, non-exclusive, sub-licensable licence to use, reproduce, communicate to the public, create derivative works of, distribute, publicly perform, publicly display, transfer, transmit, distribute and publish Your Content and subsequent versions of it for one or more of the following purposes:</p><ol style="list-style-type: none;">	<li>a) displaying it on our Website and other users of the Service; and</li>	<li>b) storing and distributing to those seeking to download or otherwise acquire it or a copy of it;</li></ol><p>in each case without attribution to you.  This licence applies to distribution and storage in any form, medium or technology now known or later developed.</p>'
            
            + '<h3><strong>7. Changing these Terms</strong></h3><p><strong>7.1.</strong> From time to time, we may change these Terms to reflect our changing business.  Any change will be in accordance with this clause 7.</p><p><strong>7.2.</strong> We will notify you of a change at least 30 days before it will take effect unless:</p><ol style="list-style-type: none;">	<li>a) the change will benefit you (in which case no notice will be given); or</li>	<li>b) we need to make the change in order to ensure we comply with the law or a contract with another party (in which case we will give you as much notice as we can).</li></ol><p><strong>7.3.</strong> If we notify you of a change and the change will make you materially worse off (and does not fall within clause 7.2(b)) you may cancel the Service and your compliance with these Terms by sending us an e-mail to that effect within 14 days of our notice of the change.  Your cancellation will take effect on the same date the change becomes effective.  If you do not notify us of your cancellation, you will be taken to have agreed to the change by continuing to use the Service after the date the change takes effect.</p><p><strong>7.4.</strong> If you are using the Service under an agreement with us that includes a minimum contract period, and you cancel under clause 7.3, you will not have to pay any cancellation fee otherwise payable.</p><p><strong>7.5.</strong> Examples of changes that make you materially worse off include:</p><ol style="list-style-type: none;">	<li>a) a significant increase in recurring fees (eg, more than CPI + 3% in a 12 month period);</li>	<li>b) withdrawal of an important feature of the Service you use regularly; or</li>	<li>c) significant increase of your obligations or significant limitations on your rights under these Terms.</li></ol><p><strong>7.6</strong> To notify you of a change we only have to inform you that we have posted a revised copy of these Terms on Our Website.</p>'
            
            + '<h3><strong>8. Suspension and Cancellation</strong></h3><p class="subheading">Cancellation by you</p><p><strong>8.1.</strong> You may cancel your Account and your obligation to comply with these Terms at any time by contacting our help desk on the number on Our Website.  If you are using the Service under an agreement with us that includes a minimum contract period, and you cancel before the end of that minimum period, we may charge you any early cancellation fee specified in your agreement with us.</p><p><strong>8.2.</strong> You may also cancel your Account and your obligation to comply with these Terms if all of the following apply:</p><ol style="list-style-type: none;">	<li>a) we seriously breach these Terms (for example, if we fail to use reasonable care and skill in providing the Service to you);</li>	<li>b) you have notified us in writing of our serious breach; and</li>	<li>c) either we have failed to remedy the breach within 14 days of us receiving your notice, or the breach is something that cannot be remedied (in which case your cancellation is effective when we receive the notice).</li></ol><p>We will not charge you any cancellation fee if you cancel because of our serious breach.</p><p class="subheading">Cancellation and suspension by us</p><p><strong>8.3.</strong> If you are using the Service under an agreement with us that includes a minimum contract period, we may cancel your Account and our obligation to comply with these Terms before that period by giving you reasonable notice of cancellation (of at least 30 days) and we take reasonable stops to appropriately offset the effect of the cancellation on you (for example, by providing a credit or rebate).  If we cancel your Account and our obligations in this way, we will not charge you any cancellation fee..</p><p><strong>8.4.</strong> If your use of the Service is not subject to any minimum contract period, we may cancel any or all of the Service, your Account and our obligations under these Terms by giving you reasonable notice (of at least 30 days).</p><p><strong>8.5.</strong> We may immediately cancel your Account and our obligations under these Terms if:</p><ol style="list-style-type: none;">	<li>a) you seriously breach these Terms, including by failing to pay charges when due, by infringing the copyright or other intellectual property in any Video or the Service, or by using the Service in a way which we reasonably believe is illegal, likely to be found illegal or likely to cause damage to our reputation; and</li>	<li>b) we have notified you in writing of these circumstances and you have failed to remedy the situation within 14 days of our notice (or any longer period set out in the notice) or the circumstances cannot be remedied (in which case the cancellation is effective upon us giving notice).</li></ol><p> Infringement of copyright or other intellectual property in any Video or the Service is a breach of these Terms which is not capable of remedy.  If we cancel your Account under this clause, we can charge you any cancellation fee that applies under an agreement between you and us.</p><p><strong>8.6.</strong> We may also cancel your Account and our obligations under these Terms by giving you as much notice as we reasonably can if:</p><ol style="list-style-type: none;">	<li>a) you become bankrupt or insolvent or seem likely to do so (in which case we can charge any cancellation fee that applies);  or</li>	<li>b) we are unable to provide the Service due to an event outside our reasonable control (such as an industrial strike or an act of nature).</li></ol><p><strong>8.7.</strong> We may suspend or restrict provision of the Service to you in the notice period before we cancel your services under clauses 8.5 or 8.6(a).  If you are liable for any charges in that time, you must pay them.  If we later determine you did not breach these Terms, we will reimburse you charges paid in that period.</p><p><strong>8.8.</strong> If we cancel our obligations under these Terms, you must immediately return all of our property in your possession to us.</p><p><strong>8.9.</strong> Clauses 2 (the Service), 3 (Charges), 5 (Privacy), 6 (Content on the Service), 7 (Changing these Terms), 8 (Cancellation and Suspension), 9 (Liability) and 10 (Miscellaneous Terms) and any other term which, by its nature survives, apply even after both parties\' obligations under these Terms are otherwise cancelled or suspended.</p>'

            + '<h3><strong>9. Liability</strong></h3><p><strong>9.1.</strong> We accept liability for the Service, but only to the extent set out in this clause 9.</p><p><strong>9.2.</strong> We will use reasonable care and skill in providing the Service and will provide it in accordance with these Terms.  However, due to the nature of the Service (including our reliance on some systems and services not owned or controlled by us) we cannot promise the Service will be continuous or fault-free.</p><p><strong>9.3.</strong> The Service is supplied to you as expressly stated in these Terms and the terms implied by consumer protection laws which cannot be excluded by us.  No other terms or conditions apply.</p><p><strong>9.4.</strong> As the Service is provided to you predominantly for personal, domestic or household use, we do not accept liability for any business-related losses that arise in relation to your use of the Service. However, we will accept such liability if it cannot be excluded due to the operation of legislation.  In those circumstances, our liability will be limited, if allowed under the legislation, to resupplying, repairing or replacing the relevant goods or services (as applicable) if it is fair and reasonable to do so..</p><p><strong>9.5.</strong> We are also not liable to you for:</p><ol style="list-style-type: none;">	<li>a) any loss to the extent caused by you (for example through negligence or breach of these Terms); or</li>	<li>b) any loss caused by our failure to comply with our obligations where that failure is caused by events outside our reasonable control (such as an industrial strike or act of nature).</li></ol><p><strong>9.6.</strong> You are liable to us if you breach these Terms or act negligently (as defined by the courts).  However you are not liable for any loss we suffer to the extent that it is caused by us (for example through our negligence or breach of these Terms).</p>'
            
            + '<h3><strong>10. Miscellaneous Terms</strong></h3><p><strong>10.1.</strong> Neither you, nor we, waive any right under these Terms just because we do not exercise them or delay in exercising them.</p><p><strong>10.2.</strong> Unless otherwise stated, a notice relating to these Terms must be in writing (which includes e-mail).  If we need to notify you, we may send a notice by e-mail to the address you provided or post a notice to your address.  If we use e-mail, you will be taken to have received the e-mail once it leaves our servers unless we receive evidence to the contrary (for example, a "bounce-back").  If we use post, you will be taken to have received the notice 2 Business Days after we post it, unless we receive evidence to the contrary (being days other than Saturday, Sunday or a public holiday in New South Wales).</p><p><strong>10.3.</strong> Your rights under these Terms belong to you alone.  You may not transfer your rights or obligations in respect of the Service or these Terms without our prior consent - which we will not unreasonably withhold.</p><p><strong>10.4.</strong> We may, from time to time, ask another party to provide some aspect of the Service.</p><p><strong>10.5.</strong> We may transfer all our rights or obligations (or both) in respect of the Service and under these Terms to any reputable, credit-worthy third party at any time.  We will notify you within 30 days of any such transfer.</p><p><strong>10.6.</strong> These Terms are governed by the law in force in New South Wales, and both you and we submit to the jurisdiction of the courts of New South Wales.</p>'

            + '<h3><strong>11.</strong> Meanings of special words</h3><p><strong>11.1.</strong> Words with special meanings in these Terms are as follows:</p><p><strong>Account</strong> refers to access to the Service using your username and password.</p><p><strong>Authorised Electronic Device</strong> means the Telstra T-Box&trade; supplied by us and such internet-ready televisions and other devices which we authorise from time to time to access the Service.</p><p><strong>BigPond Internet Access</strong> means a connection to the Internet provided by us under the BigPond brand.</p><p><strong>Broadband Connection</strong> means an Internet connection which links you Computer or Authorised Electronic Device to the Internet and to our Service and which meets the minimum connection speed required by us from time to time as set out on Our Website.</p><p><strong>Computer</strong> means a personal computer (which includes desktops, laptops and notebooks) running operating systems and software required by us as notified on our Website.</p><p><strong>Content Terms</strong> means the terms specific to your use of a Video accessed through the Service. These Terms are specified through the Service.  For example, how long or how many times you can view the Video, prohibitions on downloading, redistributing, altering or deleting the content of the video or advertising or marketing material forming part of, or associated with, the Video.</p><p><strong>Credit Information</strong> means </p><ol style="list-style-type: none;">	<li>a) identity particulars (name, address, and date of birth);</li>	<li>b) your application for credit, including the amount applied for;</li>	<li>c) the fact we are a current credit provider to you;</li>	<li>d) payments which are overdue by more than 60 days and for which debt collection has commenced;</li>	<li>e) advice that payments are no longer overdue in respect of a default which has been listed; and</li>	<li>f) information that you have committed a serious credit infringement</li></ol><p><strong>DRM Software</strong> means the digital rights management and copy protection software installed on your Authorised Electronic Device (or required as part of a firmware upgrade) or specified on Our Website as a requirement for use of the Service on your Computer (as applicable).</p><p><strong>Our Website</strong> means the web site at <a href="http: \/\/downloads.bigpondmovies.com">http: \/\/downloads.bigpondmovies.com</a></p><p><strong>Service</strong> means the service of supplying Videos via the Internet to your Computer or Authorised Electronic Device.</p><p><strong>Video</strong> means the audio-visual content such as movies and TV episodes made available via the Service.</p><p><strong>We, our and us</strong> refers to Telstra Corporation Limited (ABN 33 051 775 556) and its employees, agents, sub-agents and their respective employees.</p><p><strong>11.2.</strong> References in this Agreement to your use of the Service includes use by you and by any other person using your Account.</p>'

            }
        }
    });