/*
 * Define global function and property that needed
 */

accedo.define(
    "telstra.sua.module.bpvod.main",
    [
    "accedo.device.manager",
    "telstra.api.usage",
    "telstra.sua.module.bpvod.utils",
    "telstra.sua.core.components.loadingOverlay",
    "telstra.sua.core.components.imagePreloader",
    "telstra.sua.module.bpvod.config"
    ],
    function(){
        
        return {

            firstRun : true,
            /**
             * init the apps , Define global function and property that needed
             * ONLY if it was not done before
             */
            init: function() {
                telstra.sua.module.bpvod.config.init_parameter();
                telstra.sua.module.bpvod.utils.imagePreload();
                
                /*
                 * pre-fretch data for choose movie category page
                 */
                telstra.sua.module.bpvod.utils.getCategory(function(data){
                    telstra.sua.core.components.loadingOverlay.turnOffLoading();
                    telstra.sua.module.bpvod.utils.movieCategories = data;
                });
            }
        };
    });