accedo.define(
        "telstra.sua.module.bpvod.session",
        null,
        function(){
            return {
                isSignedIn: false,
                signedInButNotFinishedProcess: false,
                rentedMovieList : [],
                autoSignInData : null,
                lastViewVideoItem : null,
                recentlyExpiredMovieList: [],
                listingSortBy: 'most recent'
            };
        }
);
