/**
 * @fileOverview popupResultView
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define('telstra.sua.module.bpvod.views.popupResultView',
    ['accedo.ui.layout.absolute',
    'accedo.ui.label',
    'telstra.sua.module.bpvod.components.fluidButton'],
    function() {

        var label   = accedo.ui.label,
        layout      = accedo.ui.layout,
        button      = telstra.sua.module.bpvod.components.fluidButton;

        return {

            type:layout.absolute,
            id:'popupBg',
            css : 'accedo-ui-popupcontent',
            children: [

            {
                type: layout.absolute,
                id : 'popupMain',
                css:'bpmvPopup RedeemVoucherPopUp panel popup focus',
                children:[

                {
                    type : label,
                    id : 'header',
                    css : 'header label',
                    text : ''
                },
                {
                    type : label,
                    id : 'labelCredit',
                    css : 'labelCredit label',
                    text : ''
                },
                {
                    type : label,
                    id : 'labelBalance',
                    css : 'labelBalance label',
                    text : ''
                },
                {
                    type: layout.absolute,
                    id : 'buttonPanel',
                    css:'buttonPanel panel vertical focus',
                    children:[
                    {
                        type : button,
                        id : 'okButton',
                        css : 'okButton button medium green',
                        text : '',
                        width: '196px'
                    }
                    ]
                }
                ]
            }
            ]
        }
    });