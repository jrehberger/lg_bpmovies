/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.views.joinNowStep3NameView", [
    "telstra.sua.module.bpvod.views.joinNowTemplateView"],
    function(){
        //var keyboard = (accedo.device.manager.identification.getFirmwareYear() == "2011" ? telstra.sua.core.components.keyboard : telstra.sua.core.components.keyboard_new);
        var keyboard = telstra.sua.core.components.keyboard;

        var viewObj = accedo.utils.object.clone(telstra.sua.module.bpvod.views.joinNowTemplateView({
            pageTitle : "First and Last Name",
            highlightImage2 : {
                type: accedo.ui.image,
                css:  'highlight2 highLight2 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage3 : {
                type: accedo.ui.image,
                css:  'highlight3 highLight3 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            stepLabel : {
                type: accedo.ui.label,
                id:   'stepTag',
                css:  'step3tag label',
                text: '3'
            },
            backButtonCss : 'backBT button medium',
            continueButtonCss : 'continueBT button medium',
            backText : 'Back',
            continueText : 'Continue',
            corePanel : [
            {
                type: accedo.ui.label,
                css:  'step3label1 label',
                text: 'First name'
            },
            {
                type:           keyboard,
                id:             'inputField1',
                css:            'pageInputField FNinputField',
                nextDown:       'inputField2',
                params:{
                    title: "First Name",
                    width: 400,
                    maxlength: 50
                }
            },
            {
                type: accedo.ui.label,
                css:  'step3label2 label',
                text: 'Last name'
            },
            {
                type:           keyboard,
                id:             'inputField2',
                css:            'pageInputField LNinputField',
                nextUp:         'inputField1',
                nextDown:       'continueButton',
                params:{
                    title: "Last Name",
                    width: 400,
                    maxlength: 50
                }
            }
            ]
        }));

        return viewObj;
    });