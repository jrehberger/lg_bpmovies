/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define('telstra.sua.module.bpvod.views.myAccountCardDetailsType2View',
    ['accedo.ui.layout.normal',
    'accedo.ui.label',
    'telstra.sua.module.bpvod.components.fluidButton'],
    function() {
        //var keyboard = (accedo.device.manager.identification.getFirmwareYear() == "2011" ? telstra.sua.core.components.keyboard : telstra.sua.core.components.keyboard_new);
        var keyboard = telstra.sua.core.components.keyboard;

        return {

            type:   accedo.ui.layout.normal,
            id:     'bpvod_canvas',
            css:    'background2',
            children:       [
            {
                type: accedo.ui.layout.normal,
                css:  'pageStore',
                children: [
                {
                    type: accedo.ui.layout.normal,
                    id:   'accountPageGeneralClass',
                    css:  'accountPageGeneralClass updateCreditCard',
                    children: [
                    {
                        type: accedo.ui.layout.normal,
                        id: 'primePanel',
                        css: 'panal',
                        children: [
                        {
                            type: accedo.ui.layout.normal,
                            id: 'mainPanel',
                            css: 'mainPanel',
                            children: [
                            {
                                type: accedo.ui.layout.normal,
                                id: 'backgroundBorder',
                                css: 'backgroundBorder'
                            },
                            {
                                type: accedo.ui.label,
                                id: 'innerHeader',
                                css: 'innerHeader',
                                text: 'Credit Card Details'
                            },
                            {
                                type: accedo.ui.layout.normal,
                                css: 'componentPanel panel vertical',
                                children: [
                                {
                                    type: accedo.ui.layout.normal,
                                    css:   'cardDetailPanel panel vertical',
                                    children: [
                                    {
                                        type: accedo.ui.label,
                                        css:  'labelCreditNum label',
                                        text: 'Credit card number'
                                    },
                                    {
                                        type: accedo.ui.layout.normal,
                                        css: 'ccNo panel horizontal',
                                        children: [
                                        {
                                            type:           keyboard,
                                            id:             'ccInput1',
                                            css:            'inputField ccInput1',
                                            nextDown:       'expDateInput1',
                                            nextRight:      'ccInput2',
                                            params:{
                                                title: "CreditCard Number:",
                                                width: 72,
                                                maxlength: 4,
                                                keyboardPosition: {
                                                    x:118,
                                                    y:139
                                                },
                                                numericalInput:true
                                            }
                                        },
                                        {
                                            type:           keyboard,
                                            id:             'ccInput2',
                                            css:            'inputField ccInput2',
                                            nextDown:       'expDateInput1',
                                            nextLeft:       'ccInput1',
                                            nextRight:      'ccInput3',
                                            params:{
                                                title: "CreditCard Number:",
                                                width: 96,
                                                maxlength: 6,
                                                keyboardPosition: {
                                                    x:118,
                                                    y:139
                                                },
                                                numericalInput:true
                                            }
                                        },
                                        {
                                            type:           keyboard,
                                            id:             'ccInput3',
                                            css:            'inputField ccInput3',
                                            nextDown:       'expDateInput1',
                                            nextLeft:       'ccInput2',
                                            params:{
                                                title: "CreditCard Number:",
                                                width: 96,
                                                maxlength: 5,
                                                keyboardPosition: {
                                                    x:118,
                                                    y:139
                                                },
                                                numericalInput:true
                                            }
                                        }
                                        ]
                                    },
                                    {
                                        type: accedo.ui.image,
                                        css:  'creditCard image',
                                        src:  'images/module/bpvod/pages/joinNow/steps/amex.png'
                                    },
                                    {
                                        type: accedo.ui.label,
                                        css:  'labelExpiryDate label',
                                        text: 'Expiry date'
                                    },
                                    {
                                        type: accedo.ui.layout.normal,
                                        css:  'expDate panel horizontal',
                                        children: [
                                        {
                                            type:           keyboard,
                                            id:             'expDateInput1',
                                            css:            'inputField expDateInput1',
                                            nextUp:         'ccInput1',
                                            nextDown:       'cvcInput1',
                                            nextRight:      'expDateInput2',
                                            params:{
                                                title: "Expire Date:",
                                                width: 48,
                                                maxlength: 2,
                                                keyboardPosition: {
                                                    x:118,
                                                    y:139
                                                },
                                                numericalInput:true
                                            }
                                        },
                                        {
                                            type: accedo.ui.label,
                                            css: 'expDateSlash label',
                                            text: '/'
                                        },
                                        {
                                            type:           keyboard,
                                            id:             'expDateInput2',
                                            css:            'inputField expDateInput2',
                                            nextUp:         'ccInput1',
                                            nextDown:       'cvcInput1',
                                            nextLeft:       'expDateInput1',
                                            params:{
                                                title: "Expire Date:",
                                                width: 48,
                                                maxlength: 2,
                                                keyboardPosition: {
                                                    x:118,
                                                    y:139
                                                },
                                                numericalInput:true
                                            }
                                        }
                                        ]
                                    },
                                    {
                                        type: accedo.ui.label,
                                        css:  'labelCVCNum label',
                                        text: 'CVC number'
                                    },
                                    {
                                        type: accedo.ui.label,
                                        css:  'labelDigit label',
                                        text: 'This is a 4 digit<br>number on the back<br>of your card.'
                                    },
                                    {
                                        type: accedo.ui.layout.normal,
                                        css:  'CVC panel horizontal',
                                        children: [
                                        {
                                            type:           keyboard,
                                            id:             'cvcInput1',
                                            css:            'inputField cvcInput1',
                                            nextUp:         'expDateInput1',
                                            nextDown:       'saveChangeButton',
                                            params:{
                                                title: "CVC:",
                                                width: 72,
                                                maxlength: 4,
                                                keyboardPosition: {
                                                    x:118,
                                                    y:139
                                                },
                                                numericalInput:true
                                            }
                                        }
                                        ]
                                    },
                                    {
                                        type: accedo.ui.image,
                                        css:  'splitLine2 image',
                                        src:  'images/module/bpvod/pages/joinNow/steps/splitLine.png'
                                    },
                                    {
                                        type: accedo.ui.label,
                                        css:  'labelEnterCardD2 label',
                                        text: 'By entering my credit card details, I declare that I am at least 18 years of age.'
                                    }
                                    ]
                                }
                                ]
                            },
                            {
                                type: accedo.ui.layout.normal,
                                css:   'buttonPanel panel horizontal',
                                children: [
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'backButton',
                                    css:       'backButton button medium green',
                                    text:      'Back',
                                    width:     '174px',
                                    nextRight: 'saveChangeButton',
                                    nextUp:    'cvcInput1'
                                },
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'saveChangeButton',
                                    css:       'saveChangeButton button medium green',
                                    text:      'Save change',
                                    width:     '174px',
                                    nextLeft:  'backButton',
                                    nextUp:    'cvcInput1'
                                }
                                ]
                            }
                            ]
                        }
                        ]
                    },
                    {
                        type: accedo.ui.layout.normal,
                        id: 'titleText',
                        css: 'titleText'
                    },
                    {
                        type: accedo.ui.layout.normal,
                        id: 'securityIcon',
                        css: 'securityIcon'
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.normal,
                css:  'bigPondHeader',
                children: [
                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: '' 
                }
                ]
            }
            ]
        }
    });