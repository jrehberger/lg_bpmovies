/**
 * @fileOverview searchResultPageView
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.views.searchResultPageView", [
    "accedo.ui.layout.absolute","accedo.ui.label","accedo.ui.image", "accedo.ui.list", "accedo.ui.layout.relative",
    "telstra.sua.module.bpvod.components.fluidButton"],
    function(){
        return {
            type:   accedo.ui.layout.absolute,
            id:     'bpvod_canvas',
            css:    'background2',

            children: [
            {
                type: accedo.ui.layout.absolute,
                css:  'pageStore',

                children: [
                {
                    type: accedo.ui.layout.absolute,
                    css:  'page searchResultPage',

                    children: [
                    {
                        type: accedo.ui.layout.absolute,
                        css:  'panel focus',

                        children:[
                        {
                            type: accedo.ui.layout.absolute,
                            css:  'searchResultPanel verticalStripTable panel',

                            children: [
                            {
                                //List-specific properties
                                type:        accedo.ui.list,
                                css:         'panel vertical',
                                orientation: accedo.ui.list.VERTICAL,
                                //Behaviour: CAROUSEL or REGULAR
                                behaviour:   accedo.ui.list.REGULAR,
                                id:          '#search_result_vertical_listing',
                                visibleSize: 4,
                                preloadSize: 5,
                                scrollStep:  1,
                                loop: true,

                                childTemplate: {
                                    type: accedo.ui.layout.relative,
                                    css:  'dataFieldRow',
                                    children: [
                                    {
                                        type: accedo.ui.layout.relative,
                                        css:  'resultRow',
                                        children: [
                                        {
                                            type:  accedo.ui.label,
                                            id:    'titleInfo',
                                            css:   'titleInfo label',
                                            text:  ''
                                        },
                                        {
                                            type:  accedo.ui.label,
                                            id:    'classInfo',
                                            css:   'classInfo label',
                                            text:  ''
                                        },
                                        {
                                            type:  accedo.ui.label,
                                            id:    'priceInfo',
                                            css:   'priceInfo label',
                                            text:  ''
                                        },
                                        {
                                            type:  telstra.sua.module.bpvod.components.fluidButton,
                                            text:  'OK - more info',
                                            id : 'okButton',
					                        css : 'okButton button rowButton',
					                        width: '154px'
                                        }
                                        ]
                                    }
                                    ]
                                }
                            }
                            ]
                        }
                        ]
                    },
                    {
                        type: accedo.ui.label,
                        css:  'resultTitle label',
                        text: 'Search results for:'
                    },
                    {
                        type: accedo.ui.label,
                        css:  'resultOverlay'
                    },
                    {
                        type: accedo.ui.label,
                        id:   'searchInputWord',
                        css:  'searchInputWord label',
                        text: 'Keyword: '
                    },
                    {
                        type: accedo.ui.label,
                        id:   'searchInputInfo',
                        css:  'searchInputInfo label',
                        text: ''
                    },
                    {
                        type: accedo.ui.label,
                        css:  'searchResultBoard'
                    },
                    {
                        type: accedo.ui.label,
                        id:   'headerMovieTitle',
                        css:  'movieTitle label',
                        text: 'Title'
                    },
                    {
                        type: accedo.ui.label,
                        id:   'headerClassification',
                        css:  'classification label',
                        text: 'Classification'
                    },
                    {
                        type: accedo.ui.label,
                        id:   'headerPrice',
                        css:  'price label',
                        text: 'Price'
                    },
                    {
                        type: accedo.ui.layout.absolute,
                        css:  'upDownScrollerKey',
                        children: [
                        {
                            type: accedo.ui.button,
                            id:   'upScroller',
                            css:  'upDownElement up'
                        },
                        {
                            type: accedo.ui.button,
                            id:   'downScroller',
                            css:  'upDownElement down'
                        }
                        ]
                    },
                    {
                        type: accedo.ui.label,
                        id:   'searchCount',
                        css:  'searchCount label',
                        text: 'Loading...'
                    },
                    {
                        type: accedo.ui.image,
                        css:  'searchRowEdge1 image',
                        src:  'images/module/bpvod/pages/search/rowEdge.png'
                    },
                    {
                        type: accedo.ui.image,
                        id:   'searchRowEdge2',
                        css:  'searchRowEdge2 image',
                        src:  'images/module/bpvod/pages/search/rowEdge.png'
                    },
                    {
                        type: accedo.ui.image,
                        id:   'searchRowEdge3',
                        css:  'searchRowEdge3 image',
                        src:  'images/module/bpvod/pages/search/rowEdge.png'
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.absolute,
                css:  'bigPondHeader',

                children: [

                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: ''
                }
                ]
            }
            ]
        };
    });