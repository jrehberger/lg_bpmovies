/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.views.joinNowStep2PasswordView", [
    "telstra.sua.module.bpvod.views.joinNowTemplateView"],
    function(){
        //var keyboard = (accedo.device.manager.identification.getFirmwareYear() == "2011" ? telstra.sua.core.components.keyboard : telstra.sua.core.components.keyboard_new);
        var keyboard = telstra.sua.core.components.keyboard;

        var viewObj = accedo.utils.object.clone(telstra.sua.module.bpvod.views.joinNowTemplateView({
            pageTitle : "BigPond Movies Password",
            highlightImage2 : {
                type: accedo.ui.image,
                css:  'highlight2 highLight2 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            stepLabel : {
                type: accedo.ui.label,
                id:   'stepTag',
                css:  'step2tag label',
                text: '2'
            },
            backButtonCss : 'backBT button medium',
            continueButtonCss : 'continueBT button medium',
            backText : 'Back',
            continueText : 'Continue',
            corePanel : [
            {
                type: accedo.ui.label,
                css:  'step2label1 label',
                text: 'Create your password (Minimum 6 characters)'
            },
            {
                type:           keyboard,
                id:             'passwordField1',
                css:            'pageInputField passwordInput',
                nextDown:       'passwordField2',
                params:{
                    title: "Password",
                    width: 480,
                    maxlength: 50,
                    isPassword: true
                }
            },
            {
                type: accedo.ui.label,
                css:  'step2label2 label',
                text: 'Confirm password'
            },
            {
                type:           keyboard,
                id:             'passwordField2',
                css:            'pageInputField passwordConfirm',
                nextUp:         'passwordField1',
                nextDown:       'continueButton',
                params:{
                    title: "Confirm Password",
                    width: 480,
                    maxlength: 50,
                    isPassword: true
                }
            }
            ]
        }));

        return viewObj;
    });