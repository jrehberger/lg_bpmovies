/**
 * @fileOverview popupBigPondPlanView
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 */
accedo.define('telstra.sua.module.bpvod.views.popupBigPondPlanView',
    ['accedo.ui.layout.normal',
    'accedo.ui.layout.absolute',
    "accedo.ui.label",
    "accedo.ui.image",
    "telstra.sua.module.bpvod.components.fluidButton"],
    function() {

        var layout  = accedo.ui.layout,
        button      = telstra.sua.module.bpvod.components.fluidButton;

        return {

            type : layout.absolute,
            id : 'popupBg',
            css : 'accedo-ui-popupcontent',
            
            children: [
            {
                type : layout.absolute,
                id : 'popupMain',
                css : 'bigPPPopUp',
                children:[
                    {
                        type : button,
                        id : 'okButton',
                        css : 'okButton button medium focus',
                        text : 'Return',
                        width : '174px'
                    }
                ]
            }
            ]
        }
    });