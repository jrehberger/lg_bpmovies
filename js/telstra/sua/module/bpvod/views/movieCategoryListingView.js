/**
 * @fileOverview movieCategoryListingView
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 *
 * Original author: Ho
 * Date: 2010-08-18
 *
 * document reference:
 * 1) 20100715_BigPond_Movies_on_LG_NetCastTV_1.8.2.pdf    p.61-65
 * 2) BigPondUIandFuncSpec.pdf (20100823 version)    p.14-16
 *
 * Note:
 * 1) now the page would be shared by the TV view also
 */

accedo.define(
    "telstra.sua.module.bpvod.views.movieCategoryListingView",
    [
    "accedo.ui.layout.absolute",
    "accedo.ui.layout.relative",
    "accedo.ui.label",
    "accedo.ui.list",
    "accedo.ui.button",
    "accedo.ui.image",
    "accedo.ui.scrollbar",
    "telstra.sua.core.components.truncatedLabel",
    "telstra.sua.module.bpvod.components.videoPreviewBox"
    ],
    function(){
        return {

            type:   accedo.ui.layout.absolute,
            id:     'bpvod_canvas',
            css:    'background2',

            children: [
            {
                type: accedo.ui.layout.absolute,
                css:  'pageStore',

                children: [
                {
                    id:   'mediaTypeLabel',
                    type: accedo.ui.layout.absolute,
                    css:  'page movieCategoryListingPage nonCarousel force-pos-rel',

                    children: [
                    {
                        type: accedo.ui.layout.absolute,
                        css:  'movieCategoryListPrimePanel panel focus force-pos-rel',

                        children: [
                        {
                            type: accedo.ui.layout.absolute,
                            css:  'categoryPanel verticalStripButtonMenu panel force-pos-rel',

                            children: [
                            {
                                type:        accedo.ui.list,
                                id:          '#movie_category_listing',
                                css:         'panel vertical',
                                orientation: accedo.ui.list.VERTICAL,
                                behaviour:   accedo.ui.list.CAROUSEL,
                                visibleSize: 5,
                                firstSelectedIndex: 2,
                                preloadSize: 5,
                                scrollStep:  1,
                                focusable:   false,
                                childTemplate: {
                                    type:    accedo.ui.button,
                                    css:     'stripButton small',
                                    width:   '395px',
                                    height:  '35px'
                                },
                                widthStyle:  accedo.ui.component.FIT_PARENT,
                                heightStyle: accedo.ui.component.FIT_PARENT,
                                width:       '395px',
                                height:      '224px'
                            },
                            {
                                type: accedo.ui.layout.absolute,
                                css:  'upDownScrollerKey',

                                children: [
                                {
                                    type: accedo.ui.button,
                                    id:   'upScroller',
                                    css:  'upDownElement up'
                                },
                                {
                                    type: accedo.ui.button,
                                    id:   'downScroller',
                                    css:  'upDownElement down'
                                }
                                ]
                            }
                            ]
                        },
                        {
                            type:   accedo.ui.label,
                            id:     'categoryLabel',
                            css:    'categoryTitle label',
                            text:   ''
                        },
                        {
                            type: accedo.ui.layout.absolute,
                            css:  'resultPanel horizontalVideoRollingMenu panel focus',

                            children: [
                            {
                                //List-specific properties
                                type:        accedo.ui.list,
                                css:         'panel horizontal focus l51r51',
                                orientation: accedo.ui.list.HORIZONTAL,
                                behaviour:   accedo.ui.list.REGULAR,
                                id:          '#movie_horizontal_listing',
                                //REGULAR ONLY: How many children to display. -1 means all of them (default).
                                visibleSize: 6,
                                preloadSize: 60,
                                scrollStep:  6,
                                x:           "100px",
                                y:           "-22px",
                                loop: true,
                                                        
                                childTemplate: {
                                    type:   accedo.ui.layout.absolute,
                                    css:     'videoItemImage image',
                                    width:   '181px',
                                    height:  '247px',
                                    children: [
                                    {
                                        type: accedo.ui.image,
                                        id:   'bpvod_videoItemImage',
                                        css:  'pt2w113h160'
                                    },
                                    {
                                        type: accedo.ui.label,
                                        css:  'pt2w113h160 videoItemImageBg'
                                    }
                                    ]
                                },

                                //Other inherited properties
                                widthStyle:  accedo.ui.component.FIT_PARENT,
                                heightStyle: accedo.ui.component.FIT_PARENT,
                                height:      '250px',
                                width:       '100%'
                            },
                            {
                                type: accedo.ui.layout.absolute,
                                css:  'panel horizontal l51r51',
                                children:[
                                {
                                    type: accedo.ui.layout.absolute,
                                    css:  'leftRightScrollerKey',
                                    x:    '-50px',
                                    y:    '35px',
                                    children: [
                                    {
                                        type: accedo.ui.button,
                                        id:   'scrollerKeyLeft',
                                        css:  'leftElement'
                                    },
                                    {
                                        type: accedo.ui.button,
                                        id:   'scrollerKeyRight',
                                        css:  'rightElement'
                                    }
                                    ]
                                }
                                ]
                            }
                            ]
                        }
                        ]
                    },
                    {
                        type: accedo.ui.scrollbar,
                        id:   'hScrollbar',
                        css:  'horizontalScrollbar',
                        trackTopping : false,
                        orientation: accedo.ui.scrollbar.HORIZONTAL
                    },
                    {
                        type: telstra.sua.module.bpvod.components.videoPreviewBox,
                        id: 'videoPreviewBox',
                        css:  'videoPreviewBox panel'
                    },
                    {
                        type: accedo.ui.label,
                        css:  'titleText'
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.absolute,
                css:  'bigPondHeader',

                children: [

                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: ''
                }
                ]
            }
            ]
        };
    }
    );