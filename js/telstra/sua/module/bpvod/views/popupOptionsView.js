/**
 * @fileOverview popupPostcodeView
 * @author <a href="mailto:victor.leung@accedo.tv">Victor Leung</a>
 */
accedo.define('telstra.sua.module.bpvod.views.popupOptionsView',
    ["accedo.ui.layout.normal",
    "accedo.ui.layout.absolute",
    "accedo.ui.label",
    "accedo.ui.image",
    "accedo.ui.button",
    "accedo.ui.list",
    "telstra.sua.module.bpvod.components.fluidButton"],
    function() {

        var label   = accedo.ui.label,
        layout  = accedo.ui.layout;

        return {

            type:layout.absolute,
            id:'popupBg',
            css : 'accedo-ui-popupcontent',

            children: [
            {
                type: layout.absolute,
                id : '#popupOptions',
                
                children:[
                {
                    type : label,
                    id : '#popupOptions_header',
                    text : 'Options:'
                },
                {
                    type:        accedo.ui.list,
                    orientation: accedo.ui.list.VERTICAL,
                    behaviour:   accedo.ui.list.REGULAR,
                    id:          '#popupOptions_optionsList',
                    css:         'optionsList panel vertical',
                    nextUp:      'homeButton',
                    nextDown:    'homeButton',
                    visibleSize: 0,
                    firstSelectedIndex: 0,
                    preloadSize: 6,
                    scrollStep:  1,
                    focusable:   true,
                    childTemplate: {
                        type:    telstra.sua.module.bpvod.components.fluidButton,
                        css:     'stripButton mini',
                        width:   '306px',
                        height:  '44px'
                    }
                },
                {
                    type: accedo.ui.layout.absolute,
                    id: '#popupOptions_buttonPanel',

                    children: [
                    {
                        type: telstra.sua.module.bpvod.components.fluidButton,
                        id: 'homeButton',
                        css: 'button mini',
                        width: '150px',
                        text: 'Home',
                        nextRight: 'closeButton'
                    },
                    {
                        type: telstra.sua.module.bpvod.components.fluidButton,
                        id: 'closeButton',
                        css: 'button mini',
                        width: '150px',
                        text: 'Close',
                        nextLeft: 'homeButton'
                    }
                    ]
                }
                ]
            }
            ]
        }
    });