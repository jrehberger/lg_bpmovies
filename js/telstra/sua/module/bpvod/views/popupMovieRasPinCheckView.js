/**
 * @fileOverview popupFreeMoviesTermsView
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 */
accedo.define('telstra.sua.module.bpvod.views.popupMovieRasPinCheckView',
    ['accedo.ui.layout.linear',
    'accedo.ui.layout.normal',
    "accedo.ui.image",
    "accedo.ui.label",
    "telstra.sua.module.bpvod.components.label",
    "telstra.sua.module.bpvod.components.fluidButton",
    "telstra.sua.core.components.keyboard",
    "telstra.sua.core.components.keyboard_new"],
    function() {
        var label   = accedo.ui.label,
        layout      = accedo.ui.layout,
        image       = accedo.ui.image,
        button      = telstra.sua.module.bpvod.components.fluidButton,
        input       = telstra.sua.core.components.keyboard;
        //input       = (accedo.device.manager.identification.getFirmwareYear() == "2011" ? telstra.sua.core.components.keyboard : telstra.sua.core.components.keyboard_new);
        
        return {

            type:layout.absolute,
            id:'popupBg',
            css : 'accedo-ui-popupcontent',
            children: [
            {
                type: layout.normal,
                id: 'popupMain',
                css: 'popupRasPinCheck panel popup focus',
                children: [
                {
                    type: layout.normal,
                    id:'verticalPanel',
                    css: 'mainPanel panel vertical ',
                    children:[
                    {
                        type : input,
                        id:             'pinField',
                        css:            'inputField pinInputField',
                        nextDown:       'continueButton',
                        
                        params:{
                            title: "PIN",
                            width: 72,
                            maxlength: 4,
                            keyboardPosition: {
                                x:118,
                                y:139
                            },
                            isPassword:true,
                            numericalInput:true
                        }                     
                    },
                    {
                        type:layout.normal,
                        id : 'buttonPanel',
                        css : 'buttonPanel panel horizontal',
                        children: [
                        {
                            type : button,
                            id : 'forgotPin',
                            css: 'forgotButton medium button',
                            text : 'Forgotten PIN',
                            width: '166px',
                            nextUp: 'pinField',
                            nextDown : null,
                            nextLeft : null,
                            nextRight: 'cancelButton'
                        },
                        {
                            type : button,
                            id : 'cancelButton',
                            css : 'cancelButton medium button',
                            text : 'Cancel',
                            width: '166px',
                            nextUp: 'pinField',
                            nextDown : null,
                            nextLeft : 'forgotPin',
                            nextRight: 'continueButton'
                        },
                        {
                            type : button,
                            id : 'continueButton',
                            css : 'continueButton medium button',
                            text : 'Continue',
                            width: '166px',
                            nextUp: 'pinField',
                            nextDown : null,
                            nextLeft : 'cancelButton',
                            nextRight: null
                        }
                        ]
                    }
                    ]
                },
                {
                    type : label,
                    css : 'subtitle label',
                    text : 'You are about to view:'
                },
                {
                    type : label,
                    id : 'videoTitle',
                    css : 'videoTitle label',
                    text : ''
                },
                {
                    type : label,
                    css : 'ageCheckTitle label',
                    text : 'Age Verification Check'
                },
                {
                    type : image,
                    id : 'classificationImage',
                    css : 'classificationImage image',
                    src : ''
                },
                {
                    type : layout.normal,
                    css : 'classificationText truncateLabel',
                    children : [
                    {
                        type : telstra.sua.module.bpvod.components.label,
                        id : 'classificationLabel',
                        css : 'innerLabel',
                        text : ''
                    }
                    ]
                },
                {
                    type : label,
                    css : 'enterPinLabel label',
                    text : 'Enter Account PIN'
                }
                ]
            }
            ]
        }
    });