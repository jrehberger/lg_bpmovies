/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define('telstra.sua.module.bpvod.views.myAccountUpdateCreditCardView',
    ['accedo.ui.layout.normal',
    'accedo.ui.label',
    'telstra.sua.module.bpvod.components.fluidButton'],
    function() {

        return {

            type:   accedo.ui.layout.normal,
            id:     'bpvod_canvas',
            css:    'background2',
            children:       [
            {
                type: accedo.ui.layout.normal,
                css:  'pageStore',
                children: [
                {
                    type: accedo.ui.layout.normal,
                    id:   'accountPageGeneralClass',
                    css:  'accountPageGeneralClass updateCreditCard',
                    children: [
                    
                    {
                        type: accedo.ui.layout.normal,
                        id: 'primePanel',
                        css: 'panal',
                        children: [
                        {
                            type: accedo.ui.layout.normal,
                            id: 'mainPanel',
                            css: 'mainPanel',
                            children: [
                            {
                                type: accedo.ui.layout.normal,
                                id: 'backgroundBorder',
                                css: 'backgroundBorder'
                            },
                            {
                                type: accedo.ui.label,
                                id: 'innerHeader',
                                css: 'innerHeader',
                                text: 'Credit Card Details'
                            },
                            {
                                type: accedo.ui.layout.normal,
                                id: 'componentPanel',
                                css:   'componentPanel',
                                children: [
                                {
                                    type: accedo.ui.layout.normal,
                                    css:   'cardDisplayPanel panel vertical',
                                    children: [
                                    {
                                        type: accedo.ui.label,
                                        css:  'labelVisaDefault label',
                                        text: 'Card Type'
                                    },
                                    {
                                        type:           telstra.sua.core.components.keyboard,
                                        id:             'ccTypeInput1',
                                        css:            'inputField visaInputDefault',
                                        params:{
                                            title: "CreditCard Type:",
                                            width: 182
                                        }
                                    },
                                    {
                                        type: accedo.ui.label,
                                        css:  'labelCreditNumDefault label',
                                        text: 'Credit card number'
                                    },
                                    {
                                        type: accedo.ui.layout.normal,
                                        css: 'ccNoDefault panel horizontal',
                                        children: [
                                        {
                                            type:           telstra.sua.core.components.keyboard,
                                            id:             'ccInput1',
                                            css:            'inputField ccInputDefault1',
                                            params:{
                                                title: "CreditCard Number:",
                                                width: 72,
                                                maxlength: 4,
                                                keyboardPosition: {
                                                    x:118,
                                                    y:139
                                                },
                                                numericalInput:true
                                            }
                                        },
                                        {
                                            type:           telstra.sua.core.components.keyboard,
                                            id:             'ccInput2',
                                            css:            'inputField ccInputDefault2',
                                            params:{
                                                title: "CreditCard Number:",
                                                width: 96,
                                                maxlength: 4,
                                                keyboardPosition: {
                                                    x:118,
                                                    y:139
                                                },
                                                numericalInput:true
                                            }
                                        },
                                        {
                                            type:           telstra.sua.core.components.keyboard,
                                            id:             'ccInput3',
                                            css:            'inputField ccInputDefault3',
                                            params:{
                                                title: "CreditCard Number:",
                                                width: 96,
                                                maxlength: 4,
                                                keyboardPosition: {
                                                    x:118,
                                                    y:139
                                                },
                                                numericalInput:true
                                            }
                                        },
                                        {
                                            type:           telstra.sua.core.components.keyboard,
                                            id:             'ccInput4',
                                            css:            'inputField ccInputDefault4',
                                            params:{
                                                title: "CreditCard Number:",
                                                width: 96,
                                                maxlength: 4,
                                                keyboardPosition: {
                                                    x:118,
                                                    y:139
                                                },
                                                numericalInput:true
                                            }
                                        },
                                        {
                                            type:           telstra.sua.core.components.keyboard,
                                            id:             'ccInput5',
                                            css:            'inputField ccInputDefault5',
                                            params:{
                                                title: "CreditCard Number:",
                                                width: 96,
                                                maxlength: 6,
                                                keyboardPosition: {
                                                    x:118,
                                                    y:139
                                                },
                                                numericalInput:true
                                            }
                                        },
                                        {
                                            type:           telstra.sua.core.components.keyboard,
                                            id:             'ccInput6',
                                            css:            'inputField ccInputDefault6',
                                            params:{
                                                title: "CreditCard Number:",
                                                width: 96,
                                                maxlength: 5,
                                                keyboardPosition: {
                                                    x:118,
                                                    y:139
                                                },
                                                numericalInput:true
                                            }
                                        }
                                        ]
                                    },
                                    {
                                        type: accedo.ui.label,
                                        css:  'labelExpiryDateDefault label',
                                        text: 'Expiry date (MM/YY)'
                                    },
                                    {
                                        type: accedo.ui.layout.normal,
                                        css: 'expDateDefault panel horizontal',
                                        children: [
                                        {
                                            type:           telstra.sua.core.components.keyboard,
                                            id:             'expDateInput1',
                                            css:            'inputField expDateDefaultInput1',
                                            params:{
                                                title: "Expire Date:",
                                                width: 48,
                                                maxlength: 2,
                                                keyboardPosition: {
                                                    x:118,
                                                    y:139
                                                },
                                                numericalInput:true
                                            }
                                        },
                                        {
                                            type: accedo.ui.label,
                                            css: 'expDateDefaultSlash label',
                                            text: '/'
                                        },
                                        {
                                            type:           telstra.sua.core.components.keyboard,
                                            id:             'expDateInput2',
                                            css:            'inputField expDateDefaultInput2',
                                            params:{
                                                title: "CreditCard Number:",
                                                width: 48,
                                                maxlength: 2,
                                                keyboardPosition: {
                                                    x:118,
                                                    y:139
                                                },
                                                numericalInput:true
                                            }
                                        }
                                        ]
                                    },
                                    {
                                        type: accedo.ui.image,
                                        css:  'splitLineDefault image',
                                        src: 'images/module/bpvod/pages/joinNow/steps/splitLine.png'
                                    },
                                    {
                                        type: accedo.ui.label,
                                        css:  'labelCVCDefaultNum label',
                                        text: 'CVC number'
                                    },
                                    {
                                        type: accedo.ui.layout.normal,
                                        css: 'cvcDefaultPanel panel horizontal',
                                        children: [
                                        {
                                            type:           telstra.sua.core.components.keyboard,
                                            id:             'cvcInput1',
                                            css:            'inputField cvcDefaultInput1',
                                            params:{
                                                title: "CVC:",
                                                width: 72,
                                                maxlength: 4,
                                                keyboardPosition: {
                                                    x:118,
                                                    y:139
                                                },
                                                numericalInput:true
                                            }
                                        }
                                        ]
                                    },
                                    {
                                        type: accedo.ui.label,
                                        css:  'labelEnterCardDDefault label',
                                        text: 'By entering my credit card details, I declare that I am at least 18 years of age.'
                                    }
                                    ]
                                }
                                ]
                            },
                            {
                                type: accedo.ui.layout.normal,
                                css:   'buttonPanel panel horizontal',
                                children: [
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'editButton',
                                    css:       'editButton button medium green',
                                    text:      'Edit Detail',
                                    width:     '174px',
                                    nextRight: 'closeButton'
                                },
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'closeButton',
                                    css:       'closeButton button medium green',
                                    text:      'Close',
                                    width:     '174px',
                                    nextLeft:  'editButton'
                                }
                                ]
                            }
                            ]
                        }
                        ]
                    },
                    {
                        type: accedo.ui.layout.normal,
                        id: 'titleText',
                        css: 'titleText'
                    },
                    {
                        type: accedo.ui.layout.normal,
                        id: 'securityIcon',
                        css: 'securityIcon'
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.normal,
                css:  'bigPondHeader',
                children: [
                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: '' 
                }
                ]
            }
            ]
        }
    });