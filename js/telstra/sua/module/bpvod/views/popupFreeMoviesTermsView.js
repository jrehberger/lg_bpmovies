/**
 * @fileOverview popupMovieRasPinCheckView
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 */
accedo.define('telstra.sua.module.bpvod.views.popupFreeMoviesTermsView',
    ['accedo.ui.layout.linear',
    'accedo.ui.layout.normal',
    "accedo.ui.label", "telstra.sua.module.bpvod.components.fluidButton",
    "accedo.ui.image", "accedo.ui.scrollbar"],
    function() {

        var label   = accedo.ui.label,
        layout      = accedo.ui.layout,
        image       = accedo.ui.image,
        button      = telstra.sua.module.bpvod.components.fluidButton,
        scrollbar   = accedo.ui.scrollbar;

        return {

            type:layout.absolute,
            id:'popupBg',
            css : 'accedo-ui-popupcontent',
            children: [

            {

                type: layout.normal,
                id: 'popupMain',
                css: 'popupFreeMovieTerms panel popup focus',
                children: [
            
                {
                    type: label,
                    css : 'subtitle label',
                    text : 'You are about to preview:'
                },
                {
                    type : label,
                    id : 'videoTitle',
                    css : 'videoTitle label',
                    text : ''
                },
                {
                    type: layout.normal,
                    id : 'horizontalPanel',
                    css : 'panel horizontal',
                    children: [
                    {
                        type : layout.normal,
                        id : 'leftPanel',
                        css : 'leftPanel panel',
                        nextDown: null,
                        nextUp : null,
                        nextLeft: null,
                        nextRight: 'watchNow',
                        focusable : true,
                        children:[
                        {
                            type : layout.normal,
                            id: 'textContainer',
                            css : 'textContainer panel',
                            children : [
                            {
                                type : label,
                                css : 'textContent',
                                id : 'textContent',
                                text : ''
                            }
                            ]
                        }
                        ]
                    },
                    {
                        type : layout.normal,
                        id : 'rightPanel',
                        css : 'buttonPanel panel vertical',
                        children:[
                        {
                            type : button,
                            id : 'watchNow',
                            css : 'watchButton green medium button',
                            text : 'I agree - watch now',
                            width: '254px',
                            nextDown: 'cancel',
                            nextUp : null,
                            nextLeft:'leftPanel',
                            nextRight: null
                        },
                        {
                            type : button,
                            id : 'cancel',
                            css : 'cancelButton medium button',
                            text : 'Cancel',
                            width: '254px',
                            nextDown: null,
                            nextUp : 'watchNow',
                            nextLeft:'leftPanel',
                            nextRight: null
                        }
                        ]
                    }
                    ]
                },
                {
                    type : scrollbar,
                    css : "verticalScrollbar",
                    id : "scrollbar",
                    trackTopping : false
                },
                {
                    type : layout.normal,
                    css : "videoItemButton",
                    id : 'videoItemButton',
                    children: [
                    {
                        type : image,
                        css : 'videoItemBorder image',
                        src : 'images/module/bpvod/videoItem_new_unfocus3.png'
                    },
                    {
                        type : layout.normal,
                        css : 'videoItemImageContainer',
                        id : 'videoItemContainer',
                        children : [
                        {
                            type : image,
                            id : 'videoThumbnail',
                            src : '',
                            css : 'videoItemImage image'
                        }
                        ]
                    }
                    ]
                },
                {
                    type : layout.normal,
                    css : 'freeIcon'
                },
                {
                    type : label,
                    css : 'freeIconText label',
                    text : 'FREE'
                },
                {
                    type : label,
                    css : 'freeToWatchLabel label',
                    text : 'FREE to watch'
                },
                {
                    type : label,
                    css : 'freeToWatchLabel2 label',
                    text : 'Download charges may apply'
                }
                ]
            }
            ]
        }
    });