/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.views.joinNowStep4ACardTypeView", [
    "telstra.sua.module.bpvod.views.joinNowTemplateView"],
    function(){

        var viewObj = accedo.utils.object.clone(telstra.sua.module.bpvod.views.joinNowTemplateView({
            pageTitle : "Credit card details",
            highlightImage2 : {
                type: accedo.ui.image,
                css:  'highlight2 highLight2 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage3 : {
                type: accedo.ui.image,
                css:  'highlight3 highLight3 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage4 : {
                type: accedo.ui.image,
                css:  'highlight4pt1 highLight4pt1 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            stepLabel : {
                type: accedo.ui.label,
                id:   'stepTag',
                css:  'step4pt1tag label',
                text: '4'
            },
            backButtonCss : 'backBT button medium',
            continueButtonCss : 'continueBT button medium',
            backText : 'Back',
            continueText : 'Continue',
            corePanel : [
            {
                type: accedo.ui.label,
                css:  'step4pt1label1 label',
                text: 'Please select your credit card type and press Continue.'
            },
            {
                type: accedo.ui.label,
                css:  'step4pt1label2 label',
                text: 'By entering my credit card details, I declare<br>that I am at least 18 years of age.'
            },
            {
                type: accedo.ui.layout.normal,
                css: 'cardchoose panel vertical focus',
                children: [
                {
                    type:      telstra.sua.module.bpvod.components.radioButton,
                    id:        'option1',
                    css:       'option1 button',
                    text:      'Visa',
                    nextDown:  'option2'
                },
                {
                    type:      telstra.sua.module.bpvod.components.radioButton,
                    id:        'option2',
                    css:       'option2 button',
                    text:      'MasterCard',
                    nextUp:    'option1',
                    nextDown:  'option3'
                },
                {
                    type:      telstra.sua.module.bpvod.components.radioButton,
                    id:        'option3',
                    css:       'option3 button',
                    text:      'American Express',
                    nextUp:    'option2',
                    nextDown:  'continueButton'
                }
                ]
            }
            ]
        }));

        return viewObj;
    });