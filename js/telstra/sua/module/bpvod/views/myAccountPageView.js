/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define('telstra.sua.module.bpvod.views.myAccountPageView',
    ['accedo.ui.layout.normal',
    'accedo.ui.label',
    'accedo.ui.button',
    'telstra.sua.core.components.keyboard',
    'telstra.sua.core.components.keyboard_new'],
    function() {
        //var keyboard = (accedo.device.manager.identification.getFirmwareYear() == "2011" ? telstra.sua.core.components.keyboard : telstra.sua.core.components.keyboard_new);
        var keyboard = telstra.sua.core.components.keyboard;

        return {

            type:   accedo.ui.layout.normal,
            id:     'bpvod_canvas',
            css:    'background2',

            children: [
            {
                type: accedo.ui.layout.normal,
                id:   'pageStore',
                css:  'pageStore',
                children: [
                {
                    type: accedo.ui.layout.normal,
                    id: 'myAccountTitle',
                    css: 'myAccountTitle'
                },

                {
                    type: accedo.ui.layout.normal,
                    id: 'myAccountOverlay',
                    css: 'myAccountOverlay',
                    children: [
                    {
                        type: accedo.ui.label,
                        id: 'myAccountSubTitle',
                        css: 'myAccountSubTitle',
                        text: 'Please enter your account PIN to:'
                    },

                    {
                        type: accedo.ui.label,
                        id: 'myAccountContentList1',
                        css: 'myAccountContentList',
                        text: 'View your Purchase History'
                    },

                    {
                        type: accedo.ui.label,
                        id: 'myAccountContentList2',
                        css: 'myAccountContentList',
                        text: 'Redeem a Voucher'
                    },

                    {
                        type: accedo.ui.label,
                        id: 'myAccountContentList3',
                        css: 'myAccountContentList',
                        text: 'Manage your account'
                    },

                    {
                        type: accedo.ui.label,
                        id: 'loginLabel',
                        css: 'loginLabel',
                        text: 'Enter your account PIN:'
                    },

                    {
                        type: accedo.ui.layout.normal,
                        id: 'myAccountPanel',
                        css: 'myAccountPanel',
                        children: [
                        {
                            type:           keyboard,
                            id:             'myAccountInput',
                            css:            'myAccountInput',
                            nextDown:       'okButton',

                            params:{
                                title: "PIN",
                                width: 72,
                                maxlength: 4,
                                keyboardPosition: {
                                    x:118,
                                    y:139
                                },
                                isPassword:true,
                                numericalInput:true
                            }
                        },
                        {
                            type: accedo.ui.layout.normal,
                            css:   'buttonPanel',
                            children: [
                            {
                                type:      telstra.sua.module.bpvod.components.fluidButton,
                                id:        'signOutButton',
                                css:       'signOutButton button medium',
                                text:      'Sign Out',
                                width:     '198px',
                                nextRight: 'okButton',
                                nextUp:    'myAccountInput'
                            },
                            {
                                type:      telstra.sua.module.bpvod.components.fluidButton,
                                id:        'okButton',
                                css:       'okButton button medium',
                                text:      'OK-continue',
                                width:     '198px',
                                nextLeft:  'signOutButton',
                                nextUp:    'myAccountInput'
                            }
                            ]
                        }
                        ]
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.normal,
                css:  'bigPondHeader',
                children: [
                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: ''
                }
                ]
            }
            ]
        }
    });