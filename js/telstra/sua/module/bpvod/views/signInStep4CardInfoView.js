/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.views.signInStep4CardInfoView", [
    "telstra.sua.module.bpvod.views.signInTemplateView"],
    function(){

        var viewObj = accedo.utils.object.clone(telstra.sua.module.bpvod.views.signInTemplateView({
            pageTitle : "Confirm your credit card",
            highlightImage2 : {
                type: accedo.ui.image,
                css:  'highlight2 highLight2 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage3 : {
                type: accedo.ui.image,
                css:  'highlight3 highLight3 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage4 : {
                type: accedo.ui.image,
                css:  'highlight4pt1 highLight4pt1 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            stepLabel : {
                type: accedo.ui.label,
                id:   'stepTag',
                css:  'step4pt1tag label',
                text: '4'
            },
            backButtonCss : 'backBT button medium',
            continueButtonCss : 'continueBT button medium',
            backText : 'Change Card',
            continueText : 'Confirm',
            corePanel : [
            {
                type: accedo.ui.label,
                css:  'CCLabel1 CClabel1 label',
                text: 'The Credit Card associated with your account is:<br>'
            },
            {
                type: accedo.ui.layout.absolute,
                css:  'CCInfoFrame',
                children: [
                {
                    type: accedo.ui.label,
                    css:  'CCtype label',
                    text: 'Credit Card Type:<br>'
                },
                {
                    type: accedo.ui.label,
                    id:   'CCtypeBackend',
                    css:  'CCtypeBackend label',
                    text: ''
                },
                {
                    type: accedo.ui.label,
                    css:  'CCnum label',
                    text: 'Credit Card Number:<br>'
                },
                {
                    type: accedo.ui.label,
                    id:   'CCnumBackend',
                    css:  'CCnumBackend label',
                    text: ''
                },
                {
                    type: accedo.ui.label,
                    css:  'CCExp label',
                    text: 'Expiry date:<br>'
                },
                {
                    type: accedo.ui.label,
                    id:   'CCExpBackend',
                    css:  'CCExpBackend label',
                    text: ''
                },
                {
                    type: accedo.ui.image,
                    css:  'splitLine image',
                    src: 'images/module/bpvod/pages/joinNow/steps/splitLine.png'
                },
                {
                    type: accedo.ui.label,
                    css:  'CCNotice1 label',
                    text: 'Please confirm whether you wish to use this Credit Card, or change your Credit Card:<br>'
                },
                {
                    type: accedo.ui.label,
                    css:  'CCNotice2 label',
                    text: 'By confirming my credit card details, I declare that I am at least 18 years of age.'
                }
                ]
            }
            ]
        }));

        return viewObj;
    });