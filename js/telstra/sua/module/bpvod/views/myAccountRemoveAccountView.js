/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define('telstra.sua.module.bpvod.views.myAccountRemoveAccountView',
    ['accedo.ui.layout.normal',
    'accedo.ui.label',
    'accedo.ui.button',
    'telstra.sua.module.bpvod.components.fluidButton'],
    function() {

        return {

            type:   accedo.ui.layout.normal,
            id:     'bpvod_canvas',
            css:    'background2',
            children:       [
            {
                type: accedo.ui.layout.normal,
                css:  'pageStore',
                children: [
                {
                    type: accedo.ui.layout.normal,
                    id:   'accountPageGeneralClass',
                    css:  'accountPageGeneralClass removeAccount',
                    children: [
                    {
                        type: accedo.ui.layout.normal,
                        id: 'primePanel',
                        css: 'panal',
                        children: [
                        {
                            type: accedo.ui.layout.normal,
                            id: 'mainPanel',
                            css: 'mainPanel',
                            children: [
                            {
                                type: accedo.ui.layout.normal,
                                id: 'backgroundBorder',
                                css: 'backgroundBorder'
                            },
                            {
                                type: accedo.ui.label,
                                id: 'innerHeader',
                                css: 'innerHeader',
                                text: 'Delete Account information from my Device'
                            },
                            {
                                type: accedo.ui.label,
                                id: 'subtitle',
                                css: 'subtitle',
                                text: 'Deleting your Account information will erase your details and purchase history from this Device and sign you out.'
                            },
                            {
                                type: accedo.ui.label,
                                id: 'minorLabel',
                                css: 'minorLabel',
                                text: 'Your BigPond Movies Downloads Account will still be valid. You can Sign In again from the main menu to reinstate your Account information.'
                            },
                            {
                                type: accedo.ui.layout.normal,
                                css:   'buttonPanel',
                                children: [
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'deleteButton',
                                    css:       'deleteButton button medium',
                                    text:      'Delete',
                                    width:     '174px',
                                    nextRight: 'closeButton'
                                },
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'closeButton',
                                    css:       'closeButton button medium',
                                    text:      'Close',
                                    width:     '174px',
                                    nextLeft:  'deleteButton'
                                }
                                ]
                            }
                            ]
                        }
                        ]
                    },
                    {
                        type: accedo.ui.layout.normal,
                        id: 'titleText',
                        css: 'titleText'
                    },
                    {
                        type: accedo.ui.layout.normal,
                        id: 'securityIcon',
                        css: 'securityIcon'
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.normal,
                css:  'bigPondHeader',
                children: [
                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: '' 
                }
                ]
            }
            ]
        }
    });