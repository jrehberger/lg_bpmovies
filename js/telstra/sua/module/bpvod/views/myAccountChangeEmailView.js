/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define('telstra.sua.module.bpvod.views.myAccountChangeEmailView',
    ['accedo.ui.layout.normal',
    'accedo.ui.label',
    'telstra.sua.module.bpvod.components.fluidButton',
    'telstra.sua.core.components.keyboard',
    'telstra.sua.core.components.keyboard_new'],
    function() {
        //var keyboard = (accedo.device.manager.identification.getFirmwareYear() == "2011" ? telstra.sua.core.components.keyboard : telstra.sua.core.components.keyboard_new);
        var keyboard = telstra.sua.core.components.keyboard;

        return {

            type:   accedo.ui.layout.normal,
            id:     'bpvod_canvas',
            css:    'background2',
            children:       [
            {
                type: accedo.ui.layout.normal,
                css:  'pageStore',
                children: [
                {
                    type: accedo.ui.layout.normal,
                    id:   'accountPageGeneralClass',
                    css:  'accountPageGeneralClass changeEmail',
                    children: [
                    {
                        type: accedo.ui.layout.normal,
                        id: 'primePanel',
                        css: 'panal',
                        children: [
                        {
                            type: accedo.ui.layout.normal,
                            id: 'mainPanel',
                            css: 'mainPanel',
                            children: [
                            {
                                type: accedo.ui.layout.normal,
                                id: 'backgroundBorder',
                                css: 'backgroundBorder'
                            },
                            {
                                type: accedo.ui.label,
                                id: 'innerHeader',
                                css: 'innerHeader',
                                text: 'Email'
                            },
                            {
                                type: accedo.ui.layout.normal,
                                id: 'componentPanel',
                                css: 'componentPanel',
                                children: [
                                {
                                    type: accedo.ui.label,
                                    id: 'textLabel1',
                                    css: 'textLabel',
                                    text: 'Contact email address/username'
                                },
                                {
                                    type:           keyboard,
                                    id:             'emailField1',
                                    css:            'inputField',
                                    nextDown:       'emailField2',
                                    
                                    params:{
                                        title: "Email Address",
                                        width: 480,
                                        maxlength: 40
                                    } 
                                },
                                {
                                    type: accedo.ui.label,
                                    id: 'textLabel2',
                                    css: 'textLabel',
                                    text: 'Confirm your email address'
                                },
                                
                                {
                                    type:           keyboard,
                                    id:             'emailField2',
                                    css:            'inputField',
                                    nextDown:       'editDetailsButton',
                                    nextUp:         'emailField1',
                                    
                                    params:{
                                        title: "Email Address",
                                        width: 480,
                                        maxlength: 40
                                    } 
                                }
                                ]
                            },
                            {
                                type: accedo.ui.label,
                                id: 'minorLabel',
                                css: 'minorLabel',
                                text: 'Your email address is your username for logging into BigPond Movies.'
                            },
                            {
                                type: accedo.ui.layout.normal,
                                css:   'buttonPanel',
                                children: [
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'editDetailsButton',
                                    css:       'editDetailsButton button medium',
                                    text:      'Edit Details',
                                    width:     '174px',
                                    nextRight: 'closeButton'
                                },
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'closeButton',
                                    css:       'closeButton button medium',
                                    text:      'Close',
                                    width:     '174px',
                                    nextLeft:  'editDetailsButton'
                                }
                                ]
                            }
                            ]
                        }
                        ]
                    },
                    {
                        type: accedo.ui.layout.normal,
                        id: 'titleText',
                        css: 'titleText'
                    },
                    {
                        type: accedo.ui.layout.normal,
                        id: 'securityIcon',
                        css: 'securityIcon'
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.normal,
                css:  'bigPondHeader',
                children: [
                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: ''
                }
                ]
            }
            ]
        }
    });