/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define('telstra.sua.module.bpvod.views.myAccountChangeTVNameView',
    ['accedo.device.manager',
    'telstra.sua.module.bpvod.utils',
    'accedo.ui.layout.normal',
    'accedo.ui.label',
    'telstra.sua.module.bpvod.utils',
    'telstra.sua.module.bpvod.components.fluidButton',
    'telstra.sua.core.components.keyboard',
    'telstra.sua.core.components.keyboard_new'],
    function() {
        //var keyboard = (accedo.device.manager.identification.getFirmwareYear() == "2011" ? telstra.sua.core.components.keyboard : telstra.sua.core.components.keyboard_new);
        var keyboard = telstra.sua.core.components.keyboard;

        return {

            type:   accedo.ui.layout.normal,
            id:     'bpvod_canvas',
            css:    'background2',
            children:       [
            {
                type: accedo.ui.layout.normal,
                css:  'pageStore',
                children: [
                {
                    type: accedo.ui.layout.normal,
                    id:   'accountPageGeneralClass',
                    css:  'accountPageGeneralClass changeTVName',
                    children: [
                    {
                        type: accedo.ui.layout.normal,
                        id: 'primePanel',
                        css: 'panal',
                        children: [
                        {
                            type: accedo.ui.layout.normal,
                            id: 'mainPanel',
                            css: 'mainPanel',
                            children: [
                            {
                                type: accedo.ui.layout.normal,
                                id: 'backgroundBorder',
                                css: 'backgroundBorder'
                            },
                            {
                                type: accedo.ui.label,
                                id: 'innerHeader',
                                css: 'innerHeader',
                                text: accedo.device.manager.identification.getDeviceType() + ' Name'
                            },
                            {
                                type: accedo.ui.layout.normal,
                                id: 'componentPanel',
                                css: 'componentPanel',
                                children: [
                                {
                                    type: accedo.ui.label,
                                    id: 'textLabel',
                                    css: 'textLabel',
                                    text: accedo.device.manager.identification.getDeviceType() + ' Name'
                                },
                                {
                                    type: keyboard,
                                    id: 'inputField',
                                    css: 'inputField',
                                    nextDown:  'editDetailsButton',
                                    
                                    params:{
                                        title: accedo.device.manager.identification.getDeviceType() + " Name",
                                        width: 400,
                                        maxlength: 50
                                    } 
                                }]
                            },
                            {
                                type: accedo.ui.label,
                                id: 'minorLabel',
                                css: 'minorLabel',
                                text: 'To make it easier to identify purchases made from this ' + accedo.device.manager.identification.getDeviceType() + ' in your account, you can edit the ' + accedo.device.manager.identification.getDeviceType() + ' name below.'
                            },
                            {
                                type: accedo.ui.layout.normal,
                                css:   'buttonPanel',
                                children: [
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'editDetailsButton',
                                    css:       'editDetailsButton button medium',
                                    text:      'Edit Details',
                                    width:     '174px',
                                    nextRight: 'closeButton'
                                },
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'closeButton',
                                    css:       'closeButton button medium',
                                    text:      'Close',
                                    width:     '174px',
                                    nextLeft:  'editDetailsButton'
                                }
                                ]
                            }
                            ]
                        }
                        ]
                    },
                    {
                        type: accedo.ui.layout.normal,
                        id: 'titleText',
                        css: 'titleText'
                    },
                    {
                        type: accedo.ui.layout.normal,
                        id: 'securityIcon',
                        css: 'securityIcon'
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.normal,
                css:  'bigPondHeader',
                children: [
                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: '' 
                }
                ]
            }
            ]
        }
    });