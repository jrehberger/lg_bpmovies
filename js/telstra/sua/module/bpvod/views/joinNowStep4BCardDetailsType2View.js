/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.views.joinNowStep4BCardDetailsType2View", [
    "telstra.sua.module.bpvod.views.joinNowTemplateView"],
    function(){

        var viewObj = accedo.utils.object.clone(telstra.sua.module.bpvod.views.joinNowTemplateView({
            pageTitle : "Credit card details",
            highlightImage2 : {
                type: accedo.ui.image,
                css:  'highlight2 highLight2 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage3 : {
                type: accedo.ui.image,
                css:  'highlight3 highLight3 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage4 : {
                type: accedo.ui.image,
                css:  'highlight4pt1 highLight4pt1 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            stepLabel : {
                type: accedo.ui.label,
                id:   'stepTag',
                css:  'step4pt1tag label',
                text: '4'
            },
            backButtonCss : 'backBT button medium',
            continueButtonCss : 'continueBT button medium',
            backText : 'Back',
            continueText : 'Continue',
            corePanel : [
            {
                type: accedo.ui.label,
                css:  'step4pt2label1 label',
                text: 'Credit card number'
            },
            {
                type: accedo.ui.image,
                css:  'splitLine image',
                src:  'images/module/bpvod/pages/joinNow/steps/splitLine.png'
            },
            {
                type: accedo.ui.layout.normal,
                css: 'ccNo panel horizontal',
                children: [
                {
                    type:           telstra.sua.core.components.keyboard,
                    id:             'ccInput1',
                    css:            'inputField ccInput1',
                    nextDown:       'expDateInput1',
                    nextRight:      'ccInput2',
                    params:{
                        title: "CreditCard Number:",
                        width: 96,
                        maxlength: 4,
                        keyboardPosition: {
                            x:118,
                            y:139
                        },
                        numericalInput:true
                    }
                },
                {
                    type:           telstra.sua.core.components.keyboard,
                    id:             'ccInput2',
                    css:            'inputField ccInput2',
                    nextDown:       'expDateInput1',
                    nextLeft:       'ccInput1',
                    nextRight:      'ccInput3',
                    params:{
                        title: "CreditCard Number:",
                        width: 96,
                        maxlength: 6,
                        keyboardPosition: {
                            x:118,
                            y:139
                        },
                        numericalInput:true
                    }
                },
                {
                    type:           telstra.sua.core.components.keyboard,
                    id:             'ccInput3',
                    css:            'inputField ccInput3',
                    nextDown:       'expDateInput1',
                    nextLeft:       'ccInput2',
                    params:{
                        title: "CreditCard Number:",
                        width: 96,
                        maxlength: 6,
                        keyboardPosition: {
                            x:118,
                            y:139
                        },
                        numericalInput:true
                    }
                }
                ]
            },
            {
                type: accedo.ui.image,
                css:  'creditCard image',
                src:  'images/module/bpvod/pages/joinNow/steps/amex.png'
            },
            {
                type: accedo.ui.label,
                css:  'step4pt2label2 label',
                text: 'Expiry date'
            },
            {
                type: accedo.ui.layout.normal,
                css:  'expDate panel horizontal',
                children: [
                {
                    type:           telstra.sua.core.components.keyboard,
                    id:             'expDateInput1',
                    css:            'inputField expDateInput1',
                    nextUp:         'ccInput1',
                    nextDown:       'cvcInput1',
                    nextRight:      'expDateInput2',
                    params:{
                        title: "Expire Date:",
                        width: 48,
                        maxlength: 2,
                        keyboardPosition: {
                            x:118,
                            y:139
                        },
                        numericalInput:true
                    }
                },
                {
                    type: accedo.ui.label,
                    css: 'expDateSlash label',
                    text: '/'
                },
                {
                    type:           telstra.sua.core.components.keyboard,
                    id:             'expDateInput2',
                    css:            'inputField expDateInput2',
                    nextUp:         'ccInput1',
                    nextDown:       'cvcInput1',
                    nextLeft:       'expDateInput1',
                    params:{
                        title: "Expire Date:",
                        width: 48,
                        maxlength: 2,
                        keyboardPosition: {
                            x:118,
                            y:139
                        },
                        numericalInput:true
                    }
                }
                ]
            },
            {
                type: accedo.ui.label,
                css:  'step4pt2label3 label',
                text: 'CVC number'
            },
            {
                type: accedo.ui.label,
                css:  'step4pt2label4 label',
                text: 'This is a 4 digit<br>number on the front<br>of your card.'
            },
            {
                type: accedo.ui.layout.normal,
                css:  'CVC panel horizontal',
                children: [
                {
                    type:           telstra.sua.core.components.keyboard,
                    id:             'cvcInput1',
                    css:            'inputField cvcInput1',
                    nextUp:         'expDateInput1',
                    nextDown:       'continueButton',
                    params:{
                        title: "CVC:",
                        width: 72,
                        maxlength: 4,
                        keyboardPosition: {
                            x:118,
                            y:139
                        },
                        numericalInput:true
                    }
                }
                ]
            },
            {
                type: accedo.ui.label,
                css:  'step4pt2label5 label',
                text: 'By entering my credit card details, I declare<br>that I am at least 18 years of age.'
            },
            ]
        }));

        return viewObj;
    });