/**
 * @fileOverview
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 */
accedo.define('telstra.sua.module.bpvod.views.purchasePageView',
    ['accedo.ui.layout.linear',
    'accedo.ui.layout.normal',
    'accedo.ui.label',
    'accedo.ui.button',
    'telstra.sua.module.bpvod.components.label',
    'telstra.sua.core.components.keyboard',
'telstra.sua.core.components.keyboard_new'],
    function() {

        var label = telstra.sua.module.bpvod.components.label;
        //var keyboard = (accedo.device.manager.identification.getFirmwareYear() == "2011" ? telstra.sua.core.components.keyboard : telstra.sua.core.components.keyboard_new);
        var keyboard = telstra.sua.core.components.keyboard;

        return {
            type:   accedo.ui.layout.normal,
            id:     '#bpvod_canvas',
            css:    'background2',

            children: [
            {
                type: accedo.ui.layout.normal,
                id: 'pageStore',
                css:  'pageStore',
                children: [
                {
                    type: accedo.ui.layout.normal,
                    id:   'purchasePage',
                    css:  'page purchasePage',
                    children: [
                    {
                        type: accedo.ui.layout.normal,
                        id: 'mainPanel',
                        css:  'panel focus',
                        children: [
                        {
                            type: accedo.ui.layout.normal,
                            id: 'ageVerificationPanel',
                            css:  'purchasePinPanel panel vertical',
                            children: [
                            {
                                type: accedo.ui.layout.normal,
                                id: 'ageVerificationInputPanel',
                                css:  'purchasePinInputPanel panel vertical',
                                children: [
                                {
                                    type:           keyboard,
                                    id:             'pinField2',
                                    css:            'inputField labelAccountPinInput2',
                                    nextDown:       'okConfirmButton2',
                                   
                                    params:{
                                        title: "PIN",
                                        width: 72,
                                        maxlength: 4,
                                        keyboardPosition: {
                                            x:118,
                                            y:139
                                        },
                                        isPassword:true,
                                        numericalInput:true
                                    }
                                }
                                ]
                            },
                            {
                                type:   accedo.ui.label,
                                css:    'labelRent2 label',
                                text:   'You are about to rent:'
                            },
                            {
                                
                                type:   accedo.ui.label,
                                id:     'movieTitle2',
                                css:    'labelTitle2',
                                text:   ''
                            },
                            {
                                type:   accedo.ui.label,
                                css:    'labelAgeCheck label',
                                text:   'Age Verification Check'
                            },
                            {
                                type:   accedo.ui.image,
                                css:    'imgClassification2 image',
                                id:     'classificationImage2',
                                src:    ''
                            },
                            {
                                type: label,
                                id:   'classificationCommentLabel2',
                                css:  'labelRestrictadvice',
                                text:   ''
                            },
                            {
                                type:   accedo.ui.label,
                                css:    'labelAccountPin2 label',
                                text:   'Enter Account PIN:'
                            },
                            {
                                type: accedo.ui.layout.normal,
                                id: 'purchaseButtonPanel',
                                css:  'purchaseButtonPanel panel horizontal',
                                children: [
                                {
                                    type:           telstra.sua.module.bpvod.components.fluidButton,
                                    heightStyle:    accedo.ui.component.WRAP_CONTENT,
                                    text:           'Forgotten PIN',
                                    id:             'forgottenPinButton2',
                                    css:            'forgetPinButton medium green button',
                                    width:          '170px',
                                    nextUp:         'pinField2',
                                    nextDown:       null,
                                    nextLeft:       null,
                                    nextRight:      'cancelPinButton2'

                                },
                                {
                                    type:           telstra.sua.module.bpvod.components.fluidButton,
                                    heightStyle:    accedo.ui.component.WRAP_CONTENT,
                                    text:           'Cancel',
                                    id:             'cancelPinButton2',
                                    css:            'cancelPinButton medium green button',
                                    width:          '170px',
                                    nextUp:         'pinField2',
                                    nextDown:       null,
                                    nextLeft:       'forgottenPinButton2',
                                    nextRight:      'okConfirmButton2'
                                },
                                {
                                    type:           telstra.sua.module.bpvod.components.fluidButton,
                                    heightStyle:    accedo.ui.component.WRAP_CONTENT,
                                    text:           'OK - Confirm',
                                    id:             'okConfirmButton2',
                                    css:            'okPinButton medium green button',
                                    width:          '170px',
                                    nextUp:         'pinField2',
                                    nextDown:       null,
                                    nextLeft:       'cancelPinButton2',
                                    nextRight:      null
                                }
                                ]
                            }
                            ]
                        },
                        {
                            type: accedo.ui.layout.normal,
                            id:   'mainVerticalPanel',
                            css:  'mainVerticalPanel panel vertical',
                            children: [
                            {
                                type: accedo.ui.layout.normal,
                                css:  'titlePanel panel horizontal',
                                children: [
                                {
                                    type:   accedo.ui.label,
                                    css:    'labelRent label',
                                    text:   'You are about to rent:'
                                },
                                {
                                    
                                    type:   accedo.ui.label,
                                    id:     'movieTitle',
                                    css:    'labelTitle',
                                    text:   ''
                                }
                                ]
                            },
                            {
                                type: accedo.ui.layout.normal,
                                id:   'pinPanel',
                                css:  'pinPanel panel vertical',
                                children: [
                                {
                                    type:   accedo.ui.label,
                                    css:    'labelAccountPin1 label',
                                    text:   'Enter Account PIN:'
                                },
                                {
                                    type:           keyboard,
                                    id:             'pinField',
                                    css:            'inputField labelAccountPinInput1',
                                    nextDown:       'okConfirmButton',
                                    
                                    params:{
                                        title: "PIN",
                                        width: 72,
                                        maxlength: 4,
                                        keyboardPosition: {
                                            x:118,
                                            y:139
                                        },
                                        isPassword:true,
                                        numericalInput:true
                                    } 
                                },
                                {
                                    type: accedo.ui.layout.normal,
                                    id:  'pinButtonPanel',
                                    css:  'pinButtonPanel panel horizontal',
                                    children: [
                                    {
                                        type:           telstra.sua.module.bpvod.components.fluidButton,
                                        heightStyle:    accedo.ui.component.WRAP_CONTENT,
                                        text:           'Forgotten PIN',
                                        id:             'forgottenPinButton',
                                        css:            'forgetConfirmPinButton medium green button',
                                        width:          '182px',
                                        nextUp:         'pinField',
                                        nextDown:       null,
                                        nextLeft:       null,
                                        nextRight:      'cancelPinButton'
                                    },
                                    {
                                        type:           telstra.sua.module.bpvod.components.fluidButton,
                                        heightStyle:    accedo.ui.component.WRAP_CONTENT,
                                        text:           'Cancel',
                                        id:             'cancelPinButton',
                                        css:            'cancelConfirmPinButton medium green button',
                                        width:          '182px',
                                        nextUp:         'pinField',
                                        nextDown:       null,
                                        nextLeft:       'forgottenPinButton',
                                        nextRight:      'okConfirmButton'
                                    },
                                    {
                                        type:           telstra.sua.module.bpvod.components.fluidButton,
                                        heightStyle:    accedo.ui.component.WRAP_CONTENT,
                                        text:           'OK - Confirm',
                                        id:             'okConfirmButton',
                                        css:            'okConfirmPinButton medium green button',
                                        width:          '182px',
                                        nextUp:         'pinField',
                                        nextDown:       null,
                                        nextLeft:       'cancelPinButton',
                                        nextRight:      null
                                    }
                                    ]
                                }
                                ]
                            },
                            {
                                type: accedo.ui.layout.normal,
                                id: 'middlePanel',
                                css:  'middlePanel panel horizontal',
                                children: [
                                {
                                    type:   accedo.ui.image,
                                    css:    'imgClassification classification image',
                                    id:     'classificationImage',
                                    src:    ''
                                },
                                {
                                    type: label,
                                    id: 'classificationCommentLabel',
                                    css: 'labelConsumeradvice',
                                    text:   ''
                                },
                                {
                                    type:           telstra.sua.module.bpvod.components.fluidButton,
                                    heightStyle:    accedo.ui.component.WRAP_CONTENT,
                                    text:           'OK - Purchase',
                                    id:             'purchaseButton',
                                    css:            'okPurchaseButton medium green button',
                                    width:          '186px',
                                    nextUp:         null,
                                    nextDown:       'redeemButton',
                                    nextLeft:       null,
                                    nextRight:      null
                                }
                                ]
                            },
                            {
                                type: accedo.ui.layout.normal,
                                id:'bottomPanel',
                                css:  'bottomPanel panel horizontal',
                                children: [
                                {
                                    type: accedo.ui.layout.normal,
                                    id:'bottomPanelLeft',
                                    css:  'bottomLeftPanel panel vertical',
                                    children: [
                                    {
                                        type: accedo.ui.layout.normal,
                                        id: 'pricePanelHorizontal',
                                        css:  'pricePanel panel horizontal',
                                        children: [
                                        {
                                            type:   accedo.ui.image,
                                            css:    'imgPrice Price image',
                                            src:    'images/module/bpvod/pages/SS-MS-4_0_PurchaseOverlayConfirmRental/icon2_new181010.png'
                                        },
                                        {
                                            type:   accedo.ui.label,
                                            css:    'labelPriceTitle label',
                                            text:   'Rent Price'
                                        },
                                        {
                                            type:   accedo.ui.label,
                                            id:     'priceInt',
                                            css:    'labelPriceInt label',
                                            text:   ''
                                        },
                                        {
                                            type:   accedo.ui.label,
                                            id:     'priceDeci',
                                            css:    'labelPriceRem label',
                                            text:   ''
                                        },
                                        {
                                            type:   accedo.ui.label,
                                            css:    'labelPriceWarning label',
                                            text:   '<sup>*</sup>Download charges may apply. Unmetered for most BigPond customers.'
                                        }
                                        ]
                                    },

                                    {
                                        type:   accedo.ui.image,
                                        css:    'imgLine1 image',
                                        src:    'images/module/bpvod/pages/SS-MS-4_0_PurchaseOverlayConfirmRental/popup_line01.png'
                                    },

                                    {
                                        type: accedo.ui.layout.normal,
                                        css:  'timePanel panel horizontal',
                                        children: [
                                        {
                                            type:   accedo.ui.image,
                                            css:    'imgTime Time image',
                                            src:    'images/module/bpvod/pages/SS-MS-4_0_PurchaseOverlayConfirmRental/icon3.png'
                                        },
                                        {
                                            type:   accedo.ui.label,
                                            css:    'labelWatchNow label',
                                            text:   'Watch now for:'
                                        },
                                        {
                                            type:   accedo.ui.label,
                                            css:    'labelWatchLater label',
                                            text:   'Watch later within:'
                                        },

                                        {
                                            type: accedo.ui.layout.normal,
                                            id: 'bottomWatchdataPanel',
                                            css:  'bottomWatchdataPanel panel vertical',
                                            children: [
                                            {
                                                type:   accedo.ui.label,
                                                id:     'watchNow',
                                                css:    'labelWatchViewingPeriod label',
                                                text:   ''
                                            },
                                            {
                                                type:   accedo.ui.label,
                                                id:     'watchLater',
                                                css:    'labelWatchDeletionPeriod label',
                                                text:   ''
                                            }
                                            ]
                                        }
                                        ]
                                    },

                                    {
                                        type:   accedo.ui.image,
                                        css:    'imgLine2 image',
                                        src:    'images/module/bpvod/pages/SS-MS-4_0_PurchaseOverlayConfirmRental/popup_line01.png'
                                    },

                                    {
                                        type: accedo.ui.layout.normal,
                                        id : 'paymentMethodPanel',
                                        css:  'paymentMethodPanel panel horizontal',
                                        children: [
                                        {
                                            type:   accedo.ui.image,
                                            css:    'imgPayment Payment image',
                                            src:    'images/module/bpvod/pages/SS-MS-4_0_PurchaseOverlayConfirmRental/icon4_new181010.png'
                                        },
                                        {
                                            type:   accedo.ui.label,
                                            css:    'labelPaymentMethod label',
                                            text:   'Payment Method'
                                        },
                                        {
                                            type:   accedo.ui.label,
                                            id:     'creditCard',
                                            css:    'labelCreditCard label',
                                            text:   ''
                                        },
                                        {
                                            type:   accedo.ui.label,
                                            id:     'creditdeCard',
                                            css:    'labelCreditdeCard label',
                                            text:   ''
                                        },
                                        {
                                            type:   accedo.ui.label,
                                            id:     'gstInfo',
                                            css:    'labelGstdeducted label',
                                            text:   ''
                                        },
                                        {
                                            type:   accedo.ui.label,
                                            id:     'voucherCredit',
                                            css:    'labelVouncherCredit label',
                                            text:   ''
                                        },
                                        {
                                            type:   accedo.ui.label,
                                            id:     'gstCreditDeducted',
                                            css:    'labelGstCreditdeducted label',
                                            text:   ''
                                        },
                                        {
                                            type:   accedo.ui.label,
                                            id:     'remainVoucher',
                                            css:    'labelRemainVoucherCredit label',
                                            text:   ''
                                        }
                                        ]

                                    }
                                    ]
                                },
                                {
                                    type: accedo.ui.layout.normal,
                                    id: 'bottomRightPanel',
                                    css:  'bottomRightPanel panel vertical',
                                    children: [
                                    {
                                        type:           telstra.sua.module.bpvod.components.fluidButton,
                                        heightStyle:    accedo.ui.component.WRAP_CONTENT,
                                        text:           'Redeem voucher',
                                        id:             'redeemButton',
                                        css:            'redeemVoucherButton medium green button',
                                        width:          '186px',
                                        nextUp:         'purchaseButton',
                                        nextDown:       'cancelButton',
                                        nextLeft:       null,
                                        nextRight:      null
                                    },
                                    {
                                        type:           telstra.sua.module.bpvod.components.fluidButton,
                                        heightStyle:    accedo.ui.component.WRAP_CONTENT,
                                        text:           'Cancel',
                                        id:             'cancelButton',
                                        css:            'cancelPurchaseButton medium green button',
                                        width:          '186px',
                                        nextUp:         'redeemButton',
                                        nextDown:       null,
                                        nextLeft:       null,
                                        nextRight:      null
                                    },
                                    {
                                        type: accedo.ui.layout.normal,
                                        id : 'videoThumbnailPanel',
                                        css:  'warningPanel panel vertical',
                                        children: [
                                        {
                                            type: accedo.ui.layout.normal,
                                            css:  'videoThumbnailBg'
                                        },
                                        {
                                            type:   accedo.ui.image,
                                            css:    'videoThumbnailBorder image pt2w128h94',
                                            src:    'images/module/bpvod/videoItem_new_unfocus3.png'
                                        },
                                        {
                                            type:   accedo.ui.image,
                                            id:     'videoThumbnail',
                                            css:    'videoThumbnail',
                                            src:    ''
                                        },
                                        {
                                            type:   accedo.ui.label,
                                            id:     'episodeLabel',
                                            css:    'episodeLabel label',
                                            text:   ''
                                        }
                                        ]
                                    }
                                    ]
                                }
                                ]
                            }
                            ]
                        },
                        {
                            type: accedo.ui.layout.normal,
                            id: 'purchaseSuccessPanel',
                            css:  'purchaseSuccessPanel panel vertical',
                            children: [
                            {
                                type: accedo.ui.layout.normal,
                                css:  'purchaseSuccessTitlePanel panel horizontal',
                                children: [
                                {
                                    type:   accedo.ui.label,
                                    css:    'labelPurchaseSComplete label',
                                    text:   'Purchase complete'
                                }
                                ]
                            },
                            {
                                type: accedo.ui.layout.normal,
                                css:  'purchaseSuccessBottomPanel panel horizontal',
                                children: [
                                {
                                    type: accedo.ui.layout.normal,
                                    css:  'purchaseSuccessVP1Panel panel vertical',
                                    children: [
                                    {
                                        type: accedo.ui.layout.normal,
                                        css:  'purchaseSuccessWarnPanel panel vertical',
                                        children: [
                                        {
                                            type:   accedo.ui.image,
                                            css:    'videoItemBorder2 image pt2w128h94',
                                            src:    'images/module/bpvod/videoItem_new_unfocus3.png'
                                        },
                                        {
                                            type: accedo.ui.layout.normal,
                                            css:  'videoThumbnailBorder2',
                                            children: [
                                            {
                                                type: accedo.ui.layout.normal,
                                                css:  'videoThumbnailBg'
                                            },
                                            {
                                                type:   accedo.ui.image,
                                                id:     'videoThumbnail2',
                                                css:    'videoThumbnail2',
                                                src:    ''
                                            },
                                            {
                                                type:   accedo.ui.label,
                                                id:     'episodeLabel2',
                                                css:    'episodeLabel label',
                                                text:   ''
                                            }
                                            ]
                                        }
                                        ]
                                    },
                                    {
                                        type:   accedo.ui.label,
                                        css:    'labelPurchaseSThanks label',
                                        text:   'Thank you for renting'
                                    },
                                    {
                                        type:           label,
                                        id:             'movieTitle3',
                                        css:            'labelPurchaseSTitle labelPurchaseSTitleOuter truncateLabel',
                                        innerLabel_css: 'labelPurchaseSTitleInner innerLabel',
                                        text:           ''
                                    },
                                    {
                                        type:   accedo.ui.label,
                                        css:    'labelPurchaseSEmail label',
                                        text:   'You have been sent an email receipt to your nominated email address.'
                                    },
                                    {
                                        type:   accedo.ui.label,
                                        id:     'transactionNo',
                                        css:    'labelPurchaseSTranNo label',
                                        text:   'Transaction no.'
                                    }
                                    ]
                                },
                                {
                                    type: accedo.ui.layout.normal,
                                    css:  'purchaseSuccessVP2Panel panel vertical',
                                    children: [
                                    {
                                        type:   accedo.ui.label,
                                        css:    'labelPurchaseViewing label',
                                        text:   'Viewing Details'
                                    },
                                    {
                                        type:   accedo.ui.image,
                                        css:    'imgbox image',
                                        src:    'images/module/bpvod/pages/SS-MS-4_2_PurchaseOverlayPurchaseCompleteMessage/popup_bar01.png'
                                    },
                                    {
                                        type:   accedo.ui.image,
                                        css:    'imgbox2 image',
                                        src:    'images/module/bpvod/pages/SS-MS-4_2_PurchaseOverlayPurchaseCompleteMessage/popup_bar01.png'
                                    },
                                    {
                                        type:   accedo.ui.image,
                                        css:    'imgTime2 image',
                                        src:    'images/module/bpvod/pages/SS-MS-4_2_PurchaseOverlayPurchaseCompleteMessage/icon06.png'
                                    },
                                    {
                                        type:   accedo.ui.label,
                                        css:    'labelPurchaseSWatchN label',
                                        text:   'Watch now'
                                    },
                                    {
                                        type:   accedo.ui.label,
                                        id:     'expiredIn',
                                        css:    'labelPurchaseSExpire label',
                                        text:   'Rental expires in 48 hours'
                                    },
                                    {
                                        type:   accedo.ui.image,
                                        css:    'imgCal image',
                                        src:    'images/module/bpvod/pages/SS-MS-4_2_PurchaseOverlayPurchaseCompleteMessage/icon05.png'
                                    },
                                    {
                                        type:   accedo.ui.label,
                                        css:    'labelPurchaseSWatchL label',
                                        text:   'Watch later'
                                    },
                                    {
                                        type:   accedo.ui.label,
                                        id:     'availableFor',
                                        css:    'labelPurchaseSAvailable label',
                                        text:   'Rental available for 7 days'
                                    },
                                    {
                                        type:   accedo.ui.image,
                                        css:    'imgLine3 image',
                                        src:    'images/module/bpvod/pages/SS-MS-4_0_PurchaseOverlayConfirmRental/popup_line01.png'
                                    },
                                    {
                                        type:           telstra.sua.module.bpvod.components.fluidButton,
                                        heightStyle:    accedo.ui.component.WRAP_CONTENT,
                                        text:           'Watch now',
                                        id:             'watchNowButton',
                                        css:            'psWatchNowButton long green button focus',
                                        width:          '242px',
                                        nextUp:         null,
                                        nextDown:       'watchLaterButton',
                                        nextLeft:       null,
                                        nextRight:      null
                                    },
                                    {
                                        type:           telstra.sua.module.bpvod.components.fluidButton,
                                        heightStyle:    accedo.ui.component.WRAP_CONTENT,
                                        text:           'Watch later',
                                        id:             'watchLaterButton',
                                        css:            'psWatchLaterButton long green button',
                                        width:          '242px',
                                        nextUp:         'watchNowButton',
                                        nextDown:       'viewRelatedButton',
                                        nextLeft:       null,
                                        nextRight:      null
                                    },
                                    {
                                        type:           telstra.sua.module.bpvod.components.fluidButton,
                                        heightStyle:    accedo.ui.component.WRAP_CONTENT,
                                        text:           'View related movies',
                                        id:             'viewRelatedButton',
                                        css:            'psViewButton long green button',
                                        width:          '242px',
                                        nextUp:         'watchLaterButton',
                                        nextDown:       null,
                                        nextLeft:       null,
                                        nextRight:      null
                                    }
                                    ]
                                }
                                ]
                            }
                            ]
                        }
                        ]
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.normal,
                css:  'bigPondHeader',
                children: [
                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: '' 
                }
                ]
            }
            ]
        }
    });