/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.views.joinNowStep1EmailView", [
    "telstra.sua.module.bpvod.views.joinNowTemplateView"],
    function(){
        //var keyboard = (accedo.device.manager.identification.getFirmwareYear() == "2011" ? telstra.sua.core.components.keyboard : telstra.sua.core.components.keyboard_new);
        var keyboard = telstra.sua.core.components.keyboard;

        var viewObj = accedo.utils.object.clone(telstra.sua.module.bpvod.views.joinNowTemplateView({
            pageTitle : "BigPond Movies Username",
            stepLabel : {
                type: accedo.ui.label,
                id:   'stepTag',
                css:  'step1tag label',
                text: '1'
            },
            backButtonCss : '',
            continueButtonCss : 'continueBTstep1 button medium focus',
            backText : '',
            continueText : 'Continue',
            corePanel : [
            {
                type: accedo.ui.label,
                css:  'step1label1 label',
                text: 'Welcome to BigPond Movies - joining is easy. Simply start by typing in your email address.'
            },
            {
                type: accedo.ui.label,
                css:  'step1label2 label',
                text: 'Type in your email address'
            },
            {
                type:           keyboard,
                id:             'emailField',
                css:            'pageInputField emailInput',
                nextDown:       'continueButton',
                params:{
                    title: "Email Address",
                    width: 480,
                    maxlength: 40
                }
            },
            {
                type: accedo.ui.label,
                css:  'step1label3 label',
                text: 'Your email address is your username for<br>logging into BigPond Movies.'
            },
            ]
        }));

        return viewObj;
    });