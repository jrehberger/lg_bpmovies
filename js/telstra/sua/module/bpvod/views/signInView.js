/**
 * @fileOverview signInView
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.views.signInView", [
    "accedo.ui.layout.absolute","accedo.ui.label","accedo.ui.image", "accedo.ui.list",
    "telstra.sua.module.bpvod.components.fluidButton","telstra.sua.core.components.keyboard","telstra.sua.core.components.keyboard_new"],
    function(){
        //var keyboard = (accedo.device.manager.identification.getFirmwareYear() == "2011" ? telstra.sua.core.components.keyboard : telstra.sua.core.components.keyboard_new);
        var keyboard = telstra.sua.core.components.keyboard;

        return {
            type:   accedo.ui.layout.absolute,
            id:     'bpvod_canvas',
            css:    'background2',

            children: [
            {
                type: accedo.ui.layout.absolute,
                css:  'pageStore',

                children: [
                {
                    type: accedo.ui.layout.absolute,
                    css:  'page signIn',

                    children: [
                    {
                        type: accedo.ui.layout.absolute,
                        css:  'mainPanel panel focus',

                        children:[
                        {
                            type: accedo.ui.layout.absolute,
                            css:  'contentPanel panel vertical focus',

                            children: [
                            {
                                type: accedo.ui.label,
                                css:  'subtitle1 label',
                                text: 'Email and Password'
                            },
                            {
                                type: accedo.ui.image,
                                css:  'highlight1 highLight1 image',
                                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
                            },
                            {
                                type: accedo.ui.label,
                                css:  'step1tag label',
                                text: '1'
                            },
                            {
                                type: accedo.ui.label,
                                css:  'textLabel email label',
                                text: 'Email'
                            },
                            {
                                type:           keyboard,
                                id:             'emailaddress',
                                css:            'emailInputFieldSet textFieldFront',
                                nextDown:       'passwordInputField',
                                                        
                                params:{
                                    title: "Email Address",
                                    width: 480,
                                    maxlength: 40
                                }
                            },
                            {
                                type:     accedo.ui.label,
                                css:      'textLabel psw label',
                                text:     'Password',
                                y:        '163px'
                            },
                            {
                                type:           keyboard,
                                id:             'passwordInputField',
                                css:            'passwordInputField',
                                nextDown:       'saveButton',
                                nextUp:         'emailaddress',
                                                        
                                params:{
                                    title: "Password",
                                    width: 480,
                                    maxlength: 40,
                                    isPassword:true
                                }
                            },
                            {
                                type:     accedo.ui.label,
                                css:      'supportNum label',
                                text:     '<strong>Forgotten your password?</strong><br>Call us on 1800 502 502 and we\'ll help you to regain access to your account.'
                            },
                            {
                                type:  accedo.ui.layout.absolute,
                                css:   'BottomButtons panel horizontal',
                                width: '440px',
                                children: [
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'saveButton',
                                    css:       'saveButton button medium',
                                    width:     '198px',
                                    text:      'Continue',
                                    nextRight: 'closeButton',
                                    nextUp:    'passwordInputField'
                                },
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'closeButton',
                                    css:       'closeButton button medium',
                                    text:      'Close',
                                    width:     '198px',
                                    nextLeft:  'saveButton',
                                    nextUp:    'passwordInputField'
                                }
                                ]
                            }
                            ]
                        },
                        {
                            type: accedo.ui.label,
                            css:  'termtag label',
                            text: ''
                        }
                        ]
                    },
                    {
                        type: accedo.ui.label,
                        css:  'signInTitle'
                    },
                    {
                        type: accedo.ui.label,
                        id:   'secureLogo',
                        css:  'secureLogo'
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.absolute,
                css:  'bigPondHeader',

                children: [

                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: ''
                }
                ]
            }
            ]
        };
    });