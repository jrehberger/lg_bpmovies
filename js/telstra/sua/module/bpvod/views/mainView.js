/**
 * @fileOverview mainView
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.views.mainView", [
    "accedo.ui.layout.absolute","accedo.ui.label","accedo.ui.image","accedo.ui.button", "accedo.ui.list"],

    function(){
        
        return {

            type:   accedo.ui.layout.absolute,
            id:     '#bpvod_canvas',
            css:    'background3',

            children: [
            {
                type: accedo.ui.layout.absolute,
                css:  'pageStore',

                children: [
                {
                    type: accedo.ui.layout.absolute,
                    css:  'page topLevelMenuPage',

                    children: [
                    {
                        type: accedo.ui.layout.absolute,
                        css:  'panel focus',

                        children: [
                        {
                            type:        accedo.ui.list,
                            id:          '#topLevelMenu',
                            css:         'topLevelMenu panel',
                            orientation: accedo.ui.list.HORIZONTAL,
                            behaviour:   accedo.ui.list.CAROUSEL,
                            visibleSize: 3,
                            firstSelectedIndex: 1,
                            preloadSize: 3,

                            childTemplate: {
                                type:    accedo.ui.layout.absolute,
                                css:     'mediumPane',
                                children: [
                                {
                                    type: accedo.ui.label,
                                    id:   'bpvod-topLevelMenuButton-titleLabel',
                                    css:  'titleLabel label'
                                },
                                {
                                    type: accedo.ui.layout.absolute,
                                    id:   'bpvod-topLevelMenuButton-containerOverflow',
                                    css:  'containerOverflow',
                                    children: [
                                    {
                                        type: accedo.ui.label,
                                        id:   'bpvod-topLevelMenuButton-imageContainer',
                                        css:  'container'
                                    }
                                    ]
                                },
                                {
                                    type: accedo.ui.layout.absolute,
                                    id:   'bpvod-topLevelMenuButton-contentTable',
                                    css:  'contentTable',
                                    children: [
                                    {
                                        type: accedo.ui.label,
                                        id:   'bpvod-topLevelMenuButton-contentLabel',
                                        css:  'contentLabel label'
                                    }
                                    ]
                                }
                                ]
                            },
                            widthStyle:  accedo.ui.component.FIT_PARENT,
                            heightStyle: accedo.ui.component.FIT_PARENT,
                            width:       '877px',
                            height:      '171px'
                        },
                        {
                            type: accedo.ui.layout.absolute,
                            id:   '#topLevelMenu1',

                            children: [
                            {
                                type: accedo.ui.label,
                                css:  'bigPane'
                            },
                            {
                                type: accedo.ui.label,
                                css:  'bigPane-mirror'
                            },
                            {
                                type: accedo.ui.label,
                                css:  'mediumPane left'
                            },
                            {
                                type: accedo.ui.label,
                                css:  'mediumPaneLeft-mirror'
                            },
                            {
                                type: accedo.ui.label,
                                css:  'mediumPane right'
                            },
                            {
                                type: accedo.ui.label,
                                css:  'mediumPaneRight-mirror'
                            },
                            {
                                type: accedo.ui.label,
                                css:  'smallPane left'
                            },
                            {
                                type: accedo.ui.label,
                                css:  'smallPaneLeft-mirror'
                            },
                            {
                                type: accedo.ui.label,
                                css:  'smallPane right'
                            },
                            {
                                type: accedo.ui.label,
                                css:  'smallPaneRight-mirror'
                            },
                            {
                                type: accedo.ui.layout.absolute,
                                css:  'leftRightScrollerKey',
                                                        
                                children: [
                                {
                                    type: accedo.ui.button,
                                    id:   'scrollerKeyLeft',
                                    css:  'leftElement'
                                },
                                {
                                    type: accedo.ui.button,
                                    id:   'scrollerKeyRight',
                                    css:  'rightElement'
                                }
                                ]
                            }
                            ]
                        }
                        ]
                    },
                    {
                        type: accedo.ui.label,
                        css:  'titleText'
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.absolute,
                css:  'bigPondHeader',

                children: [

                {
                    id: 'onOffNetIndicator',
                    type: accedo.ui.label,
                    css: 'onNetFlag'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: ''
                }
                ]
            }
            ]
        };
    });
