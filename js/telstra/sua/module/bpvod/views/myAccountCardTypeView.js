/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define('telstra.sua.module.bpvod.views.myAccountCardTypeView',
    ['accedo.ui.layout.normal',
    'accedo.ui.label',
    'telstra.sua.module.bpvod.components.radioButton',
    'telstra.sua.module.bpvod.components.fluidButton'],
    function() {

        return {

            type:   accedo.ui.layout.normal,
            id:     'bpvod_canvas',
            css:    'background2',
            children:       [
            {
                type: accedo.ui.layout.normal,
                css:  'pageStore',
                children: [
                {
                    type: accedo.ui.layout.normal,
                    id:   'accountPageGeneralClass',
                    css:  'accountPageGeneralClass updateCreditCard',
                    children: [
                    {
                        type: accedo.ui.layout.normal,
                        id: 'primePanel',
                        css: 'panal',
                        children: [
                        {
                            type: accedo.ui.layout.normal,
                            id: 'mainPanel',
                            css: 'mainPanel',
                            children: [
                            {
                                type: accedo.ui.layout.normal,
                                id: 'backgroundBorder',
                                css: 'backgroundBorder'
                            },
                            {
                                type: accedo.ui.label,
                                id: 'innerHeader',
                                css: 'innerHeader',
                                text: 'Credit Card Details'
                            },
                            {
                                type: accedo.ui.layout.normal,
                                id: 'componentPanel',
                                css: 'componentPanel',
                                children: [
                                {
                                    type: accedo.ui.layout.normal,
                                    css:   'cardTypePanel panel vertical',
                                    children: [
                                    {
                                        type: accedo.ui.label,
                                        css: 'labelSelectCard label',
                                        text: 'Please select your credit card type and press OK.'
                                    },
                                    {
                                        type: accedo.ui.layout.normal,
                                        css: 'cardchoose panel vertical',
                                        children: [
                                        {
                                            type:      telstra.sua.module.bpvod.components.radioButton,
                                            id:        'option1',
                                            css:       'option1 button',
                                            text:      'Visa',
                                            nextDown:  'option2'
                                        },
                                        {
                                            type:      telstra.sua.module.bpvod.components.radioButton,
                                            id:        'option2',
                                            css:       'option2 button',
                                            text:      'MasterCard',
                                            nextUp:    'option1',
                                            nextDown:  'option3'
                                        },
                                        {
                                            type:      telstra.sua.module.bpvod.components.radioButton,
                                            id:        'option3',
                                            css:       'option3 button',
                                            text:      'American Express',
                                            nextUp:    'option2',
                                            nextDown:  'continueButton'
                                        }
                                        ]
                                    },
                                    {
                                        type: accedo.ui.label,
                                        css: 'labelEnterCardD label',
                                        text: 'By entering my credit card details, I declare<br>that I am at least 18 years of age.'
                                    }
                                    ]
                                }
                                ]
                            },
                            {
                                type: accedo.ui.layout.normal,
                                css:   'buttonPanel panel horizontal',
                                children: [
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'cancelButton',
                                    css:       'cancelButton button medium green',
                                    text:      'Cancel',
                                    width:     '174px',
                                    nextRight: 'continueButton',
                                    nextUp:    'option3'
                                },
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'continueButton',
                                    css:       'continueButton button medium green',
                                    text:      'Continue',
                                    width:     '174px',
                                    nextLeft:  'cancelButton',
                                    nextUp:    'option3'
                                }
                                ]
                            }
                            ]
                        }
                        ]
                    },
                    {
                        type: accedo.ui.layout.normal,
                        id: 'titleText',
                        css: 'titleText'
                    },
                    {
                        type: accedo.ui.layout.normal,
                        id: 'securityIcon',
                        css: 'securityIcon'
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.normal,
                css:  'bigPondHeader',
                children: [
                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: '' 
                }
                ]
            }
            ]
        }
    });