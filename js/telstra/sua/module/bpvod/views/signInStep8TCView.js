/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.views.signInStep8TCView", [
    "accedo.ui.scrollbar",
    "telstra.sua.module.bpvod.components.radioButton",
    "telstra.sua.module.bpvod.views.signInTemplateView"],
    function(){

        var viewObj = accedo.utils.object.clone(telstra.sua.module.bpvod.views.signInTemplateView({
            pageTitle : 'Terms and Conditions',
            highlightImage2 : {
                type: accedo.ui.image,
                css:  'highlight2 highLight2 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage3 : {
                type: accedo.ui.image,
                css:  'highlight3 highLight3 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage4 : {
                type: accedo.ui.image,
                css:  'highlight4pt1 highLight4pt1 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage5 : {
                type: accedo.ui.image,
                css:  'highlight5 highLight5 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage6 : {
                type: accedo.ui.image,
                css:  'highlight6 highLight6 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage7 : {
                type: accedo.ui.image,
                css:  'highlight7 highLight7 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage8 : {
                type: accedo.ui.image,
                css:  'termhighLight image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            stepLabel : {
                type: accedo.ui.label,
                id:   'stepTag',
                css:  'termtag label',
                text: 'Terms'
            },
            mainPanelCss: 'mainPanel TC',
            backButtonCss : 'backBT button medium',
            continueButtonCss : 'continueBT button medium',
            backText : 'Back',
            continueText : 'Register',
            corePanel : [
            {
                type: accedo.ui.layout.normal,
                css:  'termsPanel panel horizontal',
                children:[
                {
                    type : accedo.ui.layout.normal,
                    css : 'scrollControlPanel panel',
                    children:[
                    {
                        type : accedo.ui.image,
                        css : 'verticalScrollbar upperArrow',
                        src : 'images/module/bpvod/scrollBarUpArrow.png'
                    },
                    {
                        type : accedo.ui.scrollbar,
                        id : "scrollbar",
                        css : "verticalScrollbar",
                        trackTopping : false
                    },
                    {
                        type : accedo.ui.image,
                        css : 'verticalScrollbar lowerArrow',
                        src : 'images/module/bpvod/scrollBarDownArrow.png'
                    }
                    ]
                },
                {
                    type : accedo.ui.layout.normal,
                    id :  'leftPanel',
                    css : 'leftPanel panel',
                    nextUp : null,
                    nextDown: null,
                    nextRight: 'agreementYes',
                    focusable : true,
                    children:[
                    {
                        type : accedo.ui.layout.normal,
                        id: 'termsContainer',
                        css : 'termsContainer panel',
                        children : [
                        {
                            type : accedo.ui.label,
                            id : 'termsContent',
                            css : 'termsTXT label',
                            text : ''
                        }
                        ]
                    }
                    ]
                },
                {
                    type : accedo.ui.layout.normal,
                    css : 'rightPanel panel vertical',
                    children:[
                    {
                        type : accedo.ui.layout.normal,
                        css : 'agreement panel vertical',
                        children:[
                        {
                            type : accedo.ui.label,
                            css : 'agreementLabel label',
                            text : 'I agree to the terms and conditions'
                        },
                        {
                            type:      telstra.sua.module.bpvod.components.radioButton,
                            id:        'agreementYes',
                            css:       'agreementYes pinSettingYes agreementRadioYes button',
                            text:      'Yes',
                            nextDown:  'agreementNo',
                            nextLeft:  'leftPanel'
                        },
                        {
                            type:      telstra.sua.module.bpvod.components.radioButton,
                            id:        'agreementNo',
                            css:       'agreementNo pinSettingNo agreementRadioNo button',
                            text:      'No',
                            nextUp:    'agreementYes',
                            nextDown:  'recieveMailYes',
                            nextLeft:  'leftPanel'
                        }
                        ]
                    },
                    {
                        type : accedo.ui.layout.normal,
                        css : 'recieveMail panel vertical',
                        children:[
                        {
                            type : accedo.ui.label,
                            css : 'recieveMailLabel label',
                            text : 'I\'d like to receive email news and special offers for BigPond Movies and related products and services'
                        },
                        {
                            type:      telstra.sua.module.bpvod.components.radioButton,
                            id:        'recieveMailYes',
                            css:       'recieveMailYes recieveMailRadioYes pinSettingYes button',
                            text:      'Yes',
                            nextUp:    'agreementNo',
                            nextDown:  'recieveMailNo',
                            nextLeft:  'leftPanel'
                        },
                        {
                            type:      telstra.sua.module.bpvod.components.radioButton,
                            id:        'recieveMailNo',
                            css:       'recieveMailNo recieveMailRadioNo pinSettingNo button',
                            text:      'No',
                            nextUp:    'recieveMailYes',
                            nextDown:  'continueButton',
                            nextLeft:  'leftPanel'
                        }
                        ]
                    }
                    ]
                }
                ]
            }

            ]
        }));

        return viewObj;
    });