/**
 * @fileOverview
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.views.signInStep5AccountPinView", [
    "telstra.sua.module.bpvod.views.signInTemplateView"],
    function(){
        //var keyboard = (accedo.device.manager.identification.getFirmwareYear() == "2011" ? telstra.sua.core.components.keyboard : telstra.sua.core.components.keyboard_new);
        var keyboard = telstra.sua.core.components.keyboard;

        var viewObj = accedo.utils.object.clone(telstra.sua.module.bpvod.views.signInTemplateView({
            pageTitle : "Account PIN",
            highlightImage2 : {
                type: accedo.ui.image,
                css:  'highlight2 highLight2 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage3 : {
                type: accedo.ui.image,
                css:  'highlight3 highLight3 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage4 : {
                type: accedo.ui.image,
                css:  'highlight4pt1 highLight4pt1 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage5 : {
                type: accedo.ui.image,
                css:  'highlight5 highLight5 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            stepLabel : {
                type: accedo.ui.label,
                id:   'stepTag',
                css:  'step5tag label',
                text: '5'
            },
            backButtonCss : 'forgetPinButton medium green button',
            continueButtonCss : 'continueBT button medium focus',
            backText : 'Change Pin',
            continueText : 'Next step',
            corePanel : [
            {
                type: accedo.ui.label,
                css:  'PINLabel1 PINlabel1 label',
                text: 'We notice you already have an Account PIN.'
            },
            {
                type: accedo.ui.label,
                css:  'PINLabel2 PINlabel2 label',
                text: 'Please confirm your 4 digit Account PIN'
            },
            {
                type:           keyboard,
                id:             'pinField',
                css:            'inputField labelAccountPinInput',
                nextDown:       'continueButton',
                params:{
                    title: "PIN",
                    width: 72,
                    maxlength: 4,
                    keyboardPosition: {
                        x:118,
                        y:139
                    },
                    isPassword:true,
                    numericalInput:true
                }
            }
        ]
        }));

        return viewObj;
    });