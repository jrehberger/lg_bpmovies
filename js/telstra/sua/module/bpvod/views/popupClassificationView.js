/**
 * @fileOverview popupClassificationView
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 */
accedo.define('telstra.sua.module.bpvod.views.popupClassificationView',
    ['accedo.ui.layout.normal',
    'accedo.ui.layout.absolute',
    "accedo.ui.label",
    "accedo.ui.image",
    "telstra.sua.module.bpvod.components.fluidButton"],
    function() {

        var label   = accedo.ui.label,
        layout      = accedo.ui.layout,
        button      = accedo.ui.fluidButton;
        
        return {

            type : layout.absolute,
            id : 'popupBg',
            css : 'accedo-ui-popupcontent',
            children: [

            {
                type: layout.absolute,
                id : 'popupMain',
                css:'popupClassification panel popup focus',

                children:[
                {
                    type : label,
                    css : 'header label',
                    text : 'Classification Information'
                },
                {
                    type : label,
                    css : 'classificationInfo line1',
                    text : 'General'
                },
                {
                    type : label,
                    css : 'classificationInfo line2',
                    text : 'Parental guidance recommended'
                },
                {
                    type : label,
                    css : 'classificationInfo line3',
                    text : 'Recommended for mature audiences'
                },
                {
                    type : label,
                    css : 'classificationInfo line4',
                    text : 'Not suited for people under 15. Under 15s must be accompanied by a parent or adult guardian'
                },
                {
                    type : label,
                    css : 'classificationInfo line5',
                    text : 'Restricted to 18 and over'
                },
                {
                    type : button,
                    id : 'okButton',
                    css : 'okButton button',
                    width : '174px',
                    text : 'OK - Close'
                } 
                ]
            }
            ]

        }


    });