/**
 * @fileOverview
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 */
accedo.define('telstra.sua.module.bpvod.views.myRentalView',
    ['accedo.ui.layout.absolute',
    'accedo.ui.layout.normal',
    'accedo.ui.layout.relative',
    'accedo.ui.label',
    'accedo.ui.image',
    'accedo.ui.button'],
    function() {

        var image = accedo.ui.image,
        label = accedo.ui.label,
        normal = accedo.ui.layout.normal,
        scrollbar = accedo.ui.scrollbar;

        return {

            type:   accedo.ui.layout.absolute,
            id:     '#bpvod_canvas',
            css:    'background2',

            children: [

            {
                type: accedo.ui.layout.absolute,
                css:  'pageStore',
                id: 'pageStore',
                children: [
        
                {
                    type: accedo.ui.layout.absolute,
                    css:  'page myRental',
                    id: 'pageMyRental',
                    children: [
                 
                    {
                        type: accedo.ui.layout.absolute,
                        css:  'panel focus',
                        id: 'mainPanel',
                        children: [
                       
                        {
                            type: accedo.ui.layout.absolute,
                            css:  'mainVerticalPanel panel vertical focus',
                            id: 'mainVerticalPanel',
                            children: [

                            {
                                type: image,
                                css:  'categoryTitle image',
                                src: 'images/module/bpvod/pages/SS-REN-1_0_My_Rentals/MyRental.png'
                            },
                            {
                                type: image,
                                css:  'imgRentBackGround image',
                                src: 'images/module/bpvod/pages/SS-REN-1_0_My_Rentals/MyRentals_bg01.png'
                            },
                            {
                                type: label,
                                id:'noRentalLabel',
                                css:  'labelnoRental label',
                                text: 'You currently have no Rental'
                            },
                            {
                                type: label,
                                id: 'visitLabel',
                                css:  'labelVisit label',
                                text: 'Visit BigPondMovies to find movies and TV shows to rent.'
                            },
                            {
                                type: image,
                                id: 'noRentalImage',
                                css:  'imgNoRental image',
                                src: 'images/module/bpvod/topMenu_Icon03_myRental_Exbig.png'
                            },
                            {
                                type: normal,
                                css:  'haveRentalPanel panel horizontal focus',
                                id: 'haveRentalPanel',
                                children: [
                                {
                                    type: image,
                                    css:  'imgPreview image',
                                    id: 'imgPreview',
                                    src: ''
                                },

                                //list object

                                {
                                    type: accedo.ui.layout.absolute,
                                    css:  'resultPanel horizontalVideoRollingMenu panel focus',
                                    id : 'resultPanel',

                                    children: [
                                    {
                                        //List-specific properties
                                        type:        accedo.ui.list,
                                        css:         'panel horizontal focus force-pos-rel',
                                        orientation: accedo.ui.list.HORIZONTAL,
                                        //Behaviour: CAROUSEL or REGULAR
                                        behaviour:   accedo.ui.list.REGULAR,
                                        x:			 '15px',
                                        y:			 '20px',
                                        id:          '#movie_horizontal_listing',
                                        //REGULAR ONLY: How many children to display. -1 means all of them (default).
                                        visibleSize: 3,
                                        //REGULAR ONLY: How many children to cache in advance (created but not displayed). Default is 0.
                                        preloadSize: 6,
                                        //REGULAR ONLY: when 'moving' the list forward or backward, what is the step used?
                                        scrollStep:  3,
                                        loop: true,

                                        childTemplate: {
                                            type:   accedo.ui.layout.absolute,
                                            css:     'videoItemImage image',
                                            width:   '181px',
                                    		height:  '247px',
                                            children: [
                                            {
		                                        type: accedo.ui.image,
		                                        id:   'bpvod_videoItemImage',
		                                        css:  'pt2w113h160'
		                                    },
		                                    {
		                                        type: accedo.ui.label,
		                                        css:  'pt2w113h160 videoItemImageBg'
		                                    }
                                            ]
                                        },

                                        //Other inherited properties
                                        widthStyle:  accedo.ui.component.FIT_PARENT,
                                        heightStyle: accedo.ui.component.FIT_PARENT,
                                        height:      '235px',
                                        width:       '100%'
                                    },
                                    {
                                        type: accedo.ui.layout.absolute,
                                        css:  'panel horizontal',
                                        id: 'scrollerKeyPanel',
                                        children:[
                                        {
                                            type: accedo.ui.layout.absolute,
                                            css:  'leftRightScrollerKey',
                                            x:    '32px',
                                            id: 'scrollerKeyPanelLeftRight',
                                            children: [
                                            {
                                                type: accedo.ui.button,
                                                id:   'scrollerKeyLeft',
                                                css:  'leftElement'
                                            },
                                            {
                                                type: accedo.ui.button,
                                                id:   'scrollerKeyRight',
                                                css:  'rightElement'
                                            }
                                            ]
                                        }
                                        ]
                                    }
                                    ]
                                },
                                {
                                    type: label,
                                    id: 'movieTitle',
                                    css: 'labelMovieTitle',
                                    text: ''
                                },
                                {
                                    type: label,
                                    id: 'movieInfo',
                                    css: 'labelMovieInfo',
                                    text: ''
                                },
                                {
                                    type: image,
                                    css:  'imgClock image',
                                    id: 'imgClock',
                                    src: 'images/module/bpvod/pages/SS-REN-1_0_My_Rentals/icon4.png'
                                },
                                {
                                    type: image,
                                    css:  'imgReadyWatch image',
                                    id: 'readyImage',
                                    src: 'images/module/bpvod/pages/SS-REN-1_0_My_Rentals/icon_Ready2Watch.png'
                                },
                                {
                                    type: label,
                                    id : 'watchViewLabel',
                                    css:  'labelWatchView label',
                                    text: 'Watch Now:'
                                },
                                {
                                    type: label,
                                    css:  'labelWatchViewingPeriod label',
                                    text: '',
                                    id:'watchNowLabel'
                                },
                                {
                                    type: label,
                                    id : 'expireViewLabel',
                                    css:  'labelWatchExpire label',
                                    text: 'Watch Later:'
                                },
                                {
                                    type: label,
                                    css:  'labelWatchDeletionPeriod label',
                                    text: '',
                                    id: 'watchLaterLabel'
                                },
                                {
                                    type: label,
                                    css:  'labelWatchReady label',
                                    id: 'readyLabel',
                                    text: 'Ready to Watch'
                                },
                                {
                                    type: image,
                                    css:  'imgExpired image',
                                    id: 'expiredImage',
                                    src: 'images/module/bpvod/pages/SS-REN-1_0_My_Rentals/expired.png'
                                },
                                {
                                    type: label,
                                    css:  'labelExpire label',
                                    id: 'expiredLabel',
                                    text: 'Expired'
                                }
                                ]
                            }
                            ]
                        },
                        {
                            type: accedo.ui.layout.absolute,
                            css:  'categoryPanel verticalStripButtonMenu panel force-pos-rel',
                            id: 'categoryPanel',

                            children: [
                            {
                                type:        accedo.ui.list,
                                id:          '#movie_category_listing',
                                css:         'panel vertical',
                                orientation: accedo.ui.list.VERTICAL,
                                behaviour:   accedo.ui.list.CAROUSEL,
                                visibleSize: 3,
                                firstSelectedIndex: 1,
                                preloadSize: 3,
                                scrollStep:  1,
                                //focusable:   false,
                                childTemplate: {
                                    type:    accedo.ui.button,
                                    css:     'stripButton small',
                                    width:   '395px',
                                    height:  '47px'
                                },
                                widthStyle:  accedo.ui.component.FIT_PARENT,
                                heightStyle: accedo.ui.component.FIT_PARENT,
                                width:       '395px',
                                height:      '150px'
                            }
                            ]
                        },
                        {
                            type: accedo.ui.layout.absolute,
                            css:  'upDownScrollerKey categoryPanelUpDownArrow',
                            id: 'upDownKeyPanel',

                            children: [
                            {
                                type: accedo.ui.button,
                                id:   'upScroller',
                                css:  'upDownElement up'
                            },
                            {
                                type: accedo.ui.button,
                                id:   'downScroller',
                                css:  'upDownElement down'
                            }
                            ]
                        }
                        ]
                    },
                    {
                        type : scrollbar,
                        css : "horizontalScrollbar",
                        id : "scrollbar",
                        trackTopping : false,
                        orientation: scrollbar.HORIZONTAL
                    },
                    {
                        type:           telstra.sua.module.bpvod.components.fluidButton,
                        heightStyle:    accedo.ui.component.WRAP_CONTENT,
                        text:           'OK - Watch Now',
                        id:             'watchNowButton',
                        css:            'okButton medium focused green button',
                        width:          '196px'
                    },
                    {
                        type:           telstra.sua.module.bpvod.components.fluidButton,
                        heightStyle:    accedo.ui.component.WRAP_CONTENT,
                        text:           'OK - BigPond Movies',
                        id:             'bigPondMoviesButton',
                        css:            'launchButton medium focused green button',
                        width:          '226px'
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.normal,
                css:  'bigPondHeader',
                children: [
                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: ''
                }
                ]
            }
            ]
        }
    });