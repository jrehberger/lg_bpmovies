/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define('telstra.sua.module.bpvod.views.myAccountViewPurchaseView',
    ['accedo.ui.layout.normal',
    'accedo.ui.label',
    'accedo.ui.button',
    'telstra.sua.core.components.truncatedLabel',
    'telstra.sua.module.bpvod.components.purchaseHistoryItem'],
    function() {

        return {

            type:   accedo.ui.layout.normal,
            id:     'bpvod_canvas',
            css:    'background2',
            children:       [
            {
                type: accedo.ui.layout.normal,
                id:   'pageStore',
                css:  'pageStore',
                children: [
                {
                    type: accedo.ui.layout.normal,
                    id:   'accountPageGeneralClass',
                    css:  'accountPageGeneralClass account_viewPurchase',
                    children: [
                    {
                        type: accedo.ui.layout.normal,
                        id: 'primePanel',
                        css: 'panal focus',
                        children: [
                        {
                            type: accedo.ui.layout.normal,
                            id: 'mainVerticalPanel',
                            css: 'mainVerticalPanel panel vertical focus',
                            children: [
                            {
                                type: accedo.ui.layout.normal,
                                id: 'imgAccountMenuBK',
                                css: 'imgAccountMenuBK'
                            },
                            {
                                type: accedo.ui.layout.normal,
                                css:  'upDownScrollerKey categoryPanelUpDownArrow',
                                children: [
                                {
	                                type: accedo.ui.button,
	                                id:   'upScroller',
	                                css:  'upDownElement up'
	                            },
	                            {
	                                type: accedo.ui.button,
	                                id:   'downScroller',
	                                css:  'upDownElement down'
	                            }
                                ]
                            },
                            {
                                type: accedo.ui.layout.normal,
                                id: 'titleText',
                                css: 'titleText'
                            },
                            {
                                type: accedo.ui.layout.normal,
                                id: 'securityIcon',
                                css: 'securityIcon'
                            },
                            {
                                type: accedo.ui.label,
                                id: 'labelRefNo',
                                css: 'labelRefNo',
                                text: 'Ref. No.'
                            },
                            {
                                type: accedo.ui.label,
                                id: 'labelTitle',
                                css: 'labelTitle',
                                text: 'Title'
                            },
                            {
                                type: accedo.ui.label,
                                id: 'labelDevice',
                                css: 'labelDevice',
                                text: 'Device'
                            },
                            {
                                type: accedo.ui.label,
                                id: 'labelDate',
                                css: 'labelDate',
                                text: 'Date'
                            },
                            {
                                type: accedo.ui.label,
                                id: 'labelPrice',
                                css: 'labelPrice',
                                text: 'Price'
                            },
                            {
                                type: accedo.ui.label,
                                id: 'labelPage',
                                css: 'labelPage',
                                text: 'Page: '
                            },
                            {
                                type: accedo.ui.label,
                                id: 'labelInfo',
                                css: 'labelInfo',
                                text: 'For more information, please check your account at: http://bigpondmovies.com/'
                            },
                            {
                                type: accedo.ui.label,
                                id: 'innerHeader',
                                css: 'innerHeader',
                                text: 'Purchase History'
                            },
                            {
                                type: accedo.ui.layout.normal,
                                css: 'historyPanel panel vertical',
                                children:[
                                {
                                    type: telstra.sua.module.bpvod.components.purchaseHistoryItem,
                                    id: 'historyLinePanel0',
                                    index: 0
                                },
                                {
                                    type: telstra.sua.module.bpvod.components.purchaseHistoryItem,
                                    id: 'historyLinePanel1',
                                    index: 1
                                },
                                {
                                    type: telstra.sua.module.bpvod.components.purchaseHistoryItem,
                                    id: 'historyLinePanel2',
                                    index: 2
                                },
                                {
                                    type: telstra.sua.module.bpvod.components.purchaseHistoryItem,
                                    id: 'historyLinePanel3',
                                    index: 3
                                },
                                {
                                    type: telstra.sua.module.bpvod.components.purchaseHistoryItem,
                                    id: 'historyLinePanel4',
                                    index: 4
                                },
                                {
                                    type: telstra.sua.module.bpvod.components.purchaseHistoryItem,
                                    id: 'historyLinePanel5',
                                    index: 5
                                }
                                ]
                            },
                            ]
                        }
                        ]
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.normal,
                css:  'bigPondHeader',
                children: [
                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: ''
                }
                ]
            }
            ]
        }
    });