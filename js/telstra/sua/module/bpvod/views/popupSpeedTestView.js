/**
 * @fileOverview popupSpeedTestView
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 */
accedo.define('telstra.sua.module.bpvod.views.popupSpeedTestView',
    ['accedo.ui.layout.normal',
     'accedo.ui.layout.absolute',
    "accedo.ui.label",
    "accedo.ui.image",
    "telstra.sua.module.bpvod.components.fluidButton"],
    function() {

        var label   = accedo.ui.label,
        layout      = accedo.ui.layout,
        image       = accedo.ui.image,
        button      = telstra.sua.module.bpvod.components.fluidButton;

        return {

            type:layout.absolute,
            id:'popupBg',
            css : 'accedo-ui-popupcontent',
            children: [

            {
                type: layout.absolute,
                id : 'popupMain',
                css:'connectionInfoPopup panel popup focus',
                children:[
                    {
                        type: label,
                        css : 'header label',
                        text : 'Connection Information'
                    },
                    {
                        type: layout.normal,
                        id : 'componentPanel',
                        css : 'componentPanel panel vertical focus',
                        children : [
                            {
                                type: label,
                                id: 'label1',
                                css : 'labelMessageContent1 label',
                                text : '<b>Please wait while we determine the speed of your connection.</b><br>This may take up to 15 seconds.'
                            },
                            {
                                type: label,
                                id: 'label2',
                                css : 'labelMessageContent2 label',
                                text : ''
                            },
                            {
                                type: label,
                                id: 'label3',
                                css : 'labelMessageContent3 label',
                                text : ''
                            },
                            {
                                type: label,
                                id: 'label4',
                                css : 'labelMessageContent4 label',
                                text : ''
                            },
                            {
                                type: label,
                                id: 'label5',
                                css : 'labelMessageContent5 label',
                                text : ''
                            },
                            {
                                type: label,
                                id: 'label6',
                                css : 'labelMessageContent6 label',
                                text : ''
                            },
                            {
                                type: label,
                                id: 'labelContact',
                                css : 'labelcontact label',
                                text : "Please contact your ISP or BigPond on 13 POND(13 7663) <br> if you are experiencing connection difficulties."
                            },
                            {
                                type: image,
                                id:'barBase',
                                css : 'imgSpeedBar image',
                                src : 'images/module/bpvod/pages/SS-CL-1_0_ConnectionInfo/ConnectiBar_Cal.png'
                            },
                            {
                                type: layout.normal,
                                id: 'barContainer',
                                css : 'barContentContainer',
                                children : [
                                    {
                                        type : layout.normal,
                                        id : 'barRed',
                                        css : 'barContentRed',
                                        children :[
                                        	{
		                                        type: label,
				                                id: 'errorLabel',
				                                css : 'player-error label',
				                                text : "Error from player"
		                                    }
                                        ]
                                    },
                                    {
                                        type : layout.normal,
                                        id : 'barRedEnd',
                                        css : 'barContentRed_end',
                                        children :[]
                                    },
                                    {
                                        type : layout.normal,
                                        id : 'barYellow',
                                        css : 'barContentYellow',
                                        children :[]
                                    },
                                    {
                                        type : layout.normal,
                                        id : 'barYellowEnd',
                                        css : 'barContentYellow_end',
                                        children :[]
                                    },
                                    {
                                        type : layout.normal,
                                        id : 'barGreen',
                                        css : 'barContentGreen',
                                        children :[]
                                    },
                                    {
                                        type : layout.normal,
                                        id : 'barGreenEnd',
                                        css : 'barContentGreen_end',
                                        children :[]
                                    }
                                ]
                            },
                            {
                                type : layout.normal,
                                id: 'arrowContainer',
                                css : 'barContentArrowContainer',
                                children : [
                                    {
                                        type: layout.normal,
                                        css : 'downwardArrowSD',
                                        children : [
                                            {
                                                type : label,
                                                css : 'downwardLabel label',
                                                text : 'Standard Definition'
                                            }
                                        ]
                                    },
                                    {
                                        type: layout.normal,
                                        css : 'downwardArrowHD',
                                        children : [
                                            {
                                                type : label,
                                                css : 'downwardLabel label',
                                                text : 'HD'
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                type : image,
                                id : 'resultIcon',
                                css : 'imgResultIcon image',
                                src : ''
                            },
                            {
                                type : layout.normal,
                                id: 'buttonPanel',
                                css : 'hp panel horizontal focus',
                                children : [
                                    {
                                        type : button,
                                        id:'bigPondPlansButton',
                                        css : 'viewBigPondPanButton shorter small button focus',
                                        text : 'BigPond Plans',
                                        width : '174px',
                                        nextRight : 'closeButton'
                                    },
                                    {
                                        type : button,
                                        id:'closeButton',
                                        css : 'conInfobutton shorter small button',
                                        text : 'Cancel',
                                        width : '174px',
                                        nextLeft : 'bigPondPlansButton'
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
            ]
        }
    });