/**
 * @fileOverview
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.views.signInStep7DeviceNameView", [
    "telstra.sua.module.bpvod.views.signInTemplateView"],
    function(){
        var deviceName = accedo.device.manager.identification.getDeviceType();
        //var keyboard = (accedo.device.manager.identification.getFirmwareYear() == "2011" ? telstra.sua.core.components.keyboard : telstra.sua.core.components.keyboard_new);
        var keyboard = telstra.sua.core.components.keyboard;

        var viewObj = accedo.utils.object.clone(telstra.sua.module.bpvod.views.signInTemplateView({
            pageTitle : deviceName + " Name",
            highlightImage2 : {
                type: accedo.ui.image,
                css:  'highlight2 highLight2 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage3 : {
                type: accedo.ui.image,
                css:  'highlight3 highLight3 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage4 : {
                type: accedo.ui.image,
                css:  'highlight4pt1 highLight4pt1 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage5 : {
                type: accedo.ui.image,
                css:  'highlight5 highLight5 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage6 : {
                type: accedo.ui.image,
                css:  'highlight6 highLight6 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage7 : {
                type: accedo.ui.image,
                css:  'highlight7 highLight7 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            stepLabel : {
                type: accedo.ui.label,
                id:   'stepTag',
                css:  'step7tag label',
                text: '7'
            },
            backButtonCss : 'backBT button medium',
            continueButtonCss : 'continueBT button medium',
            backText : 'Back',
            continueText : 'Continue',
            corePanel : [
            {
                type: accedo.ui.label,
                css:  'step7label1 label',
                text: 'To make it easier to identify purchases made from this ' + deviceName + ' in your account, you can edit the ' + deviceName + ' name below.'
            },
            {
                type: accedo.ui.label,
                css:  'step7label2 label',
                text: deviceName + ' Name'
            },
            {
                type: accedo.ui.layout.normal,
                css : 'inputField2',
                children:[
                {
                    type: keyboard,
                    id: 'inputField',
                    css: 'pageInputField TVnameInputField',
                    nextDown:  'continueButton',
                    params:{
                        title: deviceName+ " Name",
                        width: 480,
                        maxlength: 25
                    }
                }
                ]
            },
            {
                type: accedo.ui.label,
                css:  'step7label3 label',
                text: "Some recommended examples:<br/>&middot; Justin's "
                +deviceName+"<br/>&middot; Bedroom "
                +deviceName+"<br/>&middot; Lounge "+deviceName
            }
            ]

        }));

        return viewObj;
    });