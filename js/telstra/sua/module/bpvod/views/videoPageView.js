/**
 * @fileOverview
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 */
accedo.define('telstra.sua.module.bpvod.views.videoPageView',
    ['accedo.ui.layout.normal',
    'accedo.ui.label',
    'accedo.ui.button'],
    function() {

        return {
           
            type:   accedo.ui.layout.normal,
            id:     'bpvod_canvas',
            css:    'background2',

            children: [
            {
                type: accedo.ui.layout.normal,
                css:  'pageStore',
                children: [
                {
                    type: accedo.ui.layout.normal,
                    css:  'page videoPage',
                    children: [
                    {
                        type: accedo.ui.layout.normal,
                        id: 'videoContainer',
                        children:[]
                    },
                    {
                        type: accedo.ui.layout.normal,
                        css:  'progressBarHolder',
                        id: 'progressBarHolder',
                        nextUp: null,
                        nextDown: null,
                        children:[
                        {
                            type : accedo.ui.layout.normal,
                            css: 'metering',
                            id: 'metering'
                        },
                        {
                            type: accedo.ui.layout.normal,
                            css:  'progressBarBg',
                            children:[]
                        },
                        {
                            type: accedo.ui.layout.normal,
                            children:[
                            	{
		                            type: accedo.ui.layout.normal,
		                            id: 'progressBarStopButton',
		                            css:  'progressBarButton stop',
		                            focusable : true,
		                            nextUp: null,
		                            nextDown: null,
		                            nextLeft: null,
		                            nextRight: 'progressBarBWButton',
		                            children:[]
		                        }
		                    ]
                        },
                        {
                            type: accedo.ui.layout.normal,
                            children:[
                            	{
		                            type: accedo.ui.layout.normal,
		                            id: 'progressBarBWButton',
		                            css:  'progressBarButton bw',
		                            focusable : true,
		                            nextUp: null,
		                            nextDown: null,
		                            nextLeft: 'progressBarStopButton',
		                            nextRight: 'progressBarPlayPauseButton',
		                            children:[]
		                        }
		                    ]
                        },
                        {
                            type: accedo.ui.layout.normal,
                            children:[
                            	{
	                            	type: accedo.ui.layout.normal,
		                            id: 'progressBarPlayPauseButton',
		                            css:  'progressBarButton big pause',
		                            focusable : true,
		                            nextUp: null,
		                            nextDown: null,
		                            nextLeft: 'progressBarBWButton',
		                            nextRight: 'progressBarFFButton',
		                            children:[]
		                        }
                            ]
                        },
                        {
                            type: accedo.ui.layout.normal,
                            children:[
                            	{
		                            type: accedo.ui.layout.normal,
		                            id: 'progressBarFFButton',
		                            css:  'progressBarButton ff',
		                            focusable : true,
		                            nextUp: null,
		                            nextDown: null,
		                            nextLeft: 'progressBarPlayPauseButton',
		                            nextRight: null,
		                            children:[]
		                        }
		                   ]
                        },
                        {
                            type: accedo.ui.layout.normal,
                            id: 'track',
                            css:  'progressBarContainer',
                            children:[]
                        },
                        {
                            type: accedo.ui.layout.normal,
                            id: 'trackBall',
                            css:  'trackBall',
                            children:[]
                        },
                        {
                            type:   accedo.ui.label,
                            css:    'curTimeLabel label',
                            id:     'curTime',
                            text:   '0:00:00'
                        },
                        {
                            type:   accedo.ui.label,
                            css:    'durationLabel label',
                            id:    'duration',
                            text:   '0:00:00'
                        },
                        {
                            type:   accedo.ui.label,
                            css:    'statusLabel label',
                            id:     'statusLabel',
                            text:   'Stopped'
                        }/*,
                        {
                            type: accedo.ui.layout.normal,
                            css:  'meterBars',
                            id:   'meterBars',
                            children:[]  
                        }*/
                        ]
                    }
                    ]
                }
                ]
            }
            ]
        };
    });