/**
 * @fileOverview forgotPinView
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.views.forgotPinView", [
    "accedo.ui.layout.absolute","accedo.ui.label","accedo.ui.image", "accedo.ui.list",
    "telstra.sua.module.bpvod.components.fluidButton","telstra.sua.core.components.keyboard","telstra.sua.core.components.keyboard_new"],
    function(){
        //var keyboard = (accedo.device.manager.identification.getFirmwareYear() == "2011" ? telstra.sua.core.components.keyboard : telstra.sua.core.components.keyboard_new);
        var keyboard = telstra.sua.core.components.keyboard;

        return {
            type:   accedo.ui.layout.absolute,
            id:     'bpvod_canvas',
            css:    'background2',
            children: [
            {
                type: accedo.ui.layout.absolute,
                css:  'pageStore',
                children: [
                {
                    type: accedo.ui.layout.absolute,
                    css:  'page forgottenPin',
                    children: [
                    {
                        type: accedo.ui.layout.absolute,
                        css:  'panel focus',
                        children:[
                        {
                            type: accedo.ui.layout.normal,
                            id:   'mainVerticalPanel',
                            css:  'mainVerticalPanel panel vertical focus',
                            children:[
                            {
                                type: accedo.ui.layout.normal,
                                css:  'backgroundBorder'
                            },
                            {
                                type : accedo.ui.label,
                                css: 'innerHeader label',
                                text : 'Forgotten Account PIN:Sign In'
                            },
                            {
                                type : accedo.ui.label,
                                css: 'labelInstruction label',
                                text : 'Please Sign in using your BigPond Movies Downloads nominated email address &amp; password'
                            },
                            {
                                type : accedo.ui.label,
                                css: 'labelEmail label',
                                text : 'Email address:'
                            },
                            {
                                type:           keyboard,
                                id:             'emailaddress',
                                css:            'pageInputField emailInputFieldSet',
                                nextDown:       'password',
                                                        
                                params:{
                                    title: "Email Address",
                                    width: 445,
                                    maxlength: 50
                                } 
                            },
                            {
                                type : accedo.ui.label,
                                css: 'labelPassword label',
                                text : 'Password:'
                            },
                            {
                                type:           keyboard,
                                id:             'password',
                                css:            'pageInputField passwordInput',
                                nextDown:       'saveButton',
                                nextUp:         'emailaddress',

                                params:{
                                    title: "Password",
                                    width: 445,
                                    maxlength: 50,
                                    isPassword:true
                                }
                            },
                            {
                                type : accedo.ui.label,
                                css: 'labelForgotten label',
                                text : 'Forgotten  your password?'
                            },
                            {
                                type : accedo.ui.label,
                                css: 'labelCall label',
                                text : "Call us on 1800 502 502 and we'll help you to regain access to your account."
                            },
                            {
                                type : accedo.ui.layout.normal,
                                css : 'inbuttonPanel panel horizontal',
                                children : [
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'saveButton',
                                    css:       'continueButton medium green button',
                                    text:      'Continue',
                                    width:     '174px',
                                    nextRight: 'closeButton',
                                    nextUp:    'password'
                                },
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'closeButton',
                                    css:       'cancelbutton medium green button',
                                    text:      'Cancel',
                                    width:     '174px',
                                    nextLeft:  'saveButton',
                                    nextUp:    'password'
                                }
                                ]
                            }
                            ]
                        }
                        ]
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.absolute,
                css:  'bigPondHeader',
                children: [
                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: ''
                }
                ]
            }
            ]
        };
    });