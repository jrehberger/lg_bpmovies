/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.views.joinNowTemplateView", [
    "accedo.ui.layout.normal","accedo.ui.layout.absolute","accedo.ui.label","accedo.ui.image","accedo.ui.list", "accedo.device.manager",
    "telstra.sua.module.bpvod.components.radioButton","telstra.sua.module.bpvod.components.fluidButton","telstra.sua.core.components.keyboard","telstra.sua.core.components.keyboard_new"],
    function(){


        return function(opts){

            if(!opts){
                opts = {};
            }

            var corePanelRef = opts.corePanel || [],
            pageTitleRef = opts.pageTitle ||"",
            highlightImage2Ref = opts.highlightImage2 || "",
            highlightImage3Ref = opts.highlightImage3 || "",
            highlightImage4Ref = opts.highlightImage4 || "",
            highlightImage5Ref = opts.highlightImage5 || "",
            highlightImage6Ref = opts.highlightImage6 || "",
            highlightImage7Ref = opts.highlightImage7 || "",
            highlightImage8Ref = opts.highlightImage8 || "",
            stepLabelRef = opts.stepLabel || {
                type: accedo.ui.label,
                css:  'step1tag label',
                text: '1'
            },
            backTextRef = opts.backText || 'Back',
            continueTextRef = opts.continueText || 'Continue',
            backButtonCss = opts.backButtonCss || 'saveButton button medium',
            continueButtonCss = opts.continueButtonCss || 'closeButton button medium',
            pageCss = opts.pageCss || 'page joinSteps',
            signInTitle = opts.signInTitle || 'StepTitle',
            stepPanelCss = opts.stepPanelCss || 'StepPanel panel';

            return {

                type:   accedo.ui.layout.absolute,
                id:     'bpvod_canvas',
                css:    'background2',

                children: [
                {
                    type: accedo.ui.layout.absolute,
                    css:  'pageStore',

                    children: [
                    {
                        type: accedo.ui.layout.absolute,
                        css:  pageCss,

                        children: [
                        {
                            type: accedo.ui.label,
                            id: 'StepTitle',
                            css:  signInTitle
                        },
                        {
                            type: accedo.ui.image,
                            css:  'imgJoinNowSecure',
                            src: 'images/module/bpvod/pages/joinNow/steps/secure.png'
                        },
                        {
                            type: accedo.ui.layout.absolute,
                            id:   'StepPanel',
                            css:  stepPanelCss,

                            children:[
                            {
                                type: accedo.ui.layout.absolute,
                                css:  'panel vertical',

                                children: [
                                {
                                    type: accedo.ui.label,
                                    css: 'subtitle label',
                                    id: 'pageTitle',
                                    text: pageTitleRef
                                },
                                {
                                    type: accedo.ui.image,
                                    css:  'highlight1 highLight1 image',
                                    src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
                                },
                                highlightImage2Ref,
                                highlightImage3Ref,
                                highlightImage4Ref,
                                highlightImage5Ref,
                                highlightImage6Ref,
                                highlightImage7Ref,
                                highlightImage8Ref,
                                stepLabelRef,
                                {
                                    type: accedo.ui.layout.normal,
                                    css: '',
                                    width : '600px',
                                    children : corePanelRef
                                },
                                {
                                    type:  accedo.ui.layout.absolute,
                                    css:   'backContinuePanel panel horizontal',
                                    y: '379px',
                                    width: '468px',
                                    children: [
                                    {
                                        type:      telstra.sua.module.bpvod.components.fluidButton,
                                        id:        'backButton',
                                        css:       backButtonCss,
                                        text:      backTextRef,
                                        width:     '198px',
                                        nextRight: 'continueButton'
                                    },
                                    {
                                        type:      telstra.sua.module.bpvod.components.fluidButton,
                                        id:        'continueButton',
                                        css:       continueButtonCss,
                                        text:      continueTextRef,
                                        width:     '198px',
                                        nextLeft:  'backButton'
                                    }
                                    ]
                                }
                                ]
                            }
                            ]
                        }
                        ]
                    }
                    ]
                },
                {
                    type: accedo.ui.layout.absolute,
                    css:  'bigPondHeader',

                    children: [
                    {
                        type: accedo.ui.label,
                        css: 'onNetFlag isOnNet'
                    }
                    ]
                }
                ]
            }
        };
    });