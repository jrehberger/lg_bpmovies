/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.views.joinNowStep6PinSettingView", [
    "telstra.sua.module.bpvod.views.joinNowTemplateView"],
    function(){

        var viewObj = accedo.utils.object.clone(telstra.sua.module.bpvod.views.joinNowTemplateView({
            pageTitle : "PIN Setting",
            highlightImage2 : {
                type: accedo.ui.image,
                css:  'highlight2 highLight2 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage3 : {
                type: accedo.ui.image,
                css:  'highlight3 highLight3 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage4 : {
                type: accedo.ui.image,
                css:  'highlight4pt1 highLight4pt1 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage5 : {
                type: accedo.ui.image,
                css:  'highlight5 highLight5 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage6 : {
                type: accedo.ui.image,
                css:  'highlight6 highLight6 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            stepLabel : {
                type: accedo.ui.label,
                id:   'stepTag',
                css:  'step6tag label',
                text: '6'
            },
            backButtonCss : 'backBT button medium',
            continueButtonCss : 'continueBT button medium',
            backText : 'Back',
            continueText : 'Continue',
            corePanel : [
            {
                type: accedo.ui.label,
                css:  'step6label1 label',
                text: 'You will always be required to enter your PIN to purchase content classified MA15+, R18+ and above.'
            },
            {
                type: accedo.ui.label,
                css:  'step6label2 label',
                text: 'We recommend that your PIN be entered for all transactions to avoid unauthorised use of your account.'
            },
            {
                type: accedo.ui.image,
                css:  'splitLine2 image',
                src: 'images/module/bpvod/pages/joinNow/steps/splitLine.png'
            },
            {
                type: accedo.ui.label,
                css:  'step6label3 label',
                text: 'Require a PIN for all transactions:'
            },
            {
                type: accedo.ui.layout.normal,
                css: 'pinSetting panel vertical',
                children: [
                {
                    type:      telstra.sua.module.bpvod.components.radioButton,
                    id:        'pinSettingYes',
                    css:       'pinSettingYes button',
                    text:      'Yes (recommended)',
                    nextDown:  'pinSettingNo'
                },
                {
                    type:      telstra.sua.module.bpvod.components.radioButton,
                    id:        'pinSettingNo',
                    css:       'pinSettingNo button',
                    text:      'No',
                    nextUp:    'pinSettingYes',
                    nextDown:  'continueButton'
                }
                ]
            }
            ]
        }));

        return viewObj;
    });