/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define('telstra.sua.module.bpvod.views.myAccountRedeemVoucherView',
    ['accedo.ui.layout.normal',
    'accedo.ui.label',
    'telstra.sua.core.components.keyboard',
    'telstra.sua.core.components.keyboard_new',
    'telstra.sua.module.bpvod.components.fluidButton'],
    function() {
        //var keyboard = (accedo.device.manager.identification.getFirmwareYear() == "2011" ? telstra.sua.core.components.keyboard : telstra.sua.core.components.keyboard_new);
        var keyboard = telstra.sua.core.components.keyboard;

        return {

            type:   accedo.ui.layout.normal,
            id:     'bpvod_canvas',
            css:    'background2',
            children:       [
            {
                type: accedo.ui.layout.normal,
                css:  'pageStore',
                children: [
                {
                    type: accedo.ui.layout.normal,
                    id:   'accountPageGeneralClass',
                    css:  'accountPageGeneralClass  account_redeemVoucher',
                    children: [
                    {
                        type: accedo.ui.layout.normal,
                        id: 'primePanel',
                        css: 'panal',
                        children: [
                        {
                            type: accedo.ui.layout.normal,
                            id: 'mainPanel',
                            css: 'mainPanel',
                            children: [
                            {
                                type: accedo.ui.layout.normal,
                                id: 'backgroundBorder',
                                css: 'backgroundBorder'
                            },
                            {
                                type: accedo.ui.label,
                                id: 'innerHeader',
                                css: 'innerHeader',
                                text: 'Redeem Voucher'
                            },
                            {
                                type: accedo.ui.layout.normal,
                                id: 'componentPanel',
                                css: 'componentPanel',
                                children: [
                                {
                                    type: accedo.ui.label,
                                    id: 'labelQuest',
                                    css: 'labelQuest',
                                    text: 'Please enter your 14 character voucher code below'
                                },
                                {
                                    type: accedo.ui.label,
                                    id: 'labelProm',
                                    css: 'labelProm',
                                    text: 'BigPond Movies vouchers are available at Telstra stores and participating retailers.'
                                },
                                {
                                    type: accedo.ui.label,
                                    id: 'labelCurrentVoucher',
                                    css: 'labelCurrentVoucher',
                                    text: ''
                                },
                                {
                                    type: keyboard,
                                    id: 'inputField',
                                    css: 'inputField voucherCodeInput',
                                    nextDown:  'saveButton',
                                    
                                    params:{
                                        title: "Voucher Code",
                                        width: 432,
                                        maxlength: 14,
                                        defaultCapitalInput:true
                                    } 
                                } ]
                            },
                            {
                                type: accedo.ui.layout.normal,
                                css:   'buttonPanel',
                                children: [
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'saveButton',
                                    css:       'saveButton button medium green',
                                    text:      'Save',
                                    width:     '174px',
                                    nextRight: 'cancelButton',
                                    nextUp:    'inputField'
                                },
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'cancelButton',
                                    css:       'cancelButton button medium green',
                                    text:      'Cancel',
                                    width:     '174px',
                                    nextLeft:  'saveButton',
                                    nextUp:    'inputField'
                                }
                                ]
                            }
                            ]
                        }
                        ]
                    },
                    {
                        type: accedo.ui.layout.normal,
                        id: 'titleText',
                        css: 'titleText'
                    },
                    {
                        type: accedo.ui.layout.normal,
                        id: 'securityIcon',
                        css: 'securityIcon'
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.normal,
                css:  'bigPondHeader',
                children: [
                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: '' 
                }
                ]
            }
            ]
        }
    });