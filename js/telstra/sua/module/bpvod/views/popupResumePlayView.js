/**
 * @fileOverview popupResumePlayView
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 */
accedo.define('telstra.sua.module.bpvod.views.popupResumePlayView',
    ['accedo.ui.layout.linear',
    'accedo.ui.layout.normal',
    'telstra.sua.core.components.truncatedLabel',
    "accedo.ui.label", "accedo.ui.button", "accedo.ui.image", "accedo.ui.list"],
    function() {

        var label   = accedo.ui.label,
        tLabel      = telstra.sua.core.components.truncatedLabel,
        layout      = accedo.ui.layout,
        image       = accedo.ui.image,
        button      = accedo.ui.fluidButton,
        list        = accedo.ui.list;

        return {

            type:layout.absolute,
            id:'popupBg',
            css : 'accedo-ui-popupcontent',
            children: [

            {

                type: layout.normal,
                id: 'popupNormalSoftExit',
                css: 'popupNormalSoftExit panel popup freeSoftExit focus',
                children: [
                {
                    id:       'popupTitle',
                    type:     tLabel,
                    css:      'header label',
                    innerLabel_css: 'header_inner label',
                    text:     ''
                },

                {
                    id: 'buttonPanel',
                    type: list,
                    css:      'verticalPanel panel vertical',
                    visibleSize : 3,
                    orientation:    accedo.ui.list.VERTICAL,
                    childTemplate : {
                        type:     button
                    }
                },
                {
                    type:     layout.normal,
                    css:      'videoItem videoItemButton',
                    children : [
                    {
                        type:     image,
                        css:      'videoItemBorder image',
                        src:      'images/module/bpvod/videoItem_new_unfocus3.png'
                    },
                    {
                        type:     layout.normal,
                        css:      'videoItemImageContainer',
                        children : [
                        {
                            type: layout.normal,
                            css:  'videoThumbnailBg'
                        },
                        {
                            id:     'videoItemImage',
                            type:     image,
                            css:      'videoItemImage image',
                            src:        ''
                        }
                        ]
                    },
                    {
                        id:       'episodeLabel',
                        type:     label,
                        css:      'episodeLabel label',
                        text:     ''
                    }
                    ]
                },
                {
                    id:       'textFieldLabel',
                    type:     label,
                    css:      'textField label',
                    text:     ''
                },
                {
                    id:       'rentDetailsLabel',
                    type:     label,
                    css:      'rentDetailsLabel label',
                    text:     ''
                }

                ]
            }
            ]

        }

    });
