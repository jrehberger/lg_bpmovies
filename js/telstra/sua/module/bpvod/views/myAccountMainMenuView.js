/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define('telstra.sua.module.bpvod.views.myAccountMainMenuView',
    ['accedo.ui.layout.normal',
    'accedo.ui.label',
    'accedo.ui.button'],
    function() {

        return {

            type:   accedo.ui.layout.normal,
            id:     'bpvod_canvas',
            css:    'background',

            children:       [
            {
                type: accedo.ui.layout.normal,
                id:   'pageStore',
                css:  'pageStore',
                children: [
                {
                    type: accedo.ui.layout.normal,
                    id:   'account_mainMenu',
                    css:  'account_mainMenu',
                    children: [
                    {
                        type: accedo.ui.layout.normal,
                        id: 'primePanel',
                        css: 'panal',
                        children: [
                        {
                            type: accedo.ui.layout.normal,
                            id: 'mainVerticalPanel',
                            css: 'mainVerticalPanel vertical',
                            children: [
                            {
                                type: accedo.ui.image,
                                id: 'imgAccountMenuBK',
                                css: 'imgAccountMenuBK',
                                src: 'images/module/bpvod/pages/SS-ACC-2_0_MyAccountMainMenu/ACC101_bg.png',
                                divImg: false
                            },
                            {
                                type: accedo.ui.label,
                                id: 'labelAccountInfo',
                                css: 'labelAccountInfo',
                                text: 'Changing your details here will update your account information everywhere you use BigPond Movies.'
                            },
                            {
                                type:           accedo.ui.list,
                                orientation:    accedo.ui.list.VERTICAL,
                                behaviour:      accedo.ui.list.CAROUSEL,
                                visibleSize:        5,
                                preloadSize:        5,
                                firstSelectedIndex: 2,

                                id:             'categoryPanel',
                                css:            'categoryPanel verticalStripButtonMenu',

                                childTemplate : {
                                    type:           accedo.ui.button,
                                    css:            'stripButton big'
                                }
                            },
                            {
                                type: accedo.ui.layout.normal,
                                css:  'upDownScrollerKey',
                                children: [
                                {
	                                type: accedo.ui.button,
	                                id:   'upScroller',
	                                css:  'upDownElement up'
	                            },
	                            {
	                                type: accedo.ui.button,
	                                id:   'downScroller',
	                                css:  'upDownElement down'
	                            }
                                ]
                            }
                            ]
                        }
                        ]
                    },
                    {
                        type: accedo.ui.image,
                        id: 'imgAccountMenuTitle',
                        css: 'imgAccountMenuTitle',
                        src: 'images/module/bpvod/pages/SS-ACC-1_1_MyAccountAuthentication/MyAccount_Text.png'
                    },
                    {
                        type: accedo.ui.image,
                        id: 'imgAccountMenuSecure',
                        css: 'imgAccountMenuSecure',
                        src: 'images/module/bpvod/pages/joinNow/steps/secure.png'
                    },
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.normal,
                css:  'bigPondHeader',
                children: [
                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: '' 
                }
                ]
            }
            ]
        }
    });