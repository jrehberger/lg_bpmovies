/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define('telstra.sua.module.bpvod.views.myAccountChangePasswordView',
    ['accedo.ui.layout.normal',
    'accedo.ui.label',
    'telstra.sua.module.bpvod.components.fluidButton',
    'telstra.sua.core.components.keyboard',
    'telstra.sua.core.components.keyboard_new'],
    function() {
        //var keyboard = (accedo.device.manager.identification.getFirmwareYear() == "2011" ? telstra.sua.core.components.keyboard : telstra.sua.core.components.keyboard_new);
        var keyboard = telstra.sua.core.components.keyboard;

        return {

            type:   accedo.ui.layout.normal,
            id:     'bpvod_canvas',
            css:    'background2',
            children:       [
            {
                type: accedo.ui.layout.normal,
                css:  'pageStore',
                children: [
                {
                    type: accedo.ui.layout.normal,
                    id:   'accountPageGeneralClass',
                    css:  'accountPageGeneralClass changePassword',
                    children: [
                    {
                        type: accedo.ui.layout.normal,
                        id: 'primePanel',
                        css: 'panal',
                        children: [
                        {
                            type: accedo.ui.layout.normal,
                            id: 'mainPanel',
                            css: 'mainPanel',
                            children: [
                                {
                        type: accedo.ui.layout.normal,
                        id: 'backgroundBorder',
                        css: 'backgroundBorder'
                    },
                    {
                        type: accedo.ui.label,
                        id: 'innerHeader',
                        css: 'innerHeader',
                        text: 'Change Password'
                    },
                            {
                                type: accedo.ui.layout.normal,
                                id: 'componentPanel',
                                css: 'componentPanel',
                                children: [
                                {
                                    type: accedo.ui.label,
                                    id: 'textLabel1',
                                    css: 'textLabel',
                                    text: 'Enter a new password (Minimum 6 characters)'
                                },
                                {
                                    type:           keyboard,
                                    id:             'inputField1',
                                    css:            'inputField',
                                    nextDown:       'inputField2',
                                    
                                    params:{
                                        title: "Password",
                                        width: 480,
                                        maxlength: 50,
                                        isPassword: true
                                    } 
                                },
                                {
                                    type: accedo.ui.label,
                                    id: 'textLabel2',
                                    css: 'textLabel',
                                    text: 'Confirm password'
                                },
                                {
                                    type:           keyboard,
                                    id:             'inputField2',
                                    css:            'inputField',
                                    nextDown:       'saveChangesButton',
                                    nextUp:         'inputField1',
                                    
                                    params:{
                                        title: "Confirm Password",
                                        width: 480,
                                        maxlength: 50,
                                        isPassword: true
                                    } 
                                }
                                ]
                            },
                            {
                                type: accedo.ui.layout.normal,
                                css:   'buttonPanel',
                                children: [
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'saveChangesButton',
                                    css:       'saveChangesDetailsButton button medium',
                                    text:      'Save Changes',
                                    width:     '174px',
                                    nextRight: 'closeButton',
                                    nextUp:    'inputField1'
                                },
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'closeButton',
                                    css:       'closeButton button medium',
                                    text:      'Close',
                                    width:     '174px',
                                    nextLeft:  'saveChangesButton',
                                    nextUp:    'inputField1'
                                }
                                ]
                            }
                            ]
                        }
                        ]
                    },
                    {
                        type: accedo.ui.layout.normal,
                        id: 'titleText',
                        css: 'titleText'
                    },
                    {
                        type: accedo.ui.layout.normal,
                        id: 'securityIcon',
                        css: 'securityIcon'
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.normal,
                css:  'bigPondHeader',
                children: [
                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: '' 
                }
                ]
            }
            ]
        }
    });