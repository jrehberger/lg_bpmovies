/**
 * @fileOverview
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.views.signInCongraView", [
    "accedo.ui.layout.absolute","accedo.ui.label","accedo.ui.image", "accedo.ui.list",
    "telstra.sua.module.bpvod.components.fluidButton"],
    function(){
        return {
            type:   accedo.ui.layout.absolute,
            id:     'bpvod_canvas',
            css:    'background',

            children: [
            {
                type: accedo.ui.layout.absolute,
                css:  'pageStore',

                children: [
                {
                    type: accedo.ui.layout.absolute,
                    css:  'page signIn',

                    children: [
                    {
                        type: accedo.ui.layout.normal,
                        css : 'panel joinSteps focus',
                        children: [
                        {
                            type: accedo.ui.layout.normal,
                            css : 'congraMainMenu panel focus',
                            children: [
                                {
                                    type: accedo.ui.image,
                                    css: 'imgbuffer image',
                                    src : 'images/module/bpvod/pages/SS-ACC-2_0_MyAccountMainMenu/ACC101_bg.png'
                                },
                                {
                                    type : accedo.ui.label,
                                    css : 'congraTitle label',
                                    text : 'Congratulations,'
                                },
                                {
                                    type : accedo.ui.label,
                                    css : 'congraLabel1 label',
                                    text : 'You are now a BigPond Movies member.'
                                },
                                {
                                    type : accedo.ui.label,
                                    css : 'congraLabel2 label',
                                    text : "You can rent and watch movies and TV shows anywhere<br/>you can access BigPond Movies."
                                },
                                {
                                    type: accedo.ui.image,
                                    css: 'congraSplitLine image',
                                    src : 'images/module/bpvod/pages/joinNow/steps/splitLine.png'
                                },
                                {
                                    type : accedo.ui.layout.normal,
                                    css : 'congraButtonPanel panel vertical focus',
                                    children : [
                                        {
                                            type:      telstra.sua.module.bpvod.components.fluidButton,
                                            id:        'movieButton',
                                            css:       'go2bpmv go2bpmv button long focus',
                                            text:      'Go to BigPond Movies',
                                            width:     '260px',
                                            nextDown: 'redeemButton'
                                        },
                                        {
                                            type:      telstra.sua.module.bpvod.components.fluidButton,
                                            id:        'redeemButton',
                                            css:       'redeemVocher redeemVocher button long',
                                            text:      'Redeem a voucher',
                                            width:     '260px',
                                            nextUp: 'movieButton'
                                        }
                                    ]
                                },
                                {
                                    type : accedo.ui.label,
                                    css : 'congraLabel3 label',
                                    text : 'Would you like to redeem a voucher?'
                                }
                            ]
                        }
                        ]
                    },
//                    {
//                        type: accedo.ui.label,
//                        id: 'signInTitle',
//                        css:  'signInTitle'
//                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.absolute,
                css:  'bigPondHeader',

                children: [

                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                }
                ]
            }
            ]
        };
    });