/**
 * @fileOverview searchPageView
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.views.searchPageView", [
    "accedo.ui.layout.absolute","accedo.ui.label","accedo.ui.image", "accedo.ui.list",
    "telstra.sua.module.bpvod.components.fluidButton", "telstra.sua.module.bpvod.components.optionField",
    "telstra.sua.core.components.keyboard","telstra.sua.core.components.keyboard_new"],
    function(){
        //var keyboard = (accedo.device.manager.identification.getFirmwareYear() == "2011" ? telstra.sua.core.components.keyboard : telstra.sua.core.components.keyboard_new);
        var keyboard = telstra.sua.core.components.keyboard;
        
        return {
            type:   accedo.ui.layout.absolute,
            id:     'bpvod_canvas',
            css:    'background2',

            children: [
            {
                type: accedo.ui.layout.absolute,
                css:  'pageStore',

                children: [
                {
                    type: accedo.ui.layout.absolute,
                    css:  'page searchPage',

                    children: [
                    {
                        type: accedo.ui.layout.absolute,
                        css:  'panel focus',

                        children:[
                        {
                            type: accedo.ui.layout.absolute,
                            css:  'vp panel vertical focus',

                            children: [
                            {
                                type:     telstra.sua.module.bpvod.components.optionField,
                                id:       'searchTypeField',
                                css:      'optionField searchTypeField',
                                width:    '247px',
                                options:  ['Movies','TV shows'],
                                nextDown: 'searchByField'
                            },
                            {
                                type:     telstra.sua.module.bpvod.components.optionField,
                                id:       'searchByField',
                                css:      'optionField searchByField',
                                width:    '247px',
                                options:  ['Title','Director/Actor'],
                                nextUp:   'searchTypeField',
                                nextDown: 'searchInputField'
                            },
                            {
                                type:           keyboard,
                                id:             'searchInputField',
                                css:            'searchInput',
                                nextUp:         'searchByField',
                                nextDown:       'searchButton',
                                                        
                                params:{
                                    title: "Search",
                                    width: 374,
                                    maxlength: 40
                                }
                            },
                            {
                                type: accedo.ui.layout.absolute,
                                css:  'hp panel horizontal',

                                children: [
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    css:       'searchButton button force-pos-rel',
                                    id:        'searchButton',
                                    text:      'Search',
                                    width:     '180px',
                                    nextUp:    'searchInputField',
                                    nextRight: 'newSearchButton'
                                },
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    css:       'newSearchButton button force-pos-rel',
                                    id:        'newSearchButton',
                                    text:      'New Search',
                                    width:     '180px',
                                    nextUp:    'searchInputField',
                                    nextLeft:  'searchButton'
                                }
                                ]
                            }
                            ]
                        }
                        ]
                    },
                    {
                        type: accedo.ui.image,
                        css:  'searchIcon image',
                        src:  'images/module/bpvod/pages/search/searchIcon.png'
                    },
                    {
                        type: accedo.ui.label,
                        css:  'searchTitle label',
                        text: 'Search'
                    },
                    {
                        type: accedo.ui.label,
                        css:  'searchOverlay'
                    },
                    {
                        type: accedo.ui.label,
                        css:  'searchTypeLabel label',
                        text: 'Search'
                    },
                    {
                        type: accedo.ui.label,
                        css:  'searchByLabel label',
                        text: 'Search By'
                    },
                    {
                        type: accedo.ui.label,
                        css:  'searchInputLabel label',
                        text: 'Search for'
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.absolute,
                css:  'bigPondHeader',

                children: [

                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: ''
                }
                ]
            }
            ]
        };
    });