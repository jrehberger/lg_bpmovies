/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define('telstra.sua.module.bpvod.views.myAccountChangeNewsSubscripView',
    ['accedo.ui.layout.normal',
    'accedo.ui.label',
    'telstra.sua.module.bpvod.components.radioButton',
    'telstra.sua.module.bpvod.components.fluidButton'],
    function() {

        return {

            type:   accedo.ui.layout.normal,
            id:     'bpvod_canvas',
            css:    'background2',
            children:       [
            {
                type: accedo.ui.layout.normal,
                css:  'pageStore',
                children: [
                {
                    type: accedo.ui.layout.normal,
                    id:   'accountPageGeneralClass',
                    css:  'accountPageGeneralClass ChangeNewsSubscrip',
                    children: [
                    {
                        type: accedo.ui.layout.normal,
                        id: 'primePanel',
                        css: 'panal',
                        children: [
                        {
                            type: accedo.ui.layout.normal,
                            id: 'mainPanel',
                            css: 'mainPanel',
                            children: [
                            {
                                type: accedo.ui.layout.normal,
                                id: 'backgroundBorder',
                                css: 'backgroundBorder'
                            },
                            {
                                type: accedo.ui.label,
                                id: 'innerHeader',
                                css: 'innerHeader',
                                text: 'Newsletter subscription'
                            },
                            {
                                type: accedo.ui.label,
                                id: 'minorLabel',
                                css: 'minorLabel',
                                text: 'I\'d like to receive email news and special offers for BigPond Movies and related products and services.'
                            },
                            {
                                type: accedo.ui.layout.normal,
                                id: 'componentPanel',
                                css: 'componentPanel',
                                children: [
                                {
                                    type: accedo.ui.layout.normal,
                                    id: 'settingChoose',
                                    css: 'settingChoose',
                                    children: [
                                    {
                                        type:      telstra.sua.module.bpvod.components.radioButton,
                                        id:        'option1',
                                        css:       'option1 button',
                                        text:      'Yes',
                                        nextDown:  'option2'
                                    },
                                    {
                                        type:      telstra.sua.module.bpvod.components.radioButton,
                                        id:        'option2',
                                        css:       'option2 button',
                                        text:      'No',
                                        nextUp:    'option1',
                                        nextDown:  'saveChangesButton'
                                    }
                                    ]
                                }
                                ]
                            },
                            {
                                type: accedo.ui.layout.normal,
                                css:   'buttonPanel',
                                children: [
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'saveChangesButton',
                                    css:       'saveChangesDetailsButton button medium',
                                    text:      'Save Changes',
                                    width:     '174px',
                                    nextRight: 'closeButton',
                                    nextUp:    'option2'
                                },
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'closeButton',
                                    css:       'closeButton button medium',
                                    text:      'Close',
                                    width:     '174px',
                                    nextLeft:  'saveChangesButton',
                                    nextUp:    'option2'
                                }
                                ]
                            }
                            ]
                        }
                        ]
                    },
                    {
                        type: accedo.ui.layout.normal,
                        id: 'titleText',
                        css: 'titleText'
                    },
                    {
                        type: accedo.ui.layout.normal,
                        id: 'securityIcon',
                        css: 'securityIcon'
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.normal,
                css:  'bigPondHeader',
                children: [
                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: '' 
                }
                ]
            }
            ]
        }
    });