/**
 * @fileOverview
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 */

accedo.define(
    "telstra.sua.module.bpvod.views.signInORjoinNowView",[
    "accedo.ui.layout.absolute","accedo.ui.label","accedo.ui.image",
    "telstra.sua.module.bpvod.components.imageTextButton"],
    function(){
        return {
            type:   accedo.ui.layout.absolute,
            id:     '#bpvod_canvas',
            css:    'background2',

            children: [
            {
                type: accedo.ui.layout.absolute,
                css:  'pageStore',

                children: [
                {
                    type: accedo.ui.layout.absolute,
                    css:  'page signInORjoinNow',

                    children: [
                    {
                        type: accedo.ui.layout.absolute,
                        css:  'panel focus',

                        children: [
                        {
                            type: accedo.ui.layout.absolute,
                            css:  'panel horizontal focus',

                            children: [
                            {
                                type: telstra.sua.module.bpvod.components.imageTextButton,
                                id:   'signInBT',
                                view: {
                                    type:          accedo.ui.layout.absolute,
                                    id:            "bpvod-viewbutton-layout1",
                                    css:           "bpvod-viewbutton-layout sign-in-btn button",
                                    children:[
                                    {
                                        type: accedo.ui.label,
                                        css:  'sign-in-label label',
                                        text: 'If you\'re an existing<br/>BigPond Movies member'
                                    }
                                    ]
                                },
                                nextRight: 'joinNowBT'
                            },
                            {
                                type: telstra.sua.module.bpvod.components.imageTextButton,
                                id:   'joinNowBT',
                                view: {
                                    type:          accedo.ui.layout.absolute,
                                    id:            "bpvod-viewbutton-layout2",
                                    css:           "bpvod-viewbutton-layout join-now-btn button",
                                    children: [
                                    {
                                        type: accedo.ui.label,
                                        css:  'join-now-label label',
                                        text: '<br/>Select to Join Now on your TV or go online to register at http://go.bigpond.com/tv/join'
                                    }
                                    ]
                                },
                                nextLeft: 'signInBT'
                            }
                            ]
                        }
                        ]
                    },
                    {
                        type: accedo.ui.label,
                        css:  'titleText'
                    },
                    {
                        type: accedo.ui.label,
                        css:  'unmertered unmerteredBT button'
                    },
                    {
                        type: accedo.ui.image,
                        css:  'shadow3 unmerteredBTshadow image',
                        src:  'images/module/bpvod/pages/joinNow/unmerteredBTShadow.png'
                    },
                    {
                        type: accedo.ui.image,
                        css:  'star1 image',
                        src:  'images/module/bpvod/pages/joinNow/star1.png'
                    },
                    {
                        type: accedo.ui.image,
                        css:  'star2 image',
                        src:  'images/module/bpvod/pages/joinNow/star2.png'
                    },
                    {
                        type: accedo.ui.image,
                        css:  'star3 image',
                        src:  'images/module/bpvod/pages/joinNow/star3.png'
                    },
                    {
                        type: accedo.ui.image,
                        css:  'star4 image',
                        src:  'images/module/bpvod/pages/joinNow/star3.png'
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.absolute,
                css:  'bigPondHeader',

                children: [

                {
                    type: accedo.ui.label,
                    //css: (telstra.sua.core.main.onOffNet? 'onNetFlag isOnNet' : 'onNetFlag isOffNet')
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: ''
                }
                ]
            }
            ]
        };
    });

