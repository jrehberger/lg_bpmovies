/**
 * @fileOverview movieDetailsView Telstra BigPond VoD Western Digital Widget
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 * Orignal author: Ho
 * Date: 2010-09-03
 *
 * document reference:
 * 1) 20100715_BigPond_Movies_on_LG_NetCastTV_1.8.2.pdf    p.66-73
 * 2) BigPondUIandFuncSpec.pdf (20100823 version)    p.63-67
 *
 * To do issue:
 * 1) history back issue
 *
 * Note:
 * 1) now the page would be shared by movieDetails, tvShowsEpisode List, search results, singleMovieItem
 */


accedo.define(
    "telstra.sua.module.bpvod.views.movieDetailsView",
    [
    "accedo.ui.layout.absolute",
    "accedo.ui.label",
    "accedo.ui.list",
    "accedo.ui.button",
    "accedo.ui.image",
    "accedo.ui.scrollbar",
    "accedo.ui.layout.relative",
    "telstra.sua.module.bpvod.components.videoPreviewBox"
    ],
    function(){
        return {
            type:   accedo.ui.layout.absolute,
            id:     'bpvod_canvas',
            css:    'background2',

            children: [
            {
                type: accedo.ui.layout.absolute,
                css:  'pageStore',
                children: [
                {
                    id:   'mediaTypeLabel',
                    type: accedo.ui.layout.absolute,
                    css:  'page movieDetailsPage nonCarousel force-pos-rel',

                    children: [
                    {
                        type: accedo.ui.layout.absolute,
                        css:  'panel focus force-pos-rel',

                        children: [
                        {
                            type: accedo.ui.layout.absolute,
                            css:  'resultPanel horizontalVideoRollingMenu panel focus',

                            children: [
                            {
                                //List-specific properties
                                type:        accedo.ui.list,
                                id:          '#movie_horizontal_listing',
                                css:         'panel horizontal focus l51r51',
                                orientation: accedo.ui.list.HORIZONTAL,
                                //Behaviour: CAROUSEL or REGULAR
                                behaviour:   accedo.ui.list.REGULAR,
                                x:           '645px',
                                y:			 '5px',
                                //REGULAR ONLY: How many children to display. -1 means all of them (default).
                                visibleSize: 3,
                                //REGULAR ONLY: How many children to cache in advance (created but not displayed). Default is 0.
                                preloadSize: 60,
                                //REGULAR ONLY: when 'moving' the list forward or backward, what is the step used?
                                scrollStep:  3,
                                loop: true,

                                childTemplate: {
                                    type:   accedo.ui.layout.absolute,
                                    css:     'videoItemImage image ',
                                    width:   '181px',
                                    height:  '247px',
                                    children: [
                                    {
                                        type: accedo.ui.image,
                                        id:   'bpvod_videoItemImage',
                                        css:  'pt2w113h160'
                                    },
                                    {
                                        type: accedo.ui.label,
                                        id:   'episodeLabel',
                                        css:  'episodeLabel'
                                    },
                                    {
                                        type: accedo.ui.label,
                                        css:  'pt2w113h160 videoItemImageBg'
                                    }
                                    ]
                                },

                                //Other inherited properties
                                widthStyle:  accedo.ui.component.FIT_PARENT,
                                heightStyle: accedo.ui.component.FIT_PARENT,
                                height:      '250px',
                                width:       '100%',
                                nextDown:    'Backward'
                            },
                            {
                                type: accedo.ui.layout.absolute,
                                css:  'panel horizontal l51r51',
                                children:[
                                {
                                    type: accedo.ui.layout.absolute,
                                    css:  'leftRightScrollerKey',
                                    x:    '-126px',
                                    y:    '45px',
                                    children: [
                                    {
                                        type: accedo.ui.button,
                                        id:   'scrollerKeyLeft',
                                        css:  'leftElement'
                                    },
                                    {
                                        type: accedo.ui.button,
                                        id:   'scrollerKeyRight',
                                        css:  'rightElement'
                                    }
                                    ]
                                }
                                ]
                            },
                            {
                                type: accedo.ui.scrollbar,
                                id:   'hScrollbar',
                                css:  'horizontalScrollbar',
                                trackTopping : false,
                                orientation: accedo.ui.scrollbar.HORIZONTAL
                            },
                            ]
                        },
                        {
                            type: accedo.ui.layout.absolute,
                            id:   'leftUpperPanel',
                            css:   'leftUpperPanel vishide',
                            children: [
                            {
                                type: accedo.ui.label,
                                css: 'leftUpperPanelTitle',
                                text: 'Search Result'
                            },
                            {
                                type: accedo.ui.label,
                                id: 'leftUpperPanelSubTitle1',
                                css: 'leftUpperPanelSubTitle1',
                                text: ''
                            },
                            {
                                type: accedo.ui.label,
                                id: 'leftUpperPanelSubTitle2',
                                css: 'leftUpperPanelSubTitle2',
                                text: ''
                            },
                            ]
                        },
                        {
                            id:   'leftPanelMovieInfo',
                            type: accedo.ui.layout.absolute,
                            css:  'detailsPageLeftPanel panel',
                            children: [
                            {
                                type: accedo.ui.label,
                                id:   'headerL',
                                css:  'headerLabel',
                                text: ''
                            },
                            {
                                type: accedo.ui.layout.absolute,
                                css:  'contentPanel panel',
                                children: [
                                {
                                    type: accedo.ui.layout.absolute,
                                    css:  'scrollableContentPanel panel',
                                    children: [
                                    {
                                        type: accedo.ui.layout.normal,
                                        id: 'textContainer',
                                        css:  'container panel textContainer',
                                        children: [
                                        {
                                            type:   accedo.ui.layout.absolute,
                                            id:     'textContent',
                                            css:    'content panel textContent',
                                            children: [
                                            {
                                                type:   accedo.ui.label,
                                                css:    'firstLine blueText label force-pos-rel',
                                                id:     'firstLine',
                                                text:   ''
                                            },
                                            {
                                                type:   accedo.ui.label,
                                                css:    'secondLine blueText label force-pos-rel',
                                                id:     'secondLine',
                                                text:   ''
                                            },
                                            {
                                                type:   accedo.ui.label,
                                                css:    'thirdLine blueText label force-pos-rel',
                                                id:     'thirdLine',
                                                text:   ''
                                            },
                                            {
                                                type:   accedo.ui.label,
                                                css:    'fourthLine blueText label force-pos-rel',
                                                id:     'fourthLine',
                                                text:   ''
                                            },
                                            {
                                                type: accedo.ui.label,
                                                css:  'horizontalBar force-pos-rel'
                                            },
                                            {
                                                type:   accedo.ui.label,
                                                css:    'synopsisLabel label force-pos-rel',
                                                id:     'synopsis',
                                                text:   ''
                                            }
                                            ]
                                        }
                                        ]
                                    },
                                    {
					                    type: accedo.ui.layout.absolute,
					                    id:   'scrollerContainer',
					                    css:  'upDownScrollerKey',
					                    children: [
					                    {
					                        type: accedo.ui.button,
					                        id:   'upScrollerText',
					                        css:  'upDownElement up'
					                    },
					                    {
						                    type : accedo.ui.scrollbar,
	                                        css : "verticalScrollbar",
	                                        id : "vScrollbar",
	                                        trackTopping : false
						                },
					                    {
					                        type: accedo.ui.button,
					                        id:   'downScrollerText',
					                        css:  'upDownElement down'
					                    }
					                    ]
					                }
                                    ]
                                }
                                ]
                            }
                            ]
                        },
                        {
                            //Related Movies choose people panel
                            id:   'leftPanelChoosePeople',
                            type: accedo.ui.layout.absolute,
                            css:  'detailsPageLeftPanel panel',
                            children: [
                            {
                                type: accedo.ui.label,
                                id:   'headerLRelated',
                                css:  'headerLabel',
                                text: ''
                            },
                            {
                                type: accedo.ui.layout.absolute,
                                css:  'contentPanel panel',
                                children: [
                                {
                                    type: accedo.ui.layout.absolute,
                                    css:  'relatedContentPanel panel',
                                    children: [
                                    {
                                        type: accedo.ui.layout.absolute,
                                        css:  'relatedPanel verticalStripButtonMenu panel mini2',
                                        children: [
                                        {
                                            type:        accedo.ui.list,
                                            css:         'panel vertical',
                                            id:          '#movie_related_listing',
                                            orientation: accedo.ui.list.VERTICAL,
                                            behaviour:   accedo.ui.list.CAROUSEL,
                                            visibleSize: 5,
                                            firstSelectedIndex: 2,
                                            preloadSize: 5,
                                            scrollStep:  1,
                                            focusable:   false,
                                            childTemplate: {
                                                type:    accedo.ui.button,
                                                css:     'stripButton',
                                                width:   '372px',
                                                height:  '40px'
                                            },
                                            widthStyle:  accedo.ui.component.FIT_PARENT,
                                            heightStyle: accedo.ui.component.FIT_PARENT,
                                            width:       '372px',
                                            height:      '230px'
                                        },
                                        {
                                            type:        accedo.ui.layout.absolute,
                                            css:         'upDownScrollerKey',

                                            children: [
                                            {
			                                    type: accedo.ui.button,
			                                    id:   'upScroller',
			                                    css:  'upDownElement up'
			                                },
			                                {
			                                    type: accedo.ui.button,
			                                    id:   'downScroller',
			                                    css:  'upDownElement down'
			                                }
                                            ]
                                        },
                                        {
                                            type:        accedo.ui.label,
                                            css:         'relatedPanelLabel label',
                                            text:        'Related Movies'
                                        }
                                        ]
                                    }
                                    ]
                                }
                                ]
                            }
                            ]
                        }
                        ]
                    },
                    {
                        type: telstra.sua.module.bpvod.components.videoPreviewBox,
                        id: 'videoPreviewBox',
                        css:  'videoPreviewBox panel'
                    },
                    {
                        type: accedo.ui.label,
                        id:   'titleText',
                        css:  'titleText'
                    },
                    {
                        type: accedo.ui.label,
                        id: 'categoryLabel',
                        css:  'categoryTitle label'
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.absolute,
                css:  'bigPondHeader',
                children: [
                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: ''
                }
                ]
            }
            ]
        };
    }
    );