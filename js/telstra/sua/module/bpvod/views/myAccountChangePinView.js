/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define('telstra.sua.module.bpvod.views.myAccountChangePinView',
    ['accedo.ui.layout.normal',
    'accedo.ui.label',
    'telstra.sua.module.bpvod.components.fluidButton',
    'telstra.sua.core.components.keyboard',
    'telstra.sua.core.components.keyboard_new'],
    function() {
        //var keyboard = (accedo.device.manager.identification.getFirmwareYear() == "2011" ? telstra.sua.core.components.keyboard : telstra.sua.core.components.keyboard_new);
        var keyboard = telstra.sua.core.components.keyboard;

        return {

            type:   accedo.ui.layout.normal,
            id:     'bpvod_canvas',
            css:    'background2',
            children:       [
            {
                type: accedo.ui.layout.normal,
                css:  'pageStore',
                children: [
                {
                    type: accedo.ui.layout.normal,
                    id:   'accountPageGeneralClass',
                    css:  'accountPageGeneralClass changePin',
                    children: [
                    {
                        type: accedo.ui.layout.normal,
                        id: 'primePanel',
                        css: 'panal',
                        children: [
                        {
                            type: accedo.ui.layout.normal,
                            id: 'mainPanel',
                            css: 'mainPanel',
                            children: [
                            {
                                type: accedo.ui.layout.normal,
                                id: 'backgroundBorder',
                                css: 'backgroundBorder'
                            },
                            {
                                type: accedo.ui.label,
                                id: 'innerHeader',
                                css: 'innerHeader',
                                text: 'Change Account PIN'
                            },
                            {
                                type: accedo.ui.layout.normal,
                                id: 'componentPanel',
                                css: 'componentPanel',
                                children: [
                                {
                                    type: accedo.ui.label,
                                    id: 'textLabel1',
                                    css: 'textLabel',
                                    text: 'Enter new 4 digit PIN'
                                },
                                {
                                    type:           keyboard,
                                    id:             'inputField1',
                                    css:            'inputField inputFieldBackground',
                                    nextDown:       'inputField2',
                                    
                                    params:{
                                        title: "PIN",
                                        width: 72,
                                        maxlength: 4,
                                        keyboardPosition: {
                                            x:118,
                                            y:139
                                        },
                                        isPassword:true,
                                        numericalInput:true
                                    } 
                                },
                                {
                                    type: accedo.ui.label,
                                    id: 'textLabel2',
                                    css: 'textLabel',
                                    text: 'Confirm PIN'
                                },
                                {
                                    type:           keyboard,
                                    id:             'inputField2',
                                    css:            'inputField inputFieldBackground',
                                    nextDown:  'saveChangesButton',
                                    nextUp:     'inputField1',
                                    
                                    params:{
                                        title: "PIN",
                                        width: 72,
                                        maxlength: 4,
                                        keyboardPosition: {
                                            x:118,
                                            y:139
                                        },
                                        isPassword:true,
                                        numericalInput:true
                                    } 
                                },
                                ]
                            },
                            {
                                type: accedo.ui.label,
                                id: 'noticeLabel',
                                css: 'noticeLabel',
                                text: 'Important: Your PIN will change everywhere you use it to access BigPond Movies.'
                            },
                            {
                                type: accedo.ui.layout.normal,
                                css:   'buttonPanel',
                                children: [
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'saveChangesButton',
                                    css:       'saveChangesDetailsButton button medium',
                                    text:      'Save Changes',
                                    width:     '174px',
                                    nextRight: 'cancelButton',
                                    nextUp:    'inputField2'
                                },
                                {
                                    type:      telstra.sua.module.bpvod.components.fluidButton,
                                    id:        'cancelButton',
                                    css:       'cancelButton button medium',
                                    text:      'Cancel',
                                    width:     '174px',
                                    nextLeft:  'saveChangesButton',
                                    nextUp:    'inputField2'
                                }
                                ]
                            }
                            ]
                        }
                        ]
                    },
                    {
                        type: accedo.ui.layout.normal,
                        id: 'titleText',
                        css: 'titleText'
                    },
                    {
                        type: accedo.ui.layout.normal,
                        id: 'securityIcon',
                        css: 'securityIcon'
                    }
                    ]
                }
                ]
            },
            {
                type: accedo.ui.layout.normal,
                css:  'bigPondHeader',
                children: [
                {
                    type: accedo.ui.label,
                    css: 'onNetFlag isOnNet'
                },
                {
                    type: accedo.ui.label,
                    css:  'versionLabel label',
                    text: '' 
                }
                ]
            }
            ]
        }
    });