/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.views.joinNowStep5PinView", [
    "telstra.sua.module.bpvod.views.joinNowTemplateView"],
    function(){
        //var keyboard = (accedo.device.manager.identification.getFirmwareYear() == "2011" ? telstra.sua.core.components.keyboard : telstra.sua.core.components.keyboard_new);
        var keyboard = telstra.sua.core.components.keyboard;

        var viewObj = accedo.utils.object.clone(telstra.sua.module.bpvod.views.joinNowTemplateView({
            pageTitle : "Create PIN",
            highlightImage2 : {
                type: accedo.ui.image,
                css:  'highlight2 highLight2 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage3 : {
                type: accedo.ui.image,
                css:  'highlight3 highLight3 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage4 : {
                type: accedo.ui.image,
                css:  'highlight4pt1 highLight4pt1 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            highlightImage5 : {
                type: accedo.ui.image,
                css:  'highlight5 highLight5 image',
                src:  'images/module/bpvod/pages/joinNow/steps/highLight.png'
            },
            stepLabel : {
                type: accedo.ui.label,
                id:   'stepTag',
                css:  'step5tag label',
                text: '5'
            },
            backButtonCss : 'backBT button medium',
            continueButtonCss : 'continueBT button medium',
            backText : 'Back',
            continueText : 'Continue',
            corePanel : [
            {
                type: accedo.ui.label,
                css:  'step3label1 label',
                text: 'Create your 4 digit PIN'
            },
            {
                type:           keyboard,
                id:             'PinInputField',
                css:            'pageInputField PinInputField',
                nextDown:       'PinConfirm',
                params:{
                    title: "PIN",
                    width: 72,
                    maxlength: 4,
                    keyboardPosition: {
                        x:118,
                        y:139
                    },
                    isPassword:true,
                    numericalInput:true
                }
            },
            {
                type: accedo.ui.label,
                css:  'step5label2 label',
                text: 'Confirm PIN'
            },
            {
                type:           keyboard,
                id:             'PinConfirm',
                css:            'pageInputField PinConfirm',
                nextUp:         'PinInputField',
                nextDown:       'continueButton',
                params:{
                    title: "Confirm PIN",
                    width: 72,
                    maxlength: 4,
                    keyboardPosition: {
                        x:118,
                        y:139
                    },
                    isPassword:true,
                    numericalInput:true
                }
            },
            {
                type: accedo.ui.label,
                css:  'step5label3 label',
                text: 'Child Safety information<br>Telstra strongly recommends that you restrict access by persons who may be under 18 years of age and may have access to BigPond Movies.'
            },
            {
                type: accedo.ui.image,
                css:  'step5label3Frame image',
                src:  'images/module/bpvod/pages/joinNow/steps/text_frame.png'
            },
            {
                type: accedo.ui.label,
                css:  'step5label4 label',
                text: 'We\'ll ask you to enter your Account PIN:<br>&middot; when you make a purchase<br>&middot; to prove that you meet the minimum<br>&nbsp;&nbsp;age requirements for viewing content<br>&nbsp; classified MA15+ and above.'
            }
            ]
        }));

        return viewObj;
    });