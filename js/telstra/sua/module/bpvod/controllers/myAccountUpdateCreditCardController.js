/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    'telstra.sua.module.bpvod.controllers.myAccountUpdateCreditCardController',
    ['accedo.utils.object',
    'accedo.ui.controller',
    'accedo.focus.manager',
    'telstra.api.usage',
    'telstra.sua.module.bpvod.views.myAccountUpdateCreditCardView'],
    function(){
        
        return function(opts){

            var utils = telstra.sua.module.bpvod.utils,
            views = telstra.sua.module.bpvod.views,
            optionsList = [],
            self, CCtype, CCnum1, CCnum2, CCnum3, CCnum4, CCnum5, CCnum6, CCExpMonth, CCExpYear, CCCVC;

            return accedo.utils.object.extend(accedo.ui.controller(opts), {

                onCreate: function() {
                    var closeButton, editButton,
                    creditCard = telstra.api.manager.movies.accountInfo.creditcard;

                    self = this;

                    //Set the view
                    self.setView(views.myAccountUpdateCreditCardView);

                    CCtype = self.get('ccTypeInput1');
                    CCnum1 = self.get('ccInput1');
                    CCnum2 = self.get('ccInput2');
                    CCnum3 = self.get('ccInput3');
                    CCnum4 = self.get('ccInput4');
                    CCnum5 = self.get('ccInput5');
                    CCnum6 = self.get('ccInput6');
                    CCExpMonth = self.get('expDateInput1');
                    CCExpYear = self.get('expDateInput2');
                    CCCVC = self.get('cvcInput1');
                    editButton = self.get('editButton');
                    closeButton = self.get('closeButton');

                    CCtype.deactivate();
                    CCtype.getInputField().addInputFieldClass("fill");
                    CCnum1.deactivate();
                    CCnum1.getInputField().addInputFieldClass("fill");
                    CCnum2.deactivate();
                    CCnum2.getInputField().addInputFieldClass("fill");
                    CCnum3.deactivate();
                    CCnum3.getInputField().addInputFieldClass("fill");
                    CCnum4.deactivate();
                    CCnum4.getInputField().addInputFieldClass("fill");
                    CCnum5.deactivate();
                    CCnum5.getInputField().addInputFieldClass("fill");
                    CCnum6.deactivate();
                    CCnum6.getInputField().addInputFieldClass("fill");
                    CCExpMonth.deactivate();
                    CCExpMonth.getInputField().addInputFieldClass("fill");
                    CCExpYear.deactivate();
                    CCExpYear.getInputField().addInputFieldClass("fill");
                    CCCVC.deactivate();
                    CCCVC.getInputField().addInputFieldClass("fill");

                    if (creditCard) {
                        if (creditCard.cardtype == "visa")
                            CCtype.setString("Visa");
                        else if (creditCard.cardtype == "mastercard")
                            CCtype.setString("MasterCard");
                        else
                            CCtype.setString("American Express");

                        CCnum1.setString(creditCard.maskedcardnumber.substr(0,4));

                        if (creditCard.cardtype != "amex") {
                            CCnum5.setVisible(false);
                            CCnum6.setVisible(false);

                            CCnum2.setString(creditCard.maskedcardnumber.substr(4,4));
                            CCnum3.setString(creditCard.maskedcardnumber.substr(8,4));
                            CCnum4.setString(creditCard.maskedcardnumber.substr(12,4));

                            CCCVC.setString('***');
                        }
                        else {
                            CCnum2.setVisible(false);
                            CCnum3.setVisible(false);
                            CCnum4.setVisible(false);

                            CCnum5.setString(creditCard.maskedcardnumber.substr(4,6));
                            CCnum6.setString(creditCard.maskedcardnumber.substr(10,5));

                            CCCVC.setString('****');
                        }

                        CCExpMonth.setString(creditCard.expirydate.substr(5,2));
                        CCExpYear.setString(creditCard.expirydate.substr(2,2));
                    }

                    editButton.addEventListener('click',function() {
                        utils.setDispatchController(self, 'myAccountCardTypeController', {}, false);
                    });

                    closeButton.addEventListener('click',function() {
                        self.dispatchEvent('telstra:core:historyBack');
                    });

                    optionsList.push(
						{
							type: "red",
							title: "Help"
						}
					);
					//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);

                    accedo.focus.manager.requestFocus(editButton);
                },

                onKeyRed : function() {
                    telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 20598);
                },

                onKeyBack: function() {
                    self.dispatchEvent('telstra:core:historyBack');
                }
            });
        }
    });