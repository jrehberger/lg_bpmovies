/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 *
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.signInStep8TCController", [
    // dependency
    "telstra.api.usage",
    "telstra.sua.module.bpvod.views.signInStep8TCView",
    "telstra.sua.module.bpvod.controllers.signInTemplateController"],
    function(){

        return function(opts){

            // Shorten for namespacing
            var utils = telstra.sua.module.bpvod.utils,
            self, leftPanel, termsContent, termsContainer, scrollbar, agreementYes, agreementNo, recieveMailYes, recieveMailNo, agree;

            var myPubObj = accedo.utils.object.extend(telstra.sua.module.bpvod.controllers.signInTemplateController(opts), {
    
                onCreate : function(){
                    self = this;

                    this.init(telstra.sua.module.bpvod.views.signInStep8TCView);

                    accedo.utils.fn.delay(function() {
                        leftPanel = self.get('leftPanel');
                        termsContent = self.get('termsContent');
                        termsContainer = self.get('termsContainer');
                        scrollbar = self.get('scrollbar');
                        agreementYes = self.get('agreementYes');
                        agreementNo = self.get('agreementNo');
                        recieveMailYes = self.get('recieveMailYes');
                        recieveMailNo = self.get('recieveMailNo');

                        termsContent.setText(telstra.sua.module.bpvod.config.termsTxt);

                        var containerHeight = termsContainer.getRoot().getHTMLElement().offsetHeight;
                        var currentHeight  = termsContainer.getRoot().getHTMLElement().scrollHeight;
                        var currentTop = termsContainer.getRoot().getHTMLElement().offsetTop;

                        var scrollText = function(direction){ //0 = not move, 1 = up, 2 = down
                            var targetTop;

                            if(direction == 1){
                                targetTop = currentTop + 95;
                                if(targetTop > 0){
                                    targetTop = 0;
                                }
                            }else if (direction == 2){
                                targetTop = currentTop - 95;
                                if(targetTop < -1 * (currentHeight - containerHeight) ){
                                    targetTop = -1 * (currentHeight - containerHeight);
                                }
                            }

                            if(direction != 0){
                                currentTop = targetTop;
                                accedo.console.log('currentTop: ' + currentTop);
                                termsContent.getRoot().setStyle({
                                    top: targetTop + "px"
                                });
                            }

                            scrollbar.update(-1* currentTop, containerHeight, currentHeight );
                        }

                        leftPanel.setOption('nextRight', 'agreementYes');
                        recieveMailNo.setOption('nextDown', 'continueButton');

                        leftPanel.setOption("nextUp",function(){
                            scrollText(1);
                        });

                        leftPanel.setOption("nextDown",function(){
                            scrollText(2);
                        });

                        accedo.utils.fn.defer(scrollText, 1, 0);

                        accedo.focus.manager.requestFocus(leftPanel);

                        agreementYes.addEventListener('click', function() {
                            agreementYes.getRoot().addClass('select');
                            agreementNo.getRoot().removeClass('select');
                            agree = true;
                            accedo.focus.manager.requestFocus(recieveMailYes);
                        });

                        agreementNo.addEventListener('click', function() {
                            agreementYes.getRoot().removeClass('select');
                            agreementNo.getRoot().addClass('select');
                            agree = false;
                            accedo.focus.manager.requestFocus(recieveMailYes);
                        });

                        recieveMailYes.addEventListener('click', function() {
                            recieveMailYes.getRoot().addClass('select');
                            recieveMailNo.getRoot().removeClass('select');
                            self.param.receiveNews = true;
                            accedo.focus.manager.requestFocus(self.continueButton);
                        });

                        recieveMailNo.addEventListener('click', function() {
                            recieveMailYes.getRoot().removeClass('select');
                            recieveMailNo.getRoot().addClass('select');
                            self.param.receiveNews = false;
                            accedo.focus.manager.requestFocus(self.continueButton);
                        });

                        agreementYes.onFocus = function() {
                            leftPanel.setOption('nextRight', 'agreementYes');
                        };

                        agreementNo.onFocus = function() {
                            leftPanel.setOption('nextRight', 'agreementNo');
                        };

                        recieveMailYes.onFocus = function() {
                            leftPanel.setOption('nextRight', 'recieveMailYes');
                        };

                        recieveMailNo.onFocus = function() {
                            leftPanel.setOption('nextRight', 'recieveMailNo');
                        };

                        self.continueButton.setOption('nextUp', 'recieveMailNo');

                        self.continueButton.onFocus = function() {
                            recieveMailNo.setOption('nextDown', 'continueButton');
                        };

                        self.backButton.setOption("nextUp", 'recieveMailNo');

                        self.backButton.onFocus = function() {
                            recieveMailNo.setOption('nextDown', 'backButton');
                        };
                    }, 0.3);
                },

                continueButtonAction : function() {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();

                    if (agree == null) {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : 'You must agree to the terms and conditions to join BigPond Movies.',
                            colour : 'red',
                            id : 'agreeTermsConditions'
                        });
                    }
                    else {
                        if (self.param.receiveNews == null) {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();

                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : 'You must choose whether to receive Telstra marketing communications or not',
                                colour : 'red',
                                id : 'chooseSubscriptionType'
                            });
                        }
                        else {
                            if (!agree) {
                                telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : 'You must agree to the terms and conditions to join BigPond Movies.',
                                    colour : 'red',
                                    id : 'agreeTermsConditions'
                                });
                            }
                            else {
                                telstra.api.manager.movies.accountInfo = self.param;

                                utils.updateMoviesAccount(telstra.api.manager.movies.accountInfo, {
                                    onSuccess:function() {
                                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                        utils.updateFirstTimeLogin(function() {
                                            telstra.sua.module.bpvod.session.isSignedIn = true;
                                            utils.saveAutoSignIn();
                                            
                                            if (!self.param.purchaseItem && self.param.purchaseItem == null){
                                                utils.setDispatchController(myPubObj, 'signInCongraController', self.param, true);
                                            } else {
                                                utils.purchaseAction(self.param.purchaseItem, myPubObj);
                                            }
                                        });
                                        
                                        telstra.sua.module.bpvod.session.signedInButNotFinishedProcess = false;
                                    }
                                });
                            }
                        }
                    }
                },

                backButtonAction : function() {
                    leftPanel.setOption('nextRight', 'agreementYes');
                    recieveMailNo.setOption('nextDown', 'continueButton');
                    utils.setDispatchController(myPubObj, 'signInStep7DeviceNameController', self.param, true);
                }
            });

            return myPubObj;
        }
    });