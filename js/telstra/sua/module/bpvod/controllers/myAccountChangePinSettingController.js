/**
 * @fileOverview
 * @author <a href='mailto:victor.leung@accedobroadband.com'>Victor Leung</a>
 */
accedo.define(
    'telstra.sua.module.bpvod.controllers.myAccountChangePinSettingController',
    ['accedo.utils.object',
    'accedo.ui.controller',
    'accedo.focus.manager',
    'telstra.api.manager',
    'telstra.api.usage',
    'telstra.sua.core.components.infoMessageBar',
    'telstra.sua.core.components.loadingOverlay',
    'telstra.sua.module.bpvod.views.myAccountChangePinSettingView'],
    function(){
        
        return function(opts) {

            var utils = telstra.sua.module.bpvod.utils,
            views = telstra.sua.module.bpvod.views,
            optionsList = [],
            self, option1, option2, usepin, saveChangesButton, closeButton;

            return accedo.utils.object.extend(accedo.ui.controller(opts), {

                onCreate: function() {
                    telstra.sua.core.main.mainNavBar.setMenuButton("HELP");
                    
                    self = this;
                    usepin = 1;
                    
                    //Set the view
                    self.setView(views.myAccountChangePinSettingView);
                    
                    option1 = self.get('option1');
                    option2 = self.get('option2');
                    saveChangesButton = self.get('saveChangesButton');
                    closeButton = self.get('closeButton');

                    if (telstra.api.manager.movies.accountInfo.useaccountpin)
                        option1.getRoot().addClass('select');
                    else
                        option2.getRoot().addClass('select');

                    saveChangesButton.addEventListener('click', function() {
                        self.saveAction();
                    });

                    saveChangesButton.onFocus = function() {
                        option1.setOption('nextDown', 'saveChangesButton');
                        option2.setOption('nextDown', 'saveChangesButton');
                    };

                    closeButton.addEventListener('click', function() {
                        saveChangesButton.setOption('nextUp', 'option1');
                        option1.setOption('nextDown', 'saveChangesButton');
                        option2.setOption('nextDown', 'saveChangesButton');
                        self.dispatchEvent('telstra:core:historyBack');
                    });

                    closeButton.onFocus = function() {
                        option1.setOption('nextDown', 'closeButton');
                        option2.setOption('nextDown', 'closeButton');
                    };

                    option1.addEventListener('click', function() {
                        option1.getRoot().addClass('select');
                        option2.getRoot().removeClass('select');
                        usepin = 1;
                        accedo.focus.manager.requestFocus(saveChangesButton);
                    });

                    option1.onFocus = function() {
                        saveChangesButton.setOption('nextUp', 'option1');
                        closeButton.setOption('nextUp', 'option1');
                    };

                    option2.addEventListener('click', function() {
                        option2.getRoot().addClass('select');
                        option1.getRoot().removeClass('select');
                        usepin = 0;
                        accedo.focus.manager.requestFocus(saveChangesButton);
                    });

                    option2.onFocus = function() {
                        saveChangesButton.setOption('nextUp', 'option2');
                        closeButton.setOption('nextUp', 'option2');
                    };

                    optionsList.push(
						{
							type: "red",
							title: "Help"
						}
					);
					//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);

                    accedo.focus.manager.requestFocus(saveChangesButton);
                },

                saveAction: function() {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();

                    telstra.api.manager.movies.accountInfo.useaccountpin = usepin;

                    utils.updateMoviesAccount(telstra.api.manager.movies.accountInfo, {
                        onSuccess:function() {
                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : 'Your PIN settings have been changed.',
                                colour : 'green',
                                id : 'pinChanged'
                            });

                            accedo.utils.fn.delay(function() {
                                telstra.sua.core.components.loadingOverlay.turnOffLoading();
                                self.dispatchEvent('telstra:core:historyBack');
                            }, 2);
                        }
                    });
                },

                onKeyRed : function() {
                    telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 19944);
                },

                onKeyBack: function() {
                    if (telstra.sua.core.components.loadingOverlay.isLoadingOn())
                        return;
                    
                    saveChangesButton.setOption('nextUp', 'option1');
                    option1.setOption('nextDown', 'saveChangesButton');
                    option2.setOption('nextDown', 'saveChangesButton');
                    self.dispatchEvent('telstra:core:historyBack');
                }
            });
        }
    });