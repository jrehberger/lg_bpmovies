/**
 * @fileOverview forgotPinController
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 *
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.forgotPinController", [
    // dependency
    'accedo.app',
    "accedo.utils.object", "accedo.utils.array", "accedo.utils.fn",
    "accedo.ui.controller", "accedo.ui.popupOk", 'telstra.api.usage',
    'telstra.sua.module.bpvod.views.forgotPinView',
    'telstra.sua.module.bpvod.utils'],
    function(){
        return function(opts) {

            // Shorten for namespacing
            var utils = telstra.sua.module.bpvod.utils,
            views = telstra.sua.module.bpvod.views,
            optionsList = [],
            self;

            var myPubObj = accedo.utils.object.extend(accedo.ui.controller(opts), {

                onCreate: function(context) {
                    telstra.sua.core.main.mainNavBar.setMenuButton("HELP");

                    self = this;

                    //Set the view
                    self.setView(views.forgotPinView);

                    // email address does not validate now since infoMessageBar
                    // not implemented yet (infoMessageBar should moved to core
                    // module)
                    var emailaddress = self.get('emailaddress'),
                    passwordField = self.get('password'),
                    closeButton = self.get('closeButton'),
                    saveButton = self.get('saveButton');
                    
                    closeButton.addEventListener('click',function(){
                        myPubObj.onKeyBack();
                    });

                    emailaddress.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(passwordField);
                    };
                    passwordField.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(saveButton);
                    };

                    saveButton.addEventListener('click',function(){
                        var loginName = emailaddress.getCleanedResultStr(),
                        password = passwordField.getCleanedResultStr();
                        accedo.console.log("name: " + loginName + " --- password: " + password);

                        if (typeof(loginName) == "undefined" || loginName == null || loginName == "" || typeof(password) == "undefined" || password == null || password == "") {
                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : "Please type in both your email and password and then select Continue.",
                                colour : "red",
                                id : 'emptyBothInput'
                            });
                            return;
                        }

                        if (!utils.checkEmail(loginName)) {
                            accedo.focus.manager.requestFocus(emailaddress);
                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : "Please enter an email address",
                                colour : "red",
                                id : 'emptyEmailInput'
                            });
                            return;
                        }

                        utils.validateUser(loginName, password, function(json){
                            accedo.console.log("json.accountid: " + json.accountid + " telstra.api.manager.movies.accountId: " + telstra.api.manager.movies.accountId);
                            if(json.accountid != telstra.api.manager.movies.accountId){
                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : "The email address/password entered for this account is incorrect.",
                                    colour : "red",
                                    id : 'emailPasswordIncorrect'
                                });
                            }else{
                                utils.setDispatchController(myPubObj, 'myAccountChangePinController', opts.context, true);
                            }
                            
                        }, function(json){
                            if(json  && json.errorcode){
                                if (json.errorcode == "INVALID_USER") {
                                    telstra.sua.core.components.infoMessageBar.addBar({
                                        text : "The email address entered for this account is incorrect.",
                                        colour : "red",
                                        id : 'emailIncorrect'
                                    });    
                                    return;
                                }else if (json.errorcode == "AUTH_FAIL"){
                                    telstra.sua.core.components.infoMessageBar.addBar({
                                        text : "The password entered for this account is incorrect.",
                                        colour : "red",
                                        id : 'passwordIncorrect'
                                    });    
                                    return;
                                }                                     
                            }
                            utils.errorMessageokPopup(json);
                        });
                    });
                    
                    optionsList.push(
						{
							type: "red",
							title: "Help"
						},
						{
							type: "yellow",
							title: "BigPond Plans"
						}
					);
					//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);
                    
                    accedo.utils.fn.defer(accedo.utils.fn.bind(accedo.focus.manager.requestFocus,  accedo.focus.manager), emailaddress);
                },

                onKeyRed : function() {
                    if (!telstra.sua.core.main.keyboardShown)
                        telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 19950);
                },

                onKeyYellow: function() {
                	telstra.sua.module.bpvod.controllers.popupBigPondPlanController();
                },
                
                /**
                 *  Action need to be done when back button is pressed
                 */
                onKeyBack : function(){
                    this.dispatchEvent('telstra:core:historyBack');
                }
            });

            return myPubObj;
        };
    }
    );