/**
 * @fileOverview
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 *
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.signInStep5AccountPinController", [
    // dependency
    "telstra.api.usage",
    "telstra.sua.module.bpvod.views.signInStep5AccountPinView",
    "telstra.sua.module.bpvod.controllers.signInTemplateController"],
    function(){

        return function(opts){

            // Shorten for namespacing
            var utils = telstra.sua.module.bpvod.utils,
            pinField,
            self;


            var myPubObj = accedo.utils.object.extend(telstra.sua.module.bpvod.controllers.signInTemplateController(opts), {
    
                onCreate : function(){
                    self = this;

                    this.callcode = 19927;

                    this.init(telstra.sua.module.bpvod.views.signInStep5AccountPinView);

                    pinField = this.get('pinField');
                    //test
                    //pinField.setString("1111");
                    //test
                    this.continueButton.setOption("nextUp",'pinField');
                    this.backButton.setOption("nextUp",'pinField');

                    pinField.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(self.continueButton);
                    }

                    accedo.utils.fn.delay(function(){
                        accedo.focus.manager.requestFocus(pinField);
                    },0.5);

                },

                //verify User
                continueButtonAction : function(){

                    var pinNumber = pinField.getCleanedResultStr();

                    if(pinNumber == null || accedo.utils.object.isUndefined(pinNumber) || pinNumber == "" || pinNumber.length < 4){
                        pinField.setString("");
                        accedo.focus.manager.requestFocus(pinField);

                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : "Your PIN must be 4 digit long.",
                            colour : "red",
                            id : 'enterFourDigits'
                        });
                        return;
                    }

                    if(!utils.isNumber(pinNumber)){
                        pinField.setString("");
                        accedo.focus.manager.requestFocus(pinField);

                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : "Please enter numbers only.",
                            colour : "red",
                            id : 'enterNumbersOnly'
                        });
                        return;
                    }

                    if (pinNumber == telstra.api.manager.movies.accountInfo.accountpin) {
                        //get to next step

                        self.param.pinChecked = true;
                        utils.setDispatchController(myPubObj, 'signInStep6PinSettingController', self.param, true);

                    }else{
                        pinField.setString("");
                        accedo.focus.manager.requestFocus(pinField);

                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : "The PIN you entered is incorrect. Retry, or select change PIN to create a new PIN",
                            colour : "red",
                            id : 'incorrectPIN'
                        });
                    }
                        
                },

                //change pin
                backButtonAction : function() {
                    self.param.createNewPIN = true;
                    utils.setDispatchController(myPubObj, 'myAccountChangePinController', self.param, true);
                }
            });

            return myPubObj;
        }
    });