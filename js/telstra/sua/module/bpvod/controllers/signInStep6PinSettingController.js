/**
 * @fileOverview
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 *
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.signInStep6PinSettingController", [
    // dependency
    "telstra.api.usage",
    "telstra.sua.module.bpvod.views.signInStep6PinSettingView",
    "telstra.sua.module.bpvod.controllers.signInTemplateController"],
    function(){

        return function(opts){

            // Shorten for namespacing
            var utils = telstra.sua.module.bpvod.utils,
            pinSettingYes,
            pinSettingNo,
            usepin = null,
            self;


            var myPubObj = accedo.utils.object.extend(telstra.sua.module.bpvod.controllers.signInTemplateController(opts), {

                onCreate : function(){
                    self = this;

                    this.callcode = 19928;

                    this.init(telstra.sua.module.bpvod.views.signInStep6PinSettingView);

                    pinSettingYes = this.get('pinSettingYes');
                    pinSettingNo = this.get('pinSettingNo');

                    this.continueButton.setOption("nextUp",'pinSettingNo');
                    this.backButton.setOption("nextUp",'pinSettingNo');

                    pinSettingYes.addEventListener('click', function() {
                        pinSettingYes.getRoot().addClass('select');
                        pinSettingNo.getRoot().removeClass('select');
                        usepin = 1;
                        accedo.focus.manager.requestFocus(self.continueButton);
                    });

                    pinSettingNo.addEventListener('click', function() {
                        pinSettingNo.getRoot().addClass('select');
                        pinSettingYes.getRoot().removeClass('select');
                        usepin = 0;
                        accedo.focus.manager.requestFocus(self.continueButton);
                    });

                    accedo.utils.fn.delay(function(){
                        accedo.focus.manager.requestFocus(pinSettingYes);
                    },0.5);
                },

                //verify User
                continueButtonAction : function(){
                    if(usepin != null){
                        self.param.useaccountpin = usepin;
                        utils.setDispatchController(myPubObj, 'signInStep7DeviceNameController', self.param, true);
                    }else{
                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : "You must select a PIN setting.",
                            colour : "red",
                            id : 'selectPinSetting'
                        });
                    }
                },

                //change pin
                backButtonAction : function(){
                    if (!self.param.accountpin){
                        utils.setDispatchController(myPubObj, 'signInStep5SetPinController', self.param, true);
                    }else{
                        utils.setDispatchController(myPubObj, 'signInStep5AccountPinController', self.param, true);
                    }
                }
            });

            return myPubObj;
        }
    });