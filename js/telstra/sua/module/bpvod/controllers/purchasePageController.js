/**
 * @fileOverview
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.controllers.purchasePageController",
    ["accedo.utils.object",
    "accedo.utils.fn",
    "accedo.ui.controller",
    "telstra.sua.module.bpvod.controllers.popupBigPondPlanController",
    "telstra.sua.module.bpvod.views.purchasePageView",
    "telstra.sua.core.components.loadingOverlay",
    "telstra.api.manager",
    "telstra.api.usage",
    'telstra.sua.core.controllers.popupOkInfoController'],
    function(){
        return function(opts){

            var utils = telstra.sua.module.bpvod.utils,
            isUndefined = accedo.utils.object.isUndefined,
            loading = telstra.sua.core.components.loadingOverlay,
            views = telstra.sua.module.bpvod.views,
            api = telstra.api.manager;

            var self,
            optionsList = [],
            purchaseDone = false,
            purchaseButton,
            mainVerticalPanel,
            pinPanel,
            redeemButton,
            cancelButton,
            videoThumbnail,
            videoThumbnail2,
            episodeLabel,
            episodeLabel2,
            videoItem,
            purchasePreResult,
            ageVerificationPanel,
            ageVerificationPageConfirmButton,
            ageVerificationPageCancelButton,
            ageVerificationPageForgotButton,
            pinPageConfirmButton,
            pinPageCancelButton,
            pinPageForgotButton,
            labelPurchaseSTranNo,
            purchaseSuccessPanel,
            watchNowButton,
            watchLaterButton,
            viewRelatedButton,
            middlePanel,
            pinField,
            pinField2,
            pinFilled = opts.context.pinFilled || false,

            currentPageName = null;

            var pinFailLimit = telstra.sua.module.bpvod.config.pinFailLimit;

            var myPubObj = accedo.utils.object.extend(accedo.ui.controller(opts), {

                intPinFailTime : 0,
                pinVerfied : false,
                purchaseResult : null,
                voucherStatus : 0,

                onCreate : function(){
                    if(!opts.context.videoItem){
                        accedo.console.log("error: videoItem is missing");
                    }
                    videoItem = opts.context.videoItem;
                    var self = this;

                    utils.purchaseCheck(videoItem,{
                        onSuccess : function(purchaseCheckResult){
                            opts.context.purchaseCheckResult = purchaseCheckResult;
                            self.onCreateCallback();
                        },
                        onFailure : function(json){
                            utils.errorMessageokPopup(json, function() {
                                loading.turnOnLoading();
                                self.dispatchEvent('telstra:core:historyBack');
                            });
                        },
                        onComplete : function(){
                        //loading.turnOffLoading();
                        }
                    });
                },

                onCreateCallback: function(context) {
                    //videoItem = opts.context.videoItem;
                    purchasePreResult = opts.context.purchaseCheckResult;

                    self = this;

                    //Set the view
                    self.setView(views.purchasePageView);
                    self.setVisible(false);
                    accedo.utils.fn.delay(function(){
                        self.setVisible(true);
                    },0.5);

                    purchaseButton = this.get('purchaseButton');
                    redeemButton = this.get('redeemButton');
                    cancelButton = this.get('cancelButton');
                    videoThumbnail = this.get('videoThumbnail');
                    videoThumbnail2 = this.get('videoThumbnail2');
                    episodeLabel = this.get('episodeLabel');
                    episodeLabel2 = this.get('episodeLabel2');

                    pinPageConfirmButton = this.get('okConfirmButton');
                    pinPageCancelButton = this.get('cancelPinButton');
                    pinPageForgotButton = this.get('forgottenPinButton');

                    ageVerificationPageConfirmButton = this.get('okConfirmButton2');
                    ageVerificationPageCancelButton = this.get('cancelPinButton2');
                    ageVerificationPageForgotButton = this.get('forgottenPinButton2');

                    watchNowButton = this.get('watchNowButton');
                    watchLaterButton = this.get('watchLaterButton');
                    viewRelatedButton = this.get('viewRelatedButton');
                    labelPurchaseSTranNo = this.get('transactionNo');

                    middlePanel = this.get('middlePanel');
                    pinField = this.get('pinField');
                    pinField2 = this.get('pinField2');
                    pinPanel = this.get('pinPanel');
                    
                    //test - predefined fields
                    /*pinField.setString('1111');	
                    pinField2.setString('1111');*/
                    //test
                    
                    pinField.maxLengthCallback = function() {
                        accedo.console.log("pinField.maxLengthCallback");
                        accedo.focus.manager.requestFocus(pinPageConfirmButton);
                    }

                    pinField2.maxLengthCallback = function() {
                        accedo.console.log("pinField2.maxLengthCallback");
                        accedo.focus.manager.requestFocus(ageVerificationPageConfirmButton);
                    }

                    pinPanel.hide();

                    this.get('movieTitle').setText(videoItem.title);
                    this.get('movieTitle2').setText(videoItem.title);
                    this.get('movieTitle3').setText(videoItem.title);

                    this.get('classificationImage').setSrc(telstra.api.manager.resizer(videoItem.classificationimagesmall, 208, 70, true));
                    this.get('classificationImage2').setSrc(telstra.api.manager.resizer(videoItem.classificationimagesmall, 208, 70, true));
                    if (videoItem.consumeradvice){
                    	this.get('classificationCommentLabel2').setText(videoItem.consumeradvice);
                    	this.get('classificationCommentLabel').setText(videoItem.consumeradvice);
                    }
                    this.get('expiredIn').setText("Rental expires in "+videoItem.product.viewingperiod+" hours");
                    this.get('availableFor').setText("Rental available for "+videoItem.product.deletionperiod+" days");

                    var price = videoItem.product.price;
                    this.get('priceInt').setText(price.split(".")[0] + ".");
                    this.get('priceDeci').setText(price.split(".")[1]);

                    this.get('watchNow').setText(videoItem.product.viewingperiod + "hours");
                    this.get('watchLater').setText(videoItem.product.deletionperiod + "days");

                    if(telstra.sua.module.bpvod.utils.isEpisode(videoItem)){
                        videoThumbnail.setSrc(telstra.api.manager.resizer(videoItem.coverimage, 113, 77, true));
                        videoThumbnail2.setSrc(telstra.api.manager.resizer(videoItem.coverimage, 113, 77, true));
                        videoThumbnail.getRoot().addClass("tvItem");
                        videoThumbnail2.getRoot().addClass("tvItem");
                        episodeLabel.setText("Season " + videoItem.seasonnumber + "<br/>Episode " +  videoItem.episodenumber);
                        episodeLabel2.setText("Season " + videoItem.seasonnumber + "<br/>Episode " +  videoItem.episodenumber);
                    }else{
                        videoThumbnail.setSrc(telstra.api.manager.resizer(videoItem.coverimage, 102, 144, true));
                        videoThumbnail2.setSrc(telstra.api.manager.resizer(videoItem.coverimage, 135, 193, true));
                    }

                    self.voucherStatus = purchasePreResult.voucherStatus;
                    switch (purchasePreResult.voucherStatus)
                    {
                        case 1:
                            this.get("creditCard").setText("Credit Card "+purchasePreResult.cccharge);
                            this.get("gstInfo").setText(purchasePreResult.cccharge+ " inc. GST will be deducted from your credit card.");
                            break;
                        case 2:
                            this.get("voucherCredit").setText("Voucher Credit: " + purchasePreResult.vouchercreditbeforepurchase);
                            this.get("creditdeCard").setText("Credit Card: "+purchasePreResult.cccharge);
                            this.get("gstCreditDeducted").setText(purchasePreResult.cccharge+ " inc. GST will be deducted from your credit card.");//"Your Credit Card will be charged "+this.purchasePreResult.cccharge+ " inc.GST.");
                            this.get("remainVoucher").setText("After this purchase, your remaining voucher credit will be: "+purchasePreResult.vouchercreditafterpurchase);
                            break;
                        case 3:
                            this.get("voucherCredit").setText("Voucher Credit: " + purchasePreResult.vouchercreditbeforepurchase);
                            this.get("gstInfo").setText(purchasePreResult.vouchercreditcharge+" inc. GST will be deducted from your voucher credit.");
                            this.get("remainVoucher").setText("After this purchase, your remaining voucher credit will be: "+purchasePreResult.vouchercreditafterpurchase);
                            break;
                        default:
                            accedo.console.log("Error: incorrect voucherStatus: "+purchasePreResult.voucherStatus);
                            break;
                    }

                    purchaseButton.addEventListener(
                        'click',
                        function() {
                            if (self.checkPinOption()) {
                                if(self.pinVerfied !== null && (self.pinVerfied || pinFilled)) {
                                    self.processPin(1);
                                }else {
                                    self.displayPinPage();
                                }
                            }
                            else {
                                self.processPin(0);
                            }
                        });

                    pinPageConfirmButton.addEventListener(
                        'click',
                        function() {
                            self.confirmProcessPin();
                        });

                    ageVerificationPageConfirmButton.addEventListener(
                        'click',
                        function() {
                            self.confirmPin();
                        });
                        
                    redeemButton.addEventListener(
                        'click',
                        function() {
                            self.redeemVoucher();
                        });

                    viewRelatedButton.addEventListener(
                        'click',
                        function() {
                            self.viewRelated();
                        });

                    watchNowButton.addEventListener(
                        'click',
                        function() {
                            self.watchNow();
                        });

                    watchLaterButton.addEventListener(
                        'click',
                        function() {
                            self.watchLater();
                        });

                    var cancelAction = function(){
                        self.cancelPurchase();
                    };

                    var forgotAction = function(){
                        self.forgotPin();
                    };

                    cancelButton.addEventListener('click',cancelAction);
                    pinPageCancelButton.addEventListener('click',cancelAction);
                    ageVerificationPageCancelButton.addEventListener('click',cancelAction);

                    pinPageForgotButton.addEventListener('click',forgotAction);
                    ageVerificationPageForgotButton.addEventListener('click',forgotAction);

                    mainVerticalPanel  = this.get('mainVerticalPanel');
                    ageVerificationPanel = this.get('ageVerificationPanel');
                    purchaseSuccessPanel = this.get('purchaseSuccessPanel');

                    if(this.checkAgeLimit() && !pinFilled){
                        this.displayAgeVerification();
                    }else{
                        this.displayPurchaseDetails();
                    }
                },
                
                updateMenu : function(){
                	switch(currentPageName){
                        case "pinPage":
                            optionsList.push(
								{
									type: "red",
									title: "Help"
								},
								{
									type: "green",
									title: "Forgotten PIN?"
								},
								{
									type: "yellow",
									title: "BigPond Plans"
								}
							);
                            break;
                        case "purchaseComplete":
                        	optionsList.push(
								{
									type: "red",
									title: "Help"
								},
								{
									type: "green",
									title: "My Rentals"
								},
								{
									type: "yellow",
									title: "BigPond Plans"
								}
							);
                            break;
                        case "ageVerification":
                        case "purchaseDetails":
                            optionsList.push(
								{
									type: "red",
									title: "Help"
								},
								{
									type: "yellow",
									title: "BigPond Plans"
								}
							);
                            break;
                    }
					//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);
                },

                //to make purchase
                processPin : function(mode){
                    accedo.console.log("carry out purchase!");

                    var self = this;
                    var accountid = telstra.api.manager.movies.accountId;

                    accedo.console.log("purchase - productID:  "+ videoItem.product.productid + ", account id: "+ accountid);
                    /*
                    //temp
                    var dummyData = {
                        "purchase": {
                            "purchaseid": 3347505,
                            "license": 997722387,
                            "email": "iptvdemo@bigpond.com",
                            "voucherCreditAfterPurchase": "$100.35"
                        }
                    };
                    
                    self.purchaseResult=dummyData.purchase;
                    self.purchaseSuccess(mode);
                    
                    return;
                    //temp end
                    */

                    loading.turnOnLoading();
                    telstra.api.manager.movies.purchase(videoItem.product.productid, 
                    {
                        onSuccess:function(json){
                            self.purchaseResult=json;
                            self.purchaseSuccess(mode);
                            return;
                        },
                        onFailure:function(json){
                            /*
                         *Error Codes:
                            CONN_NO_SSL
                            INVALID_DEVICE_TYPE
                            MISSING_UNIQUE_IDENTIFIER
                            INVALID_MAC_ADDR
                            OFF_COUNTRY (for new HTTP session), IPTV devices only
                            OFF_NET (NetgemSTB only)
                            INVALID_CONTENT
                            INVALID_USER
                            NO_STORED_CREDIT
                            PAY_COUNTRYERR
                            PAY_SYSERR
                            PAY_TXERR
                            THE_PLATFORM_DOWN
                            NO_LICENSE_FOUND
                            ENDUSER_NOT_FOUND_IN_THEPLATFORM
                            RELEASE_NOT_FOUND_IN_THEPLATFORM
                            SERVER_ERROR
                            CONCURRENT_PURCHASE
                            STALE_ORDER_FOUND
                         */
                            accedo.console.log("purchase failure");
                            accedo.console.log(accedo.utils.object.toJSON(json));

                            if(json && json.errorcode){
                                accedo.console.log("error here there? " + json.errorcode);
                                utils.errorMessageokPopup(json);
                            }else{
                                accedo.console.log("error here?");

                                utils.errorMessageokPopup(json);
                            }

                            return;
                        },
                        onException:function(e){
                            loading.turnOffLoading();
                            accedo.console.log("purchase exception: " + e);
                            return;
                        },
                        onComplete : function(){
                            loading.turnOffLoading();
                            return;
                        }
                    });

                    return;
                },

                purchaseSuccess : function(mode){
                    accedo.console.log("purchaseSuccess");

                    telstra.api.usage.log("bpvod", "purchase_flow_complete", {
                        logMessage: "bigpond movies:purchase:purchase complete",
                        events: "&events=purchase",
                        products: "&products=;" + videoItem.title + ";" + self.voucherStatus + ";" + videoItem.product.price,
                        paymentType: (self.voucherStatus == 1 ? "cc" : (self.voucherStatus == 2 ? "cc+voucher" : "voucher"))
                    });

                    api.time.getTimeNow(function(serverTime){
                        purchaseDone = true;

                        if (mode == null){
                            mode = 0;
                        }

                        accedo.console.log("the server time is : " + serverTime);

                        var status = {
                            "rented": true
                        },
                        currentDate = serverTime;

                    	accedo.console.log("purchaseResult: "+JSON.stringify(self.purchaseResult));

                        videoItem.purchase = self.purchaseResult;
                        videoItem.eupId = self.purchaseResult.licenseid;
                        videoItem.startDate = currentDate;
                        videoItem.expiryDate = currentDate + 86400000*videoItem.product.deletionperiod;
                        videoItem.started = false;
                        
                        var showRelatedMoviesBtn = true; 
                        var actors = (isUndefined(videoItem.actors)||isUndefined(videoItem.actors.actor)) ? [] : videoItem.actors.actor;
	            		var directors = (isUndefined(videoItem.directors)||isUndefined(videoItem.directors.director)) ? [] : videoItem.directors.director;
	            		if (!accedo.utils.object.isArray(actors)){
		                	actors = [actors];
		                }
		                if (!accedo.utils.object.isArray(directors)){
		                    directors = [directors];
		                }
		                if (actors.length == 0 && directors.length == 0){
		                	showRelatedMoviesBtn = false;
		                }

                        if (!accedo.utils.object.isUndefined(videoItem.resumeTime)) {
                            videoItem.resumeTime=null;
                        }

                        utils.updateRentedMovieStatus(videoItem,status);

                        self.hidePinPage();
                        self.displayPurchaseComplete(showRelatedMoviesBtn);
                        accedo.console.log("labelPurchaseSTranNo: "+typeof(labelPurchaseSTranNo));
                        labelPurchaseSTranNo.setText("Transaction no." + self.purchaseResult.purchaseid);
 						
                        if ( self.voucherStatus==3 ) {
                        	accedo.console.log("is 3");
                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : "Voucher credit remaining: " +self.purchaseResult.vouchercreditafterpurchase,
                                colour : "blue",
                                id : 'voucherCreditRemain'
                            });
                            accedo.focus.manager.requestFocus(watchNowButton);
                        }else{
                        	accedo.console.log("is not 3");
                            accedo.focus.manager.requestFocus(watchNowButton);
                        }
                        
                        accedo.console.log("videoItem: "+JSON.stringify(videoItem));
						/*
                        telstra.sua.core.controllers.popupHelpController({
                            headerText : "License Info",
                            innerText : videoItem.eupId,
                            innerTextCSS: "license_info",
                            callback : function(){
                                accedo.focus.manager.requestFocus(watchNowButton);
                            }
                        });
                        */
                    });
                },

                displayPinPage : function(){
                    telstra.api.usage.log("bpvod", "purchase_flow_enter_pin", {
                        logMessage: "bigpond movies:purchase:enter pin",
                        events: "&events=scAdd",
                        products: "&products=;"+ opts.context.videoItem.title
                    });

                    pinPanel.show();
                    pinPanel.root.setStyle({
                        visibility : "visible"
                    });
                    
                    middlePanel.hide();

                    mainVerticalPanel.root.setStyle({
                        left: "90px",
                        top: "120px",
                        height: "522px",
                        backgroundImage : "url(images/module/bpvod/pages/SS-MS-4_0_PurchaseOverlayConfirmRental/popup_bg01_new_111010.png)"
                    });

                    redeemButton.hide();
                    cancelButton.hide();

                    accedo.utils.fn.delay(function(){
                        accedo.focus.manager.requestFocus(pinField);
                    },0.5);

                    currentPageName = "pinPage";
                    this.updateMenu();
                },

                hidePinPage : function(){
                    mainVerticalPanel.hide();
                },

                displayPurchaseComplete : function(showRelatedMoviesBtn){
                	accedo.console.log("displayPurchaseComplete");
                	if (showRelatedMoviesBtn){
                    	viewRelatedButton.show();
                    	purchaseSuccessPanel.root.removeClass("no-related");
                    }
                    else{
                    	viewRelatedButton.hide();
                    	purchaseSuccessPanel.root.addClass("no-related");
                    }
                    purchaseSuccessPanel.root.setStyle({
                        visibility: "visible"
                    });
                    purchaseSuccessPanel.show();
                    
                    currentPageName = "purchaseComplete";
                    this.updateMenu();
                	accedo.console.log("displayPurchaseComplete");
                },

                displayAgeVerification : function(){
                    telstra.api.usage.log("bpvod", "purchase_flow_enter_pin", {
                        logMessage: "bigpond movies:purchase:enter pin",
                        events: "&events=scAdd",
                        products: "&products=;"+ opts.context.videoItem.title
                    });
                    
                    ageVerificationPanel.root.setStyle({
                        visibility: "visible"
                    });
                    ageVerificationPanel.show();
                    mainVerticalPanel.hide();
                    
                    accedo.utils.fn.delay(function(){
                        accedo.focus.manager.requestFocus(pinField2);
                    },0.5);

                    currentPageName = "ageVerification";
                    this.updateMenu();
                },

                hideAgeVerification : function(){
                    ageVerificationPanel.hide();
                },

                displayPurchaseDetails : function(){
                    telstra.api.usage.log("bpvod", "purchase_flow_confirm", {
                        logMessage: "bigpond movies:purchase:confirm",
                        events: "&events=scCheckout",
                        products: "&products=;" + videoItem.title
                    });

                    pinField2.setString("");
                    mainVerticalPanel.show();
                    this.get('movieTitle').setText(videoItem.title);
                    this.get('movieTitle3').setText(videoItem.title);
                    accedo.focus.manager.requestFocus(purchaseButton);

                    currentPageName = "purchaseDetails";
                    this.updateMenu();
                },

                cancelPurchase : function(){
                    loading.turnOnLoading();
                    accedo.console.log("cancel purchase");
                    this.dispatchEvent('telstra:core:historyBack');
                    telstra.sua.core.main.mainNavBar.showOptionBtn();
                    accedo.utils.fn.delay(function(){
                        loading.turnOffLoading();
                    },1);
                },
                
                checkAgeLimit : function(){
                    accedo.console.log("checkAgeLimit videoItem.classification: " + videoItem.classification);
                    return (videoItem.classification === "MA15+" || videoItem.classification === "R");
                },

                checkPinOption: function(){
                    if (this.checkAgeLimit()){
                        return true;
                    }
                    return (telstra.api.manager.movies.accountInfo.useaccountpin === 1 || telstra.api.manager.movies.accountInfo.useaccountpin === "1");
                },

                checkPinNumber : function(number){
                    accedo.console.log("checkPinNumber: " + number + " telstra.api.manager.movies.accountInfo.accountpin: " + telstra.api.manager.movies.accountInfo.accountpin);
                    if (telstra.api.manager.movies.accountInfo.accountpin == number){
                        return true;
                    }else{
                        this.intPinFailTime ++;
                        return false;
                    }
                },

                confirmPin : function(){
                    if (this.checkPinNumber(pinField2.getCleanedResultStr())){
                        this.pinVerfied = true;
                        this.hideAgeVerification();
                        this.displayPurchaseDetails();
                        pinFilled = true;
                    }else{
                        accedo.console.log("this.intPinFailTime: " + this.intPinFailTime + " pinFailLimit: " + pinFailLimit );
                        if (this.intPinFailTime > pinFailLimit){
                            pinField2.setString("");
                            telstra.sua.core.controllers.popupOkInfoController({
                                headerText:  'PIN incorrect',
                                innerText : 'To change the PIN associated with this account, press the Forgotten PIN button.',
                                buttonText: 'OK - Close',
                                callback : function(){
                                    accedo.focus.manager.requestFocus(pinField2);
                                }
                            });
                        }else{
                            pinField2.setString("");
                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : "The PIN you entered is incorrect. Retry, or select 'Forgotten PIN'.",
                                colour : "red",
                                id : 'pinIncorrect'
                            });
                            accedo.focus.manager.requestFocus(pinField2);
                        }
                    }
                    return;
                },

                confirmProcessPin : function(){
                    if (this.checkPinNumber(pinField.getCleanedResultStr())){
                        this.pinVerfied = true;
                        this.processPin(1);
                        pinFilled = true;
                    }else{
                        accedo.console.log("this.intPinFailTime: " + this.intPinFailTime + " pinFailLimit: " + pinFailLimit );
                        if (this.intPinFailTime > pinFailLimit){
                            pinField.setString("");
                            accedo.focus.manager.requestFocus(pinField);
                            telstra.sua.core.controllers.popupOkInfoController({
                                headerText:  'PIN incorrect',
                                innerText : 'To change the PIN associated with this account, press the Forgotten PIN button.',
                                buttonText: 'OK - Close',
                                callback : function(){
                                    accedo.focus.manager.requestFocus(pinField);
                                }
                            });
                        }else{
                            pinField.setString("");
                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : "The PIN you entered is incorrect. Retry, or select 'Forgotten PIN'.",
                                colour : "red",
                                id : 'pinIncorrect'
                            });
                            accedo.focus.manager.requestFocus(pinField);
                        }
                    }
                    return;
                },

                forgotPin: function(){
                    accedo.console.log("move to forgotPin Page");
                    utils.moveToForgotPin(self, opts.context);
                    telstra.sua.core.main.mainNavBar.showOptionBtn();
                },

                redeemVoucher : function(){
                    accedo.console.log("move to redeemVoucher Page");

                    utils.setDispatchController(self, 'myAccountRedeemVoucherController', {
                    	from: "purchasePageController"
                    }, false, {
                        videoItem : videoItem,
                        pinFilled : pinFilled,
                        purchaseCheckResult : purchasePreResult
                    });
                },

                viewRelated : function(){
                    accedo.console.log("view related movie");
                    utils.moveToRelatedMovie(videoItem, self);
                    telstra.sua.core.main.mainNavBar.showOptionBtn();

                    accedo.utils.fn.delay(function(){
                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : "Your Rental will expire in "+videoItem.product.deletionperiod+" days.",
                            colour : "blue",
                            id : 'expireRentalMessage'
                        });
                    },1);
                },

                watchNow : function(){
                    accedo.console.log("call watch now");
                    accedo.console.log(" =====================");
                    accedo.console.log(accedo.utils.object.toJSON(videoItem));
                    accedo.console.log(" =====================");

                    utils.setDispatchController(this, 'videoPageController', {
                        videoItem : videoItem
                    },true);
                    accedo.utils.fn.delay(function(){
                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : "Your Rental will expire in "+videoItem.product.viewingperiod+" hours.",
                            colour : "blue",
                            id : 'expireRentalMessage'
                        });
                    },1);
                },

                watchLater : function(){
                    this.dispatchEvent('telstra:core:historyBack');
                    telstra.sua.core.main.mainNavBar.showOptionBtn();

                    accedo.utils.fn.delay(function(){
                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : "Your Rental will expire in " + videoItem.product.deletionperiod + " days.",
                            colour : "blue",
                            id : 'expireRentalMessage'
                        });
                    },1);
                },

                onKeyRed : function(){
                    switch(currentPageName){
                        case "pinPage":
                            telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 19935);
                            break;
                        case "ageVerification":
                            telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 19935);
                            break;
                        case "purchaseComplete":
                            telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 19936);
                            break;
                        case "purchaseDetails":
                            telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 19934);
                            break;
                    }
                },

                onKeyGreen : function(){
                    switch(currentPageName){
                        case "pinPage":
                            this.forgotPin();
                            break;
                        case "ageVerification":
                            break;
                        case "purchaseComplete":
                            if(!purchaseDone){
		                        return;
		                    }
		                    
		                    utils.setDispatchController(myPubObj, 'myRentalController', {}, true);
		                    accedo.utils.fn.delay(function(){
		                        telstra.sua.core.components.infoMessageBar.addBar({
		                            text : "Your Rental will expire in "+videoItem.product.deletionperiod+" days.",
		                            colour : "blue",
		                            id : 'expireRentalMessage'
		                        });
		                    }, 1);
                            break;
                        case "purchaseDetails":
                            break;
                    }
                    
                },
                
                onKeyYellow: function() {
                	telstra.sua.module.bpvod.controllers.popupBigPondPlanController();
                },
                
                onKeyBack : function(){
                    this.cancelPurchase();
                }
            });

            return myPubObj;
        }
    });