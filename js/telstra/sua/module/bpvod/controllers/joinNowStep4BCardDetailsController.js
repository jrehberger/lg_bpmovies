/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 *
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.joinNowStep4BCardDetailsController", [
    // dependency
    "telstra.api.usage",
    "telstra.sua.module.bpvod.views.joinNowStep4BCardDetailsType1View",
    "telstra.sua.module.bpvod.views.joinNowStep4BCardDetailsType2View",
    "telstra.sua.module.bpvod.controllers.joinNowTemplateController"],
    function(){

        return function(opts){

            // Shorten for namespacing
            var utils = telstra.sua.module.bpvod.utils,
            self, ccInput1, ccInput2, ccInput3, ccInput4, expDateInput1, expDateInput2, cvcInput1;

            var myPubObj = accedo.utils.object.extend(telstra.sua.module.bpvod.controllers.joinNowTemplateController(opts), {
    
                onCreate : function(){
                    telstra.api.usage.log("bpvod", "join_now_step_4b", {
                        logMessage: "bigpond movies:join:ccstepb"
                    });

                    self = this;

                    if (telstra.sua.module.bpvod.config.creditCardInfo.cardType != "amex") {
                        this.init(telstra.sua.module.bpvod.views.joinNowStep4BCardDetailsType1View);

                        ccInput4 = self.get('ccInput4');
                    }
                    else {
                        this.init(telstra.sua.module.bpvod.views.joinNowStep4BCardDetailsType2View);
                    }

                    if (this.param.updateCC || this.param.loginCreditCardDetails) {
                        var titleRoot = this.get('StepTitle').getRoot();
                        titleRoot.removeClass('StepTitle');
                        titleRoot.addClass('signInTitle');
                    }

                    ccInput1 = self.get('ccInput1');
                    ccInput2 = self.get('ccInput2');
                    ccInput3 = self.get('ccInput3');
                    expDateInput1 = self.get('expDateInput1');
                    expDateInput2 = self.get('expDateInput2');
                    cvcInput1 = self.get('cvcInput1');
                    
                    //test - predefined input
                    /*ccInput1.setString("4564");
                    ccInput2.setString("7220");
                    ccInput3.setString("0000");
                    if (ccInput4){
                    	ccInput4.setString("2583")
                    }
                    expDateInput1.setString("08");
                    expDateInput2.setString("17");
                    cvcInput1.setString("000");*/
                    //test

                    accedo.focus.manager.requestFocus(ccInput1);

                    if (this.creditCardInfo.cardNumber.cc1 != null)
                        ccInput1.setString(this.creditCardInfo.cardNumber.cc1);
                    
                    if (this.creditCardInfo.cardNumber.cc2 != null)
                        ccInput2.setString(this.creditCardInfo.cardNumber.cc2);
                    
                    if (this.creditCardInfo.cardNumber.cc3 != null)
                        ccInput3.setString(this.creditCardInfo.cardNumber.cc3);
                    
                    if (this.creditCardInfo.cardNumber != "amex" && this.creditCardInfo.cardNumber.cc4 != null)
                        ccInput4.setString(this.creditCardInfo.cardNumber.cc4);
                    
                    if (this.creditCardInfo.expiryDate.month != null)
                        expDateInput1.setString(this.creditCardInfo.expiryDate.month);
                    
                    if (this.creditCardInfo.expiryDate.year != null)
                        expDateInput2.setString(this.creditCardInfo.expiryDate.year);

                    if (this.creditCardInfo.securityCode != null)
                        cvcInput1.setString(this.creditCardInfo.securityCode);

                    ccInput1.addEventListener('accedo:keyboard:onFocus', function() {
                        expDateInput1.setOption('nextUp', 'ccInput1');
                        expDateInput2.setOption('nextUp', 'ccInput1');
                    });

                    ccInput2.addEventListener('accedo:keyboard:onFocus', function() {
                        expDateInput1.setOption('nextUp', 'ccInput2');
                        expDateInput2.setOption('nextUp', 'ccInput2');
                    });

                    ccInput3.addEventListener('accedo:keyboard:onFocus', function() {
                        expDateInput1.setOption('nextUp', 'ccInput3');
                        expDateInput2.setOption('nextUp', 'ccInput3');
                    });

                    expDateInput1.addEventListener('accedo:keyboard:onFocus', function() {
                        ccInput1.setOption('nextDown', 'expDateInput1');
                        ccInput2.setOption('nextDown', 'expDateInput1');
                        ccInput3.setOption('nextDown', 'expDateInput1');
                        cvcInput1.setOption('nextUp', 'expDateInput1');
                    });

                    expDateInput2.addEventListener('accedo:keyboard:onFocus', function() {
                        ccInput1.setOption('nextDown', 'expDateInput2');
                        ccInput2.setOption('nextDown', 'expDateInput2');
                        ccInput3.setOption('nextDown', 'expDateInput2');
                        cvcInput1.setOption('nextUp', 'expDateInput2');
                    });

                    if (this.creditCardInfo.cardType != "amex") {
                        ccInput4.addEventListener('accedo:keyboard:onFocus', function() {
                            expDateInput1.setOption('nextUp', 'ccInput4');
                            expDateInput2.setOption('nextUp', 'ccInput4');
                        });

                        expDateInput1.addEventListener('accedo:keyboard:onFocus', function() {
                            ccInput4.setOption('nextDown', 'expDateInput1');
                        });

                        expDateInput2.addEventListener('accedo:keyboard:onFocus', function() {
                            ccInput4.setOption('nextDown', 'expDateInput2');
                        });
                    }

                    this.continueButton.setOption("nextUp", 'cvcInput1');

                    this.continueButton.onFocus = function() {
                        cvcInput1.setOption('nextDown', 'continueButton');
                    };

                    this.backButton.setOption("nextUp", 'cvcInput1');

                    this.backButton.onFocus = function() {
                        cvcInput1.setOption('nextDown', 'backButton');
                    };
                    
                    ccInput1.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(ccInput2);
                    };
                    ccInput2.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(ccInput3);
                    };

                    if (this.creditCardInfo.cardType != "amex") {
                        ccInput3.maxLengthCallback = function() {
                            accedo.focus.manager.requestFocus(ccInput4);
                        };
                        ccInput4.maxLengthCallback = function() {
                            accedo.focus.manager.requestFocus(expDateInput1);
                        };
                    }
                    else {
                        
                        ccInput3.maxLengthCallback = function() {
                            accedo.focus.manager.requestFocus(expDateInput1);
                        };
                    }

                    expDateInput1.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(expDateInput2);
                    };
                    expDateInput2.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(cvcInput1);
                    };
                    cvcInput1.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(self.continueButton);
                    };
                },

                continueButtonAction : function() {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();

                    if (self.creditCardInfo.cardType == "amex") {
                        if (ccInput1.getCleanedResultStr().length == 0 || ccInput2.getCleanedResultStr().length == 0 || ccInput3.getCleanedResultStr().length == 0 || expDateInput1.getCleanedResultStr().length == 0 || expDateInput2.getCleanedResultStr().length == 0 || cvcInput1.getCleanedResultStr().length == 0) {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();

                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : 'Please complete all of the form fields',
                                colour : "red",
                                id : 'pleaseCompleteAllFields'
                            });
                        } else if (ccInput1.getCleanedResultStr().length < 4 || ccInput2.getCleanedResultStr().length < 6 || ccInput3.getCleanedResultStr().length < 5 || expDateInput1.getCleanedResultStr().length < 2 || expDateInput2.getCleanedResultStr().length < 2 || cvcInput1.getCleanedResultStr().length < 4) {
                            if (ccInput1.getCleanedResultStr().length < 4 || ccInput2.getCleanedResultStr().length < 6 || ccInput3.getCleanedResultStr().length < 5) {
                                telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : 'The credit card information you entered is not valid. Please re-enter and try again',
                                    colour : "red",
                                    id : 'notValidCCInfo'
                                });
                            } else if (expDateInput1.getCleanedResultStr().length < 2 || expDateInput2.getCleanedResultStr().length < 2) {
                                telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : 'Invalid Year or Month',
                                    colour : "red",
                                    id : 'invalidYearMonth'
                                });
                            } else if (cvcInput1.getCleanedResultStr().length < 4) {
                                telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : 'CVC number invalid. Please re-enter your CVC number.',
                                    colour : "red",
                                    id : 'invalidCVC'
                                });
                            }
                        } else {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();

                            if (utils.checkCCExpDate(expDateInput1.getCleanedResultStr(), expDateInput2.getCleanedResultStr())) {
                                self.creditCardInfo.cardNumber.cc1 = ccInput1.getCleanedResultStr();
                                self.creditCardInfo.cardNumber.cc2 = ccInput2.getCleanedResultStr();
                                self.creditCardInfo.cardNumber.cc3 = ccInput3.getCleanedResultStr();
                                self.creditCardInfo.expiryDate.month = expDateInput1.getCleanedResultStr();
                                self.creditCardInfo.expiryDate.year = expDateInput2.getCleanedResultStr();
                                self.creditCardInfo.securityCode = cvcInput1.getCleanedResultStr();
                                self.resetNavigation();

                                if (self.param.updateCC || self.param.loginCreditCardDetails){
                                    self.updateCC();
                                }
                                else {
                                    self.resetNavigation();
                                    utils.setDispatchController(myPubObj, 'joinNowStep5PinController', self.param, true);
                                }
                            }
                        }
                    }
                    // visa or master card
                    else {
                        if (ccInput1.getCleanedResultStr().length == 0 || ccInput2.getCleanedResultStr().length == 0 || ccInput3.getCleanedResultStr().length == 0 || ccInput4.getCleanedResultStr().length == 0 || expDateInput1.getCleanedResultStr().length == 0 || expDateInput2.getCleanedResultStr().length == 0 || cvcInput1.getCleanedResultStr().length == 0) {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();

                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : 'Please complete all of the form fields',
                                colour : "red",
                                id : 'pleaseCompleteAllFields'
                            });
                        } else if (ccInput1.getCleanedResultStr().length < 4 || ccInput2.getCleanedResultStr().length < 4 || ccInput3.getCleanedResultStr().length < 4 || ccInput4.getCleanedResultStr().length < 4 || expDateInput1.getCleanedResultStr().length < 2 || expDateInput2.getCleanedResultStr().length < 2 || cvcInput1.getCleanedResultStr().length < 3) {
                            if (ccInput1.getCleanedResultStr().length < 4 || ccInput2.getCleanedResultStr().length < 4 || ccInput3.getCleanedResultStr().length < 4 || ccInput4.getCleanedResultStr().length < 4) {
                                telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : 'The credit card information you entered is not valid. Please re-enter and try again',
                                    colour : "red",
                                    id : 'notValidCCInfo'
                                });
                            } else if (expDateInput1.getCleanedResultStr().length < 2 || expDateInput2.getCleanedResultStr().length < 2) {
                                telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : 'Invalid Year or Month',
                                    colour : "red",
                                    id : 'invalidYearMonth'
                                });
                            } else if (cvcInput1.getCleanedResultStr().length < 4) {
                                telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : 'CVC number invalid. Please re-enter your CVC number.',
                                    colour : "red",
                                    id : 'invalidCVC'
                                });
                            }
                        }
                        else {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();

                            if (utils.checkCCExpDate(expDateInput1.getCleanedResultStr(), expDateInput2.getCleanedResultStr())) {
                                self.creditCardInfo.cardNumber.cc1 = ccInput1.getCleanedResultStr();
                                self.creditCardInfo.cardNumber.cc2 = ccInput2.getCleanedResultStr();
                                self.creditCardInfo.cardNumber.cc3 = ccInput3.getCleanedResultStr();
                                self.creditCardInfo.cardNumber.cc4 = ccInput4.getCleanedResultStr();
                                self.creditCardInfo.expiryDate.month = expDateInput1.getCleanedResultStr();
                                self.creditCardInfo.expiryDate.year = expDateInput2.getCleanedResultStr();
                                self.creditCardInfo.securityCode = cvcInput1.getCleanedResultStr();
                                self.resetNavigation();

                                if (self.param.updateCC || self.param.loginCreditCardDetails){
                                    self.updateCC();
                                }
                                else {
                                    self.resetNavigation();
                                    utils.setDispatchController(myPubObj, 'joinNowStep5PinController', self.param, true);
                                }
                            }
                        }
                    }
                },

                backButtonAction : function() {
                    if (telstra.sua.core.components.loadingOverlay.isLoadingOn())
                        return;

                    self.resetNavigation();
                    cvcInput1.setOption('nextDown', 'continueButton');
                    utils.setDispatchController(myPubObj, 'joinNowStep4ACardTypeController', self.param, true);
                },

                updateCC : function () {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();
                    var userCardNumber = self.creditCardInfo.cardNumber.cc1 + self.creditCardInfo.cardNumber.cc2 + self.creditCardInfo.cardNumber.cc3;
                    if (self.creditCardInfo.cardType != "amex"){
                    	userCardNumber += self.creditCardInfo.cardNumber.cc4;
                    }

                    var creditCardInfo = {
                        cardType: self.creditCardInfo.cardType,
                        cardNumber: userCardNumber,
                        expiryDate: "20" + self.creditCardInfo.expiryDate.year + "-" + self.creditCardInfo.expiryDate.month,
                        securityCode: self.creditCardInfo.securityCode
                    }

                    telstra.api.manager.movies.registerCreditCard(creditCardInfo, {
                        onSuccess: function () {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();
                            
                            if (!telstra.api.manager.movies.accountInfo.creditcard){
                            	telstra.api.manager.movies.accountInfo.creditcard = {};
                            }

                            telstra.api.manager.movies.accountInfo.creditcard.cardtype = creditCardInfo.cardType;
                            telstra.api.manager.movies.accountInfo.creditcard.maskedcardnumber = creditCardInfo.cardNumber.substr(0,6) + (creditCardInfo.cardType != "amex" ? "*******" + creditCardInfo.cardNumber.substr(13,3) : "******" + creditCardInfo.cardNumber.substr(12,3));
                            telstra.api.manager.movies.accountInfo.creditcard.expirydate = creditCardInfo.expiryDate;
                            
                            //save credit card in self.param
                            self.param.creditcard = telstra.api.manager.movies.accountInfo.creditcard;

                            self.clearData();

                            if(!self.param.accountpin){
                                utils.setDispatchController(myPubObj, 'signInStep5SetPinController', self.param, true);
                            }else{
                                utils.setDispatchController(myPubObj, 'signInStep5AccountPinController', self.param, true);
                            }
                        },
                        onFailure: function(json) {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();

                            utils.errorMessageokPopup(json);
                        },
                        onException: function () {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();
                        }
                    });
                },

                clearData : function() {
                    self.creditCardInfo.cardNumber.cc1 = null;
                    self.creditCardInfo.cardNumber.cc2 = null;
                    self.creditCardInfo.cardNumber.cc3 = null;
                    self.creditCardInfo.cardNumber.cc4 = null;
                    self.creditCardInfo.expiryDate.month = null;
                    self.creditCardInfo.expiryDate.year = null;
                    self.creditCardInfo.securityCode = null;
                },

                resetNavigation : function() {
                    ccInput1.setOption('nextDown', 'expDateInput1');
                    expDateInput1.setOption('nextUp', 'ccInput1');
                    cvcInput1.setOption('nextUp', 'expDateInput1');
                }
            });

            return myPubObj;
        }
    });