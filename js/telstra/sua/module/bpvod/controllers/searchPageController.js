/**
 * @fileOverview searchPageController
 * @author <a href="mailto:roy.chan@accedobroadband.com">Roy Chan</a>
 *
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.searchPageController", [
    // dependency
    'accedo.app',
    "accedo.utils.object", "accedo.utils.array", "accedo.utils.fn",
    "accedo.ui.controller", "accedo.ui.popupOk",
    'accedo.focus.manager',
    'accedo.device.manager',
    'telstra.api.usage',
    "telstra.sua.module.bpvod.config",
    'telstra.sua.module.bpvod.views.searchPageView',
    'telstra.sua.module.bpvod.utils',
    "telstra.sua.core.components.infoMessageBar"],
    function(){
        return function(opts) {
            accedo.console.log("Create bpvod.searchPageController instance");
            
            var searchInput,
            searchType,
            searchBy,
            searchTypeField, searchByField, searchInputField, searchButton,
            self,  setSearchTypeOption, getSearchTypeOption;
            
            getSearchTypeOption = function(){
                var label = searchTypeField.returnCurrentOption();
                if (label == 'TV shows'){
                    return 'Television';
                }else{
                    return 'Movies';
                }
            };
            
            setSearchTypeOption = function(type){
                if (type == 'Television'){
                    type = 'TV shows';
                }
                searchTypeField.setOptionByLabel(type);
            };
            
            
            var myPubObj = accedo.utils.object.extend(accedo.ui.controller(opts), {

                onCreate: function(context) {
                    telstra.api.usage.log("bpvod", "search_catalogue", {
                        logMessage: "bigpond movies:search:search box"
                    });

                    self = this;                    

                    //Set the view
                    self.setView(telstra.sua.module.bpvod.views.searchPageView);
                    self.setVisible(false);
                    accedo.utils.fn.delay(function(){
                        self.setVisible(true);
                    },0.5);

                    searchInputField = this.get('searchInputField');
                    searchTypeField = this.get('searchTypeField');
                    searchByField = this.get('searchByField');
                    searchButton = this.get('searchButton');
                    
                    if ('context' in opts && 'searchInput' in opts.context){
                        searchInputField.setString(opts.context.searchInput);
                        searchByField.setOptionByLabel(opts.context.searchBy);
                        setSearchTypeOption(opts.context.searchType);
                    }
                    
                    // Given the main focus to searchInputField
                    accedo.utils.fn.defer(accedo.utils.fn.bind(accedo.focus.manager.requestFocus, accedo.focus.manager), searchInputField);

                    searchInputField.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(searchButton);
                    }

                    this.get('newSearchButton').addEventListener('click', function(){
                        //Reset the search
                        searchInputField.setString('');
                        searchByField.setOption(0);
                        searchTypeField.setOption(0);
                    });
                    
                    
                    searchButton.addEventListener('click', function(){
                        var search_result_ds, search_callback;
                        
                        searchInput = searchInputField.getCleanedResultStr();
                        searchType  = getSearchTypeOption();
                        searchBy    = searchByField.returnCurrentOption();
                        
                        if (!searchInput){
                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : 'Please enter a word to search',
                                colour : 'red',
                                id : 'emptySearchInput'
                            });
                            return;
                        }
                        
                        search_callback = function(data){
                            // as the datasource is passed to the next controllers, it'll continue to load more data and run this sucessfull callback
                            // remove listener so that it won't again
                            search_result_ds.removeEventListener('accedo:datasource:append', search_callback);
                            var result_str, dataLen = search_result_ds.getTotalItems();
                            if (dataLen <= 0){
                                result_str = 'No result found.';
                                searchInputField.setString('');
                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : 'No results',
                                    colour : 'red',
                                	id : 'noSearchResults'
                                });
                                telstra.api.usage.log("bpvod", "search_catalogue_success_no_result", {
                                    logMessage: "bigpond movies:search:search results",
                                    events: "&events=event6"
                                });
                            }else{
                                if (dataLen == 1){
                                    result_str = dataLen + ' Result found';
                                }else{ //dataLen > 1
                                    result_str = dataLen + ' Results found';
                                }
                                
                                // dispatched = true;
                                accedo.console.log('DISPTACHING Search Results page');
                                telstra.api.usage.log("bpvod", "search_catalogue_success", {
                                    logMessage: "bigpond movies:search:search results",
                                    events: "&events=event6"
                                });
                                telstra.sua.module.bpvod.utils.setDispatchController(myPubObj, 'searchResultPageController', {
                                    //pass these context variables to the result page so that it will display them
                                    search_result_ds: search_result_ds,
                                    searchInput:      searchInput,
                                    searchType:       searchType,
                                    searchBy:         searchBy,
                                    searchResultStr:  result_str
                                }, false,{
                                    //context for this controller, this will be put in history and reused in onCreate when historyback
                                    searchInput:      searchInput,
                                    searchType:       searchType,
                                    searchBy:         searchBy
                                });
                            }
                        };
                        
                        switch(searchBy) {
                            case 'Title':
                                search_result_ds = telstra.api.manager.movies.searchTitle(searchInput, searchType, {
                                    pageSize: 16
                                });
                                break;
                            case 'Director/Actor':
                                search_result_ds = telstra.api.manager.movies.searchPeople(searchInput, searchType, {
                                    pageSize: 16
                                });
                                break;
                        }
                        search_result_ds.addEventListener('accedo:datasource:append', search_callback);
                        search_result_ds.load();
                    });
                },

                /**
                 *  Action need to be done when back button is pressed
                 */
                onKeyBack : function(){
                    this.dispatchEvent('telstra:core:historyBack');
                }
            });

            return myPubObj;
        };
    });