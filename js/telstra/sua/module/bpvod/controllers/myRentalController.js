/**
 * @fileOverview
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.controllers.myRentalController",
    ["accedo.utils.object",
    "telstra.sua.module.bpvod.controllers.popupClassificationController",
    "telstra.sua.module.bpvod.session",
    "telstra.api.usage",
    "accedo.ui.controller",
    "accedo.data.ds",
    "accedo.focus.manager",
    "accedo.focus.managerMouse",
    "telstra.sua.module.bpvod.views.myRentalView",
    "telstra.sua.core.components.loadingOverlay",
    "telstra.api.manager"],
    function(){
        return function(opts){
            
            var self,
            utils = telstra.sua.module.bpvod.utils,
            loading = telstra.sua.core.components.loadingOverlay,
            session = telstra.sua.module.bpvod.session,
            api = telstra.api.manager,
            optionsList = [],

            //ui componenet
            pageMyRental,
            mainPanel,
            mainVerticalPanel,
            noRentalLabel,
            visitLabel,
            noRentalImage,
            haveRentalPanel,
            imgPreview,
            movieTitle,
            movieInfo,
            imgClock,
            readyImage,
            watchNowLabel,
            watchViewLabel,
            watchLaterLabel,
            readyLabel,
            expiredImage,
            expireViewLabel,
            expiredLabel,
            resultPanel,
            resultPanelList,
            scrollerKeyPanel,
            scrollerKeyLeft,
            scrollerKeyRight,
            categoryPanel,
            categoryList,
            upDownKeyPanel,
            upScrollerBtn, downScrollerBtn,
            watchNowButton,
            bigPondMoviesButton,
            scrollbar,

            aboutExpireList,
            readyMovieList,
            recentlyExpiredList,

            category_ds,

            currentSelectedItem = null,

            updateContentPanel = function(data){
                api.time.getTimeNow(function(serverTime){
                    var durationHour = Math.floor(data.duration / 60);
                    var durationMinutes = data.duration % 60;
                    imgPreview.setSrc("images/module/bpvod/blank.png");
                    accedo.utils.fn.defer(function(){
                        imgPreview.setSrc(telstra.api.manager.resizer(data.trailers.trailer.trailerimage,358,202,true));
                    })
                    
                    //imgPreview.setSrc(data.trailers.trailer.trailerimage);
                    movieTitle.setText(data.title);
                    var movieInfoStr = "";
                    if (data.country){
                    	movieInfoStr += data.country + " ";
                    }
                    if (data.yearprod){
                    	movieInfoStr += "["+data.yearprod+"] ";
                    }
                    if (movieInfoStr && (durationHour > 0 || durationMinutes > 0)){
                    	movieInfoStr += " -  ";
                    }
                    if (durationHour > 0){
                		movieInfoStr += durationHour + "hr";
                	}
                	if (durationMinutes > 0){
                		movieInfoStr += durationMinutes + "min";
                	}
                    movieInfo.setText(movieInfoStr);

                    if(utils.isAboutToExpired(data) && !utils.isExpired(data)){
                        imgClock.setSrc("images/module/bpvod/pages/SS-REN-1_0_My_Rentals/icon4.png");
                    }else{
                        imgClock.setSrc("images/module/bpvod/pages/SS-REN-1_0_My_Rentals/icon3.png");
                    }

                    watchViewLabel.getRoot().setStyle({
                        top : "455px"
                    });
                    watchNowLabel.getRoot().setStyle({
                        top : "479px"
                    });

                    if(utils.isFree(data)){
                        accedo.console.log("Case 1: Free Content");
                        watchViewLabel.setText("This movie is available to watch for free.");
                        expireViewLabel.setText("");
                        watchLaterLabel.setText("");
                        watchNowLabel.setText("");
                        readyImage.hide();
                        readyLabel.hide();

                        bigPondMoviesButton.hide();
                        watchNowButton.show();

                        watchNowButton.setText("OK - Watch Now");
                    }else if(utils.isExpired(data)){
                        accedo.console.log("Case 2: Expired Content");
                        var time = data.product.viewingperiod;
                        if (time > 1){
                            time += " hours";
                        }else{
                            time += " hour";
                        }
                        watchViewLabel.setText("Watch Now:");
                        expireViewLabel.setText("");
                        watchLaterLabel.setText("");
                        watchNowLabel.setText(time);

                        expiredLabel.show();
                        expiredImage.show();
                        readyImage.hide();
                        readyLabel.hide();
                        if (data.type == "movie"){
                            watchNowButton.setText("OK - Rent Movie");
                        }else if (data.type == "episode"){
                            watchNowButton.setText("OK - Rent Episode");
                        }

                        watchViewLabel.getRoot().setStyle({
                            top : "472px"
                        });
                        watchNowLabel.getRoot().setStyle({
                            top : "496px"
                        });

                        bigPondMoviesButton.hide();
                        watchNowButton.show();

                    }else if(!accedo.utils.object.isUndefined(data.resumeTime) && data.resumeTime!=null){
                        accedo.console.log("Case 3: Video Started");
                        watchViewLabel.setText("Rental Remaining:");
                        expireViewLabel.setText("Rental Started:");

                        var now = serverTime;
                        var remainTime = data.expiryDate-now;

                        var dateDisplay = utils.roundTime(remainTime);         
                        watchNowLabel.setText(dateDisplay);

                        var dayStarted = now - data.startDate;

                        dateDisplay = utils.roundTime(dayStarted);

                        readyImage.show();
                        watchNowLabel.show();

                        watchLaterLabel.setText(dateDisplay+" ago");

                        readyImage.show();
                        readyLabel.show();
                        expiredLabel.hide();
                        expiredImage.hide();

                        bigPondMoviesButton.hide();
                        watchNowButton.show();

                        watchNowButton.setText("OK - Watch Now");
                    }else{
                        accedo.console.log("Case 4: Video haven't started");
                        watchViewLabel.setText("Watch Now:");
                        expireViewLabel.setText("Watch Later:");

                        var now = serverTime;
                        var remainTime = data.expiryDate - now;

                        dateDisplay = utils.roundTime(remainTime);

                        watchLaterLabel.setText(dateDisplay);

                        if (remainTime > data.product.viewingperiod * 3600000){
                            watchNowLabel.setText(data.product.viewingperiod + " hours");
                        }else{
                            watchNowLabel.setText(dateDisplay);
                        }

                        readyImage.show();
                        readyLabel.show();
                        expiredLabel.hide();
                        expiredImage.hide();

                        bigPondMoviesButton.hide();
                        watchNowButton.show();

                        watchNowButton.setText("OK - Watch Now");
                    }
                });
            },

            emptyPanelHandle = function(isEmpty){
              
                if(isEmpty){
                    currentSelectedItem = null;

                    noRentalLabel.show();
                    noRentalImage.show();
                    visitLabel.show();
                    bigPondMoviesButton.show();
                    noRentalLabel.show();
                    watchNowButton.hide();              
                    haveRentalPanel.hide();
                }else{
                    noRentalLabel.hide();
                    noRentalImage.hide();
                    visitLabel.hide();
                    bigPondMoviesButton.hide();
                    noRentalLabel.hide();
                    watchNowButton.show();
                    haveRentalPanel.show();
                }
            },

            resultPanelListDisplayHelper = function(component, data){
                accedo.console.log("displayHelper: " + data.title);
                var videoItemImage = component.getById('bpvod_videoItemImage');
                videoItemImage.setSrc(telstra.api.manager.resizer(data.coverimagemedium,136,192,true));
                //videoItemImage.setSrc(data.coverimagemedium,113,161,true);
                component.onSelect = function() {
                    currentSelectedItem = data;
                    accedo.console.log("resultPanelListDisplayHelper onSelect");
                    try{
                        updateContentPanel(data);
	                    refreshNavBar();
                    }catch(e){
                        accedo.console.log(e);
                    }
                }
                
                component.parent.addEventListener('mouseover', function() {
                    accedo.focus.managerMouse.requestFocus(component.root);
                });
                component.parent.addEventListener('mouseout', function() {
                    accedo.focus.managerMouse.releaseFocus();
                });
                
                component.parent.addEventListener('click', function(){
            		//check if the selected item has been clicked
	            	var visibleItems = resultPanelList.getChildren();
	            	if (visibleItems[resultPanelList.getSelectedIndex()] == component){
	            		//click through
	            		utils.purchaseAction(data, myPubObj, getHistoryItem(), "myRentalController");
	            	}
	            	else{
	            		//change selection (first - define the index relatively to the visible items)
	            		for(var i = 0; i < visibleItems.length; ++i){
	            			if (visibleItems[i] == component){
	            				resultPanelList.setSelectedIndex(i);
	            				break;		
	            			}
	            		}
	            	}	
	           });
            },
            
            refreshNavBar = function(){
                telstra.sua.core.main.mainNavBar.resetMenuButton();
                
                //grab the default button list for this screen
                var arr = accedo.utils.array.clone(optionsList);
                
                if (currentSelectedItem){
                	arr.push({
						type: "red",
							title: "View Synopsis"
					});
					//If the movie is rented, the "rent movie" green button is not needed
	                if (currentSelectedItem.rented || currentSelectedItem.product.price == '$0.00'){}
	                else {
	                	arr.push({
							type: "green",
							title: "Rent Title"
						});
	                }
                }
                telstra.sua.core.main.mainNavBar.setMenuButtons(arr);
            },

            categoryListDisplayHelper = function(component, data){
                component.setText(data.title);

                component.onSelect = function() {
                    self.checkFilterList();
                    accedo.console.log("component.onSelect: " + data.title + " number of content: " + data.ds.getCurrentSize());
                    //resultPanelList.setDisplayHelper(resultPanelListDisplayHelper);
                    resultPanelList.setDatasource(data.ds);
                    var size = data.ds.getCurrentSize();
                    emptyPanelHandle(size <= 0);
                    if(size <= 1){
                        scrollbar.hide();
                    }else{
                        scrollbar.show();
                    }
                    if (size <= resultPanelList.getVisibleSize()){
                    	scrollerKeyRight.root.addClass('mouse-block');
                    	scrollerKeyLeft.root.addClass('mouse-block');
                    }
                    else{
                    	scrollerKeyRight.root.removeClass('mouse-block');
                    	scrollerKeyLeft.root.removeClass('mouse-block');
                    }
                    noRentalLabel.setText(data.noRentalText);
                    refreshNavBar();
                }
                
                component.parent.addEventListener('click', function(){
	            	var moveStep = -1, moveDirection = true, 
                    currentIndex = component.dsIndex,
                    visibleCategories = categoryList.getChildren();
                    for(var i = 0; i < visibleCategories.length; ++i){
                    	if (visibleCategories[i].dsIndex == currentIndex){
                    		moveStep += i;
                    		if (moveStep > 0){
                    			moveDirection = false;
                    		}
                    		break;
                    	}
                    }
                    if (moveStep != 0){
                    	categoryList.moveSelection(moveDirection);
                    }
	           });
            },

            getHistoryItem = function(){
                return {
                    history : {
                        aboutExpireList : aboutExpireList,
                        readyMovieList : readyMovieList,
                        recentlyExpiredList :recentlyExpiredList,
                        resultListIndex: resultPanelList.getSelection() ? resultPanelList.getSelection().dsIndex : 0,
                        categoryListIndex: categoryList.getSelection()? categoryList.getSelection().dsIndex : 0
                    }
                }
            };

            var myPubObj = accedo.utils.object.extend(accedo.ui.controller(opts), {
                
                onCreate: function(context) {
                    telstra.api.usage.log("bpvod", "my_account_my_rentals", {
                        logMessage: "bigpond movies:my account:my rentals"
                    });

                    self = this;

                    //Set the view
                    self.setView(telstra.sua.module.bpvod.views.myRentalView);

                    loading.turnOffLoading();

                    //get Reference to UI componenet
                    pageMyRental = this.get('bpvod_canvas').getById('pageStore').getById('pageMyRental').getById('pageMyRental');
                    mainPanel = pageMyRental.getById('mainPanel');
                    mainVerticalPanel = mainPanel.getById('mainVerticalPanel');
                    noRentalLabel = mainVerticalPanel.getById('noRentalLabel');
                    visitLabel = mainVerticalPanel.getById('visitLabel');
                    noRentalImage = mainVerticalPanel.getById('noRentalImage');

                    haveRentalPanel = mainVerticalPanel.getById('haveRentalPanel');
                    imgPreview = haveRentalPanel.getById('imgPreview');
                    movieTitle = haveRentalPanel.getById('movieTitle');
                    movieInfo = haveRentalPanel.getById('movieInfo');
                    imgClock = haveRentalPanel.getById('imgClock');
                    readyImage = haveRentalPanel.getById('readyImage');
                    watchNowLabel = haveRentalPanel.getById('watchNowLabel');
                    watchLaterLabel = haveRentalPanel.getById('watchLaterLabel');
                    watchViewLabel = haveRentalPanel.getById('watchViewLabel');
                    readyLabel = haveRentalPanel.getById('readyLabel');
                    expiredImage = haveRentalPanel.getById('expiredImage');
                    expiredLabel = haveRentalPanel.getById('expiredLabel');
                    expireViewLabel = haveRentalPanel.getById('expireViewLabel');
                    resultPanel = haveRentalPanel.getById('resultPanel');
                    resultPanelList = resultPanel.getById('movie_horizontal_listing'); //list
                    scrollerKeyPanel = resultPanel.getById('scrollerKeyPanel').getById('scrollerKeyPanelLeftRight');
                    scrollerKeyLeft = scrollerKeyPanel.getById('scrollerKeyLeft');
                    scrollerKeyRight = scrollerKeyPanel.getById('scrollerKeyRight');

                    categoryPanel = mainPanel.getById('categoryPanel');
                    categoryList = categoryPanel.getById('movie_category_listing'); //list

                    upDownKeyPanel = mainPanel.getById('upDownKeyPanel');
                    upScrollerBtn = this.get('upScroller');
                    downScrollerBtn = this.get('downScroller');
                    watchNowButton = pageMyRental.getById('watchNowButton');
                    bigPondMoviesButton = pageMyRental.getById('bigPondMoviesButton');
                    
                    watchNowButton.addEventListener('click',function(evt){
                    	if(currentSelectedItem){
	                        //go to purchase when video is not purchase or expired
	                        utils.purchaseAction(currentSelectedItem, myPubObj, getHistoryItem(), "myRentalController");
	                    }
                	});
                	bigPondMoviesButton.addEventListener('click',function(evt){
                    	utils.moveToBigPondMovie(myPubObj, getHistoryItem());
                	});

                    scrollbar =  pageMyRental.getById("scrollbar");

                    resultPanelList.setScrollbar(scrollbar);

                    resultPanelList.setOption('nextUp', function(){ 
                        categoryList.moveSelection(true);
                    });
                    resultPanelList.setOption('nextDown',function(){ 
                        categoryList.moveSelection();
                    });

                    if(opts.context.history){
                        aboutExpireList = opts.context.history.aboutExpireList;
                        readyMovieList = opts.context.history.readyMovieList;
                        recentlyExpiredList = opts.context.history.recentlyExpiredList;           
                    }else{
                        aboutExpireList = accedo.data.ds();
                        readyMovieList = accedo.data.ds();
                        recentlyExpiredList = accedo.data.ds();           
                    }
                    category_ds = accedo.data.ds();
                    this.checkFilterList();

                    category_ds.appendData([
                    {
                        title : 'About to expire',
                        ds : aboutExpireList,
                        noRentalText : "No rentals about to expire"
                    },
                    {
                        title : 'Ready to watch',
                        ds : readyMovieList,
                        noRentalText : "No rentals ready to watch"
                    },
                    {
                        title : '5 most recently expired',
                        ds : recentlyExpiredList,
                        noRentalText : "No rentals expired"
                    }
                    ]);

                    resultPanelList.setDisplayHelper(resultPanelListDisplayHelper);
                    resultPanelList.addEventListener('accedo:list:moved',function(evt){
                        if (!evt.reverse){
                            scrollerKeyRight.focus();
                            setTimeout(function(){
                                scrollerKeyRight.blur();
                            }, 200);
                        } else {
                            scrollerKeyLeft.focus();
                            setTimeout(function(){
                                scrollerKeyLeft.blur();
                            }, 200);
                        }
                    });

                    categoryList.setDisplayHelper(categoryListDisplayHelper);
                    categoryList.addEventListener('accedo:list:moved', function(evt) {
                        if (evt.reverse) {
                            upScrollerBtn.getRoot().addClass('focused');
                            setTimeout(function() {
                                upScrollerBtn.getRoot().removeClass('focused');
                            }, 200);
                        } else {
                            downScrollerBtn.getRoot().addClass('focused');
                            setTimeout(function() {
                                downScrollerBtn.getRoot().removeClass('focused');
                            }, 200);
                        }
                    });
                    upScrollerBtn.addEventListener('click',function(evt){
                        categoryList.moveSelection(true);
                    });
                    downScrollerBtn.addEventListener('click',function(evt){
                        categoryList.moveSelection();
                    });
                    categoryList.setDatasource(category_ds);
                    
                    resultPanelList.addEventListener('click', function() {
                        if(currentSelectedItem != null){
                            //go to purchase when video is not purchase or expired
                            utils.purchaseAction(currentSelectedItem, myPubObj, getHistoryItem(), "myRentalController");
                        }else{
                            utils.moveToBigPondMovie(myPubObj, getHistoryItem());
                        }
                    });

                    if(opts.context.history){
                        categoryList.select(opts.context.history.categoryListIndex);
                        if(resultPanelList.getDatasource().getCurrentSize() > 0){
                            resultPanelList.select(Math.min(opts.context.history.resultListIndex,resultPanelList.getDatasource().getCurrentSize()) );
                        }
                    }
                    
                    scrollerKeyRight.addEventListener('click',function(evt){
                        if (resultPanelList.getSize() > resultPanelList.getVisibleSize()){
                        	resultPanelList.moveSelectionByPage();
                        }
                    });
                    
                    scrollerKeyLeft.addEventListener('click',function(evt){
                        if (resultPanelList.getSize() > resultPanelList.getVisibleSize()){
                        	resultPanelList.moveSelectionByPage(true);
                        }
                    });
                    
                    optionsList.push(
						{
							type: "yellow",
							title: "Classifications"
						}
					);
					//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);

                    // Given the main focus to hlist
                    accedo.utils.fn.defer(accedo.utils.fn.bind(accedo.focus.manager.requestFocus, accedo.focus.manager), resultPanelList);
                },

                checkFilterList : function(){
                    utils.screenExpired();
                    var movieList = session.rentedMovieList,
                    aboutExpireArray = [],
                    readyMovieArray = [],
                    recentlyExpiredArray = [];

                    accedo.console.log("filter the movie list movieList.length: " + movieList.length);

                    for(var i = 0; i < movieList.length; i++){
                        if(!utils.isFree(movieList[i])){
                            if(utils.isRented(movieList[i]) && !utils.isExpired(movieList[i])){
                                if(utils.isAboutToExpired(movieList[i])){
                                    aboutExpireArray.push(movieList[i]);
                                }
                                readyMovieArray.push(movieList[i]);
                            }else{
                                recentlyExpiredArray.push(movieList[i]);
                            }
                        }
                    }

                    aboutExpireList.purgeData();
                    readyMovieList.purgeData();
                    recentlyExpiredList.purgeData();

                    accedo.console.log("no. of title: aboutExpireArray: " + aboutExpireArray.length);
                    accedo.console.log("no. of title: readyMovieArray: " + readyMovieArray.length);
                    accedo.console.log("no. of title: recentlyExpiredArray: " + recentlyExpiredArray.length);

                    aboutExpireList.appendData(aboutExpireArray);
                    readyMovieList.appendData(readyMovieArray);
                    recentlyExpiredList.appendData(recentlyExpiredArray);
                },
                
                onKeyRed: function() {
                    if(currentSelectedItem != null){
                        var data = currentSelectedItem;
                        var ds = accedo.data.ds();
                        ds.appendData([data]);
                        utils.setDispatchController(myPubObj, 'movieDetailsController',{
                            category_ds: ds,
                            shift: 0,
                            selected_ch_id: data.mediaitemid,
                            currentMediaType: utils.isEpisode(currentSelectedItem) ? 'Television' : 'Movies',
                            currentCategoryName: ""
                        }, false, getHistoryItem());
                    }
                },
                
                onKeyGreen: function() {
                	//rent movie
                	if(currentSelectedItem != null){
                        //go to purchase when video is not purchase or expired
                        accedo.console.log(currentSelectedItem);
                        utils.purchaseAction(currentSelectedItem, myPubObj, getHistoryItem(), "myRentalController");
                    }
                },
                
                onKeyYellow: function() {
                	//classifications
                	telstra.api.usage.log("overlay", "", {
                        logMessage: "bigpond movies: options overlay logged " + (telstra.sua.module.bpvod.session.isSignedIn ? "in" : "out") + ": classifications"
                    });

                    telstra.sua.module.bpvod.controllers.popupClassificationController();
                },

                onKeyBack : function(){
                    this.dispatchEvent('telstra:core:historyBack');
                }
            });
            
            return myPubObj;
        }
    });