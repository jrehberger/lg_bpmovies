/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    'telstra.sua.module.bpvod.controllers.myAccountChangePasswordController',
    ['accedo.utils.object',
    'accedo.ui.controller',
    'accedo.focus.manager',
    'telstra.api.manager',
    'telstra.api.usage',
    'telstra.sua.module.bpvod.utils',
    'telstra.sua.core.components.infoMessageBar',
    'telstra.sua.core.components.loadingOverlay',
    'telstra.sua.module.bpvod.views.myAccountChangePasswordView'],
    function(){
        return function(opts) {

            var utils = telstra.sua.module.bpvod.utils,
            views = telstra.sua.module.bpvod.views,
            optionsList = [],
            self, inputField1, inputField2, saveChangesButton, closeButton;

            return accedo.utils.object.extend(accedo.ui.controller(opts), {

                onCreate: function() {
                    telstra.sua.core.main.mainNavBar.setMenuButton("HELP");

                    self = this;

                    //Set the view
                    self.setView(views.myAccountChangePasswordView);

                    inputField1 = self.get('inputField1');
                    inputField2 = self.get('inputField2');
                    saveChangesButton = self.get('saveChangesButton');
                    closeButton = self.get('closeButton');

                    inputField1.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(inputField2);
                    };
                    inputField2.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(saveChangesButton);
                    };
                    inputField2.zeroLengthCallback = function() {
                        accedo.focus.manager.requestFocus(inputField2);
                    };

                    saveChangesButton.addEventListener('click',function(){
                        self.saveAction();
                    });

                    saveChangesButton.onFocus = function() {
                        inputField2.setOption('nextDown', 'saveChangesButton');
                    };

                    closeButton.addEventListener('click',function() {
                        inputField2.setOption('nextDown', 'saveChangesButton');
                        self.dispatchEvent('telstra:core:historyBack');
                    });

                    closeButton.onFocus = function() {
                        inputField2.setOption('nextDown', 'closeButton');
                    };
                    
                    optionsList.push(
						{
							type: "red",
							title: "Help"
						}
					);
					//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);

                    accedo.focus.manager.requestFocus(inputField1);
                },

                saveAction: function() {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();

                    var input1 = inputField1.getCleanedResultStr();
                    var input2 = inputField2.getCleanedResultStr();

                    if (utils.checkAccountPassword(input1,input2)) {
                        telstra.api.manager.movies.accountInfo.pwd= input1;
                        utils.updateMoviesAccount(telstra.api.manager.movies.accountInfo, {
                            onSuccess:function() {
                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : 'Your password has been changed.',
                                    colour : 'green',
                                    id : 'passwordChanged'
                                });

                                accedo.utils.fn.delay(function() {
                                    telstra.sua.core.components.loadingOverlay.turnOffLoading();
                                    self.dispatchEvent('telstra:core:historyBack');
                                }, 2);
                            },
                            onFailure: function(json) {
                                telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                if(!accedo.utils.object.isUndefined(json) && !accedo.utils.object.isUndefined(json.error) && !accedo.utils.object.isUndefined(json.error.errorcode)) {
                                    telstra.sua.core.components.infoMessageBar.addBar({
                                        text : 'Error (Code: ' + json.error.errorcode + ')' + json.error.errordescription,
                                        colour : 'red',
                                        id : 'accountUpdateError'
                                    });
                                }
                            },
                            onException:function() {
                                telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : 'Fail to change password.',
                                    colour : 'red',
                                    id : 'changePasswordFailed'
                                });
                            }
                        });
                    }
                    else {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();
                        
                        inputField1.setString('');
                        inputField2.setString('');
                        accedo.focus.manager.requestFocus(inputField1);
                    }
                },

                onKeyRed : function() {
                    if (!telstra.sua.core.main.keyboardShown)
                        telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 19941);
                },

                onKeyBack: function() {
                    if (telstra.sua.core.components.loadingOverlay.isLoadingOn() && telstra.sua.core.main.keyboardShown)
                        return;
                    
                    inputField2.setOption('nextDown', 'saveChangesButton');
                    self.dispatchEvent('telstra:core:historyBack');
                }
            });
        }
    });