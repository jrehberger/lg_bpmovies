/**
 * @fileOverview popupFreeMoviesTermsController
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
 */

accedo.define("telstra.sua.module.bpvod.controllers.popupFreeMoviesTermsController", [
    'telstra.sua.module.bpvod.views.popupFreeMoviesTermsView',
    "accedo.utils.object",
    "accedo.utils.fn",
    "telstra.sua.module.bpvod.utils",
    "telstra.sua.module.bpvod.config",
    "telstra.sua.core.components.loadingOverlay",
    "telstra.api.manager"],
    function() {

        /**
         * @class
         * @extends accedo.ui.popup
         */
        return function(opts) {
        	
        	accedo.console.info("popupFreeMoviesTermsController");
            if(!opts.videoItem || !opts.parentController){
                accedo.console.info("resumePlay popup missing paramter")
                return;
            }

            var config  = telstra.sua.module.bpvod.config,
            utils       = telstra.sua.module.bpvod.utils,
            loading     = telstra.sua.core.components.loadingOverlay,
            myPubObj    = null,
            self,

            //ui item
            popupMain = null,
            videoTitle = null,
            horizontalPanel = null,
            videoItem   = opts.videoItem,
            parentController = opts.parentController,
            skipHistory = opts.skipHistory || false,
            historyItem = opts.historyItem || {},
            textContent = null,
            scrollbar = null,
            textContainer = null,
            videoThumbnail = null,
            leftPanel = null,
            rightPanel = null,
            watchNow = null,
            cancelButton = null;

            var termsText = "<h2 blue>BigPond Movies - Free Video Terms of Use</h2>"

            +"<p>Please read these Terms carefully so that you understand them.  If you are reading on a TV screen and would like to read on your computer or print a copy, please contact us on 1800 502 502 and we will send you a copy of the terms by e-mail.</p>"

            +"<p>Capitalised words (such as Authorised Electronic Device) have special meanings, which are explained in the Dictionary at the end of these Terms.</p>"

            +"<p><strong> 1. Viewing a Free Video</strong></p>"

            +"<p class=\"subheading\">Free Video Experience</p>"

            +"<p> 1.1. From time to time we may provide a free Video for you to view.  Any such Video will be available for a limited time. </p>"

            +"<p> 1.2. We have no obligation to provide you free Videos.  Our Service can be accessed through multiple Authorised Electronic Devices and Computers.  A free Video may be accessible only through some of these means (eg, it may not be available from a Computer).</p>"

            +"<p> 1.3. You acknowledge that due to inherent differences in the Service, viewing a free Video may not involve the exact same experience as normally viewing a Video through the Service.</p>"

            +"<p> 1.4. We provide you the Video to watch for free.  However, you may be charged data-based download charges as part of your Broadband Connection for using the Service.  Or your Broadband Connection may be \"shaped\" (or intentionally slowed down) due to the volume of Video accessed through the Service.  </p>"

            +"<p> 1.5. Most BigPond Internet Access plans do not take into account Video accessed through the Service for the purpose of calculating excess data charges or shaping - the Video is \"unmetered\".  The exceptions are hourly plans including dial-up and satellite plans and international roaming.  You can check whether the Service is unmetered on your BigPond Internet Access plan by checking the terms of your plan.</p>"

            +"<p class=\"subheading\">These Terms</p>"

            +"<p> 1.6. By viewing a free Video, you agree to be bound by these Terms.  You declare you are over 18 years of age and have legal power to agree to these Terms.</p>"

            +"<p> 1.7. You remain responsible for any use of a free Video provided to you for viewing after you accept these terms.  You declare, and must ensure, that any Video accessed using the Service is under the control of a person over 18 years of age at all times.</p>"

            +"<p><strong> 2. The Service</strong></p>"

            +"<p class=\"subheading\">Important limitations to your use</p>"

            +"<p> 2.1. You must make sure you do not use the Service in a way that would result in you, us, or any other person affected by your actions, breaching any law (including by infringing any person's copyright or breaching these Terms).</p>"

            +"<p> 2.2. You may only view the Video in accordance with Content Terms applicable to the particular Video.  You must not use the Video in any manner inconsistent with the rights or restrictions set out in the Content Terms.  In particular, unless the Content Terms allow you to do so, you must not do any of the following:</p>"

            +"<p>a) display or make the Video available to the public or in a public space;</p>"

            +"<p>b) re-supply or re-transmit the Video to another person;</p>"

            +"<p>c) transfer the Video from your Computer or Authorised Electronic Device to any other device or computer (except to the extent normal operation results in transfer to a computer monitor or television screen); or</p>"

            +"<p>d) remove the Computer or Authorised Electronic Device from Australia while it contains any Video.</p>"

            +"<p class=\"subheading\">How you can access the Service</p>"

            +"<p> 2.3. You must only access the Service from your Computer or an Authorised Electronic Device, and it must be located in Australia.</p>"

            +"<p> 2.4. To access the Service from an Authorised Electronic Device, it must be connected to the Internet. Some Authorised Electronic Devices must be connected to the Internet through BigPond Internet Access in order to access the Service -to find out if this applies to your Authorised Electronic Device, check the terms that came with it or contact us.</p>"

            +"<p> 2.5. You must not interfere with, or allow interference with, the proper functioning of the Authorised Electronic Device (including with the DRM Software and any other digital rights management and copy protection technology).</p>"

            +"<p> 2.6. To access the Service on your Computer (where that option is available):</p>"

            +"<p>a) you must be able to access Our Website; </p>"

            +"<p>b) the Computer must meet the minimum requirements set out on Our Website;</p>"

            +"<p>c) the Computer must be running a properly functioning version of the DRM Software; and</p>"

            +"<p>d) you must not interfere with, or allow interference with, the proper functioning of the DRM Software.</p>"

            +"<p> 2.7. DRM Software required to use the Service on your Computer can be downloaded from Our Website without charge (though normal connection and data charges may apply).  You will have to agree to the licence terms and conditions associated with the DRM Software.</p>"

            +"<p> 2.8. You acknowledge the DRM Software may delete Videos from your Computer or Authorised Electronic Device after the viewing period set out in the Content Terms has expired.</p>"

            +"<p class=\"subheading\">More about Internet access and device requirements</p>"

            +"<p> 2.9. You are responsible for arranging and paying for a Broadband Connection that connects your Computer or Authorised Electronic Device to the Internet, allowing it to access the Service.  The Broadband Connection must meet the minimum requirements specified from time to time on Our Website.</p>"

            +"<p> 2.10. Videos typically constitute a large amount of data.  For that reason you must ensure:</p>"

            +"<p>a) your Computer or Authorised Electronic Device has sufficient space to store the Video; and</p>"

            +"<p>b) your Broadband Connection can download the Video (for example, your service provider will not shape your Broadband Connection in a way that stops you being able to complete the download).</p>"

            +"<p> 2.11. You acknowledge that: </p>"

            +"<p>a) the experience of downloading and playing the Video (including speed and smoothness) partly depends on your Broadband Connection, other devices and users connected to your Broadband Connection and other tasks you are using your Computer or Authorised Electronic Device for at the same time; and</p>"

            +"<p>b) you may not be able to immediately view a Video after choosing to view it - it may take some time for a sufficient portion of the Video to be downloaded before it can be displayed.</p>"

            +"<p class=\"subheading\">Further limitations on your use</p>"

            +"<p> 2.12. You must not use the Service or Our Website to do any of the following:</p>"

            +"<p>a) defame, abuse, harass or otherwise violate the legal rights of another person;</p>"

            +"<p>b) publish, post, upload, e-mail, distribute or disseminate (that is, \"transmit\") any inappropriate, profane, defamatory, obscene, indecent or unlawful content (including that which infringes another person's intellectual property);</p>"

            +"<p>c) transmit files that contain viruses, corrupted files or any similar software that may damage or adversely affect the operation of another person's computer, Our Website or the Service;</p>"

            +"<p>d) advertise or offer to sell any goods or services for any commercial purpose;</p>"

            +"<p>e) transmit surveys, contests, pyramid schemes, spam, unsolicited advertising or promotional materials or chain letters;</p>"

            +"<p>f) falsify or delete author attributions, legal or other proper notices, or proprietary designations or labels which specify the origin or source of any material;</p>"

            +"<p>g) collect or store personal information about another person; or</p>"

            +"<p>h) engage in any illegal activities.</p>"

            +"<p><strong> 3. Your Privacy</strong></p>"

            +"<p> 3.1. Please read our \"Protecting Your Privacy\" statement at http://www.telstra.com.au/privacy.  It sets out how we and our related companies collect, use and disclose your personal information (including for the purpose of marketing to  you) and your rights to access and correct that information.</p>"

            +"<p> 3.2. In certain circumstances we may need to disclose information about you to others.  We ask, and you agree, that we may subject to the Privacy Act 1988 (Cth):</p>"

            +"<p>a) disclose information about you to, and obtain information from, any financial institution or card issuer to verify your details and details of the credit card; </p>"

            +"<p>b) take steps to verify there is sufficient credit on your credit card to meet any charges we consider might become due in relation to the Service.</p>"

            +"<p>c) disclose information about you (including information relating to conduct of your account) to:</p>"

            +"<p>i) credit reporting agencies to obtain and maintain a Credit Information file about you;</p>"

            +"<p>ii) credit providers or collection agents to collect overdue payments which you owe us and to notify any default by you; and</p>"

            +"<p>iii) our contractors or agents who are involved in the supply of the Service; and</p>"

            +"<p>d) obtain and use information about your creditworthiness (including consumer or commercial credit reports) from credit reporting agencies, credit providers or other businesses that report on creditworthiness for the purpose of assessing any application by you or collecting overdue payments.</p>"

            +"<p> 3.3. We ask and you agree that we may, in accordance with our \"Protecting your Privacy\" statement and clause 3.2, disclose Personal Information about you (including sensitive information about you) to Video content providers and licensors for the purpose of assisting them to enforce their intellectual property rights.</p>"

            +"<p> 3.4. Our Website uses cookies.  Cookies are pieces of information stored on your Computer which allows us to track your use of Our Website and allows us to customise information you see, improving the experience of using our Website.  Most web browsers are pre-set to accept and store cookies.  You may choose to turn this functionality off, or set your browser to warn you when a cookie is being stored on your Computer.  For more information, use the \"Help\" menu in your web browser or contact the company that makes your web browser.</p>"

            +"<p><strong> 4. Content on the Service</strong></p>"

            +"<p class=\"subheading\">Others' commercial products</p>"

            +"<p> 4.1. The Service may contain information about products and services offered by third parties, including product details, pricing, availability, performance and editorial commentary.  We are not responsible for any reliance that you place on any of this content (including, but not limited to reviews and movie descriptions).</p>"

            +"<p class=\"subheading\">User generated material</p>"

            +"<p> 4.2. We cannot guarantee the accuracy, integrity or quality of content which other users post on or in relation to the Service, including in user opinion, message board or feedback areas.  We have the right, but not the obligation, to:</p>"

            +"<p>a) review such material before it is made available to other users; and</p>"

            +"<p>b) remove any content that we, in our sole discretion, consider objectionable (on the basis of language, content, relevance, appropriateness or likely offense to others).</p>"

            +"<p> 4.3. You are solely responsible for all material that you upload, post, e-mail, transmit or otherwise make available to us and other users (\"Your Content\").  You certify you own all intellectual property rights in Your Content or have been licensed to use it as you have (including in relation to your sub-license to us below).</p>"

            +"<p> 4.4. You grant to us, our agents, and contractors a worldwide, irrevocable, royalty-free, non-exclusive, sub-licensable licence to use, reproduce, communicate to the public, create derivative works of, distribute, publicly perform, publicly display, transfer, transmit, distribute and publish Your Content and subsequent versions of it for one or more of the following purposes:</p>"

            +"<p>a) displaying it on our Website and other users of the Service; and</p>"

            +"<p>b) storing and distributing to those seeking to download or otherwise acquire it or a copy of it;</p>"

            +"<p>in each case without attribution to you.  This licence applies to distribution and storage in any form, medium or technology now known or later developed.</p>"

            +"<p><strong> 5. Suspension and Cancellation</strong></p>"

            +"<p> 5.1. We may immediately cancel your access to the Service, including free Videos and our obligations under these Terms if:</p>"

            +"<p>a) you seriously breach these Terms, including by infringing the copyright or other intellectual property in any Video or the Service, or by using the Service in a way which we reasonably believe is illegal, likely to be found illegal or likely to cause damage to our reputation; and</p>"

            +"<p>b) we have notified you in writing of these circumstances and you have failed to remedy the situation within 14 days of our notice (or any longer period set out in the notice) or the circumstances cannot be remedied (in which case the cancellation is effective upon us giving notice).</p>"

            +"<p>Infringement of copyright or other intellectual property in any Video or the Service is a breach of these Terms which is not capable of remedy. </p>"

            +"<p> 5.2. We may also cancel your access to the Service, including free Videos, and our obligations under these Terms by giving you as much notice as we reasonably can if we are unable to provide the Service due to an event outside our reasonable control (such as an industrial strike or an act of nature). </p>"

            +"<p> 5.3. We may suspend or restrict provision of the Service to you in the notice period before we cancel your services under clauses 5.1 or 5.2.</p>"

            +"<p> 5.4. Clauses 2 (the Service), 3 (Privacy), 4 (Content on the Service), 5 (Cancellation and Suspension), 6 (Liability) and 7 (Miscellaneous Terms) and any other term which, by its nature survives, apply even after both parties' obligations under these Terms are otherwise cancelled or suspended.</p>"

            +"<p><strong> 6. Liability</strong></p>"

            +"<p> 6.1. We accept liability for the Service, but only to the extent set out in this clause 6.</p>"

            +"<p> 6.2. We will use reasonable care and skill in providing the Service and will provide it in accordance with these Terms.  However, due to the nature of the Service (including our reliance on some systems and services not owned or controlled by us) we cannot promise the Service will be continuous or fault-free.</p>"

            +"<p> 6.3. The Service is supplied to you as expressly stated in these Terms and the terms implied by consumer protection laws which cannot be excluded by us.  No other terms or conditions apply.</p>"

            +"<p> 6.4. As the Service is provided to you predominantly for personal, domestic or household use, we do not accept liability for any business-related losses that arise in relation to your use of the Service. However, we will accept such liability if it cannot be excluded due to the operation of legislation.  In those circumstances, our liability will be limited, if allowed under the legislation, to resupplying, repairing or replacing the relevant goods or services (as applicable) if it is fair and reasonable to do so.</p>"

            +"<p> 6.5. We are also not liable to you for:</p>"

            +"<p>a) any loss to the extent caused by you (for example through negligence or breach of these Terms); or</p>"

            +"<p>b) any loss caused by our failure to comply with our obligations where that failure is caused by events outside our reasonable control (such as an industrial strike or act of nature).</p>"

            +"<p> 6.6. You are liable to us if you breach these Terms or act negligently (as defined by the courts).  However you are not liable for any loss we suffer to the extent that it is caused by us (for example through our negligence or breach of these Terms).</p>"

            +"<p><strong> 7. Miscellaneous Terms</strong></p>"

            +"<p> 7.1. Neither you, nor we, waive any right under these Terms just because we do not exercise them or delay in exercising them.</p>"

            +"<p> 7.2. Unless otherwise stated, a notice relating to these Terms must be in writing (which includes e-mail).  If we need to notify you, we may send a notice by e-mail to any address you have provided or post a notice to any address you provide.  If we use e-mail, you will be taken to have received the e-mail once it leaves our servers unless we receive evidence to the contrary (for example, a \"bounce-back\").  If we use post, you will be taken to have received the notice 2 Business Days after we post it, unless we receive evidence to the contrary (being days other than Saturday, Sunday or a public holiday in New South Wales).</p>"

            +"<p> 7.3. Your rights under these Terms belong to you alone.  You may not transfer your rights or obligations in respect of the Service or these Terms without our prior consent -which we will not unreasonably withhold.</p>"

            +"<p> 7.4. We may, from time to time, ask another party to provide some aspect of the Service.  </p>"

            +"<p> 7.5. We may transfer all our rights or obligations (or both) in respect of the Service and under these Terms to any reputable, credit-worthy third party at any time.  We will notify you within 30 days of any such transfer.</p>"

            +"<p> 7.6. These Terms are governed by the law in force in New South Wales, and both you and we submit to the jurisdiction of the courts of New South Wales.</p>"

            +"<p><strong> 8. Meanings of special words</strong></p>"

            +"<p> 8.1. Words with special meanings in these Terms are as follows:</p>"

            +"<p><strong>Authorised Electronic Device</strong> means the Telstra T-Box-supplied by us and such internet-ready televisions and other devices which we authorise from time to time to access the Service.</p>"

            +"<p><strong>BigPond Internet Access</strong> means a connection to the Internet provided by us under the BigPond brand.</p>"

            +"<p><strong>Broadband Connection</strong> means an Internet connection which links your Computer or Authorised Electronic Device to the Internet and to our Service and which meets the minimum connection speed required by us from time to time as set out on Our Website.</p>"

            +"<p><strong>Computer</strong> means a personal computer (which includes desktops, laptops and notebooks) running operating systems and software required by us as notified on our Website.</p>"

            +"<p><strong>Content Terms</strong> means the terms specific to your use of a Video accessed through the Service. These Terms are specified through the Service.  For example, how long or how many times you can view the Video, prohibitions on downloading, redistributing, altering or deleting the content of the video or advertising or marketing material forming part of, or associated with, the Video.</p>"

            +"<p><strong>DRM Software</strong> means the digital rights management and copy protection software installed on your Authorised Electronic Device (or required as part of a firmware upgrade) or specified on Our Website as a requirement for use of the Service on your Computer (as applicable).</p>"

            +"<p><strong>Our Website</strong> means the web site at http://downloads.bigpondmovies.com.</p>"

            +"<p><strong>Service</strong> means the service of supplying Videos via the Internet to your Computer or Authorised Electronic Device.</p>"

            +"<p><strong>Video</strong> means the audio-visual content such as movies and TV episodes made available via the Service.</p>"

            +"<p><strong>We, our and us</strong> refers to Telstra Corporation Limited (ABN 33 051 775 556) and its employees, agents, sub-agents and their respective employees.</p>"

            +"<p> 8.2. References in this Agreement to your use of the Service include use by you and by any other person accessing a free Video provided to you.</p>";

            /**
             * Set-up inheritance.
             * @scope telstra.sua.module.bpvod.components.popupConnectionInfo
             */
            myPubObj = accedo.utils.object.extend(accedo.ui.popup(opts), {

                init : function(){

                    self = this;
                    
                    //setView
                    self.setView(telstra.sua.module.bpvod.views.popupFreeMoviesTermsView);

                    accedo.utils.fn.delay(function(){
                        
                        popupMain = self.get('popupBg').getById('popupMain');
                        videoTitle = popupMain.getById('videoTitle');
                        horizontalPanel = popupMain.getById('horizontalPanel');
                        leftPanel = horizontalPanel.getById('leftPanel');
                        rightPanel = horizontalPanel.getById('rightPanel');
                        textContainer = leftPanel.getById('textContainer');
                        textContent = textContainer.getById('textContent');
                        scrollbar = popupMain.getById('scrollbar');
                        videoThumbnail = popupMain.getById('videoItemButton').getById('videoItemContainer').getById('videoThumbnail');
                        watchNow = rightPanel.getById('watchNow');
                        cancelButton = rightPanel.getById('cancel');

                        videoTitle.setText(videoItem.title);
                        videoThumbnail.setSrc(telstra.api.manager.resizer(videoItem.coverimage,135,192, true));
                        textContent.setText(termsText);

                        var scrollText = function(direction){ //0 = not move, 1 = up, 2 = down
                            var containerHeight = textContainer.getRoot().getHTMLElement().offsetHeight;
                            var currentHeight  = textContent.getRoot().getHTMLElement().scrollHeight;
                            var currentTop = textContent.getRoot().getHTMLElement().offsetTop;

                            accedo.console.log("srollText: containerHeight: " + containerHeight + " currentHeight: " + currentHeight + " currentTop: " + currentTop);

                            var targetTop;

                            if(direction == 1){
                                targetTop = currentTop + 95;
                                if(targetTop > 0){
                                    targetTop = 0;
                                }
                            }else if (direction == 2){
                                targetTop = currentTop - 95;
                                if(targetTop < -1 * (currentHeight - containerHeight) ){
                                    targetTop = -1 * (currentHeight - containerHeight);
                                }
                            }

                            if(direction != 0){
                                currentTop = targetTop;
                                textContent.getRoot().setStyle({
                                    top: targetTop + "px"
                                });
                            }

                            scrollbar.update(-1* currentTop, containerHeight, currentHeight );
                        }

                        leftPanel.setOption("nextUp",function(){
                            scrollText(1);
                        });

                        leftPanel.setOption("nextDown",function(){
                            scrollText(2);
                        });

                        accedo.utils.fn.defer(scrollText, 1, 0);

                        watchNow.addEventListener('click', function() {
                        	accedo.console.log("watchNow click");
                            loading.turnOnLoading();
							
							/*
                            utils.setDispatchController(parentController, 'videoPageController', {
                                videoItem : videoItem
                            }, skipHistory, historyItem);
                            self.close();
                            loading.turnOffLoading();
                            return;
                            */

                            telstra.api.manager.movies.getDemoLicense(videoItem.product.productid,{
                                onSuccess:function(json){
                                	accedo.console.log("getDemoLicense onSuccess");
                                	accedo.console.log(JSON.stringify(json));
                                	for(var i in json)
                                		accedo.console.log(i+':'+json[i]);
                                	
                                    var eupId = json.licenseid;
                                    videoItem.eupId = eupId;
                                	accedo.console.log("videoItem.eupId:"+videoItem.eupId);
                                    utils.setDispatchController(parentController, 'videoPageController', {
                                        videoItem : videoItem
                                    }, skipHistory, historyItem);
                                    self.close();
                                },
                                onFailure:function(json){
                                	accedo.console.log("getDemoLicense onFailure");
                                    utils.errorMessageokPopup(json);
                                },
                                onException:function(){
                                	accedo.console.log("getDemoLicense onException");
                                },
                                onComplete: function(){
                                	accedo.console.log("getDemoLicense onComplete");
                                    loading.turnOffLoading();
                                }
                            });
                        });

                        cancelButton.addEventListener('click', function() {
                            self.close();
                            if(skipHistory){
                                parentController.dispatchEvent('telstra:core:historyBack');
                            }
                        });

                        accedo.focus.manager.requestFocus(watchNow);
                    },0.3);
                },

                onKeyBack : function(){
                    self.close();
                }

            });

            myPubObj.init();
            return myPubObj;
        }
    });