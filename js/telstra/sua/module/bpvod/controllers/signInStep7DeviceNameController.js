/**
 * @fileOverview
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 *
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.signInStep7DeviceNameController", [
    // dependency
    "telstra.api.usage",
    "telstra.sua.module.bpvod.views.signInStep7DeviceNameView",
    "telstra.sua.module.bpvod.controllers.signInTemplateController"],
    function(){

        return function(opts){

            // Shorten for namespacing
            var utils = telstra.sua.module.bpvod.utils,
            inputField,
            self;

            var myPubObj = accedo.utils.object.extend(telstra.sua.module.bpvod.controllers.signInTemplateController(opts), {

                onCreate : function(){
                    self = this;

                    this.callcode = 19929;

                    this.init(telstra.sua.module.bpvod.views.signInStep7DeviceNameView);
                        
                    inputField = this.get('inputField');
                    inputField.setString(this.param.devicename);

                    inputField.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(self.continueButton);
                    }

                    this.continueButton.setOption("nextUp",'inputField');
                    this.backButton.setOption("nextUp",'inputField');

                    accedo.utils.fn.delay(function(){
                        accedo.focus.manager.requestFocus(inputField);
                    },0.5);
                },

                //verify User
                continueButtonAction : function(){

                    var deviceName = inputField.getCleanedResultStr();

                    if (deviceName == null || accedo.utils.object.isUndefined(deviceName) || deviceName == "") {
                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : "Please enter a device name for your Western Digital device.",
                            colour : "red",
                            id : 'enterDeviceName'
                        });
                    }
                    else {
                        self.param.devicename = deviceName;

                        utils.setDispatchController(myPubObj, 'signInStep8TCController', self.param, true);
                    }
                },

                //change pin
                backButtonAction : function() {
                    inputField.setOption('nextDown', 'continueButton');
                    utils.setDispatchController(myPubObj, 'signInStep6PinSettingController', self.param, true);
                }
            });

            return myPubObj;
        }
    });