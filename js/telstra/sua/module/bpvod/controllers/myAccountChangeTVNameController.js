/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    'telstra.sua.module.bpvod.controllers.myAccountChangeTVNameController',
    ['accedo.utils.object',
    'accedo.ui.controller',
    'accedo.focus.manager',
    'accedo.device.manager',
    'telstra.api.manager',
    'telstra.api.usage',
    'telstra.sua.module.bpvod.utils',
    'telstra.sua.core.components.infoMessageBar',
    'telstra.sua.core.components.loadingOverlay',
    'telstra.sua.module.bpvod.views.myAccountChangeTVNameView'],
    function(){
        return function(opts){
            
            var views = telstra.sua.module.bpvod.views,
            utils = telstra.sua.module.bpvod.utils,
            optionsList = [],
            self, inputField, editDetailsButton, closeButton,
            mode = 'change';

            return accedo.utils.object.extend(accedo.ui.controller(opts), {

                onCreate: function() {
                    telstra.sua.core.main.mainNavBar.setMenuButton("HELP");

                    self = this;
                    
                    //Set the view
                    self.setView(views.myAccountChangeTVNameView);

                    inputField = self.get('inputField');
                    editDetailsButton = self.get('editDetailsButton');
                    closeButton = self.get('closeButton');

                    inputField.getInputField().addInputFieldClass("fill");
                    inputField.setString(telstra.api.manager.movies.accountInfo.devicename);
                    inputField.deactivate();

                    inputField.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(editDetailsButton);
                    };

                    editDetailsButton.addEventListener('click', function(){
                        if (mode == 'change') {
                            mode = 'save';
                            inputField.activate();
                            inputField.getInputField().removeInputFieldClass("fill");
                            inputField.setString("");
                            editDetailsButton.setText('Save Changes');
                            closeButton.setText('Cancel');
                            editDetailsButton.setOption('nextUp', 'inputField');
                            closeButton.setOption('nextUp', 'inputField');
                            accedo.focus.manager.requestFocus(inputField);
                        }
                        else {
                            self.saveAction();
                        }
                    });

                    editDetailsButton.onFocus = function() {
                        inputField.setOption('nextDown', 'editDetailsButton');
                    };

                    closeButton.addEventListener('click',function(){
                        if (mode == 'save') {
                            self.changeBack();
                        }
                        else {
                            inputField.setOption('nextDown', 'editDetailsButton');
                            self.dispatchEvent('telstra:core:historyBack');
                        }
                    });

                    closeButton.onFocus = function() {
                        inputField.setOption('nextDown', 'closeButton');
                    };

                    optionsList.push(
						{
							type: "red",
							title: "Help"
						}
					);
					//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);

                    accedo.focus.manager.requestFocus(editDetailsButton);
                },

                changeBack: function() {
                    mode = 'change';
                    inputField.deactivate();
                    inputField.getInputField().removeInputFieldClass("fill");
                    inputField.setString(telstra.api.manager.movies.accountInfo.devicename);
                    editDetailsButton.setText('Edit Details');
                    closeButton.setText('Close');
                    editDetailsButton.setOption('nextUp', null);
                    closeButton.setOption('nextUp', null);
                    accedo.focus.manager.requestFocus(editDetailsButton);
                },

                saveAction: function() {
                    var input = inputField.getCleanedResultStr();

                    if(input.length > 0){
                        telstra.sua.core.components.loadingOverlay.turnOnLoading();

                        telstra.api.manager.movies.accountInfo.devicename = input;

                        utils.updateMoviesAccount(telstra.api.manager.movies.accountInfo, {
                            onSuccess:function() {
                                telstra.sua.core.components.loadingOverlay.turnOffLoading();
                                
                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : 'Your TV or Blu-ray name has been changed.',
                                    colour : 'green',
                                    id : 'tvNameChanged'
                                });
                                
                                self.changeBack();
                            }
                        });
                    }else{
                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : 'Please enter a device name for your Western Digital device.',
                            colour : 'red',
                            id : 'enterDeviceName'
                        });
                    }
                },

                onKeyRed : function() {
                    if (!telstra.sua.core.main.keyboardShown)
                        telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 19948);
                },

                onKeyBack: function() {
                    if (telstra.sua.core.components.loadingOverlay.isLoadingOn() && telstra.sua.core.main.keyboardShown)
                        return;

                    inputField.setOption('nextDown', 'editDetailsButton');
                    self.dispatchEvent('telstra:core:historyBack');
                }
            });
        }
    });
