/**
 * @fileOverview popupSoftExitController
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
 */

accedo.define("telstra.sua.module.bpvod.controllers.popupSoftExitController", [
    'telstra.sua.module.bpvod.views.popupSoftExitView',
    "accedo.utils.object",
    "telstra.sua.module.bpvod.utils",
    "telstra.sua.module.bpvod.config",
    "telstra.sua.module.bpvod.session",
    "telstra.api.manager"],
    function() {

        /**
         * @class
         * @extends accedo.ui.popup
         */
        return function(opts) {

            if(!opts.videoItem || !opts.parentController ){
                accedo.console.info("popupSoftExit popup missing paramter")
                return;
            }
            var myPubObj,
            self,
            config  = telstra.sua.module.bpvod.config,
            utils       = telstra.sua.module.bpvod.utils,
            isUndefined = accedo.utils.object.isUndefined,
            session     = telstra.sua.module.bpvod.session,
            videoItem   = opts.videoItem,
            parentController = opts.parentController,
            
            mainPopup = null,
            videoTitle = null,
            videoItemThumbnail = null,
            descriptionLabel = null,
            episodeLabel = null,

            buttonPanel = null,
            buttonArray = [];

            /**
             * Set-up inheritance.
             * @scope telstra.sua.module.bpvod.components.popupConnectionInfo
             */
            myPubObj = accedo.utils.object.extend(accedo.ui.popup(opts), {

                resume : function(){
                    accedo.console.log("[step 20] resume from " + videoItem.resumeTime);
                    self.close();

                    if (parentController.playVideo && videoItem.resumeTime && videoItem.resumeTime != null) {
                        //parentController.playVideo(videoItem.resumeTime);
                        parentController.resumeVideo(videoItem.resumeTime);
                    } else if (parentController.playVideo) {
                        //parentController.playVideo();
                        parentController.replayVideo();
                    }
                },

                replay : function(){
                    accedo.console.log("replay");
                    self.close();

                    if (parentController.replayVideo) {
                        parentController.replayVideo();
                    }
                },

                signIn : function(){
                    accedo.console.log("get to sign in / join now page");
                    self.close();
                    parentController.dispatchEvent('telstra:core:clearTillLast', {moduleController : 'mainController', moduleId: 'bpvod'});
                    utils.setDispatchController(parentController, 'signInORjoinNowController', {}, true);
                },

                related : function(){
                    accedo.console.log("get to related");
                    self.close();
                    parentController.dispatchEvent('telstra:core:clearTillLast', {moduleController : 'mainController', moduleId: 'bpvod'});
                    utils.moveToRelatedMovie(videoItem, parentController);
                },

                browseList : function(){
                    accedo.console.log("browse Movie Listing");
                    self.close();
                    parentController.dispatchEvent('telstra:core:clearTillLast', {moduleController : 'mainController', moduleId: 'bpvod'});
                    utils.moveToBigPondMovie(parentController);
                },
                
                browseFreeMovies : function(){
                    accedo.console.log("browse Free Movie Listing");
                    self.close();
                    parentController.dispatchEvent('telstra:core:clearTillLast', {moduleController : 'mainController', moduleId: 'bpvod'});
                    utils.moveToFreeMovie(parentController);
                },

                myRental : function(){
                    accedo.console.log("myRental");
                    self.close();
                    parentController.dispatchEvent('telstra:core:clearTillLast', {moduleController : 'mainController', moduleId: 'bpvod'});
                    utils.setDispatchController(parentController, 'myRentalController', {}, true);
                },

                init : function(){
                	accedo.console.log("popupSoftExitController init");
                    self = this;

                    //setView
                    this.setView(telstra.sua.module.bpvod.views.popupSoftExitView);

                    mainPopup = this.get('popupBg').getById('mainPopup');
                    videoTitle = mainPopup.getById('videoTitle');
                    videoItemThumbnail = mainPopup.getById('videoItem').getById('videoItemContainer').getById('videoItemThumbnail');
                    episodeLabel = mainPopup.getById('videoItem').getById('episodeLabel');
                    buttonPanel = mainPopup.getById('buttonPanel');

                    videoTitle.setText(videoItem.title);

                    if(telstra.sua.module.bpvod.utils.isEpisode(videoItem)){
                        videoItemThumbnail.getRoot().addClass("tvItem");
                        videoItemThumbnail.setSrc(telstra.api.manager.resizer(videoItem.coverimage,113,77, true));
                        episodeLabel.setText("Season " + videoItem.seasonnumber + "<br/>Episode " +  videoItem.episodenumber);
                    }else{
                        videoItemThumbnail.setSrc(telstra.api.manager.resizer(videoItem.coverimage,113,160, true));
                    }

                    var displayHelper = function(component, data) {
                        component.setText(data.text);
                        component.getRoot().addClass(data.css);

                        //click event
                        component.addEventListener('click', function() {
                            data.action();
                        });
                    };

                    var isEpisode = utils.isEpisode(videoItem);
                    
                    if(utils.isFree(videoItem)){
                        mainPopup.getRoot().addClass('freeSoftExit');
                        descriptionLabel = mainPopup.getById('descriptionLabel');
                        accedo.console.log('opts.finished '+opts.finished)
                        if(!opts.finished){
                            if(!session.isSignedIn){
                                descriptionLabel.setText("<b>We hope you are enjoying your free movie.</b><br/><br/>"+
                            "If you don't already have a BigPond Movies "+
                            "account join now to access many more great "+
                            "movies and TV shows direct to your TV. <br/><br/>"+
                            "Free movies are available for a limited time. If "+
                            "you exit this movie and play it again, it will start "+
                            "from the beginning.");
                            }else{
                                descriptionLabel.setText("<b>We hope you are enjoying your free movie.</b><br/><br/>"+
                            "Don't forget to check out our catalogue, as it's "+
                            "constantly being updated.");
                            }
                        }else{
                            if(session.isSignedIn){
                                descriptionLabel.setText("<b>We hope you enjoyed your free movie.</b><br/><br/>"+
                            "Don't forget to check out our catalogue, as it's "+
                            "constantly being updated.");

                            }else{
                                descriptionLabel.setText("<b>We hope you enjoyed your free movie.</b><br/><br/>"+
                            "Join now to access the latest and greatest movies and "+
                            "TV shows direct to your TV.");
                            }
                        }
                    }
                    
                    accedo.console.log("utils.isExpired(videoItem): " + utils.isExpired(videoItem));

                    if(opts.finished != true && (!utils.isExpired(videoItem) || utils.isFree(videoItem) )  ){
                        buttonArray.push({
                            id:       'resumeButton',
                            css:      'resumeButton button softExitButton play',
                            text:     'Resume',
                            action :  self.resume
                        });
                    }

                    if(!utils.isExpired(videoItem) && !utils.isFree(videoItem)){
                        buttonArray.push({
                            id:       'replayButton',
                            css:      'replayButton button softExitButton fromStart',
                            text:     'Play from start',
                            action :  self.replay
                        });
                    }

                    if(!session.isSignedIn){
                        buttonArray.push({
                            id:       'joinNowButton',
                            css:      'joinNowButton button softExitButton',
                            text:     'Join Now or Sign In',
                            action :  self.signIn
                        });
                    }
                    
                    var showRelatedMoviesBtn = true; 
                    var actors = (isUndefined(videoItem.actors)||isUndefined(videoItem.actors.actor)) ? [] : videoItem.actors.actor;
            		var directors = (isUndefined(videoItem.directors)||isUndefined(videoItem.directors.director)) ? [] : videoItem.directors.director;
            		if (!accedo.utils.object.isArray(actors)){
	                	actors = [actors];
	                }
	                if (!accedo.utils.object.isArray(directors)){
	                    directors = [directors];
	                }
	                if (actors.length == 0 && directors.length == 0){
	                	showRelatedMoviesBtn = false;
	                }

                    if( !utils.isFree(videoItem) || (opts.finished || session.isSignedIn)  ){
                        if (showRelatedMoviesBtn){
                        	buttonArray.push({
	                            id:       'relatedButton',
	                            css:      'relatedButton button softExitButton',
	                            text:     "View Related " + (isEpisode? "TV Episodes":"Movies"),
	                            action :  self.related
	                        });
                        }
                    }

                    buttonArray.push({
                        id:       'movieListButton',
                        css:      'movieListButton button softExitButton',
                        text:     "Browse BigPond Movies",
                        action :  self.browseList
                    });

                    if(!utils.isFree(videoItem)){
                        buttonArray.push({
                            id:       'myRentalButton',
                            css:      'myRentalButton button softExitButton',
                            text:     "View My Rentals",
                            action :  self.myRental
                        });                        
                    }

					if (buttonArray.length == 4){
						buttonPanel.getRoot().addClass("only-four");
					}
                    var ds = accedo.data.ds();
                    buttonPanel.setDisplayHelper(displayHelper);
                    buttonPanel.setDatasource(ds);
                    ds.appendData(buttonArray);

                    accedo.utils.fn.defer(accedo.utils.fn.bind(accedo.focus.manager.requestFocus,  accedo.focus.manager), buttonPanel);
                },
            
            	onKeyBack : function(){
                    if(session.isSignedIn){
                    	self.myRental();
                    }
                    else{
                    	self.browseFreeMovies();
                    }
                },
            
            });

            myPubObj.init();
            return myPubObj;
        }
    });