/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    'telstra.sua.module.bpvod.controllers.myAccountChangePinController',
    ['accedo.utils.object',
    'accedo.ui.controller',
    'accedo.focus.manager',
    'telstra.api.manager',
    'telstra.api.usage',
    'telstra.sua.module.bpvod.utils',
    'telstra.sua.core.components.infoMessageBar',
    'telstra.sua.core.components.loadingOverlay',
    'telstra.sua.module.bpvod.views.myAccountChangePinView'],
    function(){
        return function(opts) {

            var utils = telstra.sua.module.bpvod.utils,
            views = telstra.sua.module.bpvod.views,
            optionsList = [],
            self, inputField1, inputField2, saveChangesButton, cancelButton;

            return accedo.utils.object.extend(accedo.ui.controller(opts), {

                onCreate: function() {
                    telstra.api.usage.log("bpvod", "my_account_change_pin", {
                        logMessage: "bigpond movies:my account:change_pin"
                    });

                    telstra.sua.core.main.mainNavBar.setMenuButton("HELP");
                    
                    self = this;

                    //Set the view
                    self.setView(views.myAccountChangePinView);

                    inputField1 = self.get('inputField1');
                    inputField2 = self.get('inputField2');
                    saveChangesButton = self.get('saveChangesButton');
                    cancelButton = self.get('cancelButton');
                    
                    //test - predefined fields
                    /*inputField1.setString('1111');
                    inputField2.setString('1111');*/
                    //test

                    inputField1.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(inputField2);
                    };
                    inputField2.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(saveChangesButton);
                    };
                    inputField2.zeroLengthCallback = function() {
                        accedo.focus.manager.requestFocus(inputField2);
                    };

                    saveChangesButton.addEventListener('click', function() {
                        self.saveAction();
                    });

                    saveChangesButton.onFocus = function() {
                        inputField2.setOption('nextDown', 'saveChangesButton');
                    };

                    cancelButton.addEventListener('click',function() {
                        self.onKeyBack();
                    });

                    cancelButton.onFocus = function() {
                        inputField2.setOption('nextDown', 'cancelButton');
                    };
                    
                    optionsList.push(
						{
							type: "red",
							title: "Help"
						}
					);
					//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);

                    accedo.focus.manager.requestFocus(inputField1);
                },

                saveAction: function() {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();

                    var input1 = inputField1.getCleanedResultStr();
                    var input2 = inputField2.getCleanedResultStr();

                    if(input1.length == 4 && input2.length == 4) {
                        if(utils.checkIdentical(input1, input2)) {
                            telstra.api.manager.movies.accountInfo.accountpin = input1;

                            utils.updateMoviesAccount(telstra.api.manager.movies.accountInfo, {
                                onSuccess: function() {
                                    telstra.sua.core.components.infoMessageBar.addBar({
                                        text : 'Your account PIN has been changed.',
                                        colour : 'green',
                                        id : 'pinChanged'
                                    });

                                    accedo.utils.fn.delay(function() {
                                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                        if(opts.context.accountpin){
                                            opts.context.pinChecked = true;
                                            utils.setDispatchController(self, 'signInStep6PinSettingController', opts.context, true);
                                        }else{
                                            self.dispatchEvent('telstra:core:historyBack');

                                            if(opts.context.callback){
                                                opts.context.callback();
                                            }
                                        }
                                    }, 2);
                                }
                            });
                        }
                        else {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();

                            inputField1.setString('');
                            inputField2.setString('');
                            accedo.focus.manager.requestFocus(inputField1);

                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : 'Please enter the same 4 digit PIN in both fields.',
                                colour : 'red',
                                id : 'enterSameDigits'
                            });
                        }
                    }
                    else {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();
                        
                        inputField1.setString('');
                        inputField2.setString('');
                        accedo.focus.manager.requestFocus(inputField1);

                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : 'Your PIN must be 4 digits long.',
                            colour : 'red',
                            id : 'enterFourDigits'
                        });
                    }
                },

                onKeyRed : function() {
                    telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 19943);
                },

                onKeyBack: function() {
                    if (telstra.sua.core.components.loadingOverlay.isLoadingOn())
                        return;

                    inputField2.setOption('nextDown', 'saveChangesButton');
                    
                    if (opts.context.createNewPIN) {
                        utils.setDispatchController(self, 'signInStep5AccountPinController', opts.context, true);
                        accedo.console.log(opts.context);
                    }
                    else {
                        self.dispatchEvent('telstra:core:historyBack');
                    }
                }
            });
        }
    });