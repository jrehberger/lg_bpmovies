/**
 * @fileOverview
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.controllers.videoPageController",
    ["accedo.utils.object",
    "accedo.ui.controller",
    "accedo.device.manager",
    "accedo.focus.manager",
    "accedo.focus.managerMouse",
    "telstra.sua.module.bpvod.views.videoPageView",
    "telstra.sua.module.bpvod.config",
    "telstra.sua.core.error",
    "telstra.sua.core.components.loadingOverlay",
    "telstra.sua.core.controllers.popupOkInfoController",
    "telstra.sua.module.bpvod.session",
    "telstra.sua.module.bpvod.controllers.popupSoftExitController",
    "telstra.sua.module.bpvod.controllers.popupSpeedTestController"],
    function(){
        return function(opts){
            var videoItem, trackBall, curTime, duration, statusLabel, meterBars, self, 
            progressBarStopButton, progressBarBWButton, progressBarPlayPauseButton, progressBarFFButton,
            progressBarButtons = [], progressBarButtonsByType = {}, stopButtonClickHandler, bwButtonClickHandler, 
            playpauseButtonClickHandler, ffButtonClickHandler, 
            buttonMouseOverHandler, buttonMouseOutHandler,
            progressBarHolder, hideProgressBarTimeout = null, track, curTimeSec, durationSec,

            device = accedo.device.manager,
            media = accedo.device.manager.media,
            loading = telstra.sua.core.components.loadingOverlay,
            identification = accedo.device.manager.identification,
            config = telstra.sua.module.bpvod.config,
            session = telstra.sua.module.bpvod.session,
            utils = telstra.sua.module.bpvod.utils,
            isExiting = false,

            previousStatus = null,
            //slowPopupShown = false,
            videoPlayed = false,
            videoPaused = false,
            videoSeeking = false,
            videoJumping = false,
            videoFF = false,
            videoBW = false,
            currentVideoURLString = "",
            slowPopupRef = null,
            videoContainer,

            metering,
            meteredPosition = 0,
            metered_rotate,
            setOnNetLogo_firstTime = true;
            
            stopButtonClickHandler = function(){
            	accedo.console.log("stopButtonClickHandler");
            	if(previousStatus != "stopped" && previousStatus != "error") {
                    self.stopVideo();
                }
			};
            
            bwButtonClickHandler = function(){
            	accedo.console.log("bwButtonClickHandler");
            	self.skipBackward();
            };
            
            playpauseButtonClickHandler = function(){
            	accedo.console.log("playpauseButtonClickHandler; videoSeeking: " + videoSeeking);
            	if (videoPaused || videoSeeking){
            		self.playVideo();
                	videoPaused = false;
            	}
            	else{
                	self.pauseVideo();
                	videoPaused = true;
                }
            };
            
            ffButtonClickHandler = function(){
            	accedo.console.log("ffButtonClickHandler");
            	self.skipForward();
            };
            
            buttonMouseOverHandler = function(buttonType){
            	if (buttonType){
                	var currentButton = progressBarButtonsByType[buttonType];
                	if (currentButton){
                		accedo.focus.managerMouse.requestFocus(currentButton.root);
                		accedo.focus.manager.requestFocus(currentButton);
                	}
                }
            };
            
            buttonMouseOutHandler = function(buttonType){
            	accedo.focus.managerMouse.releaseFocus();
            };

            var myPubObj = accedo.utils.object.extend(accedo.ui.controller(opts), {

                isTrailer: false,
                connectionSpeedTesting: false,
                popupError: null,

                onCreate: function(context) {
                    if(!opts.context.videoItem){
                        accedo.console.log("error: videoItem is missing");
                    }

                    if(opts.context.trailer){
                        this.isTrailer = true;
                    }

                    videoItem = opts.context.videoItem;

                    if (opts.context.resumeTime) {
                        videoItem.resumeTime = opts.context.resumeTime;
                    }
                    
                    videoPlayed = false;
                    //slowPopupShown = false;
                    isExiting = false;
                    previousStatus = null;
                    currentVideoURLString = "";
                    slowPopupRef = null;

                    self = this;

                    //Set the view
                    self.setView(telstra.sua.module.bpvod.views.videoPageView);

                    track = self.get('track');
                    trackBall = self.get('trackBall');
                    curTime = self.get('curTime');
                    duration = self.get('duration');
                    statusLabel = self.get('statusLabel');
                    //meterBars = self.get('meterBars');
                    progressBarStopButton = self.get('progressBarStopButton'); 
                    progressBarBWButton = self.get('progressBarBWButton');
                    progressBarPlayPauseButton = self.get('progressBarPlayPauseButton');
                    progressBarFFButton = self.get('progressBarFFButton');
                    progressBarButtons = [
                    	{type:"stop", button:progressBarStopButton, clickHandler:stopButtonClickHandler},
                    	{type:"bw", button:progressBarBWButton, clickHandler:bwButtonClickHandler},
                    	{type:"playpause", button:progressBarPlayPauseButton, clickHandler:playpauseButtonClickHandler},
                    	{type:"ff", button:progressBarFFButton, clickHandler:ffButtonClickHandler}
                    ];
                    progressBarButtonsByType = {
                    	stop: progressBarStopButton,
                    	bw: progressBarBWButton,
                    	playpause: progressBarPlayPauseButton,
                    	ff: progressBarFFButton
                    };
                    
                    progressBarHolder = self.get('progressBarHolder');
                    metering = self.get('metering');
                    videoContainer = self.get("videoContainer");

                    self.initVideoPlayer();
					self.setMediaUrl();
                    self.setOnNetLogo();

                    progressBarHolder.setOption('nextUp', function(){
                        self.showProgressBarCall();
                    });
                    progressBarHolder.setOption('nextDown', function(){
                        self.showProgressBarCall();
                    });
                    progressBarHolder.setOption('nextLeft', function(){
                        self.showProgressBarCall();
                    });
                    progressBarHolder.setOption('nextRight', function(){
                        self.showProgressBarCall();
                    });
                    progressBarHolder.addEventListener('click',function(){
                        self.showProgressBarCall();
                    });
                    progressBarHolder.parent.addEventListener('click',function(){
                        self.showProgressBarCall();
                    });
                    
                    
                    track.parent.addEventListener('click',function(event){
                        var currentPosX = event ? event.clientX : 0;
                        //accedo.console.log('\\\\\\\\\\\ currentPosX: ' + currentPosX);
                        var xRange = {min:626, max:1048};
                        if (currentPosX > xRange.min && currentPosX < xRange.max){
                        	if(!durationSec || durationSec == null || durationSec == 0){
		                        durationSec = media.getTotalTime();
		                    }
		                    
		                    var targetSec = Math.round((durationSec * (currentPosX - xRange.min)) / (xRange.max - xRange.min));
		                    //accedo.console.log('\\\\\\\\\\\ targetSec: ' + targetSec);
		                    
                        	self.setPlayerTime(targetSec);
                        }
                    });
                    
                    
                    
                    progressBarPlayPauseButton.addEventListener('click',function(){
                        playpauseButtonClickHandler();
                    });
                    progressBarPlayPauseButton.parent.addEventListener('click',function(){
                        playpauseButtonClickHandler();
                    });
                    progressBarPlayPauseButton.setOption('nextLeft', function(){
                        //accedo.console.log("------- PLAY-PAUSE nextLeft -  go to BW -------");
                        bwButtonClickHandler();
                    });
                    progressBarPlayPauseButton.setOption('nextRight', function(){
                        //accedo.console.log("------- PLAY-PAUSE nextRight - go to FF -------");
                        ffButtonClickHandler();
                    });
                    progressBarPlayPauseButton.parent.addEventListener('mouseover',function(){
                        //buttonMouseOverHandler("playpause");
                        accedo.focus.managerMouse.requestFocus(progressBarPlayPauseButton.root);
                		accedo.focus.manager.requestFocus(progressBarPlayPauseButton);
                		self.cancelHideProgressbarCountDown();
                    });
                    progressBarPlayPauseButton.parent.addEventListener('mouseout',function(){
                        buttonMouseOutHandler("playpause");
                        self.showProgressBarCall();
                    });
                    
                    
                    
                    progressBarFFButton.addEventListener('click',function(){
                        ffButtonClickHandler();
                    });
                    progressBarFFButton.parent.addEventListener('click',function(){
                        ffButtonClickHandler();
                    });
                    progressBarFFButton.setOption('nextLeft', function(){
                        //accedo.console.log("------- FF nextLeft -  go to PLAY-PAUSE -------");
                        playpauseButtonClickHandler();
                    });
                    progressBarFFButton.parent.addEventListener('mouseover',function(){
                        //buttonMouseOverHandler("ff");
                        accedo.focus.managerMouse.requestFocus(progressBarFFButton.root);
                		accedo.focus.manager.requestFocus(progressBarFFButton);
                		self.cancelHideProgressbarCountDown();
                    });
                    progressBarFFButton.parent.addEventListener('mouseout',function(){
                        buttonMouseOutHandler("ff");
                    });
                    
                    
                    
                    progressBarBWButton.addEventListener('click',function(){
                        bwButtonClickHandler();
                    });
                    progressBarBWButton.parent.addEventListener('click',function(){
                        bwButtonClickHandler();
                    });
                    progressBarBWButton.setOption('nextLeft', function(){
                        //accedo.console.log("------- BW nextLeft -  go to STOP -------");
                        stopButtonClickHandler();
                    });
                    progressBarBWButton.setOption('nextRight', function(){
                        //accedo.console.log("------- BW nextRight -  go to PLAY-PAUSE -------");
                        playpauseButtonClickHandler();
                    });
                    progressBarBWButton.parent.addEventListener('mouseover',function(){
                        //buttonMouseOverHandler("bw");
                        accedo.focus.managerMouse.requestFocus(progressBarBWButton.root);
                		accedo.focus.manager.requestFocus(progressBarBWButton);
                		self.cancelHideProgressbarCountDown();
                    });
                    progressBarBWButton.parent.addEventListener('mouseout',function(){
                        buttonMouseOutHandler("bw");
                    });
                    
                    
                    
                    progressBarStopButton.addEventListener('click',function(){
                        stopButtonClickHandler();
                    });
                    progressBarStopButton.parent.addEventListener('click',function(){
                        stopButtonClickHandler();
                    });
                    progressBarStopButton.setOption('nextRight', function(){
                        //accedo.console.log("------- STOP nextRight -  go to BW -------");
                        bwButtonClickHandler(true);
                    });	                   
                    progressBarStopButton.parent.addEventListener('mouseover',function(){
                        //buttonMouseOverHandler("stop");
                        accedo.focus.managerMouse.requestFocus(progressBarStopButton.root);
                		accedo.focus.manager.requestFocus(progressBarStopButton);
                		self.cancelHideProgressbarCountDown();
                    });
                    progressBarStopButton.parent.addEventListener('mouseout',function(){
                        buttonMouseOutHandler("stop");
                    });
                    
                    //accedo.focus.manager.requestFocus(progressBarHolder);
                    accedo.focus.manager.requestFocus(progressBarStopButton);
                    accedo.utils.fn.delay(self.prePlaybackCheck, 1, videoItem.resumeTime);
                },

                initVideoPlayer: function() {
                	accedo.console.log("initVideoPlayer");
					media.init({
                        parentNode: videoContainer
                    });
                    media.registerCurrentTimeCallback(function(time, formattedTime, percentage, skipping){ 
						self.currentTimeCallback(time, formattedTime, percentage, skipping);    
                    });
                    media.registerEventCallback(function(evt, param){
                        self.eventCallback(evt, param);
                    });
                    media.registerStatusChangeCallback(function(status, isFF){
                        self.statusChangeCallback(status, isFF);
                    });
                },

                setMediaUrl: function(){   
                	accedo.console.log("setMediaUrl: " + identification.getDeviceType());
                	accedo.console.log("videoItem.eupId " + videoItem.eupId);
                	
                	//temp
                	//this.isTrailer = true;
                	//media.setMediaURL("http://moviesvod.vosm.bigpond.com/release_widevine/SFDev_DWS/12/49/LegendOfTheGuardiansTrailerSD__721409.wvm");
                	//return;
                	//temp end
                	
                    if(identification.getDeviceType() == "Workstation"){
                        media.setMediaURL("http://ibc.cdngc.net/Ipvision/Videos/8640/8640_Trailer.mp4");
                    }else if(this.isTrailer == true){
						currentVideoURLString = media.setMediaURL(videoItem.trailers.trailer.trailerurl, {
                            widevine : true,
                            drm_url : MMM_static.config.services['lg-movies-drm-trailer'] + videoItem.trailers.trailer.trailerlicenseid,
                            opt_data : 'ip:,streamid:,deviceid:'+accedo.device.manager.identification.getUniqueID()+',optdata:'+telstra.api.manager.movies.authToken,
                            device_id : accedo.device.manager.identification.getUniqueID(),
                            portal : "telstra"
                        });
                    }else{
                        currentVideoURLString = media.setMediaURL(videoItem.product.mediaurl, {
                            widevine : true,
                            drm_url : (videoItem.product.drmUrl? videoItem.product.drmUrl : MMM_static.config.services['lg-movies-drm']) + videoItem.eupId,
                            opt_data : 'ip:,streamid:,deviceid:'+accedo.device.manager.identification.getDeviceId()+',optdata:'+telstra.api.manager.movies.authToken,
                            device_id : accedo.device.manager.identification.getUniqueID(),
                            portal : "telstra"
                        });
                    }
                },

                setOnNetLogo: function (onNetFlag) {
                    if (setOnNetLogo_firstTime) {
                        var self = this;
                        telstra.sua.core.main.meteringCheck(function (result) {
                            var onOffNet = result;
                            if (onOffNet === false) {
                                metering.getRoot().addClass('metered');
                                metering.getRoot().setStyle({
                                    'backgroundPosition' : 'left top'
                                });
                                metered_rotate = setInterval(function () {
                                    self.setMetered_rotate();
                                }, 2000);
                            } else {
                                metering.getRoot().addClass('unmetered');
                            }
                        });
                        setOnNetLogo_firstTime = false;
                    }
                },

                setMetered_rotate: function () {
                    meteredPosition++;
                    if (meteredPosition === 1) {
                        metering.getRoot().setStyle({
                            'backgroundPosition' : 'left top'
                        });
                    } else if (meteredPosition === 2) {
                        metering.getRoot().setStyle({
                            'backgroundPosition' : 'left center'
                        });
                    } else if (meteredPosition === 3) {
                        metering.getRoot().setStyle({
                            'backgroundPosition' : 'left bottom'
                        });
                        meteredPosition = 0;
                    }
                },

                //check if it is signed in or free
                prePlaybackCheck: function(time){
                	accedo.console.log("prePlaybackCheck isTrailer:"+self.isTrailer);
                	
                    if(self.isTrailer){
                        self.playVideo();
                        return;
                    }
                    if(session.isSignedIn && !utils.isFree(videoItem)){
                        self.prePlaybackSave(time);
                    }else{
                        self.playVideo(time);
                    }

                    if(utils.isFree(videoItem)){
                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : "This FREE movie is brought to you by BigPond.",
                            colour : "green",
                            id : 'freeMovieMessage',
                            time: 30
                        });
                    }
                },

                //to save the status before and after playback
                prePlaybackSave: function(time){
                    if(!time){
                        time = null;
                    }

                    utils.updateRentedMovieStatus(videoItem, {
                        resumeTime:time,
                        watchNow:true
                    }, function(){
                        self.playVideo(time);
                    });
                },

                updateBar: function(percentage){
                    var left = 496;
                    var width = 415;
                    var newWidth = Math.round(width * percentage/100);
                    track.getRoot().setStyle({
                        width: newWidth + "px"
                    });
                    trackBall.getRoot().setStyle({
                        left: ((left+2) + newWidth - 7) + "px"
                    });
                },

                showProgressBar: function(){
                	progressBarHolder.show();
                	telstra.sua.core.main.mainNavBar.show();
                	/*if(hideProgressBarTimeout != null){
                        clearTimeout(hideProgressBarTimeout);
                        hideProgressBarTimeout = null;
                    }*/
                    if (videoFF){
                    	accedo.focus.manager.requestFocus(progressBarFFButton);
                    }
                    else if (videoBW){
                    	accedo.focus.manager.requestFocus(progressBarBWButton);
                    }
                    else{
                    	accedo.focus.manager.requestFocus(progressBarPlayPauseButton);
                    }
                },

                hideProgressBar: function(){  
                	progressBarHolder.hide();
                	telstra.sua.core.main.mainNavBar.hide();
                	accedo.focus.manager.requestFocus(progressBarHolder);
                },

                hideProgressbarCountDown: function(){
                    if(hideProgressBarTimeout != null){
                        clearTimeout(hideProgressBarTimeout);
                        hideProgressBarTimeout = null;
                    }
                    hideProgressBarTimeout = accedo.utils.fn.delay(self.hideProgressBar, 10);
                },
                
                cancelHideProgressbarCountDown: function(){
                    if(hideProgressBarTimeout != null){
                        clearTimeout(hideProgressBarTimeout);
                        hideProgressBarTimeout = null;
                    }
                },

                //handling to be implement when the video is asked to stop
                playbackDone: function(time, callback){
                	if(!videoPlayed && !isExiting){
                        return;
                    }
                    videoPlayed = false;

                    //handle trailers
                    if(self.isTrailer){
                        if(!callback){
                            callback = function(){
                                self.onUnload();
                                self.dispatchEvent('telstra:core:historyBack');
                            }
                        }
                        callback();
                        return;
                    }

                    if(!time || time == null){ //when render Complete
                        time = 0;
                        session.lastViewVideoItem = null;
                    }else{ //when stop in middle
                        videoItem.resumeTime = time;
                        if(!utils.isFree(videoItem)){
                            videoItem.lastView = true;
                            session.lastViewVideoItem = videoItem;
                        }
                    }

                    if(!callback){
                        if(previousStatus == "error"){
                            callback = function(){};
                        }else{
                            callback = function(){
                                if(slowPopupRef && slowPopupRef != null){
                                    slowPopupRef.close();
                                    slowPopupRef = null;
                                }
                                
                                accedo.utils.fn.delay(function(){ 
                                    telstra.sua.module.bpvod.controllers.popupSoftExitController({
                                        videoItem : videoItem,
                                        parentController : self,
                                        finished : (time == (null || 0)? true: false)
                                    });
                                }, 1);
                                
                            }
                        }
                    }

                    if(session.isSignedIn && !utils.isFree(videoItem) && telstra.sua.core.main.isOnline){
                        statusLabel.setText("Saving...");

                        utils.updateRentedMovieStatus(videoItem, {
                            resumeTime:time,
                            watchNow:true
                        }, function(){
                            statusLabel.setText("Stopped");
                            accedo.focus.manager.requestFocus(progressBarStopButton);
                            if(hideProgressBarTimeout != null){
		                        clearTimeout(hideProgressBarTimeout);
		                        hideProgressBarTimeout = null;
		                    }
		                    self.hideProgressBar();
                            callback();
                        });
                    }else{ // it must be free content
                        callback();
                    }
                },

                reportError: function(reference){
                	accedo.console.log("reportError with reference: " + reference);
                	
                	loading.turnOffLoading();
                	if(hideProgressBarTimeout != null){
                        clearTimeout(hideProgressBarTimeout);
                        hideProgressBarTimeout = null;
                    }
                    self.hideProgressBar();
                    
                    var receiptNo = null;
                    if(videoItem != null && !accedo.utils.object.isUndefined(videoItem.purchase)){
                        receiptNo = videoItem.purchase.purchaseid;
                    }
                    
                    if(slowPopupRef && slowPopupRef != null){
                        slowPopupRef.close();
                        slowPopupRef = null;
                    }
                    
                    var callback = function(){
                        if (self.popupError){
	                		self.popupError.close();
	                	}
                        self.onUnload();
                        utils.backToMovieHome(myPubObj);
                    }
                    
                    telstra.sua.core.controllers.popupOkInfoController({
                        headerText : "Error",
                        innerText : "You may be experiencing a technical issue."+
                        "<br/>Please contact Telstra on" +
                        "<br/>1800 502 502 for assistance and quote reference number "+ reference + ".<br/>"+
                        (receiptNo != null ? "Your receipt number is: " +  receiptNo +"." : "" ),
                        callback: callback
                    });
                    accedo.console.log("reportError done");
                },

                logPlayBackError: function(errorStr){
                    telstra.api.manager.deviceLogger.log({
                        component:"VideoPlayback",
                        logMessage:
                        "PlaybackFailed|Error:"+errorStr
                        +"|playerURL:"+currentVideoURLString
                        +"|mediaTitle:"+((accedo.utils.object.isUndefined(videoItem) ||videoItem == null)?"NoTitle":videoItem.title)
                        +"|mediaId:" + ((accedo.utils.object.isUndefined(videoItem) ||videoItem == null)?"NoId":videoItem.mediaitemid),
                        accountid: telstra.api.manager.movies.accountId,
                        email: telstra.api.manager.movies.email
                    });
                },

                currentTimeCallback: function(time,formattedTime,percentage,skipping){
                    //accedo.console.log("currentTimeCallbacks: time - : " + time + " formattedTime - : " + formattedTime + " percentage - : " + percentage);

					if (skipping){
						//accedo.console.log("skipping: " + skipping);
						// do not show update time if it is for skipping
						//return;
					}
                    curTimeSec = time;
                    //put in additional durationSec check as on 'playing' status change the duration returns 0
                    if(!durationSec || durationSec == null || durationSec == 0){
                        durationSec = media.getTotalTime();
                        accedo.console.log("durationSec " + durationSec);
                    }
                    
                    curTime.setText(formattedTime.split(" / ")[0]);
                    var durationText = formattedTime.split(" / ")[1];
                    if(typeof durationText != "undefined"){
                        duration.setText(durationText);
                    }else{
                        duration.setText("");
                    }
                    
                    //accedo.console.log("updateBar with " + percentage);
                    self.updateBar(percentage);
					return;		//JUERGEN: Why is there a return?

                    //update meter bar
                    var bitrates = media.getBitrates();
                    
                    if(bitrates != false && previousStatus == "playing"){
                        var maxBitRate = bitrates.availableBitrates[bitrates.availableBitrates.length-1];
						alert("maxBitRate: " + maxBitRate);

                        /*if(bitrates.availableBitrates.length > 0){
                            var numBars = Math.floor(((bitrates.currentBitrate / maxBitRate)*100) / 20);
                            meterBars.getRoot().setStyle({
                                backgroundPosition : ('0px -' + ((numBars >= 5 ? 5 : numBars) * 45) + 'px')
                            });
                        }*/

                        /*if(slowPopupShown == false && bitrates.currentBitrate < 1.2 * 1000 * 1000 && isExiting == false && !accedo.app.getCurrentController().isPopup){
                            accedo.console.log("bitrates.currentBitrat: " + bitrates.currentBitrate + " maxBitRate: " + maxBitRate);
                            slowPopupRef = telstra.sua.core.controllers.popupOkInfoController({
                                headerText : "Network Speed Slow",
                                innerText : "Your connection is below the recommended speed.<br/><br/>"+
                                "You will have difficulty browsing and watching movies due to either:<br/>"+
                                "1. Your internet plan speed<br/>" +
                                "2. Other household internet activity<br/>" +
                                "3. External network congestions<br/><br/>"+
                                "To upgrade your internet connection contact BigPond on 1800 997834",
                                callback : function(){
                                    slowPopupRef = null;
                                }
                            });
                            slowPopupShown = true;
                        }*/
                    }
                },

                eventCallback: function(evt, param){
                	accedo.console.log("||||| eventCallback: " + evt);
                    
                    switch(evt){
                        case "onRenderingComplete":
                            curTime.setText("0:00:00");
                            duration.setText("0:00:00");
                            self.updateBar(0);
                            break;
                        case "onBufferingStart":
                        case "onBufferingProgress":
                            loading.turnOnLoading();
                            statusLabel.setText("Buffering");
                            break;
                        case "onBufferingComplete":
                            loading.turnOffLoading(true);
                            self.hideProgressbarCountDown();
                            statusLabel.setText("Playing");
                            progressBarPlayPauseButton.getRoot().removeClass("play");
                            progressBarPlayPauseButton.getRoot().addClass("pause");
                            break;
                        case "onAuthenticationFailed":
                            self.reportError("SMGV-05");
                            self.logPlayBackError("AuthenticationFailed");
                            break;
                        case "onConnectionFailed":
                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : "Your internet connection has been lost.",
                                colour : "red",
                                id : 'internetConnectionLost'
                            });
                            self.logPlayBackError("onConnectionFailed");
                            break;
                        case "onNetworkDisconnected":
                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : "Your internet connection has been lost.",
                                colour : "red",
                                id : 'internetConnectionLost'
                            });
                            self.logPlayBackError("onNetworkDisconnected");
                            break;
                        case "onStreamNotFound":
                            self.reportError("SMGV-09");
                            self.logPlayBackError("onStreamNotFound");
                            break;
                        case "onRenderError":
                            self.reportError("SMGV-00");
                            self.logPlayBackError("onRenderError " + param);
                            break;
                        case "videoError":
                            self.reportError("SMGV-05");
                            self.logPlayBackError("videoError " + param);
                            break;
                        case "onCustomEvent":
                            self.reportError("SMGV-05");
                            self.logPlayBackError("onCustomEvent " + param);
                            break;
                    }
                },

                statusChangeCallback: function(status, isFF){
                    accedo.console.log("........ statusChangeCallback: " + status);

                    if(previousStatus == "connecting"){
                        loading.turnOffLoading();
                    }

                    switch(status){
                        case "stopped":
                            //slowPopupShown = false;
                            statusLabel.setText("Stopped");
                            accedo.focus.manager.requestFocus(progressBarStopButton);
                            accedo.console.log("curTimeSec: " + curTimeSec + " durationSec: " + durationSec);
                            telstra.api.usage.log("playback", "stop", {
                                events: "&events=event2&event47=" + curTimeSec
                            });

                            //if it is exiting, playback done is called
                            if(!isExiting && previousStatus != "error"){
                                var curTime = curTimeSec;// - 4;		//Juergen: why take off 4 sec? it was even 4000ms before
                                if(curTime < 0){
                                    curTime = 0;
                                }
                    
                                if(curTimeSec > durationSec - 4){		//Juergen: whats this check for? it was 4000ms even
                                    self.playbackDone(null); //regard it as playback complete
                                }else{
                                    self.playbackDone(curTime); //save the currentime
                                }
                            }
                            break;
                        case "finished":
                            statusLabel.setText("Finished");
                            break;
                        case "playing":
                            loading.turnOffLoading();
                            if (isFF != null){
                            	// during skipping
                            	return;
                            }
                            progressBarPlayPauseButton.getRoot().removeClass("play");
                            progressBarPlayPauseButton.getRoot().addClass("pause");
                            if (videoSeeking){
                            	videoSeeking = false;
                            	videoFF = false;
                    			videoBW = false;
                            }
                            self.showProgressBar();
                            self.hideProgressbarCountDown();
                            statusLabel.setText("Playing");
                            accedo.focus.manager.requestFocus(progressBarPlayPauseButton); 
                            if(!durationSec || durationSec == null){
                                durationSec = media.getTotalTime();
                                accedo.console.log("durationSec "+ durationSec);
                            }
                            videoPlayed = true;
                            break;
                        case "paused":
                            loading.turnOffLoading();
                            if (videoSeeking){
                            	videoSeeking = false;
                            	videoFF = false;
                    			videoBW = false;
                            }
                            self.showProgressBar();
                            self.cancelHideProgressbarCountDown();
                            statusLabel.setText("Paused");
                            progressBarPlayPauseButton.getRoot().addClass("play");
                            progressBarPlayPauseButton.getRoot().removeClass("pause");
                            break;
                        case "skipping":
                        	loading.turnOffLoading();
                        	progressBarPlayPauseButton.getRoot().removeClass("pause");
                    		progressBarPlayPauseButton.getRoot().addClass("play");
                        	if (!videoSeeking){
                            	videoSeeking = true;
                            	videoFF = isFF;
                    			videoBW = !isFF;
                            }
                        	self.showProgressBar();
                            statusLabel.setText("Seeking");
                            self.cancelHideProgressbarCountDown();
                            break;
                        case "speed":
                            self.showProgressBar();
                            var currentSpeed = media.getPlaybackSpeed();
                            statusLabel.setText(currentSpeed + "x");
                            break;
                        case "buffering":
                            self.showProgressBar();
                            statusLabel.setText("Buffering");
                            videoPlayed = true;
                            break;
                        case "connecting":
                            statusLabel.setText("Connecting");
                            loading.turnOnLoading();
                            break;
                        case "error":
                            //slowPopupShown = false;
                            statusLabel.setText("Error");
                            accedo.console.log("status: " + status + " - handling");
                            self.popupError = telstra.sua.core.error.show(telstra.sua.core.error.CODE.GLOBAL_VIDEO_FAILED);
                            loading.turnOffLoading();
                            break;
                        default:
                            accedo.console.log("status: " + status + " - not handled");
                            break;
                    }

                    previousStatus = status;
                },

                playVideo: function(time){
                	media.setFullscreen(0, 0);
                	
                    if(self.connectionSpeedTesting){
                        self.connectionSpeedTesting = false;
                        self.setMediaUrl();
                        if(!self.isTrailer){
                            if(!time || time == null){
                                time = videoItem.resumeTime;
                            }
                        }
                    }

                    if(utils.isExpired(videoItem) && time != null){
                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : "You cannot resume back as the content is already expired",
                            colour : "red",
                            id : 'contentExpired'
                        });
                        return;
                    }
                    
                    telstra.api.usage.log("playback", "start", {
                        logMessage: "bigpond movies:vod:movie",
                        events: "&events=event1",
                        contentID: "movie:" + videoItem.title
                    });
                    
                    if (videoSeeking){
                    	videoSeeking = false;
                    	videoFF = false;
            			videoBW = false;
                    }

                    //this.statusChangeCallback("connecting");
					
                    if(time && !self.isTrailer){
                        media.play({
                            sec : time
                        });
                    }
                    else if(videoJumping){
                    	media.play({
                            sec : curTimeSec
                        });
                        videoJumping = false;
                    }
                    else{
                        media.play();
                    }
                    self.showProgressBar();
                },

                replayVideo: function() {
                    self.resumeVideo(0);
                },
                
                resumeVideo: function(time) {
                	self.initVideoPlayer();
                    self.setMediaUrl();

                    accedo.utils.fn.delay(function() {
                        self.playVideo(time);
                    }, 1);
                },

                pauseVideo: function(){
                    if (videoSeeking){
                    	videoSeeking = false;
	                    videoFF = false;
	                    videoBW = false;
                    }   
                    media.pause();
                },

                stopVideo: function(){
                    if (videoSeeking){
                    	videoSeeking = false;
	                    videoFF = false;
	                    videoBW = false;
                    }
                    media.stop();
                },
                
                setPlayerTime: function(targetSec){
                	videoJumping = true;
                	curTimeSec = targetSec;
                	media.setPlayerTime(targetSec);
                },

                skipForward: function(){
                    videoSeeking = true;
                    videoFF = true;
                    videoBW = false;
                    self.showProgressBar();
                    statusLabel.setText("Seeking");
                    progressBarPlayPauseButton.getRoot().removeClass("pause");
                    progressBarPlayPauseButton.getRoot().addClass("play");
                    
                    media.skip({
	                    sec : 10,
	                    multipleFactor : 1.2,
	                    progressiveCap : 300,
	                    delaySec : 0.5,
	                    progressive : true
	                });
                    /*accedo.utils.fn.delay(function() {
                        media.skip(60);
                    }, 1);*/
                },

                skipBackward: function(){
                    videoSeeking = true;
                    videoFF = false;
                    videoBW = true;
                    self.showProgressBar();
                    statusLabel.setText("Seeking");
                    progressBarPlayPauseButton.getRoot().removeClass("pause");
                    progressBarPlayPauseButton.getRoot().addClass("play");
                    
                    media.skip({
                        sec : -10,
                        multipleFactor : 1.2,
                        progressiveCap : 300,
                        delaySec : 0.5,
                        progressive : true
                    });
                },
                
                /*speedUp: function(){
                    var currentSpeed = media.getPlaybackSpeed();
                    if(currentSpeed * 2 <= 16){
                        if(currentSpeed > 0){
                            currentSpeed = currentSpeed *2;
                        }else if(currentSpeed < -2){
                            currentSpeed = currentSpeed /2;
                        }else if(currentSpeed == -2){
                            currentSpeed = 1;
                        }
                        media.speed(currentSpeed);
                    }
                },

                speedDown: function(){
                    var currentSpeed = media.getPlaybackSpeed();
                    if(currentSpeed * 2 >= -16){
                        if(currentSpeed < 0){
                            currentSpeed = currentSpeed *2;
                        }else if(currentSpeed > 1){
                            currentSpeed = currentSpeed /2;
                        }else if(currentSpeed == 1){
                            currentSpeed = -2;
                        }
                        media.speed(currentSpeed);
                    }
                },*/

                onUnload: function(){
                    clearTimeout(metered_rotate);
                    if(slowPopupRef && slowPopupRef != null){
                        slowPopupRef.close();
                        slowPopupRef = null;
                    }
                    media.unregisterStatusChangeCallback();
                    media.unregisterCurrentTimeCallback();
                    media.unregisterEventCallback();
                    media.deinit();
                },
                
                onExit: function(callback){
                    var videoPlayedBeforeOnExit = false;
                    if(videoPlayed){
                        videoPlayedBeforeOnExit = true;
                        self.pauseVideo();
                    }
                    callback(
                        function(){
                            if(videoPlayedBeforeOnExit){
                                self.playVideo();
                            }
                        },
                        function(){
                            isExiting = true;
                            if(!videoPlayed){
                                return;
                            }
                            if(curTimeSec > durationSec - 4){
                                self.playbackDone(null,function(){}); //regard it as playback complete
                            }else{
                                self.playbackDone(curTimeSec, function(){}); //save the currentime
                            }
                            self.stopVideo();
                        });
                },

                onKeyPlay: function(){
                    if (!videoPaused && !videoSeeking){
                    	return;
                    }
                    self.playVideo();
                    videoPaused = false;
                    accedo.focus.manager.requestFocus(progressBarPlayPauseButton);
                },
				
				onKeyPause: function(){
                    this.showProgressBar();
                    accedo.focus.manager.requestFocus(progressBarPlayPauseButton);
                    videoPaused = true;
                    self.pauseVideo();
                },

                onKeyStop: function(){
                    if(previousStatus != "stopped" && previousStatus != "error") {
                        accedo.focus.manager.requestFocus(progressBarStopButton);
                        this.stopVideo();
                    }
                },

                onKeyFF: function(){
                    this.skipForward();
                },

                onKeyRW: function(){
                    this.skipBackward();
                },
                
                /*onKeyFF : function(){
                    self.speedUp();
                },

                onKeyRW : function(){
                    self.speedDown();
                },*/

                onKeyBlue: function(){
                    accedo.console.log('onKeyBlue');
                    isExiting = true;
                    self.stopVideo();
                    var callback = function(){
                        self.onUnload();
                        utils.backToUAHome(myPubObj);
                    }
                    if(curTimeSec > durationSec - 4){
                        self.playbackDone(null, callback); //regard it as playback complete
                    }else{
                        self.playbackDone(curTimeSec, callback); //save the currentime
                    }
                },
                
                onKeyHome: function(){
                	accedo.console.log('onKeyHome');
                    isExiting = true;
                    self.stopVideo();
                    var callback = function(){
                        self.onUnload();
                        telstra.sua.core.main.moveBackToMyApps();
                    }
                    if(curTimeSec > durationSec - 4){
                        self.playbackDone(null, callback); //regard it as playback complete
                    }else{
                        self.playbackDone(curTimeSec, callback); //save the currentime
                    }
                },

                onKeyBack: function(){
                	accedo.console.log('onKeyBack; previousStatus: ' + previousStatus);
                    
                    if (self.isTrailer){
                    	isExiting = true;
                    	self.playbackDone(); //stop and go back
                    }
                    else if(previousStatus != "stopped" && previousStatus != "error"){
                        self.stopVideo();
                    }else{
                        self.onUnload();
                        utils.backToMovieHome(myPubObj); //it would only happens when something unexpected happens. (because there will always be a soft exit popup)
                    }
                },
                
                showProgressBarCall: function(){
                	this.showProgressBar();
                    if(previousStatus == "playing"){
                        this.hideProgressbarCountDown();
                    }
                }
            });
            return myPubObj;
        }
    });