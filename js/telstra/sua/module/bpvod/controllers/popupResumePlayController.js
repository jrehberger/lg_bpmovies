/**
 * @fileOverview popupResumePlayController
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
 */

accedo.define("telstra.sua.module.bpvod.controllers.popupResumePlayController", [
    'telstra.sua.module.bpvod.views.popupResumePlayView',
    "telstra.sua.module.bpvod.controllers.popupMovieHasPinCheckController",
    "accedo.utils.object",
    "telstra.sua.module.bpvod.utils",
    "telstra.sua.module.bpvod.config",
    "telstra.api.manager",
    "accedo.data.ds"],
    function() {

        /**
     * @class
     * @extends accedo.ui.popup
     */
        return function(opts) {

            if(!opts.videoItem || !opts.parentController){
                accedo.console.info("resumePlay popup missing paramter")
                return;
            }

            var config  = telstra.sua.module.bpvod.config,
            utils       = telstra.sua.module.bpvod.utils,
            api         = telstra.api.manager,
            myPubObj    = null,
            videoItem   = opts.videoItem,
            parentController = opts.parentController,
            skipHistory = opts.skipHistory || false,
            textToPut = "",
            buttonPanel = null,
            buttonArray = [],
            self,

            displayHelper = function(component, data) {
                component.setText(data.text);
                component.getRoot().addClass(data.css);

                //click event
                component.addEventListener('click', function() {
                    accedo.console.info('You just clicked on ' + data.text);
                    data.action();
                });
            };


            /**
             * Set-up inheritance.
             * @scope telstra.sua.module.bpvod.components.popupConnectionInfo
             */
            myPubObj = accedo.utils.object.extend(accedo.ui.popup(opts), {

                init : function(serverTime){

                    self = this;

                    var moveToVideoPage = function(resume){
                        accedo.console.log("[step 7]: in moveToVideoPage; go to videoPageController");
                        accedo.console.log("RESUME TIME::: " + videoItem.resumeTime);
                        utils.setDispatchController(parentController, 'videoPageController', {
                            videoItem : videoItem,
                            resumeTime : (resume?videoItem.resumeTime:1)
                        }, skipHistory);
                    }

                    var checkRestriction = function(resume){
                        accedo.console.log("[step 6]: in checkRestriction");
                        if(utils.isRestricted(videoItem)){
                            telstra.sua.module.bpvod.controllers.popupMovieHasPinCheckController({
                                videoItem : videoItem,
                                parentController : parentController,
                                historyItem : opts.historyItem,
                                callback : function(){
                                    moveToVideoPage(resume);
                                }
                            });
                            
                        }else{
                            moveToVideoPage(resume);
                        }

                    }

                    this.resume = function(){
                        //@todo: nav help for popup?
                        self.close();
                        checkRestriction(true);
                        
                    };

                    this.replay = function(){
                        //@todo: nav help for popup?
                        self.close();
                        checkRestriction(false);
                        
                    };

                    this.cancel = function(){
                        self.close();
                        if(skipHistory){
                            parentController.dispatchEvent('telstra:core:historyBack');
                        }
                    };

                    if(!accedo.utils.object.isUndefined(videoItem.resumeTime) && videoItem.resumeTime != null){

                        accedo.console.log("[step 5]: in popupResumePlayController... resume from " + videoItem.resumeTime);
                        if(!utils.isFree(videoItem)){
                            textToPut = "Rental Period Remaining:<br/>" +
                            utils.roundTime( (videoItem.expiryDate - serverTime) )+
                            "<br/><br/>Rental Started:<br/>"  +
                            utils.roundTime( (serverTime - videoItem.startDate) ) + " ago" ;
                        }

                        if (videoItem.resumeTime != 0) {
                            buttonArray.push({
                                id:       'resumeButton',
                                css:      'resumeButton play button softExitButton',
                                text:     'Resume',
                                action :  self.resume
                            });
                        }

                        buttonArray.push({
                            id:       'replayButton',
                            css:      'replayButton button softExitButton fromStart',
                            text:     'Play from start',
                            action :  self.replay
                        });

                    }else{
                        
                        var now = serverTime;
                        var remainTime = videoItem.expiryDate - now;//604807000
                        var dateDisplay = utils.roundTime(remainTime);
                        var watchNowDisplay = dateDisplay;

                        if (remainTime > videoItem.product.viewingperiod * 3600000 )//||movieList[hpos].resumeTime==null)
                        {
                            watchNowDisplay = videoItem.product.viewingperiod;
                            watchNowDisplay += "hour" + ( watchNowDisplay > 1?"s":"");
                        }
                        if(!utils.isFree(videoItem)){
                            textToPut = "Watch Now:<br/>" +
                            watchNowDisplay +// "hour" + ( watchNowDisplay > 1?"s":"") +
                            "<br/><br/>Watch Later:<br/>"  + dateDisplay
                        }

                        buttonArray.push({
                            id:       'watchNowButton',
                            css:      'watchNowButton play button softExitButton',
                            text:     'Watch Now',
                            action :  self.replay
                        });
                    }

                    buttonArray.push({
                        id:       'closeButton',
                        css:      'closeButton button softExitButton',
                        text:     'Close',
                        action:   self.cancel
                    });

                    this.setView(telstra.sua.module.bpvod.views.popupResumePlayView);
                    this.get('popupNormalSoftExit').setVisible(false);
                    accedo.utils.fn.delay(function(){
                        self.get('popupNormalSoftExit').setVisible(true);
                    }, 0.4);

                    buttonPanel = this.get('buttonPanel');

                    var ds = accedo.data.ds();
                    buttonPanel.setDisplayHelper(displayHelper);
                    buttonPanel.setDatasource(ds);
                    ds.appendData(buttonArray);

                    this.get('textFieldLabel').setText(videoItem.shortsynopsis);
                    this.get('rentDetailsLabel').setText(textToPut);
                    this.get('popupTitle').setText(videoItem.title);
                    if(telstra.sua.module.bpvod.utils.isEpisode(videoItem)){
                        this.get('videoItemImage').getRoot().addClass("tvItem");
                        this.get('videoItemImage').setSrc(telstra.api.manager.resizer(videoItem.coverimage,113,77, true));
                        this.get('episodeLabel').setText("Season " + videoItem.seasonnumber + "<br/>Episode " +  videoItem.episodenumber);
                    }else{
                        this.get('videoItemImage').setSrc(telstra.api.manager.resizer(videoItem.coverimage,113,160, true));
                    }
                    

                    accedo.utils.fn.defer(
                        accedo.utils.fn.bind(accedo.focus.manager.requestFocus,  accedo.focus.manager),
                        buttonPanel
                        );
                },

                onKeyBack : function(){
                    //self.close();
                    self.cancel();
                }

            });

            api.time.getTimeNow(accedo.utils.fn.bind(myPubObj.init, myPubObj) );

            return myPubObj;

        };

    });