/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 *
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.joinNowStep3NameController", [
    // dependency
    "telstra.api.usage",
    "telstra.sua.module.bpvod.views.joinNowStep3NameView",
    "telstra.sua.module.bpvod.controllers.joinNowTemplateController"],
    function(){

        return function(opts){

            // Shorten for namespacing
            var utils = telstra.sua.module.bpvod.utils,
            self, inputField1, inputField2;

            var myPubObj = accedo.utils.object.extend(telstra.sua.module.bpvod.controllers.joinNowTemplateController(opts), {
    
                onCreate : function(){
                    telstra.api.usage.log("bpvod", "join_now_step_3", {
                        logMessage: "bigpond movies:join:name"
                    });

                    self = this;

                    this.init(telstra.sua.module.bpvod.views.joinNowStep3NameView);

                    inputField1 = self.get('inputField1');
                    inputField2 = self.get('inputField2');

                    if (this.joinNowInfo.firstName != null) {
                        inputField1.setString(this.joinNowInfo.firstName);
                    }

                    if (this.joinNowInfo.surName != null) {
                        inputField2.setString(this.joinNowInfo.surName);
                    }

                    accedo.focus.manager.requestFocus(inputField1);

                    inputField1.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(inputField2);
                    };
                    inputField2.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(self.continueButton);
                    };

                    this.continueButton.setOption("nextUp", 'inputField2');

                    this.continueButton.onFocus = function() {
                        inputField2.setOption('nextDown', 'continueButton');
                    };

                    this.backButton.setOption("nextUp", 'inputField2');

                    this.backButton.onFocus = function() {
                        inputField2.setOption('nextDown', 'backButton');
                    };
                },

                continueButtonAction : function() {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();

                    var input1 = inputField1.getCleanedResultStr();
                    var input2 = inputField2.getCleanedResultStr();

                    if (utils.checkUserName(input1,input2)) {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();
                        
                        self.joinNowInfo.firstName = input1;
                        self.joinNowInfo.surName = input2;

                        utils.setDispatchController(myPubObj, 'joinNowStep4ACardTypeController', self.param, true);
                    }
                    else {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                        accedo.focus.manager.requestFocus(inputField1);
                    }
                },

                backButtonAction : function() {
                    if (telstra.sua.core.components.loadingOverlay.isLoadingOn())
                        return;
                    
                    inputField2.setOption('nextDown', 'continueButton');
                    utils.setDispatchController(myPubObj, 'joinNowStep2PasswordController', self.param, true);
                }
            });

            return myPubObj;
        }
    });