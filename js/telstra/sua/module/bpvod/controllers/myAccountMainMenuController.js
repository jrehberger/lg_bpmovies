/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedo.tv">Victor Leung</a>
 */
accedo.define(
    'telstra.sua.module.bpvod.controllers.myAccountMainMenuController',
    ['accedo.device.manager',
    'accedo.utils.object',
    'accedo.utils.fn',
    'accedo.ui.controller',
    'accedo.focus.manager',
    'telstra.api.usage',
    'telstra.sua.module.bpvod.utils',
    'telstra.sua.module.bpvod.views.myAccountMainMenuView',
    'telstra.sua.core.controllers.popupHelpController'],
    function(){
        
        return function(opts){

            var utils = telstra.sua.module.bpvod.utils,
            views = telstra.sua.module.bpvod.views,
            optionsList = [],
            self, categoryPanel, upScrollerBtn, downScrollerBtn;

            return accedo.utils.object.extend(accedo.ui.controller(opts), {

                onCreate: function() {
                    telstra.api.usage.log("bpvod", "my_account_account_info", {
                        logMessage: "bigpond movies:my account:account info"
                    });

                    telstra.sua.core.main.mainNavBar.setMenuButton("HELP");
                    
                    self = this;

                    //Set the view
                    self.setView(views.myAccountMainMenuView);
                    self.get('pageStore').setVisible(false);
                    accedo.utils.fn.delay(function(){
                        self.get('pageStore').setVisible(true);
                    },0.1);

                    categoryPanel = self.get('categoryPanel');
                    upScrollerBtn = self.get('upScroller');
                    downScrollerBtn = self.get('downScroller');

                    if ('context' in opts) {
                        if ('selectedIndex' in opts.context) {
                            categoryPanel.select(opts.context.selectedIndex, 2);
                        }
                    }

                    //Init the list with necessary info
                    var ds = accedo.data.ds();

                    ds.appendData([
                    {
                        title: "View and edit " + accedo.device.manager.identification.getDeviceType() + " Name",
                        controller : "myAccountChangeTVNameController"
                    },{
                        title: "View and edit Email/Username",
                        controller : "myAccountChangeEmailController"
                    },{
                        title: "View and edit First name, Last name",
                        controller : "myAccountChangeNameController"
                    },{
                        title: "View and edit Credit Card Details",
                        controller : "myAccountUpdateCreditCardController"
                    },{
                        title: "View Purchase history",
                        controller : "myAccountViewPurchaseController"
                    },{
                        title: "Remove my Account Details",
                        controller : "myAccountRemoveAccountController"
                    },{
                        title: "Change Newsletter subscription",
                        controller : "myAccountChangeNewsSubscripController"
                    },{
                        title: "Redeem a voucher",
                        controller : "myAccountRedeemVoucherController"
                    },{
                        title: "Change PIN",
                        controller : "myAccountChangePinController"
                    },{
                        title: "Change PIN Settings",
                        controller : "myAccountChangePinSettingController"
                    },{
                        title: "Change Password",
                        controller : "myAccountChangePasswordController"
                    }
                    ]);
                
                    categoryPanel.setDisplayHelper(function(components, data) {
                        components.setText(data.title);

                        components.addEventListener('click', function() {
                            /*if (data.controller === 'myAccountUpdateCreditCardController' && !accedo.device.manager.identification.supportsSSL()) {
                            
                                //Display an info popup to the user if his device's firmware doesn't support SSL
                                telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 21349, 'Update Credit Card Details');

//                                telstra.sua.core.controllers.popupHelpController({
//                                    headerText:    'Update Credit Card Details',
//                                    innerText:     'To update your credit card details on screen, upgrade the software in your TV. ' +
//                                'Exit from the Smart Hub area then select Menu > Support > Software Upgrade> By Online.<br><br>' +
//                                'Or update your details on the web at: http://go.bigpond.com/tv/update'
//                                });
                                
                                //do not continue
                                return;
                                
                            } else if (data.title == "Redeem a voucher") {
                                telstra.api.usage.log("bpvod", "", {
                                    logMessage: "Account|Redeem Voucher"
                                });
                            } else if (data.title == "Change PIN") {
                                telstra.api.usage.log("bpvod", "", {
                                    logMessage: "Account|Change PIN"
                                });
                            }*/
                            self.dispatchEvent('telstra:core:loadController', {
                                moduleId: 'bpvod',
                                moduleController: data.controller,
                                fromContext: {
                                    selectedIndex: components.dsIndex
                                }
                            });
                        });
                    });

                    categoryPanel.setDatasource(ds);

                    categoryPanel.addEventListener('accedo:list:moved', function(evt) {
                        if (evt.reverse) {
                            upScrollerBtn.getRoot().addClass('focused');
                            setTimeout(function() {
                                upScrollerBtn.getRoot().removeClass('focused');
                            }, 200);
                        } else {
                            downScrollerBtn.getRoot().addClass('focused');
                            setTimeout(function() {
                                downScrollerBtn.getRoot().removeClass('focused');
                            }, 200);
                        }
                    });
                    upScrollerBtn.addEventListener('click',function(evt){
                        categoryPanel.moveSelection(true);
                    });
                    downScrollerBtn.addEventListener('click',function(evt){
                        categoryPanel.moveSelection();
                    });
                    
                    optionsList.push(
						{
							type: "red",
							title: "Help"
						}
					);
					//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);

                    accedo.focus.manager.requestFocus(categoryPanel);
                },

                onKeyRed : function() {
                    if (!telstra.sua.core.main.keyboardShown)
                    	telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 19939);
                },

                onKeyBack: function() {
                    //Exit this module
                    utils.backToMovieHome(this);
                }
            });
        }
    });