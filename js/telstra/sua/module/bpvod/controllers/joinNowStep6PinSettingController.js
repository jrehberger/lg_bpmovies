/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 *
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.joinNowStep6PinSettingController", [
    // dependency
    "telstra.api.usage",
    "telstra.sua.module.bpvod.views.joinNowStep6PinSettingView",
    "telstra.sua.module.bpvod.controllers.joinNowTemplateController"],
    function(){

        return function(opts){

            // Shorten for namespacing
            var utils = telstra.sua.module.bpvod.utils,
            self, option1, option2;

            var myPubObj = accedo.utils.object.extend(telstra.sua.module.bpvod.controllers.joinNowTemplateController(opts), {
    
                onCreate : function(){
                    telstra.api.usage.log("bpvod", "join_now_step_6", {
                        logMessage: "bigpond movies:join:pinstepb"
                    });

                    self = this;

                    this.init(telstra.sua.module.bpvod.views.joinNowStep6PinSettingView);

                    option1 = self.get('pinSettingYes');
                    option2 = self.get('pinSettingNo');

                    accedo.focus.manager.requestFocus(option1);

                    option1.addEventListener('click', function() {
                        option1.getRoot().addClass('select');
                        option2.getRoot().removeClass('select');
                        self.joinNowInfo.useAccountPin = true;
                        accedo.focus.manager.requestFocus(self.continueButton);
                    });

                    option2.addEventListener('click', function() {
                        option1.getRoot().removeClass('select');
                        option2.getRoot().addClass('select');
                        self.joinNowInfo.useAccountPin = false;
                        accedo.focus.manager.requestFocus(self.continueButton);
                    });

                    this.continueButton.setOption("nextUp", 'pinSettingNo');

                    this.continueButton.onFocus = function() {
                        option2.setOption('nextDown', 'continueButton');
                    };

                    this.backButton.setOption("nextUp", 'pinSettingNo');

                    this.backButton.onFocus = function() {
                        option2.setOption('nextDown', 'backButton');
                    };
                },

                continueButtonAction : function() {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();

                    if (self.joinNowInfo.useAccountPin != null) {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                        utils.setDispatchController(myPubObj, 'joinNowStep7DeviceNameController', self.param, true);
                    }
                    else {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : 'You must select a PIN setting.',
                            colour : 'red',
                            id : 'selectPinSetting'
                        });
                    }
                },

                backButtonAction : function() {
                    if (telstra.sua.core.components.loadingOverlay.isLoadingOn())
                        return;

                    self.joinNowInfo.useAccountPin = null;
                    option2.setOption('nextDown', 'continueButton');
                    utils.setDispatchController(myPubObj, 'joinNowStep5PinController', self.param, true);
                }
            });

            return myPubObj;
        }
    });