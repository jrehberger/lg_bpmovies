/**
 * @fileOverview signInController
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 *
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.signInController", [
    // dependency
    'accedo.app',
    "accedo.utils.object", "accedo.utils.array", "accedo.utils.fn",
    "accedo.ui.controller", "accedo.ui.popupOk", 'telstra.api.usage',
    'accedo.focus.manager',
    'accedo.device.manager',
    'telstra.sua.module.bpvod.views.signInView',
    'telstra.sua.module.bpvod.utils',
    'telstra.sua.core.controllers.popupOkInfoController'],
    function(){
        return function(opts) {
            // Logging if this view created
            accedo.console.log("Create bpvod.signInController instance");

            // Shorten for namespacing
            var utils = telstra.sua.module.bpvod.utils,
            optionsList = [],
            views = telstra.sua.module.bpvod.views;
            
            var myPubObj = accedo.utils.object.extend(accedo.ui.controller(opts), {

                onCreate: function() {
                    telstra.api.usage.log("bpvod", "my_account_sign_in", {
                        logMessage: "bigpond movies:my account:sign in"
                    });

                    telstra.sua.core.main.mainNavBar.setMenuButton("HELP");

                    //Set the view
                    this.setView(views.signInView);
                    this.setVisible(false);

                    accedo.utils.fn.delay(function(){
                        self.setVisible(true);
                    },0.3);

                    var self = this;
                    var loginTrial = 0;
                   
                    // email address does not validate now since infoMessageBar
                    // not implemented yet (infoMessageBar should moved to core
                    // module)
                    var emailaddress = this.get('emailaddress'),
                    passwordField = this.get('passwordInputField'),
                    closeButton = this.get('closeButton'),
                    saveButton = this.get('saveButton');
                    
                    //test - set predefined values
                    //emailaddress.setString("a1@z.com");
                    //passwordField.setString("111111");
                    //test

                    emailaddress.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(passwordField);
                    }

                    passwordField.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(saveButton);
                    }

                    closeButton.addEventListener('click',function(){
                        myPubObj.onKeyBack();
                    });

                    saveButton.addEventListener('click',function(){
                        var loginName = emailaddress.getCleanedResultStr(),
                        password = passwordField.getCleanedResultStr();
                        accedo.console.log("name:"+loginName +" password:"+password);

                        if (typeof(loginName) == "undefined" || loginName == null || loginName == "" || typeof(password) == "undefined" || password == null || password == "") {
                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : "Please type in both your email and password and then select Continue.",
                                colour : "red",
                                id : 'typeBothFields'
                            });
                            accedo.focus.manager.requestFocus(emailaddress);
                            return;
                        }

                        if (!utils.checkEmail(loginName)) {
                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : "Please enter an email address",
                                colour : "red",
                                id : 'enterEmail'
                            });
                            accedo.focus.manager.requestFocus(emailaddress);
                            return;
                        }

                        utils.signInAction(loginName, password, function(){
                            utils.checkFirstTimeLogin(function(firstTime){

                                var param = accedo.utils.object.clone(telstra.api.manager.movies.accountInfo);
                                param.purchaseItem = opts.context.purchaseItem || null;

                                if(firstTime){
                                    telstra.sua.module.bpvod.session.signedInButNotFinishedProcess = true;
                                    if (param.creditcard){
                                    	//credit card defined
                                    	utils.setDispatchController(myPubObj, 'signInStep4CardInfoController', param, true);
                                    }
                                    else{
                                    	param.loginCreditCardDetails = true;
                                    	utils.setDispatchController(myPubObj, 'joinNowStep4ACardTypeController', param, true);
                                    }
                                }
                                else{
                                    telstra.sua.module.bpvod.session.isSignedIn = true;
                                    utils.saveAutoSignIn();
                                    
                                    if(param.purchaseItem){
                                        utils.purchaseAction(param.purchaseItem, myPubObj);
                                    }
                                    else{
                                        //HO: who modified the backToMovieHome function
                                        var targetMainController = {
                                            moduleController : 'mainController',
                                            moduleId: 'bpvod'
                                        };
                                        if (telstra.sua.core.modulemanager.hasHistory(targetMainController)){
                                            myPubObj.dispatchEvent('telstra:core:historyBack', targetMainController);
                                        }
                                        else{
                                            myPubObj.dispatchEvent('telstra:core:clearTillLast');
                                            utils.setDispatchController(myPubObj, 'mainController', {}, true);
                                        }
                                    }
                                }
                            });
                        }, false, function(json){ //failure callback

                            if(loginTrial > 3){
                                telstra.sua.core.controllers.popupOkInfoController({
                                    headerText:  "Email address or password incorrect.",
                                    innerText : "If you are a member of our BigPond Movies "
                                    +"DVDs service you will need to create a separate "
                                    +"BigPond Movies account by signing up online at "
                                    +"http://www.bigpond.com/tv  <br/><br/>If you are a "
                                    +"BigPond Movies customer please contact us on 1800 "
                                    +"502 502 and we will help you to regain access to your account.",
                                    buttonText: 'OK'
                                });

                                loginTrial = 0;
                                emailaddress.setString('');
                                passwordField.setString('');
                                accedo.focus.manager.requestFocus(emailaddress);
                            }
                            else{
                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : "Your email address or password is incorrect. Please try again.",
                                    colour : "red",
                                    id : 'emailPasswordIncorrect'
                                });
                                loginTrial ++;
                                emailaddress.setString('');
                                passwordField.setString('');
                                accedo.focus.manager.requestFocus(emailaddress);
                            }
                        });
                    });
                    
                   optionsList.push(
						{
							type: "red",
							title: "Help"
						},
						{
							type: "green",
							title: "Cancel Sign In"
						},
						{
							type: "yellow",
							title: "BigPond Plans"
						}
					);
					//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);

                    accedo.utils.fn.defer(accedo.utils.fn.bind(accedo.focus.manager.requestFocus, accedo.focus.manager), emailaddress);
                },
                
                cancelAction : function(callback){
                    var myFocus = accedo.focus.manager.getCurrentFocus();
                    accedo.focus.manager.requestFocus(null);

                    var popupYesNo = accedo.ui.popupYesNo({
                        headline_css: 'header',
                        headline_txt: 'Cancel or Exit Sign In',
                        msg_css: 'innerLabel',
                        msg_txt: "Are you sure you want to cancel "
                        +"<br/>or exit Sign In?<br/><br/>"
                        +"If you cancel or exit Sign In, all<br/>"
                        +"information you have entered so<br/>"
                        +"far will be lost.",
                        yesBtn_css: 'yesButton block medium button',
                        yesBtn_txt: "Cancel",
                        yesBtn_width: "196px",
                        noBtn_css: 'noButton button medium',
                        noBtn_txt: "Don't Cancel",
                        noBtn_width: "196px"
                    });

                    popupYesNo.addEventListener('accedo:popupYesNo:onConfirm', function() {
                        if(callback){
                            callback();
                        }else{
                            utils.signOutAction(function(){
                                utils.setDispatchController(myPubObj, 'mainController', {}, true);
                            });
                        }
                    });

                    popupYesNo.addEventListener('accedo:popupYesNo:onClose', function() {
                        accedo.focus.manager.requestFocus(myFocus);
                    });
                },

                onKeyRed : function() {
                    if (!telstra.sua.core.main.keyboardShown)
                        telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 19922);
                },
                
                onKeyGreen: function() {
            		if (!telstra.sua.core.main.keyboardShown)
            			this.cancelAction();
                },
                
                onKeyYellow: function() {
                	telstra.sua.module.bpvod.controllers.popupBigPondPlanController();
                },
                
                onKeyBlue: function(){
                	if (telstra.sua.module.bpvod.session.signedInButNotFinishedProcess){
                		utils.signOutAction(function(){
                            utils.backToMovieHome(myPubObj);
                        });
                	}
                	else{
                		utils.backToMovieHome(myPubObj);
                	}
                },
                
                onKeyHome: function(){
                	if (telstra.sua.module.bpvod.session.signedInButNotFinishedProcess){
                		utils.signOutAction(function(){
                            telstra.sua.core.main.moveBackToMyApps();
                        });
                	}
                	else{
                		telstra.sua.core.main.moveBackToMyApps();
                	}
                },
                
                /**
                 *  Action need to be done when back button is pressed
                 */
                onKeyBack : function(){
                    if (opts && opts.context.changeEmail) {
                        this.dispatchEvent('telstra:core:clearTillLast');
                        utils.setDispatchController(myPubObj, 'mainController', {}, true);
                    }
                    else {
                    	this.cancelAction();
                    }
                }
            });

            return myPubObj;
        };
    });