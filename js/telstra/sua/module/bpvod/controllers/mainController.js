/**
 *   The first file to load when entering the BP Movies
 *   This will call the API to get the channels and show the channel selection list
 *   If 
 *   
 *   @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 *   @author <a href="mailto:roy.chan@accedobroadband.com">Roy Chan</a>
 *   
 *   @method telstra.sua.module.bpvod.controllers.mainController
 *   @param  (object) opts ,  options when "telstra:core:loadController" is dispatched
 *   @return (object) this
 *   
 */
accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.mainController", [
    // dependency
    'accedo.app',
    "accedo.utils.object", "accedo.utils.array", "accedo.utils.fn",
    "accedo.ui.controller", "accedo.ui.popupOk",  "accedo.ui.popupYesNo",
    "accedo.data.ds",
    'accedo.focus.manager',
    'accedo.focus.managerMouse',
    'accedo.device.manager',
    'telstra.api.usage',
    'telstra.api.manager',
    "telstra.sua.module.bpvod.main",
    'telstra.sua.module.bpvod.views.mainView',
    'telstra.sua.module.bpvod.utils',
    'telstra.sua.module.bpvod.session',
    "telstra.sua.module.bpvod.controllers.popupResumePlayController",
    "telstra.sua.module.bpvod.controllers.popupFreeMoviesTermsController",
    "telstra.sua.module.bpvod.controllers.popupOptionsController",
    "telstra.sua.module.bpvod.controllers.popupSpeedTestController",
    "telstra.sua.module.bpvod.controllers.popupBigPondPlanController",
    "telstra.sua.module.bpvod.controllers.popupClassificationController",
    "telstra.sua.core.components.loadingOverlay"
    ],
    function(){
        return function(opts) {

            // Shorten for namespacing
            var utils = telstra.sua.module.bpvod.utils,
            
            // Define init variables
            topLevelMenu, scrollerKeyLeft, scrollerKeyRight,
            displayHelper, myPubObj,
            session = telstra.sua.module.bpvod.session,
            currentComponent = null,
            infoPopupCounter = 0, infoPopupTimer = null,

            displayHelper = function(component, data) {
                var titleLabel = component.getById('bpvod-topLevelMenuButton-titleLabel'),
                imageContainer = component.getById('bpvod-topLevelMenuButton-imageContainer'),
                contentLabel = component.getById('bpvod-topLevelMenuButton-contentLabel'),
                containerOverflow = component.getById('bpvod-topLevelMenuButton-containerOverflow');
                
                containerOverflow.getRoot().addClass(data.template);
                titleLabel.setText(data.subchannelname || '');
                imageContainer.getRoot().setStyle({
                    'background': 'url('+data.subchannelimage + ') no-repeat center center transparent'
                });
                
                component.onSelect = function() {
                    currentComponent = component;
                    imageContainer.getRoot().setStyle({
                        'background': 'url('+data.subchannelimagebig + ') no-repeat center center transparent'
                    });
                    contentLabel.setText(data.contentlabel || '');
                    if(data.template == 'myrental'){
                        var rentalDetails = utils.countRentedItem();
                        contentLabel.root.addClass("wide");
                        contentLabel.setText('<span  style="line-height:150%;">View your current ' +
                            'rentals</span><br/>' + rentalDetails.rented + ' Ready to ' +
                            'watch<br/>' + rentalDetails.aboutToExpired + ' About to expire');
                    }else if(data.template == 'myaccount'){
                        contentLabel.root.addClass("wide");
                        contentLabel.setText('View your account information and<br/>purchase ' +
                            'history, or redeem a voucher.<br/><b>Available credit: ' +
                            (telstra.api.manager.movies.accountInfo.vouchercredit.indexOf("$0.00") != -1 ? telstra.api.manager.movies.accountInfo.vouchercredit : (telstra.api.manager.movies.accountInfo.vouchercredit.indexOf(" ") == -1 ? telstra.api.manager.movies.accountInfo.vouchercredit : telstra.api.manager.movies.accountInfo.vouchercredit.substr(0, telstra.api.manager.movies.accountInfo.vouchercredit.indexOf(" ")))) +
                            '</b>');
                    }
                };
                component.onUnselect = function() {
                    imageContainer.getRoot().setStyle({
                        'background': 'url('+data.subchannelimage + ') no-repeat center center transparent'
                    });
                    contentLabel.setText('');
                };
                component.addEventListener('click', function() {
                    topLevelMenu.purgeList();
                    
                    if (data.targetctrller == "movieDetailsController") {
                        utils.setDispatchController(myPubObj, data.targetctrller,
                        {
                            url: data.url,
                            subChannel: data.subchannelname,
                            currentCategoryName: data.currentCategoryName,
                            currentMediaType: data.mediatype,
                            selectedIndexCat: data.mediaindex,
                            selectedCatName: data.selectedCatName,
                            selected_ch_id: data.selected_ch_id,
                            shift: data.shift
                        }, false, {
                            selectedIndex: component.dsIndex
                        });
                    }
                    else {
                        utils.setDispatchController(myPubObj, data.targetctrller,
                        {
                            url: data.url,
                            subChannel: data.subchannelname,
                            currentCategoryName: data.currentCategoryName,
                            currentMediaType: data.mediatype,
                            selectedIndexCat: data.mediaindex,
                            selectedCatName: data.selectedCatName,
                            shift: data.shift
                        }, false, {
                            selectedIndex: component.dsIndex
                        });
                    }
                });
                component.parent.addEventListener('mouseover', function() {
                    accedo.focus.managerMouse.requestFocus(component.root);
                });
                component.parent.addEventListener('mouseout', function() {
                    accedo.focus.managerMouse.releaseFocus();
                });
                component.parent.addEventListener('click', function() {
                    topLevelMenu.purgeList();
                    if (data.targetctrller == "movieDetailsController") {
                        utils.setDispatchController(myPubObj, data.targetctrller,
                        {
                            url: data.url,
                            subChannel: data.subchannelname,
                            currentCategoryName: data.currentCategoryName,
                            currentMediaType: data.mediatype,
                            selectedIndexCat: data.mediaindex,
                            selectedCatName: data.selectedCatName,
                            selected_ch_id: data.selected_ch_id,
                            shift: data.shift
                        }, false, {
                            selectedIndex: component.dsIndex
                        });
                    }
                    else {
                        utils.setDispatchController(myPubObj, data.targetctrller,
                        {
                            url: data.url,
                            subChannel: data.subchannelname,
                            currentCategoryName: data.currentCategoryName,
                            currentMediaType: data.mediatype,
                            selectedIndexCat: data.mediaindex,
                            selectedCatName: data.selectedCatName,
                            shift: data.shift
                        }, false, {
                            selectedIndex: component.dsIndex
                        });
                    }
                });
            };
            
            myPubObj = accedo.utils.object.extend(accedo.ui.controller(opts), {
                onCreate: function() {
                    //Set the view
                    this.setView(telstra.sua.module.bpvod.views.mainView);

                    var self = this;
                    var autoSignInCallback = function() {
                        var onOffNet = self.get('onOffNetIndicator').getRoot();

                        //if (telstra.sua.core.main.onOffNet)
                            onOffNet.addClass("isOnNet");
                        //else
                            //onOffNet.addClass("isOffNet");

                        var ds = telstra.api.manager.movies.getChannel();
                        ds.load();

                        // Get the reference of topLevelMenu
                        topLevelMenu = self.get('topLevelMenu');
                        if ('context' in opts && 'selectedIndex' in opts.context) {
                            topLevelMenu.select(opts.context.selectedIndex);
                        }else{
                            topLevelMenu.select(0);
                        }
                        topLevelMenu.setDisplayHelper(displayHelper);
                        topLevelMenu.setDatasource(ds);

                        // Get the reference of scrollKeyLeft and scrollKeyRight
                        scrollerKeyLeft  = self.get('scrollerKeyLeft');
                        scrollerKeyRight = self.get('scrollerKeyRight');

                        // Bind scrollKeys to listen list event
                        topLevelMenu.addEventListener('accedo:list:moved',function(evt){
                            if (!evt.reverse){
                                scrollerKeyRight.focus();
                                setTimeout(function(){
                                    scrollerKeyRight.blur();
                                }, 200);
                            } else {
                                scrollerKeyLeft.focus();
                                setTimeout(function(){
                                    scrollerKeyLeft.blur();
                                }, 200);
                            }
                        });
                        
                        scrollerKeyRight.addEventListener('click',function(evt){
                            topLevelMenu.moveSelection();
                        });
                        
                        scrollerKeyLeft.addEventListener('click',function(evt){
                            topLevelMenu.moveSelection(true);
                        });

                        if (telstra.sua.module.bpvod.main.firstRun) {
                            telstra.sua.module.bpvod.main.init();

                            /*telstra.sua.core.components.infoMessageBar.addBar({
                                text : "Welcome to BigPond Movies. The minimum internet speed for this service is 3.5 Mbps (ADSL 2+ and Cable).<br/>For more information press Connection info <span class='icon'></span>",
                                colour : "greenBig",
                                id : 'minimumInternetSpeed'
                            });*/

                            telstra.sua.module.bpvod.main.firstRun = false;
                        }

                        //telstra.sua.core.main.isOnline && 
                        accedo.utils.fn.defer(function() {
                            telstra.api.usage.log("bpvod", "movies_carousel", {
                                logMessage: "bigpond movies:carousel"
                            });

                            accedo.focus.manager.requestFocus(topLevelMenu);
                        });
						
						
                        if (!telstra.sua.module.bpvod.session.isSignedIn){
							self.optionsList = [
								{
									type: "red",
									title: "Sign in or join"
								},
								{	
									type: "green",
									title: "Unmetered?"
								},
								{
									type: "yellow",
									title: "Connection info"
								},
								{
									type: "blue",
									title: "BigPond Plans"
								},
								{
									type: "home"
								}
							];
                        }else{
							self.optionsList = [
								{
									type: "red",
									title: "Sign out"
								},
								{
									type: "green",
									title: "Unmetered?"
								},
								{
									type: "yellow",
									title: "Connection info"
								},
								{
									type: "blue",
									title: "BigPond Plans"
								},
								{
									type: "home"
								}
							];
                            
                            utils.checkLastViewItem();
                            if(session.lastViewVideoItem && session.lastViewVideoItem != null){
                                if(!utils.isExpired(session.lastViewVideoItem)){
                                    telstra.sua.core.main.mainNavBar.setMenuButton("RESUME");
                                    
                                    var resumeMessage = "Resume: " + session.lastViewVideoItem.title + (session.lastViewVideoItem.yearprod ? " [" + session.lastViewVideoItem.yearprod + "]" : "");
                                    telstra.sua.core.components.infoMessageBar.addBar({
                                        text : resumeMessage,
                                        colour : "green",
                                        id : 'resumeMessage'
                                    });
                                    // change title of the green button to Resume
                                    self.optionsList[1].title = "Resume";
                                }else{
                                    session.lastViewVideoItem = null;
                                }
                            }
                        }
						
						//Set the nav bar options
						telstra.sua.core.main.mainNavBar.setMenuButtons(self.optionsList);
						
						//set the exit button
						telstra.sua.core.main.mainNavBar.setExitButton();
						
                    };

                    //Hide the spash screen - only useful for standalone BPVOD as this is its starting page
                    var BG_loader = document.getElementById('BG_loader');
                    BG_loader && (BG_loader.style.display = 'none');
                    BG_loader = null;

                    if (telstra.sua.module.bpvod.main.firstRun) {
                        telstra.sua.core.components.loadingOverlay.init();
                        telstra.sua.core.components.loadingOverlay.turnOnLoading();
                    }

                    utils.loadAutoSignIn(autoSignInCallback);
                },

                //Resume movie
                onKeyFF : function(){
                    if(session.isSignedIn && session.lastViewVideoItem && session.lastViewVideoItem != null){
                        if(utils.isExpired(session.lastViewVideoItem)){
                            session.lastViewVideoItem = null;
                        }else{
                            utils.purchaseAction(session.lastViewVideoItem, myPubObj, {
                                selectedIndex: currentComponent.dsIndex
                            }, "mainController");
                        }
                    }
                },
                
                //Show info of the app
                onKeyRW : function(){
                    var now = new Date();
                    if (infoPopupTimer == null){
                    	infoPopupTimer = now;
                    }
                    else{
                    	var diff = now - infoPopupTimer;
                    	if (diff > 2*1000){
                    		// current click was more than 2 seconds apart from the previous one => start count time again
                    		infoPopupCounter = 0;
                    		infoPopupTimer = null;
                    	}
                    	else{
                    		if (infoPopupCounter < 1){
		                    	infoPopupCounter++;
		                    	infoPopupTimer = now;
		                    }
		                    else{
		                    	infoPopupCounter = 0;
		                    	infoPopupTimer = null;
		                    	telstra.sua.core.main.showInfo();	
		                    }
                    	}
                    }   
                },
                
                onKeyRed: function() {
                	accedo.console.log('red');
                	if (!telstra.sua.module.bpvod.session.isSignedIn){
                		telstra.api.usage.log("overlay", "", {
	                        logMessage: "bigpond movies: options overlay logged out: sign in or join"
	                    });
	
	                    utils.setDispatchController(myPubObj, 'signInORjoinNowController', {}, false);
                	} else {
	                    telstra.api.usage.log("overlay", "", {
	                        logMessage: "bigpond movies: options overlay logged in: sign out"
	                    });
	
	                    var popupYesNo = accedo.ui.popupYesNo({
	                        headline_css: 'header',
	                        headline_txt: 'Sign out',
	                        msg_css: 'innerLabel',
	                        msg_txt: "Are you sure you want to sign out?<br/><br/>"+
	                        "If you sign out you will be required to sign in using "+
	                        "your BigPond Movies Downloads email address and "+
	                        "password to rent or watch movies and tv shows.",
	                        yesBtn_css: 'yesButton button medium',
	                        yesBtn_txt: 'Sign out',
	                        noBtn_css: 'noButton button medium',
	                        noBtn_txt: "Cancel"
	                    });
	
	                    popupYesNo.addEventListener('accedo:popupYesNo:onConfirm', function() {
	                        utils.signOutAction(function(){
	                            utils.setDispatchController(myPubObj, 'mainController', {}, true);
	                            accedo.utils.fn.delay(function(){
	                                telstra.sua.core.components.infoMessageBar.addBar({
	                                    text : "You have been signed out. Returning you to the Movies Store main menu.",
	                                    colour : "green",
	                                    id : 'signedOutMessage'
	                                });
	                            }, 1);
	                        });
	                    });
                	}
                },
                
                onKeyGreen: function() {
                	var greenBtnTitle = myPubObj.optionsList[1].title;
                	accedo.console.log('green - ' + greenBtnTitle);
                	
                	if (greenBtnTitle == "Resume"){
                		if(session.isSignedIn && session.lastViewVideoItem && session.lastViewVideoItem != null && currentComponent){
	                        utils.purchaseAction(session.lastViewVideoItem, myPubObj, {
                                selectedIndex: currentComponent.dsIndex
                            }, "mainController");
	                    }
                	}
                	else{
                		telstra.api.usage.log("overlay", "", {
	                        logMessage: "bigpond movies: options overlay logged " + (telstra.sua.module.bpvod.session.isSignedIn ? "in" : "out") + ": unmetered"
	                    });
	                    telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 18648, "Unmetered");
                	}	
                },
                
                onKeyYellow: function() {
                	accedo.console.log('yellow');
                	telstra.api.usage.log("overlay", "", {
                        logMessage: "bigpond movies: options overlay logged " + (telstra.sua.module.bpvod.session.isSignedIn ? "in" : "out") + ": connection info"
                    });

                    telstra.sua.module.bpvod.controllers.popupSpeedTestController();
                },
                
                onKeyBlue: function() {
                	accedo.console.log('blue');
                	telstra.api.usage.log("overlay", "", {
                        logMessage: "bigpond movies: options overlay logged " + (telstra.sua.module.bpvod.session.isSignedIn ? "in" : "out") + ": bigpond plans"
                    });

                    telstra.sua.module.bpvod.controllers.popupBigPondPlanController();
                },

                //Back
                onKeyBack: function() {
                    this.dispatchEvent('telstra:core:historyBack');
                }
            });

            return myPubObj;
        };
    });