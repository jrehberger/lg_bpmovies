/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    'telstra.sua.module.bpvod.controllers.myAccountViewPurchaseController',
    ['accedo.utils.object',
    'accedo.ui.controller',
    'accedo.focus.manager',
    'telstra.api.manager',
    'telstra.api.usage',
    'telstra.sua.core.components.loadingOverlay',
    'telstra.sua.module.bpvod.views.myAccountViewPurchaseView'],
    function(){

        return function(opts){

            var utils = telstra.sua.module.bpvod.utils,
            views = telstra.sua.module.bpvod.views,
            optionsList = [],
            self, purchaseHistory, startPos, labelPage, currentPage, totalPage, historyLinePanel0;
            var upScrollerBtn, downScrollerBtn;

            const pageSize=6;

            return accedo.utils.object.extend(accedo.ui.controller(opts), {

                onCreate: function() {
                    telstra.api.usage.log("bpvod", "my_account_purchase_history", {
                        logMessage: "bigpond movies:my account:purchase history"
                    });

                    telstra.sua.core.main.mainNavBar.setMenuButton("HELP");

                    self = this;
                    currentPage = 1;
                    startPos = 0;

                    //Set the view
                    self.setView(views.myAccountViewPurchaseView);
                    self.get('pageStore').setVisible(false);
                    accedo.utils.fn.delay(function(){
                        self.get('pageStore').setVisible(true);
                    },0.1);

                    upScrollerBtn = self.get('upScroller');
                    downScrollerBtn = self.get('downScroller');
                    labelPage = self.get('labelPage');
                    historyLinePanel0 = self.get('historyLinePanel0');
                    self.getPurchaseHistory();

                    historyLinePanel0.addEventListener('accedo:list:moved', function(evt) {
                        if (evt.reverse) {
                            upScrollerBtn.getRoot().addClass('focused');
                            setTimeout(function() {
                                upScrollerBtn.getRoot().removeClass('focused');
                            }, 200);

                            if (currentPage>1) {
                                currentPage--;
                                startPos-= 6;
                                self.displayResult(purchaseHistory);
                            }else
                            {
                                currentPage = totalPage;
                                startPos = (totalPage-1) * 6;
                                self.displayResult(purchaseHistory);
                            }
                        } else {
                            downScrollerBtn.getRoot().addClass('focused');
                            setTimeout(function() {
                                downScrollerBtn.getRoot().removeClass('focused');
                            }, 200);

                            if (totalPage> currentPage)
                            {
                                currentPage++;
                                startPos += 6;
                                self.displayResult(purchaseHistory);
                            }else
                            {
                                currentPage = 1;
                                startPos = 0;
                                self.displayResult(purchaseHistory);
                            }
                        }
                    });
                    upScrollerBtn.addEventListener('click',function(evt){
                        historyLinePanel0.moveSelection(true);
                    });
                    downScrollerBtn.addEventListener('click',function(evt){
                        historyLinePanel0.moveSelection();
                    });
                    
                    optionsList.push(
						{
							type: "red",
							title: "Help"
						}
					);
					//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);
					
                    accedo.focus.manager.requestFocus(historyLinePanel0);
                },
                
                getPurchaseHistory: function() {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();

                    telstra.api.manager.movies.getAccountPurchaseInfo({
                        currentPage:1,
                        pageSize: 0
                    }, {
                        onSuccess: function(data) {
                            purchaseHistory = data;
                            totalPage = Math.ceil(purchaseHistory.length / 6);

                            if (!accedo.utils.object.isUndefined(purchaseHistory.length) && purchaseHistory.length > 0) {
                                if (purchaseHistory.length > 6)
                                    labelPage.setText('Page: ' + currentPage + '/' + totalPage);
                                else
                                    labelPage.setText('Page: 1/1');

                                self.displayResult(purchaseHistory);
                            }
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();
                        }
                    });
                },

                displayResult: function(purchaseHistory) {
                    var currentPurchaseItem;

                    for (var i = startPos, j = 0; i < startPos + pageSize; i++, j++) {
                        currentPurchaseItem = purchaseHistory[i];

                        if (typeof currentPurchaseItem != "undefined") {
                            var refNo = currentPurchaseItem.purchaseid,
                            title = currentPurchaseItem.title,
                            deviceName = currentPurchaseItem.devicename;
                            date = currentPurchaseItem.date.substr(0,10),
                            price = currentPurchaseItem.price;
                        
                            self.get('historyLinePanel' + j).setText(refNo, title, deviceName, date, price);
                        }
                        else {
                            self.get('historyLinePanel' + j).setText("", "", "", "", "");
                        }
                    }

                    labelPage.setText('Page: ' + currentPage + '/' + totalPage);
                },

                onKeyRed : function() {
                    telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 19946);
                },

                onKeyBack: function() {
                    if (telstra.sua.core.components.loadingOverlay.isLoadingOn())
                        return;

                    self.dispatchEvent('telstra:core:historyBack');
                }
            });
        }
    });