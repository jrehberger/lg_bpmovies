/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    'telstra.sua.module.bpvod.controllers.myAccountPageController',
    ['accedo.utils.object',
    'accedo.utils.fn',
    'accedo.ui.controller',
    'accedo.ui.popupYesNo',
    'accedo.focus.manager',
    'telstra.api.manager',
    'telstra.api.usage',
    'telstra.sua.core.components.infoMessageBar',
    'telstra.sua.core.components.loadingOverlay',
    'telstra.sua.module.bpvod.utils',
    'telstra.sua.module.bpvod.views.myAccountPageView'],
    function(){
        
        return function(opts) {

            var utils = telstra.sua.module.bpvod.utils,
            views = telstra.sua.module.bpvod.views,
            optionsList = [],
            self, myAccountInput, signOutButton, okButton;

            return accedo.utils.object.extend(accedo.ui.controller(opts), {

                onCreate: function() {
                    telstra.sua.core.main.mainNavBar.setMenuButton("HELP");

                    self = this;
                    
                    //Set the view
                    self.setView(views.myAccountPageView);
                    self.get('pageStore').setVisible(false);
                    accedo.utils.fn.delay(function(){
                        self.get('pageStore').setVisible(true);
                        accedo.focus.manager.requestFocus(myAccountInput);
                    },0.1);

                    myAccountInput = self.get('myAccountInput');
                    signOutButton = self.get('signOutButton');
                    okButton = self.get('okButton');
                    
                    //test - predefined fields
                    //myAccountInput.setString('1111');
                    //test

                    myAccountInput.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(okButton);
                    }

                    signOutButton.addEventListener('click',function(){
                        self.onLogout();
                    });

                    signOutButton.onFocus = function() {
                        myAccountInput.setOption('nextDown', 'signOutButton');
                    };

                    okButton.addEventListener('click',function(){
                        self.executeMyAccount();
                    });

                    okButton.onFocus = function() {
                        myAccountInput.setOption('nextDown', 'okButton');
                    };
                    
                    optionsList.push(
						{
							type: "red",
							title: "Help"
						},
						{
							type: "green",
							title: "Forgotten PIN?"
						}
					);
					//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);
                },

                onLogout: function() {
                    var popupYesNo = accedo.ui.popupYesNo({
                        headline_css: 'header',
                        headline_txt: 'Sign out',
                        msg_css: 'innerLabel',
                        msg_txt: "Are you sure you want to sign out?<br/><br/>"
                        + "If you sign out you will be required to sign in using "
                        + "your BigPond Movies Downloads email address and "
                        + "password to rent or watch movies and tv shows.",
                        yesBtn_css: 'signoutButton button medium',
                        yesBtn_txt: 'Sign out',
                        yesBtn_width: '196px',
                        noBtn_css: 'cancelButton button medium',
                        noBtn_txt: 'Cancel',
                        noBtn_width: '196px'
                    });

                    popupYesNo.addEventListener('accedo:popupYesNo:onConfirm', function() {
                        self.logout();
                    });
                },

                executeMyAccount: function() {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();

                    var input = myAccountInput.getCleanedResultStr();

                    if (utils.checkPin(input)) {
                        self.dispatchEvent('telstra:core:loadController', {
                            moduleId: 'bpvod',
                            moduleController: 'myAccountMainMenuController'
                        });

                        telstra.sua.core.components.loadingOverlay.turnOffLoading();
                    }
                    else {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : "The PIN you entered is incorrect. Retry, or select 'Change PIN' to create a new PIN.",
                            colour : "red",
                            id : 'pinIncorrect'
                        });

                        myAccountInput.clear();
                        accedo.focus.manager.requestFocus(myAccountInput);
                    }
                },

                logout: function() {
                    utils.signOutAction(function(){
                        self.dispatchEvent('telstra:core:historyBack', {
                            moduleController : 'mainController',
                            moduleId: 'bpvod'
                        });
                        
                        accedo.utils.fn.delay(function(){
                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : "You have been signed out. Returning you to the Movies Store main menu.",
                                colour : "green",
                                id : 'signedOutMessage'
                            });
                        },1);
                    });
                },

                onKeyRed : function() {
                    telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 19938);
                },
                
                onKeyGreen : function(){
                    accedo.console.log("move to forgotPin Page");
                    utils.moveToForgotPin(self, opts.context);
                },

                onKeyBack: function() {
                    //Exit this module
                    this.dispatchEvent('telstra:core:historyBack');
                }
            })
        }
    });
