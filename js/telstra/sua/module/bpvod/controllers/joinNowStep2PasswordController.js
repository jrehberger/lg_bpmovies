/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 *
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.joinNowStep2PasswordController", [
    // dependency
    "telstra.api.usage",
    "telstra.sua.module.bpvod.views.joinNowStep2PasswordView",
    "telstra.sua.module.bpvod.controllers.joinNowTemplateController"],
    function(){

        return function(opts){

            // Shorten for namespacing
            var utils = telstra.sua.module.bpvod.utils,
            self, passwordField1, passwordField2;

            var myPubObj = accedo.utils.object.extend(telstra.sua.module.bpvod.controllers.joinNowTemplateController(opts), {
    
                onCreate : function(){
                    telstra.api.usage.log("bpvod", "join_now_step_2", {
                        logMessage: "bigpond movies:join:password"
                    });

                    self = this;

                    this.init(telstra.sua.module.bpvod.views.joinNowStep2PasswordView);

                    passwordField1 = this.get('passwordField1');
                    passwordField2 = this.get('passwordField2');

                    accedo.focus.manager.requestFocus(passwordField1);

                    passwordField1.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(passwordField2);
                    };
                    passwordField2.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(self.continueButton);
                    };
                    passwordField2.zeroLengthCallback = function() {
                        accedo.focus.manager.requestFocus(passwordField2);
                    };

                    this.continueButton.setOption("nextUp", 'passwordField2');

                    this.continueButton.onFocus = function() {
                        passwordField2.setOption('nextDown', 'continueButton');
                    };

                    this.backButton.setOption("nextUp", 'passwordField2');

                    this.backButton.onFocus = function() {
                        passwordField2.setOption('nextDown', 'backButton');
                    };
                },

                continueButtonAction : function() {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();

                    var input1 = passwordField1.getCleanedResultStr();
                    var input2 = passwordField2.getCleanedResultStr();

                    if (utils.checkAccountPassword(input1, input2)) {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                        self.joinNowInfo.pwd = input1;

                        utils.setDispatchController(myPubObj, 'joinNowStep3NameController', self.param, true);
                    }
                    else {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                        passwordField1.setString('');
                        passwordField2.setString('');
                        accedo.focus.manager.requestFocus(passwordField1);
                    }
                },

                backButtonAction : function() {
                    if (telstra.sua.core.components.loadingOverlay.isLoadingOn())
                        return;

                    passwordField2.setOption('nextDown', 'continueButton');
                    utils.setDispatchController(myPubObj, 'joinNowStep1EmailController', self.param, true);
                }
            });

            return myPubObj;
        }
    });