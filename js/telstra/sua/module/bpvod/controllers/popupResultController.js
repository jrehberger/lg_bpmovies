/**
 * @fileOverview popupResultController
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */

accedo.define("telstra.sua.module.bpvod.controllers.popupResultController",
    ['accedo.utils.object',
    'telstra.sua.module.bpvod.views.popupResultView'],
    function() {

        /**
         * @class
         * @extends accedo.ui.popup
         */
        return function(opts) {

            var myPubObj, self,
            headerText = opts.headerText || "Header",
            creditText = opts.creditText || "creditText",
            balanceText = opts.balanceText || "balanceText",
            buttonText = opts.buttonText || "OK",
            callback = opts.callback || function(){},
            popupMain = null,
            okButton = null;

            myPubObj = accedo.utils.object.extend(accedo.ui.popup(opts), {

                init : function(){
                    self = this;
                    //setView
                    self.setView(telstra.sua.module.bpvod.views.popupResultView);

                    popupMain = self.get('popupBg').getById('popupMain');
                    popupMain.getById('header').setText(headerText);
                    popupMain.getById('labelCredit').setText(creditText);
                    popupMain.getById('labelBalance').setText(balanceText);
                    okButton = popupMain.getById('buttonPanel').getById('okButton');
                    okButton.setText(buttonText);

                    okButton.addEventListener('click', function() {
                        self.close();
                        callback();
                    });
                    
                    accedo.focus.manager.requestFocus(okButton);
                },

                onKeyBack : function(){
                    self.close();
                    callback();
                }
            });

            myPubObj.init();
            return myPubObj;
        }
    }
    );