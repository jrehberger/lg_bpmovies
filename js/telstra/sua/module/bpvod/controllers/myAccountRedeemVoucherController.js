/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    'telstra.sua.module.bpvod.controllers.myAccountRedeemVoucherController',
    ['accedo.utils.object',
    'accedo.ui.controller',
    'accedo.focus.manager',
    'telstra.api.manager',
    'telstra.api.usage',
    'telstra.sua.module.bpvod.utils',
    'telstra.sua.core.components.infoMessageBar',
    'telstra.sua.core.components.loadingOverlay',
    'telstra.sua.core.controllers.popupOkInfoController',
    'telstra.sua.module.bpvod.controllers.popupResultController',
    'telstra.sua.module.bpvod.views.myAccountRedeemVoucherView'],
    function(){
        
        return function(opts){

            var utils = telstra.sua.module.bpvod.utils,
            views = telstra.sua.module.bpvod.views,
            optionsList = [],
            fromController = null,
            self, inputField, labelCurrentVoucher, saveButton, cancelButton;

            return accedo.utils.object.extend(accedo.ui.controller(opts), {

                onCreate: function() {
                    telstra.api.usage.log("bpvod", "my_account_redeem_voucher", {
                        logMessage: "bigpond movies:my account:redeem voucher"
                    });

                    self = this;
                    fromController = opts.context ? opts.context.from : ""; 
                    
                    //Set the view
                    self.setView(views.myAccountRedeemVoucherView);

                    inputField = self.get('inputField');
                    labelCurrentVoucher = self.get('labelCurrentVoucher');
                    saveButton = self.get('saveButton');
                    cancelButton = self.get('cancelButton');

                    if (telstra.api.manager.movies.accountInfo.vouchercredit.indexOf("$0.00") != -1)
                        labelCurrentVoucher.setText("Current voucher credit balance: "+ telstra.api.manager.movies.accountInfo.vouchercredit);
                    else {
                        if (telstra.api.manager.movies.accountInfo.vouchercredit.indexOf(" ") == -1)
                            labelCurrentVoucher.setText("Current voucher credit balance: "+ telstra.api.manager.movies.accountInfo.vouchercredit);
                        else
                            labelCurrentVoucher.setText("Current voucher credit balance: "+ telstra.api.manager.movies.accountInfo.vouchercredit.substr(0, telstra.api.manager.movies.accountInfo.vouchercredit.indexOf(" ")));
                    }

                    inputField.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(saveButton);
                    };

                    saveButton.addEventListener('click', function(){
                        self.saveAction();
                    });

                    saveButton.onFocus = function() {
                        inputField.setOption('nextDown', 'saveButton');
                    };

                    cancelButton.addEventListener('click',function() {
                        inputField.setOption('nextDown', 'saveButton');
                        self.dispatchEvent('telstra:core:historyBack');
                    });

                    cancelButton.onFocus = function() {
                        inputField.setOption('nextDown', 'cancelButton');
                    };

                    optionsList.push(
						{
							type: "red",
							title: "Help"
						}
					);
					if (fromController == "purchasePageController"){
						optionsList.push(
							{
								type: "yellow",
								title: "BigPond Plans"
							}
						);	
					}
					//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);

                    accedo.focus.manager.requestFocus(inputField);
                },

                saveAction: function() {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();

                    var input = inputField.getCleanedResultStr();

                    if(input.length == 14) {
                        if (utils.isCapLetterOrNumber(input)) {
                            telstra.api.manager.movies.registerVoucher(input, {
                                onSuccess:function(json) {
                                    telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                    telstra.sua.module.bpvod.controllers.popupResultController({
                                        headerText:  'Voucher credit added',
                                        creditText: "Your account has been credited with " + json.thisvouchercredit,
                                        balanceText: "Your voucher credit balance is: " +  json.vouchercredit,
                                        buttonText: 'OK - Close',
                                        callback: self.onKeyBack
                                    });

                                    accedo.utils.fn.delay(function() {
                                        telstra.api.manager.movies.accountInfo.vouchercredit = json.vouchercredit;
                                        inputField.setString("");
                                        labelCurrentVoucher.setText("Current voucher credit balance: "+ telstra.api.manager.movies.accountInfo.vouchercredit);
                                    }, 0.5);
                                },
                                onFailure:function(json) {
                                    telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                    if (json != null && json.errorcode == "VOUCHER_NOT_FOUND") {
                                        telstra.sua.core.components.infoMessageBar.addBar({
                                            text : 'The Voucher Code you entered was incorrect. Please try again.',
                                            colour : 'red',
                                            id : 'voucherIncorrect'
                                        });                                        
                                    }
                                    else if (json != null && json.errorcode == "VOUCHER_ALREADY_ASSIGNED" ) {
                                        telstra.sua.core.controllers.popupOkInfoController({
                                            headerText:  'The voucher code you entered has already been used.',
                                            innerText : 'If you believe that your voucher is still valid please contact Telstra on 1800 502 502 and quote reference number DVX-16.',
                                            buttonText: 'OK'
                                        });
                                    }
                                    else if (json != null) {
                                        telstra.sua.core.components.infoMessageBar.addBar({
                                            text : json.errordescription,
                                            colour : 'red',
                                            id : 'registerVoucherError'
                                        });
                                    }

                                    inputField.setString('');
                                    accedo.focus.manager.requestFocus(inputField);
                                },
                                onException:function() {
                                    telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                    inputField.setString('');
                                    accedo.focus.manager.requestFocus(inputField);

                                    telstra.sua.core.components.infoMessageBar.addBar({
                                        text : 'The Voucher Code you entered was incorrect. Please try again.',
                                        colour : 'red',
                                        id : 'voucherIncorrect'
                                    });
                                }
                            });
                        }
                        else {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();

                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : 'Voucher must contain capital letters and numbers only.',
                                colour : 'red',
                                id : 'invalidVoucherFormat'
                            });
                        }
                    }
                    else {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();
                        
                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : 'Voucher must be 14 characters long.',
                            colour : 'red',
                            id : 'invalidVoucherLength'
                        });
                    }
                },

                onKeyRed : function() {
                    if (!telstra.sua.core.main.keyboardShown)
                        telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 19945);
                },
                
                onKeyYellow: function() {
                	telstra.sua.module.bpvod.controllers.popupBigPondPlanController();
                },

                onKeyBack: function() {
                    if (telstra.sua.core.components.loadingOverlay.isLoadingOn() && telstra.sua.core.main.keyboardShown)
                        return;

                    inputField.setOption('nextDown', 'saveButton');
                    self.dispatchEvent('telstra:core:historyBack');
                }
            });
        }
    });