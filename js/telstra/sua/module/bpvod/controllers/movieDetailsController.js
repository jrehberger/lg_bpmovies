/**
 * @fileOverview movieDetailsController
 *               It handles the movie detail, related movies and the page following search resutls
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 * @author <a href="mailto:roy.chan@accedobroadband.com">Roy Chan</a>
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.movieDetailsController", [
    // dependency
    "accedo.utils.object", "accedo.utils.fn",
    "accedo.ui.controller", "accedo.ui.list",
    "accedo.data.ds",
    "accedo.focus.manager",
    "accedo.focus.managerMouse",
    "telstra.api.manager",
    "telstra.api.usage",
    'telstra.sua.module.bpvod.views.movieDetailsView',
    "telstra.sua.module.bpvod.utils",
    "telstra.sua.module.bpvod.main", 
    "telstra.sua.module.bpvod.session",
    "telstra.sua.module.bpvod.controllers.popupClassificationController",
    "telstra.sua.core.components.loadingOverlay",
    "telstra.sua.module.bpvod.controllers.mainController",
    "telstra.sua.core.error"],
    function(){

        return function(opts) {

            // Shorten namespace
            var utils = telstra.sua.module.bpvod.utils,
            isUndefined = accedo.utils.object.isUndefined,

            self, hlist, scrollerKeyLeft, scrollerKeyRight, category_ds,
            currentMediaType, displayHelperH, myPubObj, currentCategoryName, updateLeftPanelText,
            updateVideoPreviewTimeout = null,
            currentData, getHistoryItem, currentPage = 'detail', currentPageSpecial = null, currentPrice,
            forceListingMode = false, componentClickHandler,
            currentActor, currentDirector, relatedMoviesAvailable = true,
                
            videoPreviewBox, trailerImageContainer,
            optionsList = [],
                
            //left column items
            headerL, firstLine, secondLine, thirdLine, fourthLine, synopsis,
                
            textContainer, textContent, vScrollbar, scrollerContainer,
            targetShift, refreshNavBar,
                
            //left Panels
            leftPanelMovieInfo, leftPanelChoosePeople, vlist, displayHelperV, refresHListTimeout,
            headerLRelated, upScrollerBtn, downScrollerBtn, leftUpperPanel, upScrollerText, downScrollerText;
                
            /**
             * Defines the display of the button in Choose People List for related movies
             * It's only used if thei page is "related movies" instead of "detail"
             * @param {accedo.ui.component} component each display component
             * @param {Object} data representing one person
             */
            displayHelperV = function(component, data) {
                //data.text ususally is "Movies ... [actor name]"
                component.setText(data.text);
                component.onSelect = function() {
                    if (!accedo.utils.object.isUndefined(refresHListTimeout)){
                        clearTimeout(refresHListTimeout);
                        refresHListTimeout = null;
                    }

                    refresHListTimeout = setTimeout(function(){
                        category_ds = telstra.api.manager.movies.peopleDetails(data.url);
                        category_ds.load();
                        
                        //Init the list with necessary info
                        hlist.setDatasource(category_ds);

                        if (currentPage == "related") {
                            if (currentMediaType == "Television" && (currentPageSpecial != "search_title" && currentPageSpecial != "search_people")) {
                                telstra.api.usage.log("bpvod", "tv_shows_related", {
                                    logMessage: "bigpond movies:related",
                                    actor: data.text
                                });
                            }else if (currentPageSpecial != "search_title" && currentPageSpecial != "search_people") {
                                telstra.api.usage.log("bpvod", "movies_related", {
                                    logMessage: "bigpond movies:related",
                                    actor: data.text
                                });
                            }
                        }
                    }, 350);
                };
                component.parent.addEventListener('click', function(){
	            	var moveStep, moveDirection = true, 
                    currentIndex = component.dsIndex,
                    visibleCategories = vlist.getChildren();
                    if (visibleCategories.length <= 1){
                    	return;
                    }
                    moveStep = (visibleCategories.length == 2) ? -1 : -2;
                    for(var i = 0; i < visibleCategories.length; ++i){
                    	if (visibleCategories[i].dsIndex == currentIndex){
                    		moveStep += i;
                    		if (moveStep > 0){
                    			moveDirection = false;
                    		}
                    		break;
                    	}
                    }
                    if (moveStep != 0){
                    	vlist.moveSelection(moveDirection);
                    	if (Math.abs(moveStep) == 2){
                    		vlist.moveSelection(moveDirection);
                    	}
                    }
	           });
            };
            /**
             * Set display for the component in the Horizontal list
             * @param {accedo.ui.component} component The accedo ui component to assign
             * @param {Object} data An object representing a BPMovies title or episode
             */
            displayHelperH = function(component, data) {
                var videoItemImage = component.getById('bpvod_videoItemImage');
                
                if (!forceListingMode && currentMediaType == 'Television' && currentPage=='detail'){
                    videoItemImage.getRoot().addClass('television');
                    videoItemImage.setSrc(telstra.api.manager.resizer(data.coverimage, 112, 77, true));
                    component.getById('episodeLabel').setText('Season '+ data.seasonnumber + '<br/>Episode '+data.episodenumber);
                }else{
                    videoItemImage.setSrc(telstra.api.manager.resizer(data.coverimagemedium, 113, 160, true));
                }
                /**
                 * refresh the videoPreviewBox and left panel txt when video item is selected
                 */
                component.onSelect = function() {
                    if (updateVideoPreviewTimeout){
                        clearTimeout(updateVideoPreviewTimeout);
                        updateVideoPreviewTimeout = null;
                    }
                    updateVideoPreviewTimeout = setTimeout(function(){
                        videoPreviewBox.updateDisplay(data);
                        currentData = data;
                        trailerImageContainer = videoPreviewBox.getStillImageContainer();
	                    if (trailerImageContainer){
	                    	trailerImageContainer.addEventListener('click',function(evt){
		                    	utils.playTrailerAction(currentData, myPubObj, getHistoryItem());
		                	});
	                    }
                        
                        /**
                         * additional handling of Left panel text compared with listing controller
                         */
                        if (currentPage == 'detail'){
                            updateLeftPanelText(data);
                        }
                        refreshNavBar();
                        
                        if (currentPage == "detail"){
                            if (currentMediaType == "Television" && (currentPageSpecial != "search_title" && currentPageSpecial != "search_people")) {
                                telstra.api.usage.log("bpvod", "tv_shows_synopsis", {
                                    logMessage: "bigpond movies:synopsis",
                                    events: "&events=prodView",
                                    products: "&products=;" + data.title
                                });
                            }
                            else if (currentPageSpecial != "search_title" && currentPageSpecial != "search_people") {
                                telstra.api.usage.log("bpvod", "movies_synopsis", {
                                    logMessage: "bigpond movies:synopsis",
                                    events: "&events=prodView",
                                    products: "&products=;" + data.title
                                });
                            }
                        }
                    }, 500);
                    if (scrollerKeyRight && scrollerKeyLeft){
                    	if (hlist.getSize() <= hlist.getVisibleSize()){
	                    	scrollerKeyRight.root.addClass('mouse-block');
	                    	scrollerKeyLeft.root.addClass('mouse-block');
	                    }
	                    else{
	                    	scrollerKeyRight.root.removeClass('mouse-block');
	                    	scrollerKeyLeft.root.removeClass('mouse-block');
	                    }
                    }   
                };
                
                /**
                 * go to detail page for episode or buy item
                 */
                component.addEventListener('click', function() {
                    componentClickHandler(data);
                });
                
                component.parent.addEventListener('mouseover', function() {
                    accedo.focus.managerMouse.requestFocus(component.root);
                });
                component.parent.addEventListener('mouseout', function() {
                    accedo.focus.managerMouse.releaseFocus();
                });
                
                component.parent.addEventListener('click', function() {
                    //check if the selected item has been clicked
	            	var visibleItems = hlist.getChildren();
	            	if (visibleItems[hlist.getSelectedIndex()] == component){
	            		//click through
	            		componentClickHandler(data);
	            	}
	            	else{
	            		//change selection (first - define the index relatively to the visible items)
	            		for(var i = 0; i < visibleItems.length; ++i){
	            			if (visibleItems[i] == component){
	            				hlist.setSelectedIndex(i);
	            				break;		
	            			}
	            		}
	            	}
                });
            };
            /**
             * Update the text in left panel
             * @param {Object} data is the data representing the video item 
             */
            updateLeftPanelText = function(data){
                var titleL = data.title;
                if (data.yearprod){
                	titleL += ' [' + data.yearprod+']';
                }
                var duration = parseInt(data.duration),
                firstLineText,
                secondLineText =  '<b>'+ data.classification + '</b> ' + ( data.consumeradvice ? data.consumeradvice : ''),
                thirdLineText = accedo.utils.object.isUndefined(data.directors)? '': '<b>Director: </b> ' + data.directors.director.dirname,
                fourthLineText = '<b>With: </b>', s = '',
                actor, actors, i;
                
                firstLineText  = '<b>'+ ~~( duration/ 60) + 'h ' + ~~(duration % 60) + 'mins</b>';
                
                firstLineText = firstLineText + (data.country? ' ' + data.country : '') + (data.yearprod ? ' [' + data.yearprod +']' : '');
                actors      = (isUndefined(data.actors)||isUndefined(data.actors.actor)) ? [] : data.actors.actor;
                i=0;
                            
                // Concatenate actors
                if (!accedo.utils.object.isArray(actors)){
                    actors = [actors];
                }                    
                if (actors.length > 0){
	                for (;i<actors.length;i++) {
	                    s += (actors[i].actorname + ', ');
	                }
	                // Remove extra comma and space in the end of actors
	                fourthLineText += s.substring(0, s.length - 2);
	                fourthLine.setText(fourthLineText);
                }
                
                headerL.setText(titleL);
                firstLine.setText(firstLineText);
                secondLine.setText(secondLineText);
                thirdLine.setText(thirdLineText);

                if (data.longsynopsis)
                    synopsis.setText(data.longsynopsis);
                else
                    synopsis.setText(data.shortsynopsis);
            
                accedo.utils.fn.defer(function(){
                    telstra.sua.module.bpvod.utils.scrollText('reset', textContainer, textContent, vScrollbar);
                });
            };
            componentClickHandler = function(dataObj){
            	if ((currentPage == 'related' || forceListingMode) && currentMediaType == 'Television'){
                    //go to detail page of movies/television
                    utils.setDispatchController(myPubObj, 'movieDetailsController', {
                        selected_ch_id: dataObj.mediaitemid,
                        currentMediaType: currentMediaType,
                        currentCategoryName: currentCategoryName
                    }, false, getHistoryItem());
                }else{
                    dataObj.categoryname = currentCategoryName || "";
                    //purchase this movie
                    utils.purchaseAction(dataObj, myPubObj, getHistoryItem(), "movieDetailsController");
                }
            };
            /**
             * Get the current context for saving into history
             */
            getHistoryItem = function(){
                var historyItem = {};
                accedo.utils.object.extend(historyItem, opts.context);
                historyItem['shift'] = hlist.getSelection().dsIndex;
                historyItem['shiftList'] = hlist.getSelectedIndex();
                
                return historyItem;
            };
            /**
             * Refresh the navigation bar according to different conditions
             */
            refreshNavBar = function(){
                telstra.sua.core.main.mainNavBar.resetMenuButton();
                
                //grab the default button list for this screen
                var arr = accedo.utils.array.clone(optionsList);

                //If the movie is rented, the "rent movie" green button is not needed
                if (forceListingMode || (currentData && (currentData.rented || currentData.product.price == '$0.00'))) {}
                else {
                    if (currentPage == "related" && currentMediaType == 'Television'){
                    }
                    else{
                    	//telstra.sua.core.main.mainNavBar.setMenuButton("RENT TITLE");
                    	arr.push({
							type: "green",
							title: "Rent Title"
						});
                    }
                }
                
                if (currentPage == 'detail'){
                	currentActor = (isUndefined(currentData.actors)||isUndefined(currentData.actors.actor)) ? [] : currentData.actors.actor;
	            	currentDirector = (isUndefined(currentData.directors)||isUndefined(currentData.directors.director)) ? [] : currentData.directors.director;
	            	
	            	if (!accedo.utils.object.isArray(currentActor)){
	                	currentActor = [currentActor];
	                }
	                if (!accedo.utils.object.isArray(currentDirector)){
	                    currentDirector = [currentDirector];
	                }
                }
                
                if (currentPage == 'detail' && currentPageSpecial != 'search_title' && currentPageSpecial != 'search_people'){
	                if (currentActor.length == 0 && currentDirector.length == 0){
	                    accedo.console.log("cannot go to related search as there are no actors/directors");
	                    relatedMoviesAvailable = false;
	                }
	                else {
	                	relatedMoviesAvailable = true;
	                	if (currentMediaType != 'Television'){
		                	arr.push({
								type: "red",
								title: "Related Movies"
							});
						}
	                }
 				}
   
 				if (currentPageSpecial == 'search_title' || currentPageSpecial == 'search_people'){
        			if (opts.context.currentSearchTitle == 'Movies'){
        				arr.push({
							type: "red",
							title: "Related Movies"
						});
        			}
        			else if(opts.context.currentSearchTitle == 'TV Shows'){
        				arr.push({
							type: "green",
							title: "View Episodes"
						});
        			}
  				}
                
                //set nav bar buttons
                telstra.sua.core.main.mainNavBar.setMenuButtons(arr);
            };

            myPubObj = accedo.utils.object.extend(accedo.ui.controller(opts), {
                /**
                 * Handle action needed to take on creating controller
                 * @param context
                 */
                onCreate: function(context) {
                    //In order to ensure autosignIn is called through deeping linking from the moives
                    // list in home page, this loadautosignin is needed
                    self = this;
                    utils.loadAutoSignIn(function(){
                        self.onCreateAfterLogin(context);
                    });
                },
                /**
                 * After detecting auto-sign in, this will be run
                 */
                onCreateAfterLogin: function(context) {
                    var loadingOverlay = telstra.sua.core.components.loadingOverlay;
                    loadingOverlay.init();
                    loadingOverlay.turnOffLoading();

                    //Set the view
                    this.setView(telstra.sua.module.bpvod.views.movieDetailsView);
                    
                    //left column items
                    headerL = this.get('headerL');
                    firstLine = this.get('firstLine');
                    secondLine = this.get('secondLine');
                    thirdLine = this.get('thirdLine');
                    fourthLine = this.get('fourthLine');
                    synopsis = this.get('synopsis');
                    
                    //left column text panel scroll bar related items
                    textContainer = this.get('textContainer');
                    textContent = this.get('textContent');
                    vScrollbar = this.get('vScrollbar');
                    scrollerContainer = this.get('scrollerContainer');
                    //vScrollbar.hide();
                    scrollerContainer.hide();
                    
                    // Get listing on the movieDetails
                    hlist = this.get('movie_horizontal_listing');
                    
                    //left panels
                    leftPanelMovieInfo = this.get('leftPanelMovieInfo');
                    leftPanelChoosePeople = this.get('leftPanelChoosePeople');
                    
                    //default hide this and show movieInfo
                    leftPanelChoosePeople.hide();
                    hlist.setDisplayHelper(displayHelperH);
                    
                    /**
                     * Start parsing the context parameters
                     * opts.context must contain
                     *       1. currentMediaType: 'Television' or 'Movies'
                     *       2. related_ds (for related movies)
                     *          or selected_ch_id (for tv)
                     *          or url (for movies)
                     */
                    currentMediaType = opts.context.currentMediaType;
                    if (!currentMediaType){
                        currentMediaType = "Movies";
                    }
                    
                    if (opts.context.currentPageSpecial){
                        currentPageSpecial = opts.context.currentPageSpecial;
                    }
                    if (opts.context.currentPage){
                        currentPage = opts.context.currentPage;
                    }
                    if (opts.context.currentPrice){
                        currentPrice = opts.context.currentPrice;
                    }
                    if (opts.context.related_ds){
                        related_ds = opts.context.related_ds;
                    }else if (opts.context.category_ds){
                        //mainly for detail in movies, and for television when in history context
                        category_ds = opts.context.category_ds;
                    }else if (opts.context.selected_ch_id){
                        //for televisions
                        currentPage = "detail";
                        currentMediaType = "Television";
                        category_ds = telstra.api.manager.movies.getSeasonFeed(opts.context.selected_ch_id);
                    }else if (opts.context.mediaItemId){
                        //deeplinking from promo

                        telstra.sua.module.bpvod.config.init_parameter();
                        
                        //If a category url is present, create the according DS
                        if ('url' in opts.context) {
                            category_ds = telstra.api.manager.movies.getCategory(
                                opts.context.url,
                                {
                                    sort: telstra.sua.module.bpvod.session.listingSortBy,
                                    prependMediaItem: opts.context.mediaItemId
                                },
                                {
                                    onFailure: function(){
                                        telstra.sua.core.error.show(telstra.sua.core.error.CODE.BPVOD_MEDIAITEM_NOT_FOUND);
                                    }
                                });
                        } else {
                            category_ds = accedo.data.ds();
                            telstra.api.manager.movies.getMediaItem(opts.context.mediaItemId, {
                                onSuccess: function(resp){
                                    category_ds.appendData([resp]);
                                },
                                onFailure: function(){
                                    telstra.sua.core.error.show(telstra.sua.core.error.CODE.BPVOD_MEDIAITEM_NOT_FOUND);
                                }
                            });
                        }
                    }else if (opts.context.url){
                        category_ds = telstra.api.manager.movies.getCategory(opts.context.url, {
                            sort: telstra.sua.module.bpvod.session.listingSortBy
                        });
                        telstra.sua.module.bpvod.config.init_parameter();
                    }else{
                        accedo.console.log("context parameters not correctly set, probably errors will come up");
                    }
                    
                    //preselect the horizontal video item list
                    if (opts.context.shift){
                        hlist.select(opts.context.shift, ('shiftList' in opts.context)? opts.context.shiftList : null);
                        targetShift = opts.context.shift;
                    }
                    if (opts.context.forceListingMode){
                        forceListingMode = opts.context.forceListingMode;
                    }
                    
                    videoPreviewBox = this.get('videoPreviewBox');
                    okButton = videoPreviewBox.root ? videoPreviewBox.root.okButton : null;
                    if(okButton){
                    	okButton.addEventListener('click',function(evt){
	                    	accedo.console.log('okButton click handler');
	                    	componentClickHandler(currentData);
	                	});
	                }
                    //video preview box has different behaviour for different pages
                    videoPreviewBox.setCurrentParams({
                        currentMediaType: currentMediaType,
                        currentPage: currentPage,
                        currentPageSpecial: currentPageSpecial,
                        currentPrice: currentPrice,
                        forceListingMode: forceListingMode
                    });
                    
                    /**
                     * Handling of different purposes for this controller
                     * currentMediaType: 'Television' or 'Movies'
                     * currentPageSpeical: 'search_title' or 'search_people' (for pages following the search results)
                     * forceListingMode: for listing out the TV series instead of the episodes, even in detail page
                     * currentPage: 'related' or 'detail'
                     */
                    if (currentMediaType == 'Television'){
                        this.get('mediaTypeLabel').getRoot().addClass('tvShows');
                    }
                    //for page following search results
                    if (currentPageSpecial == 'search_title' || currentPageSpecial == 'search_people'){
                        this.get('mediaTypeLabel').getRoot().addClass('related');
                        leftUpperPanel = this.get('leftUpperPanel');
                        leftUpperPanel.setVisible(true);
                        this.get('titleText').hide();
                        this.get('categoryLabel').hide();
                        this.get('leftUpperPanelSubTitle1').setText(opts.context.currentTitle);
                        this.get('leftUpperPanelSubTitle2').setText(opts.context.currentSearchTitle);
                        
                        if (currentPageSpecial == 'search_people'){
                            category_ds = telstra.api.manager.movies.peopleDetails(opts.context.currentSearchUrl);
                            //Listing out the TV series instead of episodes
                            forceListingMode = true;
                        }
                    }
                    
                    if (currentPage == 'related') {
                        //handling for related movies, we have an extra menu on the left
                        leftPanelMovieInfo.hide();
                        leftPanelChoosePeople.show();
                        headerLRelated = leftPanelChoosePeople.getById('headerLRelated');
                        
                        if (opts.context.currentTitle){
                            headerLRelated.setText(opts.context.currentTitle);
                        }
                        
                        vlist = this.get('movie_related_listing');
                        
                        //TODO: If the currentSize is less that 5, the choose people category list should change to regular
                        var currentSize = related_ds.getCurrentSize();
                        if (currentSize < 5){
                            vlist.setOption('visibleSize', currentSize);
                        }
                        else {
                            vlist.setOption('visibleSize', 5);
                        }
                        //default should select the 1st element
                        vlist.select(0);

                        vlist.setDisplayHelper(displayHelperV);

                        // Bind data source to list
                        vlist.setDatasource(opts.context.related_ds);
                        upScrollerBtn = this.get('upScroller');
                    	downScrollerBtn = this.get('downScroller');

                        vlist.addEventListener('accedo:list:moved', function(evt) {
                            if (evt.reverse) {
                                upScrollerBtn.getRoot().addClass('focused');
                                setTimeout(function() {
                                    upScrollerBtn.getRoot().removeClass('focused');
                                }, 200);
                            } else {
                                downScrollerBtn.getRoot().addClass('focused');
	                            setTimeout(function() {
	                                downScrollerBtn.getRoot().removeClass('focused');
	                            }, 200);
                            }
                        });
                        
                        upScrollerBtn.addEventListener('click',function(evt){
	                        vlist.moveSelection(true);
	                    });
	                    
	                    downScrollerBtn.addEventListener('click',function(evt){
	                        vlist.moveSelection();
	                    });

                        // Since vlist does not allow focusing on design, so the
                        // only way to move it is key up and down
                        hlist.setOption('nextUp', function(){
                            vlist.moveSelection(true);
                        });
                        hlist.setOption('nextDown',function(){
                            vlist.moveSelection();
                        });
                    }else if (currentPage == 'detail') {
                        if (category_ds.getCurrentSize()<=0){
                            if (targetShift){
                                category_ds.load({
                                    dsIndex: targetShift
                                });
                            }else{
                                category_ds.load();
                            }
                        }

                        if (opts.context.currentCategoryName){
                            currentCategoryName = opts.context.currentCategoryName;
                            this.get('categoryLabel').setText(opts.context.currentCategoryName);
                        }
                        // Bind data source to list
                        hlist.setDatasource(category_ds);
                        hlist.setOption("nextUp",function(){
                            telstra.sua.module.bpvod.utils.scrollText('up', textContainer, textContent, vScrollbar);
                        });
                        hlist.setOption("nextDown",function(){
                            telstra.sua.module.bpvod.utils.scrollText('down', textContainer, textContent, vScrollbar);
                        });
                        
                        upScrollerText = this.get('upScrollerText');
                    	downScrollerText = this.get('downScrollerText');
                    	upScrollerText.addEventListener('click',function(evt){
                        	telstra.sua.module.bpvod.utils.scrollText('up', textContainer, textContent, vScrollbar);
	                    });
	                    downScrollerText.addEventListener('click',function(evt){
	                        telstra.sua.module.bpvod.utils.scrollText('down', textContainer, textContent, vScrollbar);
	                    });
                    }

                    // Hook up horizontal scrollbar
                    hlist.setScrollbar(this.get('hScrollbar'));
                    
                    // Get the reference of scrollKeyLeft and scrollKeyRight
                    scrollerKeyLeft  = this.get('scrollerKeyLeft');
                    scrollerKeyRight = this.get('scrollerKeyRight');

                    // Bind scrollKeys to listen hlist event
                    hlist.addEventListener('accedo:list:moved',function(evt){
                        if (!evt.reverse){
                            scrollerKeyRight.focus();
                            setTimeout(function(){
                                scrollerKeyRight.blur();
                            }, 200);
                        } else {
                            scrollerKeyLeft.focus();
                            setTimeout(function(){
                                scrollerKeyLeft.blur();
                            }, 200);
                        }
                    });
                    
                    scrollerKeyRight.addEventListener('click',function(evt){
                        if (hlist.getSize() > hlist.getVisibleSize()){
                        	hlist.moveSelectionByPage();
                        }
                    });
                    
                    scrollerKeyLeft.addEventListener('click',function(evt){
                        if (hlist.getSize() > hlist.getVisibleSize()){
                        	hlist.moveSelectionByPage(true);
                        }
                    });
                    
                    optionsList = [];
                    if (currentPage == 'detail'){
                    	if (currentMediaType == 'Television'){
                    		optionsList.push({
								type: "red",
								title: "Related Shows"
							});
                    	}
                    }
                    else{
                    	if (currentMediaType == 'Television'){
                    		
                    	}	
                    	else{
                    		optionsList.push({
								type: "red",
								title: "View Synopsis"
							});
                    	}
                    }
                    optionsList.push(
						{
							type: "yellow",
							title: "Classifications"
						},
						{
							type: "blue",
							title: "Movies Home"
						}
					);

                    // Set focus to horizontal list
                    accedo.utils.fn.defer(accedo.utils.fn.bind(accedo.focus.manager.requestFocus,  accedo.focus.manager), hlist);
                   
                   	//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);
                },

                onKeyPlay : function(){
                    utils.playTrailerAction(currentData, myPubObj, getHistoryItem());
                },

                onKeyFF : function() {
                    if (telstra.sua.core.main.mainNavBar.hasMenuButton())
                        hlist.getSelection().dispatchEvent('click');
                },
                
                onKeyRed: function() {
                	if (currentPage == 'detail'){
                        //related
                        if (!relatedMoviesAvailable){
	                		return;
	                	}
	                	
                        var related_ds = accedo.data.ds();
                        var array = [], i, actor_len = currentActor.length, dir_len = currentDirector.length;
                        for (i = 0; i < actor_len; i++) {
                            array.push({
                                text: 'Movies with '+ currentActor[i].actorname,
                                url: currentActor[i].url
                            });
                        }
                        for (i = 0; i < dir_len; i++) {
                            array.push({
                                text: 'Movies directed by ' + currentDirector[i].dirname,
                                url: currentDirector[i].url
                            });
                        }
                        related_ds.appendData(array);
                        utils.setDispatchController(myPubObj, 'movieDetailsController', {
                            related_ds: related_ds,
                            currentMediaType: currentMediaType,
                            currentTitle: currentData.title + ' [' + currentData.yearprod+']',
                            currentPage: 'related',
                            currentPrice: currentPrice,
                            forceListingMode: forceListingMode
                        },
                        false, getHistoryItem());
                    }else{ //current page == 'related'
                        //go to "View Synopsis" if its's already in related movie
                        utils.setDispatchController(myPubObj, 'movieDetailsController', {
                            //passing ds only for movies, so users could look through all the search results items
                            category_ds: category_ds,
                            shift: hlist.getSelection().dsIndex,
                            shiftList: hlist.getSelectedIndex(),
                            currentMediaType: currentMediaType,
                            currentCategoryName: currentCategoryName,
                            currentPrice: currentPrice,
                            //currentPage: 'related',
                            //This one is speical, in detail view of television, it'll show listing
                            //This is the View Synopsis page
                            forceListingMode: true
                        }, false, getHistoryItem());
                        
                        /*utils.setDispatchController(myPubObj, 'movieDetailsController', {
	                        url: currentDataUrl,
	                        shift: hlist.getSelection().dsIndex,
	                        shiftList: hlist.getSelectedIndex(),
	                        currentMediaType: currentMediaType,
	                        currentCategoryName: currentCategoryName,
	                        currentPrice: currentData.product.price,
	                        forceListingMode: forceListingMode
	                    }, false, getHistoryItem());*/
                        
                    }
                },
                
                onKeyGreen: function() {
                	//rent movie
                	currentData.categoryname = currentCategoryName || "";
                    utils.purchaseAction(currentData, myPubObj, getHistoryItem(), "movieDetailsController");
                },
                
                onKeyYellow: function() {
                	//classifications
                	telstra.api.usage.log("overlay", "", {
                        logMessage: "bigpond movies: options overlay logged " + (telstra.sua.module.bpvod.session.isSignedIn ? "in" : "out") + ": classifications"
                    });

                    telstra.sua.module.bpvod.controllers.popupClassificationController();
                },

                onKeyBack : function(){
                    utils.historyBack(this);
                }
            });
            
            return myPubObj;
        };
    });