/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 *
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.joinNowStep8TCController", [
    // dependency
    "telstra.api.usage",
    "telstra.sua.module.bpvod.views.joinNowStep8TCView",
    "telstra.sua.module.bpvod.controllers.joinNowTemplateController",
    "telstra.sua.core.controllers.popupOkInfoController"],
    function(){

        return function(opts){

            // Shorten for namespacing
            var utils = telstra.sua.module.bpvod.utils,
            self, leftPanel, termsContent, termsContainer, scrollbar, agreementYes, agreementNo, recieveMailYes, recieveMailNo, agree;

            var myPubObj = accedo.utils.object.extend(telstra.sua.module.bpvod.controllers.joinNowTemplateController(opts), {
    
                onCreate : function(){
                    telstra.api.usage.log("bpvod", "join_now_step_8", {
                        logMessage: "bigpond movies:join:terms"
                    });

                    self = this;

                    this.init(telstra.sua.module.bpvod.views.joinNowStep8TCView);

                    accedo.utils.fn.delay(function() {
                        leftPanel = self.get('leftPanel');
                        termsContent = self.get('termsContent');
                        termsContainer = self.get('termsContainer');
                        scrollbar = self.get('scrollbar');
                        agreementYes = self.get('agreementYes');
                        agreementNo = self.get('agreementNo');
                        recieveMailYes = self.get('recieveMailYes');
                        recieveMailNo = self.get('recieveMailNo');

                        termsContent.setText(telstra.sua.module.bpvod.config.termsTxt);

                        var containerHeight = termsContainer.getRoot().getHTMLElement().offsetHeight;
                        var currentHeight  = termsContainer.getRoot().getHTMLElement().scrollHeight;
                        var currentTop = termsContainer.getRoot().getHTMLElement().offsetTop;

                        var scrollText = function(direction){ //0 = not move, 1 = up, 2 = down
                            var targetTop;

                            if(direction == 1){
                                targetTop = currentTop + 95;
                                if(targetTop > 0){
                                    targetTop = 0;
                                }
                            }else if (direction == 2){
                                targetTop = currentTop - 95;
                                if(targetTop < -1 * (currentHeight - containerHeight) ){
                                    targetTop = -1 * (currentHeight - containerHeight);
                                }
                            }

                            if(direction != 0){
                                currentTop = targetTop;
                                accedo.console.log('currentTop: ' + currentTop);
                                termsContent.getRoot().setStyle({
                                    top: targetTop + "px"
                                });
                            }

                            scrollbar.update(-1* currentTop, containerHeight, currentHeight );
                        }

                        leftPanel.setOption('nextRight', 'agreementYes');
                        recieveMailNo.setOption('nextDown', 'continueButton');

                        leftPanel.setOption("nextUp",function(){
                            scrollText(1);
                        });

                        leftPanel.setOption("nextDown",function(){
                            scrollText(2);
                        });

                        accedo.utils.fn.defer(scrollText, 1, 0);

                        accedo.focus.manager.requestFocus(leftPanel);

                        agreementYes.addEventListener('click', function() {
                            agreementYes.getRoot().addClass('select');
                            agreementNo.getRoot().removeClass('select');
                            agree = true;
                            accedo.focus.manager.requestFocus(recieveMailYes);
                        });

                        agreementNo.addEventListener('click', function() {
                            agreementYes.getRoot().removeClass('select');
                            agreementNo.getRoot().addClass('select');
                            agree = false;
                            accedo.focus.manager.requestFocus(recieveMailYes);
                        });

                        recieveMailYes.addEventListener('click', function() {
                            recieveMailYes.getRoot().addClass('select');
                            recieveMailNo.getRoot().removeClass('select');
                            self.joinNowInfo.receiveNews = true;
                            accedo.focus.manager.requestFocus(self.continueButton);
                        });

                        recieveMailNo.addEventListener('click', function() {
                            recieveMailYes.getRoot().removeClass('select');
                            recieveMailNo.getRoot().addClass('select');
                            self.joinNowInfo.receiveNews = false;
                            accedo.focus.manager.requestFocus(self.continueButton);
                        });

                        agreementYes.onFocus = function() {
                            leftPanel.setOption('nextRight', 'agreementYes');
                        };

                        agreementNo.onFocus = function() {
                            leftPanel.setOption('nextRight', 'agreementNo');
                        };

                        recieveMailYes.onFocus = function() {
                            leftPanel.setOption('nextRight', 'recieveMailYes');
                        };

                        recieveMailNo.onFocus = function() {
                            leftPanel.setOption('nextRight', 'recieveMailNo');
                        };

                        self.continueButton.setOption('nextUp', 'recieveMailNo');

                        self.continueButton.onFocus = function() {
                            recieveMailNo.setOption('nextDown', 'continueButton');
                        };

                        self.backButton.setOption("nextUp", 'recieveMailNo');

                        self.backButton.onFocus = function() {
                            recieveMailNo.setOption('nextDown', 'backButton');
                        };
                    }, 0.3);
                },

                continueButtonAction : function() {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();

                    if (agree == null) {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : 'You must agree to the terms and conditions to join BigPond Movies.',
                            colour : 'red',
                            id : 'agreeTermsConditions'
                        });
                    }
                    else {
                        if (self.joinNowInfo.receiveNews == null) {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();

                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : 'You must choose whether to receive Telstra marketing communications or not',
                                colour : 'red',
                                id : 'chooseSubscriptionType'
                            });
                        }
                        else {
                            if (!agree) {
                                telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : 'You must agree to the terms and conditions to join BigPond Movies.',
                                    colour : 'red',
                                    id : 'agreeTermsConditions'
                                });
                            }
                            else {
                                var creditCardInfo = {
                                    cardType: self.creditCardInfo.cardType,
                                    cardNumber: (self.creditCardInfo.cardType == "amex" ? self.creditCardInfo.cardNumber.cc1 + self.creditCardInfo.cardNumber.cc2 + self.creditCardInfo.cardNumber.cc3 : self.creditCardInfo.cardNumber.cc1 + self.creditCardInfo.cardNumber.cc2 + self.creditCardInfo.cardNumber.cc3 + self.creditCardInfo.cardNumber.cc4),
                                    expiryDate: "20" + self.creditCardInfo.expiryDate.year + "-" + self.creditCardInfo.expiryDate.month,
                                    securityCode: self.creditCardInfo.securityCode
                                }

                                self.joinNowInfo.cardType = creditCardInfo.cardType;
                                self.joinNowInfo.cardNumber = creditCardInfo.cardNumber;
                                self.joinNowInfo.expiryDate = creditCardInfo.expiryDate;
                                self.joinNowInfo.securityCode = creditCardInfo.securityCode;
                                
                                accedo.console.log(accedo.utils.object.toJSON(self.joinNowInfo));

                                telstra.api.manager.movies.registerMoviesAccount(self.joinNowInfo, {
                                    onSuccess:function() {
                                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                        utils.signInAction(self.joinNowInfo.email, self.joinNowInfo.pwd, function() {
                                            utils.updateFirstTimeLogin(function() {
                                                var param = accedo.utils.object.clone(telstra.api.manager.movies.accountInfo);
                                                param.purchaseItem = opts.context.purchaseItem || null;
                                                
                                                telstra.sua.module.bpvod.session.isSignedIn = true;
                                                telstra.sua.module.bpvod.session.signedInButNotFinishedProcess = false;
                                                utils.saveAutoSignIn();

                                                if(!self.param.purchaseItem && self.param.purchaseItem == null) {
                                                    self.param.joinSteps = true;
                                                    utils.setDispatchController(myPubObj, 'signInCongraController', self.param, true);
                                                } else{
                                                    utils.purchaseAction(self.param.purchaseItem, myPubObj);
                                                }

                                                self.clearData();
                                            });
                                        }, false, function(json) {
                                            });
                                    },
                                    onFailure: function(json) {
                                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                        if(!accedo.utils.object.isUndefined(json) && !accedo.utils.object.isUndefined(json.errorcode)) {
                                            var popupOk = telstra.sua.core.controllers.popupOkInfoController, debugMsg = "";

                                            if (!accedo.utils.object.isUndefined(json.errordebugmsg)) {
                                                debugMsg = (json.errordebugmsg).replace(/\n/g, "<br/><br/>");
                                            }

                                            switch (json.errorcode) {
                                                case "PAY_COUNTRYERROR":
                                                case "PAY_COUNTRYRERROR":
                                                    popupOk({
                                                        headerText: 'Error',
                                                        innerText: 'The credit card information you entered is not valid.' +
                                                        '<br/>Please re-enter a valid Australian credit card and try again.' +
                                                        '<br/>For further help call Telstra on' +
                                                        '1800 502 502 and quote reference DVX-10.',
                                                        buttonText: 'CLOSE',
                                                        callback: function() {
                                                            utils.setDispatchController(myPubObj, 'joinNowStep4BCardInfoController', self.param, true);
                                                        }
                                                    });
                                                    break;
                                                case "PAY_TXERROR":
                                                    popupOk({
                                                        headerText: 'Error',
                                                        innerText: 'You may be experiencing a technical issue.' +
                                                        '<br/>Please contact Telstra on' +
                                                        '<br/>1800 502 502 for assistance and quote reference number DVX-12.',
                                                        buttonText: 'CLOSE',
                                                        callback: function() {
                                                            utils.setDispatchController(myPubObj, 'joinNowStep4BCardDetailsController', self.param, true);
                                                        }
                                                    });
                                                    break;
                                                case "CREDIT_CARD_NOT_VALID":
                                                    popupOk({
                                                        headerText: 'Error',
                                                        innerText: 'Your credit card number is not valid. Please try again.',
                                                        buttonText: 'CLOSE',
                                                        callback: function() {
                                                            utils.setDispatchController(myPubObj, 'joinNowStep4BCardDetailsController', self.param, true);
                                                        }
                                                    });
                                                    break;
                                                case "CREDIT_CARD_TYPE_UNSUPPORTED":
                                                    popupOk({
                                                        headerText: 'Error',
                                                        innerText: 'Your credit card type is not supported. Please try again.',
                                                        buttonText: 'CLOSE',
                                                        callback: function() {
                                                            utils.setDispatchController(myPubObj, 'joinNowStep4ACardTypeController', self.param, true);
                                                        }
                                                    });
                                                    break;
                                                case "EMAIL_ALREADY_REGISTERED":
                                                    utils.setDispatchController(myPubObj, 'joinNowStep1EmailController', self.param, true);

                                                    accedo.utils.fn.delay(function(){
                                                        telstra.sua.core.components.infoMessageBar.addBar({
                                                            text : 'Your email has already been registered. Please try again.',
                                                            colour : 'red',
                                                            id : 'emailAlreadyRegistered'
                                                        });
                                                    }, 0.2);
                                                    break;
                                                default:
                                                    popupOk({
                                                        headerText: 'Error (Code: ' + json.errorcode + ')',
                                                        innerText: json.errordescription + '<br/>' + debugMsg,
                                                        buttonText: 'OK'
                                                    });
                                                    break;
                                            }
                                        }
                                        else {
                                            telstra.sua.core.components.infoMessageBar.addBar({
                                                text : 'Register failed, please try again.',
                                                colour : 'red',
                                                id : 'registerFailed'
                                            });
                                        }
                                    },
                                    onException: function() {
                                        telstra.sua.core.components.loadingOverlay.turnOffLoading();
                                    }
                                });
                            }
                        }
                    }
                },

                backButtonAction : function() {
                    if (telstra.sua.core.components.loadingOverlay.isLoadingOn())
                        return;

                    leftPanel.setOption('nextRight', 'agreementYes');
                    recieveMailNo.setOption('nextDown', 'continueButton');
                    utils.setDispatchController(myPubObj, 'joinNowStep7DeviceNameController', self.param, true);
                },

                clearData : function() {
                    self.joinNowInfo.email = null;
                    self.joinNowInfo.pwd = null;
                    self.joinNowInfo.firstName = null;
                    self.joinNowInfo.surName = null;
                    self.joinNowInfo.receiveNews = null;
                    self.joinNowInfo.useAccountPin = null;
                    self.joinNowInfo.accountPin = null;
                    self.joinNowInfo.deviceName = null;
                    self.joinNowInfo.cardType = null;
                    self.joinNowInfo.cardNumber = null;
                    self.joinNowInfo.expiryDate = null;
                    self.joinNowInfo.securityCode = null;
                    self.creditCardInfo.cardType = null;
                    self.creditCardInfo.cardNumber.cc1 = null;
                    self.creditCardInfo.cardNumber.cc2 = null;
                    self.creditCardInfo.cardNumber.cc3 = null;
                    self.creditCardInfo.cardNumber.cc4 = null;
                    self.creditCardInfo.expiryDate.month = null;
                    self.creditCardInfo.expiryDate.year = null;
                    self.creditCardInfo.securityCode = null;
                }
            });

            return myPubObj;
        }
    });