/**
 * @fileOverview movieCategoryListingController
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 * @author <a href="mailto:roy.chan@accedobroadband.com">Roy Chan</a>
 *
 */

// @todo: if category listing (vlist) items have only less than 5 then should convert to normal list

accedo.define(
    "telstra.sua.module.bpvod.controllers.movieCategoryListingController",[
    "accedo.utils.object", "accedo.utils.fn",
    "accedo.ui.controller", "accedo.ui.list",
    "accedo.data.ds",
    "accedo.focus.manager",
    "accedo.focus.managerMouse",
    "telstra.api.manager",
    "telstra.api.usage",
    "telstra.sua.module.bpvod.views.movieCategoryListingView", "telstra.sua.module.bpvod.utils",
    "telstra.sua.module.bpvod.session",
    "telstra.sua.module.bpvod.components.popupSortBy", 
    "telstra.sua.module.bpvod.controllers.popupClassificationController"],
    function(){
        return function(opts){
            // Shorten namespace
            var utils = telstra.sua.module.bpvod.utils,
            session = telstra.sua.module.bpvod.session,

            // Define two cached data source
            subchannel_ds,
            category_ds,

            self, vlist, hlist, categoryLabel, scrollerKeyLeft, scrollerKeyRight, upScrollerBtn, downScrollerBtn,
            currentDataUrl, currentMediaType, currentUrl, currentCategoryName, forceListingMode = false,
            getMovieTimeout = null, goToSearchTimeout = null, updateVideoPreviewTimeout = null,
            displayHelperV, displayHelperH, myPubObj, refreshHList, firstTimeRefreshHList = true,
            selectedIndexCat, getHistoryItem, componentClickHandler,
                
            currentPage = 'listing', currentPageSpecial = null, currentData, currentDataIndex,
            videoPreviewBox, okButton, trailerImageContainer,
            refreshNavBar, 
            optionsList;

            /**
             * Refresh HList when the categroy list is selected
             */
            refreshHList = function(){
                category_ds = telstra.api.manager.movies.getCategory(currentDataUrl, {
                    sort: session.listingSortBy
                });
               
                hlist.setDatasource(category_ds);
                hlist.setDisplayHelper(displayHelperH);
                
                if (category_ds.getCurrentSize()<=0){
                    category_ds.load();
                }
                
                if (firstTimeRefreshHList){
                    firstTimeRefreshHList = false;
                }

                //omniture code, do it when selecting category
                if (currentMediaType === 'Television') {
                    telstra.api.usage.log("bpvod", "browse_catalogue", {
                        logMessage: "bigpond movies:browse catalogue:tv shows:" + currentCategoryName
                    });
                } else {
                    telstra.api.usage.log("bpvod", "browse_catalogue", {
                        logMessage: "bigpond movies:browse catalogue:movies:" + currentCategoryName
                    });
                }
            },
            /**
             * Set display for the component in the Vertical category list
             * @param {accedo.ui.component} each component of the list
             * @param {Object} data An object representing a BPMovies Category
             */
            displayHelperV = function(component, data) {

                var categoryName = data.categoryname;
               
                // Special case styling for New Release category (come with a star
                // bullet point)
                if (categoryName === 'New Releases'){
                    component.getRoot().addClass('star');
                }

                // Set the category name to component
                component.setText(categoryName);

                component.onSelect = function() {
                    
                    if (!accedo.utils.object.isUndefined(getMovieTimeout)) {
                        clearTimeout(getMovieTimeout);
                        getMovieTimeout = null;
                    }
                    if (!accedo.utils.object.isUndefined(goToSearchTimeout)){
                        clearTimeout(goToSearchTimeout);
                        goToSearchTimeout = null;
                    }
                    categoryLabel.setText(categoryName);
                    currentCategoryName = categoryName;
                    // Create data source once vlist focusing on certain a
                    // category by calling api
                    if ('url' in data){
                        //normal category (not 'Search')
                        currentDataUrl = data.url;
                        if (firstTimeRefreshHList){
                            refreshHList();
                        }else{
                            getMovieTimeout = setTimeout(refreshHList, 500);
                        }
                        selectedIndexCat = component.dsIndex;
                    }else{
                        //handling of search
                        goToSearchTimeout = setTimeout(function(){
                            var historyItem = accedo.utils.object.extend(getHistoryItem(), {
                                selectedIndexCat:1
                            });
                            utils.setDispatchController(myPubObj, 'searchPageController',{}, false, historyItem);
                        }, 500);
                    }
                };
                
                component.parent.addEventListener('click', function(){
	            	var moveStep = -2, moveDirection = true, 
                    currentIndex = component.dsIndex,
                    visibleCategories = vlist.getChildren();
                    for(var i = 0; i < visibleCategories.length; ++i){
                    	if (visibleCategories[i].dsIndex == currentIndex){
                    		moveStep += i;
                    		if (moveStep > 0){
                    			moveDirection = false;
                    		}
                    		break;
                    	}
                    }
                    if (moveStep != 0){
                    	vlist.moveSelection(moveDirection);
                    	if (Math.abs(moveStep) == 2){
                    		vlist.moveSelection(moveDirection);
                    	}
                    }
	           });
            };

            /**
             * Set display for the component in the Horizontal list
             * @param {accedo.ui.component} component The accedo ui component to assign
             * @param {Object} data An object representing a BPMovies title or episode
             */
            displayHelperH = function(component, data) {
                var videoItemImage = component.getById('bpvod_videoItemImage');

                if (!forceListingMode && currentMediaType == 'Television' && currentPage=='detail') {
                    videoItemImage.getRoot().addClass('television');
                    videoItemImage.setSrc(telstra.api.manager.resizer(data.coverimage, 112, 77, true));
                    component.getById('episodeLabel').setText('Season '+ data.seasonnumber + '<br/>Episode '+data.episodenumber);
                }
                else {
                    videoItemImage.setSrc(telstra.api.manager.resizer(data.coverimagemedium, 135, 192, true));
                }

                /**
                 * refresh the videoPreviewBox and left panel txt when video item is selected
                 */
                component.onSelect = function() {
                    telstra.sua.core.components.loadingOverlay.turnOffLoading();

                    if (updateVideoPreviewTimeout){
                        clearTimeout(updateVideoPreviewTimeout);
                        updateVideoPreviewTimeout = null;
                    }

                    updateVideoPreviewTimeout = setTimeout(function(){
                        videoPreviewBox.updateDisplay(data);
                        currentData = data;
                        currentDataIndex = component.dsIndex;
                        trailerImageContainer = videoPreviewBox.getStillImageContainer();
	                    if (trailerImageContainer){
	                    	trailerImageContainer.addEventListener('click',function(evt){
		                    	utils.playTrailerAction(currentData, myPubObj, getHistoryItem());
		                	});
	                    }
                        refreshNavBar();
                    }, 500);
                    
                    if (hlist.getSize() <= hlist.getVisibleSize()){
                    	scrollerKeyRight.root.addClass('mouse-block');
                    	scrollerKeyLeft.root.addClass('mouse-block');
                    }
                    else{
                    	scrollerKeyRight.root.removeClass('mouse-block');
                    	scrollerKeyLeft.root.removeClass('mouse-block');
                    }
                };

                /**
                 * go to detail page for episode or buy item
                 */
                component.addEventListener('click', function() {
                    componentClickHandler(data, component.dsIndex);
                });
                
                component.parent.addEventListener('mouseover', function() {
                    accedo.focus.managerMouse.requestFocus(component.root);
                });
                component.parent.addEventListener('mouseout', function() {
                    accedo.focus.managerMouse.releaseFocus();
                });
                
                component.parent.addEventListener('click', function(){
	            	//check if the selected item has been clicked
	            	var visibleItems = hlist.getChildren();
	            	if (visibleItems[hlist.getSelectedIndex()] == component){
	            		//click through
	            		componentClickHandler(data, component.dsIndex);
	            	}
	            	else{
	            		//change selection (first - define the index relatively to the visible items)
	            		for(var i = 0; i < visibleItems.length; ++i){
	            			if (visibleItems[i] == component){
	            				hlist.setSelectedIndex(i);
	            				break;		
	            			}
	            		}
	            	}
	           });
                
            };
            componentClickHandler = function(dataObj, dataIndex){
            	if (dataObj.rented) {
                    dataObj.categoryname = currentCategoryName || "";
                    utils.purchaseAction(dataObj, myPubObj, getHistoryItem(), "movieCategoryListingController");
                }
                else if (currentMediaType == 'Television') {
                    utils.setDispatchController(myPubObj, 'movieDetailsController', {
                        selected_ch_id: dataObj.mediaitemid,
                        currentMediaType: currentMediaType,
                        currentCategoryName: currentCategoryName
                    }, false, getHistoryItem());
                }
                else { //movies
                    utils.setDispatchController(myPubObj, 'movieDetailsController', {
                        url: currentDataUrl,
                        shift: dataIndex,
                        shiftList: dataIndex,
                        currentMediaType: currentMediaType,
                        currentCategoryName: currentCategoryName,
                        currentPrice: dataObj.product.price
                    }, false, getHistoryItem());
                }
            };
            /**
             * Get the current context for saving into history
             */
            getHistoryItem = function(){
                if (updateVideoPreviewTimeout){
                    clearTimeout(updateVideoPreviewTimeout);
                    updateVideoPreviewTimeout = null;
                }
                return {
                    currentMediaType: currentMediaType,
                    url: currentUrl,
                    selectedIndexH: hlist.getSelection() ? hlist.getSelection().dsIndex : 0,
                    selectedIndexHList: hlist.getSelection()? hlist.getSelectedIndex() : 0,
                    selectedIndexCat: selectedIndexCat
                }
            };
            /**
             * Refresh navigation bar according to currently selected video item and media type
             */
            refreshNavBar = function(){
                telstra.sua.core.main.mainNavBar.resetMenuButton();
                
                //grab the default button list for this screen
                var arr = accedo.utils.array.clone(optionsList);

                //If the movie is rented, the "rent movie" green button is not needed
                if (currentMediaType == 'Television' || (currentData && (currentData.rented || currentData.product.price == '$0.00'))) {}
                else {
                    //telstra.sua.core.main.mainNavBar.setMenuButton("RENT TITLE");
                    
                    arr.push({
						type: "green",
						title: "Rent Title"
					});
                }
                
                if (currentMediaType == 'Television') {
                	arr.push({
						type: "green",
						title: "View Episodes"
					});	
                }
                
                //set nav bar buttons
                telstra.sua.core.main.mainNavBar.setMenuButtons(arr);
            };
            myPubObj = accedo.utils.object.extend(accedo.ui.controller(opts), {
                /**
                 * Handle action needed to take on creating controller
                 * @param context
                 */
                onCreate: function(context) {
                    // Set the view
                    this.setView(telstra.sua.module.bpvod.views.movieCategoryListingView);
                    self = this;
                    vlist = this.get('movie_category_listing');
                    hlist = this.get('movie_horizontal_listing');
                    categoryLabel = this.get('categoryLabel');

                    // Initial get from network with media type specified
                    //opts.context.subChannel may be "Free Movies", "Movies", or "Television"
                    currentMediaType =  opts.context.currentMediaType;
                    currentUrl = opts.context.url;
                    
                    if ('subchannel_ds' in opts.context) {
                        // Handle return from movieDetail controller
                        subchannel_ds = opts.context.subchannel_ds;
                    }else{
                        subchannel_ds = telstra.api.manager.movies.getSubChannel(currentUrl);
                        
                        if (opts.context.selectedCatName){
                            subchannel_ds.addEventListener('accedo:datasource:append', function(){
                                vlist.select(subchannel_ds.search(function(data,index){
                                    return data.categoryname == opts.context.selectedCatName;
                                }));
                            });
                        }
                    }
                    
                    if ('selectedIndexCat' in opts.context){
                        vlist.select(opts.context.selectedIndexCat);
                    }
                    //
                    if ('selectedIndexH' in opts.context){
                        hlist.select(opts.context.selectedIndexH, 'selectedIndexHList' in opts.context? opts.context.selectedIndexHList : null);
                    }

                    if (subchannel_ds.getCurrentSize()<=0){
                        subchannel_ds.load();
                    }
                    
                    videoPreviewBox = this.get('videoPreviewBox');
                    okButton = videoPreviewBox.root ? videoPreviewBox.root.okButton : null;
                    if(okButton){
                    	okButton.addEventListener('click',function(evt){
	                    	accedo.console.log('okButton click handler');
	                    	componentClickHandler(currentData, currentDataIndex);
	                	});
	                }
                    //video preview box has different behaviour for different pages
                    videoPreviewBox.setCurrentParams({
                        currentMediaType: currentMediaType,
                        currentPage: currentPage,
                        currentPageSpecial: currentPageSpecial,
                        forceListingMode: forceListingMode
                    });
                    
                    if (currentMediaType == 'Television'){
                        this.get('mediaTypeLabel').getRoot().addClass('tvShows');
                    }
                    // Bind vlist data source and helper
                    vlist.setDisplayHelper(displayHelperV);
                    vlist.setDatasource(subchannel_ds);

                    hlist.setDisplayHelper(displayHelperH);

                    // Since vlist does not allow focusing on design, so the
                    // only way to move it is key up and down
                    hlist.setOption('nextUp', function(){
                        vlist.moveSelection(true);
                    });
                    hlist.setOption('nextDown',function(){
                        vlist.moveSelection();
                    });

                    // Hook up horizontal scrollbar
                    hlist.setScrollbar(this.get('hScrollbar'));

                    // Given the main focus to hlist
                    accedo.utils.fn.defer(accedo.utils.fn.bind(accedo.focus.manager.requestFocus, accedo.focus.manager), hlist);

                    // Hook up scollerKey left, right, up, and down
                    scrollerKeyLeft  = this.get('scrollerKeyLeft');
                    scrollerKeyRight = this.get('scrollerKeyRight');

                    upScrollerBtn = this.get('upScroller');
                    downScrollerBtn = this.get('downScroller');

                    vlist.addEventListener('accedo:list:moved', function(evt) {
                        if (evt.reverse) {
                            upScrollerBtn.getRoot().addClass('focused');
                            setTimeout(function() {
                                upScrollerBtn.getRoot().removeClass('focused');
                            }, 200);
                        } else {
                            downScrollerBtn.getRoot().addClass('focused');
                            setTimeout(function() {
                                downScrollerBtn.getRoot().removeClass('focused');
                            }, 200);
                        }
                    });
                    
                    upScrollerBtn.addEventListener('click',function(evt){
                        vlist.moveSelection(true);
                    });
                    
                    downScrollerBtn.addEventListener('click',function(evt){
                        vlist.moveSelection();
                    });

                    // Bind scrollKeys to listen list event
                    hlist.addEventListener('accedo:list:moved',function(evt){
                        if (!evt.reverse){
                            scrollerKeyRight.focus();
                            setTimeout(function(){
                                scrollerKeyRight.blur();
                            }, 200);
                        } else {
                            scrollerKeyLeft.focus();
                            setTimeout(function(){
                                scrollerKeyLeft.blur();
                            }, 200);
                        }
                    });
                    
                	scrollerKeyRight.addEventListener('click',function(evt){
                        //hlist.moveSelectionLoop();
                        if (hlist.getSize() > hlist.getVisibleSize()){
                        	hlist.moveSelectionByPage();
                        }
                    });
                    
                    scrollerKeyLeft.addEventListener('click',function(evt){
                        //hlist.moveSelectionLoop(true);
                        if (hlist.getSize() > hlist.getVisibleSize()){
                        	hlist.moveSelectionByPage(true);
                        }
                    });
                    
                    optionsList = [
						{
							type: "red",
							title: "Sort by"
						},
						{
							type: "yellow",
							title: "Classifications"
						}
                    ];
                   
                   	//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);
                },

                /**
                 * Play the trailer
                 */
                onKeyPlay : function(){
                    utils.playTrailerAction(currentData, myPubObj, getHistoryItem());
                },
                
                onKeyRed: function() {
                	//sort by
                	var returnfocus = accedo.focus.manager.getCurrentFocus(),
                    popupSortBy = telstra.sua.module.bpvod.components.popupSortBy({
                        id:    'popupSortBy',
                        css:   'popupSortBy panel popup focus',
                        showpopular: (currentDataUrl.indexOf('Free Trial Movies') < 0)
                    });
                    if (!session.sortOption){
                    	session.sortOption = 'ReleaseDate';
                    }
                    accedo.utils.fn.defer(accedo.utils.fn.bind(accedo.focus.manager.requestFocus, accedo.focus.manager), popupSortBy.get(session.sortOption));

                    // Listen to popup onClose event
                    popupSortBy.addEventListener('bpvod:popupSortBy:onClose', function(evt){
                        if (evt.sortby !== '' && evt.sortby !== session.listingSortBy){
                            category_ds = telstra.api.manager.movies.getCategory(currentDataUrl, {
                                sort: evt.sortby
                            });
                            category_ds.load();
                            hlist.setDatasource(category_ds);
                            session.listingSortBy = evt.sortby;
                            session.sortOption = evt.sortOption;
                        }
                        // Restore the original focus
                        accedo.focus.manager.requestFocus(returnfocus);
                    });
                },
                
                onKeyGreen: function() {
                	if (currentMediaType == 'Television'){
                		//Action: View episodes
                		componentClickHandler(currentData);
                	}
                	else{
                        //Action: Rent Movie
                        currentData.categoryname = currentCategoryName || "";
                        utils.purchaseAction(currentData, myPubObj, getHistoryItem(), "movieCategoryListingController");
                    }
                },
                
                onKeyYellow: function() {
                	//classifications
                	telstra.api.usage.log("overlay", "", {
                        logMessage: "bigpond movies: options overlay logged " + (session.isSignedIn ? "in" : "out") + ": classifications"
                    });

                    telstra.sua.module.bpvod.controllers.popupClassificationController();
                },

                /**
                 *  Action need to be done when back button is pressed
                 */
                onKeyBack : function(){
                    utils.historyBack(this);
                }
            });
            return myPubObj;
        };
    }
    );

