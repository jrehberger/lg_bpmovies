/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    'telstra.sua.module.bpvod.controllers.myAccountChangeNewsSubscripController',
    ['accedo.utils.object',
    'accedo.ui.controller',
    'accedo.focus.manager',
    'telstra.api.manager',
    'telstra.api.usage',
    'telstra.sua.core.components.infoMessageBar',
    'telstra.sua.core.components.loadingOverlay',
    "telstra.sua.module.bpvod.views.myAccountChangeNewsSubscripView"],
    function(){
        return function(opts){

            var utils = telstra.sua.module.bpvod.utils,
            views = telstra.sua.module.bpvod.views,
            optionsList = [],
            self, option1, option2, subNews, saveChangesButton, closeButton;

            return accedo.utils.object.extend(accedo.ui.controller(opts), {

                onCreate: function() {
                    telstra.sua.core.main.mainNavBar.setMenuButton("HELP");

                    self = this;

                    //Set the view
                    self.setView(views.myAccountChangeNewsSubscripView);

                    option1 = self.get('option1');
                    option2 = self.get('option2');
                    saveChangesButton = self.get('saveChangesButton');
                    closeButton = self.get('closeButton');

                    if (telstra.api.manager.movies.accountInfo.receivenewsletter) {
                        option1.getRoot().addClass('select');
                        subNews = 1;
                    }
                    else {
                        option2.getRoot().addClass('select');
                        subNews = 0;
                    }

                    saveChangesButton.addEventListener('click', function() {
                        self.saveAction();
                    });

                    saveChangesButton.onFocus = function() {
                        option2.setOption('nextDown', 'saveChangesButton');
                    };

                    closeButton.addEventListener('click', function() {
                        option2.setOption('nextDown', 'saveChangesButton');
                        self.dispatchEvent('telstra:core:historyBack');
                    });

                    closeButton.onFocus = function() {
                        option2.setOption('nextDown', 'closeButton');
                    };

                    option1.addEventListener('click', function() {
                        option1.getRoot().addClass('select');
                        option2.getRoot().removeClass('select');
                        subNews = 1;
                        accedo.focus.manager.requestFocus(saveChangesButton);
                    });

                    option2.addEventListener('click', function() {
                        option2.getRoot().addClass('select');
                        option1.getRoot().removeClass('select');
                        subNews = 0;
                        accedo.focus.manager.requestFocus(saveChangesButton);
                    });

                    optionsList.push(
						{
							type: "red",
							title: "Help"
						}
					);
					//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);

                    accedo.focus.manager.requestFocus(saveChangesButton);
                },

                saveAction: function() {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();

                    telstra.api.manager.movies.accountInfo.receivenewsletter = subNews;

                    utils.updateMoviesAccount(telstra.api.manager.movies.accountInfo, {
                        onSuccess:function() {
                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : 'Your newsletter subscription has been updated.',
                                colour : 'green',
                                id : 'subscriptionUpdated'
                            });

                            accedo.utils.fn.delay(function() {
                                telstra.sua.core.components.loadingOverlay.turnOffLoading();
                                self.dispatchEvent('telstra:core:historyBack');
                            }, 2);
                        }
                    });
                },

                onKeyRed : function() {
                    telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 19949);
                },

                onKeyBack: function() {
                    if (telstra.sua.core.components.loadingOverlay.isLoadingOn())
                        return;

                    option2.setOption('nextDown', 'saveChangesButton');
                    self.dispatchEvent('telstra:core:historyBack');
                }
            });
        }
    });