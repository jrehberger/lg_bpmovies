/**
 * @fileOverview popupSpeedTestController
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
 */

var testConnectionBar = null;

accedo.define("telstra.sua.module.bpvod.controllers.popupSpeedTestController", [
    'telstra.sua.module.bpvod.views.popupSpeedTestView',
    "accedo.utils.object",
    "telstra.api.usage",
    "telstra.sua.module.bpvod.views.videoPageView",
    "telstra.sua.core.components.loadingOverlay",
    "telstra.sua.module.bpvod.controllers.popupBigPondPlanController",
    "accedo.ui.popup"],
    function() {

        /**
         * @class
         * @extends accedo.ui.popup
         */
        return function(opts) {

            var myPubObj,
            self,
            media = accedo.device.manager.media,
            loading = telstra.sua.core.components.loadingOverlay,
            config = telstra.sua.module.bpvod.config,
            identification = accedo.device.manager.identification,
            popupMain = null,
            barBase,
            resultIcon,
            closeButton,
            bigPondPlansButton,
            labelContact,
            label1, label2, label3, label4, label5, label6,
            barContainer,
            arrowContainer,
            barRed, barRedEnd,
            barYellow, barYellowEnd,
            barGreen, barGreenEnd,

            speedTestDone = false,
            counter = 0;

            myPubObj = accedo.utils.object.extend(accedo.ui.popup(opts), {

                init : function(){
                	
                	accedo.console.log('popupSpeedTestController init');
                	
                    self = this;
                    //setView
                    this.setView(telstra.sua.module.bpvod.views.popupSpeedTestView);

                    counter = 0;

                    popupMain = this.get('popupBg').getById('popupMain');
                    closeButton = this.get('closeButton');
                    bigPondPlansButton = this.get('bigPondPlansButton');
                    barBase = this.get('barBase');
                    resultIcon = this.get('resultIcon');
                    labelContact = this.get('labelContact');
                    label1 = this.get('label1');
                    label2 = this.get('label2');
                    label3 = this.get('label3');
                    label4 = this.get('label4');
                    label5 = this.get('label5');
                    label6 = this.get('label6');
                    barContainer = this.get('barContainer');
                    arrowContainer = this.get('arrowContainer');
                    barRed = this.get('barRed');
                    barRedEnd = this.get('barRedEnd');
                    barYellow = this.get('barYellow');
                    barYellowEnd = this.get('barYellowEnd');
                    barGreen = this.get('barGreen');
                    barGreenEnd = this.get('barGreenEnd');
                    errorLabel = this.get('errorLabel');
                    barContainer.hide();
                    arrowContainer.hide();
                    errorLabel.hide();

                    closeButton.addEventListener('click', function() {
                        if(!speedTestDone){
                            self.stopTestAndClose();
                        }else{
                            self.close();
                        }
                    });

                    bigPondPlansButton.addEventListener('click', function() {
                        telstra.sua.module.bpvod.controllers.popupBigPondPlanController();
                    });

                    accedo.focus.manager.requestFocus(bigPondPlansButton);

                    testConnectionBar = this;

                    speedTestDone = false;
                    /*if (accedo.device.manager.identification.getMac() == "AACCEEDD0012"){
                    	var testBitrates = {
                    		currentBitrate : 2849304,//7343792,
                            availableBitrates : [1646184]//[10251784]//[409616,509576,1009648,2009752,3009832,3509848,4009880,4509936,5009888,5516416,6016352]
                        };
                    	self.updateBar(testBitrates);
                    }
                    else*/ if(accedo.device.manager.identification.getDeviceId() != "workstation"){
                        self.playVideo();
                    }
                    else {
                        speedTestDone = true;
                    }
                },

                playVideo : function(){
                	accedo.console.log('playVideo');
                	media.stop();//stop the video if there is any bptv acting behind
                    media.init({
                        setMediaSource:false
                    });

                    media.registerCurrentTimeCallback(function(time,formattedTime,percentage){
                        self.currentTimeCallback(time,formattedTime,percentage);
                    });
                    media.registerStatusChangeCallback(function(status){
                        self.statusChangeCallback(status);
                    });

                    media.setMediaURL(config.speedTestVideoUrl, {
                        widevine : true,
                        drm_url : config.speedTestDrmUrl,
                        opt_data : 'ip:,streamid:,deviceid:'+accedo.device.manager.identification.getDeviceId()+',optdata:'+telstra.api.manager.movies.authToken,
                        device_id : accedo.device.manager.identification.getUniqueID(),
                        portal : "telstra", 
                        speedtest: true
                    });

                    media.setFullscreen(-5000, 0);
                    var self = this;
                    accedo.utils.fn.delay(function() {
                    	media.play();
                    }, 1);
                },

                currentTimeCallback : function(time,formattedTime,percentage){

                    counter++;
                	accedo.console.log('currentTimeCallback ['+ counter + ']; formattedTime: ' + formattedTime);

                    if(counter > 4 && !speedTestDone){
                        var bitrates = media.getBitrates();
                        self.updateBar(bitrates);
                    }
                },

                statusChangeCallback : function(status){
                	accedo.console.log('statusChangeCallback ' + status);
                    switch(status){
                        case "playing" :
                            break;
                        case "error" :
                            self.updateBar(false);
                            break;
                    }
                },

                updateBar : function(bitrates){
                    //media.stop();
                    self.mediaDeinit();
                    speedTestDone = true;
                    var currentBit, availableBit, showError = false;
                    
                    //test
                    //bitrates = false;
                     
                    if(bitrates == false){
                        currentBit = 1.1;
                        availableBit = [409616,509576,1009648,2009752,3009832,3509848,4009880,4509936,5009888,5516416,6016352];
                        showError = true;
                    }else{
                        accedo.console.log("currentBitrate: " + bitrates.currentBitrate);
                        currentBit = bitrates.currentBitrate  / (1000 * 1000);
                        availableBit = bitrates.availableBitrates;
                    }
                    
                    accedo.console.log("updateBar: " + currentBit + " availableBit: " + accedo.utils.object.toJSON(availableBit));

                    closeButton.setText('Close');
                    
                    var maximumValue = 0;

                    for(var i = 0; i < availableBit.length; i ++){
                        var bitrate = parseInt(availableBit[i]);
                        if(bitrate > maximumValue){
                            maximumValue = bitrate;
                        }
                    }

                    maximumValue = maximumValue /  (1000 * 1000);
                    if (maximumValue > config.speedTestMaxLimit){
                    	//currentBit = (currentBit/maximumValue) * config.speedTestMaxLimit;
                    	maximumValue = config.speedTestMaxLimit; 
                    }
                    
                    if (currentBit > config.speedTestMaxLimit){
                    	currentBit = config.speedTestMaxLimit; 
                    }
                    
                    if (maximumValue < currentBit){
                    	maximumValue = currentBit;
                    }
                    
                    if (maximumValue < config.speedTestMaxLimit){
                    	maximumValue = config.speedTestMaxLimit;
                    }

                    //update background
                    if (currentBit < config.speedTestRedLimit){
                        labelContact.setText("If you are experiencing further connection difficulties <br> contact your ISP or BigPond on 13 POND (13 7663).");
                        popupMain.getRoot().removeClass('long');

                    }else{
                        labelContact.setText("To upgrade your internet connection contact <br> BigPond on 1800 997834.");
                        popupMain.getRoot().addClass('long');
                        self.get('componentPanel').getRoot().addClass('success');
                        self.get('buttonPanel').getRoot().addClass('success');
                        labelContact.getRoot().setStyle({
                            'top' : '295px'
                        });
                    }
                    
                    //update result icon
                    if (currentBit < config.speedTestYellowStart){
                        resultIcon.setSrc('images/module/bpvod/pages/SS-CL-1_0_ConnectionInfo/icon_no.png');
                    }else if (currentBit < config.speedTestSDLimit){
                        resultIcon.setSrc('images/module/bpvod/pages/SS-CL-1_0_ConnectionInfo/icon_ok.png');
                    }else{
                        resultIcon.setSrc('images/module/bpvod/pages/SS-CL-1_0_ConnectionInfo/icon_yes.png');
                    }

                    label1.getRoot().setStyle({
                        'left' : '101px',
                        'top' : '123px'
                    })

                    //update message content
                    if (currentBit < config.speedTestRedLimit){
                        label1.getRoot().setStyle({
                            'left' : '100px',
                            'top' : '136px',
                            'width' : '275px'
                        })
                        label1.setText("We cannot establish a connection to carry out a speed test.");
                    }else if (currentBit < config.speedTestYellowStart){
                        label1.setText("Your connection is below the recommended speed.");
                        label2.setText("You will not be able to browse and watch movies due to either:");
                        label4.setText("<li>your internet plan speed</li>");
                        label5.setText("<li>other household internet activity</li>");
                        label6.setText("<li>external network congestion</li>");
                    }else if(currentBit < config.speedTestSDLimit){
                        label1.setText("Your connection is below the recommended speed.");
                        label2.setText("You may have difficulty browsing and watching <br> movies due to:");
                        label4.setText("<li>your internet plan speed</li>");
                        label5.setText("<li>other household internet activity</li>");
                        label6.setText("<li>external network congestion</li>");
                    }else if(currentBit < config.speedTestHDLimit){
                        label1.setText("Congratulations.");
                        label2.setText("Your connection meets the recommended speed for <br> browsing and watching Standard Definition (SD) movies.");
                        label3.setText("Note: other household internet activity may <br> affect your experience.");
                    }else{
                        label1.setText("Congratulations.");
                        label2.setText("Your connection meets the recommended speed for <br> browsing and watching High Definition (HD) movies.");
                        label3.setText("Note: other household internet activity may <br> affect your experience.");
                    }

                    //update bar
                    if(currentBit < config.speedTestRedLimit){
                        barBase.setSrc("images/module/bpvod/pages/SS-CL-1_0_ConnectionInfo/ConnectBar_Error.png");
                        barContainer.hide();
                        arrowContainer.hide();
                    }else{
                        barBase.setSrc("images/module/bpvod/pages/SS-CL-1_0_ConnectionInfo/ConnectBar_base.png");
                        barContainer.show();
                        arrowContainer.show();

                        var redWidth, yellowWidth, greenWidth;
                        var startConfig = 1;
                        maximumValue -= startConfig;

                        if(currentBit < config.speedTestYellowStart){
                            redWidth = 0.5;
                            yellowWidth = 0;
                            greenWidth = 0;
                        }else{
                            redWidth = (currentBit > config.speedTestRedLimit ? config.speedTestRedLimit - 0.05 : currentBit);
                            yellowWidth = (currentBit > config.speedTestYellowLimit ? config.speedTestYellowLimit -0.05 : currentBit) - redWidth;
                            if(yellowWidth < 0){
                                yellowWidth = 0;
                            }
                            greenWidth = currentBit - redWidth - yellowWidth;
                            if(greenWidth < 0){
                                greenWidth = 0;
                            }

                            //set to one color in BPMV1.1
                            if(greenWidth > 0){
                                greenWidth = currentBit - startConfig;
                                redWidth = 0;
                                yellowWidth = 0;
                            }else if(yellowWidth > 0){
                                yellowWidth = currentBit - startConfig;
                                redWidth = 0;
                            }
                        }

                        var containerWidth = barContainer.getRoot().getHTMLElement().scrollWidth;

                        var hdPos = (config.speedTestHDLimit > maximumValue ) ? null : Math.round(containerWidth * config.speedTestHDLimit / maximumValue) -8;
                        var sdPos = (config.speedTestSDLimit > maximumValue ) ? null : Math.round(containerWidth * config.speedTestSDLimit / maximumValue) -8;

                        accedo.console.log(" === maximumValue: " + maximumValue + " redWidth: " + redWidth + " yellowWidth: " + yellowWidth + " greenWidth: " + greenWidth + " hdPos: " + hdPos + " sdPos: " + sdPos);

                        barRed.getRoot().setStyle({
                            'width' : Math.floor(containerWidth * redWidth / maximumValue)-5 + "px"
                        });
                        barYellow.getRoot().setStyle({
                            'width' : Math.floor(containerWidth * yellowWidth / maximumValue)-5 + "px"
                        });
                        barGreen.getRoot().setStyle({
                            'width' : Math.floor(containerWidth * greenWidth / maximumValue)-5 + "px"
                        });

                        if(greenWidth > 0){
                            barGreenEnd.getRoot().setStyle({
                                'width' : '5px'
                            });
                        }else if(yellowWidth > 0){
                            barYellowEnd.getRoot().setStyle({
                                'width' : '5px'
                            });
                        }else{
                            barRedEnd.getRoot().setStyle({
                                'width' : '5px'
                            });
                        }
                        
                        if (showError){
                        	barRed.getRoot().setStyle({
                                'width' : '400px'
                            });
                            barRed.getRoot().addClass('error');
                            errorLabel.show();
                            arrowContainer.hide();
                            barRedEnd.hide();
                        }
                    }
                },

                onKeyBack : function(){
                    if(!speedTestDone){
                        self.stopTestAndClose();
                    }else{
                        self.close();
                    }
                },
                
                stopTestAndClose : function(){
                	self.mediaDeinit();
                	self.close();
                },
                
                mediaDeinit : function(){
                	media.stop();
                	media.unregisterStatusChangeCallback();
                    media.unregisterCurrentTimeCallback();
                    media.deinit();
                }
            });

            myPubObj.init();
            return myPubObj;
        }
    });