/**
 * @fileOverview
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.signInCongraController", [
    'telstra.api.usage',
    'telstra.sua.module.bpvod.views.signInCongraView'],
    function(){
        return function(opts) {

            // Shorten for namespacing
            var utils = telstra.sua.module.bpvod.utils,
            views = telstra.sua.module.bpvod.views,
            movieButton,
            redeemButton;

            var myPubObj = accedo.utils.object.extend(accedo.ui.controller(opts), {

                onCreate: function() {
                    var self = this;

                    //Set the view
                    this.setView(views.signInCongraView);

                    movieButton = this.get('movieButton');
                    redeemButton = this.get('redeemButton');

                    if (opts.context.joinSteps) {
                        //this.get('signInTitle').hide();

                        telstra.api.usage.log("bpvod", "join_now_complete", {
                            logMessage: "bigpond movies:join:sign up complete"
                        });
                    }

                    movieButton.addEventListener('click',function(){
                        utils.moveToBigPondMovie(self);
                    });

                    redeemButton.addEventListener('click',function(){
                        utils.setDispatchController(self, 'myAccountRedeemVoucherController', {}, true);
                    });

                    accedo.utils.fn.delay(function(){
                        accedo.focus.manager.requestFocus(movieButton);
                    },0.5);
                },

                onKeyBack : function(){
                    telstra.sua.core.components.infoMessageBar.addBar({
                        text : 'You are already a BigPond Movies Member.',
                        colour : 'green',
                        id : 'alreadyMember'
                    });
                }
            });

            return myPubObj;
        };
    });