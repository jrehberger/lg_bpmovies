/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    'telstra.sua.module.bpvod.controllers.myAccountCardTypeController',
    ['accedo.utils.object',
    'accedo.ui.controller',
    'accedo.focus.manager',
    'telstra.api.manager',
    'telstra.api.usage',
    'telstra.sua.core.components.infoMessageBar',
    'telstra.sua.core.components.loadingOverlay',
    "telstra.sua.module.bpvod.views.myAccountCardTypeView"],
    function(){
        return function(opts){

            var utils = telstra.sua.module.bpvod.utils,
            views = telstra.sua.module.bpvod.views,
            optionsList = [],
            self, option1, option2, option3, continueButton, cancelButton, creditCardInfo;

            return accedo.utils.object.extend(accedo.ui.controller(opts), {

                onCreate: function() {
                    self = this;
                    creditCardInfo = telstra.sua.module.bpvod.config.creditCardInfo;
                    creditCardInfo.cardType = telstra.api.manager.movies.accountInfo.creditcard.cardtype;

                    //Set the view
                    self.setView(views.myAccountCardTypeView);
                    
                    option1 = self.get('option1');
                    option2 = self.get('option2');
                    option3 = self.get('option3');
                    continueButton = self.get('continueButton');
                    cancelButton = self.get('cancelButton');

                    if (telstra.api.manager.movies.accountInfo.creditcard.cardtype == "visa")
                        option1.getRoot().addClass('select');
                    else if (telstra.api.manager.movies.accountInfo.creditcard.cardtype == "mastercard")
                        option2.getRoot().addClass('select');
                    else
                        option3.getRoot().addClass('select');

                    option1.addEventListener('click', function() {
                        option1.getRoot().addClass('select');
                        option2.getRoot().removeClass('select');
                        option3.getRoot().removeClass('select');
                        creditCardInfo.cardType = "visa";
                        accedo.focus.manager.requestFocus(continueButton);
                    });

                    option2.addEventListener('click', function() {
                        option1.getRoot().removeClass('select');
                        option2.getRoot().addClass('select');
                        option3.getRoot().removeClass('select');
                        creditCardInfo.cardType = "mastercard";
                        accedo.focus.manager.requestFocus(continueButton);
                    });

                    option3.addEventListener('click', function() {
                        option1.getRoot().removeClass('select');
                        option2.getRoot().removeClass('select');
                        option3.getRoot().addClass('select');
                        creditCardInfo.cardType = "amex";
                        accedo.focus.manager.requestFocus(continueButton);
                    });

                    continueButton.onFocus = function() {
                        option3.setOption('nextDown', 'continueButton');
                    };
                    
                    continueButton.addEventListener('click', function() {
                        self.saveAction();
                    });

                    cancelButton.onFocus = function() {
                        option3.setOption('nextDown', 'cancelButton');
                    };

                    cancelButton.addEventListener('click', function() {
                        self.onKeyBack();
                    });

                    optionsList.push(
						{
							type: "red",
							title: "Help"
						}
					);
					//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);

                    accedo.focus.manager.requestFocus(option1);
                },

                saveAction: function() {
                    utils.setDispatchController(self, 'myAccountCardDetailsController', {}, false);
                },

                onKeyRed : function() {
                    telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 19949);
                },

                onKeyBack: function() {
                    if (telstra.sua.core.components.loadingOverlay.isLoadingOn())
                        return;

                    option3.setOption('nextDown', 'saveChangesButton');
                    self.dispatchEvent('telstra:core:historyBack');
                }
            });
        }
    });