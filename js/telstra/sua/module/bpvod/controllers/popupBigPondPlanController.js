/**
 * @fileOverview popupBigPondPlanController
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
 */

accedo.define("telstra.sua.module.bpvod.controllers.popupBigPondPlanController", [
    'telstra.sua.module.bpvod.views.popupBigPondPlanView',
    "telstra.sua.module.bpvod.config",
    "accedo.utils.object"],
    function() {

        return function(opts) {

            var myPubObj,
            self,
            popupMain = null,
            okButton = null;

            myPubObj = accedo.utils.object.extend(accedo.ui.popup(opts), {

                init : function(){

                    self = this;
                    this.setView(telstra.sua.module.bpvod.views.popupBigPondPlanView);

                    popupMain = this.get('popupBg').getById('popupMain');
                    if (telstra.sua.module.bpvod.config.isProduction)
                        popupMain.getRoot().addClass('production');
                    
                    okButton = popupMain.getById('okButton');
                    okButton.addEventListener('click', function() {
                        self.close();
                    });

                    accedo.focus.manager.requestFocus(okButton);
                },

                onKeyBack : function(){
                    self.close();
                }
            });

            myPubObj.init();
            return myPubObj;
        }
    });