/**
 * @fileOverview
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 *
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.signInStep5SetPinController", [
    // dependency
    "telstra.api.usage",
    "telstra.sua.module.bpvod.views.signInStep5SetPinView",
    "telstra.sua.module.bpvod.controllers.signInTemplateController"],
    function(){

        return function(opts){

            // Shorten for namespacing
            var utils = telstra.sua.module.bpvod.utils,
            pinField1,
            pinField2,
            self;


            var myPubObj = accedo.utils.object.extend(telstra.sua.module.bpvod.controllers.signInTemplateController(opts), {

                onCreate : function(){
                    self = this;

                    this.callcode = 19927;

                    this.init(telstra.sua.module.bpvod.views.signInStep5SetPinView);

                    this.continueButton.setOption("nextUp",'pinField2');
                    this.backButton.setOption("nextUp",'pinField2');

                    pinField1 = this.get('pinField1');
                    pinField2 = this.get('pinField2');

                    pinField1.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(pinField2);
                    }

                    pinField2.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(self.continueButton);
                    }

                    accedo.utils.fn.delay(function(){
                        accedo.focus.manager.requestFocus(pinField1);
                    },0.5);

                },

                continueButtonAction : function(){

                    var pinNumber = pinField1.getCleanedResultStr();
                    var pinNumber2 = pinField2.getCleanedResultStr();
                        
                    if(!utils.checkIdentical(pinNumber, pinNumber2)){
                        pinField1.setString("");
                        pinField2.setString("");
                        accedo.focus.manager.requestFocus(pinField1);

                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : "Please enter the same 4 digit PIN in both fields.",
                            colour : "red",
                            id : 'enterSameDigits'
                        });
                        return;
                    }

                    if(pinNumber == null || accedo.utils.object.isUndefined(pinNumber) || pinNumber == "" || pinNumber.length < 4){
                        pinField1.setString("");
                        pinField2.setString("");
                        accedo.focus.manager.requestFocus(pinField1);
                        
                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : "Your PIN must be 4 digit long.",
                            colour : "red",
                            id : 'enterFourDigits'
                        });
                        return;
                    }

                    if(!utils.isNumber(pinNumber)){
                        pinField1.setString("");
                        pinField2.setString("");
                        accedo.focus.manager.requestFocus(pinField1);
                        
                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : "Please enter numbers only.",
                            colour : "red",
                            id : 'enterNumbersOnly'
                        });
                        return;
                    }

                    self.param.accountpin = pinNumber;
                    utils.setDispatchController(myPubObj, 'signInStep6PinSettingController', self.param, true);
                },

                backButtonAction : function(){
                    utils.setDispatchController(myPubObj, 'signInController', {}, true);
                }

            });

            return myPubObj;

        }

    });