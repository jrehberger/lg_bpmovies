/**
 * @fileOverview popupOptionsController
 * @author <a href="mailto:victor.leung@accedo.tv">Victor Leung</a>
 */

accedo.define("telstra.sua.module.bpvod.controllers.popupOptionsController", [
    "accedo.utils.object",
    "telstra.sua.module.bpvod.views.popupOptionsView"],
    function() {

        /**
         * @class
         * @extends accedo.ui.popup
         */
        return function(opts) {

            var self,
            utils = telstra.sua.module.bpvod.utils,
            views = telstra.sua.module.bpvod.views,
            popupOptions, optionsList,

            callbackArray = {
                "Join Now or Sign in": function() {
                    telstra.api.usage.log("overlay", "", {
                        logMessage: "bigpond movies: options overlay logged out: join now or sign in"
                    });

                    utils.setDispatchController(opts.controller, 'signInORjoinNowController', {}, false);
                },
                "Sign out": function() {
                    telstra.api.usage.log("overlay", "", {
                        logMessage: "bigpond movies: options overlay logged in: sign out"
                    });

                    var popupYesNo = accedo.ui.popupYesNo({
                        headline_css: 'header',
                        headline_txt: 'Sign out',
                        msg_css: 'innerLabel',
                        msg_txt: "Are you sure you want to sign out?<br/><br/>"+
                        "If you sign out you will be required to sign in using "+
                        "your BigPond Movies Downloads email address and "+
                        "password to rent or watch movies and tv shows.",
                        yesBtn_css: 'yesButton button medium',
                        yesBtn_txt: 'Sign out',
                        //yesBtn_width: '204px',
                        noBtn_css: 'noButton button medium',
                        noBtn_txt: "Cancel",
                        //noBtn_width: '204px'
                    });

                    popupYesNo.addEventListener('accedo:popupYesNo:onConfirm', function() {
                        utils.signOutAction(function(){
                            utils.setDispatchController(opts.controller, 'mainController', {}, true);
                            accedo.utils.fn.delay(function(){
                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : "You have been signed out. Returning you to the Movies Store main menu.",
                                    colour : "green",
                                    id : 'signOutMessage'
                                });
                            }, 1);
                        });
                    });
                },
                "Forgotten PIN?": function() {
                    telstra.api.usage.log("overlay", "", {
                        logMessage: "bigpond movies: options overlay logged in: forgotten pin"
                    });

                    utils.setDispatchController(opts.controller, 'forgotPinController', {}, false);
                },
                "Connection info": function() {
                    telstra.api.usage.log("overlay", "", {
                        logMessage: "bigpond movies: options overlay logged " + (telstra.sua.module.bpvod.session.isSignedIn ? "in" : "out") + ": connection info"
                    });

                    telstra.sua.module.bpvod.controllers.popupSpeedTestController();
                },
                "Unmetered?": function() {
                    telstra.api.usage.log("overlay", "", {
                        logMessage: "bigpond movies: options overlay logged " + (telstra.sua.module.bpvod.session.isSignedIn ? "in" : "out") + ": unmetered"
                    });

                    telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 18648, "Unmetered");
                },
                "BigPond Plans": function() {
                    telstra.api.usage.log("overlay", "", {
                        logMessage: "bigpond movies: options overlay logged " + (telstra.sua.module.bpvod.session.isSignedIn ? "in" : "out") + ": bigpond plans"
                    });

                    telstra.sua.module.bpvod.controllers.popupBigPondPlanController();
                },
                "Classifications": function() {
                    telstra.api.usage.log("overlay", "", {
                        logMessage: "bigpond movies: options overlay logged " + (telstra.sua.module.bpvod.session.isSignedIn ? "in" : "out") + ": classifications"
                    });

                    telstra.sua.module.bpvod.controllers.popupClassificationController();
                }
            },
           
            obj = accedo.utils.object.extend(accedo.ui.popup(opts), {
                
                init : function(){
                    telstra.api.usage.log("overlay", "", {
                        logMessage: "bigpond movies:options overlay logged " + (telstra.sua.module.bpvod.session.isSignedIn ? "in" : "out")
                    });
                    
                    self = this;
                    this.setView(views.popupOptionsView);

                    var ds = accedo.data.ds();
                    ds.appendData(opts.optionsList);

                    popupOptions = this.get('popupOptions');
                    optionsList = this.get('popupOptions_optionsList');

                    if (opts.optionsList.length == 6)
                        popupOptions.getRoot().addClass("big");
                    else if (opts.optionsList.length == 5)
                        popupOptions.getRoot().addClass("middle")
                    
                    optionsList.setDisplayHelper(function(components, data) {
                        components.setText(data.title);

                        components.addEventListener('click', function() {
                            if (data.callback) {
                                self.onKeyBack();
                                data.callback();
                            }
                            else if (callbackArray[data.title]) {
                                self.onKeyBack();
                                callbackArray[data.title]();
                            }
                        });
                    });

                    optionsList.setDatasource(ds);
                    
                    this.get('homeButton').addEventListener('click',function(){
                        self.onKeyBack();
                        
                        if (opts.backToHomeCallback)
                            opts.backToHomeCallback();
                        else {
                            opts.controller && utils.backToUAHome(opts.controller);
                        }
                    });

                    this.get('homeButton').onFocus = function() {
                        optionsList.getSelection().getParent().removeClass("selected");
                        optionsList.setBorderNextLeftUp("homeButton");
                        optionsList.setBorderNextRightDown("homeButton");
                    };

                    this.get('homeButton').setOption("nextUp", function() {
                        optionsList.getSelection().getParent().addClass("selected");
                        optionsList.setSelectedIndex(opts.optionsList.length - 1);
                        accedo.focus.manager.requestFocus(optionsList);
                    });

                    this.get('homeButton').setOption("nextDown", function() {
                        optionsList.getSelection().getParent().addClass("selected");
                        optionsList.setSelectedIndex(0);
                        accedo.focus.manager.requestFocus(optionsList);
                    });
                    
                    this.get('closeButton').addEventListener('click',function(){
                        self.onKeyBack();
                    });

                    this.get('closeButton').onFocus = function() {
                        optionsList.getSelection().getParent().removeClass("selected");
                        optionsList.setBorderNextLeftUp("closeButton");
                        optionsList.setBorderNextRightDown("closeButton");
                    };

                    this.get('closeButton').setOption("nextUp", function() {
                        optionsList.getSelection().getParent().addClass("selected");
                        optionsList.setSelectedIndex(opts.optionsList.length - 1);
                        accedo.focus.manager.requestFocus(optionsList);
                    });

                    this.get('closeButton').setOption("nextDown", function() {
                        optionsList.getSelection().getParent().addClass("selected");
                        optionsList.setSelectedIndex(0);
                        accedo.focus.manager.requestFocus(optionsList);
                    });

                    accedo.focus.manager.requestFocus(optionsList);
                },

                onKeyBack : function(){
                    optionsList.setOption("nextDown", "homeButton");
                    self.close();
                }
            });

            obj.init();
            return obj;
        }
    });