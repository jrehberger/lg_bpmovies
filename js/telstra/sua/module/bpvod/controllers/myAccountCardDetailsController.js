/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    'telstra.sua.module.bpvod.controllers.myAccountCardDetailsController',
    ['accedo.utils.object',
    'accedo.ui.controller',
    'accedo.focus.manager',
    'telstra.api.manager',
    'telstra.sua.core.components.infoMessageBar',
    'telstra.sua.core.components.loadingOverlay',
    'telstra.sua.module.bpvod.views.myAccountCardDetailsType1View',
    'telstra.sua.module.bpvod.views.myAccountCardDetailsType2View'],
    function(){
        return function(opts){

            var utils = telstra.sua.module.bpvod.utils,
            views = telstra.sua.module.bpvod.views,
            optionsList = [],
            self, ccInput1, ccInput2, ccInput3, ccInput4, expDateInput1, expDateInput2, cvcInput1, saveChangeButton, backButton, creditCardInfo;

            return accedo.utils.object.extend(accedo.ui.controller(opts), {

                onCreate: function() {
                    telstra.api.usage.log("bpvod", "my_account_payment_options", {
                        logMessage: "bigpond movies:my account:payment options"
                    });

                    telstra.sua.core.main.mainNavBar.setMenuButton("HELP");
                    
                    self = this;
                    creditCardInfo = telstra.sua.module.bpvod.config.creditCardInfo;

                    //Set the view
                    if (telstra.sua.module.bpvod.config.creditCardInfo.cardType != "amex") {
                        self.setView(views.myAccountCardDetailsType1View);

                        ccInput4 = self.get('ccInput4');
                    }
                    else {
                        self.setView(views.myAccountCardDetailsType2View);
                    }

                    ccInput1 = self.get('ccInput1');
                    ccInput2 = self.get('ccInput2');
                    ccInput3 = self.get('ccInput3');
                    expDateInput1 = self.get('expDateInput1');
                    expDateInput2 = self.get('expDateInput2');
                    cvcInput1 = self.get('cvcInput1');
                    saveChangeButton = self.get('saveChangeButton');
                    backButton = self.get('backButton');
                    
                    ccInput1.addEventListener('accedo:keyboard:onFocus', function() {
                        expDateInput1.setOption('nextUp', 'ccInput1');
                        expDateInput2.setOption('nextUp', 'ccInput1');
                    });

                    ccInput2.addEventListener('accedo:keyboard:onFocus', function() {
                        expDateInput1.setOption('nextUp', 'ccInput2');
                        expDateInput2.setOption('nextUp', 'ccInput2');
                    });

                    ccInput3.addEventListener('accedo:keyboard:onFocus', function() {
                        expDateInput1.setOption('nextUp', 'ccInput3');
                        expDateInput2.setOption('nextUp', 'ccInput3');
                    });

                    expDateInput1.addEventListener('accedo:keyboard:onFocus', function() {
                        ccInput1.setOption('nextDown', 'expDateInput1');
                        ccInput2.setOption('nextDown', 'expDateInput1');
                        ccInput3.setOption('nextDown', 'expDateInput1');
                        cvcInput1.setOption('nextUp', 'expDateInput1');
                    });

                    expDateInput2.addEventListener('accedo:keyboard:onFocus', function() {
                        ccInput1.setOption('nextDown', 'expDateInput2');
                        ccInput2.setOption('nextDown', 'expDateInput2');
                        ccInput3.setOption('nextDown', 'expDateInput2');
                        cvcInput1.setOption('nextUp', 'expDateInput2');
                    });

                    if (creditCardInfo.cardType != "amex") {
                        ccInput4.addEventListener('accedo:keyboard:onFocus', function() {
                            expDateInput1.setOption('nextUp', 'ccInput4');
                            expDateInput2.setOption('nextUp', 'ccInput4');
                        });

                        expDateInput1.addEventListener('accedo:keyboard:onFocus', function() {
                            ccInput4.setOption('nextDown', 'expDateInput1');
                        });

                        expDateInput2.addEventListener('accedo:keyboard:onFocus', function() {
                            ccInput4.setOption('nextDown', 'expDateInput2');
                        });
                    }

                    saveChangeButton.setOption("nextUp", 'cvcInput1');

                    saveChangeButton.onFocus = function() {
                        cvcInput1.setOption('nextDown', 'saveChangeButton');
                    };

                    saveChangeButton.addEventListener('click', function() {
                        self.saveAction();
                    });

                    backButton.setOption("nextUp", 'cvcInput1');

                    backButton.onFocus = function() {
                        cvcInput1.setOption('nextDown', 'backButton');
                    };

                    backButton.addEventListener('click', function() {
                        self.onKeyBack();
                    });

                    ccInput1.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(ccInput2);
                    };
                    ccInput2.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(ccInput3);
                    };

                    if (creditCardInfo.cardType != "amex") {
                        ccInput3.maxLengthCallback = function() {
                            accedo.focus.manager.requestFocus(ccInput4);
                        };
                        ccInput4.maxLengthCallback = function() {
                            accedo.focus.manager.requestFocus(expDateInput1);
                        };
                    }
                    else {
                        ccInput3.maxLengthCallback = function() {
                            accedo.focus.manager.requestFocus(expDateInput1);
                        };
                    }

                    expDateInput1.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(expDateInput2);
                    };
                    expDateInput2.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(cvcInput1);
                    };
                    cvcInput1.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(saveChangeButton);
                    };
                    
                    optionsList.push(
						{
							type: "red",
							title: "Help"
						}
					);
					//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);

                    accedo.focus.manager.requestFocus(ccInput1);
                },

                saveAction: function() {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();

                    if (creditCardInfo.cardType == "amex") {
                        if (ccInput1.getCleanedResultStr().length == 0 || ccInput2.getCleanedResultStr().length == 0 || ccInput3.getCleanedResultStr().length == 0 || expDateInput1.getCleanedResultStr().length == 0 || expDateInput2.getCleanedResultStr().length == 0 || cvcInput1.getCleanedResultStr().length == 0) {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();

                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : 'Please complete all of the form fields',
                                colour : "red",
                                id : 'pleaseCompleteAllFields'
                            });
                        } else if (ccInput1.getCleanedResultStr().length < 4 || ccInput2.getCleanedResultStr().length < 6 || ccInput3.getCleanedResultStr().length < 5 || expDateInput1.getCleanedResultStr().length < 2 || expDateInput2.getCleanedResultStr().length < 2 || cvcInput1.getCleanedResultStr().length < 4) {
                            if (ccInput1.getCleanedResultStr().length < 4 || ccInput2.getCleanedResultStr().length < 6 || ccInput3.getCleanedResultStr().length < 5) {
                                telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : 'The credit card information you entered is not valid. Please re-enter and try again',
                                    colour : "red",
                                    id : 'notValidCCInfo'
                                });
                            } else if (expDateInput1.getCleanedResultStr().length < 2 || expDateInput2.getCleanedResultStr().length < 2) {
                                telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : 'Invalid Year or Month',
                                    colour : "red",
                                    id : 'invalidYearMonth'
                                });
                            } else if (cvcInput1.getCleanedResultStr().length < 4) {
                                telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : 'CVC number invalid. Please re-enter your CVC number.',
                                    colour : "red",
                                    id : 'invalidCVC'
                                });
                            }
                        } else {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();

                            if (utils.checkCCExpDate(expDateInput1.getCleanedResultStr(), expDateInput2.getCleanedResultStr())) {
                                creditCardInfo.cardNumber.cc1 = ccInput1.getCleanedResultStr();
                                creditCardInfo.cardNumber.cc2 = ccInput2.getCleanedResultStr();
                                creditCardInfo.cardNumber.cc3 = ccInput3.getCleanedResultStr();
                                creditCardInfo.expiryDate.month = expDateInput1.getCleanedResultStr();
                                creditCardInfo.expiryDate.year = expDateInput2.getCleanedResultStr();
                                creditCardInfo.securityCode = cvcInput1.getCleanedResultStr();
                                self.updateCC();
                            }
                        }
                    }
                    // visa or master card
                    else {
                        if (ccInput1.getCleanedResultStr().length == 0 || ccInput2.getCleanedResultStr().length == 0 || ccInput3.getCleanedResultStr().length == 0 || ccInput4.getCleanedResultStr().length == 0 || expDateInput1.getCleanedResultStr().length == 0 || expDateInput2.getCleanedResultStr().length == 0 || cvcInput1.getCleanedResultStr().length == 0) {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();

                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : 'Please complete all of the form fields',
                                colour : "red",
                                id : 'pleaseCompleteAllFields'
                            });
                        } else if (ccInput1.getCleanedResultStr().length < 4 || ccInput2.getCleanedResultStr().length < 4 || ccInput3.getCleanedResultStr().length < 4 || ccInput4.getCleanedResultStr().length < 4 || expDateInput1.getCleanedResultStr().length < 2 || expDateInput2.getCleanedResultStr().length < 2 || cvcInput1.getCleanedResultStr().length < 3) {
                            if (ccInput1.getCleanedResultStr().length < 4 || ccInput2.getCleanedResultStr().length < 4 || ccInput3.getCleanedResultStr().length < 4 || ccInput4.getCleanedResultStr().length < 4) {
                                telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : 'The credit card information you entered is not valid. Please re-enter and try again',
                                    colour : "red",
                                    id : 'notValidCCInfo'
                                });
                            } else if (expDateInput1.getCleanedResultStr().length < 2 || expDateInput2.getCleanedResultStr().length < 2) {
                                telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : 'Invalid Year or Month',
                                    colour : "red",
                                    id : 'invalidYearMonth'
                                });
                            } else if (cvcInput1.getCleanedResultStr().length < 4) {
                                telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : 'CVC number invalid. Please re-enter your CVC number.',
                                    colour : "red",
                                    id : 'invalidCVC'
                                });
                            }
                        }
                        else {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();

                            if (utils.checkCCExpDate(expDateInput1.getCleanedResultStr(), expDateInput2.getCleanedResultStr())) {
                                creditCardInfo.cardNumber.cc1 = ccInput1.getCleanedResultStr();
                                creditCardInfo.cardNumber.cc2 = ccInput2.getCleanedResultStr();
                                creditCardInfo.cardNumber.cc3 = ccInput3.getCleanedResultStr();
                                creditCardInfo.cardNumber.cc4 = ccInput4.getCleanedResultStr();
                                creditCardInfo.expiryDate.month = expDateInput1.getCleanedResultStr();
                                creditCardInfo.expiryDate.year = expDateInput2.getCleanedResultStr();
                                creditCardInfo.securityCode = cvcInput1.getCleanedResultStr();
                                self.updateCC();
                            }
                        }
                    }
                },

                updateCC : function () {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();

                    var mergedCreditCardInfo = {
                        cardType: creditCardInfo.cardType,
                        cardNumber: (creditCardInfo.cardType == "amex" ? creditCardInfo.cardNumber.cc1 + creditCardInfo.cardNumber.cc2 + creditCardInfo.cardNumber.cc3 : creditCardInfo.cardNumber.cc1 + creditCardInfo.cardNumber.cc2 + creditCardInfo.cardNumber.cc3 + creditCardInfo.cardNumber.cc4),
                        expiryDate: "20" + creditCardInfo.expiryDate.year + "-" + creditCardInfo.expiryDate.month,
                        securityCode: creditCardInfo.securityCode
                    }
                    
                    telstra.api.manager.movies.registerCreditCard(mergedCreditCardInfo, {
                        onSuccess: function () {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();

                            telstra.api.manager.movies.accountInfo.creditcard.cardtype = mergedCreditCardInfo.cardType;
                            telstra.api.manager.movies.accountInfo.creditcard.maskedcardnumber = mergedCreditCardInfo.cardNumber.substr(0,6) + (mergedCreditCardInfo.cardType != "amex" ? "*******" + mergedCreditCardInfo.cardNumber.substr(13,3) : "******" + mergedCreditCardInfo.cardNumber.substr(12,3));
                            telstra.api.manager.movies.accountInfo.creditcard.expirydate = mergedCreditCardInfo.expiryDate;

                            self.clearData();
                            self.resetNavigation();
                            
                            utils.setDispatchController(self, 'myAccountMainMenuController', {
                                selectedIndex: 1
                            }, true);

                            accedo.utils.fn.delay(function() {
                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : 'Your Credit Card Details have been changed.',
                                    colour : 'green',
                                    id : 'cardDetailsChanged'
                                });
                            }, 0.2);
                        },
                        onFailure: function(json) {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();

                            utils.errorMessageokPopup(json);
                        },
                        onException: function () {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();
                        }
                    });
                },

                clearData : function() {
                    creditCardInfo.cardNumber.cc1 = null;
                    creditCardInfo.cardNumber.cc2 = null;
                    creditCardInfo.cardNumber.cc3 = null;
                    creditCardInfo.cardNumber.cc4 = null;
                    creditCardInfo.expiryDate.month = null;
                    creditCardInfo.expiryDate.year = null;
                    creditCardInfo.securityCode = null;
                },

                resetNavigation : function() {
                    ccInput1.setOption('nextDown', 'expDateInput1');
                    expDateInput1.setOption('nextUp', 'ccInput1');
                    cvcInput1.setOption('nextUp', 'expDateInput1');
                    cvcInput1.setOption('nextDown', 'saveChangeButton');
                },

                onKeyRed : function() {
                    telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 19949);
                },

                onKeyBack: function() {
                    if (telstra.sua.core.components.loadingOverlay.isLoadingOn())
                        return;

                    self.resetNavigation();
                    self.dispatchEvent('telstra:core:historyBack');
                }
            });
        }
    });