/**
 * @fileOverview searchPageResultController
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 * @author <a href="mailto:roy.chan@accedobroadband.com">Roy Chan</a>
 *
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.searchResultPageController", [
    // dependency
    'accedo.app',
    "accedo.utils.object", "accedo.utils.array", "accedo.utils.fn",
    "accedo.ui.controller",
    "accedo.focus.manager",
    "accedo.focus.managerMouse",
    "accedo.device.manager",
    "telstra.api.manager",
    "telstra.api.usage",
    "telstra.sua.module.bpvod.config",
    "telstra.sua.module.bpvod.views.searchResultPageView",
    "telstra.sua.module.bpvod.utils",
    "telstra.sua.module.bpvod.controllers.popupClassificationController"],
    function(){
        return function(opts) {
            // Logging if this view created
            accedo.console.log("Create bpvod.searchResultPageController instance");

            // Some local elements and variables
            var list, upScrollerBtn, downScrollerBtn,
                
            //search parameters got from context
            searchInput, searchType, searchBy, search_result_ds, currentMediaType,
                
            //get UI component references
            searchInputWord,
            searchInputInfo,
            headerMovieTitle,
            headerClassification,
            headerPrice,
                
            getHistoryItem;

            /**
             * Display each row of the result table according to the data
             * @param {accedo.ui.component} each row of the result table
             * @param {Object} data An object representing a search result
             */
            var displayHelperV = function(component, data) {

                switch(searchBy) {
                    case 'Title':
                        component.getById('titleInfo').setText(data.title);
                        component.getById('classInfo').setText(data.classification);
                        if (searchType == "Movies" && !accedo.utils.object.isUndefined(data.product) && !accedo.utils.object.isUndefined(data.product.price)){
                            component.getById('priceInfo').setText((data.product.price === '$0.00')?'Free':data.product.price);
                        }
                        break;
                    case 'Director/Actor':
                        component.getById('titleInfo').setText(data.name);
                        component.getById('classInfo').setText(searchType);
                        component.getById('priceInfo').setText('');
                        break;
                }
                component.addEventListener('click', function() {
                    componentClickHandler(data);
                });
                component.parent.addEventListener('mouseover', function() {
                    //change selection
                    var visibleItems = list.getChildren();
	            	if (visibleItems[list.getSelectedIndex()] != component){
	            		//change selection (first - define the index relatively to the visible items)
	            		for(var i = 0; i < visibleItems.length; ++i){
	            			if (visibleItems[i] == component){
	            				list.setSelectedIndex(i);
	            				break;		
	            			}
	            		}
	            	}
                    accedo.focus.managerMouse.requestFocus(component.root);
                });
                component.parent.addEventListener('mouseout', function() {
                    accedo.focus.managerMouse.releaseFocus();
                });
                component.parent.addEventListener('click', function(){
	            	componentClickHandler(data);
	           	});
            };
            componentClickHandler = function(dataObj){
            	var dispatch_context;
                    
                //TODO: add handling for director/actor searching
                if (searchBy == 'Title'){
                    if (searchType == 'Movies'){
                        dispatch_context = {
                            //passing ds only for movies, so users could look through all the search results items
                            category_ds: search_result_ds,
                            shift: list.getSelection().dsIndex,
                            currentMediaType: currentMediaType,
                            currentPageSpecial: 'search_title',
                            currentTitle: dataObj.title,
                            currentSearchTitle: "Movies"
                        };
                    }else{//searchType is Television
                        if (dataObj.type != 'season'){
                            return;
                        }
                        dispatch_context = {
                            //only need to pass selected_ch_id for Television, as it'll use it to retrieve all the episode to show
                            selected_ch_id: dataObj.mediaitemid,
                            currentMediaType: currentMediaType,
                            currentPageSpecial: 'search_title',
                            currentTitle: dataObj.title,
                            currentSearchTitle: "TV Shows"
                        };
                    }
                }else{//searchBy == 'Director/Actor'
                    dispatch_context = {
                        currentMediaType: currentMediaType,
                        currentPageSpecial: 'search_people',
                        currentTitle: dataObj.name,
                        currentSearchTitle: searchType + ': '+ searchBy,
                        currentSearchUrl: dataObj.url,
                        forceListingMode: true
                    };
                }
                
                telstra.sua.module.bpvod.utils.setDispatchController(myPubObj, 'movieDetailsController', dispatch_context, false, getHistoryItem());
            };
            getHistoryItem = function(){
                var historyItem = accedo.utils.object.extend(opts.context, {
                    shift: list.getSelection().dsIndex
                    });
                return historyItem;
            };

            var myPubObj = accedo.utils.object.extend(accedo.ui.controller(opts), {

                onCreate: function(context) {
                    var self = this,
                    optionsList = [];

                    //Set the view
                    this.setView(telstra.sua.module.bpvod.views.searchResultPageView);
                    this.setVisible(false);
                    accedo.utils.fn.delay(function(){
                        self.setVisible(true);
                    },0.1);

                    list = this.get('search_result_vertical_listing');
                    this.get('searchRowEdge2').setVisible(false);
                    this.get('searchRowEdge3').setVisible(false);

                    if (opts.context.search_result_ds.getTotalItems() >=2) {
                        this.get('searchRowEdge2').setVisible(true);
                    }
                    if (opts.context.search_result_ds.getTotalItems() >=3) {
                        this.get('searchRowEdge3').setVisible(true);
                    }

                    // get the context passed by history, it must be passed so we don't check if it exitsts
                    searchInput = opts.context.searchInput,
                    searchType  = opts.context.searchType,
                    searchBy    = opts.context.searchBy;
                    
                    if (searchType != "Movies"){
                        searchType = "TV shows";
                    }
                    currentMediaType = searchType != "Movies" ? "Television" : "Movies";
                    
                    if (opts.context.shift){
                        list.select(opts.context.shift);
                    }
                    
                    //get UI component references
                    searchInputWord = this.get('searchInputWord');
                    searchInputInfo = this.get('searchInputInfo');
                    headerMovieTitle = this.get('headerMovieTitle');
                    headerClassification = this.get('headerClassification');
                    headerPrice = this.get('headerPrice');
                    
                    //set table headers text by different search type
                    searchInputWord.setText('Keyword: ' + searchInput);
                    searchInputInfo.setText(searchType +': '+searchBy);

                    switch(searchBy) {
                        case 'Title':
                            headerMovieTitle.setText('Title');
                            headerClassification.setText('Classification');
                            headerPrice.setText((searchType === 'Movies')?'Price':'');
                            break;
                        case 'Director/Actor':
                            headerMovieTitle.setText('Person');
                            headerClassification.setText('Type');
                            headerClassification.getRoot().addClass('people');
                            headerPrice.setText('');
                            break;
                    }

                    // Took the data source given from search page (search page will only pass thie reference if results >0)
                    search_result_ds = opts.context.search_result_ds;
                    //we don't need to load this datasource again as it's already loaded before

                    list.setDisplayHelper(displayHelperV);
                    list.setDatasource(search_result_ds);

                    // Handle how many records found once search returns
                    this.get('searchCount').setText(opts.context.searchResultStr);

                    // Hook scrollerKey up and down
                    upScrollerBtn = this.get('upScroller');
                    downScrollerBtn = this.get('downScroller');

                    list.addEventListener('accedo:list:moved', function(evt) {
                        if (evt.reverse) {
                            upScrollerBtn.getRoot().addClass('focused');
                            setTimeout(function() {
                                upScrollerBtn.getRoot().removeClass('focused');
                            }, 200);
                        } else {
                            downScrollerBtn.getRoot().addClass('focused');
                            setTimeout(function() {
                                downScrollerBtn.getRoot().removeClass('focused');
                            }, 200);
                        }
                    });
                    upScrollerBtn.addEventListener('click',function(evt){
                        list.moveSelection(true);
                    });
                    downScrollerBtn.addEventListener('click',function(evt){
                        list.moveSelection();
                    });
                    
                    optionsList.push(
						{
							type: "red",
							title: "New Search"
						},
						{
							type: "yellow",
							title: "Classifications"
						}
					);

                    accedo.utils.fn.defer(accedo.utils.fn.bind(accedo.focus.manager.requestFocus, accedo.focus.manager), list);
                    //Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);
                },

                /**
                 *  Action need to be done when back button is pressed
                 */
                onKeyBack : function(){
                    this.dispatchEvent('telstra:core:historyBack');
                },
                onKeyYellow: function() {
                	//classifications
                	telstra.api.usage.log("overlay", "", {
                        logMessage: "bigpond movies: options overlay logged " + (telstra.sua.module.bpvod.session.isSignedIn ? "in" : "out") + ": classifications"
                    });

                    telstra.sua.module.bpvod.controllers.popupClassificationController();
                },
                onKeyRed: function() {
            		var historyItem = accedo.utils.object.extend(getHistoryItem(), {
                        selectedIndexCat:1
                    });
                    utils.setDispatchController(myPubObj, 'searchPageController',{}, false, historyItem);
                }
            });

            return myPubObj;
        };
    });