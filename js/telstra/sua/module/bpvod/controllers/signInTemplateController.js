/**
 * @fileOverview signInTemplateController
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">CheungChunHo</a>
 *
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.signInTemplateController", [
    // dependency
    "telstra.sua.module.bpvod.utils"],
    function(){
        return function(opts) {

            // Shorten for namespacing
            var utils = telstra.sua.module.bpvod.utils,
            optionsList = [],
            self;

            var myPubObj = accedo.utils.object.extend(accedo.ui.controller(opts), {

                continueButton : null,
                backButtonm : null,
                callcode : 19927,
                param : {},

                init: function(view) {
                    telstra.sua.core.main.mainNavBar.setMenuButton("HELP");

                    self = this;

                    //Set the view
                    this.setView(view);

                    this.param = opts.context || {};

                    this.continueButton = this.get('continueButton');
                    this.backButton = this.get('backButton');

                    this.continueButton.addEventListener('click', self.continueButtonAction);
                    this.backButton.addEventListener('click', self.backButtonAction);

                    optionsList.push(
						{
							type: "red",
							title: "Help"
						},
						{
							type: "green",
							title: "Cancel Sign In"
						},
						{
							type: "yellow",
							title: "BigPond Plans"
						}
					);
					//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);
                },
                
                cancelAction : function(callback){
                    var myFocus = accedo.focus.manager.getCurrentFocus();
                    accedo.focus.manager.requestFocus(null);

                    var popupYesNo = accedo.ui.popupYesNo({
                        headline_css: 'header',
                        headline_txt: 'Cancel or Exit Sign In',
                        msg_css: 'innerLabel',
                        msg_txt: "Are you sure you want to cancel "
                        +"<br/>or exit Sign In?<br/><br/>"
                        +"If you cancel or exit Sign In, all<br/>"
                        +"information you have entered so<br/>"
                        +"far will be lost.<br/><br/>"
                        +"If you are having problems<br/>"
                        +"joining up on your TV, please visit http:\/\/bigpond.com\/joinmovies to<br/>"
                        +"complete your registration.",
                        yesBtn_css: 'yesButton block medium button',
                        yesBtn_txt: "Cancel",
                        yesBtn_width: "196px",
                        noBtn_css: 'noButton button medium',
                        noBtn_txt: "Don't Cancel",
                        noBtn_width: "196px"
                    });

                    popupYesNo.addEventListener('accedo:popupYesNo:onConfirm', function() {
                        if(callback){
                            callback();
                        }else{
                            utils.signOutAction(function(){
                                utils.setDispatchController(myPubObj, 'mainController', {}, true);
                            });
                        }
                    });

                    popupYesNo.addEventListener('accedo:popupYesNo:onClose', function() {
                        accedo.focus.manager.requestFocus(myFocus);
                    });
                },

                onKeyRed: function() {
                    if (!telstra.sua.core.main.keyboardShown)
                        telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', this.callcode);
                },
                
                onKeyGreen: function() {
            		if (!telstra.sua.core.main.keyboardShown)
                        this.cancelAction();
                },
                
                onKeyYellow: function() {
                	telstra.sua.module.bpvod.controllers.popupBigPondPlanController();
                },

                /**
                 *  Action need to be done when back button is pressed
                 */
                onKeyBack : function(){
                    if (!telstra.sua.core.main.keyboardShown) {
                        if (accedo.app.getCurrentController().get("coreSubController").get('pageTitle').getText() == "Confirm your credit card" ||
                            accedo.app.getCurrentController().get("coreSubController").get('stepTag').getText() == "5") {
                            this.cancelAction(function(){
                                utils.signOutAction(function(){
                                    utils.backToMovieHome(myPubObj);
                                });
                            });
                        }
                        else {
                            this.backButtonAction();
                        }
                    }
                },

                continueButtonAction : function(){},
                backButtonAction : function(){}
            });

            return myPubObj;
        };
    });