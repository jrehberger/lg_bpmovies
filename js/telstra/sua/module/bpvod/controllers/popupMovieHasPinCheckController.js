/**
 * @fileOverview popupMovieHasPinCheckController
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
 */

accedo.define("telstra.sua.module.bpvod.controllers.popupMovieHasPinCheckController", [
    'telstra.sua.module.bpvod.views.popupMovieRasPinCheckView',
    "accedo.utils.object",
    "accedo.utils.fn",
    "telstra.sua.module.bpvod.utils",
    "telstra.sua.module.bpvod.config",
    "telstra.api.manager",
    'telstra.sua.core.controllers.popupOkInfoController'],
    function() {


        /**
         * @class
         * @extends accedo.ui.popup
         */
        return function(opts) {

            if(!opts.videoItem || !opts.parentController || !opts.callback){
                accedo.console.info("popupMovieHasPinCheckController popup missing paramter")
                return;
            }

            var config  = telstra.sua.module.bpvod.config,
            utils       = telstra.sua.module.bpvod.utils,
            myPubObj = null,
            videoItem   = opts.videoItem,
            parentController = opts.parentController,
            historyItem = opts.historyItem,
            callback = opts.callback,

            //ui component
            popupMain = null,
            verticalPanel = null,
            videoTitle = null,
            pinField = null,
            buttonPanel = null,
            forgotPin = null,
            cancelButton = null,
            continueButton = null,
            classificationText = null,
            classificationImage = null,

            intPinFailTime = 0;

            /**
             * Set-up inheritance.
             * @scope telstra.sua.module.bpvod.components.popupConnectionInfo
             */
            myPubObj = accedo.utils.object.extend(accedo.ui.popup(opts), {

                init : function(){
                    var self = this;

                    //setView
                    this.setView(telstra.sua.module.bpvod.views.popupMovieRasPinCheckView);

                    //declare ui componenet
                    accedo.utils.fn.delay(function(){

                        popupMain = self.get('popupBg').getById('popupMain');
                        videoTitle = popupMain.getById('videoTitle');
                        verticalPanel = popupMain.getById('verticalPanel');
                        pinField = verticalPanel.getById('pinField');
                        buttonPanel = verticalPanel.getById('buttonPanel');
                        forgotPin = buttonPanel.getById('forgotPin');
                        cancelButton = buttonPanel.getById('cancelButton');
                        continueButton = buttonPanel.getById('continueButton');
                        classificationText = popupMain.getById('classificationLabel');
                        classificationImage = popupMain.getById('classificationImage');

                        videoTitle.setText(videoItem.title);
                        classificationText.setText(videoItem.consumeradvice);
                        classificationImage.setSrc(telstra.api.manager.resizer(videoItem.classificationimage, 150, 51, true));

                        pinField.maxLengthCallback = function() {
                            accedo.focus.manager.requestFocus(continueButton);
                        }

                        cancelButton.addEventListener('click', function() {
                            self.close();
                        });
                    
                        forgotPin.addEventListener('click', function() {
                            accedo.console.log("forgot pin handle");
                            utils.moveToForgotPin(parentController, historyItem, function(){
                                accedo.utils.fn.delay(function(){
                                   var controller = accedo.app.getCurrentController().get("coreSubController");
                                   utils.playVideoAction(videoItem, controller, historyItem);
                                }, 1);
                            });
                            self.close();
                        });
                    
                        continueButton.addEventListener('click', function() {
                            accedo.console.log("continue button click");
                            try{
                                if(!utils.checkNumeric(pinField.getCleanedResultStr())){
                                    telstra.sua.core.components.infoMessageBar.addBar({
                                        text : "Please input numbers in the pin field.",
                                        colour : "red",
                                        id : 'pinInfoInput'
                                    });
                                    accedo.console.log("not numeric");
                                    accedo.focus.manager.requestFocus(pinField);
                                    return;
                                }

                                if(utils.checkPin(pinField.getCleanedResultStr())){
                                    accedo.console.log("correct Pin");
                                    self.close();
                                    callback();
                                }else{

                                    intPinFailTime++;

                                    if (intPinFailTime > config.pinFailLimit)
                                    {
                                        pinField.setString("");
                                        telstra.sua.core.controllers.popupOkInfoController({
                                            headerText:  "PIN Incorrect",
                                            innerText : 'To change the PIN associated with this account, press the Forgotten PIN button.',
                                            buttonText: 'OK - Close',
                                            callback: function(){
                                                
                                                accedo.focus.manager.requestFocus(pinField);
                                            }
                                        });
                                        accedo.console.log("pin fail over limit");
                                        
                                    }else{
                                        telstra.sua.core.components.infoMessageBar.addBar({
                                            text : "The PIN you entered is incorrect. Retry, or select 'Forgotten PIN'.",
                                            colour : "red",
                                            id : 'pinIncorrect'
                                        });
                                        accedo.console.log("pin fail");
                                        pinField.setString("");
                                        accedo.focus.manager.requestFocus(pinField);
                                    }
                                }
                            }catch(e){
                                accedo.console.log(e);
                            }

                        });

                        accedo.utils.fn.defer(function(){
                            accedo.focus.manager.requestFocus(pinField);
                        });
                    }, 0.5);
                },

                onKeyBack : function(){
                    this.close();
                }
            });

            myPubObj.init();
            return myPubObj;
        };
    });