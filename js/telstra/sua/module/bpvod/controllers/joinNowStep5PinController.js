/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 *
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.joinNowStep5PinController", [
    // dependency
    "telstra.api.usage",
    "telstra.sua.module.bpvod.views.joinNowStep5PinView",
    "telstra.sua.module.bpvod.controllers.joinNowTemplateController"],
    function(){

        return function(opts){

            // Shorten for namespacing
            var utils = telstra.sua.module.bpvod.utils,
            self, pinInputField, pinConfirm;

            var myPubObj = accedo.utils.object.extend(telstra.sua.module.bpvod.controllers.joinNowTemplateController(opts), {
    
                onCreate : function(){
                    telstra.api.usage.log("bpvod", "join_now_step_5", {
                        logMessage: "bigpond movies:join:pinstepa"
                    });
                    
                    self = this;

                    this.init(telstra.sua.module.bpvod.views.joinNowStep5PinView);

                    pinInputField = self.get('PinInputField');
                    pinConfirm = self.get('PinConfirm');
                    
                    //test - predefined input
                    /*pinInputField.setString("1111");
                    pinConfirm.setString("1111");*/
                    //test

                    accedo.focus.manager.requestFocus(pinInputField);

                    pinInputField.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(pinConfirm);
                    };
                    pinConfirm.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(self.continueButton);
                    };

                    this.continueButton.setOption("nextUp", 'PinConfirm');

                    this.continueButton.onFocus = function() {
                        pinConfirm.setOption('nextDown', 'continueButton');
                    };

                    this.backButton.setOption("nextUp", 'PinConfirm');

                    this.backButton.onFocus = function() {
                        pinConfirm.setOption('nextDown', 'backButton');
                    };
                },

                continueButtonAction : function() {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();

                    var input1 = pinInputField.getCleanedResultStr();
                    var input2 = pinConfirm.getCleanedResultStr();

                    if(input1.length == 4 && input2.length == 4) {
                        if(utils.checkIdentical(input1, input2)) {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();
                        
                            self.joinNowInfo.accountPin = input1;

                            utils.setDispatchController(myPubObj, 'joinNowStep6PinSettingController', self.param, true);
                        }
                        else {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();

                            pinInputField.setString('');
                            pinConfirm.setString('');
                            accedo.focus.manager.requestFocus(pinInputField);

                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : 'Please enter the same 4 digit PIN  in both fields.',
                                colour : 'red',
                                id : 'enterSameDigits'
                            });
                        }
                    }
                    else {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                        pinInputField.setString('');
                        pinConfirm.setString('');
                        accedo.focus.manager.requestFocus(pinInputField);

                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : 'Your PIN must be 4 digits long.',
                            colour : 'red',
                            id : 'enterFourDigits'
                        });
                    }
                },

                backButtonAction : function() {
                    if (telstra.sua.core.components.loadingOverlay.isLoadingOn())
                        return;
                    
                    utils.setDispatchController(myPubObj, 'joinNowStep4BCardDetailsController', self.param, true);
                }
            });

            return myPubObj;
        }
    });