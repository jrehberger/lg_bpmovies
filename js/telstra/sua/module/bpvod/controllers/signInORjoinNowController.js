/**
 * @fileOverview
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.signInORjoinNowController",[
    // dependency
    "telstra.sua.core.controllers.popupHelpController",
    "telstra.sua.module.bpvod.controllers.popupBigPondPlanController",
    "telstra.sua.core.controllers.popupOkInfoController",
    "accedo.utils.object","accedo.ui.controller", 'telstra.api.usage',
    "telstra.sua.module.bpvod.utils",
    "telstra.sua.module.bpvod.views.signInORjoinNowView"],
    function(){
        return function(opts){
            accedo.console.log("Create signInORjoinNowController instance");
            var utils = telstra.sua.module.bpvod.utils
            optionsList = [], self,
            views = telstra.sua.module.bpvod.views;

            return accedo.utils.object.extend(accedo.ui.controller(opts), {

                onCreate: function() {
                    //Set the view
                    this.setView(views.signInORjoinNowView);

                    self = this;
                    var signInBT = this.get('signInBT');
                    var joinNowBT = this.get('joinNowBT');

                    signInBT.addEventListener('click', function() {
                        utils.setDispatchController(self, 'signInController', opts.context, true);
                    });

                    joinNowBT.addEventListener('click', function() {
                        utils.setDispatchController(self, 'joinNowStep1EmailController', opts.context, true);
                    });
                    
                    optionsList.push(
						{
							type: "red",
							title: "Help"
						},
						{
							type: "green",
							title: "Unmetered?"
						},
						{
							type: "yellow",
							title: "BigPond Plans"
						}
					);
                    //Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);

                    accedo.focus.manager.requestFocus(joinNowBT);
                },
                
                onKeyRed: function() {
            		telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 19721);
                },
                
                onKeyGreen: function() {
            		telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 18648, "Unmetered");
                },
                
                onKeyYellow: function() {
                	telstra.sua.module.bpvod.controllers.popupBigPondPlanController();
                },

                onKeyBack : function(){
                    this.dispatchEvent('telstra:core:historyBack');
                }
            });
        };
    });