/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    'telstra.sua.module.bpvod.controllers.myAccountChangeEmailController',
    ['accedo.utils.object',
    'accedo.utils.fn',
    'accedo.focus.manager',
    'accedo.ui.controller',
    'accedo.ui.popupYesNo',
    'telstra.api.manager',
    'telstra.api.usage',
    'telstra.sua.module.bpvod.utils',
    'telstra.sua.core.components.infoMessageBar',
    'telstra.sua.core.components.loadingOverlay',
    'telstra.sua.core.controllers.popupOkInfoController',
    'telstra.sua.module.bpvod.views.myAccountChangeEmailView'],
    function(){
        return function(opts) {

            var views = telstra.sua.module.bpvod.views,
            utils = telstra.sua.module.bpvod.utils,
            self, emailField1, emailField2, editDetailsButton, closeButton,
            originalEmail = telstra.api.manager.movies.accountInfo.email,
            optionsList = [],
            mode = 'change';

            return accedo.utils.object.extend(accedo.ui.controller(opts), {

                onCreate: function() {
                    telstra.sua.core.main.mainNavBar.setMenuButton("HELP");

                    self = this;

                    //Set the view
                    self.setView(views.myAccountChangeEmailView);

                    emailField1 = self.get('emailField1');
                    emailField2 = self.get('emailField2');
                    editDetailsButton = self.get('editDetailsButton');
                    closeButton = self.get('closeButton');

                    emailField1.deactivate();
                    emailField1.getInputField().addInputFieldClass("fill");
                    emailField1.setString(telstra.api.manager.movies.accountInfo.email);
                    emailField2.deactivate();
                    emailField2.getInputField().addInputFieldClass("fill");
                    emailField2.setString(telstra.api.manager.movies.accountInfo.email);

                    emailField1.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(emailField2);
                    };
                    emailField2.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(editDetailsButton);
                    };

                    editDetailsButton.addEventListener('click',function() {
                        if (mode == 'change') {
                            var popupYesNo = accedo.ui.popupYesNo({
                                headline_css: 'header',
                                headline_txt: 'Edit Email / Username',
                                msg_css: 'innerLabel',
                                msg_txt: 'Are you sure you want to change your email address/username for your BigPond Movies account?<br/><br/>If you change your email address, you will be required to sign in again entering in your username (email address) and password.',
                                yesBtn_css: 'yesButton button medium',
                                yesBtn_txt: 'Edit',
                                yesBtn_width: "196px",
                                noBtn_css: 'noButton button medium',
                                noBtn_txt: "Don't Edit",
                                noBtn_width: "196px"
                            });

                            popupYesNo.addEventListener('accedo:popupYesNo:onConfirm', function() {
                                mode = 'save';
                                emailField1.activate();
                                emailField2.activate();
                                editDetailsButton.setText('Save Changes');
                                closeButton.setText('Cancel');
                                editDetailsButton.setOption('nextUp', 'emailField2');
                                closeButton.setOption('nextUp', 'emailField2');
                                
                                accedo.utils.fn.delay(function() {
                                    accedo.focus.manager.requestFocus(emailField1);
                                }, 0.1);
                            });
                        }
                        else {
                            self.saveAction();
                        }
                    });

                    editDetailsButton.onFocus = function() {
                        emailField2.setOption('nextDown', 'editDetailsButton');
                    };

                    closeButton.addEventListener('click',function() {
                        if (mode == 'save') {
                            self.changeBack();
                        }
                        else {
                            emailField2.setOption('nextDown', 'editDetailsButton');
                            self.dispatchEvent('telstra:core:historyBack');
                        }
                    });

                    closeButton.onFocus = function() {
                        emailField2.setOption('nextDown', 'closeButton');
                    };
                    
                    optionsList.push(
						{
							type: "red",
							title: "Help"
						}
					);
					//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);

                    accedo.focus.manager.requestFocus(editDetailsButton);
                },

                changeBack: function() {
                    mode = 'change';
                    emailField1.deactivate();
                    emailField1.getInputField().addInputFieldClass("fill");
                    emailField1.setString(telstra.api.manager.movies.accountInfo.email);
                    emailField2.deactivate();
                    emailField2.getInputField().addInputFieldClass("fill");
                    emailField2.setString(telstra.api.manager.movies.accountInfo.email);
                    editDetailsButton.setText('Edit Details');
                    closeButton.setText('Close');
                    editDetailsButton.setOption('nextUp', null);
                    closeButton.setOption('nextUp', null);
                    accedo.focus.manager.requestFocus(editDetailsButton);
                },

                saveAction: function() {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();

                    var input1 = emailField1.getCleanedResultStr();
                    var input2 = emailField2.getCleanedResultStr();

                    if (input1.length <=1 || input2.length <=1) {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : 'Please enter your email address in both fields',
                            colour : "red",
                            id : 'enterEmailBothFields'
                        });

                        accedo.focus.manager.requestFocus(emailField1);
                        return;
                    }

                    if (utils.checkIdentical(input1,input2)) {
                        if (utils.checkEmail(input1)) {
                            telstra.api.manager.movies.verifyUser(input1, "", {
                                onSuccess: function() {
                                //should be never success
                                },
                                onFailure: function(json) {
                                    if (json.errorcode == "AUTH_FAIL") {
                                        telstra.sua.core.components.loadingOverlay.turnOffLoading();
                                        
                                        telstra.sua.core.controllers.popupOkInfoController({
                                            headerText:  'An account already exists with this email address.',
                                            innerText : 'Please sign in to the account used by this email address or enter a different email address.',
                                            buttonText: 'OK'
                                        });
                                    }else if (json.errorcode != "INVALID_USER" && input1 ==  telstra.api.manager.movies.accountInfo.email) {
                                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                        telstra.sua.core.controllers.popupOkInfoController({
                                            headerText:  'An account already exists with this email address.',
                                            innerText : 'Please sign in to the account used by this email address or enter a different email address.',
                                            buttonText: 'OK'
                                        });
                                    }else{
                                        telstra.api.manager.movies.accountInfo.email = input1;

                                        utils.updateMoviesAccount(telstra.api.manager.movies.accountInfo, {
                                            onSuccess:function() {
                                                telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                                //go to signinpage
                                                utils.signOutAction(function(){
                                                    utils.setDispatchController(self, 'signInController', {
                                                        changeEmail: true
                                                    }, true);

                                                    accedo.utils.fn.delay(function(){
                                                        telstra.sua.core.components.infoMessageBar.addBar({
                                                            text : "Your email address/username has been changed.",
                                                            colour : "green",
                                                            id : 'emailChanged'
                                                        });
                                                    },1);
                                                });
                                            },
                                            onFailure:function() {},
                                            onException:function() {}
                                        });
                                    }
                                },
                                onException:function() {}
                            })
                        } else {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();

                            accedo.focus.manager.requestFocus(emailField1);

                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : 'The email address you entered is invalid. Please try again.',
                                colour : 'red',
                                id : 'emailInvalid'
                            });
                        }
                    } else {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                        accedo.focus.manager.requestFocus(emailField1);

                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : "The email addresses you entered don't match. Please try again.",
                            colour : 'red',
                            id : 'emailNotMatched'
                        });
                    }
                },

                onKeyRed : function() {
                    if (!telstra.sua.core.main.keyboardShown)
                        telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 19940);
                },

                onKeyBack: function() {
                    if (telstra.sua.core.components.loadingOverlay.isLoadingOn() && telstra.sua.core.main.keyboardShown)
                        return;

                    emailField2.setOption('nextDown', 'editDetailsButton');
                    self.dispatchEvent('telstra:core:historyBack');
                }
            });
        }
    });