/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */
accedo.define(
    "telstra.sua.module.bpvod.controllers.myAccountChangeNameController",
    ["accedo.utils.object",
    "accedo.ui.controller",
    'accedo.focus.manager',
    'telstra.api.manager',
    'telstra.api.usage',
    'telstra.sua.module.bpvod.utils',
    'telstra.sua.core.components.infoMessageBar',
    'telstra.sua.core.components.loadingOverlay',
    'telstra.sua.module.bpvod.views.myAccountChangeNameView'],
    function(){
        return function(opts){

            var utils = telstra.sua.module.bpvod.utils,
            views = telstra.sua.module.bpvod.views,
            self, inputField1, inputField2, editDetailsButton, closeButton,
            originalFirstname = telstra.api.manager.movies.accountInfo.firstname,
            originalSurname = telstra.api.manager.movies.accountInfo.surname,
            optionsList = [],
            mode = 'change';

            return accedo.utils.object.extend(accedo.ui.controller(opts), {

                onCreate: function() {
                    telstra.sua.core.main.mainNavBar.setMenuButton("HELP");

                    self = this;

                    //Set the view
                    self.setView(views.myAccountChangeNameView);

                    inputField1 = self.get('inputField1');
                    inputField2 = self.get('inputField2');
                    editDetailsButton = self.get('editDetailsButton');
                    closeButton = self.get('closeButton');

                    inputField1.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(inputField2);
                    };
                    inputField2.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(editDetailsButton);
                    };

                    inputField1.deactivate();
                    inputField1.getInputField().addInputFieldClass("fill");
                    inputField1.setString(telstra.api.manager.movies.accountInfo.firstname);
                    inputField2.deactivate();
                    inputField2.setString(telstra.api.manager.movies.accountInfo.surname);
                    inputField2.getInputField().addInputFieldClass("fill");

                    editDetailsButton.addEventListener('click',function() {
                        if (mode == 'change') {
                            mode = 'save';
                            inputField1.activate();
                            inputField2.activate();
                            inputField1.setString("");
                            inputField2.setString("");
                            inputField1.getInputField().removeInputFieldClass("fill");
                            inputField2.getInputField().removeInputFieldClass("fill");
                            editDetailsButton.setText('Save Changes');
                            closeButton.setText('Cancel');
                            editDetailsButton.setOption('nextUp', 'inputField2');
                            closeButton.setOption('nextUp', 'inputField2');
                            accedo.focus.manager.requestFocus(inputField1);
                        }
                        else {
                            self.saveAction();
                        }
                    });

                    editDetailsButton.onFocus = function() {
                        inputField2.setOption('nextDown', 'editDetailsButton');
                    };

                    closeButton.addEventListener('click',function() {
                        if (mode == 'save') {
                            self.changeBack();
                        }
                        else {
                            inputField2.setOption('nextDown', 'editDetailsButton');
                            self.dispatchEvent('telstra:core:historyBack');
                        }
                    });

                    closeButton.onFocus = function() {
                        inputField2.setOption('nextDown', 'closeButton');
                    };
                    
                    optionsList.push(
						{
							type: "red",
							title: "Help"
						}
					);
					//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);

                    accedo.focus.manager.requestFocus(editDetailsButton);
                },

                changeBack: function() {
                    mode = 'change';
                    inputField1.deactivate();
                    inputField1.getInputField().addInputFieldClass("fill");
                    inputField1.setString(telstra.api.manager.movies.accountInfo.firstname);
                    inputField2.deactivate();
                    inputField2.setString(telstra.api.manager.movies.accountInfo.surname);
                    inputField2.getInputField().addInputFieldClass("fill");
                    editDetailsButton.setText('Edit Changes');
                    closeButton.setText('Close');
                    editDetailsButton.setOption('nextUp', null);
                    closeButton.setOption('nextUp', null);
                    accedo.focus.manager.requestFocus(editDetailsButton);
                },

                saveAction: function() {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();

                    var input1 = inputField1.getCleanedResultStr();
                    var input2 = inputField2.getCleanedResultStr();

                    if (utils.checkUserName(input1,input2)) {
                        telstra.api.manager.movies.accountInfo.firstname = input1;
                        telstra.api.manager.movies.accountInfo.surname = input2;

                        utils.updateMoviesAccount(telstra.api.manager.movies.accountInfo, {
                            onSuccess:function() {
                                telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : 'Your name has been changed.',
                                    colour : 'green',
                                    id : 'nameChanged'
                                });
                                originalFirstname = input1;
                                originalSurname = input2;
                                self.changeBack();
                                accedo.focus.manager.requestFocus(closeButton);
                            },
                            onFailure:function() {
                                telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                telstra.sua.core.components.infoMessageBar.addBar({
                                    text : 'Change User Name Fail.',
                                    colour : 'red',
                                    id : 'changeFailed'
                                });
                                telstra.api.manager.movies.accountInfo.firstname = originalFirstname;
                                telstra.api.manager.movies.accountInfo.surname = originalSurname;
                            }
                        });
                    }
                    else {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();
                        
                        accedo.focus.manager.requestFocus(inputField1);
                    }
                },

                onKeyRed : function() {
                    if (!telstra.sua.core.main.keyboardShown)
                        telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 19952);
                },

                onKeyBack: function() {
                    if (telstra.sua.core.components.loadingOverlay.isLoadingOn() && telstra.sua.core.main.keyboardShown)
                        return;

                    inputField2.setOption('nextDown', 'editDetailsButton');
                    self.dispatchEvent('telstra:core:historyBack');
                }
            });
        }
    });