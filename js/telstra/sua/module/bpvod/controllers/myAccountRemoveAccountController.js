/**
 * @fileOverview
 * @author <a href='mailto:victor.leung@accedobroadband.com'>Victor Leung</a>
 */
accedo.define(
    'telstra.sua.module.bpvod.controllers.myAccountRemoveAccountController',
    ['accedo.utils.object',
    'accedo.ui.controller',
    'accedo.ui.popupYesNo',
    'accedo.focus.manager',
    'telstra.api.manager',
    'telstra.api.usage',
    'telstra.sua.core.components.infoMessageBar',
    'telstra.sua.module.bpvod.utils',
    'telstra.sua.module.bpvod.views.myAccountRemoveAccountView'],
    function(){
        
        return function(opts){

            var utils = telstra.sua.module.bpvod.utils,
            views = telstra.sua.module.bpvod.views,
            optionsList = [],
            self, deleteButton, closeButton;

            return accedo.utils.object.extend(accedo.ui.controller(opts), {

                onCreate: function() {
                    telstra.sua.core.main.mainNavBar.setMenuButton("HELP");

                    self = this;

                    //Set the view
                    self.setView(views.myAccountRemoveAccountView);
                    
                    deleteButton = self.get('deleteButton');
                    closeButton = self.get('closeButton');

                    deleteButton.addEventListener('click',function() {
                        self.onDelete();
                    });

                    closeButton.addEventListener('click',function() {
                        self.dispatchEvent('telstra:core:historyBack');
                    });

                    optionsList.push(
						{
							type: "red",
							title: "Help"
						}
					);
					//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);

                    accedo.focus.manager.requestFocus(deleteButton);
                },

                onDelete: function(){
                    var popupYesNo = accedo.ui.popupYesNo({
                        headline_css: 'header',
                        headline_txt: 'Delete Account information from my Device',
                        msg_css: 'innerLabel',
                        msg_txt: "<hr>Are you sure you want to delete your account information and purchase history from this Device?<br><hr>"
                        + "Telstra and its suppliers may retain Personal "
                        + "Information previously collected. For more "
                        + "information please see Telstra's Protecting "
                        + "your Privacy Statement at "
                        + "http://www.telstra.com.au/privacy/privacy_statement.html",
                        yesBtn_css: 'deleteButton button medium',
                        yesBtn_txt: 'Delete',
                        yesBtn_width: '196px',
                        noBtn_css: 'cancelButton button medium',
                        noBtn_txt: 'Cancel',
                        noBtn_width: '196px'
                    });

                    popupYesNo.addEventListener('accedo:popupYesNo:onConfirm', function() {
                        self.deleteAccount();
                    });
                },

                deleteAccount: function() {
                    utils.signOutAction(function(){
                        self.dispatchEvent('telstra:core:clearTillLast');
                        utils.setDispatchController(self, 'mainController', {}, true);

                        accedo.utils.fn.delay(function(){
                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : "Your Account Information has been deleted. Returning you to the Movies Store main menu.",
                                colour : "green",
                                id : 'accountInfoDeleted'
                            });
                        },1);
                    });
                },

                onKeyRed : function() {
                    telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', 19947);
                },

                onKeyBack: function() {
                    if (telstra.sua.core.components.loadingOverlay.isLoadingOn())
                        return;

                    self.dispatchEvent('telstra:core:historyBack');
                }
            });
        }
    });