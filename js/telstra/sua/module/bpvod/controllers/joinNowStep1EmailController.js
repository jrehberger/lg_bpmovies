/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 *
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.joinNowStep1EmailController", [
    // dependency
    "telstra.api.usage",
    "telstra.sua.module.bpvod.views.joinNowStep1EmailView",
    "telstra.sua.module.bpvod.controllers.joinNowTemplateController"],
    function(){

        return function(opts){

            // Shorten for namespacing
            var utils = telstra.sua.module.bpvod.utils,
            self, emailField;

            var myPubObj = accedo.utils.object.extend(telstra.sua.module.bpvod.controllers.joinNowTemplateController(opts), {
    
                onCreate : function(){
                    telstra.api.usage.log("bpvod", "join_now_step_1", {
                        logMessage: "bigpond movies:join:email address"
                    });

                    self = this;

                    this.init(telstra.sua.module.bpvod.views.joinNowStep1EmailView);

                    emailField = this.get('emailField');

                    if (this.joinNowInfo.email != null) {
                        emailField.setString(this.joinNowInfo.email);
                    }
                    
                    accedo.focus.manager.requestFocus(emailField);

                    emailField.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(self.continueButton);
                    }

                    this.continueButton.setOption("nextUp", 'emailField');
                    this.continueButton.setOption("nextLeft", null);
                },

                continueButtonAction : function() {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();

                    var input = emailField.getCleanedResultStr();

                    if (input.length <= 1) {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : 'Please enter your email address',
                            colour : "red",
                            id : 'emptyEmail'
                        });

                        accedo.focus.manager.requestFocus(emailField);
                        return;
                    }

                    if (self.joinNowInfo.email != null && input == self.joinNowInfo.email) {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                        var popupYesNo = accedo.ui.popupYesNo({
                            headline_css: 'header',
                            headline_txt: 'Confirm Email Address',
                            msg_css: 'innerLabel',
                            msg_txt: "Is the email address you entered correct?<br/><br/><a style='font-weight:20px;'>" + "a@a.com" + "</a>",
                            yesBtn_css: 'yesButton button medium',
                            yesBtn_txt: 'Yes',
                            yesBtn_width: '196px',
                            noBtn_css: 'noButton button medium',
                            noBtn_txt: "No",
                            noBtn_width: '196px'
                        });
                        
                        popupYesNo.addEventListener('accedo:popupYesNo:onConfirm', function() {
                            utils.setDispatchController(myPubObj, 'joinNowStep2PasswordController', self.param, true);
                        });
                    }
                    else {
                        if (utils.checkEmail(input)) {
                            telstra.api.manager.movies.verifyUser(input, "", {
                                onSuccess: function(json) {
                                    telstra.sua.core.components.loadingOverlay.turnOffLoading();
                                },
                                onFailure: function(json) {
                                    telstra.sua.core.components.loadingOverlay.turnOffLoading();

                                    if (json.errorcode == "AUTH_FAIL") {
                                        utils.setDispatchController(myPubObj, 'signInController', self.param, true);

                                        accedo.utils.fn.delay(function() {
                                            telstra.sua.core.components.infoMessageBar.addBar({
                                                text : 'An account already exists with this email address. Please sign in to continue',
                                                colour : "red",
                                                id : 'accountAlreadyExist'
                                            });
                                        }, 0.2);
                                    } else if (json.errorcode == "INVALID_USER") {
                                        var popupYesNo = accedo.ui.popupYesNo({
                                            headline_css: 'header',
                                            headline_txt: 'Confirm Email Address',
                                            msg_css: 'innerLabel',
                                            msg_txt: "Is the email address you entered correct?<br/><br/><a style='font-weight:20px;'>" + input + "</a>",
                                            yesBtn_css: 'yesButton button medium',
                                            yesBtn_txt: 'Yes',
                                            yesBtn_width: '196px',
                                            noBtn_css: 'noButton button medium',
                                            noBtn_txt: "No",
                                            noBtn_width: '196px'
                                        });

                                        popupYesNo.addEventListener('accedo:popupYesNo:onConfirm', function() {
                                            self.joinNowInfo.email = input;

                                            utils.setDispatchController(myPubObj, 'joinNowStep2PasswordController', self.param, true);
                                        });
                                    }
                                },
                                onException:function() {
                                    telstra.sua.core.components.loadingOverlay.turnOffLoading();
                                }
                            })
                        } else {
                            telstra.sua.core.components.loadingOverlay.turnOffLoading();

                            telstra.sua.core.components.infoMessageBar.addBar({
                                text : 'The email address you entered is invalid. Please try again.',
                                colour : 'red',
                                id : 'invalidEmail'
                            });

                            accedo.focus.manager.requestFocus(emailField);
                        }
                    }
                },

                backButtonAction : function() {
                    if (telstra.sua.core.components.loadingOverlay.isLoadingOn())
                        return;
                    
                    self.dispatchEvent('telstra:core:historyBack');
                }
            });

            return myPubObj;
        }
    });