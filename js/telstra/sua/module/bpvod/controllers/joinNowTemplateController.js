/**
 * @fileOverview signInTemplateController
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 *
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.joinNowTemplateController", [
    // dependency
    "accedo.device.manager",
    "telstra.api.manager",
    "telstra.sua.module.bpvod.config",
    "telstra.sua.module.bpvod.utils"],
    function(){
        return function(opts) {

            // Shorten for namespacing
            var utils = telstra.sua.module.bpvod.utils,
            optionsList = [],
            self;

            var myPubObj = accedo.utils.object.extend(accedo.ui.controller(opts), {

                continueButton : null,
                backButton : null,
                callcode : 19927,
                cancelText : "Cancel Join Now",
                param : {},

                init: function(view) {
                    telstra.sua.core.main.mainNavBar.setMenuButton("HELP");

                    self = this;

                    this.param = opts.context || {};
                    if (this.param.loginCreditCardDetails){
                    	this.cancelText = "Cancel Sign In";
                    }
                    this.joinNowInfo = telstra.sua.module.bpvod.config.joinNowInfo;
                    this.creditCardInfo = telstra.sua.module.bpvod.config.creditCardInfo;

                    //Set the view
                    this.setView(view);

                    this.continueButton = this.get('continueButton');
                    this.backButton = this.get('backButton');

                    this.continueButton.addEventListener('click', self.continueButtonAction);
                    this.backButton.addEventListener('click', self.backButtonAction);

                    optionsList.push(
						{
							type: "red",
							title: "Help"
						},
						{
							type: "green",
							title: this.cancelText
						}
					);
					//Set the nav bar options
					telstra.sua.core.main.mainNavBar.setMenuButtons(optionsList);
                },

                cancelAction : function() {
                    var myFocus = accedo.focus.manager.getCurrentFocus();
                    accedo.focus.manager.requestFocus(null);
                    var msgText = "Are you sure  you want to cancel "
                        +"<br/>or exit " + this.cancelText + "?<br/>"
                        +"<br/>If you cancel or exit " + this.cancelText + ", all<br/>"
                        +"information you have entered so<br/>"
                        +"far will be lost.";
                    if (this.cancelText != "Cancel Sign In"){
                    	msgText += "<br/><br/>If you are having problems<br/>joining up on your TV, please visit http:\/\/bigpond.com\/joinmovies to<br/>complete your registration.";	
                    }

                    var popupYesNo = accedo.ui.popupYesNo({
                        headline_css: 'header',
                        headline_txt: "Cancel or Exit " + this.cancelText,
                        msg_css: 'innerLabel',
                        msg_txt: msgText,
                        yesBtn_css: 'yesButton block medium button',
                        yesBtn_txt: "Cancel",
                        yesBtn_width: '196px',
                        noBtn_css: 'noButton button medium',
                        noBtn_txt: "Don't Cancel",
                        noBtn_width: '196px'
                    });
                    
                    popupYesNo.addEventListener('accedo:popupYesNo:onConfirm', function() {
                        self.clearData();
                        utils.signOutAction(function(){
                            utils.setDispatchController(myPubObj, 'mainController', {
                                selectedIndex: 3
                            }, true);
                        });
                    });

                    popupYesNo.addEventListener('accedo:popupYesNo:onClose', function() {
                        accedo.focus.manager.requestFocus(myFocus);
                    });
                },

                clearData : function() {
                    self.joinNowInfo.email = null;
                    self.joinNowInfo.pwd = null;
                    self.joinNowInfo.firstName = null;
                    self.joinNowInfo.surName = null;
                    self.joinNowInfo.useAccountPin = null;
                    self.joinNowInfo.accountPin = null;
                    self.joinNowInfo.deviceName = null;
                    self.creditCardInfo.cardType = null;
                    self.creditCardInfo.cardNumber.cc1 = null;
                    self.creditCardInfo.cardNumber.cc2 = null;
                    self.creditCardInfo.cardNumber.cc3 = null;
                    self.creditCardInfo.cardNumber.cc4 = null;
                    self.creditCardInfo.expiryDate.month = null;
                    self.creditCardInfo.expiryDate.year = null;
                    self.creditCardInfo.securityCode = null;
                },

                onKeyRed : function() {
                    if (!telstra.sua.core.main.keyboardShown)
                        telstra.api.manager.rightNowAPI.callHelp('BigPond Movies', this.callcode);
                },
                
                onKeyGreen: function() {
            		if (!telstra.sua.core.main.keyboardShown)
            			this.cancelAction();
                },
                
                onKeyBlue: function(){
                	if (telstra.sua.module.bpvod.session.signedInButNotFinishedProcess){
                		utils.signOutAction(function(){
                            utils.backToMovieHome(myPubObj);
                        });
                	}
                	else{
                		telstra.sua.core.main.moveBackToMyApps();
                	}
                },
                
                onKeyHome: function(){
                	if (telstra.sua.module.bpvod.session.signedInButNotFinishedProcess){
                		utils.signOutAction(function(){
                            telstra.sua.core.main.moveBackToMyApps();
                        });
                	}
                	else{
                		telstra.sua.core.main.moveBackToMyApps();
                	}
                },
                
                /**
                 *  Action need to be done when back button is pressed
                 */
                onKeyBack : function() {
                    if (!telstra.sua.core.main.keyboardShown) {
                        if (accedo.app.getCurrentController().get("coreSubController").get('stepTag').getText() != "1") {
                            this.backButtonAction();
                        }
                        else {
                            this.cancelAction();
                        }
                    }
                },

                continueButtonAction : function(){},
                backButtonAction : function(){}
            });

            return myPubObj;
        };
    });