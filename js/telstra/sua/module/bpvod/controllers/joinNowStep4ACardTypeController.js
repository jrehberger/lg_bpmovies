/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 *
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.joinNowStep4ACardTypeController", [
    // dependency
    "telstra.api.usage",
    "telstra.sua.module.bpvod.views.joinNowStep4ACardTypeView",
    "telstra.sua.module.bpvod.controllers.joinNowTemplateController"],
    function(){

        return function(opts){

            // Shorten for namespacing
            var utils = telstra.sua.module.bpvod.utils,
            self, option1, option2, option3, selection;

            var myPubObj = accedo.utils.object.extend(telstra.sua.module.bpvod.controllers.joinNowTemplateController(opts), {
    
                onCreate : function(){
                    
                    self = this;

                    this.init(telstra.sua.module.bpvod.views.joinNowStep4ACardTypeView);
                    
                    if (this.param.loginCreditCardDetails){
                    	telstra.sua.core.components.infoMessageBar.addBar({
                            text : 'Your existing account does not have a registered credit card. Please register your credit card.',
                            colour : "red",
                            id : 'noRegisteredCC'
                        });
                    }

                    if (this.param.updateCC || this.param.loginCreditCardDetails) {
                        var titleRoot = this.get('StepTitle').getRoot();
                        titleRoot.removeClass('StepTitle');
                        titleRoot.addClass('signInTitle');
                    }
                  	
                  	telstra.api.usage.log("bpvod", "join_now_step_4a", {
                        logMessage: "bigpond movies:join:ccstepa"
                    });

                    option1 = self.get('option1');
                    option2 = self.get('option2');
                    option3 = self.get('option3');

                    accedo.focus.manager.requestFocus(option1);

                    option1.addEventListener('click', function() {
                        option1.getRoot().addClass('select');
                        option2.getRoot().removeClass('select');
                        option3.getRoot().removeClass('select');
                        selection = "visa";
                        accedo.focus.manager.requestFocus(self.continueButton);
                    });

                    option2.addEventListener('click', function() {
                        option1.getRoot().removeClass('select');
                        option2.getRoot().addClass('select');
                        option3.getRoot().removeClass('select');
                        selection = "mastercard";
                        accedo.focus.manager.requestFocus(self.continueButton);
                    });

                    option3.addEventListener('click', function() {
                        option1.getRoot().removeClass('select');
                        option2.getRoot().removeClass('select');
                        option3.getRoot().addClass('select');
                        selection = "amex";
                        accedo.focus.manager.requestFocus(self.continueButton);
                    });

                    this.continueButton.setOption("nextUp", 'option3');

                    this.continueButton.onFocus = function() {
                        option3.setOption('nextDown', 'continueButton');
                    };

                    this.backButton.setOption("nextUp", 'option3');

                    this.backButton.onFocus = function() {
                        option3.setOption('nextDown', 'backButton');
                    };
                },

                continueButtonAction : function() {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();

                    if (selection != null) {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();
                        
                        if (self.creditCardInfo.cardType != selection) {
                            self.creditCardInfo.cardType = selection;
                            self.clearData();
                        }

                        utils.setDispatchController(myPubObj, 'joinNowStep4BCardDetailsController', self.param, true);
                    }
                    else {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : 'Please choose your card type',
                            colour : 'red',
                            id : 'chooseCardType'
                        });
                    }
                },

                backButtonAction : function() {
                    if (telstra.sua.core.components.loadingOverlay.isLoadingOn())
                        return;

                    option3.setOption('nextDown', 'continueButton');
                    
                    if (self.param.updateCC) {
                        utils.setDispatchController(myPubObj, 'signInStep4CardInfoController', self.param, true);
                    }
                    else if (self.param.loginCreditCardDetails) {
                        self.cancelAction();
                    }
                    else {
                        utils.setDispatchController(myPubObj, 'joinNowStep3NameController', self.param, true);
                    }
                },

                clearData : function() {
                    self.creditCardInfo.cardNumber.cc1 = null;
                    self.creditCardInfo.cardNumber.cc2 = null;
                    self.creditCardInfo.cardNumber.cc3 = null;
                    self.creditCardInfo.cardNumber.cc4 = null;
                    self.creditCardInfo.expiryDate.month = null;
                    self.creditCardInfo.expiryDate.year = null;
                    self.creditCardInfo.securityCode = null;
                }
            });

            return myPubObj;
        }
    });