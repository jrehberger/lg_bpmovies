/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 *
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.joinNowStep7DeviceNameController", [
    // dependency
    "telstra.api.usage",
    "telstra.sua.module.bpvod.views.joinNowStep7DeviceNameView",
    "telstra.sua.module.bpvod.controllers.joinNowTemplateController"],
    function(){

        return function(opts){

            // Shorten for namespacing
            var utils = telstra.sua.module.bpvod.utils,
            self, TVnameInputField;

            var myPubObj = accedo.utils.object.extend(telstra.sua.module.bpvod.controllers.joinNowTemplateController(opts), {
    
                onCreate : function(){
                    telstra.api.usage.log("bpvod", "join_now_step_7", {
                        logMessage: "bigpond movies:join:device name"
                    });

                    self = this;

                    this.init(telstra.sua.module.bpvod.views.joinNowStep7DeviceNameView);

                    TVnameInputField = this.get('TVnameInputField');
                    TVnameInputField.setString(self.joinNowInfo.firstName + "\'s " + accedo.device.manager.identification.getDeviceType());

                    TVnameInputField.maxLengthCallback = function() {
                        accedo.focus.manager.requestFocus(self.continueButton);
                    }

                    this.continueButton.setOption("nextUp", 'TVnameInputField');

                    this.continueButton.onFocus = function() {
                        TVnameInputField.setOption('nextDown', 'continueButton');
                    };

                    this.backButton.setOption("nextUp", 'TVnameInputField');

                    this.backButton.onFocus = function() {
                        TVnameInputField.setOption('nextDown', 'backButton');
                    };

                    accedo.focus.manager.requestFocus(TVnameInputField);
                },

                continueButtonAction : function() {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();

                    var deviceName = TVnameInputField.getCleanedResultStr();

                    if (accedo.utils.object.isUndefined(deviceName) || deviceName == null || deviceName == "") {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : 'Please enter a name for your Western Digital device.',
                            colour : "red",
                            id : 'enterDeviceName'
                        });

                        accedo.focus.manager.requestFocus(TVnameInputField);
                    }
                    else {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();
                        
                        self.joinNowInfo.deviceName = deviceName;

                        utils.setDispatchController(myPubObj, 'joinNowStep8TCController', self.param, true);
                    }
                },

                backButtonAction : function() {
                    if (telstra.sua.core.components.loadingOverlay.isLoadingOn())
                        return;

                    TVnameInputField.setOption('nextDown', 'continueButton');
                    utils.setDispatchController(myPubObj, 'joinNowStep6PinSettingController', self.param, true);
                }
            });

            return myPubObj;
        }
    });