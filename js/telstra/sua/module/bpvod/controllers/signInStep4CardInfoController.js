/**
 * @fileOverview
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 *
 */

accedo.define(
    // main class
    "telstra.sua.module.bpvod.controllers.signInStep4CardInfoController", [
    // dependency
    "telstra.api.usage",
    "telstra.sua.module.bpvod.views.signInStep4CardInfoView",
    "telstra.sua.module.bpvod.controllers.signInTemplateController"],
    function(){

        return function(opts){

            // Shorten for namespacing
            var utils = telstra.sua.module.bpvod.utils,
            self, CCtypeBackend, CCnumBackend, CCExpBackend;

            var myPubObj = accedo.utils.object.extend(telstra.sua.module.bpvod.controllers.signInTemplateController(opts), {
    
                onCreate : function(){
                    self = this;

                    this.callcode = 19927;

                    this.init(telstra.sua.module.bpvod.views.signInStep4CardInfoView);

                    CCtypeBackend = this.get('CCtypeBackend');
                    CCnumBackend = this.get('CCnumBackend');
                    CCExpBackend = this.get('CCExpBackend');
                    
                    if (this.param.creditcard) {
                        if (this.param.creditcard.cardtype == "visa")
                            CCtypeBackend.setText("Visa");
                        else if (this.param.creditcard.cardtype == "mastercard")
                            CCtypeBackend.setText("Mastercard");
                        else
                            CCtypeBackend.setText("American Express");
                        CCnumBackend.setText(this.param.creditcard.maskedcardnumber);
                        CCExpBackend.setText(this.param.creditcard.expirydate);
                    }

                    accedo.focus.manager.requestFocus(self.continueButton);
                },

                continueButtonAction : function() {
                    telstra.sua.core.components.loadingOverlay.turnOnLoading();
                    
                    if (accedo.utils.object.isUndefined(self.param.creditcard)) {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();
                        
                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : 'Your existing account does not have a registered credit card. Please register a new credit card.',
                            colour : "red",
                            id : 'noRegisteredCC'
                        });

                        accedo.focus.manager.requestFocus(self.backButton);
                    }
                    else if (!utils.checkCCExpDate(self.param.creditcard.expirydate.substr(5,2), self.param.creditcard.expirydate.substr(2,2))) {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                        accedo.focus.manager.requestFocus(self.backButton);
                    }
                    else {
                        telstra.sua.core.components.loadingOverlay.turnOffLoading();

                        if(!self.param.accountpin){
                            utils.setDispatchController(myPubObj, 'signInStep5SetPinController', self.param, true);
                        }else{
                            utils.setDispatchController(myPubObj, 'signInStep5AccountPinController', self.param, true);
                        }
                    }
                },

                //change card
                backButtonAction : function(){
                    self.param.updateCC = true;
                    utils.setDispatchController(myPubObj, 'joinNowStep4ACardTypeController', self.param, true);
                }
            });

            return myPubObj;
        }
    });