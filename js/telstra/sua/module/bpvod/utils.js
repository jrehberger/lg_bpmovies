/**
 * @fileOverview utilities for varies functions shared over all controllers and components.
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 * @auth <a href="mailto:roy.chan@accedobroadband.com">Roy Chan </a>
 */

accedo.define(
    "telstra.sua.module.bpvod.utils", [
    "accedo.utils.object",
    "accedo.device.manager",
    "telstra.api.usage",
    "telstra.sua.core.components.imagePreloader",
    "telstra.sua.module.bpvod.config",
    "telstra.sua.core.components.loadingOverlay",
    "telstra.sua.module.bpvod.session",
    'telstra.sua.core.controllers.popupOkInfoController'],
    function(){
        return {
            movieCategories: null,
            /**
             * Scroll text and update a vertical scroll bar
             * @param {String} direction either 'up', or 'down', 'refresh', 'reset'
             * @param {Object} textContainer the layout object containing the text panel that's limiting what to be shown, this is the one with overflow:hidden
             * @param {Object} textContent the layout objecgt containing the Text that's to be scrolled
             * @param {Object} scrollbar is the scrollbar to be updated
             */
            scrollText: function(direction, textContainer, textContent, scrollbar, step){
                var containerHeight = textContainer.getRoot().getHTMLElement().offsetHeight,
                currentHeight  = textContent.getRoot().getHTMLElement().scrollHeight,
                currentTop = textContent.getRoot().getHTMLElement().offsetTop;
                
                step = step || 80;
                
                if (currentHeight <= containerHeight){
                    textContainer.getRoot().addClass('noScroll');
                    textContent.getRoot().setStyle({
                        top: "0px"
                    });
                    scrollbar.parent.hide();
                    return;
                }
                var targetTop;
                if (direction == 'reset'){
                    targetTop = 0;
                }
                else if(direction == 'up'){
                    targetTop = currentTop + step;
                    if(targetTop > 0){
                        targetTop = 0;
                    }
                }else if (direction == 'down'){
                    targetTop = currentTop - step;
                    if(targetTop < -1 * (currentHeight - containerHeight) ){
                        targetTop = -1 * (currentHeight - containerHeight);
                    }
                }

                currentTop = targetTop;
                textContent.getRoot().setStyle({
                    top: targetTop + "px"
                });
                textContainer.getRoot().removeClass('noScroll');
                scrollbar.parent.show();
                scrollbar.update(-1* currentTop, containerHeight, currentHeight );
                
            },
            /**
             * Now the XML to Json library is changed from xml2json to xmlobjectifier, however the json objects output is a bit different,
             * this function is to convert the new JSON to the old one so that we don't have to change the code
             * I would illustrate the main difference by example:
             * In xmlobjectifier format: product[0].price[0].Text, in xml2jsonFormat: product.price
             * @param {Object} item in XML objectifier format
             * @return {Object} item in xml2json format
             */
            migrateXmlJson: function(item){
                var migrated = {};
                for ( var key in item ) {
                    //ignore all _attributes, _child properties that's not used
                    if (key.substring(0,1) == '_'){
                        continue;
                    }
                    if (key == 'typeOf'){
                        continue;
                    }
                    //xml2json only uses lower case tag names
                    var m_key = key.toLowerCase();
                    
                    
                    if (accedo.utils.object.isString(item[key])){
                        //all properties having the Text is ignored, because it'll be abc[0].Text which needs to be handled below
                        if (key == 'Text'){
                            continue;
                        }
                        
                        migrated[m_key] = item[key];
                    }else if (!accedo.utils.object.isUndefined(item[key][0])){
                        if (item[key][0].Text!=''){
                            //change abc.prop[0].Text to abc.prop
                            migrated[m_key] = item[key][0].Text;
                        }else if (item[key].length == 1){
                            //chagne abc.product[0] to abc.prodct if there's only one
                            migrated[m_key] = this.migrateXmlJson(item[key][0]);
                        }else{
                            migrated[m_key] = [];
                            var itemLen = item[key].length;
                            for (var j=0; j<itemLen; j++){
                                migrated[m_key][j] = this.migrateXmlJson(item[key][j]);
                            }
                        }
                    }
                    
                }
                return migrated;
            },
            /**
             * Set up target controller for switch on event with
             * respective initial data
             * @param {String} targetController Target controller to
             *   switch to
             * @param {Object} data Object to pass
             */
            setDispatchController: function(sourceController, targetController, context, skipHistory, fromContext) {
                var loading = telstra.sua.core.components.loadingOverlay;
                
                //the loading overlay of bpmv is a bit different than the others
                telstra.sua.core.components.loadingOverlay.addClass('bpmv');
                
                loading.turnOnLoading();
                if(sourceController.onUnload){
                    sourceController.onUnload();
                }
                sourceController.dispatchEvent(
                    'telstra:core:loadController', {
                        moduleId: 'bpvod',
                        moduleController: targetController,
                        context: context || {},
                        fromContext: fromContext || {},
                        skipHistory : skipHistory || false
                    }
                    );
                accedo.utils.fn.delay(accedo.utils.fn.bind(loading.turnOffLoading, loading),1);
            },
            /**
             * Set up target controller for switch on event with
             * respective initial data
             * @param {String} targetController Target controller to
             *   switch to
             * @param {Object} data Object to pass
             */
            historyBack: function(controller){
                var loading = telstra.sua.core.components.loadingOverlay;
                loading.turnOnLoading();
                controller.dispatchEvent('telstra:core:historyBack');
                accedo.utils.fn.delay(accedo.utils.fn.bind(loading.turnOffLoading, loading),1);
            },
            /**
             * Set the content on several HTML elements on view.
             * Note that the length of elemStrArr must have the
             * same length with dataStrArr
             * @param {accedo.ui.layout.*} view
             * @param {Array} elemStrArr String array for each
             *     element id                         *
             * @param {Array} dataStrArr String array for data to
             *     set to
             */
            setContent: function(view, elemStrArr, dataStrArr) {
                var i = 0, len = elemStrArr.length;
                for(; i<len; i++) {
                    //@todo: change to use native element type identification
                    if (typeof(view.get(elemStrArr[i]).setText) == 'function'){
                        view.get(elemStrArr[i]).setText(dataStrArr[i]);
                    } else if (typeof(view.get(elemStrArr[i]).setSrc) == 'function'){
                        view.get(elemStrArr[i]).setSrc(dataStrArr[i]);
                    }
                }
            },

            /**
             * Preload image for smooth display
             *
             */
            imagePreload : function() {
                telstra.sua.core.components.imagePreloader.preload(telstra.sua.module.bpvod.config.imageList);
            },

            getCategory : function(callback) {
                callback({});
            },

            /**
             * Ellipise a given string
             * @param {Object} opts An object contains text and width
             * @param {String} opts.text The string to ellipise
             * @param {Number} opts.width How many characters should keep
             * @param {Boolean} opts.dots whether to append dots when it's truncated default to true (i.e. append dots)
             * @example var a = 'Hello World!!';
             *          b = telstra.sua.module.bpvod.utils.ellipiseIt({text: a, width: 1}); // b shall be 'H...'
             */
            ellipiseIt : function(opts) {
                if (opts.text.length === 0 || opts.width <= 0) {
                    return opts.text;
                }
                
                var append_dots = opts.dots ? "..." : "";
                return (opts.text.length > opts.width) ? (opts.text.substr(0, opts.width) + append_dots) : opts.text;
            },


            //methods =============
            //methods that could commonly be used would be put here
            /*
             *check and update a specific movie status
              possible object inside status:
              watchNow - whether the user started watching the video
              startDate - date which the user start rent/ initially watch the video
              expiryDate - date which the video would be expired
              resumeTime - position in the video which the user last stopped
              rented - true = rented; false = rented but expired; null = not rented (to be confirmed)
              eupId - the id required to get widevine DRM license
              lastView - true: it is the last item being view (automatically updated with set resumeTime)

              @param videoItem [json] videoItem object to be updated
              @param status [json] status object to put in
              @param callback [function] callbck function

             */
            updateRentedMovieStatus : function(videoItem, status, callback){
            	accedo.console.log("[step 10]: in updateRentedMovieStatus with callback to playVideo");
            	accedo.console.log("updateRentedMovieStatus");
                var api = telstra.api.manager;

                api.time.getTimeNow(function(serverTime){
                    var config = telstra.sua.module.bpvod.config,
                    utils  = telstra.sua.module.bpvod.utils,
                    session = telstra.sua.module.bpvod.session,
                    isUndefined = accedo.utils.object.isUndefined;

                    if(videoItem.type != "movie" && videoItem.type != "episode"){
                        accedo.console.info("incorrect media item type");
                        return;
                    }

                    if(isUndefined(status)){
                        status = {};
                    }

                    if(isUndefined(status.watchNow)){
                        status.watchNow = false;
                    }

                    var currentDate = serverTime,
                    oldItem = null,
                    len = session.rentedMovieList.length,
                    i;
                    accedo.console.info("currentDate: " + currentDate);

                    for(i = 0; i < len; i ++){
                        if(session.rentedMovieList[i].mediaitemid == videoItem.mediaitemid){
                            accedo.console.info("the item is rented already");
                            oldItem = i;
                        }
                    }

                    videoItem.rented = true;

                    //check watch now config
                    if(status.watchNow){
                        if(oldItem == null || (oldItem != null && !session.rentedMovieList[oldItem].started)){
                            videoItem.startDate = currentDate;
                            if(config.testExpiry){
                                videoItem.expiryDate = currentDate + 60000;
                            }else{
                                videoItem.expiryDate = currentDate + 3600000*videoItem.product.viewingperiod;
                            }
                        }
                        videoItem.started = true;

                    }else if(!status.watchNow){
                        if(oldItem == null ){
                            videoItem.startDate = currentDate;
                            if(config.testExpiry){
                                videoItem.expiryDate = currentDate + 60000;
                            }else{
                                videoItem.expiryDate = currentDate + 86400000*videoItem.product.deletionperiod;
                            }
                            videoItem.started = false;
                        }
                    }

                    //resume time
                    if(!isUndefined(status.resumeTime) && !utils.isFree(videoItem)){
                        if( (oldItem == null && !videoItem.started) || (oldItem != null && !session.rentedMovieList[oldItem].started)) {
                            accedo.console.info("resumeTime should be applied only for started video");
                        }else{
                            videoItem.resumeTime = status.resumeTime;
                            if(videoItem.resumeTime != null){
                                //reset all other videoItem's lastView to false

                                for(i = 0; i < len; i ++){
                                    session.rentedMovieList[i].lastView = false;
                                }
                                videoItem.lastView = true;
                                session.lastViewVideoItem = videoItem;
                            }else{
                                videoItem.lastView = false;
                            }
                        }

                    } else if(utils.isFree(videoItem)) {
                        videoItem.resumeTime = null;
                        videoItem.lastView = false;
                    }

                    //eupId : the id for getting widevine license
                    if(!isUndefined(status.eupId)){
                        accedo.console.info("updated eupId: " + status.eupId);
                        videoItem.eupId = status.eupId;
                    }

                    //replace the old item if it is existing item
                    //accedo.console.info("updated videoItem: " + accedo.utils.object.toJSON(videoItem));
                    if(oldItem != null){
                        session.rentedMovieList[oldItem] = videoItem; //replace the existing item
                    }else{
                        if(!utils.isFree(videoItem)){
                            session.rentedMovieList.unshift(videoItem);
                        }
                    }

                    //it signed in, do the save to server
                    if(!telstra.sua.module.bpvod.session.isSignedIn){
                        callback && callback();
                    }else{
                        utils.updateRentedMovieList(callback);
                    }
                });
            	accedo.console.log("updateRentedMovieStatus done");
            },

            /*
             * check and remove expired (not sure if expired item has to be saved for some days)
             */
            screenExpired : function(){
                var config = telstra.sua.module.bpvod.config,
                utils  = telstra.sua.module.bpvod.utils,
                session = telstra.sua.module.bpvod.session,

                newList = [],
                expireList=[],
                rentedMovieListLen = session.rentedMovieList.length,
                i;
                for(i = 0 ; i < rentedMovieListLen; i ++){
                    if(!utils.isExpired(session.rentedMovieList[i])){
                        newList.push(session.rentedMovieList[i]);
                    }
                    else{
                        expireList.push(session.rentedMovieList[i]);
                    }
                }

                // update the expireMovie List and store the 5 most expired one.
                if (expireList.length<=5) {
                    session.recentlyExpiredMovieList = expireList;
                } else {        //newly expire item is more then five
                    expireList.sort( function (itemA, itemB) {
                        return (itemA.expiryDate > itemB.expiryDate);
                    });
                    session.recentlyExpiredMovieList = [];
                    for (i = 0; i < 5; i ++){
                        session.recentlyExpiredMovieList.push(expireList[i]);
                    }
                    expireList = session.recentlyExpiredMovieList;
                }

                //put back the expired item into rented Movie list for storing into server
                for (i = 0; i < 5 && i < expireList.length; i++) {
                    if (expireList[i]!=null) {
                        newList.push(expireList[i]);
                    }
                }
                session.rentedMovieList = [];
                session.rentedMovieList = newList;
                accedo.console.info("newList Length:"+newList.length );
                accedo.console.info("telstra.sua.module.bpvod.session.rentedMovieList Length:" + rentedMovieListLen);
            },

            /*
             * update the rented movie list
             * @param callback [function] callback function
             */
            updateRentedMovieList : function(callback){
                var config  = telstra.sua.module.bpvod.config,
                utils   = telstra.sua.module.bpvod.utils,
                session = telstra.sua.module.bpvod.session,
                loading = telstra.sua.core.components.loadingOverlay;
                if(!telstra.sua.module.bpvod.session.isSignedIn){
                    return;
                }

                if(accedo.utils.object.isUndefined(callback)){
                    callback = function(){};
                }
                utils.screenExpired();
                var value = accedo.utils.object.toJSON(session.rentedMovieList);
                //accedo.console.info("telstra.sua.module.bpvod.session.rentedMovieList: " + value);

                value = value.replace(/&amp;/g,'&');   // to fixed the movie title problem of "Julia & Julia"
                loading.turnOnLoading();

                telstra.api.manager.movies.updateDevUserProfile("rentedMovie",value, {
                    onSuccess:function(){
                        accedo.console.info("success updateDevUserProfile #");
                        callback && callback();
                    },
                    onFailure:function(){
                        accedo.console.info("failure");                   

                        telstra.sua.core.controllers.popupOkInfoController({
                            headerText:  'Error',
                            innerText : 'System came across an error. Exiting widget.',
                            callback : function(){
                                accedo.device.manager.dispatchKey("exit");
                            }
                        });
                    },
                    onException:function(e){
                        loading.turnOffLoading();
                        accedo.console.log("exception: " + e);
                        return;
                    },
                    onComplete: function(){
                        loading.turnOffLoading();
                    }
                });
            },

            /*
             * load the rented movie list (should be done once during login)
             * @todo: a proxy setting is needed
             * @todo: remove eval
             */
            loadRentedMovieList : function(callback){
                var config = telstra.sua.module.bpvod.config,
                utils  = telstra.sua.module.bpvod.utils,
                session = telstra.sua.module.bpvod.session,
                loading = telstra.sua.core.components.loadingOverlay;
                loading.turnOnLoading();
                telstra.api.manager.movies.getDevUserProfile("rentedMovie", {
                    onSuccess:function(json){
                        accedo.console.info("getDevUserProfile succss");
                        var jsonStr = json.profileattribute.value;
                        jsonStr = jsonStr.replace(/typeof: "JSXBObject",/g, "");
                        jsonStr = escape(jsonStr).replace(/%0D/g,"\\r").replace(/%0A/g,"\\n");
                        jsonStr = unescape(jsonStr);

                        if(typeof jsonStr == "object"){ //probably  it is empty
                            jsonStr = "[]";
                        }

                        try {
                            var listArray = eval("{" + jsonStr + "}");//(new function("return {" + objText + "}"))();
                        } catch(e) {
                            loading.turnOffLoading();
                        }
                        
                        session.rentedMovieList = listArray;
                        utils.screenExpired();
                        accedo.console.info("telstra.sua.module.bpvod.session.rentedMovieList.length: " + session.rentedMovieList.length);

                        //accedo.console.info("====== telstra.sua.module.bpvod.utils.loadRentedMovieList =====");
                        //accedo.console.info(accedo.utils.object.toJSON(session.rentedMovieList));
                        //accedo.console.info("====== =============================== =====");

                        utils.checkLastViewItem(); //find out the last viewed item
                        callback();
                    },
                    onFailure:function(){
                        accedo.console.info("failure");
                        //empty list when it is 404 error
                        session.rentedMovieList = [];
                        callback();
                    },
                    onException:function(){
                        accedo.console.info("exception");
                    },
                    onComplete : function(){
                        loading.turnOffLoading();
                    }
                });
            },

            /**
             * Returns data with the rent status if the user is signed in
             * @param {Array} data 
             * @returns {Array} data with each in rented status
             * @public
             */
            updateVideoDataRented : function(data){
                if (!telstra.sua.module.bpvod.session.isSignedIn){
                    return data;
                }
                var len = data.length,
                j=0;
                for (;j<len;j++){
                    data[j] = telstra.sua.module.bpvod.utils.checkUpdateVideoItem(data[j]);
                }
                return data;
                
            },
            /*
             * check whether the newly loaded vidoe item is in the rented movie list and update its status
            use when new item is loaded from server

              watchNow - whether the user started watching the video
              startDate - date which the user start rent/ initially watch the video
              expiryDate - date which the video would be expired
              resumeTime - position in the video which the user last stopped
              rented - true = rented; false = rented but expired; null = not rented (to be confirmed)
              eupId - the id required to get widevine DRM license
              lastView - true: it is the last item being view (automatically updated with set resumeTime)
             *
             * @param videoItem [json] video Item to be checked
             */
            checkUpdateVideoItem : function(videoItem, checkExpired){
                var config      = telstra.sua.module.bpvod.config,
                utils       = telstra.sua.module.bpvod.utils,
                isUndefined = accedo.utils.object.isUndefined,
                session = telstra.sua.module.bpvod.session,
                i = 0,
                rentedMovieListLen = session.rentedMovieList.length;

                if(accedo.utils.object.isUndefined(checkExpired)){
                    checkExpired = true;
                }

                if(isUndefined(videoItem)){
                    accedo.console.info("no video item inputted");
                    return;
                }

                var found = null;
                for(; i < rentedMovieListLen; i ++){
                    if(session.rentedMovieList[i].mediaitemid == videoItem.mediaitemid){
                        found = i;
                        break;
                    }
                }

                if(found != null){

                    accedo.console.info("=== update content for video item: " + videoItem.title);

                    //watchNow
                    if(!isUndefined(session.rentedMovieList[found].watchNow) ){
                        videoItem.watchNow = session.rentedMovieList[found].watchNow;
                    }
                    //rented
                    if(!isUndefined(session.rentedMovieList[found].rented) ){
                        if(checkExpired && utils.isExpired(session.rentedMovieList[found])){
                            videoItem.rented = false;
                        }else{
                            videoItem.rented = session.rentedMovieList[found].rented;
                        }

                    }
                    //startDate
                    if(!isUndefined(session.rentedMovieList[found].startDate) ){
                        videoItem.startDate = session.rentedMovieList[found].startDate;
                    }
                    //expiryDate
                    if(!isUndefined(session.rentedMovieList[found].expiryDate) ){
                        videoItem.expiryDate = session.rentedMovieList[found].expiryDate;
                    }
                    //resumeTime
                    if(!isUndefined(session.rentedMovieList[found].resumeTime) ){
                        videoItem.resumeTime = session.rentedMovieList[found].resumeTime;
                    }
                    //eupId
                    if(!isUndefined(session.rentedMovieList[found].eupId) ){
                        videoItem.eupId = session.rentedMovieList[found].eupId;
                    }
                    //lastView
                    if(!isUndefined(session.rentedMovieList[found].lastView) ){
                        videoItem.lastView = session.rentedMovieList[found].lastView;
                    }
                }
                return videoItem;
            },







            // =============
            // Video Item check
            // =============

            /*
             * check if the video item is episode
             * @param videoItem [json] video item to be check
             */
            isEpisode : function(videoItem){
                var isUndefined = accedo.utils.object.isUndefined;
                if(!isUndefined(videoItem)){
                    if(!isUndefined(videoItem.type)){
                        return (videoItem.type == "episode");
                    }
                }
                accedo.console.info("telstra.sua.module.bpvod.utils.isEpisode error");
                return false;

            },

            /*
             * check if the video item is episode
             * @param videoItem [json] video item to be check
             */
            isSeason : function(videoItem){
                var isUndefined = accedo.utils.object.isUndefined;
                if(!isUndefined(videoItem)){
                    if(!isUndefined(videoItem.type)){
                        return (videoItem.type == "season");
                    }
                }
                accedo.console.info("telstra.sua.module.bpvod.utils.isSeason error");
                return false;

            },

            /*
             * check if the video item is free
             * @param videoItem [json] video item to be check
             */
            isFree : function(videoItem){
                var isUndefined = accedo.utils.object.isUndefined;
                if(!isUndefined(videoItem)){
                    if(!isUndefined(videoItem.product)){
                        if(!isUndefined(videoItem.product.price)){
                            return (videoItem.product.price == "$0.00");
                        }
                    }
                }
                accedo.console.info("telstra.sua.module.bpvod.utils.isFree error");
                return false;

            },

            /*
             * check if the video item is rented
             * @param videoItem [json] video item to be check
             */
            isRented : function(videoItem){
                accedo.console.log("==============");
                accedo.console.log("check isRented: " + videoItem.title);
                var isUndefined = accedo.utils.object.isUndefined;
                if(!isUndefined(videoItem)){
                    if(!isUndefined(videoItem.rented)){
                        if(videoItem.rented){
                            if(!telstra.sua.module.bpvod.utils.isExpired(videoItem)){
                                accedo.console.log("video rented");
                                return true;
                            }else{
                                accedo.console.log("video rented but expired");
                                videoItem.rented = false; // mark as expired
                                return false;
                            }
                        }else{
                            accedo.console.log("video not rented");
                            return false;
                        }
                    }
                }
                accedo.console.info("telstra.sua.module.bpvod.utils.isRented error");
                return false;
            },

            /*
             * check if the video item is expired
             * @param videoItem [json] video item to be check
             */
            isExpired : function(videoItem){
            	accedo.console.info("isExpired check");
                var isUndefined = accedo.utils.object.isUndefined;
                if(!isUndefined(videoItem)){

                    var currentTime = telstra.api.manager.time.getServerTime();

                    accedo.console.info("============");
                    accedo.console.info("check expiry: " + videoItem.title);
                    accedo.console.info("currentTime: " + currentTime);
                    accedo.console.info("expiryTime: " +videoItem.expiryDate );
                    accedo.console.info("============");
                    if(!isUndefined(videoItem.expiryDate)){
                        if(videoItem.expiryDate == null || (videoItem.rented && currentTime > videoItem.expiryDate)){
                            videoItem.rented = false;//set as expired
                            videoItem.expiryDate = null;
                            return true;
                        }else if (videoItem.expiryDate != null && currentTime < videoItem.expiryDate){
                            videoItem.rented = true; //not actually needed, to heal the item that goes wrong with server time issue
                            return false;
                        }else{
                            return true; //expired or not rented
                        }
                    }
                }
                accedo.console.info("telstra.sua.module.bpvod.utils.isExpired error");
                return false;
            },

            //to test the expirted case
            setExpired : function(videoItem){
                var isUndefined = accedo.utils.object.isUndefined;
                if(!isUndefined(videoItem)){

                    var currentTime = telstra.api.manager.time.getServerTime();

                    //                    accedo.console.info("check expiry");
                    //                    accedo.console.info("currentTime: " + currentTime);
                    //                    accedo.console.info("expiryTime: " +videoItem.expiryDate );
                    if(!isUndefined(videoItem.expiryDate)){
                        if(videoItem.rented){
                            videoItem.expiryDate = currentTime - 1000000;
                            accedo.console.log("setExpiryDateDone");
                            accedo.console.info("currentTime: " + currentTime);
                            accedo.console.info("expiryTime: " +videoItem.expiryDate );
                            return true;
                        }else{
                            accedo.console.log("setExpiryDate Fail");
                            return false;
                        }
                    }
                }
                accedo.console.info("telstra.sua.module.bpvod.utils.isExpired error");
                return false;
            },

            /*
             * check if the video item is about to expired
             * @param videoItem [json] video item to be check
             */
            isAboutToExpired : function(videoItem){
                var isUndefined = accedo.utils.object.isUndefined;
                if(!isUndefined(videoItem)){
                    //var tmwTime = (new Date()).getTime() + 24*60*60*1000;
                    var tmwTime = telstra.api.manager.time.getServerTime() + 86400000;
                    if(!isUndefined(videoItem.expiryDate)){
                        return (tmwTime > videoItem.expiryDate);
                    }
                }
                accedo.console.info("telstra.sua.module.bpvod.utils.isAboutToExpired error");
                return false;
            },

            /*
             * check if the video item is restricted item
             * @param videoItem [json] video item to be check
             */
            isRestricted : function(videoItem){
                if(videoItem.classification != "MA15+" && videoItem.classification != "R18+"){
                    return false;
                }else{
                    return true;
                }
            },

            /*
             * count the avaialbe rented item
             */
            countRentedItem : function(){
                var config = telstra.sua.module.bpvod.config,
                utils  = telstra.sua.module.bpvod.utils,
                session = telstra.sua.module.bpvod.session,
                rented = 0,
                aboutToExpired = 0,
                i = 0,
                rentedMovieListLen = session.rentedMovieList.length;

                for(; i < rentedMovieListLen; i++){
                    var itemToCheck = session.rentedMovieList[i];
                    if(utils.isRented(itemToCheck)){
                        if(!utils.isExpired(itemToCheck)){
                            rented +=1;
                            if(utils.isAboutToExpired(itemToCheck)){
                                aboutToExpired +=1;
                            }
                        }
                    }
                }

                return {
                    "rented": rented,
                    "aboutToExpired": aboutToExpired
                };
            },

            /*
             * find out and return the last rented item
             */
            checkLastViewItem : function(){
                accedo.console.log("checkLastViewItem");
                var main  = telstra.sua.module.bpvod.config,
                session = telstra.sua.module.bpvod.session,
                utils  = telstra.sua.module.bpvod.utils,
                i = 0,
                rentedMovieListLen = session.rentedMovieList.length;
                for(; i < rentedMovieListLen; i++){
                    if(session.rentedMovieList[i].lastView){
                        if(utils.isExpired(session.rentedMovieList[i])){
                            session.rentedMovieList[i].lastView = false;
                        }else{
                            session.lastViewVideoItem = session.rentedMovieList[i];
                            //accedo.console.log(accedo.utils.object.toJSON(session.rentedMovieList[i]));
                            return session.rentedMovieList[i];
                        }
                        
                    }
                }
                return false;
            },

            /*
             * parse time (in mili sec) in to format 1 day 14h 59m ...
             */
            parseTime : function(getTime){
                var min  = Math.ceil((getTime % 3600000) / 60000),
                hour = Math.floor((getTime % 86400000)/3600000),
                day  = Math.floor(getTime / 86400000),

                output = "";
                if(day > 0){
                    output += "" + day + "day" + (day>1?"s":"") + " ";
                }
                if(hour > 0){
                    output += "" + hour + "h ";
                }
                output += min + "m";
                accedo.console.info("telstra.sua.module.bpvod.utils.parseTime: " + output);
                return output;
            },

            /*
             * round time (in mili sec) in to format 1 day 14h 59m ...
             */
            roundTime : function(getTime){

                var nows = Math.floor(getTime/1000),
                day  = Math.floor(nows/(86400)),
                hour = Math.floor((nows-day*86400)/3600),
                min  = Math.round((nows-day*86400-hour*3600)/60);
                if (min>=60){
                    min=0;
                    hour+=1;
                }
                if (min<0) min=0;
                if (hour>=24){
                    hour=0;
                    day+=1;
                }
                accedo.console.info("day:"+day+" hour:"+hour+" min:"+min);

                var dateDisplay="";
                if (day>0)
                    dateDisplay+=day+"d ";
                if (hour>0)
                    dateDisplay+=hour+"h ";
                if (min>0)
                    dateDisplay+=min+"m";

                if ( hour<=0 && min<=0)
                {
                    if (day == 1)
                        dateDisplay = day+" day";
                    else if (day > 1)
                        dateDisplay = day+" days";
                }

                if (dateDisplay.length<=0) dateDisplay+="0m";
                return dateDisplay;
            },

            // =============
            // Validation stuff
            // =============
            /*
             * check if the pin is correct
             * @param pinInput [string] pin to check
             */
            checkPin : function(pinInput) {
                if(!telstra.sua.module.bpvod.session.isSignedIn){
                    return;
                }

                if(!accedo.utils.object.isString(pinInput)){
                    return;
                }

                var pin = telstra.api.manager.movies.accountInfo.accountpin;

                //return true
                return (pinInput == pin);
            },

            /*
             * check if it is numeric
             * @param input [string] string to check
             */
            checkNumeric : function(input){

                if(!accedo.utils.object.isString(input)){
                    return false;
                }
                return (/^[0-9]+$/i.test(input));
            },

            /*
             * check two input identical
             * @param input1 [string] input to compare
             * @param input2 [string] input to compare
             */
            checkIdentical : function(input1, input2){

                var isString = accedo.utils.object.isString;
                if(!isString(input1)){
                    return false;
                }

                if(!isString(input2)){
                    return false;
                }

                return (input1 === input2);
            },

            /*
             * check if account pasword is correct and display the corrsponding error message
             * @param input1 [string] password to compare
             * @param input2 [string] reconfirm password to compare
             */
            checkAccountPassword : function(input1,input2){
                var isUndefined = accedo.utils.object.isUndefined,
                utils = telstra.sua.module.bpvod.utils;
                if (isUndefined(input1)||input1 == null ||input1.length<=0||isUndefined(input2)||input2 == null||input2.length<=0) {
                    telstra.sua.core.components.infoMessageBar.addBar({
                        text : "Please enter your password in both fields.",
                        colour : "red",
                        id : 'enterPasswordBothFields'
                    });
                    return false;
                }else if (utils.checkIdentical(input1, input2)) {
                    if (input1.length < 6) {
                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : "Password must longer than 6 characters",
                            colour : "red",
                            id : 'passwordLength'
                        });
                        return false;
                    }
                } else {
                    telstra.sua.core.components.infoMessageBar.addBar({
                        text : "The passwords you entered don't match. Please try again.",
                        colour : "red",
                        id : 'passwordNotMatched'
                    });
                    return false;
                }

                if (!utils.isLetterOrNumber(input1))
                {
                    telstra.sua.core.components.infoMessageBar.addBar({
                        text : "Please use letters and numbers only.",
                        colour : "red",
                        id : 'lettersNumbersOnly'
                    });
                    return false;
                }

                return true;
            },

            /*
             * check if account pasword is correct and display the corrsponding error message
             * @param lastname [string] lastname to check
             * @param firstname [string] firstname to check
             */
            checkUserName : function(lastname,firstname){
                if (lastname == null || firstname == null || lastname.length == null || lastname.length == null
                    || lastname.length <= 0 || firstname.length <= 0)
                    {
                    telstra.sua.core.components.infoMessageBar.addBar({
                        text : "Please type in both your first and last names.",
                        colour : "red",
                        id : 'typeBothNames'
                    });
                    return false;
                }

                var utils = telstra.sua.module.bpvod.utils;
                if (!utils.checkName(lastname) || !utils.checkName(firstname))
                {
                    telstra.sua.core.components.infoMessageBar.addBar({
                        text : "First and Last name should be at least 2 letters long and should contain only letters or space.",
                        colour : "red",
                        id : 'namesFormat'
                    });
                    return false;
                }

                return true;
            },

            /*
             * validating a email string
             * @param str [string] email stirng input
             */
            checkEmail : function(str){

                if(!accedo.utils.object.isString(str)){
                    return false;
                }
                return /^(?:[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|asia|com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum|travel)\b)$/i.test(str);
            },

            /*
             * check credit card number and display the corrsponding error message
             * @param input1, input2, input3, input4 [string] credit card field
             */
            checkCreditCardNumber : function(input1,input2,input3,input4){
                var utils = telstra.sua.module.bpvod.utils;
                if (!utils.checkNumeric(input1)||
                    !utils.checkNumeric(input2)||
                    !utils.checkNumeric(input3)||
                    !utils.checkNumeric(input4))
                    {
                    telstra.sua.core.components.infoMessageBar.addBar({
                        text : "Invalid Credit Card Number",
                        colour : "red",
                        id : 'invalidCCNumber'
                    });
                    return false;
                }
                return true;
            },

            /*
             * check email domain
             * @param str [string] string to check
             */
            checkDomain : function(str){

                for(var i = 0; i < str.length; i ++){
                    var subChar = str[i];
                    if(!( (subChar >= "a" && subChar <= "z") || (subChar >= "A" && subChar <= "Z")
                        || (subChar >= "0" && subChar <= "9") || subChar == "-")){
                        return false;
                    }
                }
                return true;
            },


            /*
             * check credit card expiry year
             * @param str [string] year string to be checked
             */
            checkCCExpY : function(year, month){
                if(!accedo.utils.object.isString(year)){
                    return false;
                }
                var currentTime = new Date(telstra.api.manager.time.getServerTime()),
                expYear = parseInt("20"+year,10),
                expMonth = month,
                diff = expYear - currentTime.getFullYear();

                if (diff > 40) {
                    telstra.sua.core.components.infoMessageBar.addBar({
                        text : "Please enter the expiry date as MM / YY.",
                        colour : "red",
                        id : 'enterExpireDate'
                    });
                    return false;
                } else if (diff < 0) {
                    telstra.sua.core.components.infoMessageBar.addBar({
                        text : "Your credit card has expired.",
                        colour : "red",
                        id : 'expiredCC'
                    });
                    return false;
                } else if (diff == 0) {
                    if (parseInt(expMonth,10) < currentTime.getMonth()+1) {
                        telstra.sua.core.components.infoMessageBar.addBar({
                            text : "Your credit card has expired.",
                            colour : "red",
                            id : 'expiredCC'
                        });
                        return false;
                    }
                    else
                        return true;
                }
                else
                    return true;
            },

            /*
             * check credit card expiry month
             * @param str [string] month string to be checked
             */
            checkCCExpM : function(str){
                if(!accedo.utils.object.isString(str)){
                    return false;
                }
                var month = parseInt(str,10);
                return !(month > 12 || month < 1);
            },


            /*
             * Check credit card's year and month validation and display the corrsponding error message
             * @param month [string] month input
             * @param year [string] year input
             */
            checkCCExpDate : function(month, year)//mm, YY
            {
                var utils    = telstra.sua.module.bpvod.utils,
                config   = telstra.sua.module.bpvod.config;
                
                if (!utils.checkCCExpM(month)) {
                    telstra.sua.core.components.infoMessageBar.addBar({
                        text : "Please enter the expiry date as MM / YY.",
                        colour : "red",
                        id : 'enterExpireDate'
                    });
                    return false;
                }
                else if (!utils.checkCCExpY(year, month))
                    return false;
                else
                    return true;
            },


            /*
             * Check credit card's CVC number and display the corrsponding error message
             * @param cvcNumber [string] cvc number input
             */
            checkCCVC : function(cvcNumber){
                if (cvcNumber ==null || !telstra.sua.module.bpvod.utils.isNumber(cvcNumber) ||cvcNumber.length!=3)
                {

                    telstra.sua.core.components.infoMessageBar.addBar({
                        text : "CVC number invalid. Please re-enter your CVC number.",
                        colour : "red",
                        id : 'invalidCVCNumber'
                    });
                    return false;
                }else {
                    return true;
                }

            },



            /*
             * Check if string are all letters or Numbers
             * @param s [string] string to check
             */
            isLetterOrNumber : function (s) {
                return (/^[A-Za-z0-9]+$/i.test(s));
            },

            /*
             * Check if string are capital letters or Numbers
             * @param s [string] string to check
             */
            isCapLetterOrNumber : function (s) {
                return (/^[A-Z0-9]+$/.test(s));
            },

            /*
             * Check if string are all letters
             * @param s [string] string to check
             */
            isLetter : function (s) {
                return (/^[A-Za-z]+$/i.test(s));
            },

            /*
             * Check if string are all Numbers
             * @param s [string] string to check
             */
            isNumber : function (s) {
                return (/^[0-9]+$/i.test(s));
            },


            /*
             * Check Credit card expiration
             * @param ccDate [string] string to put in (format:yyyy-mm)
             */
            checkCCExp : function(ccDate){
                var config = telstra.sua.module.bpvod.config,
                currentTime = new Date(telstra.api.manager.time.getServerTime());
                config.currentYear  = currentTime.getFullYear();
                config.currentMonth = currentTime.getMonth()+1;
                var currentDate = config.currentYear+""+(config.currentMonth>9?config.currentMonth:"0"+config.currentMonth),
                ccDateHandle = ccDate.substring(0,4)+""+ccDate.substring(5,7);
                accedo.console.info("The current year is==========================>"+config.currentYear);
                accedo.console.info("The current month is==========================>"+config.currentMonth);
                accedo.console.info("The current Date is==========================>"+parseInt(currentDate, 10));
                accedo.console.info("The CC Date handle=>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+parseInt(ccDateHandle,10));
                return (parseInt(currentDate, 10) > parseInt(ccDateHandle, 10));
            },

            /*
             * username validation
             * @param s [string] username input
             */
            checkName : function (s){
                //var patrn=/^[a-zA-Z]+?[\.|-]{0,1}[a-zA-Z]+?$/;
                var patrn=/^[a-zA-Z]+([a-zA-Z]*[\s|\.|-]{1}[a-zA-Z]*)*[a-zA-Z]+$/;
                return patrn.exec(s);
            },

            // =============
            // Login Issue
            // =============
            /*
             * check if it is the first time the user logged in to the bigPondMovie
             * @param callback [function] callback function
             */
            checkFirstTimeLogin : function(callback){

                var loading = telstra.sua.core.components.loadingOverlay;

                loading.turnOnLoading();
                if(accedo.utils.object.isUndefined(callback)){
                    callback = function(){};
                }

                telstra.api.manager.movies.getDevUserProfile("firstTimeLogin", {
                    onSuccess:function(){
                        accedo.console.info("user has logged in before");
                        return callback(false);
                    },
                    onFailure:function(){
                        accedo.console.info("user has not logged in before");
                        return callback(true);
                    },
                    onException:function(){

                    },
                    onComplete:function(){
                        loading.turnOffLoading();
                    }
                });
            },


            /*
             * update the status of firstTimeLogin (when the user gone through joinNow or Sign in for once)
             * @param callback [function] callback function
             */
            updateFirstTimeLogin : function(callback){
                if(accedo.utils.object.isUndefined(callback)){
                    callback = function(){};
                }

                var loading = telstra.sua.core.components.loadingOverlay;
                var popupOk = telstra.sua.core.controllers.popupOkInfoController;

                loading.turnOnLoading();
                telstra.api.manager.movies.updateDevUserProfile("firstTimeLogin","true", {
                    onSuccess:function(){
                        accedo.console.info("update success");
                        callback();
                    },
                    onFailure:function(json){
                        accedo.console.info("update failure");
                        telstra.sua.module.bpvod.utils.errorMessageokPopup(json);
                    },
                    onComplete:function(){
                        loading.turnOffLoading();
                    }
                });
            },

            /*
             * update the account infromation
             * @param info [object] accountInfo
             * @param callback [function] callback function
             */
            updateMoviesAccount: function(accountInfo, callback) {
                if(accedo.utils.object.isUndefined(callback)){
                    callback = function(){};
                }

                telstra.api.manager.movies.updateMoviesAccount(accountInfo,{
                    onSuccess:function(json){
                        accedo.console.info("update success");
                        callback.onSuccess(json);
                    },
                    onFailure:function(json){
                        accedo.console.info("update fail");
                        callback.onFailure(json);
                    },
                    onException:function(json) {
                        accedo.console.info("onException");
                        callback.onException(json);
                    }
                });
            },

            /*
             * get the device type
             */
            getDeviceType : function(){
                var deviceType = accedo.device.manager.identification.getDeviceId();

                if(deviceType == "tv"){
                    deviceType = "TV";
                }else if(deviceType == "bd"){
                    deviceType = "Blu-ray";
                }else if(deviceType == "ht"){
                    deviceType = "Home Theatre";
                }
                else if(deviceType == "workstation"){
                    deviceType = "Workstation";
                }
                return deviceType;
            },

            /*
             * popup message box for error code
             * @param error [json] error object recived from api
             */
            errorMessageokPopup : function(error, callback){
                var popupOk     = telstra.sua.core.controllers.popupOkInfoController,
                isUndefined = accedo.utils.object.isUndefined;
                if (error != null && !isUndefined(error) && !isUndefined(error.errorcode)) {
                    switch(error.errorcode) {
                        case "AUTH_FAIL":

                            popupOk({
                                headerText: 'Error',
                                innerText: 'You may be experiencing a technical issue.' +
                                '<br/>Please contact Telstra on' +
                                '<br/>1800 502 502 for assistance and quote reference number DX-19.',
                                buttonText: 'CLOSE'
                            });
                            break;
                        case "INVALID_CONTENT":

                            if (accedo.utils.object.isFunction(callback)) {
                                popupOk({
                                    headerText: 'Error',
                                    innerText: 'This movie is no longer available to rent. Please view the catalogue for movies currently available on BigPond Movies',
                                    buttonText: 'CLOSE',
                                    callback : callback
                                });
                            }
                            else {
                                popupOk({
                                    headerText: 'Error',
                                    innerText: 'This movie is no longer available to rent. Please view the catalogue for movies currently available on BigPond Movies',
                                    buttonText: 'CLOSE'
                                });
                            }
                            break;
                        case "CONCURRENT_PURCHASE":

                            popupOk({
                                headerText: 'Error',
                                innerText: 'You have already rented the video item requested.',
                                buttonText: 'OK'
                            });
                            break;
                        case "PAY_TXERR":

                            popupOk({
                                headerText: 'Error',
                                innerText: 'You may be experiencing a technical issue.' +
                                '<br/>Please contact Telstra on' +
                                '<br/>1800 502 502 for assistance and quote reference number DVX-12.',
                                buttonText: 'CLOSE'
                            });
                            break;
                        case "PAY_SYERROR":

                            popupOk({
                                headerText: 'Error',
                                innerText: 'You may be experiencing a technical issue.' +
                                '<br/>Please contact Telstra on' +
                                '<br/>1800 502 502 for assistance and quote reference number DVX-11.',
                                buttonText: 'CLOSE'
                            });
                            break;
                        case "PAY_TXERROR":
                            if(telstra.sua.module.bpvod.session.isSignedIn && !isUndefined(telstra.api.manager.movies.accountInfo.creditcard) && telstra.sua.module.bpvod.utils.checkCCExp(telstra.api.manager.movies.accountInfo.creditcard.expirydate)){

                                popupOk({
                                    headerText: 'Error',
                                    innerText: 'Your Credit Card has expired.' +
                                    '<br/>Please update credit card details.',
                                    buttonText: 'CLOSE'
                                });
                            }else{

                                popupOk({
                                    headerText: 'Error',
                                    innerText: 'You may be experiencing a technical issue.' +
                                    '<br/>Please contact Telstra on' +
                                    '<br/>1800 502 502 for assistance and quote reference number DVX-12.',
                                    buttonText: 'CLOSE'
                                });
                            }
                            break;
                        case "CREDIT_CARD_NOT_VALID":

                            popupOk({
                                headerText: 'Error',
                                innerText: 'Your credit card number is not valid. Please try again.',
                                buttonText: 'CLOSE'
                            });
                            break;
                        case "CREDIT_CARD_TYPE_UNSUPPORTED":

                            popupOk({
                                headerText: 'Error',
                                innerText: 'Your credit card type is not supported. Please try again.',
                                buttonText: 'CLOSE'
                            });
                            break;
                        case "NO_STORED_CREDIT_CARD":

                            popupOk({
                                headerText: 'Error',
                                innerText: 'Your existing account does not have a registered credit card.' +
                                '<br/>Please register a new account on http://go.bigpond.com/tv/update',
                                buttonText: 'CLOSE',
                                callback : callback
                            });
                            break;
                        case "NO_STORED_CREDIT":
  
                            popupOk({
                                headerText: 'Error',
                                innerText: 'Your account has insufficient credit to make this purchase.' +
                                '<br/>Please check your payment options in My Account.',
                                buttonText: 'CLOSE'
                            });
                            break;
                        case "CONN_NO_SSL":

                            popupOk({
                                headerText: 'Error',
                                innerText: 'You may be experiencing a technical issue.' +
                                '<br/>Please contact Telstra on' +
                                '<br/>1800 502 502 for assistance and quote reference number DVX-01.',
                                buttonText: 'CLOSE'
                            });
                            break;
                        case "INVALID_DEVICE_TYPE":

                            popupOk({
                                headerText: 'Error',
                                innerText: 'This device is not supported.' +
                                '<br/>Please visit go.bigpond.com for a list of authorised devices.',
                                buttonText: 'CLOSE'
                            });
                            break;
                        case "INVALID_USER":

                            popupOk({
                                headerText: 'Error',
                                innerText: 'You may be experiencing a technical issue.' +
                                '<br/>Please contact Telstra on' +
                                '<br/>1800 502 502 for assistance and quote reference number DVX-05.',
                                buttonText: 'CLOSE'
                            });
                            break;

                        case "PAY_COUNTRYERR":
                        case "PAY_COUNTRYERROR":
                        case "PAY_COUNTRYRERROR":

                            popupOk({
                                headerText: 'Error',
                                innerText: 'The credit card information you entered is not valid.' +
                                '<br/>Please re-enter a valid Australian credit card and try again.' +
                                '<br/>For further help call Telstra on' +
                                '<br/>1800 502 502 and quote reference DVX-10.',
                                buttonText: 'CLOSE'
                            });
                            break;
                        case "OFF_COUNTRY":

                            popupOk({
                                headerText: 'Error',
                                innerText: 'You may be experiencing a technical issue.' +
                                '<br/>Please contact Telstra on' +
                                '<br/>1800 502 502 for assistance and quote reference number DVX-03.',
                                buttonText: 'CLOSE'
                            });
                            break;
                        case "SERVER_ERROR":

                            popupOk({
                                headerText: 'Error',
                                innerText: 'You may be experiencing a technical issue.' +
                                '<br/>Please contact Telstra on' +
                                '<br/>1800 502 502 for assistance and quote reference number DVX-04.',
                                buttonText: 'CLOSE'
                            });
                            break;
                        default:
                            popupOk({
                                headerText: 'Error',
                                innerText: 'You may be experiencing a technical issue.' +
                                '<br/>Please contact Telstra on' +
                                '<br/>1800 502 502 for assistance.'  +
                                '<br/> Error code: #' +error.errorcode,
                                buttonText: 'CLOSE',
                                callback : function(){
                                    telstra.sua.core.main.exitUnifiedApp(true, false);
                                }
                            });
                            break;
                    }

                    telstra.api.usage.log("error", "", {
                        description: error.errorcode
                    });
                }else {
                    accedo.console.info("Undefine Error!!");
                    popupOk({
                        headerText: 'Error',
                        innerText: 'You may be experiencing a technical issue.' +
                        '<br/>Please contact Telstra on' +
                        '<br/>1800 502 502 for assistance.',
                        buttonText: 'CLOSE',
                        callback : function(){
                            telstra.sua.core.main.exitUnifiedApp(true, false);
                        }
                    });
                }
            },

            purchaseCheck : function(videoItem, eventHandler)
            {
                var loading = telstra.sua.core.components.loadingOverlay;
                var productID = videoItem.product.productid;
                accedo.console.log(productID);

                //purchase check couldn't work fine outside samsung platform. Dummy data is needed.
                /*if(accedo.device.manager.identification.getDeviceId() == "workstation"){
                    var dummyData =  {
                        "accountpin": 1111,
                        "price": "$5.99",
                        "cccharge": "$5.99",
                        "vouchercreditcharge": "$0.00",
                        "vouchercreditbeforepurchase": "$0.00",
                        "vouchercreditafterpurchase": "$0.00"
                    };

                    var obj = accedo.utils.object.extend(dummyData,{
                        voucherStatus : 1
                    });

                    if(eventHandler.onSuccess){
                        eventHandler.onSuccess(obj);
                    }
                    if(eventHandler.onComplete){
                        eventHandler.onComplete(obj);
                    }
                    
                    return;
                }*/

                loading.turnOnLoading();
                telstra.api.manager.movies.purchaseCheck(productID,{
                    onSuccess:function(json){
                        accedo.console.log("purchaseCheck: " + productID + " success: " + accedo.utils.object.toJSON(json));

                        var voucherStatus = 1;
                        var purchasePreResult=json;
                        /*var purchasePreResult={
                        "accountpin": 1111,
                        "price": "$7.99",
                        "cccharge": "$0.00",
                        "vouchercreditcharge": "$7.99",
                        "vouchercreditbeforepurchase": "$131.99",
                        "vouchercreditafterpurchase": "$124.00"
                    };*/

                        var vouchercreditcharge = purchasePreResult.vouchercreditcharge;
                        var cccharge = purchasePreResult.cccharge;

                        if (vouchercreditcharge.charAt(0)=='$')
                            vouchercreditcharge=vouchercreditcharge.substr(1); //remove the $
                        if (cccharge.charAt(0)=='$')
                            cccharge=cccharge.substr(1); //remove the $

                        if (vouchercreditcharge<=0)
                            voucherStatus=1;         //credit purphase
                        else if (cccharge > 0)
                            voucherStatus=2;         //party voucher purphase
                        else
                            voucherStatus=3;         // fully voucher purphase

                        accedo.console.log("voucherStatus: " + voucherStatus);
                        
                        var obj = accedo.utils.object.extend(purchasePreResult, {
                            voucherStatus : voucherStatus
                        });

                        if(eventHandler.onSuccess){
                            eventHandler.onSuccess(obj);
                        }
                    },

                    onFailure: function(json){
                        accedo.console.log("purchase Check on Failure");
                        if(eventHandler.onFailure){
                            eventHandler.onFailure(json);
                        }
                    },
                    onException:function(){
                        accedo.console.log("purchase Check on Exception");
                        if(eventHandler.onException){
                            eventHandler.onException();
                        }
                    },
                    onComplete : function(){
                        loading.turnOffLoading();
                    }
                });

                return;
            },

            //do purchase check and redirect to purchase page
            purchaseAction : function(videoItem, controllerObj, historyItem, controllerNameString){
                var skipHistory = false;
                if(!historyItem){
                    historyItem = {};
                    skipHistory = true;
                }
                var utils = telstra.sua.module.bpvod.utils;
                var loading = telstra.sua.core.components.loadingOverlay;
                var session = telstra.sua.module.bpvod.session;

                //check if it is free. If free go to play action directly
                if(utils.isFree(videoItem)){
                    utils.playVideoAction(videoItem, controllerObj, historyItem, skipHistory);
                }else{
                    //check if user has been signed in. If not get to sign in page
                    if(!session.isSignedIn){
                        accedo.console.info("not signed in. Couldn't process to purchase");
                        utils.setDispatchController(controllerObj, 'signInORjoinNowController', {
                            purchaseItem : videoItem
                        }, false, historyItem);
                    }else{
                    	accedo.console.log("[step 2]: not free and signed in");
                        videoItem = utils.checkUpdateVideoItem(videoItem, false);
                        //accedo.console.log("controllerNameString: " + controllerNameString);
                        accedo.console.log("----------- rented: " + videoItem.rented + "; resumeTime: " + videoItem.resumeTime);
                        if(videoItem.rented && utils.isExpired(videoItem) && controllerNameString){
                            //renew own page
                            var popupTitle = (videoItem.type == "movie" ? "Title now expired." : "TV Show now expired");
                            telstra.sua.core.controllers.popupOkInfoController({
                                headerText:  popupTitle,
                                innerText : videoItem.title + " has now expired. <br/><br/>Select continue to update your rentals. ",
                                callback : function(){
                                    utils.setDispatchController(controllerObj, controllerNameString, historyItem, true);
                                }
                            });
                            return;
                        }else if(utils.isRented(videoItem)) {
                            accedo.console.log("[step 3]: go to utils.playVideoAction");
                            utils.playVideoAction(videoItem, controllerObj, historyItem, skipHistory);
                        }else{
                            loading.turnOnLoading();
                            telstra.api.manager.movies.getVoucher({
                                onSuccess:function(){
                                    loading.turnOffLoading();
                                    if (accedo.utils.object.isUndefined((telstra.api.manager.movies.accountInfo.creditcard))) {

                                        telstra.sua.core.controllers.popupOkInfoController({
                                            headerText: 'Error',
                                            innerText: 'Your existing account does not have a registered credit card.' +
                                            '<br/>Please register a new account on http://go.bigpond.com/tv/update',
                                            buttonText: 'CLOSE'
                                        });
                                    } else if (telstra.sua.module.bpvod.utils.checkCCExp(telstra.api.manager.movies.accountInfo.creditcard.expirydate)) {

                                        telstra.sua.core.controllers.popupOkInfoController({
                                            headerText: 'Error',
                                            innerText: 'Your Credit Card has expired.' +
                                            '<br/>Please update credit card details.',
                                            buttonText: 'CLOSE'
                                        });
                                    } else {
                                        utils.setDispatchController(controllerObj, 'purchasePageController', {
                                            videoItem : videoItem
                                        }, skipHistory, historyItem);
                                    }
                                },
                                onFailure : function(json){
                                    loading.turnOffLoading();
                                    utils.errorMessageokPopup(json, function() {
                                        loading.turnOnLoading();
                                        controllerObj.dispatchEvent('telstra:core:historyBack');
                                    });
                                }
                            });
                        }
                    }
                }
            },

            //Handle checking and flow for before redirecting to the video page (except from purchase page)
            playVideoAction : function(videoItem, controllerObj, historyItem, skipHistory){
                if(!historyItem){
                    historyItem = {};
                }
                if(!videoItem || !controllerObj){
                    accedo.console.log("missing parameters");
                    return;
                }

                var utils = telstra.sua.module.bpvod.utils;

                //check if it is free and handle it differently
                if(utils.isFree(videoItem)){
                    telstra.sua.module.bpvod.controllers.popupFreeMoviesTermsController({
                        videoItem: videoItem,
                        parentController : controllerObj,
                        historyItem : historyItem,
                        skipHistory : skipHistory
                    });
                }else{
                    accedo.console.log("[step 4]: go to popupResumePlayController");
                    telstra.sua.module.bpvod.controllers.popupResumePlayController({
                        videoItem: videoItem,
                        parentController : controllerObj,
                        historyItem : historyItem,
                        skipHistory : skipHistory
                    });
                }

            },

            playTrailerAction : function(videoItem, controllerObj, historyItem){
                if(!historyItem){
                    historyItem = {};
                }
                if(!videoItem || !controllerObj){
                    accedo.console.log("missing parameters");
                    return;
                }

                if(!(videoItem.trailers && videoItem.trailers.trailer && videoItem.trailers.trailer.trailerurl )){
                    accedo.console.log("no trailer available");
                    return;
                }

                var utils = telstra.sua.module.bpvod.utils;

                utils.setDispatchController(controllerObj, 'videoPageController', {
                    videoItem : videoItem,
                    trailer : true
                },false,historyItem);

            },

            backToUAHome : function(controllerObj){
                controllerObj.dispatchEvent('telstra:core:clearTillLast');
                controllerObj.dispatchEvent('telstra:core:exitModule');
            },

            backToMovieHome : function(controllerObj){
                var targetMainController = {
                    moduleController : 'mainController',
                    moduleId: 'bpvod'
                };
                if (telstra.sua.core.modulemanager.hasHistory(targetMainController)){
                    controllerObj.dispatchEvent('telstra:core:historyBack', targetMainController);
                }else{
                    controllerObj.dispatchEvent('telstra:core:clearTillLast');
                    this.setDispatchController(controllerObj, 'mainController', {}, true);
                }
            },

            moveToBigPondMovie : function(controllerObj, historyItem){
                var skipHistory = false;
                if(!historyItem){
                    historyItem = {};
                    skipHistory = true;
                }
                var ds = telstra.api.manager.movies.getChannel();
                ds.addEventListener('accedo:datasource:append', function(){
                    var utils = telstra.sua.module.bpvod.utils;
                    var item = ds.getDataAtIndex(ds.search(function(data, index){
                        return (data.subchannelname  && data.subchannelname == "Movies");
                    }));
                    utils.setDispatchController(controllerObj, item.targetctrller, {
                        subChannel: item.subchannelname,
                        url: item.url
                    }, skipHistory, historyItem);
                });
                ds.load();
            },
            
            moveToFreeMovie : function(controllerObj, historyItem){
                var skipHistory = false;
                if(!historyItem){
                    historyItem = {};
                    skipHistory = true;
                }
                var ds = telstra.api.manager.movies.getChannel();
                ds.addEventListener('accedo:datasource:append', function(){
                    var utils = telstra.sua.module.bpvod.utils;
                    var item = ds.getDataAtIndex(ds.search(function(data, index){
                        return (data.subchannelname  && data.subchannelname == "Free Movies");
                    }));
                    utils.setDispatchController(controllerObj, item.targetctrller, {
                        url: item.url,
                        subChannel: item.subchannelname,
                        currentMediaType: item.mediatype,
                        selectedCatName: item.selectedCatName
                    }, skipHistory, historyItem);
                });
                ds.load();
            },

            moveToRelatedMovie : function(videoItem, controllerObj, historyItem){
                if(!videoItem || !controllerObj){
                    return;
                }
                var utils  = telstra.sua.module.bpvod.utils;
                var isUndefined = accedo.utils.object.isUndefined;
                var skipHistory = false;
                if(!historyItem){
                    historyItem = {};
                    skipHistory = true;
                }

                var currentMediaType = (utils.isEpisode(videoItem) || utils.isSeason(videoItem) ? 'Television' : 'Movies' );

                //Go to related movies, we need to prepare the datasource of people for related
                var actor      = (isUndefined(videoItem.actors)||isUndefined(videoItem.actors.actor)) ?
                [] : videoItem.actors.actor,
                director   = (isUndefined(videoItem.directors)||isUndefined(videoItem.directors.director)) ?
                [] : videoItem.directors.director;

                if (!accedo.utils.object.isArray(actor)){
                    actor = [actor];
                }
                if (!accedo.utils.object.isArray(director)){
                    director = [director];
                }
                if (actor.length == 0 && director.length == 0){
                    accedo.console.log("cannot go to related search as there are no actors/directors");
                    return;
                }

                var related_ds = accedo.data.ds();

                var array = [], i, actor_len = actor.length, dir_len = director.length;
                for (i = 0; i < actor_len; i++) {
                    array.push({
                        text: 'Movies with '+ actor[i].actorname,
                        url: actor[i].url
                    });
                }
                for (i = 0; i < dir_len; i++) {
                    array.push({
                        text: 'Movies directed by ' + director[i].dirname,
                        url: director[i].url
                    });
                }
                related_ds.appendData(array);

                utils.setDispatchController(controllerObj, 'movieDetailsController',{
                    related_ds: related_ds,
                    currentMediaType: currentMediaType,
                    currentTitle: videoItem.title,
                    currentPage: 'related'
                },
                skipHistory, historyItem);
            },

            moveToRelatedTelevision : function(videoItem, controllerObj, historyItem){
                if(!videoItem || !controllerObj){
                    return;
                }
                var utils  = telstra.sua.module.bpvod.utils;
                var isUndefined = accedo.utils.object.isUndefined;
                var skipHistory = false;
                if(!historyItem){
                    historyItem = {};
                    skipHistory = true;
                }
                
                var currentMediaType = (utils.isEpisode(videoItem) || utils.isSeason(videoItem) ? 'Television' : 'Movies' );

                //Go to related movies, we need to prepare the datasource of people for related
                var actor      = (isUndefined(videoItem.actors)||isUndefined(videoItem.actors.actor)) ?
                [] : videoItem.actors.actor,
                director   = (isUndefined(videoItem.directors)||isUndefined(videoItem.directors.director)) ?
                [] : videoItem.directors.director;

                if (!accedo.utils.object.isArray(actor)){
                    actor = [actor];
                }
                if (!accedo.utils.object.isArray(director)){
                    director = [director];
                }
                if (actor.length == 0 && director.length == 0){
                    accedo.console.log("cannot go to related search as there are no actors/directors");
                    return;
                }

                var related_ds = accedo.data.ds();

                var array = [], i, actor_len = actor.length, dir_len = director.length;
                for (i = 0; i < actor_len; i++) {
                    array.push({
                        text: 'Movies with '+ actor[i].actorname,
                        url: actor[i].url
                    });
                }
                for (i = 0; i < dir_len; i++) {
                    array.push({
                        text: 'Movies directed by ' + director[i].dirname,
                        url: director[i].url
                    });
                }
                related_ds.appendData(array);

                utils.setDispatchController(controllerObj, 'movieDetailsController',{
                    related_ds: related_ds,
                    currentMediaType: currentMediaType,
                    currentTitle: videoItem.title,
                    currentPage: 'related'
                },
                skipHistory, historyItem);
            },

            moveToForgotPin : function(controllerObj, historyItem, callback){
                if(!historyItem){
                    historyItem = {};
                }
                if(!callback){
                    callback = function(){};
                }
                var utils  = telstra.sua.module.bpvod.utils;
                utils.setDispatchController(controllerObj, 'forgotPinController',{
                    callback: callback
                }, false, historyItem);

            },

            validateUser : function(username, password, callback, failcallback){

                var loading = telstra.sua.core.components.loadingOverlay,
                utils  = telstra.sua.module.bpvod.utils;

                telstra.api.manager.movies.login(username, password, {
                    onFailure: function(json){
                        if(failcallback){
                            failcallback(json);
                        }else{
                            utils.errorMessageokPopup(json);
                        }
                    },
                    onComplete : function(){
                        loading.turnOffLoading();
                    },
                    onSuccess: function(resp){
                        callback(resp);
                    }
                }, true);
            },

            signInAction : function(username, password, callback, useToken, failcallback){

                var loading = telstra.sua.core.components.loadingOverlay,
                utils  = telstra.sua.module.bpvod.utils;
                
                var postSignInCallback = function(){

                    //would there be some mechanisms to avoid redundant API call.
                    loading.turnOnLoading();
                    telstra.api.manager.movies.getAccount({
                        onSuccess: function(){
                            try{
                                if (useToken){
                                	telstra.sua.module.bpvod.session.isSignedIn = true;
                                }
                                telstra.sua.module.bpvod.utils.loadRentedMovieList(function(){ 
                                    callback();
                                    loading.turnOffLoading();
                                });
                            }catch(e){
                                accedo.console.log(e);
                            }
                        },
                        onFailure: function(json){
                            loading.turnOffLoading();

                            if(useToken){
                                telstra.api.manager.movies.accountId = null;
                                telstra.api.manager.movies.authToken = null;
                                utils.saveAutoSignIn();
                                callback(); //callback as if it doesn't signed in
                            }else{
                                utils.errorMessageokPopup(json);
                            }
                        },
                        onComplete : function(){
                            loading.turnOffLoading();
                        }
                    });
                }

                if(!useToken){
                    loading.turnOnLoading();
                    telstra.api.manager.movies.login(username, password, {
                        onSuccess: function(resp){
                            //utils.saveAutoSignIn(); - moved to the place where the signin process finished
                            postSignInCallback();
                        },
                        onFailure: function(json){
                            loading.turnOffLoading();
                            
                            if(failcallback){
                                failcallback(json);
                            }else{
                                utils.errorMessageokPopup(json);
                            }
                        },
                        onComplete : function(){
                            loading.turnOffLoading();
                        }
                    });
                }else{
                    telstra.api.manager.movies.accountId = username;
                    telstra.api.manager.movies.authToken = password;
                    postSignInCallback();
                }
            },

            loadAutoSignIn : function(callback){

                var session = telstra.sua.module.bpvod.session,
                utils  = telstra.sua.module.bpvod.utils;
                
                telstra.api.manager.time.getTimeNow(function(){
                    accedo.console.log("loadAutoSignIn");
                    if(telstra.sua.module.bpvod.session.isSignedIn || accedo.device.manager.identification.getDeviceId() != "lg"){
                        callback();
                        return;
                    }

                    if(!callback){
                        callback = function(){};
                    }

                    if(!session.autoSignInData || session.autoSignInData == null){
                        accedo.console.log("autoSignIn storage initializing");
                        //create storage object
                        session.autoSignInData = accedo.device.manager.system.storage();
                        if(session.autoSignInData){
                            session.autoSignInData.initialize("AccedoServiceBigPondMovie_LG");
                        }
                    }

                    accedo.console.log("load autoSignIn Data");
                    try{
                        var storageLoaded = session.autoSignInData.load();
                        if(storageLoaded){
                            var accountid = session.autoSignInData.get("accountid");
                            var authtoken = session.autoSignInData.get("authtoken");
                            accedo.console.log("////////accountid: " + accountid + " authtoken: " + authtoken);

                            if(accedo.utils.object.isUndefined(accountid) || accedo.utils.object.isUndefined(authtoken) || accountid == null || authtoken == null){
                                accedo.console.log("couldn't load accountid and authtoken");
                            }
                            else{
                                accedo.console.log("success loaded, carry out sign in action");
                                utils.signInAction(accountid, authtoken, callback, true);
                                return;
                            }
                        }
                        else{
                        	accedo.console.log("//////////// Storage NOT FOUND!!!!");
                        }
                    }catch(e){
                        accedo.console.log(accedo.utils.object.toJSON(e));
                    }
                    accedo.console.log("call callback");
                    //callback if it didn't return before
                    callback();
                });
            },

            saveAutoSignIn : function(){
                var session = telstra.sua.module.bpvod.session;
                if(session.autoSignInData != null){
                    session.autoSignInData.set("accountid", telstra.api.manager.movies.accountId);
                    session.autoSignInData.set("authtoken", telstra.api.manager.movies.authToken);
                    session.autoSignInData.save();
                }
            },
            
            signOutAction : function(callback){
            	if(telstra.sua.module.bpvod.session.isSignedIn || telstra.sua.module.bpvod.session.signedInButNotFinishedProcess){
                    var loading = telstra.sua.core.components.loadingOverlay,
                    utils  = telstra.sua.module.bpvod.utils;
                    loading.turnOnLoading();
                    telstra.api.manager.movies.logout({
                        onSuccess: function(resp){
                            telstra.sua.module.bpvod.session.isSignedIn = false;
                            telstra.sua.module.bpvod.session.signedInButNotFinishedProcess = false;
                            telstra.sua.module.bpvod.session.recentlyExpiredMovieList = [];
                            telstra.sua.module.bpvod.session.lastViewVideoItem = null;
                            utils.saveAutoSignIn();
                            callback();
                            loading.turnOffLoading();
                        },
                        onFailure: function(json){
                            loading.turnOffLoading();
                            utils.errorMessageokPopup(json);
                        },
                        onComplete : function(){
                            loading.turnOffLoading();
                        }
                    });                    
                }else{
                    callback();
                }
            }
            
        }
    }
    );