/**
 * @fileOverview optionField
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 */

accedo.define(
        "telstra.sua.module.bpvod.components.optionField", [
        "accedo.utils.object", "accedo.utils.dom", "accedo.ui.label", "accedo.ui.component","accedo.ui.layout.absolute",
        "telstra.sua.module.bpvod.config"],
function() {

    /**
     *
     * @class telstra.sua.module.bpvod.components.optionField
     * @extends accedo.ui.component
     * @constructor
     * @param {Object} opts The options object
     * @param {Array} opts.options The string array label to use for options
     */
    return function(opts) {
        
        // shorten namespacing
        var domE     = accedo.utils.dom.element,
            image    = accedo.ui.image,
            layout   = accedo.ui.layout,
            label    = accedo.ui.label,
            button   = accedo.ui.button,
            config   = telstra.sua.module.bpvod.config,

        // base image path
            img_base = 'images/module/bpvod/pages/search/',
        
        // private variables
            selectAsFocus = config.keypadSelectAsFocus,
            selected = false,
            selectable = true,
            toolBox = false,
            t9mode = false,
            defaultNumberInput = false,
            options = opts.options,
            currentOption = 0,
            modal = false,
            optionfieldbackground = null,
            labelContainer = null,
            leftArrContainer = null,
            rightArrContainer = null,
            leftScroller = null,
            rightScroller = null;
        
        opts.options = opts.options || [''];

        opts.root = accedo.utils.dom.element('div', {id:'bpvod-optionfield-base'});

        opts.focusable = true; //Explicit definition of object being focusable

        opts.nextRight = function(){
            accedo.console.info('optionField nextRight called');
            obj.onKeyRight();
        };

        opts.nextLeft = function(){
            accedo.console.info('optionField nextLeft called');
            obj.onKeyLeft();
        };
        
        opts.rightClickHandler = function(){
            obj.onKeyRight(true);
        };

        opts.leftClickHandler = function(){
            obj.onKeyLeft(true);
        };

        /**
         * Set-up inheritance.
         * @scope telstra.sua.module.bpvod.components.optionField
         */
        var obj = accedo.utils.object.extend(accedo.ui.component(opts), {
            /**
             * Initialize option field
             *
             */
            init: function() {

                optionfieldbackground = layout.absolute({
                    parent: this,
                    id:     'optionfieldbackground',
                    css:    'optionFieldBackground',
                    children: [
                        {
                            type: label,
                            css:  'left'
                        },
                        {
                            type: label,
                            id:   'middle',
                            css:  'middle'
                        },
                        {
                            type: label,
                            css:  'right'
                        }
                    ]
                });

                optionfieldbackground.getById('middle').getRoot().setStyle({width:(parseInt(opts.width,10)-32)+'px'});

                labelContainer = layout.absolute({
                    parent: this,
                    id:     'labelContainer',
                    css:    'labelContainer',
                    children: [
                        {
                            type:   label,
                            id:     'options_label',
                            text:   opts.options[0]
                        }
                    ]
                });
                leftArrContainer = layout.absolute({
                    parent: this,
                    id:     'leftArrContainer',
                    css:    'leftArrContainer',
                    children: [
                        {
                            type: button,
	                        id:   'leftScroller',
	                        css:  'leftRightElement left inactive'
                        }
                    ]
                });
                rightArrContainer = layout.absolute({
                    parent: this,
                    id:     'rightArrContainer',
                    css:    'rightArrContainer',
                    children: [
                        {
                            type: button,
	                        id:   'rightScroller',
	                        css:  'leftRightElement right inactive'
                        }
                    ]
                });
                
                leftScroller = leftArrContainer.getById('leftScroller');
                rightScroller = rightArrContainer.getById('rightScroller');
                
                if (options.length > 1) {
                    this.setRightFocus();
                }

                currentOption = 0;
                modal = false;
                return this;
            },

            /**
             *
             */
            setLeftFocus : function(){
                leftScroller.root.removeClass('inactive');
                leftScroller.root.addEventListener('click', opts.leftClickHandler, false);
            },

            /**
             *
             */
            setLeftUnFocus : function(){
                leftScroller.root.addClass('inactive');
            },

            /**
             *
             */
            setRightFocus : function(){
                rightScroller.root.removeClass('inactive');
                rightScroller.root.addEventListener('click', opts.rightClickHandler, false);
            },

            /**
             *
             */
            setRightUnFocus : function(){
                rightScroller.root.addClass('inactive');
            },

            /**
             *
             */
            resetOption : function(){
                currentOption = 0;
                labelContainer.getById('options_label').setText(options[0]);
                this.setLeftUnFocus();
                if (options.length > 1) {
                    this.setRightFocus();
                }
            },

            /**
             *
             * @param curOption
             */
            setOption : function(curOption){
                currentOption = curOption;
                labelContainer.getById('options_label').setText(options[curOption]);
                if (curOption > 0) {
                    this.setLeftFocus();
                } else {
                    this.setLeftUnFocus();
                }
                if (curOption == options.length - 1) {
                    this.setRightUnFocus();
                } else {
                    this.setRightFocus();
                }
            },
            /**
             * Set the current option by providing the label of the current option
             * @param {String} curOptionLabel is the text label of the option to be selected
             * @return {Boolean} returns true if it can find the label and select it, returns false otherwise
             * @public
             */
            setOptionByLabel : function(curOptionLabel){
                var i=0;
                for (;i<options.length; i++){
                    if (options[i] == curOptionLabel){
                        this.setOption(i);
                        return true;
                    }
                }
                return false;
            },
            /**
             *
             */
            returnCurrentOption : function(){
                return options[currentOption];
            },

            /**
             *
             * @param e
             */
            onKeyEvent : function(e){
                switch(e) {
                    case "left":
                        return this.onKeyLeft();
                        break;
                    case "right":
                        return this.onKeyRight();
                        break;
                    case "blue":
                        return true;
                        break;
                    case "enter":
                        return this.onKeyEnter();
                        break;
                    case "tools":
                        return this.onKeyTools();
                        break;
                    case "back":
                        return this.onKeyBack();
                        break;
                    default:
                        // propagate events
                        if(!modal){
                            accedo.console.log("return true");
                            return true;
                        }

                }
            },

            /**
             *
             */
            onKeyBlue : function() {
                if (!t9mode) {
                    t9mode = true;
                }
            },

            /**
             *
             */
            onKeyLeft : function(fromMagicMotion){
                accedo.console.log("Option field left");
                if (selected || fromMagicMotion) {
                    if (currentOption > 0) {
                        currentOption--;
                        labelContainer.getById('options_label').setText(options[currentOption]);

                        this.setRightFocus();
                        if (currentOption == 0)
                            this.setLeftUnFocus();
                    }
                }
            },

            /**
             *
             */
            onKeyRight : function(fromMagicMotion){
                accedo.console.log("Option field right");
                if (selected || fromMagicMotion) {
                    if (currentOption < options.length - 1) {
                        currentOption++;
                        labelContainer.getById('options_label').setText(options[currentOption]);

                        this.setLeftFocus();
                        if (currentOption == options.length - 1)
                            this.setRightUnFocus();
                    }
                }
            },

            /**
             *
             */
            onKeyEnter : function(){
                if(t9mode){
                    t9mode = false;
                    return;
                }
                if(toolBox){
                    accedo.console.log("toolBox off");
                    toolBox = false;
                    return;
                }
                if(!selectAsFocus){
                    if(selected){
                        this.unselect();
                    }else{
                        this.select();
                    }
                }

            },

            /**
             *
             */
            onKeyTools : function() {
                if (!toolBox) {
                    accedo.console.log("toolBox on");
                    toolBox = true;
                }
            },

            /**
             *
             */
            onKeyBack : function(){
                if(toolBox){
                    accedo.console.log("toolBox off");
                    toolBox = false;

                }else{
//                    if (closeable) {
//                        return this.close();
//                    } else {
                        if(selected){
                            this.unselect();
                        }else{
                            return true;
                        }
//                    }
                }
            },


            /**
             *
             */
            onFocus : function(){
//                UIK.controller.registerHandler(this.onKeyEvent.bind(this));
                if(selectAsFocus){
                    this.select();
                }
            },

            /**
             *
             */
            onBlur : function(){
                if(selectAsFocus){
                    this.unselect();
                }
//                UIK.controller.unregisterHandler();
            },

            /**
             *
             */
            select : function(){
                accedo.console.log("input field select");

                selected = true;
                this.root.addClass("selected");

            },

            /**
             *
             */
            unselect : function(){
                accedo.console.log("input field unselect");

                selected = false;
                modal = false;
                this.root.removeClass("selected");
            },


            /**
             * This function dispatches a click event
             * @private
             */
            dispatchClickEvent: function() {
                this.dispatchEvent('click');
            }

        }).init();

        //Listen for browsers click event
        // opts.root.addEventListener('click', function() {
            // obj.dispatchClickEvent();
        // }, false);

        return obj;
    };
});
