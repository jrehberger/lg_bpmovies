/**
 * @fileOverview videoItemButton, an image button for style fit the BPVOD
 * original button layout Derived from accedo.ui.component
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 * @example var v = telstra.sua.module.bpvod.components.videoItemButton({css: 'css', src: 'path/to/img'});
 */

accedo.define(
    "telstra.sua.module.bpvod.components.videoItemButton", [
    "accedo.utils.object", "accedo.utils.dom", "accedo.ui.image", "accedo.ui.label", "accedo.ui.component",
    "telstra.sua.module.bpvod.config"],
function() {

    /**
     * Creates a new button instance.
     * Can dispatch "click" event
     * @class A button implementation
     * @extends accedo.ui.component
     * @constructor
     * @param {Object} opts The options object
     * @param {String} opts.src The image to use for the button
     */
    return function(opts) {

        opts.src  = opts.src || 'none';
        opts.text = opts.text || '';


        var img, label;

        /**
         * This function creates a button object.
         * @private
         */
        var create = function() {
            var element = accedo.utils.dom.element('div');
            element.addClass('bpvod-components-videoitembutton');

            img = accedo.ui.image({
                parent: element,
                src:    opts.src
            });
            if (telstra.sua.module.bpvod.config.currentMediaType === 'Television') {
                label = accedo.ui.label({
                    parent: element,
                    css:    'episodeLabel label',
                    text:   ''
                });
            }
            return element;
        };
        opts.root = create(opts);
        opts.focusable = true; //Explicit definition of object being focusable

        /**
         * Set-up inheritance.
         * @scope telstra.sua.module.bpvod.components.videoItemButton
         */
        var obj = accedo.utils.object.extend(accedo.ui.component(opts), {

            /**
             * This function dispatches a click event
             * @private
             */
            dispatchClickEvent: function() {
                this.dispatchEvent('click');
            },

            /**
             * This function sets the image source inside the button.
             * @param {String} text The image source string
             * @memberOf telstra.sua.module.bpvod.components.videoItemButton#
             * @public
             * @example v.setSrc('path/to/image/src');
             */
            setSrc: function(text) {
                img.setSrc(text);
            },
            /**
             * This function sets the label text inside the button. Note that
             * This only work on if telstra.sua.module.bpvod.config.currentMediaType
             * on 'Television' mode
             * @param {String} text The text string to set to
             * @memberOf telstra.sua.module.bpvod.components.videoItemButton#
             * @public
             * @example v.setText('Episode 1<br>2011');
             */
            setText: function(text) {
                if (!accedo.utils.object.isUndefined(label)) {
                    label.setText(text);
                }
            },
            /**
             * This function sets the image css style sheet inside the button.
             * @param {String} text The css class string to set to
             * @memberOf telstra.sua.module.bpvod.components.videoItemButton#
             * @public
             * @example v.setImgCss('white padding6px');
             */
            setImgCss: function(text) {
                img.getRoot().addClass(text);
            }
        });

        //Set initial text
        obj.setImgCss(opts.img_css);
        obj.setText(opts.text);
        obj.setSrc(opts.src);

        //Listen for browsers click event
        // opts.root.addEventListener('click', function() {
            // obj.dispatchClickEvent();
        // }, false);

        return obj;
    };
});
