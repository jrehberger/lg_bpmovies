/**
 * @fileOverview popupSortBy
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 */

accedo.define("telstra.sua.module.bpvod.components.popupSortBy", [
    "accedo.utils.object",
    "accedo.ui.popup", "accedo.ui.label", "accedo.ui.button",
    "accedo.ui.layout.absolute",
    "telstra.sua.module.bpvod.components.fluidButton"],
    function() {
        /**
     * @class
     * @extends accedo.ui.popup
     */
        return function(opts) {

            // shorten namespacing
            var label       = accedo.ui.label,
            layout      = accedo.ui.layout,
            fluidButton = telstra.sua.module.bpvod.components.fluidButton,

            // private variables
            cmpnentPl   = null,
            myPubObj    = null;

            /**
         * Set-up inheritance.
         * @scope telstra.sua.module.bpvod.components.popupConnectionInfo
         */
            myPubObj = accedo.utils.object.extend(accedo.ui.popup(opts), {
                init: function(){
                    cmpnentPl = layout.absolute({
                        css: 'panel vertical focus',
                        children: [
                        {
                            type: layout.absolute,
                            id:   'buttonOptionsPanel',
                            css:  'buttonOptionsPanel panel vertical focus',
                            children: [
                            {
                                type:     fluidButton,
                                id:       'ReleaseDate',
                                css:      'stripButton mini',
                                text:     'Release date',
                                width:    '306px',
                                nextDown: 'AlphabeticalOrder'
                            },
                            {
                                type:     fluidButton,
                                id:       'AlphabeticalOrder',

                                css:      'stripButton mini',
                                text:     'Alphabetical order',
                                width:    '306px',
                                nextUp:   'ReleaseDate',
                                nextDown: 'MostPopular'
                            },
                            {
                                type:     fluidButton,
                                id:       'MostPopular',
                                css:      'stripButton mini',
                                text:     'Most popular',
                                width:    '306px',
                                nextUp:   'AlphabeticalOrder',
                                nextDown: 'CancelButton'
                            }
                            ]
                        },
                        {
                            type:     fluidButton,
                            id:     'CancelButton',
                            css:    'cancelButton medium button',
                            width:  '200px',
                            text:   'Cancel',
                            nextUp: 'MostPopular'
                        }
                        ]
                    });

                    var self = this;
                    var header = label({
                        css:'header label',
                        text:'Sort By:'
                    });
                
                    this.addToContent(header);
                    this.addToContent(cmpnentPl);

                    if (!opts.showpopular) {
                        this.get('MostPopular').hide();

                        var alphabeticalOrder = this.get('AlphabeticalOrder');
                        var cancelButton = this.get('CancelButton');

                        alphabeticalOrder.setOption("nextDown", function() {
                            accedo.focus.manager.requestFocus(cancelButton);
                        });
                        cancelButton.setOption("nextUp", function() {
                            accedo.focus.manager.requestFocus(alphabeticalOrder);
                        });
                    }

                    this.get('ReleaseDate').addEventListener('click',function(){
                        accedo.console.info('Sort by:Release Date clicked');
                        myPubObj.dispatchEvent('bpvod:popupSortBy:onClose', {
                            sortby:'most recent',
                            sortOption: 'ReleaseDate'
                        });
                        self.close();
                    });
                    this.get('AlphabeticalOrder').addEventListener('click',function(){
                        accedo.console.info('Sort by:Alphabetical Order clicked');
                        myPubObj.dispatchEvent('bpvod:popupSortBy:onClose', {
                            sortby:'title',
                            sortOption: 'AlphabeticalOrder'
                        });
                        self.close();
                    });
                    this.get('MostPopular').addEventListener('click',function(){
                        accedo.console.info('Sort by:Most Popular clicked');
                        myPubObj.dispatchEvent('bpvod:popupSortBy:onClose', {
                            sortby:'popularity',
                            sortOption: 'MostPopular'
                        });
                        self.close();
                    });
                    this.get('CancelButton').addEventListener('click',function(){
                        accedo.console.info('Sort by:Cancel clicked');
                        myPubObj.dispatchEvent('bpvod:popupSortBy:onClose', {
                            sortby:''
                        });
                        self.close();
                    });
                
                    this.root.removeClass(opts.css);
                    this.root.addClass('accedo-ui-popupcontent');

                    return this;
                },

                onKeyBack : function(){
                    this.close();
                }
            }).init();

            return myPubObj;
        };
    });