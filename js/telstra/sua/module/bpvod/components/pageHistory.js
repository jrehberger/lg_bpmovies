/**
 * @fileOverview pageHistory
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Xie</a>
 */

accedo.define("telstra.sua.module.bpvod.components.pageHistory", [
    "accedo.utils.object","telstra.sua.module.bpvod.utils"],

/**
 * @class pageHistory is responsible for handling page history. Only one
 * pageHistory object available on whole bpvod life cycle
 * @constructor
 */
function(){
    /**
     * Shorten namespacing
     */
    var utils = telstra.sua.module.bpvod.utils,

    /**
     * Queue
     * @field
     * @private
     */
        queue = [],
        self = this;
    return {
        /**
         * item storing the required info for an history item
         * @param {Object} opts Options passed to be stored
         * @param {String} opts.page The controller would like to pass to
         * @param {Object} opts.data The data argument passed to to initialize
         *   during page start
         */
        item: function(opts) {
            return {
                page: opts.page,
                data: opts.data
            };
        },

        /**
         * add to history Queue
         * @param {Object} opts
         */
        add: function(opts) {
            queue.push(self.item(opts));
        },
        /**
         * reset the queue
         */
        clear: function() {
            queue = [];
        },
        /**
         * alias of clear()
         */
        clearTillLast: function() {
            self.clear();
        },
        /**
         * restore to previous page state
         */
        back: function() {
            var queue_len = queue.length,
                current_ctrller = accedo.app.getCurrentController().get("coreSubController").getInstance(),
                previousItem;
            accedo.console.info('History back: ' + queue_len);
            if (queue_len === 0) {
                utils.setDispatchController('mainController');
            } else {
                previousItem = queue.pop();
                // data presented, go back to previous page with argument pre-set
                if (!accedo.utils.object.isUndefined(previousItem.data) && previousItem.data) {
                    utils.setDispatchController(current_ctrller, previousItem.page, previousItem.data);
                // no data passing, just go back to previous page
                } else {
                    utils.setDispatchController(current_ctrller, previousItem.page);
                }
            }
        }
    };
});