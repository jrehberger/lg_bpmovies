/**
 * @fileOverview Video preview prox in the top right of listing and detail pages
 * @author <a href="mailto:roy.chan@accedobroadband.com">Roy Chan</a>
 */

accedo.define(
    "telstra.sua.module.bpvod.components.videoPreviewBox", [
    "accedo.utils.object", "accedo.utils.dom", "accedo.ui.label", "accedo.ui.component",
    "accedo.ui.layout.absolute", "accedo.ui.image",
    "telstra.sua.module.bpvod.components.fluidButton",
    "telstra.sua.module.bpvod.utils"
    ],
        
    function() {

        /**
     * Creates a new VideoPreviewBox instance
     * @class absolute layout
     * @extends accedo.ui.absolute
     * @constructor
     * @param {Object} opts The options object
     */
        return function(opts) {
            //video preview box items
            var subTitleTvShows, rentalPeriod, rentalDetailsLabel, rentalPeriodIcon,
            headerR, priceTagText, stillImage, classificationImage, subTitleFree, playOverlay,
            okButton, stillImageContainer,

            //private functions
            create, refreshPriceTag,
            
            //opts parameters
            currentPage, currentPageSpecial, currentMediaType, currentPrice, forceListingMode, imgTimeout,
            resetOkButton,
            mouseFocusable = false,
            obj, utils = telstra.sua.module.bpvod.utils;

        
            /**
         * This function creates a VideoPrevieBox object.
         * @private
         */
            create = function() {
                var element = accedo.utils.dom.element('div'),
                child = accedo.ui.layout.absolute({
                    parent: element,
                    children: [
                    {
                        type: accedo.ui.layout.absolute,
                        css:  'stillImageContainer',

                        children: [
	                        {
		                        type:	accedo.ui.image,
	                            css:	'stillImage image',
	                            id:		'stillImage',
	                            src:	'images/core/empty.png'
	                        },
	                        {
		                        type:	accedo.ui.image,
		                        css:	'classificationImage image',
		                        id:		'classificationImage',
		                        src:	'images/core/empty.png'
		                    },
		                    {
		                        type:	accedo.ui.label,
		                        id:		'playOverlay',
		                        css:	'playOverlay vishide'
		                    },
		                    {
		                        type:	accedo.ui.label,
		                        css:	'hoverBorder'
		                    }
                        ]
                    },
                    {
                        type:	accedo.ui.label,
                        css:	'title letterspacing003em',
                        id:		'headerR'
                    },
                    {
                        type:	accedo.ui.label,
                        css:	'subTitle label vishide',
                        text:	'Rental Details',
                        id:		'rentalDetailsLabel'
                    },
                    {
                        type:	accedo.ui.label,
                        css:	'subTitleFree label vishide',
                        text:	'This movie is available to watch for free.',
                        id:		'subTitleFree'
                    },
                    {
                        type:	accedo.ui.label,
                        css:	'subTitleTvShows label vishide',
                        id:		'subTitleTvShows'
                    },
                    {
                        type:	accedo.ui.label,
                        css:	'rentedStatus label vishide',
                        id:		'rentalStatusLabel'
                    },
                    {
                        type:	accedo.ui.label,
                        css:	'textContent label',
                        text:	'<sup>*</sup>Download charges may apply. Unmetered for most BigPond customers.'
                    },
                    {
                        type:	accedo.ui.label,
                        css:	'videoScreen playing'
                    },
                    {
                        type:	telstra.sua.module.bpvod.components.fluidButton,
                        css:	'okButton block green medium focused button',
                        id:		'okButton',
                        width:	'172px',
                        height:	'54px',
                        text:	'OK - More Info'
                    },
                    {
                        type:	accedo.ui.label,
                        css:	'rentalPeriodIcon vishide',
                        id:		'rentalPeriodIcon'
                    },
                    {
                        type:	accedo.ui.label,
                        css:	'rentalPeriodText label vishide',
                        id:		'rentalPeriod',
                        text:	''
                    },
                    {
                        type:	accedo.ui.label,
                        css:	'priceTagIcon'
                    },
                    {
                        type:	accedo.ui.label,
                        css:	'priceTagText label',
                        id:		'priceTagText',
                        text:	''
                    }
                    ]
                });
                //Video preview box items
                subTitleTvShows = child.getById('subTitleTvShows');
                rentalPeriod = child.getById('rentalPeriod');
                rentalDetailsLabel = child.getById('rentalDetailsLabel');
                rentalPeriodIcon = child.getById('rentalPeriodIcon');
                headerR = child.getById('headerR');
                priceTagText = child.getById('priceTagText');
                stillImage = child.getById('stillImage');
                classificationImage = child.getById('classificationImage');
                subTitleFree = child.getById('subTitleFree');
                playOverlay = child.getById('playOverlay');
                okButton = child.getById('okButton');
                element.okButton = okButton;
                
                return element;
            };
            /**
         * Refresh price tag (it's used when onSelect of Hlist)
         */
            refreshPriceTag = function(data, isEpisode){
                var price;
                if (accedo.utils.object.isUndefined(isEpisode)){
                    isEpisode = false;
                }
                if (data.product.price == '$0.00'){
                    obj.getRoot().addClass('freeVideoItem');
                    price = 'FREE';
                    if (currentPage != 'listing'){
                        okButton.setText("OK - Watch Now");
                    }
                }else{
                    obj.getRoot().removeClass('freeVideoItem');

                    if (isEpisode){
                        price = data.product.price.replace(/\$(\d+)\.(\d+)/g,
                            '<span class="bigText">$$$1</span>.$2*');
                        price = price + '<br/><span class="episodeText">EPISODE</span>';
                        priceTagText.getRoot().addClass('episode');
                    }else{
                        if (currentPage != 'listing') {
                            if (currentMediaType == 'Television')
                                okButton.setText("OK - Rent Episode");
                            else
                                okButton.setText("OK - Rent Movie");
                        }

                        price = data.product.price.replace(/\$(\d+)\.(\d+)/g,
                            '<span class="bigText">$$$1</span>.$2<sup>*</sup>');
                        priceTagText.getRoot().removeClass('episode');
                    }
                }
                priceTagText.setText(price);
                if (price == 'FREE'){
                    subTitleFree.setVisible(true);
                }
                return price;
            };
            resetOkButton = function(){
                if (currentMediaType == 'Television'){
                    if (currentPage == 'detail' && !forceListingMode){
                        okButton.setText('OK - Rent Episode');
                    }else{ // currentPage == 'listing' or 'related'
                        okButton.setText('OK - More Info');
                    }
                }else{ // currentMediType == 'Movies'
                    if (currentPage == 'detail'){
                        if (currentPrice && currentPrice == '$0.00'){
                            okButton.setText('OK - Watch Now');
                        } else{
                            okButton.setText('OK - Rent Movie');
                        }
                    } else {  // currentPage == 'listing' or 'related
                        okButton.setText('OK - More Info');
                    }
                }
            };
            setPreviwBoxActive = function(){
            	if (!stillImageContainer){
            		return;
            	}
            	stillImageContainer.addEventListener('click', obj.dispatchClickEvent, false);
		        stillImageContainer.addEventListener('mouseover', obj.dispatchMouseOverEvent, false);
		        stillImageContainer.addEventListener('mouseout', obj.dispatchMouseOutEvent, false);
            };
            opts.root = create(opts);

            /**
         * Set-up inheritance.
         * @scope accedo.ui.button
         */
            obj = accedo.utils.object.extend(accedo.ui.component(opts), {
                /**
             * Update the display of this video preview box according to the video item data supplied
             * @public
             * @param data contains the video item data that needs to be displayed
             */
                updateDisplay: function(data){
                    var titleR       = data.title + (data.yearprod ? ' [' + data.yearprod+']' : ''),
                    viewingPeriod,
                    isPriceRefreshed=false, price='';
                
                    if (data.product.viewingperiod){
                        viewingPeriod = parseInt(data.product.viewingperiod);
                        if (viewingPeriod > 72 && viewingPeriod % 24 ==0){
                            viewingPeriod = (viewingPeriod/24) + ' Days';
                        }else{
                            viewingPeriod =  viewingPeriod + ' Hours';
                        }
                    }
                
                    classificationImage.setSrc('images/core/empty.png');
                    stillImage.setSrc('images/core/empty.png');
                    
                    stillImageContainer = stillImage.getParent()
                    setPreviwBoxActive();
                
                    headerR.setText(titleR);
                    subTitleTvShows.setVisible(false);
                    rentalPeriod.setVisible(false);
                    rentalDetailsLabel.setVisible(false);
                    rentalPeriodIcon.setVisible(false);
                    subTitleFree.setVisible(false);
                    playOverlay.setVisible(false);
                    rentalPeriod.getRoot().removeClass('rented');
                    rentalPeriodIcon.getRoot().removeClass('rented');
                    resetOkButton();
                
                    //Regardles of it's TV/Movies, some have prices and some don't
                    if (accedo.utils.object.isUndefined(data.product.price)) {
                        //this is tv episode, an extra call has to be added to get prices
                        priceTagText.setText('');
                        telstra.api.manager.movies.getMediaItem(data.episodes.episodemediaitemid, {
                            onSuccess: function(data){
                                refreshPriceTag(data, true);
                            }
                        });
                    } else {
                        //this is a movie under Movie or Television(yes Television also has movie)
                        price = refreshPriceTag(data);
                        isPriceRefreshed = true;
                    }
                    if (data.rented){
                        rentalPeriod.setText("Ready to Watch");
                    
                        rentalPeriod.setVisible(true);
                        rentalDetailsLabel.setVisible(true);
                        rentalPeriod.getRoot().addClass('rented');
                        rentalPeriodIcon.getRoot().addClass('rented');
                        rentalPeriodIcon.setVisible(true);
                        okButton.setText("OK - Watch Now");
                    }else if ((currentPage == 'listing' || currentPage == 'related' || forceListingMode) && !accedo.utils.object.isUndefined(data.seasonnumber)){
                        subTitleTvShows.setText('Season '+ data.seasonnumber +
                            '<br/>Episode '+data.seasonepisodecount);
                        
                        subTitleTvShows.setVisible(true);
                    }else if (isPriceRefreshed && price == 'FREE'){
                        subTitleFree.setVisible(true);
                    }else if (!accedo.utils.object.isUndefined(viewingPeriod)){
                        rentalPeriod.setText(viewingPeriod);
                    
                        rentalPeriod.setVisible(true);
                        rentalDetailsLabel.setVisible(true);
                        rentalPeriodIcon.setVisible(true);
                    }
                    if (imgTimeout){
                        clearTimeout(imgTimeout);
                        imgTimeout = null;
                    }
                    imgTimeout = setTimeout(function(){
                        //this is to make sure the old images have been made to empty image,
                        // before loading the new images
                        stillImage.setSrc(telstra.api.manager.resizer(data.screenshotmedium, 325, 184, true));
                        classificationImage.setSrc(telstra.api.manager.resizer(data.classificationimagesmall, 167, 57, true));
                        if (data.trailers && data.trailers.trailer && data.trailers.trailer.trailerurl){
                            playOverlay.setVisible(true);
                            mouseFocusable = true;
                        }
                        else{
                        	mouseFocusable = false;
                        }
                    }, 200);
                },
                /**
             * Set the parameters for different behaviour of displaying for different pages
             */
                setCurrentParams: function(params){
                    currentPage = params.currentPage;
                    currentPageSpecial = params.currentPageSpeical;
                    currentMediaType = params.currentMediaType;
                    currentPrice = params.currentPrice;
                    forceListingMode = params.forceListingMode;
                    resetOkButton();
                },
                
                getStillImageContainer: function(){
                    return stillImageContainer; 
                },
                
                dispatchClickEvent: function() {
	                obj.dispatchEvent('click');
	            },
	            
	            dispatchMouseOverEvent: function() {
	                if (!mouseFocusable){
	                	return;
	                }
	                accedo.focus.managerMouse.requestFocus(stillImageContainer);
	            },
	            
	            dispatchMouseOutEvent: function() {
	                if (!mouseFocusable){
	                	return;
	                }
	                accedo.focus.managerMouse.releaseFocus();
	            },
            });

            return obj;
        };
    });
