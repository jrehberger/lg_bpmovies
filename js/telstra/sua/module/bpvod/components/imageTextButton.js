/**
 * @fileOverview imageTextButton
 * original button layout Derived from accedo.ui.component
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 * @example var button = telstra.sua.module.bpvod.components.imageTextButton({
 *              css: 'css', src: 'path/to/img', text: 'a sample text'
 *              });
 */

accedo.define(
    "telstra.sua.module.bpvod.components.imageTextButton", [
    "accedo.utils.object", "accedo.utils.dom", "accedo.ui.image", "accedo.ui.label", "accedo.ui.container", "accedo.focus.managerMouse"],
function() {

    /**
     * Creates a new button instance.
     * Can dispatch "click" event
     * @class A button implementation
     * @extends accedo.ui.component
     * @constructor
     * @param {Object} opts The options object
     * @param {String} opts.src The image to use for the button
     * @param {String} opts.text The text for the button label
     */
    return function(opts) {

        if (accedo.utils.object.isUndefined(opts)) opts = {};
        opts.text = opts.text || '';
        opts.src  = opts.src  || '';

        var viewObj, defaultView;

        viewObj = null;

        defaultView = {
            type:          accedo.ui.layout.absolute,
            id:            "bpvod-viewbutton-layout",
            css:           "bpvod-viewbutton-layout " + opts.css||"",

            children:[
                {
                    type:   accedo.ui.image,
                    css:    'accedo-ui-image',
                    src:    opts.src
                },
                {
                    type:   accedo.ui.label,
                    css:    'accedo-ui-label',
                    text:   opts.text
                }
            ]
        };

        opts.view = opts.view || defaultView;
        opts.root = accedo.utils.dom.element('div', {id:'bpvod-viewbuttoncontainer'});
        opts.focusable = opts.focusable || true;

        /**
         * Set-up inheritance.
         * @scope telstra.sua.module.bpvod.components.imageTextButton
         */
        var obj = accedo.utils.object.extend(accedo.ui.container(opts), {

            /**
             * This function dispatches a click event
             * @private
             */
            dispatchClickEvent: function() {
                obj.dispatchEvent('click');
            },
            
            dispatchMouseOverEvent: function() {
                accedo.focus.manager.requestFocus(obj);
                accedo.focus.managerMouse.requestFocus(opts.root);
            },
            
            dispatchMouseOutEvent: function() {
                accedo.focus.managerMouse.releaseFocus();
            },

            /**
             * Append element to popup container
             * @param {accedo.ui.component | accedo.utils.dom} el the element to
             *   append to
             * @example var button = accedo.ui.button({text'ok'});popup.addContent(button);
             */
            addToContent: function(el)
            {
                if (viewObj == null)
                {
                    this.setView(defaultView);
                }
                viewObj.append(el);
            },

            /**
             * Set current view.
             * @param {Object} view JSON definition of the view.
             * @memberOf accedo.ui.controller#
             */
            setView: function(view)
            {
                //Assign parent
                view.parent = this;//opts.root;

                //Instantiate view
                viewObj = view.type(view);
            },

            /**
             * Get component by it's ID. Note that since a popup is not attached
             * to the root of the controller launched it, we have to rely on
             * this method to get the component on the popup
             * @param {String} id Elements ID
             * @return {accedo.ui.component} Component selected by ID or null
             * @memberOf accedo.ui.controller#
             */
            get: function(id)
            {
                return viewObj.getById(id);
            }
        }).containerInit();

        obj.setView(opts.view);
        
        opts.root.addEventListener('click', obj.dispatchClickEvent, false);
        opts.root.addEventListener('mouseover', obj.dispatchMouseOverEvent, false);
        opts.root.addEventListener('mouseout', obj.dispatchMouseOutEvent, false);
        
        //Listen for browsers click event
        // opts.root.addEventListener('click', function() {
            // obj.dispatchClickEvent();
        // }, false);

        return obj;
    };
});
