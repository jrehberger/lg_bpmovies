/**
 * @fileOverview Fluid button object, a clone to accedo.ui.button, just for 100%
 *   similiar to the original one as Reuben requested.
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 */

accedo.define(
    "telstra.sua.module.bpvod.components.fluidButton", [
    "accedo.utils.object", "accedo.utils.dom", "accedo.ui.label", "accedo.ui.component", "accedo.focus.managerMouse"],
    function() {

        /**
     * Creates a new button instance.
     * Can dispatch "click" event
     * @class A button implementation
     * @extends accedo.ui.component
     * @constructor
     * @param {Object} opts The options object
     * @param {String} opts.text The text label to use for the button
     */
        return function(opts) {

            opts.text = opts.text || '';
            var label,left,middle,right,element;

            /**
         * This function creates a button object.
         * @private
         */

            var create = function() {
                element = accedo.utils.dom.element('div');
                element.addClass("bpvod-fluidbutton");
                left = accedo.utils.dom.element('div');
                left.addClass("left");
                middle = accedo.utils.dom.element('div');
                middle.addClass("middle");
                right = accedo.utils.dom.element('div');
                right.addClass("right");
                element.appendChild(left);
                element.appendChild(middle);
                element.appendChild(right);

                label = accedo.ui.label({
                    parent: middle
                });

                return element;
            };

            opts.root = create(opts);
            opts.focusable = true; //Explicit definition of object being focusable

            /**
         * Set-up inheritance.
         * @scope accedo.ui.button
         */
            var obj = accedo.utils.object.extend(accedo.ui.component(opts), {

                /**
             * Thess functions dispatch click, mouseover and mouseout events
             * @private
             */
                dispatchClickEvent: function() {
                    obj.dispatchEvent('click');
                },
                dispatchMouseOverEvent: function() {
	                accedo.focus.managerMouse.requestFocus(opts.root);
	            },
	            dispatchMouseOutEvent: function() {
	                accedo.focus.managerMouse.releaseFocus();
	            },

                /**
             * This function sets the width of the button
             * @param {number} new width
             * @public
             */
                setWidth: function(width) {
                    var lr = parseInt(left.getStyle("width")) + parseInt(right.getStyle("width"));
                    lr = (isNaN(lr)? 0 : lr);
                    middle.setStyle({"width":(parseInt(width)-lr) +"px"});
                    element.setStyle({"width":width});
                },

                /**
             * This function sets the text label of the button.
             * @param {String} text The text label
             * @memberOf accedo.ui.button#
             * @public
             */
                setText: function(text) {
                    label.setText(text || '');
                }
            });

            //Set initial text
            obj.setText(opts.text);
            
            opts.root.addEventListener('click', obj.dispatchClickEvent, false);
            opts.root.addEventListener('mouseover', obj.dispatchMouseOverEvent, false);
            opts.root.addEventListener('mouseout', obj.dispatchMouseOutEvent, false);

            accedo.utils.fn.delay(function() {
                obj.setWidth(opts.width);
            }, 0.01);

            return obj;
        };
    });
