/**
 * @fileOverview Fluid button object, a clone to accedo.ui.button, just for 100%
 *   similiar to the original one as Reuben requested.
 * @author <a href="mailto:victor.leung@accedobroadband.com">Victor Leung</a>
 */

accedo.define(
    "telstra.sua.module.bpvod.components.purchaseHistoryItem", [
    "accedo.utils.object", "accedo.utils.dom", "accedo.ui.label", "accedo.ui.component", ],
    function() {

        /**
     * Creates a new button instance.
     * Can dispatch "click" event
     * @class A button implementation
     * @extends accedo.ui.component
     * @constructor
     * @param {Object} opts The options object
     * @param {String} opts.text The text label to use for the button
     */
        return function(opts) {

            var element, labelDataRefNo, labelDataTitle, labelDataDevice, labelDataDate, labelDataPrice;

            /**
         * This function creates a button object.
         * @private
         */

            var create = function(opts) {
                element = accedo.utils.dom.element('div');
                element.addClass("historyLinePanel panel horizontal");
           
                labelDataRefNo = accedo.ui.label({
                    parent: element,
                    id: "labelDataRefNo" + opts.index,
                    css: "labelDataRefNo"
                });

                labelDataTitle = accedo.ui.label({
                    parent: element,
                    id: "labelDataTitle" + opts.index,
                    css: "labelDataTitle"
                });

                labelDataDevice = accedo.ui.label({
                    parent: element,
                    id: "labelDataDevice" + opts.index,
                    css: "labelDataDevice"
                });

                labelDataDate = accedo.ui.label({
                    parent: element,
                    id: "labelDataDate" + opts.index,
                    css: "labelDataDate"
                });

                labelDataPrice = accedo.ui.label({
                    parent: element,
                    id: "labelDataPrice" + opts.index,
                    css: "labelDataPrice"
                });

                return element;
            };

            opts.root = create(opts);
            opts.focusable = true; //Explicit definition of object being focusable

            opts['nextUp'] = function() {
                //Move selection up
                obj.moveSelection(true);
            };

            //Define nextRight or nextDown, nextLeft or nextUp as internal selection change
            opts['nextDown'] = function() {
                //Move selection down
                obj.moveSelection(false);
            };


            var obj = accedo.utils.object.extend(accedo.ui.component(opts), {

                moveSelection: function(reverse) {
                    //Notify the list did move (some UI effects can be triggered based on that)
                    obj.dispatchEvent('accedo:list:moved', {
                        reverse: reverse
                    });
                },

                setText: function(textDataRefNo, textDataTitle, textDataDevice, textDataDate, textDataPrice) {
                    labelDataRefNo.setText(textDataRefNo);
                    labelDataTitle.setText(textDataTitle);
                    labelDataDevice.setText(textDataDevice);
                    labelDataDate.setText(textDataDate);
                    labelDataPrice.setText(textDataPrice);
                }
            });

            return obj;
        };
    });
