/**
 * @fileOverview A UI Label object, providing basic functionality.
 * @author <a href="mailto:victor.leung@accedo.tv">Victor Leung</a>
 */

accedo.define("telstra.sua.module.bpvod.components.label", ["accedo.utils.object", "accedo.utils.dom", "accedo.ui.component"], function() {

    /**
     * Creates a new label instance.
     * @name label
     * @class A label implementation
     * @extends accedo.ui.component
     * @constructor
     * @param {Object} opts The options object
     * @param {String} opts.text The text label
     */
    return function(opts) {

        /**
         * Internal storage for label text.
         * @field
         * @private
         */
        opts.text = opts.text || '';

        opts.root = accedo.utils.dom.element('div');
        opts.root.addClass('accedo-ui-label');

        var table = accedo.utils.dom.element('table', {
            'width': '100%',
            'height': '100%'
        }),
        tr = accedo.utils.dom.element('tr'),
        td = accedo.utils.dom.element('td', {
            'valign': 'middle'
        });

        tr.appendChild(td);
        table.appendChild(tr);
        opts.root.appendChild(table);

        var obj = accedo.utils.object.extend(accedo.ui.component(opts), {

            /**
             * Sets the text of the label.
             * @name setText
             * @function
             * @param {String} text The text to set
             * @public
             */
            setText: function(text) {
                td.setText(text);
            },

            /**
             * Gets the text of the label.
             * @name getText
             * @function
             * @public
             * @return {String} Text of the label
             */
            getText: function() {
                return td.getText();
            }

        });

        //Set initial text
        obj.setText(opts.text);

        return obj;
    };
});
