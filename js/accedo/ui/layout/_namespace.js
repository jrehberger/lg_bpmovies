/**
 * @fileOverview defines the namespace for accedo.ui.layout
 * 
 * 
 * @author Calle Gustafsson <calle.gustafsson@accedobroadband.com>
 */
/**
 * @name accedo.ui.layout
 * @namespace
 */