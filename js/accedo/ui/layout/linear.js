/**
 * @fileOverview Linear layout container
 * @author <a href="mailto:ted.bjorling@accedobroadband.com">Ted Bjorling</a>
 * Modified by Gregory Desfour to use div tags + CSS float instead of table tags
 * Note: The horizontal layout floats elements left as defined in xdk.css (using CSS class name accedo-ui-layout-linear-h)
 */

//= require <accedo/ui/container>
//= require <accedo/utils/object>
//= require <accedo/utils/array>
//= require <accedo/utils/dom>

accedo.define("accedo.ui.layout.linear", ["accedo.utils.object", "accedo.utils.array", "accedo.utils.dom", "accedo.ui.container"], function() {

    /**
     * Linear layout
     * @name linear
     * @memberOf accedo.ui.layout
     * @class This container is used to align child elements, either vertically
     *        or horizontally.
     * @constructor
     * @extends accedo.ui.container
     * @param {Object} opts The options object
     * @param {Integer} [opts.orientation="accedo.ui.layout.linear.VERTICAL"] The orientation of the layout
     */
    var linear = function(opts) {

        /**
         * This function creates the base structure for a layout.linear container.
         * @private
         */
        var create = function() {
            var div = accedo.utils.dom.element('div');

            if (opts.orientation == linear.HORIZONTAL) {
                div.addClass('accedo-ui-layout-linear-h');
            } else {
                div.addClass('accedo-ui-layout-linear-v');
            }

            return div;
        };
        opts.root = create(opts);


        //Set default orientation
        if (!opts.orientation) {
            opts.orientation = linear.VERTICAL;
        }


        return accedo.utils.object.extend(accedo.ui.container(opts), {

            /**
             * This function returns the orientation of this layout.
             * @name getOrientation
             * @see accedo.ui.layout.linear.VERTICAL
             * @see accedo.ui.layout.linear.HORIZONTAL
             * @public
             * @memberOf accedo.ui.layout.linear#
             * @return {Integer} The orientation
             * @function
             */
            getOrientation: function() {
                return opts.orientation;
            },

            /**
             * This function is called by the container function whenever
             * a child element has been appended.
             * @name onAppend
             * @param {accedo.ui.component} child
             * @memberOf accedo.ui.layout.linear#
             * @public
             * @function
             */
            onAppend: function(child) {

                //Create a new slot for this child
                var div = accedo.utils.dom.element('div');
                
                div.addClass('accedo-ui-layout-linear-element');

                this.getRoot().appendChild(div);

                //Styles holder
                var divStyles = {};

                //Container styles
                var height = child.getOption('height');
                if (height) {
                    divStyles.height = height;
                }
                var width = child.getOption('width');
                if (width) {
                    divStyles.width = width;
                }
                
                var gravity = child.getOption('gravity');
                if (gravity & linear.GRAVITY_LEFT) {
                    divStyles.textAlign = 'left';
                } else if (gravity & linear.GRAVITY_RIGHT) {
                    divStyles.textAlign = 'right';
                } else if (gravity & linear.GRAVITY_CENTER) {
                    divStyles.textAlign = 'center';
                }

                //Effectively set the styles (child container)
                div.setStyle(divStyles);
                
                //Attaching child to it's parent should be after parent is in DOM, or focus amnagement will not work well.
                child.attachTo(div);
            },
            /**
             * This function is called when a child is removed
             * @name onRemove
             * @memberOf accedo.ui.layout.linear#
             * @param {accedo.ui.component} child
             * @function
             */
            onRemove: function(child) {
                //Remove parent DIV
                child.getParent().remove();
            }

        }).containerInit();

    };

    /**
     * This is used to indicate a vertical layout.
     * @name VERTICAL
     * @memberOf accedo.ui.layout.linear
     * @constant
     */
    linear.VERTICAL = 0x01;
    
    /**
     * This is used to indicate a horizontal layout.
     * @name HORIZONTAL
     * @constant
     * @memberOf accedo.ui.layout.linear
     */
    linear.HORIZONTAL = 0x02;

    /**
     * This places a child at:
     * <pre>
     *  - - -
     * |     |
     * |x    |
     * |     |
     *  - - -
     * </pre>
     * @name GRAVITY_LEFT
     * @constant
     * @memberOf accedo.ui.layout.linear
     */
    linear.GRAVITY_LEFT = 0x01;

    /**
     * This places a child at:
     * <pre>
     *  - - -
     * |     |
     * |    x|
     * |     |
     *  - - -
     * </pre>
     * @name GRAVITY_RIGHT
     * @constant
     * @memberOf accedo.ui.layout.linear
     */
    linear.GRAVITY_RIGHT = 0x02;

    /**
     * This places a child at:
     * <pre>
     *  - - -
     * |     |
     * |  x  |
     * |     |
     *  - - -
     * </pre>
     * @name GRAVITY_CENTER
     * @constant
     * @memberOf accedo.ui.layout.linear
     */
    linear.GRAVITY_CENTER = 0x04;

    /**
     * This places a child at:
     * <pre>
     *  - - -
     * |  x  |
     * |     |
     * |     |
     *  - - -
     * </pre>
     * @name GRAVITY_TOP
     * @constant
     * @memberOf accedo.ui.layout.linear
     */
    linear.GRAVITY_TOP = 0x08;

    /**
     * This places a child at:
     * <pre>
     *  - - -
     * |     |
     * |     |
     * |  x  |
     *  - - -
     * </pre>
     * @name GRAVITY_BOTTOM
     * @constant
     * @memberOf accedo.ui.layout.linear
     */
    linear.GRAVITY_BOTTOM = 0x10;

    /**
     * This places a child at:
     * <pre>
     *  - - -
     * |     |
     * |  x  |
     * |     |
     *  - - -
     * </pre>
     * @name GRAVITY_MIDDLE
     * @constant
     * @memberOf accedo.ui.layout.linear
     */
    linear.GRAVITY_MIDDLE = 0x20;

    return linear;

});
