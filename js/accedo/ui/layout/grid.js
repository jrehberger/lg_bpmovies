/**
 * @fileOverview Grid layout container.
 * @author <a href="mailto:mattias.lindvall@accedobroadband.com">Mattias Lindvall</a>
 */

//= require <accedo/ui/container>
//= require <accedo/utils/object>
//= require <accedo/utils/array>
//= require <accedo/utils/dom>
//accedo.require('accedo.utils.object');
//accedo.require('accedo.utils.dom');
//accedo.require("accedo.utils.array");
//accedo.require('accedo.ui.container');
//
//accedo.provide('accedo.ui.layout.grid');
//accedo.provide('accedo.ui.layout.grid.row');


//accedo.ui.layout.grid = function(opts) {


accedo.define("accedo.ui.layout.grid", [
    "accedo.utils.object",
    "accedo.utils.dom",
    "accedo.utils.array",
    "accedo.ui.container",
    "accedo.ui.layout.gridRow"
], function() {

    /**
     * Generates a table grid
     *@name grid
     *@memberOf accedo.ui.layout
    * @class Generate a table grid.
    * @constructor
    * @extends accedo.ui.container
    * @param {Object} opts The options object
    * @param {Object} [opts.tableStyle] Styles to be applied on the table.
    * @param {Object} [opts.defaultAlignment] Default alignment on be applied to each table cell.
    */
    return function(opts) {

        opts.root = accedo.utils.dom.element('table');

        var tableStyle = opts.tableStyle;
        opts.root.setStyle(tableStyle);

        opts.root.setAttributes({
            cellpadding: 0, cellspacing: 0
        });

        var temp = [];

        // for all children
        accedo.utils.array.each(opts.children, function(child, index, arr){

            // if using short hand row definitions
            if ( accedo.utils.object.isArray(child) ) {
                // transform into a Row object
                child = {
                    type: accedo.ui.layout.gridRow,
                    children: child
                };
            }

            // extend each Row child with the default alignment option
            for ( var j = 0, l = child.children.length; j < l; j++ ) {
                accedo.utils.object.extend(child.children[j], { 'defaultAlignment': opts.defaultAlignment });
            }

            // store the new Row object
            temp.push(child);
        });

        opts.children = temp;

        return accedo.utils.object.extend(accedo.ui.container(opts), {

            /**
            * Handles defined children.
            * @name onAppend
            * @function
            * @param {accedo.ui.component} child
            * @memberOf accedo.ui.layout.grid
            * @public
            */
            onAppend: function(child) {
                //console.log(child)
                child.attachTo(this.getRoot());
            }

        }).containerInit();

    }
});

