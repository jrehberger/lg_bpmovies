/**
 * @fileOverview Absolute layout container.
 * @author <a href="mailto:ted.bjorling@accedobroadband.com">Ted Björling</a>
 */

//= require <accedo/ui/container>
//= require <accedo/utils/object>
//= require <accedo/utils/dom>

accedo.define("accedo.ui.layout.absolute", ["accedo.utils.object", "accedo.utils.dom", "accedo.ui.container"], function() {
    /**
     * @name absolute
     * @memberOf accedo.ui.layout
     * @constructor
     * @param {Object} opts params
     * @class This container is used when you would like to position child elements
     *        in an absolute manor. Child elements are positioned by defining 'x' and 'y'
     *        attributes. Both 'x' and 'y' are optional, if not defined the position will
     *          not be set here and the CSS style will apply (top and left 0 by default)
     * @extends accedo.ui.container
     */
    return function(opts) {

        opts.root = accedo.utils.dom.element('div');


        return accedo.utils.object.extend(accedo.ui.container(opts), {

            /**
             * This function is called by the container function whenever
             * a child element has been appended.
             * @name onAppend
             * @function
             * @param {accedo.ui.component} child
             * @memberOf accedo.ui.layout.absolute#
             * @public
             */
            onAppend: function(child) {
                child.attachTo(this.getRoot());

                var newStyles = {
                    'position': 'absolute'
                };

                //If we have a 'y' option use it
                var y = child.getOption('y');
                if (y) {
                    newStyles.top = y;
                }

                //If we have a 'x' option use it
                var x = child.getOption('x');
                if (x) {
                    newStyles.left = x;
                }

                //Height and width
                var height = child.getOption('height');
                if (height) {
                    newStyles.height = height;
                }

                var width = child.getOption('width');
                if (width) {
                    newStyles.width = width;
                }

                child.getRoot().setStyle(newStyles);

            }
        }).containerInit();

    };
});