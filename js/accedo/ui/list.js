/**
 * @fileOverview List container.
 * - Based on the linear layout (but only extending accedo.ui.container), the biggest
 *      difference is the List is linked to a datasource.
 * - Displays part or all of its content based on a user-defined template. Properties
 *      common to all children are defined in the view (see the childTemplate option), while
 *      the particulars of each child are defined in the controller and passed as a callback.
 *      Read details about it in the setDisplayHelper function comments.
 * - Also noteworthy is the fact it uses an internal focus so as not to mess with the
 *       main one. The CSS class used is 'selected' instead of 'focused', and the selected
 *       element can be distinguished even when the focus has moved out of the list.
 *       Similarly to onFocus and onBlur, you can define onSelect and onUnselect callbacks on
 *       the list's children.
 * - Supports CAROUSEL behaviour on option. Carousel-style lists can display less children than
 *      are present in the datasource like any REGULAR list, but they will also display more, by
 *      duplicating children, when there are more visible elements than data objects in the datasource.
 *      For CAROUSELs, the selected element always appears in the same position (unless calling setSelectedIndex),
 *      and any selection move scrolls the list. Also the last element is linked to the first one.
 *      A REGULAR list scrolls only when necessary and doesn't have this 'looping' behaviour.
 * - Supports association with a accedo.ui.scrollbar via the public setScrollbar function.
 * - Dispatches 'accedo:list:moved' event with a 'reverse' indicator when the list's selection is successfully changed
 *      by a key input or a manual call to moveSelection
 * - Options: those supported by any container, and :
 *    - orientation   (accedo.ui.list.HORIZONTAL (default) | accedo.ui.list.VERTICAL)
 *    - behaviour     (accedo.ui.list.REGULAR (default) |  accedo.ui.list.CAROUSEL)
 *    - visibleSize   (Number (default 1)) How many children to display. 0 means all of them.
 *    - preloadSize   (Number (default 0)) How many children to preload in advance (created but not displayed).
 *    - noCache       (boolean (default false)) If true, the list's cache will not be used, and removed children will be deinited immediately.
 *    - scrollStep    (Number (default 1)) For REGULAR lists only, How many elements to scroll when scrolling the list.
 *    - loop          (boolean (default false)) For REGULAR lists only, do we link the selection of the child at the backward
 *                  border to the one at the forward border, and vice-versa. NOTE when going left or up from the first child,
 *                  you will end up selecting the last child loaded by the datasource (not necessarily the last 'loadable' one).
 *    - scrollBeforeBorder (Number | Array (default [0,0])) For REGULAR lists only, try to scroll when the selection is N or
 *                  less children away from the border.
 *                  If the option is a number, N is this number.
 *                  If the option is an Array, N is the first number in the Array when moving backward, and
 *                                                  the second number in the array when moving forward.
 *    - forceSelectOnFocus (boolean (default false)) If true, when the list gets focused it will launch the onSelect callback
 *                  even if the selection was already set.
 *    - firstSelectedIndex  (Number (default 0)) The child at the specified list index will be selected when the list is focused
 *                  for the 1st time. Use -1 to set it to the last child.
 *    - childTemplate (Object)  Template used to create every child of the list, for example:
 *                              {
 *                                  type:           accedo.ui.image,
 *                                  src:            'path/to/default/image.png', //compulsory attribute!
 *                                  css:            'BPMovies_mediaitem',
 *                                  height:         '225px',
 *                                  width:          '156px'
 *                              }
 *                  
 * - Notes:
 *    - You can change the datasource dynamically (the list's children and cache will be emptied)
 *    - You can grow or shrink the list's size dynamically (the selected child will not be removed)
 *    - You can deactivate/reactivate the onSelect callback when selecting a child (activateCallbackOnSelect(boolean))
 *    - The nextLeft/nextRight (for a horizontal list), nextUp/nextUp (for a vertical list) options
 *      will be overwritten to manage the list's internal selection, however, the value you specified
 *      will be used to focus another element when the current selection is a REGULAR list's border.
 *
 * @author <a href="mailto:gregory.desfour@accedobroadband.com">Gregory Desfour</a>
 */

accedo.define("accedo.ui.list", [
    "accedo.utils.object",
    "accedo.utils.array",
    "accedo.utils.dom",
    "accedo.utils.fn",
    "accedo.ui.container"],
function() {

    /**
     * @name list
     * @memberOf accedo.ui
     * @class This container is used to align child elements, either vertically
     *        or horizontally.
     * @constructor
     * @extends accedo.ui.container
     * @param {Object} opts The options object
     * @param {Integer} [opts.orientation="accedo.ui.list.VERTICAL"]
     */
    var list = function(opts) {
        /**
         * @ignore
         */
        var create, datasource, scrollbar, displayHelperFn, isAllVisible, getCurrentSize,
            myPublicObject, onChildFocus, createChild, onDatasourceAppend, updateScrollbar,
            nextRightDown, nextLeftUp, selectedChild, selectChild, iteratorChildId,
            getFirstChild, getLastChild, generateChildId, onClick, isCarousel, scroll,
            moveCarousel, getChildAtIndex, legacy_nextRightDown, legacy_nextLeftUp,
            getCache, setCache, cache, attachChild, fetchChild, getBorderIndex, unselect,
            getCorrectedIndex, onDetach, onAttach, askForMoreData, waitForMoreData,
            triggerOnSelect, additionalOnFocus, selectRequest, callbackOnSelect = true,
            animationFn, noCache;
        /**
         * This function creates the base structure for a list container.
         * @private
         * @ignore
         */
        create = function() {
            var div = accedo.utils.dom.element('div');

            if (opts.orientation == list.HORIZONTAL) {
                div.addClass('accedo-ui-list accedo-ui-layout-linear-h');
            } else {
                div.addClass('accedo-ui-list accedo-ui-layout-linear-v');
            }

            return div;
        };
        
        //---INIT START---
        opts = opts || {};
        /**
         *@ignore
         */
        opts.root = create();
        
        //Focusable unless explicitely specified otherwise
        if (opts.focusable !== false) {
            opts.focusable = true;
        }
        
        //Set default values of properties not set or incorrect
        if (!('orientation' in opts)) {
            opts.orientation = list.HORIZONTAL;
        }
        
        if (!('firstSelectedIndex' in opts)) {
            opts.firstSelectedIndex = 0;
        }
        
        if (!('visibleSize' in opts)) {
            opts.visibleSize = 1;
        } else if (opts.visibleSize === 0) {
            isAllVisible = true;
        }
        
        if (!('preloadSize' in opts) || opts.preloadSize < 0 ) {
            opts.preloadSize = 0;
        }
        
        if (!('behaviour' in opts)) {
            opts.behaviour = list.REGULAR;
        }
        
        noCache = !!opts.noCache;
        
        //Behaviour-specific properties
        switch(opts.behaviour) {
            case list.REGULAR:
                if (!('scrollStep' in opts) || opts.scrollStep > opts.visibleSize) {
                    opts.scrollStep = 1;
                }
                
                //Turn scrollBeforeBorder into an array if necessary
                if (typeof opts.scrollBeforeBorder == 'number') {
                    opts.scrollBeforeBorder = [opts.scrollBeforeBorder, opts.scrollBeforeBorder];
                } else if (!('scrollBeforeBorder' in opts) || !opts.scrollBeforeBorder.length == 2) {
                    opts.scrollBeforeBorder = [0, 0];
                }
                
                break;
            case list.CAROUSEL:
                //Set the properties that are not for the user to define in a CAROUSEL
                opts.scrollStep = 1;
                isCarousel = true; //for convenience in the following code
                break;
        }
        
        //Iterator used to generate unique IDs for the created child components
        iteratorChildId = 0;
        
        //Cache array used for DOM preloading (will preload images too), empty at first
        cache = [];
        
        //Deduce the interesting focus direction to set
        if (opts.orientation == list.VERTICAL) {
            nextRightDown = 'nextDown';
            nextLeftUp = 'nextUp';
        } else {
            nextRightDown = 'nextRight';
            nextLeftUp = 'nextLeft';
        }

        //Save the user-defined params that we are going to override
        legacy_nextRightDown = opts[nextRightDown];
        legacy_nextLeftUp = opts[nextLeftUp];
        
        //Define nextRight or nextDown, nextLeft or nextUp as internal selection change
        nextRightDownFunc = function() {
            //Move selection forward
            if (!myPublicObject.moveSelection()) {
                //Moving was impossible. Do we loop?
                if (!isCarousel && opts.loop && getCurrentSize()) {
                    if (!waitForMoreData){
                        //select the 1st element for looping
                        myPublicObject.select(0);
                    }
                } else {
                    //No loop, return the legacy, user-defined ID (which will be focused)
                    return legacy_nextRightDown;
                }
            }
        };
        
        nextLeftUpFunc = function() {
            //Move selection backward
            if (!myPublicObject.moveSelection(true)) {
                //Moving was impossible. Do we loop?
                if (!isCarousel && opts.loop && getCurrentSize()) {
                    if (!waitForMoreData){
                        //looping to the last item in datasource
                        myPublicObject.select(datasource.getTotalItems()-1);
                    }
                } else {
                    //No loop, return the legacy, user-defined ID (which will be focused)
                    return legacy_nextLeftUp;
                }
            }
        };
        
        opts[nextRightDown] = nextRightDownFunc;
        opts[nextLeftUp] = nextLeftUpFunc;
        
        //---INIT END---
        
        /**
         * Correct the index if the list is a CAROUSEL and index is out of bounds
         * @returns Number the corrected index (or the original one if no correction is needed or the list is REGULAR)
         * @private
         * @memberOf accedo.ui.list#
         */
        getCorrectedIndex = function(index) {
            var dsSize = datasource && datasource.getCurrentSize();
            if (!dsSize) {
                return index;
            }
            if (isCarousel) {
                while (index >= dsSize) {
                    index -= dsSize;
                }
                while(index < 0) {
                    index += dsSize;
                }
            }
            return index;
        };
        
        /**
         * Shortcut function to get the current number of children (regardless of their visibility)
         * @returns {Number} the current number of children (regardless of their visibility)
         * @private
         * @memberOf accedo.ui.list#
         */
        getCurrentSize = function() {
            return myPublicObject.getChildren().length;
        };
        
        /**
         * Shortcut function to get the first child of the list
         * @returns {accedo.ui.component} the current first child, or null
         * @private
         * @memberOf accedo.ui.list#
         */
        getFirstChild = function() {
            if (getCurrentSize()) {  
                return myPublicObject.getChildren()[0];
            }
            return null;
        };
        
        /**
         * Shortcut function to get the last child of the list
         * @returns {accedo.ui.component} the current last child, or null
         * @private
         * @memberOf accedo.ui.list#
         */
        getLastChild = function() {
            var myLength = getCurrentSize();
            if (myLength) {  
                return myPublicObject.getChildren()[myLength-1];
            }
            return null;
        };
        
        /**
         * Shortcut function to get the n-th child of the list
         * @param {Number} index Index of the child to be returned
         * @returns {accedo.ui.component} the current n-th child, or null
         * @private
         * @memberOf accedo.ui.list#
         */
        getChildAtIndex = function(index) {
            var myLength = getCurrentSize();
            if (myLength && index >= 0 && index < myLength) {  
                return myPublicObject.getChildren()[index];
            }
            return null;
        };
        
        /**
         * This function groups the necessary actions to be done when a
         * child of this list is focused (so it is only is useful for focusable children).
         * Essentially what we do is, for ease of use, select this child in the list.
         * This allows selecting a child using the mouse instead of the keyboard...
         * for non-CAROUSEL lists only
         * @param {accedo.ui.component} child One of the list's children
         * @private
         * @memberOf accedo.ui.list#
         */
        onChildFocus = function(child) {
            //Select this child - not for carousels ! Controlling a carousel by mouse would be a headache to implement.
            if (!isCarousel) {
                selectChild(child, true);
            }
        };
        
        /**
         * This function groups the necessary actions to be done when the
         * associated datasource signals it has new children
         * @param {Object} evt Object with newData (Array) and fromIndex (number) properties.
         * @private
         * @memberOf accedo.ui.list#
         */
        onDatasourceAppend = function(evt) {
            var i, currentSize, totalSlotsToAdd, emptyVisibleSlots, visibleSlotsToAdd,
                child;
            
            //accedo.console.info('Currently ' + datasource.getCurrentSize() + ' elements, among which ' + evt.newData.length + ' new');
            //Ensure there's something new
            if (evt.newData && evt.newData.length) {
                
                //Notify we can ask for even more data when needed
                waitForMoreData = false;
                
                currentSize = getCurrentSize();
                            
                // totalSlotsToAdd = opts.visibleSize + opts.preloadSize - currentSize;
                
                /* Step 0:
                PRELOADED (unattached) child elements backward, ONLY if we had no data before (not useful otherwise).
                This will preload data on a carousel list.
                */
                if (!currentSize && !isAllVisible) {
                    for (i = -opts.preloadSize; i < 0; i++) {
                        // accedo.console.info('trying to cache index ' + (evt.fromIndex + i));
                        fetchChild(evt.fromIndex + i);
                    }
                }

                /* Step 1:
                VISIBLE child elements */
                emptyVisibleSlots = opts.visibleSize - currentSize;
                
                //Total number of VISIBLE elements to add. Note
                //Note that a CAROUSEL can create more slots than the number of objects in the datasource
                visibleSlotsToAdd = isAllVisible ? evt.newData.length : emptyVisibleSlots;
                
                /* Add the necessary children  */
                if (visibleSlotsToAdd >0 && visibleSlotsToAdd < opts.visibleSize && evt.fromIndex < getFirstChild().dsIndex){
                    //adding the children backward
                    for (i = 1; i <= visibleSlotsToAdd; i++) {
                        child = fetchChild(evt.fromIndex + evt.newData.length - i);
                        if (child) {
                            attachChild(child, true);
                        }
                    }
                    
                    /* Step 2:
                    Update the scrollbar */
                    updateScrollbar();

                    /* Step 3:
                    PRELOADED (unattached) child elements backward */
                    if (!isAllVisible) {
                        for (i = 1; i <= opts.preloadSize; i++) {
                            // accedo.console.info('trying to cache index ' + (evt.fromIndex + i + visibleSlotsToAdd));
                            fetchChild(evt.fromIndex + evt.newData.length - visibleSlotsToAdd -i);
                        }
                    }
                }else{
                    //adding the children forward
                    for (i = 0; i < visibleSlotsToAdd; i++) {
                        // accedo.console.info('trying to attach index ' + (evt.fromIndex + i));
                        child = fetchChild(evt.fromIndex + i);
                        if (child) {
                            attachChild(child);
                        }
                    }
                    /* Step 2:
                    Update the scrollbar */
                    updateScrollbar();
                        
                    /* Step 3:
                    PRELOADED (unattached) child elements forward */
                    if (!isAllVisible) {
                        for (i = 0; i < opts.preloadSize; i++) {
                            // accedo.console.info('trying to cache index ' + (evt.fromIndex + i + visibleSlotsToAdd));
                            fetchChild(evt.fromIndex + i + visibleSlotsToAdd);
                        }
                    }
                }
                    
                
                /* Step 4:
                Ensure something is selected */
                //Treat the previously requested 'select' call that couldn't be treated at that time
                if (selectRequest) {
                    myPublicObject.select(selectRequest.dsIndex, selectRequest.listIndex);
                }
                
                myPublicObject.ensureSelection();
                //accedo.console.log('ensureSelection; myPublicObject.getChildren().length = ' + myPublicObject.getChildren().length);
                //myPublicObject.attachChildEvents();
                
                //If everything should be visible, there could be more data. Ask for it
                if (isAllVisible) {
                    askForMoreData();
                }
                
            }
            // else {
                // accedo.console.info('I cannot haz more children');
            // }
        };
        
        /**
         * Function creating a new child ready to be put in the list, using some data.
         * Triggered by datasource events.
         * @param {Number} dsIndex Index of the data object in the datasource, to create this child
         * @param {boolean} cacheIndex (optional) If set, cache the new child using cacheIndex instead of dsIndex
         * @returns {accedo.ui.component} The new child or null
         * @private 
         * @memberOf accedo.ui.list#
         */
        createChild = function(dsIndex) {
        
            var data, childType, newId, childOptions, newChild,
            divStyles, div, height, width, gravity, isEmpty;
            
            //Requirements check : childTemplate is an Object and has a function as its 'type' property
            if (! accedo.utils.object.isPlainObject(opts.childTemplate)) {
                return null;
            }
            
            childType = opts.childTemplate.type;
            if (! accedo.utils.object.isFunction(childType)) {
                //No support for controller children here - might be added later if useful
                return null;
            }
            
            //Can we get the data ?
            data = datasource.getDataAtIndex(dsIndex);
            if (data == null) {
                return null;
            }
            
            //Build the new child with its options and new generated id
            newId = generateChildId();
            childOptions = accedo.utils.object.clone(opts.childTemplate);
            childOptions.id = newId;
            /**
             * @ignore
             */
            newChild = childType(childOptions);
            
            //Define an onFocus callback so the List knows which of its children is focused
            /**
             * @ignore
             */
            newChild.onFocus = function() {
                onChildFocus(newChild);
            };
            
            //Create a new slot for this child
            div = accedo.utils.dom.element('div');
            div.addClass('accedo-ui-layout-linear-element');
            
            //Gather the styles in an object
            divStyles = {};
            height = newChild.getOption('height');
            if (height) {
                divStyles.height = height;
            }
            width = newChild.getOption('width');
            if (width) {
                divStyles.width = width;
            }
            
            switch (newChild.getOption('gravity')) {
                case list.GRAVITY_LEFT:
                    divStyles.textAlign = 'left';
                    break;
                case list.GRAVITY_RIGHT:
                    divStyles.textAlign = 'right';
                    break;
                case list.GRAVITY_CENTER:
                    divStyles.textAlign = 'center';
                    break;
            }

            //Effectively set the styles (newChild container)
            div.setStyle(divStyles);
            
            //Attach newChild to its container
            newChild.attachTo(div);
            
            //We'll need this info to duplicate a child for carousels in some cases
            newChild.dsIndex = dsIndex;
            
            //Use the displayHelper, if any
            if (displayHelperFn) {
                displayHelperFn(newChild, data);
            }
                        
            //Add newChild to the list cache
            setCache(dsIndex, newChild);
            
            return newChild;
        };
        
        /**
         * Attach a child to the DOM tree in the right place.
         * @param {accedo.ui.component} child
         * @param {boolean} isFirstPosition (optional) If true, append in first position. Default: false (last position)
         * @returns {accedo.ui.component} the attached child (usually the 1st param, but sometimes a new copy of it)
         * @private
         * @memberOf accedo.ui.list#
         */
        attachChild = function(child, isFirstPosition) {
            var myChild = child;
            
            if (!child) {
                accedo.console.info('just some code for a breakpoint');
            }
            //If already attached, duplicate it (happens on carousels with a visibleSize bigger than the DS's currentSize)
            if (child.getParent().isAttached()) {
                myChild = createChild(myChild.dsIndex);
            }
            
            //Insert the new child's container in the correct position
            if (getCurrentSize() && isFirstPosition) {
                myPublicObject.getRoot().insertBefore(myChild.getParent(), myPublicObject.getRoot().getHTMLElement().firstChild);
            } else {
                myPublicObject.getRoot().appendChild(myChild.getParent());
            }
            
            //myChild.getParent().addEventListener('mouseover', onChildMouseOver, false);
            //myChild.getParent().addEventListener('mouseout', onChildMouseOut, false);
            
            //myChild.addEventListener('mouseover', onChildMouseOver, false);
            //myChild.addEventListener('mouseout', onChildMouseOut, false);
            
            //myChild.addEventListener('click', onChildClick, false);            
            
            
            //Use the container inherited method
            myPublicObject.append(myChild, isFirstPosition);

            return myChild;
        };
        
        /**
         * Returns a string to be set as a child component's ID
         * We just prefix this list's ID by a meaningless counter and increment this latter one
         * @returns {string}
         * @private
         * @memberOf accedo.ui.list#
         */
        generateChildId = function() {
            return myPublicObject.getId() + '_' + iteratorChildId++;
        };
        
        /**
         * Select one of the list's children (more precisely, add a 'selected' CSS class
         * to its DOM parent). This acts as the list's internal focus.
         * Beforehand it will unselect the previously selected child if any (ONLY if the child corresponding
         * to childId can be found in the list). 
         * Also fires the new selected child's onSelect method if any, unless skipCallback is true.
         * Be careful to select a component that is indeed a list's child when using this private function.
         * @param {accedo.ui.component} child ID of the child to be selected
         * @param {boolean} skipCallback When true, do not trigger the child's onSelect (set to true when
         *                      the child focus triggers its selection so as to avoid an infinite loop)
         * @returns {boolean} true if the selection was effectively changed
         * @private
         * @memberOf accedo.ui.list#
         */
        selectChild = function(child, skipCallback) {
            //Don't bother doing anything if the current selection is already OK or nothing is to be selected
            if (child && (child !== selectedChild)) {
            
                //Remove the previous selection if any
                unselect();
                
                //Select the given child
                selectedChild = child;
                selectedChild.getParent().addClass('selected');
                
                //Try launching its onSelect method, unless specified otherwise
                if (!skipCallback) {
                    triggerOnSelect();
                }
                return true;
            }
            return false;
        };
        
        /**
         * Triggers the child's onSelect function if defined, unless specified otherwise
         * @public
         * @memberOf accedo.ui.list#
         */
        triggerOnSelect = function() {
            if (selectedChild && callbackOnSelect && accedo.utils.object.isFunction(selectedChild.onSelect)) {
                selectedChild.onSelect();
            }
            //Update the scrollbar after each selection change
            updateScrollbar();
        };
        
        /**
         * Unselect the current selection and launch its onUnselect callback, if any
         * @private
         * @memberOf accedo.ui.list#
         */
        unselect = function() {
            if (selectedChild) {
                if (accedo.utils.object.isFunction(selectedChild.onUnselect)) {
                    selectedChild.onUnselect.call(selectedChild);
                }
                if (selectedChild.getParent()) {
                    selectedChild.getParent().removeClass('selected');
                }
            }
            selectedChild = null;
        };
        
        /**
         * onClick function that will be associated with the click event (see below)
         * We keep this reference so as to remove the event listener when the list is removed
         *
         * The list transfers a click to its selected child, if any
         * @private
         * @memberOf accedo.ui.list#
         */
        onClick = function() {
            var selected = myPublicObject.getSelection();
            if (selected) {
                selected.dispatchEvent('click');
            }
        };
        
        onChildMouseOver = function() {
            //accedo.console.log('onChildMouseOver');
            /*if (!this.className.match(/(?:^|\s)mouse-focused(?!\S)/)){
            	this.className += " mouse-focused";	
            }*/
        };
        
        onChildMouseOut = function() {
            //accedo.console.log('	onChildMouseOut');
            //this.className = this.className.replace(/(?:^|\s)mouse-focused(?!\S)/g , ' ');
        };
        
        onChildClick = function() {
            //accedo.console.log('		onChildClick');
        };
        
        /**
         * onDetach function that will be associated with the detach event (see below)
         * @private
         * @memberOf accedo.ui.list#
         */
        onDetach = function() {
            myPublicObject.removeEventListener('click', onClick);
            myPublicObject.removeEventListener('detach', onDetach);
            datasource && datasource.removeEventListener('accedo:datasource:append', onDatasourceAppend);
        };
        
        /**
         * onAttach function that will be associated with the attach event (see below)
         * @private
         * @memberOf accedo.ui.list#
         */
        onAttach = function() {
            myPublicObject.addEventListener('click', onClick);
            myPublicObject.addEventListener('detach', onDetach);
            datasource && datasource.addEventListener('accedo:datasource:append', onDatasourceAppend);
            
        };
        
        /**
         * Scroll the current display forward by given parameter or default scrollStep.
         * Then takes cares of preloading data if needed.
         * @param {boolean} reverse (optional) When true, scroll backward. Otherwise scroll forward
         * @param {boolean} skipSelection (optional) If true, do not change the selection after the first scroll
         * @param {boolean} scrollStep (optional) If set, override the current scrollStep option
         * @returns {boolean} true if the list was scrolled at least once
         * @private 
         * @memberOf accedo.ui.list#
         */
        scroll = function(reverse, skipSelection, scrollStep) {
            var i, index, hasScrolled, child, childToRemove, scrollStep, preloadSize;
            
            scrollStep = scrollStep || opts.scrollStep || 1;
            preloadSize = opts.preloadSize || 0;
            hasScrolled = false;
            
            //Each scrolling step means fetching a child and attaching it
            while(scrollStep--) {
                //Get the next child from the cache or create it
                index = getBorderIndex(reverse) + (reverse ? -1 : 1);
                if (index == myPublicObject.getSize() && !reverse){
                	index = 0;
                }
                if (index == -1 && reverse){
                	index = myPublicObject.getSize() - 1;
                }
                child = fetchChild(index);
                if (child) {
                    //Attach it
                    child = attachChild(child, reverse);
                    
                    //Find the appropriate child to remove
                    childToRemove = reverse ? getLastChild() : getFirstChild();
                    myPublicObject.remove(childToRemove);
                    
                    //The first new child gets selected
                    if (!hasScrolled && !skipSelection) {
                        selectChild(child);
                    }
                    
                    hasScrolled = true;
                } else {
                    //The child could not be found, prevent further scrolling
                    break;
                }
            }
            
            if (hasScrolled) {
                
                //Each preloading step means fetching a child (NOT attaching it)
                i = 1;
                while (preloadSize--) {
                    fetchChild(getBorderIndex(reverse) + i++ * (reverse ? -1 : 1));
                    // accedo.console.info('caching index ' + (getBorderIndex(reverse) + (i-1) * (reverse ? -1 : 1)));
                }
            }
            
            return hasScrolled;
        };
        
        /**
         * Return the DS index of the data corresponding to the first child (if reverse), or to the last one
         * @param {boolean} reverse
         * @returns {Number}
         * @private
         * @memberOf accedo.ui.list#
         */
        getBorderIndex = function(reverse) {
            return reverse ? getFirstChild().dsIndex : getLastChild().dsIndex;
        };
        
        /**
         * Adds a child to the cache Array.
         * @param {Number} index Index of the data represented by this child in the datasource
         * @param {accedo.ui.component} child to cache
         * @private
         * @memberOf accedo.ui.list#
         */
        setCache = function(index, child) {
            if (!noCache) {
                //Keep a reference to the parent as the link is broken when we remove a child from a container.
                cache[index] = {
                    child: child,
                    parent: child.getParent()
                };
            }
        };
        
        /**
         * Pops an element from the cache Array
         * @param {Number} index Index of the data represented by this child in the datasource
         * @returns {accedo.ui.component} the cached child or null
         * @private
         * @memberOf accedo.ui.list#
         */
        getCache = function(index) {
            if (index >= 0 && cache.length > index) {
                return cache[index];
            }
            return null;
        };
        
        /**
         * Gets a child from cache if possible, otherwise create it.
         * The requested index is corrected if the list has a CAROUSEL behaviour
         * @param {Number} index
         * @returns the child ready to be appended to the list, or null
         * @private
         * @memberOf accedo.ui.list#
         */
        fetchChild = function(index) {
            var child, myCache;
            
            if (index < 0 && !isCarousel) {
                return null;
            }
            
            //prevent preloading of elements which are out of bounds
            if (!isCarousel && datasource.getTotalItems() > 0 && index >= datasource.getTotalItems()){
                return null;
            }
            
            //Correct the out of bounds index for a carousel
            index = getCorrectedIndex(index);
            
            //In the cache ?
            myCache = getCache(index);
            if (myCache) {
                child = myCache.child;
                //If the child-parent link was broken, set it back (happens when removing a child from container)
                if (!child.getParent()) {
                    child.attachTo(myCache.parent);
                }
            }
            if (!child) {
                //Can be created using the datasource?
                child = createChild(index);
                if (!child) {
                    //Could not be created, ask the DS for more data for the particular child
                    askForMoreData(index);
                }
            }
            
            return child;
        };
        
        /**
         * Ask the datasource for more data, unless we've already asked
         * @private
         * @param {Number} dsIndex (optional) specifiy the ds position your want to get more data. If not specified it'll just ask for more data appending to the datasource
         * @ignore
         */
        askForMoreData = function(dsIndex) {
            if (!waitForMoreData) {
                //Notify we shouldn't ask for more data until we first receive some
                waitForMoreData = true;
                if (typeof dsIndex == 'number'){
                    datasource.load({dsIndex: dsIndex});
                }else{
                    datasource.load();
                }
            }
        };
        
        /**
         * Updates the scrollbar: hides it when the list is childless or shows all of its children,
         * refreshes and shows it otherwise.
         * Could be simply called on attachChild and onRemove - but then there may be flickering when these
         * happen successively, so instead we call it after functions that affect the display (scroll,
         * size change, new elements received from the datasource)
         * @private
         * @ignore
         */
        updateScrollbar = function() {
            if (!scrollbar || !datasource || !selectedChild) {
                return;
            };
            //In some cases a scrollbar is not needed (0 or 1 child)
            if (getCurrentSize() < 2) {
                scrollbar.hide();
            } else {
                //some datasource doesn't know it's total size, we'll just use the current size for the scrollbar
                var total = datasource.getTotalItems() || datasource.getCurrentSize();
                scrollbar.update(selectedChild.dsIndex, 1, total);
                scrollbar.show();
            };
        };
        
        /**
         * Set-up inheritance. This is the object that will be returned when creating a list.
         * @scope accedo.ui.list#
         * 
         */
        myPublicObject = accedo.utils.object.extend(accedo.ui.container(opts), {

            /**
             * onFocus function that is triggered automatically (by the focus manager) when the list
             * is focused.
             * @public
             * @function
             */
            onFocus : function() {
                var selectionChanged = myPublicObject.ensureSelection();
                //Force re-selection of the selected child ?
                if (!selectionChanged && opts.forceSelectOnFocus) {
                    triggerOnSelect();
                }
                //User-defined additional callback
                if (accedo.utils.object.isFunction(additionalOnFocus)) {
                    additionalOnFocus();
                }
            },
            
            /**
             * Provides the user with a way of defining his own onFocus callback,
             * as setting the list's onFocus method would overwrite the above behaviour
             * @param {Function} fn User-defined onFocus callback 
             * @public
             */
            setAdditionalOnFocus : function(fn) {
                additionalOnFocus = fn;
            },
            
            /**
             * Ensures something is selected (if not, select the user-defined firstSelectedIndex
             * or the last child)
             * @public
             * @returns {boolean} True when a new selection was set, false when it was not needed
             */
            ensureSelection : function() {
                if (!selectedChild) {
                    selectChild(getChildAtIndex(opts.firstSelectedIndex) || getLastChild());
                    return true;
                }
                return false;
            },
            
            /**
             * onRemove callback, to be called by the container
             * @param {accedo.ui.component} child Child to be removed
             * @param {Number} previousIndex Index of the child before it was removed
             * @param {boolean} removeAll Are we removing all the children ? (previousIndex is meaningless then)
             * @public
             */
            onRemove: function(child, previousIndex, removeAll) {
                //First, unselect the current selection, otherwise it will automatically select
                //another one when removed, and then trigger an unwanted onSelect
                if (removeAll) {
                    unselect();
                }
                //Now we can assume we're removing one element only, and the children Array has been updated
                //(Not true for removeAll, but we've treated that matter already).
                
                //If it was the selected child, change the selection to another child...
                if (this.getChildren().length) {
                    if (child == selectedChild) {
                        if (previousIndex === 0) {
                            selectChild(getFirstChild());
                        } else {
                            selectChild(getLastChild());
                        }
                    }
                } else { //...Unless there is nothing to select!
                    unselect();
                }
                
                //Remove parent DIV
                if (child.getParent()) {
                    child.getParent().remove();
                    animationFn && animationFn.call(this, previousIndex);
                    
                    //Deinit the child if we are in noCache mode. Otherwise it gets deinited within the cache on this list's deinit function.
                    if (noCache && child.deinit) {
                        child.deinit();
                    }
                }
                
            },
            
            /**
             * Move the list selection to the next child, if possible.
             * Move forward by default, backward if reverse is set to true
             * @param {boolean} reverse When true, move backward
             * @returns {boolean} true if the selection changed
             * @public
             */
            moveSelection: function(reverse) {
                var pos, newSelectionPos, hasMoved;
                if (!selectedChild) {
                    return false;
                }
                
                //Find out whether the new selection is currently displayed or not
                pos = accedo.utils.array.indexOf(this.getChildren(), selectedChild);
                newSelectionPos = pos + (reverse ? -1 : 1);
                
                if (newSelectionPos < 0 || newSelectionPos == this.getChildren().length) {
                    //Moving to an element that is not displayed - scroll needed
                    hasMoved = scroll(reverse);
                } else {
                    //Moving to a displayed element
                    var newSelection = this.getChildren()[newSelectionPos];
                    
                    
                    /* Any selection change scrolls the list if it is a carousel
                    (do it before selecting the element for a better user experience as
                    onSelect callbacks can be time consuming). A regular list can also scroll
                    to another displayed child, according to opts.scrollBeforeBorder
                    */
                    if (isCarousel ||
                        (reverse && pos <= opts.scrollBeforeBorder[0]) ||
                        (!reverse && (getCurrentSize() - 1 - pos) <= opts.scrollBeforeBorder[1])
                    ) {
                        scroll(reverse, true);
                    }
                    
                    selectChild(newSelection);
                    
                    hasMoved = true;
                }
                
                if (hasMoved) {
                    //Notify the list did move (some UI effects can be triggered based on that)
                    myPublicObject.dispatchEvent('accedo:list:moved', {reverse: reverse});
                }
                
                return hasMoved;
            },
            
            moveSelectionLoop: function(reverse){
            	if (reverse){
            		nextLeftUpFunc();	
            	}
            	else{
            		nextRightDownFunc();
            	}
            },
            
            moveSelectionByPage: function(reverse){
            	scroll(reverse, true, opts.visibleSize);
            },
            
            getSize: function(){
            	return datasource.getTotalItems() || datasource.getCurrentSize();
            },
            
            getVisibleSize: function(){
            	return opts.visibleSize;
            },
            
            attachChildEvents: function(){
            	
            },
            
            /**
             * Added by request - sets the selection to one of the displayed children by indicating
             * its current index in the list (previously this function name was 'setSelection')
             * @param {Number} childIndex index of the displayed child to select
             *      (N will select the N-th child, use -1 for the last one)
             * @public
             */
            setSelectedIndex: function(childIndex) {
                var newSelection;
                
                if (childIndex === -1) {
                    newSelection = getLastChild();
                } else if (childIndex >= 0 && childIndex < getCurrentSize()) {
                    newSelection = this.getChildren()[childIndex];
                }
                
                //This won't bug if newSelection was not set
                selectChild(newSelection);
            },
            
            /**
             * Returns the list index of the selected child (relatively to displayed children only)
             * @returns {Number} the list index of the selected child, or -1
             * @public
             */
            getSelectedIndex: function() {
                var result = -1;
                
                if (!selectedChild) {
                    return result;
                }
                accedo.utils.array.each(this.getChildren(), function(child, i) {
                    if (selectedChild == child) {
                        result = i;
                        throw accedo.$break;
                    }
                })
                
                return result;
            },
             
            /**
             * Returns the currently selected child, or null
             * @returns {accedo.ui.component} The currently selected child, or null
             * @public
             */
            getSelection: function() {
                return selectedChild;
            },
            
            /**
             * This function returns the behaviour of this layout.
             * @see accedo.ui.list.REGULAR
             * @see accedo.ui.list.CAROUSEL
             * @returns {Integer} The behaviour
             * @public
             */
            getBehaviour: function() {
                return opts.behaviour;
            },
            
            /**
             * This function returns the orientation of this layout.
             * @see accedo.ui.list.VERTICAL
             * @see accedo.ui.list.HORIZONTAL
             * @returns {Integer} The orientation
             * @public
             */
            getOrientation: function() {
                return opts.orientation;
            },
            
            /**
             * Changing the visibleSize option dynamically.
             * The currently selected element (if any) will not change and will not be removed.
             * @param {Number} size
             * @public
             */
            setVisibleSize: function(size) {
                var child, 
                    diff = opts.visibleSize - size;
                
                //Don't do anything if: Same size than before, illegal argument, no datasource linked or no current children 
                if (!diff || size < 0 || !datasource || !getCurrentSize()) {
                    return;
                };
                
                //From now on use the effective visible size (as the list may have less children than opts.visibleSize)
                diff = this.getChildren().length - size;
                
                //If everything was visible
                if (opts.visibleSize === 0) {
                    //Notify it's not the case any more
                    isAllVisible = false;
                } else if (size === 0) {
                    //If everything is to be visible, notify so
                    isAllVisible = true;
                    //Calculate the current size difference for easier calculations
                    diff = opts.visibleSize - datasource.getCurrentSize();
                }
                
                //The list may grow...
                if (diff < 0) {
                    //Add elements at the forward side
                    while (diff != 0 && (child = fetchChild(getLastChild().dsIndex + 1))) {
                        diff++;
                        attachChild(child);
                    };
                    //Add elements at the backward side
                    while (diff != 0 && (child = fetchChild(getFirstChild().dsIndex - 1))) {
                        diff++;
                        attachChild(child, true);
                    };
                } else if (diff > 0) { //... or it may shrink
                    //Remove elements at the forward side if not the selected child, backward side otherwise
                    while (diff != 0) {
                        diff--;
                        this.remove(getLastChild() != selectedChild ? getLastChild() : getFirstChild());
                    };
                };
                
                opts.visibleSize = size;
                if (opts.visibleSize === 0) {
                    isAllVisible = true;
                };
            },
            
            /**
             * Changing the firstSelectedIndex option.
             * @param {Number} index (index of the child in the list's children Array)
             * @public
             */
            setFirstSelectedIndex: function(index) {
                opts.firstSelectedIndex = index;
            },
            
            /**
             * Associates a datasource to this list and listens to it.
             * Also does the needed operations if another datasource was previously defined.
             * @param {accedo.data.ds} ds
             * @example var myDS = myAPI.getMovies('a1234'); // Supposing it returns a DS !
             *          list.setDataSource(myDS);
             *          myDS.load();
             * @public
             */
            setDatasource: function(ds) {
                //If the given datasource is already registered for this list, do nothing
                if (datasource === ds) {
                    return;
                }
                
                //Remove the previous datasource, if any
                if (datasource) {
                    datasource.removeEventListener('accedo:datasource:append', onDatasourceAppend);
                }
                
                //Remove all the current children (method inherited from container)
                this.removeAll();
                
                //Clear the list cache
                this.purgeCache();
                                
                //Register the new datasource
                datasource = ds;
                if (ds) {
                    //Notify we can ask for even more data when needed
                    waitForMoreData = false;

                    //Listen for more data to come
                    ds.addEventListener('accedo:datasource:append', onDatasourceAppend);
                    //Append the existing data (as we missed the previous events)
                    onDatasourceAppend({newData: ds.getData(), fromIndex: 0});
                }
            },
            
            /**
             * Function to get direct access to the datasource
             * @returns {accedo.data.datasource}
             * @private
             */
            getDatasource: function() {
                return datasource;
            },
            
            /**
             * Associates a displayHelper to this list.
             * This helper defines what to do when an element is created for display (it is then possible
             * to add clic events, callbacks on selection, etc).
             * Typically you will want to set the component's onSelect property, as it is the one that gets
             * triggered when the component is selected in the list, and a 'click' event.
             * @param {Function} dh
             * @public
             * @example
             *  setDisplayHelper(function(component, category) {
             *      //Just do whatever you want with the provided data
             *      component.getRoot().setAttributes({'src': category.coverimagemedium});
             *      component.onSelect = function() {
             *              accedo.console.info('You just selected ' + category.title);
             *              myController.get('summary').setText(category.shortsynopsis);
             *      };
             *      component.onUnselect = function() {
             *              accedo.console.info('You just unselected ' + category.title);
             *      };
             *      component.addEventListener('click', function() {
             *              accedo.console.info('You just clicked on ' + category.title);
             *      });
             *  });
             */
            setDisplayHelper: function(dh) {
                displayHelperFn = dh;
            },

            /**
             * Associate a scrollbar to this list, then updates it.
             * The previous one (if any) will stop being updated but won't be reset.
             * This is not available for a CAROUSEL.
             * @param {accedo.ui.scrollbar} sb 
             * @public
             */
            setScrollbar: function(sb) {
                if (!isCarousel) {
                    scrollbar = sb;
                    updateScrollbar();
                };
            },
            
            /**
             * Activates or deactivates the automatic triggering of the children's onSelect function
             * when it gets selected in the list.
             * Usefull if you want to scroll or set a new selection without triggering any
             * of the children onSelect callbacks.
             * Remember to re-activate it !
             * @param {boolean} isActivated True to (re)activate the children onSelect callbacks.
             *                              False to deactivate it.
             * @public
             */
            activateCallbackOnSelect: function(isActivated) {
                callbackOnSelect = isActivated;
            },
            
            /**
             * Deletes the cache's contents and reset the cache to an empty Array
             * @public
             */
            purgeCache : function() {
            
                
                var i = 0,
                    len = cache.length;
                
                //deinit the children
                for (; i < len; i++) {
                    if (cache[i] && cache[i].child.deinit){
                        cache[i].child.deinit();
                    }
                }
                
                cache = [];
            },
            
            /**
             * Selects an element by its index in the DS (usually you wil use the DS's search function to find the wanted index)
             * Sometimes it isn't possible: if no datasource has been set yet or its size is 0, the selection will apply
             * when a datasource with data is set or when it receives data for the first time.
             * Otherwise if the index is out of the DS bounds, nothing will happen.
             * Optionnally you can decide where the new selection will appear in the list, using listIndex.
             * @param {Function} iterator Iterator function to use for testing (returns true for pass, false for fail)
             * @param {Number} dsIndex Index, in the DS, of the data to select or -1 (last item)
             * @param {Number} listIndex (optional) If the search is successful, the list will try to display
             *  its newly selected child as the one in the specified index. If this option is set to undefined or null,
             *  listIndex will be set to :
             *      -(for a carousel) the value of the 'firstSelectedIndex' option used at list creation time.
             *      -(otherwise) the highest possible value.
             * @returns {boolean} True if the selection was successful
             * @public
             */
            select: function(dsIndex, listIndex) {
                var child,
                    elementsToAdd;

                //No datasource or the datasource doesn't contain the data we want -> apply the select later
                if (!datasource || (-1 !== dsIndex && datasource.getDataAtIndex(dsIndex) == null)) {
                    //prevent double selection which would easily trigger by pressing the left/right button on looping
                    if (!(selectRequest && selectRequest.dsIndex == dsIndex)){
                        // Keep this request in memory to launch it when a datasource is set
                        selectRequest = {dsIndex: dsIndex, listIndex: listIndex};
                    }
                    //Request for the specified data if we did not have enough
                    datasource && datasource.load({dsIndex: dsIndex});
                    return false;
                }
                
                //clear the select request
                selectRequest = null;
                
                elementsToAdd = opts.visibleSize || datasource.getCurrentSize();

                //...or if it is already selected
                if (selectedChild && dsIndex === selectedChild.dsIndex) {
                    return true;
                }
                
                //Correct the listIndex parameter as explained above
                if (!listIndex && listIndex !== 0) {
                    if (isCarousel) {
                        listIndex = opts.firstSelectedIndex;
                    } else if (isAllVisible) {
                        listIndex = datasource.getCurrentSize() - 1;
                    } else {
                        listIndex = opts.visibleSize - 1;
                    }
                }
                
                //Refreshing the list display
                //Step 1: Remove all children
                myPublicObject.removeAll();
                //Step 2: Add and select the child that matches the search
                child = fetchChild(dsIndex === -1 ? datasource.getCurrentSize() - 1 : dsIndex);
                attachChild(child);
                elementsToAdd--;
                selectChild(child);
                //Step 3: Add the proper number of elements at the backward side
                while (listIndex && elementsToAdd && (child = fetchChild(getFirstChild().dsIndex - 1))) {
                    elementsToAdd--;
                    listIndex--;
                    attachChild(child, true);
                };
                
                //Step 4: Add the proper number of elements at the forward side
                while (elementsToAdd && (child = fetchChild(getLastChild().dsIndex + 1))) {
                    elementsToAdd--;
                    attachChild(child);
                };
                
                //Step 5: Add some again at the backward side if not enough could be added forward
                while (elementsToAdd && (child = fetchChild(getFirstChild().dsIndex - 1))) {
                    elementsToAdd--;
                    attachChild(child, true);
                };
                
                updateScrollbar();
                
                return true;
            },
            
            /**
             * Registers a callback that will be triggered during child removal (see onRemove)
             * @param {Function} callback (will be passed the index of the child that gets removed)
             * @public
             */
            setAnimationCallBack: function(callback) {
                animationFn = callback;
            },
            
            /**
             * Allows setting the nextLeft or nextDown option of the List that will be used when the current selection
             * is at the list's left (for an horizontal list) or top (for a vertical one) border.
             * By default the view's nextLeft or nextDown option of the list is used.
             * @param {Object} XDK ID or callback function
             * @public
             */
            setBorderNextLeftUp: function(next) {
                legacy_nextLeftUp = next;
            },
            
            /**
             * Allows setting the nextRight or nextUp option of the List that will be used when the current selection
             * is at the list's right (for an horizontal list) or down (for a vertical one) border.
             * By default the view's nextRight or nextUp option of the list is used.
             * @param {Object} XDK ID or callback function
             * @public
             */
            setBorderNextRightDown: function(next) {
                legacy_nextRightDown = next;
            },
            
            /**
             * Removes the link to the datasource, which then removes all visible and preloaded children.
             * This method is here because it's not trivial to know that setDatasource(null) is the thing to do
             * to free the resources used by this list without deleting it completely.
             * @public
             */
            purgeList: function() {
                this.setDatasource(null);
            },
            /**
             * Remove all listeners of the children
             * @public
             */
            deinit: function(){
                this.removeAllListeners();
                
                var i, len;

                i = 0;
                
                len = cache.length;
                for (; i < len; i++) {
                    if (cache[i] && accedo.utils.object.isFunction(cache[i].child.deinit)){
                        cache[i].child.deinit();
                    }
                }
                this.purgeList();
            }
            
        });
        
        myPublicObject.addEventListener('attach', onAttach);
        
        return myPublicObject;

    };
    
    
    /**
     * This is used to indicate a regular behaviour
     * The scrollStep option is only available in this behaviour
     * @constant
     * @name REGULAR
     * @memberOf accedo.ui.list
     */
    list.REGULAR = 0x01;
    
    /**
     * This is used to indicate a carousel behaviour
     * The scrollStep option is NOT available in this behaviour (will be set to 1)
     * @constant
     * @name CAROUSEL
     * @memberOf accedo.ui.list
     */
    list.CAROUSEL = 0x02;
    
    /**
     * This is used to indicate a vertical layout.
     * @constant
     * @memberOf accedo.ui.list
     */
    list.VERTICAL = 0x01;
    
    /**
     * This is used to indicate a horizontal layout.
     * @constant
     * @name HORIZONTAL
     * @memberOf accedo.ui.list
     */
    list.HORIZONTAL = 0x02;

    /**
     * This places a child at:
     * <pre>
     *  - - -
     * |     |
     * |x    |
     * |     |
     *  - - -
     * </pre>
     * @name GRAVITY_LEFT
     * @constant
     * @memberOf accedo.ui.list
     */
    list.GRAVITY_LEFT = 0x01;

    /**
     * This places a child at:
     * <pre>
     *  - - -
     * |     |
     * |    x|
     * |     |
     *  - - -
     * </pre>
     * @name GRAVITY_RIGHT
     * @constant
     * @memberOf accedo.ui.list
     */
    list.GRAVITY_RIGHT = 0x02;

    /**
     * This places a child at:
     * <pre>
     *  - - -
     * |     |
     * |  x  |
     * |     |
     *  - - -
     * </pre>
     * @name GRAVITY_CENTER
     * @constant
     * @memberOf accedo.ui.list
     */
    list.GRAVITY_CENTER = 0x04;

    return list;

});
