/**
 * @fileOverview Popup container. Note that the developer must assign both
 *   htmlid and id, otherwise the error will be thrown. Popup also relies on
 *   two stylesheet class: .accedo-ui-popuppage, .accedo-ui-popupcontent.
 *   Either one of these two class must have CSS position: absolute and
 *   z-index:9999 in order to get it work properly. Also, a normal popup
 *   should tell device manager / focus manager store / block all background key
 *   events and focuses once the  popup's visible, and restore it back once it
 *   is close (Previously we made the popup is capable to be set to invisible
 *   state, but this will cause focus manager and key event manager confused,
 *   and hard to manage on the class derived from it. So we remove extra
 *   invisible state.)
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 * @example var popup = accedo.ui.popup({id:'popup',htmlid:'popup',css:'bk-red'});
 */

accedo.define("accedo.ui.popup", [
    "accedo.utils.object", "accedo.utils.dom", "accedo.ui.container", "accedo.app", "accedo.focus.manager","accedo.ui.layout.absolute"],
function() {
    /**
     * @name popup
     * @memberOf accedo.ui
     * @class This container is used when you would like to position child elements
     *        in an absolute manor. Child elements are positioned by defining 'x' and 'y'
     *        attributes. Both 'x' and 'y' are optional, if not defined the position will
     *          not be set here and the CSS style will apply (top and left 0 by default)
     * @extends accedo.ui.container
     */
    return function(opts) {

        if (accedo.utils.object.isUndefined(opts)) opts = {};
        
        var viewObj, backgroundConfig, defaultView, create;

        viewObj = null,
        backgroundConfig = [],
        defaultView = {
            type:          accedo.ui.layout.absolute,
            css:           "accedo-ui-popupcontent " + opts.css || ""
        };
        /**
         * @ignore
         */
        create = function(){
            var div = accedo.utils.dom.element('div');
            accedo.app.getRoot().appendChild(div);
            return div;
        };

        opts.root = create();


        return accedo.utils.object.extend(accedo.ui.container(opts), {
            
            isPopup : true,

            /**
             *  Store the current background config like focus and key events
             *  @name pushBackgroundconfig
             *  @memberOf accedo.ui.popup#
             *  @example popup.pushBackgroundconfig();
             */
            pushBackgroundConfig: function() {
                try{
                    backgroundConfig["currentFocus"] = accedo.focus.manager.getCurrentFocus();
                    accedo.focus.manager.requestFocus(null);
                }catch(e){
                    accedo.console.info("storing the background focus");
                }
                
                /* set the current Controller to popup*/
                this.backgroundController = accedo.app.getCurrentController();
                accedo.app.currentController = this;
                
                accedo.device.manager.pushAllKeyEvent();
                accedo.console.info('push once');

                this.dispatchEvent('attach');
                return this;
            },
            
            /**
             *  Restore the current background config like focus and key events
             *  @name popBackgroundConfig
             *  @memberOf accedo.ui.popup#
             *  @example popup.popBackgroundConfig();
             */
            popBackgroundConfig: function() {
                /* restore the current Controller*/
                accedo.app.currentController = this.backgroundController;
                
                if ( backgroundConfig["currentFocus"]!=null) {
                    accedo.focus.manager.requestFocus(backgroundConfig["currentFocus"]);
                }
             
                accedo.device.manager.popAllKeyEvent();
                // console.log('pop once');
            },
                        
            /**
             * Append element to popup container
             * @name addToContent
             * @memberOf accedo.ui.popup#
             * @param {accedo.ui.component | accedo.utils.dom} el the element to
             *   append to
             * @example var button = accedo.ui.button({text'ok'});popup.addContent(button);
             */
            addToContent: function(el) {
                if (viewObj == null) {
                    this.setView(defaultView);
                }
                viewObj.append(el);

            },
            /**
             * Close the popup. Note that the difference between this method
             * and setInvisible is this method destroys itself, whereas
             * setInvisible just hides it. It's better to bind popup default
             * close button to close() to prevent memory leak.
             * @name close
             * @memberOf accedo.ui.popup#
             * @example popup.close();
             */
            close: function() {
                //Prevent memory leaks in Samsung devices using a left-hand operand
                var child = accedo.app.getRoot().removeChild(this.getRoot());
                child = null;
                viewObj = null;
                this.popBackgroundConfig();
                this.dispatchEvent("accedo:popup:onClose");
            },
            /**
             * Set the callback function to specified the behavior for popup
             * container once being clicked.
             * @name setOnClick
             * @memberOf accedo.ui.popup#
             * @param {Function} onclick The callback function name
             */
            setOnClick: function(onclick) {
                this.onclick = onclick;
            },
            /**
             * Set up how many pixels between the top of browser window and the
             * popup container.
             * @name setTop
             * @memberOf accedo.ui.popup#
             * @param {Integer} topParam How many pixels
             */
            setTop: function(topParam) {
                viewObj.getRoot().setStyle({
                    top: topParam + "px"
                });
            },
            /**
             * Get how many pixels between the top of browser window and the
             * popup container.
             * @name getTop 
             * @memberOf accedo.ui.popup#
             * @return {Integer} the amount of pixels between the top of
             *   browser window and the popup container
             * @example popup.getTop();
             */
            getTop: function() {
                return parseInt(viewObj.getRoot().getStyle('top'),10);
            },
         
            
            /**
             * Set current view.
             * @name setView
             * @param {Object} view JSON definition of the view.
             * @memberOf accedo.ui.popup#
             */
            setView: function(view) {
                //Assign parent
                view.parent = this;//opts.root;

                //Instantiate view
                viewObj = view.type(view);
                viewObj.setVisible(false);
                accedo.utils.fn.delay(function(){
                    viewObj.setVisible(true);
                },0.1);
            },
            
            /**
             * Get component by it's ID. Note that since a popup is not attached
             * to the root of the controller launched it, we have to rely on
             * this method to get the component on the popup
             * @name get
             * @param {String} id Elements ID
             * @return {accedo.ui.component} Component selected by ID or null
             * @memberOf accedo.ui.popup#
             */
            get: function(id) {
                return viewObj.getById(id);
            }
        }).pushBackgroundConfig().containerInit();

    };
});