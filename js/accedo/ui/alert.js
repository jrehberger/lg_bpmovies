/**
 * @fileOverview alert dialog
 * 
 * 
 * @author Calle Gustafsson <calle.gustafsson@accedobroadband.com>
 */
 
accedo.define('accedo.ui.alert',
    [
    'accedo.ui.dialog',
    'accedo.ui.label',
    'accedo.ui.button',
    'accedo.utils.object',
    'accedo.utils.fn',
    'accedo.focus.manager'
    ],
    function(){
        
        /**
     * alert dialog
     * @name alert
     * @class
     * @constructor
     * @param {String | Object} opts 
     * @memberOf accedo.ui
     * @extends accedo.ui.dialog 
     * @example var alertdialogSimple = accedo.ui.alert('Alerttext');
     * @example var alertdialogAdvandced = accedo.ui.alert({
     *          headerText:'This text will show in the header',
     *          text:'Lorem ipsum',buttonText:'Text on the button'
     *          });
     */
 
        return function(opts){
            var closeBtn;
            
            if(accedo.utils.object.isUndefined(opts)){
                opts = {}
            }
            
            if(accedo.utils.object.isString(opts)){
                var newOpts = {}
                newOpts.text = opts;
                newOpts.headerText = 'Alert';
                newOpts.buttonText = 'Ok';
                opts = newOpts;
            }
            
            opts.txt = opts.text || 'Alert!';
            opts.headerText = opts.headerText ||'Alert!';
            opts.buttonText = opts.buttonText || 'Ok';
            
            var publicObj = accedo.utils.object.extend(accedo.ui.dialog(opts),{
                init: function(){
                    accedo.console.log("init");
                    var self = this;
                    
                    this.addToHeader(accedo.ui.label({
                        css: opts.headerCss || 'header',
                        text: opts.headerText
                    }));
                    
                    this.addToContent(accedo.ui.label({
                        css: opts.msgCss || 'content',
                        text: opts.text
                    }));
                    closeBtn = accedo.ui.button({
                        css: opts.closeBtn_css,
                        text: opts.buttonText
                    })
                    this.addToFooter(closeBtn);
                    
                    closeBtn.addEventListener('click',function(){
                        self.dispatchEvent('accedo:alertDialog:onClose');
                        self.close();
                    });
                }
            })
            publicObj.init();
            accedo.utils.fn.defer(accedo.utils.fn.bind(accedo.focus.manager.requestFocus,accedo.focus.manager), closeBtn);
                    
            return publicObj;
            
        }
    }
    );
