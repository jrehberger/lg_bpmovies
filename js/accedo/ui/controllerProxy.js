/**
 * @fileOverview Abstract controller implementation.
 * @author <a href="mailto:alexej.kubarev@acedobroadband.com">Alexej Kubarev</a>
 */


//= require <accedo/utils/object>
//= require <accedo/utils/dom>
//= require <accedo/ui/component>

accedo.define("accedo.ui.controllerProxy", ["accedo.utils.object", "accedo.utils.dom", "accedo.ui.component"], function() {

    /**
     * @name controllerProxy
     * @memberOf accedo.ui
     * @class A controller proxy is a wrapper for Abstract controller. It may be used as a "subview" container to hold other controllers.
     * Aother way to see subcontainer is simmilar to HTML iFrame.
     * @extends accedo.ui.component
     */
    return function(opts) {

        var instance, container;

        container = accedo.utils.dom.element('div');
        opts.root = container;

        var controller = opts.controller;
        
        var obj = accedo.utils.object.extend(accedo.ui.component(opts), {
            /**
             * Proxy to accedo.ui.controller#get
             * @name get
             * @param {String} id String Identifier of component to get.
             * @return {accedo.ui.component} Component with given ID or null.
             * @see {accedo.ui.controller#get}
             * @memberOf accedo.ui.controllerProxy#
             * @function
             */
            get: function(id) {
                return instance.get(id);
            },

            /**
             * Returns the controller instance that this proxy redirects to
             * @name getInstance
             * @return {accedo.ui.controller} Controller currently proxied or null.
             * @memberOf accedo.ui.controllerProxy#
             * @function
             */
            getInstance: function() {
                return instance;
            },

            /**
             * Proxy to accedo.ui.controller#changeToController. 
             * Changes from current controller to a new one in the same container.
             * Controller reference may be obtained either via direct linking or from appDef - accedo.app#getControllerById
             * @name changeToController
             * @param {Function} Controller constructor reference
             * @param {Object} context      (optional) Context object to be passed to the new controller
             * @param {Function} context    (optional) Callback launched after the new controller's onCreate function has been executed
             * @memberOf accedo.ui.controllerProxy#
             * @function
             */
            changeToController: function(controller, context, callback) {
                instance.changeToController(controller, context, callback);
            },
            
            /**
             * Creates controller instance with options
             * @name createInstance
             * @private
             * @function
             * @memberOf accedo.ui.controllerProxy#
             */
            createInstance: function(controller, options) {         
                //Override options root, if such is set
                options.root = accedo.utils.dom.element('div');
                instance = controller(options);

                //Fire event
                // var instrumentStart = (new Date()).getTime();
                if (accedo.utils.object.isFunction(instance.onCreate)) {
                    instance.onCreate();
                }
                // var instrumentEnd = (new Date()).getTime();
//                console.info("[INSTRUMENTATION] proxy ControllerOnCreate = " + (instrumentEnd - instrumentStart)  + "ms");

                instance.attachTo(container);

                instance.addEventListener('navigate', function(ev) {
                    instance.deinit && instance.deinit();
                    instance.detach && instance.detach();

                    //instance.root = null;
                    //options.root = null;
                    
                    //Add specific context if provided
                    if (ev && 'context' in ev) {
                        options.context = ev.context;
                    }
                    
                    obj.createInstance(ev.controller, options);
                    //If there's a callback, pass the instance as arg, so it can play with it (e.g. adding some event listeners to it)
                    if (typeof ev.callback == 'function') {
                        ev.callback(instance);
                    }
                });
            },
            /**
             * Deinits the controllerProxy
             * @name deinit
             * @function
             * @memberOf accedo.ui.controllerProxy#
             */
            deinit: function(){
                if (instance){
                    instance.deinit();
                }
                container = null;
                opts.root = null;
                this.removeAllListeners();
            }
        });

        obj.createInstance(controller, accedo.utils.object.clone(opts));

        return obj;
    };
});