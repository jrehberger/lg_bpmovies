/**
 * @fileOverview Button with 3 part background, can have an icon
 * 
 * 
 * @author Calle Gustafsson <calle.gustafsson@accedobroadband.com>
 */

accedo.define('accedo.ui.segmentedControlItem',
    [
        'accedo.ui.image',
        'accedo.ui.label',
        'accedo.ui.component',
        'accedo.utils.dom',
        'accedo.utils.object'
    ],
    function(){
     /**
     * Creates a new segmentedControlItem to be used in a segmentedControl instance.
     * Can dispatch "click" event
     * @name segmentedControlItem
     * @memberOf accedo.ui
     * @class A button implementation 
     * @extends accedo.ui.component
     * @constructor
     * @param {Object} opts The options object
     * @param {String} opts.text The text label to use for the button
     * @param {String} opts.icon The src to an image
     */
        return function(opts){
            opts.text = opts.text || '';
            opts.icon = opts.icon || '';
            var icon, label,create, contentContainer,container,leftBorder,rightBorder,event;
            
            event = opts.event || null;
            
            event.type = event.type || 'click'
            event.data = event.data || null;
            /**
             * @ignore
             */
            create = function(opts){
               container = accedo.utils.dom.element('div');
              container.addClass('accedo-ui-segmentedControlItem');
               leftBorder = accedo.utils.dom.element('div',{},container);
              leftBorder.setStyle({
                  top:'0px',
                  left:'0px'
              });
              leftBorder.addClass('corner left');
              
      
              
              contentContainer = accedo.utils.dom.element('div',{},container);

              contentContainer.addClass('content');
      
               rightBorder = accedo.utils.dom.element('div',{},container);
              rightBorder.setStyle({
                  top:'0px',
                  right:'0px'
              });
              
              rightBorder.addClass('corner right');
      
              icon = accedo.ui.image({
                  parent:contentContainer,
                  src: opts.icon
              });
              label = accedo.ui.label({
                  parent: contentContainer,
                  text: opts.text
              });

              return container;  
            }
            opts.root = create(opts);
            opts.focusable = true;
            create = null;
            var self;
            var obj = accedo.utils.object.extend(accedo.ui.component(opts),{
                initItem: function(){
                    self = this;
                    return this;
                },
                dispatchClickEvent: function(){
                    if(event){
                        self.dispatchEvent(event.type, event.data);
                    }else{
                        self.dispatchEvent('click');
                    }
                },
                /**
                 * Sets the text
                 * @name setText
                 * @param {String} text text
                 * @function
                 * @memberOf accedo.ui.segmentedControlItem#
                 * 
                 */
                setText: function(text){
                    label.setText(text);
                },
                /**
                 * Sets an icon
                 * @name setIcon
                 * @param {String} src the src attribute string to set, the URL to the image
                 * @function
                 * @memberOf accedo.ui.segmentedControlItem#
                 */
                setIcon: function(src){
                    icon.setSrc(src);
                }
                
            }).initItem();
            
            //Listen for browsers click event
            //Disabled - DOM events currently leak memory
            // opts.root.addEventListener('click', function() {
                // obj.dispatchClickEvent();
            // }, false);
            
            return obj;
        }
    }
);
