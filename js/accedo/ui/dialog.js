/**
 * @fileOverview base class for dialogs, has: header, body, footer
 * 
 * 
 * @author Calle Gustafsson <calle.gustafsson@accedobroadband.com>
 */

accedo.define('accedo.ui.dialog',
    [
    'accedo.ui.overlayContainer',
    'accedo.ui.layout.linear',
    'accedo.ui.layout.normal',
    'accedo.utils.object'
    ],
    function(){
        /**
         * Base dialog class
         * @name dialog
         * @memberOf accedo.ui
         * @class Base dialog class
         * @constructor
         */
  
        return function(opts){
            if(accedo.utils.object.isUndefined(opts)){
                opts = {}
            }
               opts.focusable = true; 
            opts.dialog_css = 'accedo-ui-dialog '+ (opts.dialog_css ?  opts.dialog_css: '');            
            var viewObj= null, backgroundConfig = [], 
            headerObj=null, bodyObj=null, footerObj=null, publicObj, 
            

            
            defaultView = ({
                type: accedo.ui.layout.linear,
                x: opts.top || '30%',
                y: opts.left || '25%',
                css:  opts.dialog_css ,
                width: opts.dialog_width || '300px',
                height: opts.dialog_height || '200px',
                children: [{
                    type: opts.header_type || accedo.ui.layout.normal,
                    id: 'header',
                    css: 'header',
                    height: opts.header_height || '10%'
                },
                {
                    type: opts.body_type || accedo.ui.layout.normal,
                    id: 'body',
                    css: 'content',
                    height: opts.body_height || '75%'
                },
                {
                    type: opts.footer_type || accedo.ui.layout.normal,
                    id: 'footer',
                    css: 'footer',
                    height: opts.footer_height || '15%'
                }
                ]
            }
            );
            //opts.children = defaultView;
            publicObj = accedo.utils.object.extend(accedo.ui.overlayContainer(opts),{
                 /**
                 * Sets the view of the dialog
                 * @name setView
                 * @function
                 * @param {accedo.ui.container} the view object
                 * @memberOf accedo.ui.dialog#
                 */
                setView: function(view){
        
                    viewObj = view.type(view);
                    this.getContainer().append(viewObj);
                        
                },
                 /**
                 * Initiates the dialog
                 * @name dialogInit
                 * @function
                 * @memberOf accedo.ui.dialog#
                 */
                dialogInit: function(){
                    this.setView(defaultView);
                    headerObj = viewObj.getById('header');
                    bodyObj = viewObj.getById('body');
                    footerObj = viewObj.getById('footer');
                    this.pushBackgroundConfig();
                    var self = this;
                    accedo.device.manager.registerKey('back',function(){
                        self.close();
                        delete self;
                    });
                    
                    return this;
                },
                 /**
                 * Adds an item to the header of the dialog
                 * 
                 * @name addToHeader
                 * @function
                 * @memberOf accedo.ui.dialog#
                 * @param {accedo.ui.container} el The object to append
                 */
                addToHeader: function(el){
                    headerObj.append(el);
                },
                 /**
                 * Adds an item to the content container of the dialog
                 * @name addToContent
                 * @function
                 * @memberOf accedo.ui.dialog#
                 * @param {accedo.ui.container} el The object to append
                 */
                addToContent: function(el){
                    bodyObj.append(el);
                },
                 /**
                 * dds an item to the footer of the dialog
                 * @name addToFooter
                 * @function
                 * @memberOf accedo.ui.dialog#
                 * @param {accedo.ui.container} el The object to append
                 */
                addToFooter: function(el){
                    footerObj.append(el);
                },
                
                /**
                 * Closes the dialog
                 * @name close
                 * @function
                 * @memberOf accedo.ui.dialog#
                 */
                close: function() {
                    this.getRoot().remove();
                    viewObj = null;
                    this.popBackgroundConfig();
                    this.dispatchEvent("accedo:dialog:onClose");
                },
                /**
                *  Store the current background config like focus and key events
                 * @name pushBackgroundConfig
                 * @function
                 * @memberOf accedo.ui.dialog#
                 * 
                */ 
                pushBackgroundConfig: function(){
                    try{
                        backgroundConfig["currentFocus"] = accedo.focus.manager.getCurrentFocus();
                        accedo.focus.manager.requestFocus(null);
                    }catch(e){
                        accedo.console.info("storing the background focus");
                    }

                    /* set the current Controller to popup*/
                    this.backgroundController = accedo.app.getCurrentController();
                    accedo.app.currentController = this;

                    accedo.device.manager.pushAllKeyEvent();

                    this.dispatchEvent('attach');
                    return this;    
                },
                
                /**
                *  Restore the current background config like focus and key events
                *  @name popBackgroundConfig
                *  @function
                *  @memberOf accedo.ui.dialog#
                */
                popBackgroundConfig: function() {
                    /* restore the current Controller*/
                    accedo.app.currentController = this.backgroundController;
                
                    if ( backgroundConfig["currentFocus"]!=null) {
                        accedo.focus.manager.requestFocus(backgroundConfig["currentFocus"]);
                    }
             
                    accedo.device.manager.popAllKeyEvent();
                // console.log('pop once');
                },
                
             /**
             * Get component by it's ID. Note that since a popup is not attached
             * to the root of the controller launched it, we have to rely on
             * this method to get the component on the popup
             * @name get
             * @function
             * @param {String} id Elements ID
             * @return {accedo.ui.component} Component selected by ID or null
             * @memberOf accedo.ui.dialog#
             */
            get: function(id) {
                return viewObj.getById(id);
            }
                
                
            }).dialogInit().containerInit();
            
            
            
       
            
            return publicObj;
        };
    }   
    );
