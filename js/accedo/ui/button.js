/**
 * @fileOverview A button object, providing basic functionality.
 * @author <a href="mailto:thomas.johansson@accedobroadband.com">Thomas Johansson</a>
 */

accedo.define(
    "accedo.ui.button", ["accedo.utils.object", "accedo.utils.dom", "accedo.ui.label", "accedo.ui.component", "accedo.focus.managerMouse"], function() {

    /**
     * Creates a new button instance.
     * Can dispatch "click" event
     * @name button
     * @memberOf accedo.ui
     * @class A button implementation
     * @extends accedo.ui.component
     * @constructor
     * @param {Object} opts The options object
     * @param {String} opts.text The text label to use for the button
     */
        return function(opts) {

            opts.text = opts.text || '';
            var label, element;

            /**
         * This function creates a button object.
         * @private
         */

            var create = function() {
                element = accedo.utils.dom.element('div');
                element.addClass("accedo-ui-button");
                
                label = accedo.ui.label({
                    parent: element
                });

                return element;
            };

            opts.root = create(opts);
            opts.focusable = true; //Explicit definition of object being focusable

            var obj = accedo.utils.object.extend(accedo.ui.component(opts), {

                /**
             * This function dispatches a click event
             * @private
             * @function
             * @memberOf accedo.ui.fluidButton#
             */
                dispatchClickEvent: function() {
                    obj.dispatchEvent('click');
                },
                
                dispatchMouseOverEvent: function() {
	                accedo.focus.managerMouse.requestFocus(opts.root);
	            },
	            
	            dispatchMouseOutEvent: function() {
	                accedo.focus.managerMouse.releaseFocus();
	            },

                /**
	             * This function sets the text label of the button.
	             * @name setText
	             * @param {String} text The text label
	             * @memberOf accedo.ui.fluidButton#
	             * @public
	             * 
	             */
                setText: function(text) {
                    label.setText(text || '');
                },
                
                /**
	             * Return the inner label (i.e. so as to access its DOM element)
	             * @returns {accedo.ui.label}
	             * @public
	             */
	            getLabel: function() {
	                return label;
	            },
	            
	            /**
	             * Return the inner label (i.e. so as to access its DOM element)
	             * DO NOT USE IT ANY MORE! Use 'getLabel'. THis one is just here for compatibility, should be removed later.
	             * @returns {accedo.ui.label}
	             * @public
	             */
	            getText: function() {
	                return label;
	            }
            });

            //Set initial text
            obj.setText(opts.text);
            
            opts.root.addEventListener('click', obj.dispatchClickEvent, false);
            opts.root.addEventListener('mouseover', obj.dispatchMouseOverEvent, false);
            opts.root.addEventListener('mouseout', obj.dispatchMouseOutEvent, false);
            
            return obj;
        };
    });