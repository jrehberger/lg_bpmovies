/**
 * @fileOverview A scrollbar object.
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
 */

//= require <accedo/utils/object>
//= require <accedo/utils/dom>
//= require <accedo/ui/component>
accedo.define("accedo.ui.scrollbar", ["accedo.utils.object", "accedo.utils.dom", "accedo.ui.component"], function() {
    /**
     * Scrollbar
     * @name scrollbar
     * @memberOf accedo.ui
     * @class A scrollbar implementation
     * @extends accedo.ui.component
     * @constructor
 
     */

    var scrollbar = function(opts) {

        var track,
            nodeTable,
            vertical = true,
            trackTopTd,
            trackMiddleTd,
            nodeMiddleTd,
            trackTopping = true, //false for node overlapping the top and bottom area of the track

        /**
         * Set up specific border setting on table elements
         * @private
         * @memberOf accedo.ui.scrollbar#
         * @ignore
         */
            setNoBorderWidth = function(target){
                target.setStyle({
                    padding:"0",
                    margin:"0",
                    borderWidth:"0",
                    borderStyle:"none"
                });
            };

        /**
         * This function creates a button object.
         * @private
         * @memberOf accedo.ui.scrollbar#
         * @ignore
         */
        var create = function(){

            var element = accedo.utils.dom.element('div');
            element.addClass('accedo-ui-scrollbar');
            element.setStyle({
                position:"absolute",
                padding:"0",
                margin:"0"
            });

            if (opts.orientation == accedo.ui.scrollbar.HORIZONTAL) {
                vertical = false;
                element.addClass('accedo-ui-scrollbar-linear-h');
            } else {
                element.addClass('accedo-ui-scrollbar-linear-v');
            }

            if(opts.trackTopping == false){
                trackTopping = opts.trackTopping;
            }

            track = accedo.utils.dom.element('div');
            track.addClass('track');

            var trackTable = accedo.utils.dom.element('table', {
                cellpadding : "0",
                cellspacing : "0",
                width: "100%"
            });

            trackTable.addClass('trackTable');
            var trackTopTr = accedo.utils.dom.element('tr');
            trackTopTd = accedo.utils.dom.element('td');
            trackTopTd.addClass('trackTop');
            if(vertical){
                var trackMiddleTr = accedo.utils.dom.element('tr');
                var trackBottomTr = accedo.utils.dom.element('tr');
            }
            
            trackMiddleTd = accedo.utils.dom.element('td');
            trackMiddleTd.addClass('trackMiddle');
            
            var trackBottomTd = accedo.utils.dom.element('td');
            trackBottomTd.addClass('trackBottom');

            var node = accedo.utils.dom.element('div');
            node.addClass('node');

            nodeTable = accedo.utils.dom.element('table', {
                cellpadding : "0",
                cellspacing : "0",
                width       : "100%"
            });
            nodeTable.addClass('nodeTable');


            var nodeTopTr = accedo.utils.dom.element('tr');
            var nodeTopTd = accedo.utils.dom.element('td');
            nodeTopTd.addClass('nodeTop');
            if(vertical){
                var nodeMiddleTr = accedo.utils.dom.element('tr');
                var nodeBottomTr = accedo.utils.dom.element('tr');
            }
            
            nodeMiddleTd = accedo.utils.dom.element('td');
            nodeMiddleTd.addClass('nodeMiddle');
            
            var nodeBottomTd = accedo.utils.dom.element('td');
            nodeBottomTd.addClass('nodeBottom');

            trackTopTr.appendChild(trackTopTd);
            if(vertical){
                trackMiddleTr.appendChild(trackMiddleTd);
                trackBottomTr.appendChild(trackBottomTd);
            }else{
                trackTopTr.appendChild(trackMiddleTd);
                trackTopTr.appendChild(trackBottomTd);
            }

            trackTable.appendChild(trackTopTr);
            if(vertical){
                trackTable.appendChild(trackMiddleTr);
                trackTable.appendChild(trackBottomTr);
            }

            track.appendChild(trackTable);

            nodeTopTr.appendChild(nodeTopTd);
            if(vertical){
                nodeMiddleTr.appendChild(nodeMiddleTd);
                nodeBottomTr.appendChild(nodeBottomTd);
            }else{
                nodeTopTr.appendChild(nodeMiddleTd);
                nodeTopTr.appendChild(nodeBottomTd);
            }

            nodeTable.appendChild(nodeTopTr);
            if(vertical){
                nodeTable.appendChild(nodeMiddleTr);
                nodeTable.appendChild(nodeBottomTr);
            }

            node.appendChild(nodeTable);

            element.appendChild(track);
            element.appendChild(node);

            return element;
        };

        opts.root = create(opts);
        opts.focusable = false;


        var obj = accedo.utils.object.extend(accedo.ui.component(opts), {

            /**
             * Update the size of the node
             * @name update
             * @function
             * @param {Integer} start Starting position of the data being shown
             * @param {Integer} length Length of the data being shown
             * @param {Integer} total Total number of data in the data
             * @memberOf accedo.ui.scrollbar#
             * @public
             */
            update : function(start, length, total){

                var deferFunc = function(){

                    var trackE = track.getHTMLElement(),
                        nodeStyle = nodeTable.getHTMLElement().style,
                        trackTopTdE = trackTopTd.getHTMLElement(),
                        nodeMiddleTdE = nodeMiddleTd.getHTMLElement();
                    if(vertical){
                        var trackHeight = trackE.scrollHeight - (trackTopping?trackTopTdE.scrollHeight * 2:0); //44;
                        var trackY = trackE.scrollTop;

                        var nodeHeight = Math.max(Math.ceil(trackHeight * length/total), trackTopTdE.scrollHeight * 2);
                        var nodeY = Math.ceil(trackY + (trackTopping?trackTopTdE.scrollHeight:0) + start / (total - length ) * (trackHeight - nodeHeight));
                        accedo.console.info("trackHeight: " + trackHeight + " trackY: " + trackY + " nodeHeight: " + nodeHeight + " nodeY: " + nodeY);
                        nodeStyle.top = nodeY + "px";
                        nodeStyle.height = nodeHeight + "px";
                        nodeStyle.position = "absolute";
                    }else{
                        var trackWidth = trackE.scrollWidth - (trackTopping?trackTopTdE.scrollWidth * 2:0);
                        var trackX = trackE.scrollLeft;
						
						var nodeWidth = Math.ceil(trackWidth * length/total) + trackTopTdE.scrollWidth * 2;
                        var nodeX = Math.ceil(trackX + (trackTopping?trackTopTdE.scrollWidth:0) + start / (total-length) * (trackWidth - nodeWidth));

                        nodeStyle.left = nodeX + "px";
                        nodeStyle.width = nodeWidth + "px";
                        nodeStyle.position = "absolute";

                        trackMiddleTd.getHTMLElement().style.width = (trackWidth - 36) + "px";
                        nodeMiddleTd.getHTMLElement().style.width = (nodeWidth - 36) + "px";
                    }
                };

                accedo.utils.fn.defer(deferFunc);
            },
            
            dispatchClickEvent: function(evt) {
                obj.dispatchEvent('click');
            },
        });
        
        opts.root.addEventListener('click', obj.dispatchClickEvent, false);
        return obj;
    };

    /**
     * This is used to indicate a vertical layout.
     * @constant
     * @name VERTICAL
     * @memberOf accedo.ui.scrollbar
     */
    scrollbar.VERTICAL = 0x01;
    /**
     * This is used to indicate a horizontal layout.
     * @constant
     * @name HORIZONTAL
     * @memberOf accedo.ui.scrollbar
     */
    scrollbar.HORIZONTAL = 0x02;

    return scrollbar;
});