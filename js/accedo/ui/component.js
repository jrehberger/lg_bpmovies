/**
 * @fileOverview Abstract component used as a base for all UI components.
 * @author <a href="mailto:alexej.kubarev@accedobroadband.com">Alexej Kubarev</a>
 */

//= require <accedo/events/dispatcher>
//= require <accedo/utils/object>
//= require <accedo/utils/dom>

accedo.define("accedo.ui.component", ["accedo.utils.object", "accedo.utils.dom", "accedo.utils.fn", "accedo.events.dispatcher", "accedo.focus.manager", "accedo.focus.managerMouse"], 
    function() {

    /**
     * @name component
     * @memberOf accedo.ui
     * @class This is the abstract component function that all UI object, be it
     *        containers or components, inherrits from.
     * @extends accedo.events.dispatcher
     * @param {Object} opts Options
     * @param {Document Element} opts.root The DOM root element (the container for this component)
     * @param {Document Element} opts.parent The DOM parent element
     * @param {Integer} opts.widthStyle How to layout the width of this component
     * @param {Integer} opts.heightStyle How to layout the height of this component
     * @param {String} opts.id The ID of this component
     */
    var component = function(opts) {
        var focused, focusable, disabled;

        /**
         * Currently focused.
         * @private
         * @field
         * @ignore
         */
        focused = false;

        /**
         * Is able to receive focus.
         * @private
         * @field
         * @ignore
         */
        focusable = false;

        /**
         * Is component disabled.
         * @private
         * @field
         * @ignore
         */
        disabled = false;


        return accedo.utils.object.extend(accedo.events.dispatcher(), ({
            /**
             * The parent object.
             * @private
             * @ignore
             */
            parent: opts.parent,

            /**
             * The root object.
             * @private
             * @ignore
             */
            root:   opts.root,

            /**
             * This function initiates a component using the options given in the constructor.
             * @name init
             * @function
             * @private
             * @memberOf accedo.ui.component#
             */
            init: function() {
                
                if (this.root && this.parent) {
                    this.attachTo(this.parent);
                }
                if (opts.id) {
                    //When opts.id starts with '#', we also set the htmlid
                    if (accedo.utils.string.startsWith(opts.id, '#')) {
                        if (opts.id.length > 1) {
                            this.root.id = opts.id.substr(1);
                            this.root.setAttributes({id:this.root.id});
                        }
                    } else {
                        this.root.id = opts.id;
                    }
                }
                if (opts.css) {
                    this.root.addClass(opts.css);
                }

                if (opts.focusable) {
                    focusable = opts.focusable;
                }
                this.setWidthStyle(opts.widthStyle);
                this.setHeightStyle(opts.heightStyle);


                // if (this.isFocusable()) {
                    // opts.root.addEventListener('mouseover', accedo.utils.fn.bind(function() {
                        // if (this.isEnabled() && accedo.focus && accedo.focus.manager
                                // && accedo.utils.object.isFunction(accedo.focus.manager.requestFocus)) {
                            // accedo.focus.manager.requestFocus(this);
                        // }
                    // }, this), false);
                // }

                return this;
            },

            /**
             * Returns true if component is focusable, false otherwise.
             * @name isFocusable
             * @function
             * @return {Boolean}
             * @memberOf accedo.ui.component#
             */
            isFocusable: function() {
                return focusable;
            },

            /**
             * Checks to see if component is focused.
             * @name isFocused
             * @function
             * @return {Boolean} true if focused, false otherwise.
             * @memberOf accedo.ui.component#
             */
            isFocused: function() {
                return focused;
            },

            /**
             * Checks to see if component is enabled.
             * @name isEnabled
             * @function
             * @return {Boolean} true if enabled, false otherwise.
             * @memberOf accedo.ui.component#
             */
            isEnabled: function() {
                return !disabled;
            },

            /**
             * Sets enabled state for component.
             * If disabled, sets CSS class "disabled"
             * @name setEnabled
             * @function
             * @param {Boolean} toggle true or false for enabled state
             * @memberOf accedo.ui.component#
             */
            setEnabled: function(toggle) {
                if (toggle) {
                    this.root.removeClass("disabled");
                } else {
                    this.root.addClass("disabled");
                }

            },

            /**
             * Focuses component if it is focusable and calls custom onFocus method if such is defined.
             * Focus is identified by "focused" CSS Class name.
             * @name focus
             * @function
             * @memberOf accedo.ui.component#
             */
            focus: function() {
                if (focusable && !focused && !disabled) {
                    this.root.addClass("focused");
                    focused = true;
                    if (accedo.utils.object.isFunction(this.onFocus)) {
                        this.onFocus();
                    }
                }
            },

            /**
             * Removes focus from (blurs) component and calls custom onBlur method if such is defined.
             * @name blur
             * @function
             * @memberOf accedo.ui.component#
             */
            blur: function() {
                if (focused) {
                    this.root.removeClass("focused");
                    focused = false;
                    if (accedo.utils.object.isFunction(this.onBlur)) {
                        this.onBlur();
                    }
                }
            },

            /**
             * This function returns itself if the supplied id matches its own id,
             * otherwise it null is return
             * @name getById
             * @function
             * @public
             * @memberOf accedo.ui.component#
             * @returns {accedo.ui.component|null}
             */
            getById: function(id) {
                return (this.root.id === id ? this : null);
            },
            
            /**
             * Returns this component's id
             * @name getId
             * @function
             * @returns {string|null}
             * @memberOf accedo.ui.component#
             */
            getId: function() {
                return this.root.id || null;
            },

            /**
             * This function sets the width style of this component
             * @name setWidthStyle
             * @function
             * @public
             * @memberOf accedo.ui.component#
             * @param {Integer} style The new width style
             * @see accedo.ui.component.FIT_PARENT
             * @see accedo.ui.component.WRAP_CONTENT
             */
            setWidthStyle: function(style) {
                if (style == component.FIT_PARENT) {
                    this.root.setStyle({'width': '100%'});
                } else {
                    this.root.removeStyle('width');
                }
            },

            /**
             * This function sets the height style of this component
             * @name setHeightStyle
             * @function
             * @public
             * @memberOf accedo.ui.component#
             * @param {Integer} style The new height style
             * @see accedo.ui.component.FIT_PARENT
             * @see accedo.ui.component.WRAP_CONTENT
             */
            setHeightStyle: function(style) {
                if (style == component.FIT_PARENT) {
                    this.root.setStyle({'height': '100%'});
                } else {
                    this.root.removeStyle('height');
                }
            },

            /**
             * This function returns the specified option and falls back to the supplied
             * default value it this option has not been defined for this component.
             * @name getOption
             * @function
             * @public
             * @memberOf accedo.ui.component#
             * @param {String} name The option name
             * @param {String} [defaultValue] The fallback value
             */
            getOption: function(name, defaultValue) {
                var ret = opts[name];
                if (accedo.utils.object.isUndefined(ret)) {
                    ret = defaultValue;
                }
                return ret;
            },

            /**
             * Sets a new option with given name.
             * @name setOption
             * @function
             * @public
             * @memberOf accedo.ui.component#
             * @param {String} name The option name
             * @param {String} defaultValue The value of the option
             */
            setOption: function(name, value) {
                opts[name] = value;
            },

            /**
             * This function returns the root element of this component.
             * @name getRoot
             * @function
             * @return {DOM Element} The root element
             * @public
             * @memberOf accedo.ui.component#
             */
            getRoot: function() {
                return this.root;
            },

            /**
             * This function returns the parent element of this component.
             * @name getParent
             * @function
             * @return {DOM Element} The parent element
             * @public
             * @memberOf accedo.ui.component#
             */
            getParent: function() {
                return this.parent;
            },

            /**
             * This function shows this component. Dispatches a 'show' event.
             * @name show
             * @function
             * @public
             * @memberOf accedo.ui.component#
             */
            show: function() {
                this.root.show();

                //Dispatches an event
                this.dispatchEvent('show');
            },

            /**
             * This function shows this component. Dispatches a 'hide' event.
             * @name hide
             * @function
             * @public
             * @memberOf accedo.ui.component#
             */
            hide: function() {
                this.root.hide();

                //Dispatches an event
                this.dispatchEvent('hide');
            },

            /**
             * This function toggle this component's display between the none and inherit values.
             * @name toggle
             * @function
             * @public
             * @memberOf accedo.ui.component#
             */
            toggle: function() {
                if (this.root.toggle()) {
                    this.dispatchEvent('show');
                } else {
                    this.dispatchEvent('hide');
                }
            },

            /**
             * Sets the components visibility by changing roots visibility.
             * @name setVisible
             * @function
             * @param {Boolean} visible
             * @public
             * @memberOf accedo.ui.component#
             */
            setVisible: function(visible) {
                this.root.setVisible(visible);
            },

            /**
             * This function finds out whether this component is attached
             * to a parent component or not.
             * @name isAttached
             * @function
             * @public
             * @memberOf accedo.ui.component#
             * @returns {true|false}
             */
            isAttached: function() {
                return this.parent != null;
            },

            /**
             * This function detaches this component from its current parent
             * Dispatches a 'detach' event.
             * @name detach
             * @function
             * @public
             * @memberOf accedo.ui.component#
             */
            detach: function() {
                if (!this.isAttached()) {
                    return;
                }

                if (this.parent) {
                    this.root.remove();
                }
                this.parent = null;
               
                //Dispatches an event
                this.dispatchEvent('detach');
            },

            /**
             * This function attaches this component to the designated parent DOM Element (and
             * detaches itself from its current parent).
             * Dispatches a 'attach' event.
             * @name attachTo
             * @function
             * @public
             * @memberOf accedo.ui.component#
             * @param {Document Element} parent The parent element to attach to
             */
            attachTo: function(parent) {
                this.detach();
                this.parent = parent;

                //this.parent.appendChild( (parent == document.body) ? this.root.getHTMLElement() : this.root);

                if (this.parent.getRoot) {
                    this.parent.getRoot().appendChild(this.root);
                } else {
                    this.parent.appendChild(this.root);
                }

                //Dispatches an event
                this.dispatchEvent('attach');
            },

           
            /**
             * Returns components horizontal alignment
             * @name getHorizontalAlignment
             * @function
             * @private
             * @memberOf accedo.ui.component#
             */
            getHorizontalAlignment: function(alignment, defaultAlignment) {
                var result = null;

                if (alignment & component.LEFT) {
                    result = 'left';
                }
                else if (alignment & component.RIGHT) {
                    result = 'right';
                }
                else if (alignment & component.CENTER) {
                    result = 'center';
                } else if (defaultAlignment !== null) {
                    result = this.getHorizontalAlignment(defaultAlignment, null);
                }

                return result;
            },

            /**
             * Returns components vertical alignment
             * @name getVerticalAlignment
             * @function
             * @private
             * @memberOf accedo.ui.component#
             */
            getVerticalAlignment: function(alignment, defaultAlignment) {

                var result = null;

                if (alignment & component.TOP) {
                    result = 'top';
                }
                else if (alignment & component.BOTTOM) {
                    result = 'bottom';
                }
                else if (alignment & component.MIDDLE) {
                    result = 'middle';
                } else if (defaultAlignment !== null) {
                    result = this.getVerticalAlignment(defaultAlignment, null);
                }

                return result;
            },
            /**
             * Deinit
             * @name deinit
             * @function
             * @public
             * @memberOf accedo.ui.component#
             */
            deinit: function() {
                this.removeAllListeners();
            }

        })).init();
    };

    /**
     * This indicates that the component should grow to fit its parent.
     * @constant
     * @name FIT_PARENT
     * @memberOf accedo.ui.component
     */
    component.FIT_PARENT = 0x01;

    /**
     * This indicates that the component should grow to fit its content.
     * @constant
     * @name WRAP_CONTENT
     * @memberOf accedo.ui.component
     */
    component.WRAP_CONTENT = 0x02;

    /**
     * Alignment: left.
     * @constant
     * @name LEFT
     * @memberOf accedo.ui.component
     */
    component.LEFT = 0x01;

    /**
     * Alignment: center.
     * @constant
     * @name CENTER
     * @memberOf accedo.ui.component
     */
    component.CENTER = 0x02;

    /**
     * Alignment: right.
     * @constant
     * @name RIGHT
     * @memberOf accedo.ui.component
     */
    component.RIGHT = 0x04;

    /**
     * Alignment: top.
     * @constant
     * @name TOP
     * @memberOf accedo.ui.component
     */
    component.TOP = 0x08;

    /**
     * Alignment: middle.
     * @constant
     * @name MIDDLE
     * @memberOf accedo.ui.component
     */
    component.MIDDLE = 0x10;

    /**
     * Alignment: bottom.
     * @constant
     * @name BOTTOM
     * @memberOf accedo.ui.component
     */
    component.BOTTOM = 0x20;

    return component;
});