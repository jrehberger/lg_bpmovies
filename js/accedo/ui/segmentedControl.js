/**
 * @fileOverview Button with 3 part background, can have an icon
 * 
 * 
 * @author Calle Gustafsson <calle.gustafsson@accedobroadband.com>
 */

accedo.define('accedo.ui.segmentedControl',
    [
        'accedo.ui.image',
        'accedo.ui.label',
        'accedo.ui.container',
        'accedo.ui.component',
        'accedo.utils.dom',
        'accedo.utils.object'
    ],
    function(){
        /**
         * An segmented control
         * 
         * @class segmented control
         * @name segmentedControl
         * @memberOf accedo.ui
         * @constructor 
         * @param {Object} opts
         */
        return function(opts){
         if(accedo.utils.object.isUndefined(opts)){
             opts = {};
         }
         opts.root = accedo.utils.dom.element('div');
         opts.root.addClass('accedo-ui-segmentedControl');
       
         return accedo.utils.object.extend(accedo.ui.container(opts),{
             /**
              * runs on append
              * @name onAppend
              * @function
              * @memberOf accedo.ui.segmentedControl#
              */
             onAppend: function(child){

                 child.attachTo(opts.root);
                 var childs = this.getChildren();


                 for(var i=0; i<childs.length;i++){
                     if(i>0){
                         childs[i].setOption('nextLeft', childs[i-1].getId());
                     }
                     if(childs[i+1]){
                        childs[i].setOption('nextRight', childs[i+i].getId());
                     }
                     childs[i].getRoot().addClass('tab');
                     childs[i].getRoot().removeClass('last');
                     childs[i].focusable = true;
                 }
                 /* Set first and last tab */
                 childs[0].getRoot().addClass('first');
                 if(childs[1]){
                    childs[0].setOption('nextRight',childs[1].getId());
                 }
                 childs[childs.length-1].getRoot().addClass('last');
             }
             
         }).containerInit();
    }
    }
);
