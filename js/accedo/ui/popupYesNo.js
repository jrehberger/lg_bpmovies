/**
 * @fileOverview General purpose Yes and No popup with OK button to close it.
 * @extend accedo.ui.popup
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 */

accedo.define("accedo.ui.popupYesNo", [
    "accedo.ui.layout.normal",
    "accedo.ui.popup",
    "accedo.ui.label",
    'accedo.utils.fn',
    "accedo.utils.object",
    "accedo.focus.manager",
    "accedo.ui.fluidButton"],

    function() {

        /**
      *@name popupYesNo
      *@memberOf accedo.ui
     * @class
     * @extends accedo.ui.popup
     * @example var popupYesNo = accedo.ui.popupYesNo({
     *              headline_css: 'headline_css',
     *              headline_txt: 'headline_text [Optional]',
     *              msg_css: 'msg_css',
     *              msg_txt: 'msg_txt [Optional]',
     *              YesBtn_css: 'YesBtn_css',
     *              YesBtn_txt: 'YesBtn_txt [Optional]',
     *              NoBtn_css: 'NoBtn_css',
     *              NoBtn_txt: 'NoBtn_txt [Optional]',
     *              root_css: 'root_css [Optional]'
     * });
     */
        return function(opts) {

            var cmpnentPl = null, self,
            label       = accedo.ui.label,
            layout      = accedo.ui.layout,
            fluidButton = accedo.ui.fluidButton;

            return accedo.utils.object.extend(accedo.ui.popup(opts), {
                /**
             * Initialize popup ok object and set up close button focus & event.
             * It will be automatically called once the object created.
             */
                init: function(){
                    var yesButton, noButton;
                    self = this;

                    cmpnentPl = layout.normal(
                    {
                        type: layout.normal,
                        css: opts.popup_css || 'bpmvPopup panel popup focus',
                        children: [
                        {
                            type: label,
                            css:  opts.headline_css,
                            text: opts.headline_txt || 'Default headline here'
                        },
                        {
                            type: label,
                            css:  opts.msg_css,
                            text: opts.msg_txt || 'Default message content here'
                        },
                        {
                            type: layout.normal,
                            css: 'buttonPanel panel vertical focus',
                            children: [
                            {
                                type:      fluidButton,
                                id:        'noButton',
                                css:       opts.noBtn_css,
                                text:      opts.noBtn_txt || 'No',
                                width:     opts.noBtn_width,
                                nextDown:  'yesButton'
                            },
                            {
                                type:      fluidButton,
                                id:        'yesButton',
                                css:       opts.yesBtn_css,
                                text:      opts.yesBtn_txt || 'Yes',
                                width:     opts.yesBtn_width,
                                nextUp:    'noButton'
                            }
                            ]
                        }
                        ]
                    })

                    this.addToContent(cmpnentPl);
                    this.root.removeClass(opts.css);
                    if (opts.root_css){
                        this.root.addClass(opts.root_css);
                    }

                    yesButton = this.get('yesButton');
                    noButton = this.get('noButton');
           
                    // Route click event
                    yesButton.addEventListener('click',function(){
                        self.dispatchEvent('accedo:popupYesNo:onConfirm');
                        self.close();
                    });

                    // Route click event & back event to the same place.
                    noButton.addEventListener('click', accedo.utils.fn.bind(self.exitPopup, self));

                    // Temporary disable background view key event and focus while
                    // the popup OK visible
                    accedo.focus.manager.requestFocus(noButton);

                    return this;
                },
                
                exitPopup: function() {
                    this.dispatchEvent('accedo:popupYesNo:onClose');
                    this.close();
                }
            }).init();
        };
    });