/**
 * @fileOverview Container is an abstract component that can have children.
 * @author <a href="mailto:alexej.kubarev@accedobroadband.com">Alexej Kubarev</a>
 */

//= require <accedo/utils/object>
//= require <accedo/ui/component>
//= require <accedo/ui/controllerProxy>

accedo.define("accedo.ui.container", ["accedo.utils.object", "accedo.utils.array", "accedo.focus.manager", "accedo.focus.managerMouse", "accedo.ui.controllerProxy", "accedo.ui.component"], 
    function() {

    /**
     * @name container
     * @memberOf accedo.ui
     * @class This is the abstract function that all UI containers is based upon.
     * @extends accedo.ui.component
     * @constructor
     */
    return function(opts) {

        var children, obj;

        /**
         * This array holds a pointer to all child objects.
         * @private
         * @memberOf accedo.utils.object
         */
        children = [];


        obj = accedo.utils.object.extend(accedo.ui.component(opts), ({

            /**
             * This function retrieves a child object by its ID. It is worth
             * noting that this is a recursive function, so it will search
             * child containers as well for the give ID.
             * @name getById
             * @function
             * @param {String} id The object identifier
             * @returns {accedo.ui.component|null} The component having the given identifier,
             *                                     or null if not found
             * @memberOf accedo.ui.container#
             * @public
             */
            getById: function(id) {
                var i, len, obj;

                if (this.getId() === id) {
                    return this;
                }

                i = 0;
                len = children.length;
                for (; i < len; i++) {

                    // Fix for a subcontroller's controllerProxy : use get instead of getById
                    // (controllerProxy's getById exists on the component level only)
                    if (accedo.utils.object.isFunction(children[i].get)) {
                        obj = children[i].get(id);
                        if (obj) {
                            return obj;
                        }
                    }

                    if (accedo.utils.object.isFunction(children[i].getById)) {
                        obj = children[i].getById(id);
                        if (obj) {
                            return obj;
                        }
                    }
                }
                return null;
            },

            /**
             * This function retrives the current set of children for this container.
             * @name getChildren
             * @function
             * @return {Array} The child array
             * @memberOf accedo.ui.container#
             * @public
             */
            getChildren: function() {
                return children;
            },

            /**
             * This function appends a child object.
             * @name append
             * @function
             * @param {accedo.ui.component} child The child object to append
             * @param {boolean} isFirst (optional) If true, append in first position. Default: false
             * @memberOf accedo.ui.container#
             * @public
             */
            append: function(child, isFirst) {
                if (isFirst) {
                    children = [child].concat(children);
                } else {
                    children.push(child);
                }
                
                if (accedo.utils.object.isFunction(this.onAppend)) {
                    this.onAppend(child);
                }
            },

            /**
             * This function removes a child object.
             * @name remove
             * @function
             * @param {accedo.ui.component} child The child object to remove
             * @memberOf accedo.ui.container#
             * @public
             */
            remove: function(child) {
                var i = 0, valid = false, len = children.length;
                for (; i < len; i++) {
                    if (children[i] == child) {
                        children.splice(i, 1);
                        valid = true;
                        break;
                    }
                }

                if (valid && accedo.utils.object.isFunction(this.onRemove)) {
                    this.onRemove(child, i);
                }

                if (valid) {
                    child.detach();
                }
            },

            /**
             * This function removes all child objects.
             * @name removeAll
             * @function
             * @memberOf accedo.ui.container#
             * @public
             */
            removeAll: function() {
                var i = children.length;
                while (i--) {
                    if (accedo.utils.object.isFunction(this.onRemove)) {
                        this.onRemove(children[i], i, true);
                    }
                    children[i].detach();
                }
                children = [];
            },
          
            /**
             * Deinit remove event listeners for itself and also ask its children to remove the event listers
             * @name deinit
             * @function
             * @memberOf accedo.ui.container#
             * @public
             */
            deinit: function() {
                this.removeAllListeners();
                
                var i, len;

                i = 0;
                len = children.length;
                for (; i < len; i++) {
                    if (accedo.utils.object.isFunction(children[i].deinit)){
                        children[i].deinit()
                    }
                }
                
                this.removeAll();
            },
            /**
             * This function initiates the container, and is called from the
             * instantiating container function.
             * @name containerInit
             * @function
             * @returns {accedo.ui.container} A handle to itself
             * @memberOf accedo.ui.container#
             * @public
             */
            containerInit: function() {
                    // var instrumentStart = (new Date()).getTime();
            
                var i, l, childType, childController, childOpts;
                if (accedo.utils.object.isArray(opts.children)) {
                    i = 0;
                    l = opts.children.length;

                    for (; i < l; i++) {
                        childOpts = opts.children[i];

                        if (accedo.utils.object.isPlainObject(childOpts)) {
                            childType = childOpts.type;
                            if (accedo.utils.object.isFunction(childType)) {
                                this.append(childType(childOpts));
                            } else {
                                //This is a sub controller
                                childController = childOpts.controller;
                                if (accedo.utils.object.isFunction(childController)) {
                                    this.append(accedo.ui.controllerProxy(childOpts));
                                }
                            }
                        }
                    }
                }
                
                // var instrumentEnd = (new Date()).getTime();
//                console.info("[INSTRUMENTATION] containerInit = " + (instrumentEnd - instrumentStart)  + "ms");
                
                return this;
            }
        }));

        return obj;
    }; 
});