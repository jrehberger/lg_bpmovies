/**
 * @fileOverview General purpose popup with OK button to close it.
 * @extend accedo.ui.popup
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 */

accedo.define("accedo.ui.popupOk", [
    "accedo.ui.popup", "accedo.ui.label", "accedo.utils.object",'accedo.ui.overlayContainer'],
function() {

    /**
     * @name popupOk
     * @memberOf accedo.ui
     * @class
     * @extends accedo.ui.popup
     * @example var popupOk = accedo.ui.popupOk({
     *      headline_css: 'headline_css',
     *      headline_txt: 'headline_text [Optional]',
     *      msg_css: 'msg_css',
     *      msg_txt: 'msg_txt [Optional]',
     *      closeBtn_css: 'closeBtn_css',
     *      closeBtn_txt: 'closeBtn_txt [Optional]'
     * });
     */
    return function(opts) {

        var closeBtn;
            
        return accedo.utils.object.extend(accedo.ui.popup(opts), {
            
            /**
             * Initialize popup ok object and set up close button focus & event.
             * It will be automatically called once the object created.
             * 
             * @name init
             * @memberOf accedo.ui.popupOk#
             * @private
             */
            init: function(){
                var self = this;

                this.addToContent(accedo.ui.label({css: opts.headline_css, text: opts.headline_txt}));
                this.addToContent(accedo.ui.label({css: opts.msg_css, text: opts.msg_txt}));
                closeBtn = accedo.ui.button({css: opts.closeBtn_css, text: ('closeBtn_txt' in opts ? opts.closeBtn_txt : 'Ok')});
                this.addToContent(closeBtn);

                // Route click event & back event to the same place.
                closeBtn.addEventListener('click',function(){
                    self.dispatchEvent('accedo:popupOk:onClose');
                    self.close();
                });

                // Temporary disable background view key event and focus while
                // the popup OK visible
                this.addEventListener('attach', function() {
                    accedo.console.info('popupOk attached');
                    accedo.utils.fn.defer(
                        accedo.utils.fn.bind(accedo.focus.manager.requestFocus,  accedo.focus.manager),
                        closeBtn
                    );
                });

                this.root.removeClass(opts.css);
                
                return this;
            },
            
            /**
             * Gives access to the OK Button
             * @name getButton
             * @memberOf accedo.ui.popupOk#
             * @public
             */
            getButton: function() {
                return closeBtn;
            }
            
        }).init();
    };
}
);