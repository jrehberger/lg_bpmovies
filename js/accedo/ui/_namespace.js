/**
 * @fileOverview defines the accedo.ui namespace
 * 
 * 
 * @author Calle Gustafsson <calle.gustafsson@accedobroadband.com>
 */
/**
 * @name accedo.ui
 * @namespace
 */