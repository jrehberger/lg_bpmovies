/**
 * @fileOverview alert dialog
 * 
 * 
 * @author Calle Gustafsson <calle.gustafsson@accedobroadband.com>
 
 */
 
accedo.define('accedo.ui.confirm',
    [
        'accedo.ui.dialog',
        'accedo.ui.label',
        'accedo.ui.button',
        'accedo.utils.object',
        'accedo.utils.fn',
        'accedo.focus.manager'
    ],
    function(){
        /*
         *
         *@name confirm
         *@memberOf accedo.ui
         *@class Confirm dialog with Ok and Cancel button
         *@construct
         *@param {String | Object} opts String or an object
        * @example var confirmdialogSimple = accedo.ui.confirm('Confirm text');
        * @example var confirmdialogAdvandced = accedo.ui.confirm({headerText:'This text will show in the header',text:'Lorem ipsum',cancelButtonText:'Cancel button', confirmButtonText:'Confirm button'});
        */
        return function(opts){
            var confirmBtn, closeBtn;
            
            if(accedo.utils.object.isUndefined(opts)){
                opts = {}
            }
            
            if(accedo.utils.object.isString(opts)){
                var newOpts = {}
                newOpts.text = opts;
                newOpts.headerText = 'Confirm?';
                newOpts.cancelButtonText = 'Cancel';
                newOpts.confirmButtonText = 'Confirm';
                opts = newOpts;
            }
            
            opts.txt = opts.text || 'Confirm?';
            opts.headerText = opts.headerText ||'Confirm?';
            opts.cancelButtonText = opts.cancelButtonText || 'Cancel';
            opts.confirmButtonText = opts.confirmButtonText || 'Confirm';
            
            var publicObj =  accedo.utils.object.extend(accedo.ui.dialog(opts),{
                init: function(){
                    var self = this;
                    
                    this.addToHeader(accedo.ui.label({
                        css: opts.headerCss || 'header',
                        text: opts.headerText
                    }));
                    
                    this.addToContent(accedo.ui.label({
                        css: opts.msgCss || 'content',
                        text: opts.text
                    }));
                    closeBtn = accedo.ui.button({
                        id:'btn1',
                        css: opts.cancelBtn_css,
                        text: opts.cancelButtonText,
                        nextLeft:'btn0' 
                    })
                    
                    confirmBtn = accedo.ui.button({
                        id:'btn0',
                        css:opts.confirmBtn_css,
                        text: opts.confirmButtonText,
                        nextRight:'btn1'
                    })
                    
                    var footContainer = accedo.ui.layout.linear({orientation:accedo.ui.layout.linear.HORIZONTAL})
                    
                    footContainer.append(confirmBtn);
                    footContainer.append(closeBtn);
                    
                    this.addToFooter(footContainer);
                    
                    closeBtn.addEventListener('click',function(){
                        self.dispatchEvent('accedo:confirm:onCancel');
                        self.close();
                    });
                    
                    confirmBtn.addEventListener('click',function(){
                        self.dispatchEvent('accedo:confirm:onConfirm');
                        self.close();
                    });
              
                      
                     accedo.focus.manager.requestFocus(closeBtn);   
                     
                    return this;
                }
            })
           publicObj.init();
          /* accedo.utils.fn.defer(
                            accedo.utils.fn.bind(accedo.focus.manager.requestFocus,accedo.focus.manager), 
                            closeBtn
                        ); */
           return publicObj; 
        }
    }
);
