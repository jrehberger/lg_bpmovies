
accedo.define("accedo.ui.htmlComponent", ["accedo.utils.object", "accedo.utils.dom", "accedo.utils.array", "accedo.ui.container", "accedo.ui.component", "accedo.focus.manager", "accedo.ui.popupOk"], 
    function() {

/**
 * HTML component allows setting custom HTML and even making it focusable.
 * Hint: the htmlComponent's nextLeft (and other navigation options) is applied to the focusable elements imported if they don't have their own navigation definitions
 * @name htmlComponent
 * @memberOf accedo.ui
 * @class
 * @constructor
 */
return function(opts) {

        /**
         * This function creates a button object.
         * @private
         */
        var create = function() {
                var element = accedo.utils.dom.element('div');
                element.addClass('accedo-ui-html');

                return element;
            },
        
            /**
             * Gets interesting DOM elements
             * Interesting means it has at least one of the following:
             * - a xdkid property
             * - a xdkaction property
             * - a focusable proerty with 'true' value
             * @private
             * @memberOf accedo.ui.htmlComponent#
             */
            getDOMElements = function(parent, elements) {
                var elements = elements || [];
                if (
                    parent.attributes && (
                        parent.attributes["xdkid"] ||
                        parent.attributes["xdkaction"] || (
                            parent.attributes["focusable"] && parent.attributes["focusable"].nodeValue == "true"
                        )
                    )
                ) {
                    elements.push(parent);
                } 
                //Also check this element's children
                accedo.utils.array.each(parent.childNodes, function(item) {
                    if (item.nodeType != 1) {
                        return;
                    }

                    elements = getDOMElements(item, elements);
                }, this);

                return elements;
            },
            
            //Object we will return
            obj;
        
        opts.content = opts.content || '';
        opts.root = create(opts);
        opts.focusable = false; //Explicit definition of object being focusable
        
        obj = accedo.utils.object.extend(accedo.ui.container(opts), {

            /**
             * This function sets the HTML to contents of the container and parses it for focusability
             * @name setHTMLData
             * @function
             * @param {String} text The HTML data to set
             * @memberOf accedo.ui.htmlComponent#
             * @public
             */
            setHTMLData: function(content) {
                obj.getRoot().setText(content || '');

                var xmlDom = obj.getRoot().getHTMLElement(),
                    elements = getDOMElements(xmlDom);
                      /**
                         * @ignore
                         */
                        
                accedo.utils.array.each(elements, function(element) {
                    try {
                        var attr = element.attributes,
                            isFocusable = attr["focusable"] && attr["focusable"].nodeValue == "true",
                            newOpts = {},
                            component;
                        
                        //XDK Attribute xdkid
                        if(attr["xdkid"]) {
                            newOpts.id = attr["xdkid"].nodeValue;
                        }
                        
                        //XDK Attribute focusable set to true
                        if (isFocusable) {
                            newOpts.focusable = true;

                            accedo.utils.array.each(["nextUp", "nextDown", "nextRight", "nextLeft"], function(item) {
                                //next navigation is defined
                                if (attr[item.toLowerCase()] && attr[item.toLowerCase()].nodeValue) {
                                    newOpts[item] = attr[item.toLowerCase()].nodeValue;
                                } else if (item in opts) {
                                //next navigation is not defined, apply the htmlComponent's default if any
                                    newOpts[item] = opts[item];
                                }
                            }, this);
                        }
                        
                        //All required options have been set, create the element
                        newOpts.root = accedo.utils.dom.element(element);
                        component = new accedo.ui.component(newOpts);
                        this.append(component);
                        component.parent = this;
                        
                        //Listen for the browser's click event
                        // newOpts.root.addEventListener('click', function() {
                            // component.dispatchEvent('click');
                        // }, false);
                  
                        //XDK Attribute xdkaction
                        if (attr["xdkaction"] && attr["xdkaction"].nodeValue) {
                            var actionValue = attr["xdkaction"].nodeValue,
                                paramPos = actionValue.search(/:/),
                                action = actionValue.substr(0, paramPos),
                                param = actionValue.substr(paramPos + 1),
                                actionFn;
                            
                            switch (action) {
                                case 'show':
                                case 'hide':
                                case 'toggle':
                                    /**
                                     * @ignore
                                     */
                                    actionFn = function() {
                                        var elem = obj.getById(param);
                                        if (elem) {
                                            elem[action]();
                                        }
                                    };
                                    break;
                                case 'pop':
                                    /**
                                     * @ignore
                                     */
                                    actionFn = function() {
                                        var elem = obj.getById(param);
                                        if (elem) {
                                            var popup = accedo.ui.popupOk({
                                                id:'customPopup_' + param
                                            });

                                            //Make its exit button the only focusable thing, and hide it
                                            var popupBtn = popup.getButton();
                                            popupBtn.setOption('nextUp', null);
                                            popupBtn.setOption('nextDown', null);
                                            popupBtn.setOption('nextRight', null);
                                            popupBtn.setOption('nextLeft', null);

                                            popupBtn.hide();

                                            popup.addToContent(elem);
                                            elem.show();
                                            
                                            //'Back' will close the popup
                                            accedo.device.manager.registerKey("back", function(){
                                                popupBtn.dispatchClickEvent();
                                            });
                                            
                                            accedo.focus.manager.requestFocus(null);
                                            //Focus the popup's exit button (update: deactivated) 
                                            // accedo.utils.fn.defer(
                                                // accedo.utils.fn.bind(accedo.focus.manager.requestFocus, accedo.focus.manager),
                                                // popupBtn
                                            // );
                                        }
                                    };
                                    break;
                                case 'event':
                                    /**
                                     * @ignore
                                     */
                                    actionFn = function() {
                                        //Send the event from the htmlComponent, as it is easier to listen to it
                                        obj.dispatchEvent('accedo:html:custom', {param: param, source: newOpts.id});
                                    };
                                    break;
                            }
                            
                            if (actionFn) {
                                //Add a property that makes it possible to know this action is linked to an htmlComponent
                                actionFn.isCustom = true;
                            
                                //If a key is defined for this action use it
                                if(attr["xdkkey"] && attr["xdkkey"].nodeValue) {
                                    var xdkkey = attr["xdkkey"].nodeValue;
                                    //Allow only color keys (so far)
                                    if (accedo.utils.array.some(['green', 'red', 'yellow', 'blue'], function(i) {return i === xdkkey})) {
                                        accedo.device.manager.registerKey(attr["xdkkey"].nodeValue, actionFn);
                                    }
                                } else {
                                    //Otherwise the action will be triggered on click
                                    component.addEventListener('click', actionFn);
                                }
                            }
                        }

                    } catch (ex) {
                        throw new Error(ex);
                    }
                }, this);
                

               
            }

        });

        //Set initial text
        obj.setHTMLData(opts.content);

        return obj;
    };
});