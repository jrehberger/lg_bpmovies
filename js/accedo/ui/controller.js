/**
 * @fileOverview Abstract controller implementation.
 * @author <a href="mailto:alexej.kubarev@acedobroadband.com">Alexej Kubarev</a>
 */


//= require <accedo/utils/object>
//= require <accedo/utils/array>
//= require <accedo/ui/container>

accedo.define("accedo.ui.controller", ["accedo.utils.object", "accedo.utils.array", "accedo.ui.container"], function() {

    /**
     * @name controller
     * @memberOf accedo.ui
     * @class An abstract controller is the base logic container for all UI objects.
     * @extends accedo.ui.container
     */
    return function(opts) {
        var viewObj = null;

        var obj = accedo.utils.object.extend(accedo.ui.container(opts), {

            /**
             * Get component by it's ID.
             * @param {String} id Elements ID
             * @return {accedo.ui.component} Component selected by ID or null
             * @memberOf accedo.ui.controller#
             */
            get: function(id) {
                return viewObj.getById(id);
            },

            /**
             * Changes from current controller to a new one in the same container.
             * Controller reference may be obtained either via direct linking or from appDef - accedo.app#getControllerById
             * @param {Function} controller Controller Constructor reference
             * @param {Object} context      (optional) Context object to be passed to the new controller
             * @param {Function} context    (optional) Callback launched after the new controller's onCreate function has been executed
             * @memberOf accedo.ui.controller#
             */
            changeToController: function(controller, context, callback) {
                this.dispatchEvent('navigate', {controller: controller, context: context, callback: callback});  
            },


            /**
             * Set current view.
             * @param {Object} view JSON definition of the view.
             * @memberOf accedo.ui.controller#
             */
            setView: function(view) {
                //Instantiate view
                
                viewObj = view.type(view);
                viewObj.attachTo(this);

                //Listen for 'detach' events
                this.addEventListener('detach', function() {
                    if (viewObj) {
                        viewObj.detach();
                        viewObj.deinit();
                        viewObj = null;
                    }
                });
//              console.info("[INSTRUMENTATION] setView = " + (instrumentEnd - instrumentStart)  + "ms");
            },
            /**
             * deinit while switching to a new controller
             * @memberOf accedo.ui.controller#
             */
            deinit: function(){
                if (viewObj) {
                    viewObj.deinit();
                    viewObj.detach();
                    viewObj = null;
                }
                this.detach();
                this.removeAllListeners();
                
                var key;
                for (key in this){
                    delete this[key];
                }
            }
        });

        return obj;
    };
});