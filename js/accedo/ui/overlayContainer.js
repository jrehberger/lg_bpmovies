/**
 * @fileOverview creates an overLayContainer which overlays the specified parent or TVArea
 * 
 * 
 * @author Calle Gustafsson <calle.gustafsson@accedobroadband.com>
 */
accedo.define('accedo.ui.overlayContainer',
    ['accedo.ui.container','accedo.utils.dom','accedo.utils.object','accedo.ui.layout.absolute'],
    function(){
     /**
     * @class This container is used to overlay the specified parent or TVArea
     * @name overlayContainer
     * @memberOf accedo.ui      
     * @constructor
     * @extends accedo.ui.container
     * @param {Object} opts The options object
     * @param {Integer} [opts.zIndex="99999"] Which z-index @default 99999
     * @param {String} [opts.position="absolute"] How to position the object, 
     *                  valid CSS position-styles "absolute" | "relative", @default "absolute"
     * @param {String} [opts.top='0px'] top position @default "0px"
     * @param {String} [opts.left='0px'] left position @default "0px"
     * @param {String} [opts.width] Width of the container @default parent width
     * @param {String} [opts.height] Height of the container @default parent height
     */
        return function(opts){
            
            if(accedo.utils.object.isUndefined(opts)){
                opts = {};
            }
            opts.parent = opts.parent || accedo.utils.dom.getById('TVArea');
            
            opts.root = accedo.utils.dom.element('div');
            opts.root.setStyle({
                zIndex:opts.zIndex || 99999,
                position: opts.position || 'absolute',
                top: opts.top || '0px',
                left: opts.left || '0px',
                width:opts.width || opts.parent.getHTMLElement().offsetWidth,
                height:opts.height || opts.parent.getHTMLElement().offsetHeight
                
            });
            
            opts.root.addClass('accedo-ui-overlayContainer');
            opts.root.setStyle({
                width:opts.width || opts.parent.getHTMLElement().offsetWidth,
                height:opts.height || opts.parent.getHTMLElement().offsetHeight
            });

            var containerRoot = accedo.ui.layout.absolute({parent:opts.root, 
             zIndex:opts.zIndex || 99999,
                position: opts.position || 'absolute',
                top: opts.top || '0px',
                left: opts.left || '0px'
            });
            
           
            /** As the above does not set height and width set it manually **/
            containerRoot.getRoot().getParent().setStyle({width:'100%',height:'100%'});
            
            return accedo.utils.object.extend(accedo.ui.container(opts),{
                /**
                 * @name onAppend
                 * @function
                 * @memberOf accedo.ui.overlayContainer#
                 */
                onAppend: function(child){
                    var div = accedo.utils.dom.element('div');
                    accedo.console.log("append");
                    this.getById('containerRoot').appendChild(div)
                    child.attachTo(div);
                   accedo.console.log(child);
                },
                /**
                 * getContainer
                 * @name getContainer
                 * @function
                 * @memberOf accedo.ui.overlayContainer#
                 * @return container
                 */
                getContainer: function(){
                    return containerRoot;
                }
                
            }).containerInit();
        }
    }
);