/**
 * @fileOverview Function for working with Image components.
 * @author <a href="mailto:joe.chang@accedobroadband.com">Joe Chang</a>
 */

accedo.define("accedo.ui.image", ["accedo.utils.object", "accedo.utils.dom", "accedo.ui.component"], function() {

    /**
     * Creates a new image instance.
     * Can dispatch "image:load" Event when loading of the image is complete.
     * @name image
     * @memberOf accedo.ui
     * @class An image implementation
     * @extends accedo.ui.component
     * @constructor
     * @param {Object} opts The options object
     * @param {String} opts.src The source of the image
     * @return {accedo.ui.image} A Button reference
     */
    return function(opts) {
        opts.src  = opts.src || 'data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==';
        
        if (opts.divImg === true){
            opts.root = accedo.utils.dom.element('div');
        }else{
            opts.root = accedo.utils.dom.element('img', {'src': opts.src});
        }
        
        var obj = accedo.utils.object.extend(accedo.ui.component(opts), {

            /**
             * This function dispatches a an image:load event
             * @private
             */
            dispatchLoadEvent: function() {
                this.dispatchEvent('image:load');
            },
            /**
             * Sets the source of an image object.
             * @name setSrc
             * @function
             * @param {String} src The new image source
             * @memberOf accedo.ui.image#
             * @public
             */
            setSrc: function(src) {
                if (opts.divImg === true)
                {
                    //obj.getRoot().setStyle({"background-image":"url("+ src +")"});
                }else
                {
                    var root = obj.getRoot();
                    
                    var htmlElem = root.getHTMLElement();
					htmlElem.onload = function(){
						//accedo.console.log("+++++++++ image[" + src + "] loaded!");
					};
					htmlElem.onerror = function(){
						accedo.console.log("+++++++++++++ image[" + src + "] failed!");
					};
                    root.setAttributes({'src': src});
                }
            },

            /**
             * Sets alternative text for an image object.
             * @name setAlt
             * @function
             * @param {String} alt New Alternative text
             * @memberOf accedo.ui.image#
             * @public
             */
            setAlt: function(alt) {
                obj.getRoot().setAttributes({'alt': alt});
            }
        });
        
        //Listen for browsers click event
        // obj.getRoot().addEventListener('load', function() {
            // obj.dispatchLoadEvent();
        // }, false);

        return obj;
    };
});
