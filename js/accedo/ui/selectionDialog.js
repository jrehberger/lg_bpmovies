/**
 * @fileOverview General purpuse selection popup
 * @extend accedo.ui.popup
 * 
 * @author Calle Gustafsson <calle.gustafsson@accedobroadband.com>
 * 
*/
accedo.define('accedo.ui.selectionDialog',
    [   
        'accedo.ui.popup',
        'accedo.ui.segmentedControl',
        'accedo.ui.segmentedControlItem',
        'accedo.utils.object',
        'accedo.ui.layout.normal'
    ],
    
     function(){
         /**
          * @name selectionDialog
          * @memberOf accedo.ui
          * @class 
          * @constructor
          * @param {Mixed} opts
          * @extends accedo.ui.dialog
          * 
          *
           * @example var selectionDialog = accedo.ui.selectionDialog({
 *                 headline_css: 'headline_css',
 *                 headline_text: 'headline_text [Optional]',
 *                 msg_css: 'msg_css',
 *                 msg_txt: 'msg_txt [Optional]',
 *                 buttons: [
 *                          {
 *                              type: accedo.ui.segmentedControlItem,
 *                              id: 'button1',
 *                              text: 'Button 1',
 *                              icon: 'button1_icon.png' // [Optional]
 *                          },
 *                          {
 *                              type: accedo.ui.segmentedControlItem,
 *                              id: 'button2',
 *                              text: 'Button 2',
 *                          },
 *                          {
 *                              type: accedo.ui.segmentedControlItem,
 *                              id: 'button3',
 *                              text: 'Button 3',
 *                          }
 *                          ]
 *              });
 *              
 */
         
         return function(opts){
             var buttonsContainer = null, self, buttons = [];
             if(accedo.utils.object.isUndefined(opts)){
                 opts = {};
             }
             opts.text = opts.text || "What do you want?";
             opts.headerText = opts.headerText ||'Selection Dialog';
             
             opts.buttons = opts.buttons || [{text:'Close'}];
             
             return accedo.utils.object.extend(accedo.ui.dialog(opts), {
                   
               init: function(){
                     self = this;
                     this.addToHeader(accedo.ui.label({
                        css: opts.headerCss || 'header',
                        text: opts.headerText
                    }));
                    
                    this.addToContent(accedo.ui.label({
                        css: opts.msgCss || 'content',
                        text: opts.text
                    }));
                    
                     buttonsContainer = accedo.ui.segmentedControl();
                    
                     this.addToFooter(buttonsContainer);
                     
                     var z = 0;
                     for(var i=0;i<opts.buttons.length;i++){
                         
                         buttons[i] = accedo.ui.segmentedControlItem({id:'btn_'+i,text:opts.buttons[i].text,event:{type:'accedo:selectionDialog:btnClick',data:{id:i}}});

                         buttons[i].addEventListener('click',
                             buttons[i].dispatchClickEvent
                         );
                         buttons[i].addEventListener('accedo:selectionDialog:btnClick',function(d){
                             self.dispatchEvent('accedo:selectionDialog:click',{btn:d.id});
                             self.close();
                         });
                         buttonsContainer.append(buttons[i]);
                         
                     }
                     
                    
                    
    
                     // Focus first button
                  //   accedo.focus.manager.requestFocus(this.get(opts.buttons[0].id));
                  /*
                    accedo.utils.fn.defer(
                            accedo.utils.fn.bind(accedo.focus.manager.requestFocus,accedo.focus.manager), 
                            buttons[0]
                        );
                            */
                      accedo.focus.manager.requestFocus(buttons[0]);
                            
                    
                     return this;
                 }
                
                 
             }).init();
             
         }
     }
 );