/**
 * @fileOverview An input field, providing basic functionality. It inherits from accedo.ui.button class
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 * Orginal author: <a href="mailto:alexej.kubarev@accedobroadband.com">Alexej Kubarev</a>
 * @example var inputField = accedo.ui.inputField({text:'plz input here', isPassword:false, maxLength:20});
 */

//= require <accedo/utils/object>
//= require <accedo/utils/dom>
//= require <accedo/ui/component>

accedo.define("accedo.ui.inputField", ["accedo.utils.object", "accedo.utils.dom", "accedo.ui.component"], function() {
    /**
     * Creates a new label instance.
     * @name inputField
     * @memberOf accedo.ui
     * @class A inputField implementation
     * @extends accedo.ui.component
     * @constructor
     * @param {Object} opts The options object
     * @param {String} opts.text The text label
     */
    return function(opts) {
        var maxLength = opts.maxLength || 20,
            isPassword = opts.isPassword || false,
            labelText  = opts.text || '';
        
        /**
         * This function creates a button object.
         * @private
         */
        var create = function() {
            var element = accedo.utils.dom.element('div');
            element.addClass('accedo-ui-inputfield');
            label = accedo.ui.label({
                parent: element
            });
            return element;
        };
        opts.root = create(opts);

        var inputField = accedo.utils.object.extend(accedo.ui.button(opts), {
            /**
             * This function dispatches click, mouseover and mouseout events
             * @private
             */
            dispatchClickEvent: function() {
                inputField.dispatchEvent('click');
            },
            dispatchMouseOverEvent: function() {
                accedo.focus.managerMouse.requestFocus(opts.root);
            },
            dispatchMouseOutEvent: function() {
                accedo.focus.managerMouse.releaseFocus();
            },
            /**
             * Append character to the input field
             * @name appendChar
             * @function
             * @param {String} ch The character to append to
             * @memberOf accedo.ui.inputField#
             * @example inputField.appendChar('a');
             */
            appendChar: function(ch) {
                var t = this.getText();
                this.setText(t + '' + ch);
            },
            /**
             * Delete the last character in the input field
             * @name deleteChar
             * @function
             * @memberOf accedo.ui.inputField#
             * @public
             */
            deleteChar: function() {
                var t = this.getText();
                var newT = "";
                if(t.length > 1) {
                    newT = t.truncate(t.length - 1, "");
                }
                this.setText(newT);
            },
            /**
             * Set max length of input field
             * @name setMaxLength
             * @function
             * @param {Integer} l The number of characters to set
             * @memberOf accedo.ui.inputField#
             * @example inputField.setMaxLength(100);
             */
            setMaxLength: function(l) {
                maxLength = l;
            },
            /**
             * Get the input field text
             * @name getText
             * @function
             * @return {String} labelText Return the current label text
             * @memberOf accedo.ui.inputField#
             * @example inputField.getText();
             */
            getText: function() {
                return labelText;
            },
            /**
             * Set text on the input field
             * @name setText
             * @function
             * @param {String} text
             * @memberOf accedo.ui.inputField#
             * @example inputField.setText();
             */
            setText: function(text) {
                if(!text) {
                    text = "";
                }

                var textChanged = (text != labelText);

                this.text = text.truncate(maxLength, "");
                if(isPassword) {
                    var len = labelText.length;
                    var t = "";
                    for(var i = 0; i < len; i++){
                        t += "*";
                    }
                    this.root.update(t);
                } else {
                    this.root.update(text.escapeHTML());
                }

                if(textChanged && this.onTextChanged){
                    this.onTextChanged();
                }
            },
            /**
             * Clear input field text
             * @name clearText
             * @function
             * @memberOf accedo.ui.inputField#
             * @example inputField.clearText();
             */
            clearText: function() {
                // this.contentHandle.update();
                this.setText("");
            },
            /**
             * Set the focus to input field
             * @name setFocus
             * @function
             * @memberOf accedo.ui.inputField#
             * @example inputField.setFocus();
             */
            setFocus: function()
            {
                this.root.addClass("focused");
            },
            /**
             * Remove the focus on input field
             * @name removeFocus
             * @function
             * @memberOf accedo.ui.inputField#
             * @example inputField.removeFocus();
             */
            removeFocus: function()
            {
                this.root.removeClass("focused");
            },
            /**
             * Set selection on the input field
             * @name setSelection
             * @function
             * @memberOf accedo.ui.inputField#
             * @example inputField.setSelection();
             */
            setSelection: function()
            {
                this.root.addClass("selected");
            },
            /**
             * Remove selection on the input field
             * @name removeSelection
             * @function
             * @memberOf accedo.ui.inputField#
             * @example inputField.removeSelection();
             */
            removeSelection: function()
            {
                this.root.removeClass("selected");
            },
            /**
             * Check if input field is selected
             * @name isSelected
             * @function
             * @return {Boolean} true if selected; false otherwise
             * @memberOf accedo.ui.inputField#
             * @example inputField.isSelected();
             */
            isSelected: function()
            {
                return this.root.hasClass("selected");
            },
            /**
             * Check if input field is focused
             * @name isFocused
             * @function
             * @return {Boolean} true if focused; false otherwise
             * @memberOf accedo.ui.inputField#
             * @example inputField.isFocused();
             */
            isFocused: function()
            {
                return this.root.hasClass("focused");
            }

        });
        //Set initial text
        inputField.setText(opts.text);
        
        opts.root.addEventListener('click', inputField.dispatchClickEvent, false);
        opts.root.addEventListener('mouseover', inputField.dispatchMouseOverEvent, false);
        opts.root.addEventListener('mouseout', inputField.dispatchMouseOutEvent, false);

        return inputField;
    };
});