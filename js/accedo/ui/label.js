/**
 * @fileOverview A UI Label object, providing basic functionality.
 * @author <a href="mailto:alexej.kubarev@accedobroadband.com">Alexej Kubarev</a>
 */

//= require <accedo/utils/object>
//= require <accedo/utils/dom>
//= require <accedo/ui/component>

accedo.define("accedo.ui.label", ["accedo.utils.object", "accedo.utils.dom", "accedo.ui.component"], function() {

    /**
     * Creates a new label instance.
     * @name label
     * @memberOf accedo.ui
     * @class A label implementation
     * @extends accedo.ui.component
     * @constructor
     * @param {Object} opts The options object
     * @param {String} opts.text The text label
     */
    return function(opts) {

        /**
         * Internal storage for label text.
         * @field
         * @private
         */
        opts.text = opts.text || '';

        opts.root = accedo.utils.dom.element('div');
        opts.root.addClass('accedo-ui-label');

        var obj = accedo.utils.object.extend(accedo.ui.component(opts), {

            /**
             * Sets the text of the label.
             * @name setText
             * @function
             * @param {String} text The text to set
             * @memberOf accedo.ui.label#
             * @public
             */
            setText: function(text) {
                //Use this.getRoot() instead of opts.root as it seems there are cases that opts.root will be changed by the other elements
                this.getRoot().setText(text);
            },

            /**
             * Gets the text of the label.
             * @name getText
             * @function
             * @memberOf accedo.ui.label#
             * @public
             * @return {String} Text of the label
             */
            getText: function() {
                return this.getRoot().getText();
            }

        });

        //Set initial text
        obj.setText(opts.text);

        return obj;
    };
});
