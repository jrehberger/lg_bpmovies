/**
 * @fileOverview Global app handling.
 * @author <a href="mailto:alex@accedobroadband.com">Alexej Kubarev</a>
 */

//= require <accedo/utils/object>
//= require <accedo/utils/dom>
//= require <accedo/focus/manager>

/**
 * @name accedo.app
 * @namespace Global app handling.
 */

accedo.define("accedo.app", ["accedo.utils.object", "accedo.utils.dom"], function() {

    var root = accedo.utils.dom.element('div', {id:'TVArea'});

    return {

        appDef:              null,
        currentController:   null,
        controllers:         {},

        /**
         * Set Application definition.
         * @name setApp
         * @memberOf accedo.app
         * @function
         * @param {Object} appDef Application definition object
         */
        setApp: function(appDef) {
            this.appDef = appDef;
        },
        /**
         * Get root object
         * @name getRoot
         * @memberOf accedo.app
         * @function
         * @return {Mixed} root object
         */
        getRoot: function() {
            return root;
        },

        /**
         * Get Controller constructor by ID mapping.
         * @name getControllerById
         * @memberOf accedo.app
         * @function
         * @param {String} id String identifier.
         * @return Constructor for controller found by ID
         */
        getControllerById: function(id) {
            return this.controllers[id].controller;
        },

        /**
         * Changes current view to a new controller.
         * @name changeToController
         * @memberOf accedo.app
         * @function
         * @param {Function} controller Constructor for the new controller to change to
    	 * @param {Object} params Parameters holder to be passed to the controller's onCreate function, if any (optional)
         */
        changeToController: function(controller, params) {

            if (this.currentController) {
                this.currentController.detach();
            }

            if(accedo.utils.object.isString(controller)) {
                controller = this.getControllerById(controller);
            }

            this.currentController = controller({
                root: accedo.utils.dom.element('div')
            });

            //Fire event
            if (accedo.utils.object.isFunction(this.currentController.onCreate)) {
                this.currentController.onCreate(params);
            }

            this.currentController.attachTo(root);
        },

        /**
         * Add controller to application definition programatically.
         * @name addController
         * @memberOf accedo.app
         * @function
         * @param {String} id Unique controller identifier
         * @param {Function} controllerDef reference to controller constructor
         * @public
         */
        addController: function(id, controllerDef) {
            this.controllers[id] = controllerDef;
        },

        /**
         * Gets pointer for current controller instance.
         * @name getCurrentController
         * @memberOf accedo.app
         * @function
         * @return Current controller instance
         */
        getCurrentController: function() {
            return this.currentController;
        },

        /**
         * Main bootstrap function to be called onLoad of DOM (or any time desired).
         * @name main
         * @memberOf accedo.app
         * @function
         */
        main: function() {
            document.body.appendChild(root.getHTMLElement());

            if (accedo.utils.object.isPlainObject(this.appDef)) {
                if (accedo.utils.object.isPlainObject(this.appDef.controllers)) {

                    var mainControllerId, controllerDef, firstControllerId, id, controllers = this.appDef.controllers;

                    for (id in controllers) {
                        controllerDef = this.appDef.controllers[id];

                        //Make sure we have the required attributes
                        if (accedo.utils.object.isFunction(controllerDef.controller)) {

                            this.controllers[id] = controllerDef;

                            if (!firstControllerId) {
                                firstControllerId = id;
                            }

                            if (controllerDef.main === true) {
                                mainControllerId = id;
                            }
                        }
                    }

                    if (!mainControllerId && firstControllerId) {
                        mainControllerId = firstControllerId;
                    }

                    this.changeToController(mainControllerId);
                }
            }

        }
    };
});