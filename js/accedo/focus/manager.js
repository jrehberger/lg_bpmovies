/**
 * @fileOverview Focus Manager implementation.
 * @author <a href="mailto:alexej.kubarev@acedobroadband.com">Alexej Kubarev</a>
 */

 //= require <accedo/utils/array>
 //= require <accedo/app>

accedo.define("accedo.focus.manager", ["accedo.utils.array", "accedo.app", "accedo.utils.fn"], function() {

    /**
     * @name manager
     * @memberOf accedo.focus
     * @class Focus manager is reponsible for application focus handling.
     * Only one focus manager is avaialble during whole lifecycle.
     * @constructor
     */
    var focusManager = (function() {

        /**
         * Current focus object.
         * @field
         * @private
         */
        var currentFocus = null;
        
      



        return {

         


            /**
             * Explicitly focus given component, if possible.
             * @param {accedo.ui.component} component Component to focus
             * @memeberOf accedo.focus.manager#
             */
            requestFocus: function(component) {
                if(currentFocus) {
                    if (accedo.utils.object.isFunction(currentFocus.blur)){
                        currentFocus.blur();
                    }
                }
                currentFocus = component;
                
                if (currentFocus) {
                    if (accedo.utils.object.isFunction(currentFocus.focus)){
                        currentFocus.focus();
                    }
                }
            },

            /**
             * return the current focus
             * @return {accedo.ui.component} current focused component
             * @memeberOf accedo.focus.manager#
             */
            getCurrentFocus : function(){
                if(currentFocus){
                    return currentFocus;
                }else{
                    return null;
                }
            },

          
            
            /**
             * Change focus in given direction.
             * If any focusable element found - it will be focused and current element will loose focus.
             * @param {accedo.focus.manager.FOCUS_UP | accedo.focus.manager.FOCUS_DOWN | accedo.focus.manager.FOCUS_LEFT | accedo.focus.manager.FOCUS_RIGHT} direction Focus direction
             * @memeberOf accedo.focus.manager#
             */
            focusDirectionChange: function(direction) {

                var focusable;
                var ctrl = accedo.app.getCurrentController();

                if(currentFocus) {
                    //Block checking predefines
                    var opt;
                    if(direction === focusManager.FOCUS_UP) {
                        opt = "nextUp";
                    } else if(direction === focusManager.FOCUS_DOWN) {
                        opt = "nextDown";
                    } else if(direction === focusManager.FOCUS_LEFT) {
                        opt = "nextLeft";
                    } else if(direction === focusManager.FOCUS_RIGHT) {
                        opt = "nextRight";
                    }

                    opt = currentFocus.getOption(opt);
                    if(!accedo.utils.object.isUndefined(opt)) {
                        if(opt === null) {
                            //Explicit empty difinition found - block navigation
                            return;
                        }
                        if (typeof opt == 'function') {
                        /* Usually opt is a component ID, but sometimes a callback function can be very convenient!
                        That function can do some processing and not return anything,
                        or also return a component ID so that it gets the focus */
                            opt = opt.call(currentFocus);
                        }
                        //No 'else' here - opt may have just been changed
                        //Usually opt is a component ID
                        if (typeof opt == 'string') {
                            focusable = ctrl.get(opt);
                        }
                    }
                    
                }

                //Focusable element is defined? If so, request focus for it
                if(focusable) {
                    this.requestFocus(focusable);
                    return true;
                }else{
                    return false;
                }
            }
        };
    })();

    /**
     * Focus direction: UP.
     * @static
     * @field
     * @name FOCUS_UP
     * @memberOf accedo.focus.manager
     */
    focusManager.FOCUS_UP            = 0x01;
    /**
     * Focus direction: DOWN.
     * @static
     * @field
     * @name FOCUS_DOWN
     * @memberOf accedo.focus.manager
     */
    focusManager.FOCUS_DOWN          = 0x02;
    /**
     * Focus direction: LEFT.
     * @static
     * @field
     * @name FOCUS_LEFT
     * @memberOf accedo.focus.manager
     */
    focusManager.FOCUS_LEFT          = 0x04;
    /**
     * Focus direction: RIGHT.
     * @static
     * @field
     * @name FOCUS_RIGHT
     * @memberOf accedo.focus.manager
     */
    focusManager.FOCUS_RIGHT         = 0x08;


    return focusManager;
});