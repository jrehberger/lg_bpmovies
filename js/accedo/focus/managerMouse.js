/**
 * @fileOverview Focus Manager implementation.
 * @author <a href="mailto:alexej.kubarev@acedobroadband.com">Alexej Kubarev</a>
 */

 //= require <accedo/utils/array>
 //= require <accedo/app>

accedo.define("accedo.focus.managerMouse", ["accedo.utils.array", "accedo.app", "accedo.utils.fn"], function() {

    /**
     * @name manager
     * @memberOf accedo.focus
     * @class Focus manager is reponsible for application focus handling.
     * Only one focus manager is avaialble during whole lifecycle.
     * @constructor
     */
    var mouseFocusManager = (function() {

        /**
         * Current focus object.
         * @field
         * @private
         */
        var currentMouseFocus = null;
        return {            /**
             * Explicitly focus given component, if possible.
             * @param {accedo.ui.component} component Component to focus
             * @memeberOf accedo.focus.manager#
             */
            requestFocus: function(component) {
                //accedo.console.log('/// request new mouse focus ////');
                if(currentMouseFocus) {
                    if (accedo.utils.object.isFunction(currentMouseFocus.removeClass)){
                        currentMouseFocus.removeClass("mouse-focused");
                    }
                }
                currentMouseFocus = component;
                
                if (currentMouseFocus) {
                    if (accedo.utils.object.isFunction(currentMouseFocus.addClass)){
                        currentMouseFocus.addClass("mouse-focused");
                    }
                }
            },
            
            releaseFocus: function() {
                //accedo.console.log("::: in releaseFocus ::::");
                if(currentMouseFocus) {
                    if (accedo.utils.object.isFunction(currentMouseFocus.removeClass)){
                        currentMouseFocus.removeClass("mouse-focused");
                    }
                    currentMouseFocus = null;
                }
            },

            /**
             * return the current focus
             * @return {accedo.ui.component} current focused component
             * @memeberOf accedo.focus.manager#
             */
            getCurrentFocus : function(){
                if(currentMouseFocus){
                    return currentMouseFocus;
                }else{
                    return null;
                }
            }
        };
    })();

    return mouseFocusManager;
});