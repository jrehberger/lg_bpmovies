/**
 * @fileOverview Event dispatcher functionality
 * @author <a href="mailto:alexej.kubarev@accedobroadband.com">Alexej Kubarev</a>
 */

//= require <accedo/utils/object>
//= require <accedo/utils/fn>
//= require <accedo/utils/hash>

accedo.define("accedo.events.dispatcher", ["accedo.utils.object", "accedo.utils.array", "accedo.utils.fn", "accedo.utils.hash"], function() {


    return function() {
    /**
     * @class Allows registration
     * @constructor
     * @name dispatcher
     * @memberOf accedo.events
     */

        var obj =  {
           
            /**
             * A hashmap for event handling
             * @private
             * @filed
             * 
             */
            _eventCallbacks: accedo.utils.hash({}),

            /**
             * Gets a list of handlers for given type
             * @name _getHandlersForType
             * @param {String} Type description
             * @return {Array} Handlers for given type
             * @private
             * @memberOf accedo.events.dispatcher#
             */
            _getHandlersForType: function(type) {
                var handlers = this._eventCallbacks.get(type);
                if(!handlers && !accedo.utils.object.isArray(handlers)) {
                    handlers = [];
                }

                return handlers;
            },

            /**
             * Dispatches an event with additional data.
             * @name dispatchEvent
             * @param {String} type String type to dispatch
             * @param {Object} data Memo object.
             * @function
             * @public
             * @memberOf accedo.events.dispatcher#
             * 
             */
            dispatchEvent: function(type, data) {
                var handlers = this._getHandlersForType(type);
                
                data = data || {};

                accedo.utils.array.each(handlers, function(handler) {
                    if(accedo.utils.object.isFunction(handler)) {
                        handler(data);
                    }
                });
            },

            /**
             * Registers an event listener for a certain type
             * @name addEventListener
             * @param {String} type Event type
             * @param {Function} handler Event listener function
             * @function
             * @public
             * @memberOf accedo.events.dispatcher#
             */
            addEventListener: function(type, handler, context) {
                var handlers = this._eventCallbacks.get(type);
                if(!handlers || !accedo.utils.object.isArray(handlers)) {
                    handlers = [];
                    this._eventCallbacks.set(type, handlers);
                }

                if(context) {
                    handler = accedo.utils.fn.bind(handler, context);
                }

                //Do not add handler if it is already registred
                if(accedo.utils.array.indexOf(handlers, handler) === -1) {
                    handlers.push(handler);
                }
            },

            /**
             * Removes an event listener
             * @name removeEventListener
             * @param {String} type Event type
             * @param {Function} handler Event listener function
             * @function
             * @public
             * @memberOf accedo.events.dispatcher#
             */
            removeEventListener: function(type, handler) {
                var handlers = this._eventCallbacks.get(type);
                if (handlers && handlers.length) {
                    var i = accedo.utils.array.indexOf(handlers, handler);
                    if(i !== -1) {
                        handlers.splice(i, 1);
                    }
                }
            },

            /**
             * Removes all event listeners for a type
             * @param {String} type Event type
             * @name removeAllListeners
             * @function
             * @public
             * @memberOf accedo.events.dispatcher#
             */
            removeAllListeners: function(type) {
                if (!accedo.utils.object.isUndefined(type)){
                    this._eventCallbacks.unset(type);
                }else{
                    this._eventCallbacks = accedo.utils.hash({});
                }
            }
        };
        //These code are to check memory issue
        // if (typeof(global_events) == 'undefined'){
            // global_events = [];
        // }
        // global_events.push(obj._eventCallbacks);
        return obj;

    };

});