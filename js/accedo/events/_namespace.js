/**
 * @fileOverview defines the namespace for accedo.events
 * 
 * 
 * @author Calle Gustafsson <calle.gustafsson@accedobroadband.com>
 */
/**
 * @name accedo.events
 * @namespace
 */