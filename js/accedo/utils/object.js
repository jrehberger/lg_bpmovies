/**
 * @fileOverview Convenience methods for Objects.
 * @author <a href="mailto:alex@accedobroadband.com">Alexej Kubarev</a>
 */

//= require <accedo>
    /**
     * Object utilities.
     * If configuration parameter "extendObject" is set, functions will extend naitive Object.
     * @name object
     * @memberOf accedo.utils
     * @class
     */
accedo.define("accedo.utils.object", null, function() {

    /**
     * A reference to naitive toString function.
     * @private
     */
    var _toString = Object.prototype.toString,

    /**
     *@ignore
     */
    objUtils = {
        /**
         * Extends a dest with properties from src.
         * Possible deep copy/extending by setting deep = true.
         * @name extend
         * @memberOf accedo.utils.object
         * @function
         * @param {Object} dest Destination object to be extended
         * @param {Object} src Source object with properties to extend with
         * @param {Boolean} [deep] Deep copy
         * @return {Object} extended object
         * @static
         * @public
         */
        
        
        /**
         * we ignore this as it will give warnings and incorrect namespace in the docs
         *@ignore
        */
        extend: function(dest, src, deep) {
            var objIsArray, orig, copy, clone, att;
            for (att in src) {

                orig = dest[att];
                copy = src[att];

                // Prevent never-ending loop
                if ( dest === copy ) {
                    continue;
                }

                if(deep === true && copy && (objIsArray = objUtils.isArray(copy) || objUtils.isPlainObject(copy))) {
                    if(objIsArray) {
                        objIsArray = false;
                        clone = orig && objUtils.isArray(orig) ? orig : [];
                    } else {
                        clone = orig && objUtils.isPlainObject(src) ? orig : {};
                    }

                    // Never move original objects, clone them
                    dest[att] = objUtils.extend(clone, copy, true);
                } else if (!objUtils.isUndefined(copy)) {
                    dest[att] = copy;
                }
            }
            return dest;
        },

        /**
         * Clones an object.
         * @name clone
         * @function
         * @memberOf accedo.utils.object
         * @param {Object} obj Object to check
         * @param {Boolean} [deep] Deep copy
         * @return {Object} a copy of the object
         * @static
         * @public
         */
        
        /**
         * we ignore this as it will give warnings and incorrect namespace in the docs
         *@ignore
        */
        clone: function(obj, deep) {
            return objUtils.extend({}, obj, deep);
        },

        /**
         * Checks is object is undefined.
         * @name isUndefined
         * @function
         * @memberOf accedo.utils.object
         * @param {Object} obj Object to check
         * @return {Boolean} true if object is undefined, false otherwise
         * @static
         * @public
         */
        
        /**
         * we ignore this as it will give warnings and incorrect namespace in the docs
         *@ignore
        */
        isUndefined: function(obj) {
            return typeof obj === "undefined";
        },

        /**
         * Checks if the object is a plain object, created using {} or new Object().
         * @name isPlainObject
         * @function
         * @memberOf accedo.utils.object
         * @param {Object} obj Object to test.
         * @return {Boolean} true if parameter is an object, false otherwise
         * @static
         * @public
         */
        
        /**
         * we ignore this as it will give warnings and incorrect namespace in the docs
         *@ignore
        */
        isPlainObject: function(obj) {
            if(typeof obj !== "object") {
                 return false;
            }

            //Loop through all properties to get access to the last one.
            var key;
            for ( key in obj ) {}

            //Own properties are iterated first, so it is enough to look at the last one.
            return (key === undefined || obj.hasOwnProperty(key));
        },

        /**
         * Checks if the object is an array.
         * @name isArray
         * @function
         * @memberOf accedo.utils.object
         * @param {Object} obj Object to check
         * @return {Boolean} true if object is an Array, false otherwise
         * @public
         * @static
         */
        
        /**
         * we ignore this as it will give warnings and incorrect namespace in the docs
         *@ignore
        */
        isArray: function(obj) {
            //Information  on the best way to perform this type of check is taken from:
            // http://perfectionkills.com/instanceof-considered-harmful-or-how-to-write-a-robust-isarray/

            return _toString.call(obj) === '[object Array]';
        },

        /**
         * Checks if the object is Function.
         * @name isFunction
         * @function
         * @memberOf accedo.utils.object
         * @param {Object} obj Object to check
         * @return {Boolean} true if object is a Function, false otherwise
         * @public
         * @static
         */
        
        /**
         * we ignore this as it will give warnings and incorrect namespace in the docs
         *@ignore
        */
        isFunction: function(obj) {
            return _toString.call(obj) === '[object Function]';
        },

        /**
         * Checks if the object is a string.
         * @name isString
         * @function
         * @memberOf accedo.utils.object
         * @param {Object} obj Object to check
         * @return {Boolean} true if object is a String, false otherwise
         * @public
         * @static
         */
        
        /**
         * we ignore this as it will give warnings and incorrect namespace in the docs
         *@ignore
        */
        isString: function(obj) {
            return _toString.call(obj) === '[object String]';
        },

        /**
         * Checks if the object is a date.
         * @name isDate
         * @function
         * @memberOf accedo.utils.object
         * @param {Object} obj Object to check
         * @return {Boolean} true if object is a Date, false otherwise
         * @public
         * @static
         */
        
        /**
         * we ignore this as it will give warnings and incorrect namespace in the docs
         *@ignore
        */
        isDate: function(obj) {
            return _toString.call(obj) === '[object Date]';
        },

        /**
         * Checks if the object is a DataSource.
         * @name isDataSource
         * @function
         * @memberOf accedo.utils.object
         * @param {Object} obj Object to check
         * @return {Boolean} true if object is a DataSource, false otherwise
         * @public
         * @static
         */
        
        /**
         * we ignore this as it will give warnings and incorrect namespace in the docs
         *@ignore
        */
        isDataSource: function(obj) {
            return (typeof obj==="object") && (obj.toString() === '[object DataSource]');
        },

        /**
         * Checks if the object is a DOM element.
         * @name isDOMElement
         * @function
         * @memberOf accedo.utils.object
         * @param {Object} obj Object to check
         * @return {Boolean} true if object is a DOM element, false otherwise
         * @public
         * @static
         */
        
        /**
         * we ignore this as it will give warnings and incorrect namespace in the docs
         *@ignore
        */
        isDOMElement: function(obj) {
            //Browsers not supporting W3 DOM2 don't have HTMLElement and
            //an exception is thrown and we end up here. Testing some
            //properties that all elements have. (works on IE7)
            return (typeof obj === "object") &&
                (obj.nodeType === 1) && (typeof obj.style === "object") &&
                (typeof obj.ownerDocument === "object") &&
                (typeof obj.nodeName === "string");
        },

        /**
         * Checks if the object is an instance of accedo.utils.Hash.
         * @name isHash
         * @function
         * @memberOf accedo.utils.object
         * @param {Object} obj Object to check
         * @return {Boolean} true if object is a Hash, false otherwise
         * @public
         * @static
         */
        
        /**
         * we ignore this as it will give warnings and incorrect namespace in the docs
         *@ignore
        */
        isHash: function(obj) {
            try {
                return obj.toString() === "accedo.utils.hash";
            } catch (ex) {
                return false;
            }
        },
        
        /**
         * Tests whether an object is empty or not (an empty object is {})
         * @name isEmpty
         * @function
         * @memberOf accedo.utils.object
         * @param {Object} obj Object to check
         * @return {Boolean} true if object is empty, false otherwise
         * @public
         * @static
         */
        
        /**
         * we ignore this as it will give warnings and incorrect namespace in the docs
         *@ignore
        */
        isEmpty: function(obj) {
            var prop;
            for(prop in obj) {
                if(obj.hasOwnProperty(prop)) {
                    return false;
                }
            }

            return true;
        },

        /**
         * Tests whether an object is null
         * @name isNull
         * @function
         * @memberOf accedo.utils.object
         * @param {Object} obj Object to check
         * @return {Boolean} true if object is empty, false otherwise
         * @public
         * @static
         */
        
        /**
         * we ignore this as it will give warnings and incorrect namespace in the docs
         *@ignore
        */
        isNull: function(obj){
            return obj === null;
        },
        
        /**
         * Returns a JSON string. (yet to test whether it works fine for all cases)
         * @name toJSON
         * @function
         * @memberOf accedo.utils.object
         * @param {Object} obj Object to check
         * @return {String} a string of the object
         * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
         * @public
         * @static
         */
        
        /**
         * we ignore this as it will give warnings and incorrect namespace in the docs
         *@ignore
        */
        toJSON:  function(object) {
            var type = typeof object,
                results = [],
                i = 0, 
                value, 
                property, 
                len;
            
            switch (type) {
              case 'undefined':
              case 'function':
              case 'unknown': return;
              case 'boolean': return object.toString();
              case 'string' : return '"' + object.replace(/"/g,'\\"') + '"';
              case 'number' : return "" + object;
            }

            if(this.isArray(object)) {
                results = [];
                len = object.length;

                for(; i < len; i++) {
                    value = accedo.utils.object.toJSON(object[i]);
                    if (!this.isUndefined(value)) {
                        results.push(value);   
                    }
                }
                return '[' + results.join(',') + ']';
            } else if (object === null) {
                return 'null';
            } else if (accedo.utils.object.isFunction(object.toJSON)) {
               return object.toJSON();
            } else if (accedo.utils.object.isDOMElement(object)) {
                return;
            }

            results = [];
            for (property in object) {
              value = accedo.utils.object.toJSON(object[property]);
              if (!accedo.utils.object.isUndefined(value)) {
                  results.push('"' + property.toString() + '":' + value);
              }
            }

            return '{' + results.join(',') + '}';
        }
    };


    /**
     * @scope Object
     */
    // Extend Naitive Object if so required
    if(accedo.getConfigParameter("extendObject")) {
        objUtils.extend(Object, {
            /**
             * Extends a dest with properties from src.
             * Possible deep copy/extending by setting deep = true.
             * Shortcut function to {@link accedo.utils.object.extend}
             * @param {Object} dest Destination object to be extended
             * @param {Object} src Source object with properties to extend with
             * @param {Boolean} [deep] Deep copy
             * @return {Object} extended object
             * @see accedo.utils.object.extend
             * @memberOf Object#
             * @function
             * @static
             * @requires "extendObject" configuration parameter
             */
            extend: objUtils.extend,
            /**
             * Clones an object.
             * Shortcut function to {@link accedo.utils.object.clone}
             * @param {Object} obj Object to check
             * @return {Object} a copy of the object
             * @see accedo.utils.object.clone
             * @memberOf Object#
             * @function
             * @static
             * @requires "extendObject" configuration parameter
             */
            clone: objUtils.clone,
            /**
             * Checks if the object is a plain object, created using {} or new Object().
             * Shortcut function to {@link accedo.utils.object.isPlainObject}
             * @param {Object} obj Object to test.
             * @return {Boolean} true if parameter is an object, false otherwise
             * @see accedo.utils.object.isPlainObject
             * @memberOf Object#
             * @function
             * @static
             * @requires "extendObject" configuration parameter
             */
            isPlainObject: objUtils.isPlainObject,
            /**
             * Checks is object is undefined.
             * Shortcut function to {@link accedo.utils.object.isUndefined}
             * @param {Object} obj Object to check
             * @return {Boolean} true if object is undefined, false otherwise
             * @see accedo.utils.object.isUndefined
             * @memberOf Object#
             * @function
             * @static
             * @requires "extendObject" configuration parameter
             */
            isUndefined: objUtils.isUndefined,
            /**
             * Checks if the object is an array.
             * Shortcut function to {@link accedo.utils.object.isArray}
             * @param obj Object to check
             * @return true if object is an Array, false otherwise
             * @see accedo.utils.object.isArray
             * @memberOf Object#
             * @function
             * @static
             * @requires "extendObject" configuration parameter
             */
            isArray: objUtils.isArray,
            /**
             * Checks if the object is a function.
             * Shortcut function to {@link accedo.utils.object.isFunction}
             * @param obj Object to check
             * @return true if object is a Function, false otherwise
             * @see accedo.utils.object.isFunction
             * @memberOf Object#
             * @function
             * @static
             * @requires "extendObject" configuration parameter
             */
            isFunction: objUtils.isFunction,
            /**
             * Checks is object is a String.
             * Shortcut function to {@link accedo.utils.object.isString}
             * @param {Object} obj Object to check
             * @return {Boolean} true if object is a String, false otherwise
             * @see accedo.utils.object.isString
             * @memberOf Object#
             * @function
             * @static
             * @requires "extendObject" configuration parameter
             */
            isString: objUtils.isString,
            /**
             * Checks is object is a DataSource.
             * Shortcut function to {@link accedo.utils.object.isDate}
             * @param {Object} obj Object to check
             * @return {Boolean} true if object is a DataSource, false otherwise
             * @see accedo.utils.object.isDataSource
             * @memberOf Object#
             * @function
             * @static
             * @requires "extendObject" configuration parameter
             */
            isDataSource: objUtils.isDataSource,
            /**
             * Checks is object is a Date.
             * Shortcut function to {@link accedo.utils.object.isDate}
             * @param {Object} obj Object to check
             * @return {Boolean} true if object is a Date, false otherwise
             * @see accedo.utils.object.isDate
             * @memberOf Object#
             * @function
             * @static
             * @requires "extendObject" configuration parameter
             */
            isDate: objUtils.isDate,
            /**
             * Checks is object is a Hash (instance of {@link accedo.utils.Hash}).
             * Shortcut function to {@link accedo.utils.object.isHash}
             * @param {Object} obj Object to check
             * @return {Boolean} true if object is undefined, false otherwise
             * @see accedo.utils.object.isHash
             * @memberOf Object#
             * @function
             * @static
             * @requires "extendObject" configuration parameter
             */
            isHash: objUtils.isHash,
            /**
             * Checks is object is a Hash (instance of {@link accedo.utils.isDOMElement}).
             * Shortcut function to {@link accedo.utils.object.isDOMElement}
             * Checks if the object is a DOM element.
             * @param {Object} obj Object to check
             * @return {Boolean} true if object is a DOM element, false otherwise
             * @see accedo.utils.object.isDOMElement
             * @memberOf Object#
             * @function
             * @static
             * @requires "extendObject" configuration parameter
             */
            isDOMElement: objUtils.isDOMElement,
            /**
             * Tests whether an object is empty or not (an empty object is {})
             * @param {Object} obj Object to check
             * @return {Boolean} true if object is empty, false otherwise
             * @public
             * @static
             */
            isEmpty: objUtils.isEmpty,
             /**
             * Returns a JSON string.
             * @param {Object} obj Object to check
             * @return {String} a string of the object
             * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
             * @see accedo.utils.object.toJSON
             * @memberOf Object#
             * @function
             * @static
             * @requires "extendObject" configuration parameter
             */
            toJSON: objUtils.toJSON
        });
    }

    return objUtils;
});