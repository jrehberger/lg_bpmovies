/**
 * @fileOverview A set of convenience functions used for communicating with a server.
 * @author <a href="mailto:alex@accedobroadband.com">Alexej Kubarev</a>
 */

//= require <accedo>
//= require <accedo/utils/object>
//= require <accedo/utils/fn>
//= require <accedo/utils/string>

accedo.define("accedo.utils.ajax", ["accedo.utils.object", "accedo.utils.fn", "accedo.utils.string"], function() {
    /**
     * @name ajax
     * @memberOf accedo.utils
     * @namespace
     */
    /**
     * Gets a transport by trying all of the know XHR objects
     * @return A new XHR object or false if none found
     * @private
     */
    var _getTransport = function() {
        return accedo.Try(
            function() {
                return new XMLHttpRequest();
            },
            function() {
                return new ActiveXObject('Msxml2.XMLHTTP');
            },
            function() {
                return new ActiveXObject('Microsoft.XMLHTTP');
            }
            ) || false;
    };

    return {

        /**
         * Gets a transport by trying all of the know XHR objects
         * @return A new XHR object or false if none found
         * @private
         */
        _getTransport: function() {
            return accedo.Try(
                function() {
                    return new XMLHttpRequest();
                },
                function() {
                    return new ActiveXObject('Msxml2.XMLHTTP');
                },
                function() {
                    return new ActiveXObject('Microsoft.XMLHTTP');
                }
                ) || false;
        },

        /**
         * Makes a XHR request to the given URL with parameters.
         * @name request
         * @memberOf accedo.utils.ajax
         * @class
         * @param {String} url URL to make a request to
         * @param {Object} [options] An options map
         * @param {String} [options.method] HTTP Method to use for this request, i.e 'get' or 'post' or 'delete' or 'put'. If method is not 'get' or 'post' or 'delete' or 'put' it is automatically changed
         * to 'post' and _method parameter is appended to the querystring of the request. Default: 'get'
         * @param {Boolean} [options.async] Does request need to be asyncrounious. Default: 'true'
         * @param {String} [options.contentType] Content-Type header of the request. Default: 'application/x-www-form-urlencoded'
         * @param {String} [options.encoding] Encoding of the request. Default: 'UTF-8'
         * @param {String} [options.postBody] Post body (only used for POST requests).
         * @param {String|accedo.utils.Hash} [options.parameters] Parameters to be sent with the request
         * @param {Function} [options.onSuccess] Event handler function to call when request has been completed
         * @param {Function} [options.onFailure] Event handler function to call when there was a communication error
         * @param {Function} [options.onAbort] Event handler function to call when AJAX is aborted
         * @param {Function} [options.onComplete] Event handler function to call when the request process is complete, always called no matter if request was successfull or not
         * @param {Function} [options.onException] Event handler function to call when an uncaught exception has been thrown either during request or in one of the handler functions.
         * @return {accedo.utils.ajax.request} instance of this request
         * @constructor
         */
        request: function(url, options) {

            /**
             * @scope accedo.utils.ajax.request
             */
            return new (function() {
                /**
                 * URL to send the request to.
                 * @memberOf accedo.utils.ajax.request
                 * @field {String}
                 */
                this.url = "";

                this._complete = false;
                this.transport = _getTransport();

                /**
                 * Internal State change callback function.
                 * Dispatched ReadyState handler if needed.
                 * @name onStateChange
                 * @memberOf accedo.utils.ajax.request
                 * @private
                 */
                this.onStateChange = function() {
                    var readyState = this.transport.readyState;
                    if (readyState > 1 && !((readyState === 4) && this._complete)) {
                        this.respondToReadyState(readyState);
                    }
                };

                /**
                 * Internal ReadyState handler.
                 * Parses the response and also tries to parse the response to JSON if needed.
                 * Calls state handlers from options array if such are set.
                 * @name respondToReadyState
                 * @memberOf accedo.utils.ajax.request
                 * @function
                 * @private
                 */
                this.respondToReadyState = function(readyState) {
                    try {
                        // State == 4 means DONE
                        if(readyState === 4 && !this.aborted) {
                            this._complete = true;

                            var status = this.transport.status;
                            var success = true;
                            if( (!status || (status >= 200 && status < 300) || status === 304 ) ) {
                                //Request seems to be successfull
                                //Parse JSON is possible
                                var resp = this.transport.responseText;
                                if(accedo.utils.string.isJSON(resp)) {
                                    this.transport.responseJSON = accedo.utils.string.parseJSON(resp);
                                }

                                if(accedo.utils.object.isFunction(this.options.onSuccess)) {
                                    this.options.onSuccess(this.transport);
                                }
                            } else {
                                //Unsuccessfull request
                                success = false;
                                if(accedo.utils.object.isFunction(this.options.onFailure)) {
                                    this.options.onFailure(this.transport);
                                }
                            }

                            // onComplete is always called when request is "DONE"
                            if(accedo.utils.object.isFunction(this.options.onComplete)) {
                                this.options.onComplete(this.transport, success);
                            }
                            
                            //Prevent memory leaks in Samsung devices
                            if (this.transport.destroy){
                                this.transport.destroy();
                            }
                        }
                    } catch (e) {
                        if(accedo.utils.object.isFunction(this.options.onException)) {
                            this.options.onException(this.transport, e);
                            //this is for Samsung only, in official doc they say if not destroy() it'll cause memory issue
                            if (this.transport.destroy){
                                this.transport.destroy();
                            }
                        }
                    }
                };

                /**
                 * Resend the request.
                 * @parameter {String} [newUrl] (optional) url that will replace the previous one, if set
                 * @return {accedo.utils.ajax.request} instance of the modified (or not) request
                 * @memberOf accedo.utils.ajax.request#
                 * @public
                 */
                this.resend = function(newUrl) {
                    if (newUrl) {
                        this.url = newUrl;
                    }
                    
                    delete this.aborted;
                    this._complete = false;

                    this.send();
                    
                    return this;
                };

                /**
                 * Abort request as soon as possible.
                 * @memberOf accedo.utils.ajax.request#
                 * @public
                 * @void
                 */
                this.abort = function() {
                    /*
                     The idea is to allow user to abort the request and stop execution.
                    */
                    
                    //Notify this request has been aborted (will be checked against in the onSuccess)
                    this.aborted = true;
                    
                    try {
                        this.transport.abort();
                    } catch (e) {
                        //Abort is not supported
                        delete this.aborted;
                    }

                    if(accedo.utils.object.isFunction(this.options.onAbort)) {
                        this.options.onAbort(this.transport);
                    }
                };
                
                /**
                 * Opens the transport, initialize it correctly and send the request
                 * @memberOf accedo.utils.ajax.request#
                 * @public
                 * @void
                 */
                this.send = function() {
                    //Prepare request and send it
                    try {
                        this.transport.open(this.options.method.toUpperCase(), this.url, this.options.async);

                        //ReadyState
                        accedo.console.log("----------- ajax async: [" + this.options.async + "] --- " + (this.options.async ? "asynchronous" : "synchronous"));
                        if(this.options.async){
       						this.transport.onreadystatechange = accedo.utils.fn.bind(this.onStateChange, this);
						}else{
							this.transport.onreadystatechange = function() {};
                        }

                        //Set request headers if needed
                        var headers = this.options.requestHeaders || {};
                        
                        if (this.options.method == 'post') {
                            //for application/xml - do not add encoding
                            headers['Content-type'] = this.options.contentType;
                            if (this.options.contentType != "application/xml"){
                            	headers['Content-type'] += (this.options.encoding ? '; charset=' + this.options.encoding : '');	
                            }
                        }
                        
                        var name;
                        accedo.console.log("------ ajax-send: headers start -----------");
                        for (name in headers) {
                            if (headers[name] != null){
                            	accedo.console.log(name + " : " + headers[name]);
	                            this.transport.setRequestHeader(name, headers[name]);
                            }   
                        }
                        accedo.console.log("--------- ajax-send: headers end -----------");
                        
                        //Prepare body
                        this.body = (this.options.method === 'post' || this.options.method === 'put') ? (this.options.postBody || params) : null;
                        this.transport.send(this.body);

                        /* Force Firefox to handle ready state 4 for synchronous requests */
                        if(this.options.async || (!this.options.async && this.transport.overrideMimeType)){
       						this.onStateChange();
						}

                    } catch (e) {
                        if(accedo.utils.object.isFunction(this.options.onException)) {
                            this.options.onException(this.transport, e);
                        }
                    }
                };

                //Request URL
                this.url = url;

                // Defaults
                this.options = {
                    method: 'get',
                    async: true,
                    contentType:  'text/plain',
                    encoding:     'UTF-8',
                    useProxy: false,
                    parameters:   ''
                };

                accedo.utils.object.extend(this.options, options || {});
                this.options.method = this.options.method.toLowerCase();

                //Request parameters
                var params = accedo.utils.object.isString(this.options.parameters) ? 
                this.options.parameters : (accedo.utils.object.isFunction(this.options.parameters.toQueryString) ? this.options.parameters.toQueryString(): "" );

                if (this.options.useProxy){
                	params = params.replace(/&/g, "@|@");
                }
                //accedo.console.log('params:'+params);

                //If method is not GET or POST or DELETE or PUT, set _method argument and append it to querystring. Change method to POST
                if(['get', 'post', 'delete', 'put'].indexOf(this.options.method) === -1) {
                    params += (params ? '&' : '') + "_method=" + this.options.method;
                    this.options.method = 'post';
                }

                //Prepare URL for GET request
                // if (params && this.options.method === 'get') {
                if ( accedo.utils.object.isString(params) && params.length>0){
                    // when GET, append parameters to URL
                    if (this.options.useProxy){
	                	this.url += '@?@' + escape(params);
	                }
	                else{
	                	this.url += ((this.url.indexOf('?')>0) ? '&' : '?') + params;	
	                }
                }
                accedo.console.log("ajax: use proxy? " + this.options.useProxy);
                accedo.console.log("ajax: url: " + this.url);

                //Manage the transport and send the request
                this.send();
            })();
        }
    };
});