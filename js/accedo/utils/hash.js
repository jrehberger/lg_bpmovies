/**
 * @fileOverview Support object: Hashmap.
 * @author <a href="mailto:alex@accedobroadband.com">Alexej Kubarev</a>
 */


//= require <accedo>
//= require <accedo/utils/object>

accedo.define("accedo.utils.hash", ["accedo.utils.object"], function() {

    /**
     * Creates a new Hash and returns its instance.
     * By defining "shortHash" config parameter it is possible to use $H()
     * @name hash
     * @memberOf accedo.utils
     * @class
     * @param {accedo.utils.Hash|Object} [obj] Initial data structure
     * @return A new Hash instance
     * @namespace
     * @constructor
     * @function
     */
    var hashUtils = function(obj) {

        return {

        _data: accedo.utils.object.isHash(obj) ? obj.toObject() : accedo.utils.object.clone(obj),


        /**
         * Gets a value associated with the key in the map.
         * @name get
         * @function
         * @param {String} key Key to look for
         * @return associated value or null
         * @memberOf accedo.utils.hash#
         * @public
         * @example
         * var h = accedo.utils.hash({a:1,b:2,c:3});
         * h.get("a"); // -> 1
         * h.get("c"); // -> 3
         * h.get("d"); // -> null
         */
        get: function(key) {
            // simulating poorly supported hasOwnProperty
            if (this._data[key] !== Object.prototype[key]) {
                return this._data[key];
            }

            return null;
        },

        /**
         * Sets a value and associates it to a key in the map.
         * @name set
         * @function
         * @param {String} key Kkey to associate with
         * @param value Mixed data to associate with the key
         * @void
         * @memberOf accedo.utils.hash#
         * @public
         * @example
         * var h = accedo.utils.hash();
         * h.get("a"); // -> null
         * h.set("a", 1);
         * h.get("a"); // -> 1
         */
        set: function(key, value) {
            this._data[key] = value;
        },

        /**
         * Unsets a value associated with the key, deletes the key and returns the value.
         * @name unset
         * @function
         * @param {String} key Key to unset
         * @return value associated with the removed key
         * @memberOf accedo.utils.hash#
         * @public
         * @example
         * var h = accedo.utils.hash({a:1});
         * h.get("a"); // -> 1
         * h.unset("a");
         * h.get("a"); // -> null
         */
        unset: function(key) {
            var value = this._data[key];
            delete this._data[key];
            return value;
        },

        /**
         * Returns only the data structure without any function references.
         * @name toObject
         * @function
         * @return {Object} Internal data structure in the form of JS Object
         * @memberOf accedo.utils.hash#
         * @public
         * @example
         * var h = accedo.utils.hash({a:1});
         * h.set("b", 2);
         * h.toObject(); // -> {a:1, b:2}
         */
        toObject: function() {
            return accedo.utils.object.clone(this._data);
        },

        /**
         * Iterates hash and for each item calls iterator function, with given context.
         * To break the loop, throw {@link accedo.$break}
         * Same type of functionality as {@link accedo.utils.array#each}
         * @name each
         * @function
         * @see accedo.utils.Array.each
         * @param {Function} iter Iterator function
         * @param context A context to bind iterator to
         * @memberOf accedo.utils.hash#
         * @public
         * @void
         * @example
         * var h = accedo.utils.Hash({a: 1, b:2, c:3});
         * h.each(function(pair){
         *    var key = pair.key // -> "a", "b", "c" (for first, second or third runs)  
         *    var value = pair.value // -> 1, 2, 3 (for first, second or third runs)
         * })
         */
        each: function(iter, context) {
            try {
                var key;
                for (key in this._data) {
                    var value = this._data[key], pair = [key, value];
                    pair.key = key;
                    pair.value = value;

                    iter.call(context, pair);
                }
            } catch (e) {
                if (e !== accedo.$break) {
                    throw e;
                }
            }
        },

        /**
         * Returns an array of keys.
         * Note: Key order is depending on JS implementation
         * @name keys
         * @function
         * @return {Array} Keys of this Hash
         * @memberOf accedo.utils.hash#
         * @public
         * @example
         * var h = accedo.utils.Hash({a:1, b:2, c:3});
         * h.keys(); // -> ["a", "b", "c"]
         */
        keys: function() {
            var k = [];
            var key;
            for (key in this._data) {
                k.push(key);
            }

            return k;
        },

        /**
         * Returns an array of values.
         * Note: Key order is depending on JS implementation
         * @name values
         * @function
         * @return {Array} Values of this Hash
         * @memberOf accedo.utils.hash#
         * @public
         * @example
         * var h = accedo.utils.Hash({a:1, b:2, c:3});
         * h.values(); // -> [1, 2, 3]
         */
        values: function() {
            var v = [];
            var key;
            for (key in this._data) {
                v.push(this._data[key]);
            }

            return v;
        },
        
        /**
         * Gets object type as string.
         * @name toString
         * @memberOf accedo.utils.hash#
         * @function
         * @return Type of object
         */
        toString: function() {
            return "accedo.utils.hash";
        },

        /**
         * Returns a URL-encoded string containing the hash's contents as query parameters.
         * @name toQueryString
         * @function
         * @memberOf accedo.utils.hash#
         * @public
         * @return {String} key-value pairs represented as a querystring
         * @example
         *  accedo.utils.hash({action: 'ship',
         *      order_id: 123,
         *      fees: ['f1', 'f2']
         *  }).toQueryString(); // -> "action=ship&order_id=123&fees=f1&fees=f2"
         *
         *  accedo.utils.hash({comment: '',
         *      'key with spaces': true,
         *      related_order: undefined,
         *      contents: null,
         *      'label': 'a demo'
         *  }).toQueryString(); // -> "comment=&key%20with%20spaces=true&related_order&contents=&label=a%20demo"
         *
         *  // an empty hash is an empty query string:
         *  accedo.utils.hash().toQueryString(); // -> ""
         */
        toQueryString: function() {
            var results = [];

            /**
             * Converts a key/value pair to a query pair in the form of "key=value"
             * @private
             */
            var toQueryPair = function(key, value) {
                if (accedo.utils.object.isUndefined(value)){
                    return key;
                }
                
                return (key + '=' + encodeURIComponent( (value === null) ? "" : String(value)));
            };

            
            this.each(function(pair){
                var key = encodeURIComponent(pair.key), values = pair.value;
                
                if (values && typeof values === 'object') {
                    if (accedo.utils.object.isArray(values)) {
                        var i = 0, len = values.length, value, queryValues = [];
                        for (; i < len; i++) {
                            value = values[i];
                            queryValues.push(toQueryPair(key, value));            
                        }
                        results = results.concat(queryValues);
                    }
                } else {
                    results.push(toQueryPair(key, values));
                }

            }, this);

            return results.join("&");
        }

        };
    };

    /**
     * @scope accedo
     */

    // Extend Naitive Array if so required
    if(accedo.getConfigParameter("shortHash")) {
        /**
         * Shortcut function to create a hash.
         * @param {accedo.utils.Hash|Object} [obj] Initial data structure
         * @requires "shortHash" configuration parameter
         * @return {accedo.utils.Hash} A new Hash instance
         * @function
         * @constructor
         * @memberOf accedo
         * @see accedo.utils.Hash
         * @class
         * @public
         */
        accedo.$H = function(object) {
            return hashUtils(object);
        }
    }

    return hashUtils;
});