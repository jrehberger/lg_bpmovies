/**
 * @fileOverview Convenience methods for Strings.
 * @author <a href="mailto:alex@accedobroadband.com">Alexej Kubarev</a>
 */


//= require <accedo>
//= require <accedo/utils/object>
//= require <accedo/utils/fn>
accedo.define("accedo.utils.string", ["accedo.utils.object", "accedo.utils.fn"], function() {

    /**
     * Helper function for working with Strings.
     * @class
     * @name string
     * @memberOf accedo.utils
     */
    var strUtils = {

        /**
         * Truncates a string to a given length, with optional truncation
         * @name truncate
         * @function
         * @memberOf accedo.utils.string
         * @param str String to work with
         * @param length Number of characters to strip at (Default 30)
         * @param truncation A truncation(ending) string. (Default '...')
         * @return Truncated string
         */
        truncate: function(str, length, truncation) {
            length = length || 30;
            truncation = accedo.utils.object.isUndefined(truncation) ? '...' : truncation;

            return str.length > length ? str.slice(0, length - truncation.length) + truncation : String(str);
        },

        /**
         * Strips all leading and trailing whitespaces from a string.
         * @name strip
         * @function
         * @memberOf accedo.utils.string
         * @param str String to work with
         * @return String with striped whitespaces
         */
        strip: function(str) {
            return str.replace(/^\s+/, '').replace(/\s+$/, '');
        },

        /**
         * Strips all tags from the given string.
         * This only strips tags that are not namespace prefixed.
         * This does not remove tag content (i.e '<script>var a=2</script>' will be striped to 'var a=2').
         * Use accedo.utils.String.stripScripts() to remove script contents.
         * @name stripTags
         * @function
         * @memberOf accedo.utils.string
         * @param str String to work with
         * @return A new string without tags
         */
        stripTags: function(str) {
            return str.replace(/<\w+(\s+("[^"]*"|'[^']*'|[^>])+)?>|<\/\w+>/gi, '');
        },

        /**
         * Strips a string of things that look like an HTML script blocks.
         * @name stripScripts
         * @function
         * @memberOf accedo.utils.string
         * @param str String to work with
         * @return A new string without script tags and their content
         */
        stripScripts: function(str) {
            return str.replace(new RegExp('<script[^>]*>([\\S\\s]*?)<\/script>', 'img'), '');
        },

        /**
         * Converts HTML special characters to their entity equivalents.
         * @name escapeHTML
         * @function
         * @memberOf accedo.utils.string
         * @param str String to work with
         * @return String with escaped HTML
         */
        escapeHTML: function(str) {
            return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        },

        /**
         * Converts the entity forms of special HTML characters to their normal form.
         * @name unescapeHTML
         * @function
         * @memberOf accedo.utils.string
         * @param str String to work with.
         * @return String with unescaped HTML special characters
         */
        unescapeHTML: function(str) {
            return str.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&amp;/g, '&');
        },

        /**
         * Converts a string separated by dashes into a camelCase equivalent. For
         *  instance, `'foo-bar'` would be converted to `'fooBar'`.
         *  @name camelize
         *  @function
         *  @memberOf accedo.utils.string
         * @param str String to work with.
         * @return Camelized string
         */
        camelize: function(str) {
            return str.replace(/-+(.)?/g, function(match, chr) {
                return chr ? chr.toUpperCase() : '';
            });
        },

        /**
         * Checks if the string is empty or only contains whitespaces.
         * @name empty
         * @function
         * @memberOf accedo.utils.string
         * @param str String to work with
         * @return true if string is empty, false otherwise.
         */
        empty: function(str) {
            return /^\s*$/.test(str);
        },

        /**
         * Checks if the given string is a JSON object.
         * @name isJSON
         * @function
         * @memberOf accedo.utils.string
         * @param str String to check
         * @return true if given string is a valid JSON object, false otherwise
         */
        isJSON: function(str) {
            if (strUtils.empty(str)) {
                return false;
            }

            var test = str;
            test = test.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@');
            test = test.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']');
            test = test.replace(/(?:^|:|,)(?:\s*\[)+/g, '');

            return (/^[\],:{}\s]*$/).test(test);
        },

        /**
         * Parses JSON data and return corresponding object or throws an error if the string is invalid.
         * @name parseJSON
         * @function
         * @memberOf accedo.utils.string
         * @param str String to parse
         * @return Corresponding JSON object
         */
        parseJSON: function(str) {
            if (strUtils.isJSON(str)) {
                return (window.JSON && window.JSON.parse) ? window.JSON.parse(str) : (new Function("return " + str))();
            }

            throw new SyntaxError('Badly formed JSON string: ' + str);
        },

        /**
         * Checks if a string starts by the same characters than another specified string
         * @name startsWith
         * @function
         * @memberOf accedo.utils.string
         * @param {String} str String inside which we search for a starting substring
         * @param {String} startStr Starting substring to search for
         * @return true if str starts with startStr
         */
        startsWith: function(str, startStr) {
            return str.substr(0, startStr.length) === startStr;
        }

    };


    // Extend Naitive String if so required
    if (accedo.getConfigParameter("extendString")) {
        var mtd = accedo.utils.fn.methodize;

        accedo.utils.object.extend(String.prototype, {
            truncate:       mtd(strUtils.truncate),
            strip:          mtd(strUtils.strip),
            stripTags:      mtd(strUtils.stripTags),
            stripScripts:   mtd(strUtils.stripScripts),
            escapeHTML:     mtd(strUtils.escapeHTML),
            unescapeHTML:   mtd(strUtils.unescapeHTML),
            camelize:       mtd(strUtils.camelize),
            empty:          mtd(strUtils.empty),
            isJSON:         mtd(strUtils.isJSON),
            parseJSON:      mtd(strUtils.parseJSON),
            startsWith:        mtd(strUtils.startsWith)
        });
    }

    return strUtils;
});