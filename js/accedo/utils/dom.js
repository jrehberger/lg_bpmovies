/**
 * @fileOverview Convenience functions useful when working the dom.
 * @author <a href="mailto:alexej.kubarev@accedobroadband.com">Alexej Kubarev</a>
 */

//= require <accedo/utils/object>
//= require <accedo/utils/array>
//= require <accedo/utils/string>

accedo.define("accedo.utils.dom", ["accedo.utils.object", "accedo.utils.array", "accedo.utils.string"], function() {


    /**
     * Helper function for working with the Document Object Model (DOM).
     * @name dom
     * @static
     * @class
     * @memberOf accedo.utils
     * 
     */
    var domUtils = {
        /**
         * Gets the element by ID and returs a corresponding wrapper or null of element does not exist.
         * @name getById
         * @memberOf accedo.utils.dom
         * @param {String} id String ID of the element in the DOM
         * @return {accedo.utils.dom.element} Element wrapper
         */
        getById: function(id) {
            var _dom = document.getElementById(id);
            if(_dom && accedo.utils.object.isDOMElement(_dom)) {
                return this.element(_dom);
            }

            return null;
        },

        /**
         * Creates a node using the default namespace, and returns it.
         * @name element
         * @class
         * @param {String | DOM Element} type The node type, eg. "g" in SVG, "div" in HTML
         * @param {Object} [attributes] A map with the attributes and their values, eg
         *                             {x: 0, y: 15}
         * @param {accedo.utils.dom.element} [parent] The parent node
         * @return {accedo.utils.dom.element} the element node
         * @memberOf accedo.utils.dom
         * @namespace DOM Element wrapper namespace
         */
        element: function(type, attributes, parent) {
            var _dom;

            if(accedo.utils.object.isDOMElement(type)) {
                _dom = type;
            } else {
                _dom = document.createElement(type);
            }

            var retObj = {

                /**
                 * Returns the actual HTML Element.
                 * @name getHTMLElement
                 * @function
                 * @public
                 * @memberOf accedo.utils.dom.element#
                 */
                getHTMLElement: function() {
                    return _dom;
                },
                
                /**
                 * Gets parent element wrapped in DOM object.
                 * @name getParent
                 * @function
                 * @memberOf accedo.utils.dom.element#
                 */
                getParent: function() {
                    var parent = _dom.parentNode;
                    if(parent) {
                        return domUtils.element(parent);
                    }

                    return null;
                },

                /**
                 * Remove element from the DOM tree.
                 * @name remove
                 * @function
                 * @memberOf accedo.utils.dom.element#
                 */
                remove: function() {
                    var parent = _dom.parentNode,
                        child;
                        
                    if(parent) {
                        //Prevent memory leaks in Samsung devices using a left-hand operand
                        child = parent.removeChild(_dom);
                    }
                    
                    child = null;
                    parent = null;
                },

                /**
                 * Sets the element as depending on its parent (so it shows it, if its parent is not hidden itself)
                 * @name show
                 * @function
                 * @memberOf accedo.utils.dom.element#
                 */
                show: function() {
                    _dom.style.display  = '';
                },

                /**
                 * Hides element
                 * @name hide
                 * @function
                 * @memberOf accedo.utils.dom.element#
                 */
                hide: function() {
                    _dom.style.display = 'none';
                },
                
                /**
                 * Hides or shows the element (if its display is none, sets it to inherit, otherwise set it to none.
                 * It means you'll lose the current value by toggling twice, if display was 'inline' or 'block' for instance)
                 * @name toggle
                 * @function
                 * @memberOf accedo.utils.dom.element#
                 * @returns {boolean} false if the element is hidden after toggling, true otherwise 
                 */
                toggle: function() {
                    if (_dom.style.display == 'none') {
                        retObj.show();
                        return true;
                    }
                    retObj.hide();
                    return false;
                },

                /**
                 * Set's visibility of the element.
                 * This is different from show/hide and toggle functions that work with display property.
                 * This function updates the visibility CSS property instead.
                 * As a result, no change in layout is done.
                 * @name setVisible
                 * @param {Boolean} visible
                 * @function
                 * @memberOf accedo.utils.dom.element#
                 */
                setVisible: function(visible) {
                    _dom.style.visibility = visible ? 'visible' : 'hidden';
                },

                /**
                 * Add event listener to the DOM Object.
                 * This is a direct proxy to document.element#addEventListener
                 * @name addEventListener
                 * @function
                 * @param {String} type Event type
                 * @param {Function} listener Function to call back on event fire
                 * @param {Boolean} useCapture Use capture?
                 * @memberOf accedo.utils.dom.element#
                 */
                addEventListener: function(type, listener, useCapture) {
                    _dom.addEventListener(type, listener, useCapture);
                },

                /**
                 * Removes event listener from the DOM Object.
                 * This is a direct proxy to document.element#removeEventListener
                 * @name removeEventListener
                 * @function
                 * @param {String} type Event type
                 * @param {Function} listener Function to call back on event fire
                 * @param {Boolean} useCapture Use capture?
                 * @memberOf accedo.utils.dom.element#
                 */
                removeEventListener: function(type, handler, useCapture) {
                    _dom.removeEventListener(type, listener, useCapture);
                },

                /**
                 * This function removes a child node from a parent node.
                 * @name removeChild
                 * @function
                 * @param {DOM Element} child The child node
                 * @memberOf accedo.utils.dom.element#
                 */
                removeChild: function(child) {
                    if(!accedo.utils.object.isDOMElement(child)) {
                        child = child.getHTMLElement();
                    }

                    accedo.utils.array.each(
                        _dom.childNodes,
                        function (_child) {
                            if (_child == child) {
                                //Prevent memory leaks in Samsung devices using a left-hand operand
                                var myChild = _dom.removeChild(_child);
                                myChild = null;
                            }
                        }
                    );
                },

                /**
                 * This function appends a child node to a parent node.
                 * @name appendChild
                 * @function
                 * @param {DOM Element | accedo.utils.dom.element} child The child node
                 * @public
                 * @memberOf accedo.utils.dom.element#
                 */
                appendChild: function(child) {
                    if(accedo.utils.object.isDOMElement(child)) {
                        _dom.appendChild(child);
                    } else {
                        _dom.appendChild(child.getHTMLElement());
                    }
                },
                
                /**
                 * This function appends a new child node to a parent node, before one of its children.
                 * @name insertBefore
                 * @function
                 * @param {DOM Element | accedo.utils.dom.element} newChild The new child node
                 * @param {DOM Element | accedo.utils.dom.element} existingChild The existing child node
                 * @public
                 * @memberOf accedo.utils.dom.element#
                 */
                insertBefore: function(newChild, existingChild) {
                    var existingC = accedo.utils.object.isDOMElement(existingChild) ? existingChild : existingChild.getHTMLElement();
                    var newC = accedo.utils.object.isDOMElement(newChild) ? newChild : newChild.getHTMLElement();
                    
                    _dom.insertBefore(newC, existingC);
                },
                 /**
                 * This function appends a new child node to a parent node, before the first child.
                 * @name prependChild
                 * @function
                 * @param {DOM Element | accedo.utils.dom.element} newChild The new child node
                 * @public
                 * @memberOf accedo.utils.dom.element#
                 */
                
                prependChild: function(newChild){
                    var _dom = this.getHTMLElement();
                    var newC = accedo.utils.object.isDOMElement(newChild) ? newChild : newChild.getHTMLElement();
                    _dom.insertBefore(newC, _dom.childNodes[0]);
                },
                
                /**
                 * This function finds out whether this node is attached to a parent or not.
                 * It reflects the accedo.ui.component's method with the same name.
                 * @name isAttached
                 * @function
                 * @public
                 * @memberOf accedo.utils.dom.element#
                 * @returns {true|false}
                 */
                isAttached: function() {
                    return this.getHTMLElement().parentNode != null;
                },
                
                /**
                 * This function removes an attribute from an DOM element.
                 * @name removeAttribute
                 * @function
                 * @param {String} attribute The attribute
                 * @public
                 * @memberOf accedo.utils.dom.element#
                 */
                removeAttribute: function(attribute) {
                    if (this.hasAttribute(attribute)) {
                        _dom.removeAttribute(attribute);
                    }
                },

                /**
                 * This function removes a style definition from an DOM element.
                 * @name removeStyle
                 * @function
                 * @param {String} styleName The style name
                 * @public
                 * @memberOf accedo.utils.dom.element#
                 */
                removeStyle: function(styleName) {
                    if (this.hasStyle(styleName)) {
                        delete _dom.style[styleName];
                    }
                },

                /**
                 * This function checks if an element has a certain attribute defined or not.
                 * @name hasAttribute
                 * @function
                 * @param {String} attribute The attribute
                 * @return true/false
                 * @public
                 * @memberOf accedo.utils.dom.element#
                 */
                hasAttribute: function(attribute) {
                    return !accedo.utils.object.isUndefined(_dom[attribute]);
                },

                /**
                 * This function checks if an element has a certain style defined or not.
                 * @name hasStyle
                 * @function
                 * @param {String} styleName The style name
                 * @return true/false
                 * @public
                 * @memberOf accedo.utils.dom.element#
                 */
                hasStyle: function(styleName) {
                    return !accedo.utils.object.isUndefined(_dom.style[styleName]);
                },

                /**
                 * This function sets style attributes on a give context.
                 * @name setStyle
                 * @function
                 * @param {Object} styles A object containing CSS name/value pairs
                 * @return {Element} The context, for chaining
                 * @public
                 * @memberOf accedo.utils.dom.element#
                 */
                setStyle: function(styles) {
                    return this.setAttributes({'style': styles});
                },

                /**
                 * Gets a given style for the DOM element.
                 * @name getStyle
                 * @function
                 * @memberOf accedo.utils.dom.element#
                 * @param {String} style Style name to get.
                 * @return style value or null
                 */
                getStyle: function(style) {
                    style = style == 'float' ? 'cssFloat' : accedo.utils.string.camelize(style);

                    var value = null;
                    if (document.defaultView && document.defaultView.getComputedStyle) {
                        value = document.defaultView.getComputedStyle(_dom, null)[style];
                    } else if (_dom.currentStyle) {
                        value = _dom.currentStyle[style];
                    } else {
                        value = _dom.style[style];
                    }

                    /*if (!value || value == 'auto') {
                      var css = document.defaultView.getComputedStyle(_dom, null);
                      value = css ? css[style] : null;
                    }*/
                    if (style == 'opacity') return value ? parseFloat(value) : 1.0;
                    return value == 'auto' ? null : value;
                },

                /**
                 * Gets Cumulative offset for element.
                 * Returned data is an object with x and y properties.
                 * @name cumulativeOffset
                 * @function
                 * @memberOf accedo.utils.dom.element#
                 * @return {Object} OFsset object with x and y properties
                 */
                cumulativeOffset: function() {
                    var valueT = 0, valueL = 0;
                    var element = _dom;
                    if (element.parentNode) {
                      do {
                        valueT += element.offsetTop  || 0;
                        valueL += element.offsetLeft || 0;
                        element = element.offsetParent;
                      } while (element);

                    }
                    return {x: valueL, y: valueT};
                },

                /**
                 * This function adds one or more CSS classes to the given element.
                 * @name addClass
                 * @function
                 * @param {String | Array} className Either an array of CSS classname or a single CSS classname
                 * @public
                 * @memberOf accedo.utils.dom.element#
                 */
                addClass: function(className) {
                    if (accedo.utils.object.isArray(className)) {
                        accedo.utils.array.each( className, function(def) {
                                this.addClass(def);
                        }, this);
                    } else if (accedo.utils.object.isString(className) && !this.hasClass(className)) {
                        //Either add space or do not add space before appending className
                        _dom.className += (_dom.className ? ' ' : '') + className;
                    }
                },

                /**
                 * Checks if the element has a CSS Class name assigned.
                 * @name hasClass
                 * @function
                 * @memberOf accedo.utils.dom.element#
                 * @param {String} className CSS Class name to look for.
                 * @return {Boolean} true if element has class name, false otherwise
                 */
                hasClass: function(className) {
                    var elemClass = _dom.className;
                    return (elemClass.length > 0 && (elemClass == className || new RegExp("(^|\\s)" + className + "(\\s|$)").test(elemClass)));
                },

                /**
                 * Removes a given CSS class name from the element.
                 * @name removeClass
                 * @function
                 * @memberOf accedo.utils.dom.element#
                 * @param {String} className CSS Class name to remove from the element.
                 */
                removeClass: function(className) {
                    var cname = _dom.className.replace(new RegExp("(^|\\s+)" + className + "(\\s+|$)"), ' ');
                    cname = accedo.utils.string.strip(cname);
                    _dom.className = cname;
                },
                
                /**
                 * Replaces the current CSS class(es) with given the given className
                 * @name setClass
                 * @function
                 * @param {String} className CSS Classname
                 * @public
                 * @memberOf accedo.utils.dom.element#
                 */
                setClass: function(className){
                    _dom.className = accedo.utils.string.strip(className);
                },
                
                /**
                 * This function sets the textual content of a DOM element.
                 * @name setText
                 * @function
                 * @param {String} content The text content to set
                 * @public
                 * @memberOf accedo.utils.dom.element#
                 */
                setText: function(content) {
                    _dom.innerHTML = content;
                },
                
                /**
                 * This function returns the textual content of a DOM element.
                 * @name getText
                 * @function
                 * @public
                 * @memberOf accedo.utils.dom.element#
                 */
                getText: function() {
                    return _dom.innerHTML;
                },

                /**
                 * Alters and/or adds attributes in a node using the default namespace
                 * @name setAttributes
                 * @function
                 * @param element [node] the node to which attribues shall be applied
                 * @param attributes [Object] map with the attributes and their values, eg
                 *                             { x: 0, y: 15 }
                 * @return [node] the element node
                 * @memberOf accedo.utils.dom.element#
                 */
                setAttributes: function(attributes) {
                    for (var attr in attributes){
                        if (attributes.hasOwnProperty(attr)){
                            if (attr == 'style'){
                                for (var style in attributes[attr]){
                                    if (attributes[attr].hasOwnProperty(style)){
                                        _dom.style[style] = attributes[attr][style];
                                    }
                                }
                            } else if (attr == 'textContent'){
                                _dom.textContent = attributes[attr];
                            } else {
                                _dom.setAttribute(attr, attributes[attr]);
                            }
                        }
                    }
                    return this;
                }
            };

            //Parse and set attributes as requested
            if (accedo.utils.object.isPlainObject(attributes)) {
                retObj.setAttributes(attributes);
            }

            //Append child to parent if possible
            if (parent) {
                parent.appendChild(_dom);
            }

            return retObj;
        }

    };

    return domUtils;

});
