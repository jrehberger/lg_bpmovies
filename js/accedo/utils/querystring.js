/**
 * @fileOverview Processing QueryStrings.
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
 */

accedo.define("accedo.utils.querystring",
    ["accedo.utils.array"],
    function() {

        var arrayContains = function(array, element){
            for (var i = 0; i < array.length; i++)
            {
                if (array[i] == element)
                {
                    return true;
                }
            }
            return false;
        };


        var querystring = {

            excludes : null,
            keyValuePairs : null,
        
            init : function(){
                this.getAllParameters();  
            },
        
            getAllParameters : function() {
                var url = null;
                var index = window.location.href.indexOf("?");
            
                if (index !== -1) {
                    // was questionmark last character in string?
                    if (index !== (window.location.href.length-1)) {
                        url = window.location.href.substring(index+1);    
                    }            
                }
            
                // check for no param case        
                if (url === null) {
                    this.keyValuePairs = [];
                } else {        
                    this.keyValuePairs = url.split("&");    	
                }
            },
        
            getValue : function(_key) {
                var key, value;      
                      
                for (var param=0; param < this.keyValuePairs.length; param++) {	
                    key = this.keyValuePairs[param].split("=")[0];
                    if (key == _key) {
                        value = this.keyValuePairs[param].split("=")[1];	    
                        this.lastIndex = param;
                        return value;
                    }
                }
        
                // no value found for specified key
                this.lastIndex = null;
                return null;
            },
            
            parameters : function(a) {
                
                /* a = array of either:
                 *	- a string (without =-sign) indicating that this key should not be included in the parameters list
                 * or   - a key/value pair (a=b) indicating that this pair should appear in the parameters results 
                 */
                
                var params;
                
                // populate params with values if they exist
                if (this.keyValuePairs === null) {
                    params = [];
                } else {
                    params = this.keyValuePairs.slice(0);
                }
                
                var excludes = [];
                var includes = [];
                
                if (a != null) {
                    // iterate array and find relevant entries
                    for (var j=0; j < a.length; j++) {
                        if (a[j].indexOf("=") == -1) {
                            excludes.push(a[j]);
                        } else {
                            // overwrite existing parameter (if it exists)
                            if (this.getValue(a[j].split("=")[0])) {
				params[this.lastIndex] = a[j];
                            } else {
				params.push(a[j]);
                            }
                        }
                    }
                } 	
		
                var additionalParams = "";
                for (var i=0; i < params.length; i++) {	
                    var key = params[i].split("=")[0];
                    var value = params[i].split("=")[1];
                    
                    //if (!excludes.contains(key)) {
                    if(!arrayContains(excludes,key)){
                        // this is not a reserved parameter so include it in list
                        additionalParams += key+"="+value+"&";
                    }
                }
                
                var lengthOfParams = additionalParams.length;
                if (lengthOfParams > 0) {
                    additionalParams = additionalParams.substring(0, additionalParams.length-1);    
                }
                
                return additionalParams;
            }

        };

        querystring.init();

        return querystring;

    });