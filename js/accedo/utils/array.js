/**
 * @fileOverview Convenience methods for Arrays.
 * @author <a href="mailto:alex@accedobroadband.com">Alexej Kubarev</a>
 */


//= require <accedo>
//= require <accedo/utils/object>
//= require <accedo/utils/fn>

accedo.define("accedo.utils.array", ["accedo.utils.object", "accedo.utils.fn"], function() {

    /**
     * Helper function for working with Arrays.
     * If configuration parameter "extendArray" is set, functions will extend naitive Array.
     * @name array
     * @memberOf accedo.utils
     * @see Array
     * @class
     */
    var arrUtils = {

        /**
         * Clears the array (makes it empty) and returns the array reference.
         * @name clear
         * @param {Array} arr Array to clear
         * @return Array reference
         * @static
         * @public
         * @function
         * @memberOf accedo.utils.array
         */
        clear: function(arr) {
            arr.length = 0;
            return arr;
        },

        /**
         * Returns a duplicate of the array, leaving the original array intact.
         * Performs deep copy if so required (slower).
         * @name clone
         * @memberOf accedo.utils.array
         * @function
         * @param {Array} arr Array to duplicate
         * @param {Boolean} deep Perform deep copy
         * @return {Array} a new copy of the array
         * @public
         * @static
         */
        clone: function(arr, deep) {
            if(deep === true) {
                var copy = [];
                accedo.utils.object.extend(copy, arr, true);
                return copy;
            }
            return Array.prototype.slice.call(arr, 0);
        },

        /**
         * Iterates array and for each item calls iterator function, with given context.
         * To break the loop, throw {accedo.$break}
         * @name each
         * @memberOf accedo.utils.array
         * @function
         * @param {Array} arr Array to iterate
         * @param {Function} iter Iterator function
         * @param {Object} context A context to bind iterator to
         * @public
         * @static
         */
        each: function(arr, iter, context) {
            var i = 0, len = arr.length >>> 0;

            for (; i < len; i++) {
                if (i in arr) {
                    try {
                        iter.call(context, arr[i], i, arr);
                    } catch (e) {
                        if (e != accedo.$break) {
                            throw e;
                        } else {
                            return;
                        }
                    }
                }
            }
        },


        /**
         * Tests whether all elements in the array pass the test implemented by the provided iterator functionfunction.
         * For compatibility with JS 1.6 this also passes while array to iterator as 3rd argument
         * @name every
         * @memberOf accedo.utils.array
         * @param {Array} array to loop through
         * @param {Function} iterator function
         * @param {Object} context to bind iterator to
         * @return {Boolean} true if every element passes the test, false otherwise
         * @public
         * @static
         */
        every: function(arr, iter, context) {
            iter = iter || function(x) { return x };
            var result = true;
            arrUtils.each(arr, function(value, index) {
                result = result && !! iter.call(context, value, index, arr);
                //Stop execution as we foudn first "false"
                if (!result) {
                    throw accedo.$break;
                }
            });

            return result;
        },

        /**
         * Creates a new array with all elements that pass the test implemented by the provided iterator function.
         * For compatibility with JS 1.6 this also passes while array to iterator as 3rd argument
         * @name filter
         * @memberOf accedo.utils.array
         * @function
         * @param {Array} arr Array to search in
         * @param {Function} iter Iterator function to use for testing (returns true for pass, false for fail)
         * @param {Object} context Scope of the iterator
         * @return {Array} Elements that passed the test
         * @public
         * @static
         */
        filter: function(arr, iter, context) {
            var results = [];
            arrUtils.each(arr, function(value, index) {
                if (iter.call(context, value, index, arr)) {
                    results.push(value);
                }
            });

            return results;
        },

        /**
         * Returns the first element of the array.
         * @name first
         * @memberOf accedo.utils.array
         * @function
         * @param {Array} arr Array to fetch element of
         * @return first element of the array
         * @public
         * @static
         */
        first: function(arr) {
            return arr[0];
        },

        /**
         * Returns the first index at which a given element can be found in the array, or -1 if it is not present.
         * IndexOf implementation for JS < 1.6.
         * @name indexOf
         * @memberOf accedo.utils.array
         * @function
         * @param {Array} arr Array to search in
         * @param item Item to search for
         * @param {int} i Index to search from (optional)
         * @return {int} first index at which element is found int he array or -1 if not present
         * @public
         * @static
         */
        indexOf: function(arr, item, i) {
            i || (i = 0);
            var len = arr.length;
            if (i < 0) {
                i = len + i;
            }

            for (; i < len; i++) {
                if (arr[i] === item) {
                    return i;
                }
            }
            return -1;
        },

        /**
         * Returns the last element of the array.
         * @name last
         * @function
         * @memberOf accedo.utils.array
         * @param {Array} arr Array to fetch element of
         * @return last element of the array
         * @public
         * @static
         */
        last: function(arr) {
            return arr[arr.length - 1];
        },

        /**
         * Returns the position of the last occurrence of 'item' within the array or -1 if 'item' doesn't exist in the array.
         * @name lastIndexOf
         * @function
         * @memberOf accedo.utils.array
         * @param {Array} arr Array to search in
         * @param item Item to search for
         * @param {int} i Index to start searching from (optional)
         * @return {int} last index of the item in the array or -1 if item does not exist
         * @public
         * @static
         */
        lastIndexOf: function(arr, item, i) {
            i = isNaN(i) ? arr.length : (i < 0 ? arr.length + i : i) + 1;
            var n = arrUtils.indexOf( arr.slice(0, i).reverse(), item);
            return (n < 0) ? n : i - n - 1;
        },

        /**
         * Creates a new array with the results of calling a provided iterator function on every element in this array.
         * @name map
         * @function
         * @memberOf accedo.utils.array
         * @param {Array} arr Array to work on
         * @param {Function} iter Iterator function to call on every object
         * @param {Object} context Scope to bind everything to
         * @return {Array} New array with modified values
         * @public
         * @static
         */
        map: function(arr, iter, context) {
            iter = iter || function(x){return x;};
            var results = [];

            arrUtils.each(arr, function(value, index) {
                results.push(iter.call(context, value, index));
            });

            return results;
        },

        /**
         * Returns the length of array. This is the same as calling arr.length
         * @name size
         * @function
         * @memberOf accedo.utils.array
         * @param {Array} arr Array to get size of
         * @return {int} length of the array
         * @public
         * @static
         */
        size: function(arr) {
            return arr.length;
        },

        /**
         * Tests whether some element in the array passes the test implemented by the provided iterator function.
         * @name some
         * @function
         * @memberOf accedo.utils.array
         * @param {Array} arr Array to work on
         * @param {Function} iter Iterator function to test with
         * @param {Object} context Scope to bind everything to
         * @return {Boolean} true if at least one object passes iterator test, false otherwise
         * @public
         * @static
         */
        some: function(arr, iter, context) {
            iter = iter || function(x){return x;};
            var result = false;
            arrUtils.each(arr, function(value, index) {
                if (result = !!iter.call(context, value, index)) {
                    throw accedo.$break;
                }
            });
            return result;
        },
        
        
        /**
         * Array Remove - By John Resig (MIT Licensed)
         * http://ejohn.org/blog/javascript-array-remove/
         * Removes all items of an Array from an index to another
         * /!\ Your array will be modified, not a copy of it
         * @name remove
         * @function
         * @memberOf accedo.utils.array
         * @param {Array} arr Array to work on
         * @param {Integer} from Index from which we remove elements
         * @param {Integer} to (optional) Index up to which we remove elements
         * @returns {Integer} the resulting Array's length
         * @public
         * @static
         * @examples 
         * Remove the second item from the array
         *  array.remove(1);
         * Remove the second-to-last item from the array
         *  array.remove(-2);
         * Remove the second and third items from the array
         *  array.remove(1,2);
         * Remove the last and second-to-last items from the array
         *  array.remove(-2,-1);
         */
        remove : function(arr, from, to) {
          var rest = arr.slice((to || from) + 1 || arr.length);
          arr.length = from < 0 ? arr.length + from : from;
          return arr.push.apply(arr, rest);
        },

        /**
         * Array contains
         * check if the array contains certain element
         * @name contains
         * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
         * @param array {Array} An array to check with
         * @prarm element {Object} An element to look for
         * @return { Boolean } state whether the array contains the element or not
         * @public
         * @static
         *
         */
        contains : function(array, element){
            for (var i = 0; i < array.length; i++)
            {
                if (this[i] == element)
                {
                    return true;
                }
            }
            return false;
        }

    };


    /**
     * @scope Array
     */
    // Extend Naitive Array if so required
    if(accedo.getConfigParameter("extendArray")) {

        var mtd = accedo.utils.fn.methodize;

        accedo.utils.object.extend(Array.prototype, {
            /**
             * Iterates array and for each item calls iterator function, with given context.
             * This is a shortcut function for accedo.utils.Array#each (without first argument). If Array#forEach is supported by naitive implementation, maps to it instead.
             * To break the loop, throw {accedo.$break}
             * @param {Function} iter Iterator function
             * @param {Object} context A context to bind iterator to
             * @example
             * var arr = [1,2,3];
             * arr.each(function(item, index) {
             *      //First run: item = 1, index = 0
             *      //Second run: item = 2, index = 1
             *      throw accedo.$break; //No third run
             * });
             *
             * @see accedo.utils.array.each
             * @memberOf Array#
             * @requires "extendArray" configuration parameter
             * @function
             */
            each: Array.prototype.forEach ? Array.prototype.forEach : mtd(arrUtils.each),         // Alias, to JS 1.6 function Array#forEach
            /**
             * Iterates array and for each item calls iterator function, with given context.
             * Shortcut function for accedo.utils.Array#each. If Array#forEach is supported by naitive implementation, no change is made and naitive implementation is used.
             * To break the loop, throw {accedo.$break}
             * @param {Function} iter Iterator function
             * @param {Object} context A context to bind iterator to
             * @example
             * var arr = [1,2,3];
             * arr.forEach(function(item, index) {
             *      //First run: item = 1, index = 0
             *      //Second run: item = 2, index = 1
             *      throw accedo.$break; //No third run
             * });
             *
             * @see accedo.utils.array.each
             * @memberOf Array#
             * @requires "extendArray" configuration parameter, if not supported by naitive implementation
             * @function
             */
            forEach: Array.prototype.forEach ? Array.prototype.forEach : mtd(arrUtils.each), // For maximal compatibility, JS 1.6
            /**
             * Shortcut function for accedo.utils.Array#every. If Array#every is supported by naitive implementation, no change is made and naitive implementation is used.
             * @see accedo.utils.array.every
             * @memberOf Array#
             * @requires "extendArray" configuration parameter, if not supported by naitive implementation
             * @function
             */
            every: Array.prototype.every ? Array.prototype.every : mtd(arrUtils.every), // JS 1.6
            /**
             * Shortcut function for accedo.utils.Array#filter. If Array#filter is supported by naitive implementation, no change is made and naitive implementation is used.
             * @see accedo.utils.array.filter
             * @memberOf Array#
             * @requires "extendArray" configuration parameter, if not supported by naitive implementation
             * @function
             */
            filter: Array.prototype.filter ? Array.prototype.filter : mtd(arrUtils.filter), // JS 1.6
            /**
             * Shortcut function for accedo.utils.Array#indexOf. If Array#indexOf is supported by naitive implementation, no change is made and naitive implementation is used.
             * @see accedo.utils.array.indexOf
             * @memberOf Array#
             * @requires "extendArray" configuration parameter, if not supported by naitive implementation
             * @function
             */
            indexOf: Array.prototype.indexOf ? Array.prototype.indexOf : mtd(arrUtils.indexOf), // JS 1.6
            /**
             * Shortcut function for accedo.utils.Array#lastIndexOf. If Array#lastIndexOf is supported by naitive implementation, no change is made and naitive implementation is used.
             * @see accedo.utils.array.lastIndexOf
             * @memberOf Array#
             * @requires "extendArray" configuration parameter, if not supported by naitive implementation
             * @function
             */
            lastIndexOf: Array.prototype.lastIndexOf ? Array.prototype.lastIndexOf : mtd(arrUtils.lastIndexOf), // JS 1.6
            /**
             * Shortcut function for accedo.utils.Array#map. If Array#map is supported by naitive implementation, no change is made and naitive implementation is used.
             * @see accedo.utils.array.map
             * @memberOf Array#
             * @requires "extendArray" configuration parameter, if not supported by naitive implementation
             * @function
             */
            map: Array.prototype.map ? Array.prototype.map : mtd(arrUtils.map), // JS 1.6
            /**
             * Shortcut function for accedo.utils.Array#lastIndexOf. If Array#lastIndexOf is supported by naitive implementation, no change is made and naitive implementation is used.
             * @see accedo.utils.array.lastIndexOf
             * @memberOf Array#
             * @requires "extendArray" configuration parameter, if not supported by naitive implementation
             * @function
             */
            some: Array.prototype.some ? Array.prototype.some : mtd(arrUtils.some), // JS 1.6
            /**
             * Shortcut function for accedo.utils.Array#size.
             * @see accedo.utils.array.size
             * @memberOf Array#
             * @requires "extendArray" configuration parameter
             * @function
             */
            size: mtd(arrUtils.size),
            /**
             * Shortcut function for accedo.utils.Array#clear.
             * @see accedo.utils.array.clear
             * @memberOf Array#
             * @requires "extendArray" configuration parameter
             * @function
             */
            clear: mtd(arrUtils.clear),
            /**
             * Returns a duplicate of the array, leaving the original array intact.
             * Performs deep copy if so required (slower).
             * Shortcut function for {@link accedo.utils.Array#clone}.
             * @param {Boolean} deep Perform deep copy
             * @return {Array} a new copy of the array
             * @see accedo.utils.array.clone
             * @memberOf Array#
             * @requires "extendArray" configuration parameter
             * @function
             */
            clone: mtd(arrUtils.clone),
            /**
             * Shortcut function for accedo.utils.Array#first.
             * @see accedo.utils.array.first
             * @memberOf Array#
             * @requires "extendArray" configuration parameter
             * @function
             */
            first: mtd(arrUtils.first),
            /**
             * Shortcut function for accedo.utils.Array#last.
             * @see accedo.utils.array.last
             * @memberOf Array#
             * @requires "extendArray" configuration parameter
             * @function
             */
            last: mtd(arrUtils.last),
            /**
             * Alias of {@link Array.clone}
             * @see Array.clone
             * @memberOf Array#
             * @requires "extendArray" configuration parameter
             * @function
             */
            toArray: mtd(arrUtils.clone),
            /**
             * Array Remove - By John Resig (MIT Licensed)
             * http://ejohn.org/blog/javascript-array-remove/
             * Removes all items of an Array from an index to another
             * /!\ Your array will be modified, not a copy of it
             * @param {Array} arr Array to work on
             * @param {Integer} from Index from which we remove elements
             * @param {Integer} to (optional) Index up to which we remove elements
             * @returns {Integer} the resulting Array's length
             * @public
             * @static
             * @examples 
             * Remove the second item from the array
             *  array.remove(1);
             * Remove the second-to-last item from the array
             *  array.remove(-2);
             * Remove the second and third items from the array
             *  array.remove(1,2);
             * Remove the last and second-to-last items from the array
             *  array.remove(-2,-1);
             */
            remove : mtd(arrUtils.remove),

            /**
             * Array contains
             * check if the array contains certain element
             * @name contains
             * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
             * @param array {Array} An array to check with
             * @prarm element {Object} An element to look for
             * @return { Boolean } state whether the array contains the element or not
             * @public
             * @static
             *
             */
            contains : mtd(arrUtils.contains)
        });
    }
    return arrUtils;
});