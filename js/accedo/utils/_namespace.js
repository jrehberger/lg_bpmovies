/**
 * @fileOverview defines the accedo.utils namespace
 * 
 * 
 * @author Calle Gustafsson <calle.gustafsson@accedobroadband.com>
 */
/**
 * @name accedo.utils
 * @namespace 
 */