/**
 * @fileOverview Convenience methods for Functions.
 * @author <a href="mailto:alex@accedobroadband.com">Alexej Kubarev</a>
 */

//= require <accedo>
//= require <accedo/utils/object>
accedo.define("accedo.utils.fn", ["accedo.utils.object"], function() {

    /**
     * Updates an array (of arguments) with values arguments list (args).
     * This function modifies the "array".
     * @param array Array to update
     * @param args Arguments list
     * @return Updated array of arguments
     * @private
     * @ignore
     */
    var __update = function(array, args) {
        var arrayLength = array.length, length = args.length;
        while (length--) {
            array[arrayLength + length] = args[length];
        }

        return array;
    },

    /**
     * Copies array and updates it with arguments.
     * Same as __update but does not modify the original array
     * @param array Array to update (Not modified)
     * @param args Arguments list
     * @return Updated array of arguments
     * @private
     * @ignore
     */
    __merge = function(array, args) {
        array = Array.prototype.slice.call(array, 0);
        return __update(array, args);
    },
    /**
     * Utility functions.
     * These function could be bound firectly to Functions.prototype by setting "extendFunction" parameter.
     * @name fn
     * @memberOf accedo.utils
     * @class
     * @static
     */
    
    /**
    *@ignore
    */
    
    fnUtils = {

        /**
         * Bind a function (func) to a context.
         * If used directly on the function and extending function is allowed, first argument can be omited.
         * @name bind
         * @static
         * @function
         * @memberOf accedo.utils.fn 
         * @param func Function to extend (if function extending allowed, this is to be omited)
         * @param context The context to bind to
         * @return Wraped function where execution is guaranteed in the given context
         */
        
        /**
         * we ignore this as it will give warnings and incorrect namespace in the docs
         *@ignore
        */
        bind: function(func, context) {
            if (arguments.length < 2 && accedo.utils.object.isUndefined(arguments[0])) {
                return func;
            }

            var __method = func, args = Array.prototype.slice.call(arguments, 2);

            return function() {
                var a = __merge(args, arguments);
                return __method.apply(context, a);
            };
        },

        /**
         * Return a list of argument names for a function.
         * @name argumentNames
         * @static
         * @function
         * @memberOf accedo.utils.fn
         * @param func Function to return a list of arguments for
         * @return An array of strings (argument names)
         */
        /**
         * we ignore this as it will give warnings and incorrect namespace in the docs
         *@ignore
        */
        argumentNames: function(func) {
            var names = func.toString().match(/^[\s\(]*function[^(]*\(([^)]*)\)/)[1]
                .replace(/\/\/.*?[\r\n]|\/\*(?:.|[\r\n])*?\*\//g, '')
                .replace(/\s+/g, '').split(',');

            return names.length === 1 && !names[0] ? [] : names;
        },

        /**
         * Delays execution of function "func" for a time specified in seconds by "timeout".
         * 
         * @name delay
         * @static
         * @function
         * @memberOf accedo.utils.fn
         * @param func Function to delay execution of
         * @param timeout Delay time in seconds
         * @return Timer reference to cancel if needed (with clearTimeout())
         */
        /**
         * we ignore this as it will give warnings and incorrect namespace in the docs
         *@ignore
        */
        delay: function(func, timeout) {
            var __method = func, args = Array.prototype.slice.call(arguments, 2);
            timeout = timeout * 1000;

            return window.setTimeout(function() {
                return __method.apply(__method, args);
            }, timeout);
        },

        /**
         * Defers execution of the function until the processor is ready.
         * This is essencialy the same as setting timeout to 1ms.
         * @name defer
         * @static
         * @function
         * @memberOf accedo.utils.fn
         * @param func Function to defer execution of
         * @return Time reference to cancel if needed (with clearTimeout())
         */
        /**
         * we ignore this as it will give warnings and incorrect namespace in the docs
         *@ignore
        */
        defer: function(func) {
            var args = __update([func, 0.01], Array.prototype.slice.call(arguments, 1));
            var f = fnUtils.delay;
            return f.apply(func, args);
        },

        /**
         * Returns a function "wrapped" around the original function.
         * This function lets you easily build on existing functions by specifying before and after behavior, transforming the return value, or
         *  even preventing the original function from being called.
         *
         *  The wraper function is called with this signature:
         *      function wrapper(callOriginal[, args...])
         *
         * "callOriginal" is a function that can be used to call the original (wrapped) function (or not, as appropriate).
         * It is not a direct reference to the original function, there's a layer of indirection in-between that sets up the proper context for it.
         *
         * @name wrap
         * @static
         * @function
         * @memberOf accedo.utils.fn
         * @param target Function to wrap
         * @param wrapper Function to wrap with
         * @return Wrapped function with correct references
         */
        /**
         * we ignore this as it will give warnings and incorrect namespace in the docs
         *@ignore
        */
        wrap: function (target, wrapper) {
            var __method = target;

            return function() {
                var a =  __update([fnUtils.bind(__method, this)], arguments);
                return wrapper.apply(this, a);
            };
        },

        /**
         * Wraps the function inside another function that, when called, pushes 'this' to the original function as the first argument (with any further arguments following it).
         * This method transforms the original function that has an explicit first argument to a function that passes `this` (the current context) as an implicit first argument at call time.
         * It is useful when wanted to transform a function that takes an object to a method of that object or its prototype, shortening its signature by one argument.
         * @name methodize
         * @static
         * @function
         * @memberOf accedo.utils.fn
         * @param target Function to transform.
         * @return wrapped function
         */
        
        
        /**
         * we ignore this as it will give warnings and incorrect namespace in the docs
         *@ignore
         */
        methodize: function(target) {
             /**
             * @ignore
             */
            if(target._methodized) {
                return target._methodized;
            }
            /**
             * @ignore
             */
            var __method = target;
            return (
                /**
                 * @ignore
                 */
                target._methodized = function() {
                var a = __update([this], arguments);
                return __method.apply(null, a);
            });
        }
    };


    // Extend Native Function if so required
    if(accedo.getConfigParameter("extendFunction")) {
          /**
             * @ignore
             */
        var mtd = fnUtils.methodize;
        accedo.utils.object.extend(Function.prototype, {
            argumentNames:  mtd(fnUtils.argumentNames),
            bind:           mtd(fnUtils.bind),
            delay:          mtd(fnUtils.delay),
            defer:          mtd(fnUtils.defer),
            wrap:           mtd(fnUtils.wrap),
            methodize:      mtd(fnUtils.methodize)
        });
    }

    return fnUtils;

});