/**
 * @fileOverview DataSource
 * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
 * @author <a href="mailto:gregory.desfour@accedobroadband.com">Gregory Desfour</a>
 */

accedo.define(
        "accedo.data.ds",
        [
            "accedo.utils.object",
            "accedo.events.dispatcher",
            "accedo.utils.array"
        ],
function() {

    /**
     * @class Data container class that can ask for more data, should the data provider send incomplete data (e.g. paginated),
     * and append new data to what was previously loaded.
     * @constructor
     * @param {Object} [opts] Options object
     * @param {boolean} [opts.hasPagination] (false by default)
     * @extends accedo.events.dispatcher
     * @name ds
     * @memberOf accedo.data
     */
    return function(opts) {

        var totalItems,
            sourceHasPagination = false,
            loadedData, lastPageLoaded, hasMoreData, varReset;
        
        //Save the variables init phase in a function so that we can reinit easily after calling purgeData.
        //totalItems and sourceHasPagination don't have to be reset when we call purgeData so they're not in varReset
        /**
         * @ignore
         */
        varReset = function() {
            loadedData = [];    //Array of loaded data objects
            lastPageLoaded = 0; //Number of the last page loaded (when it applies)
            hasMoreData = true; //Indicates there is *maybe* more loadable data, or definitely not
        };

        varReset();
        
        
        //Total number of items when fully loaded
        totalItems = null;
        
        //Is data paginated ? Override the default value if the option opts.hasPagination was used
        if (opts) {
            sourceHasPagination = !!opts.hasPagination;
        }
        
        //Set up inheritance
        return accedo.utils.object.extend(accedo.events.dispatcher(), {

            /**
             * Append data from an Array, and notify 3rd-parties by the 'accedo:datasource:append'
             * event, which passes the 'new data' object Array as argument for convenience
             * @param {Array} newData Array of data objects to append to the previously loaded data
             * @param {Number} start the starting dsIndex of appending data
             * @memberOf accedo.data.ds#
             * @name appendData
             * @function
             * @public
             */
            appendData: function(newData, start) {
                var fromIndex, i, newDataLen;
                
                //This DS is not paginated - Notify there is no more data to be loaded
                if (!sourceHasPagination) {
                    hasMoreData = false;
                }
                
                //Ensure we have an array as argument
                if (accedo.utils.object.isArray(newData)) {
                    if (!accedo.utils.object.isUndefined(start)){
                        //start is defined, so we append/overwrite loadedData with index start
                        newDataLen = newData.length;
                        for (i=0; i<newDataLen;i++){
                            loadedData[start+i] = newData[i];
                        }
                        fromIndex = start;
                    }else{
                        //start is not defined, so we append the data to the end of the loadedData
                        fromIndex = loadedData.length;
                        loadedData = loadedData.concat(newData);
                    }
                    if ((totalItems || totalItems === 0)  && loadedData.length >= totalItems) {
                        //we have to ensure that all items inside loadedData is not undefined
                        //in order to be sure there's no more data to load
                        var loadedDataLen = loadedData.length;
                        var setHasMoreDataFalse = true;
                        for (i=0; i<loadedDataLen; i++){
                            if (!loadedData[i]){
                                setHasMoreDataFalse =false;
                                break;
                            }
                        }
                        if (setHasMoreDataFalse){
                            hasMoreData = false;
                        }
                    }
                    
                    
                    // notify 3rd-parties
                    this.dispatchEvent('accedo:datasource:append', {newData: newData, fromIndex: fromIndex});
                }
            },

            /**
             * This function simply dispatches the 'accedo:datasource:load' event,
             * only if it has not been indicated this datasource has no more data to fetch.
             * The receiver should then call appendData() to append new data, if any
             * @name load
             * @function
             * @param {Object} loadParams contains additional information to load data, it would be dispatched
                       to the eventListener loading data. Example {dsIndex: 50} - the intended behaviour
                       is to load the page with dsIndex 50
             * @memberOf accedo.data.ds#
             * @public
             */
            load: function(loadParams) {
                if (hasMoreData) {
                    if (loadParams && 'dsIndex' in loadParams){
                        this.dispatchEvent('accedo:datasource:load', {dsIndex: loadParams.dsIndex});
                    }else{
                        this.dispatchEvent('accedo:datasource:load');
                    }
                }
            },

            /**
             * Return the currently loaded data in an Array.
             * @name getData
             * @function
             * @return {Array} loadedData Array of data objects that have been loaded so far
             * @memberOf accedo.data.ds#
             * @public
             */
            getData: function(){
                return loadedData;
            },
            
            /**
             * For ease-of-use - return a particular data object (indicated by its index among the loaded data).
             * @name getDataAtIndex
             * @function
             * @param {Number} i Index of the targeted data object in the loaded data 
             * @return {Object} A particular data object or null
             * @memberOf accedo.data.ds#
             * @public
             */
            getDataAtIndex: function(i) {
                if (i >= 0 && loadedData.length > i) {
                    return loadedData[i];
                }
                return null;
            },

            /**
             * Can this datasource load more data ?
             * Before the first call to load() it is impossible to tell, in that case assume it can load more
             * (Hence hasMoreData has an initial value of true)
             * @name hasMore
             * @function
             * @return {Boolean} false if there is no need to call load() any more, true otherwise.
             * @memberOf accedo.data.ds#
             * @public
             */
            hasMore: function(){
                return hasMoreData;
            },
            
            /**
             * Inform this datasource can fetch no more data, as all data has been fetched already.
             * Increases performance (no more load() call), but be well sure there is really no more data
             * that can be fetched as otherwise, it will never be fetched.
             * No parameter - Setting true would not make sense as it's the default value
             * @name setHasNoMore
             * @function
             * @memberOf accedo.data.ds#
             * @public
             */
            setHasNoMore: function() {
                hasMoreData = false;
            },
            
            /**
             * Is this DS using pagination ? (as set by option on constructor, true otherwise)
             * @name hasPagination
             * @function
             * @return {Boolean}
             * @memberOf accedo.data.ds#
             * @public
             */
            hasPagination: function() {
                return sourceHasPagination;
            },

            /**
             * Return the number of elements currently in the datasource
             * @name getCurrentSize
             * @function
             * @return {Number} Current size
             * @memberOf accedo.data.ds#
             * @public
             */
            getCurrentSize: function(){
                return loadedData.length;
            },

            /**
             * Returns the total number of items available when fully populated
             * @name getTotalItems
             * @function
             * @return {Number} Total number of items available when fully populated if known,
             *         otherwise, returns null (which is totalItems's initial value)
             * @memberOf accedo.data.ds#
             * @public
             */
            getTotalItems: function() {
                return totalItems;
            },
            
            /**
             * Sets the total number of items available when fully populated if the argument is not undefined
             * @name setTotalItems
             * @function
             * @param {Number} n Total number of items available when fully populated 
             * @memberOf accedo.data.ds#
             * @public
             */
            setTotalItems: function(n) {
                if (typeof n !== 'undefined') {
                    if (accedo.utils.object.isString(n)){
                        totalItems = parseInt(n, 10);
                    }else{
                        totalItems = n;
                    }
                }
            },
            
            /**
             * Returns the number of the last page loaded
             * @name getLastPageLoaded
             * @function
             * @return {Number} Number of the last page loaded
             * @memberOf accedo.data.ds#
             * @public
             */
            getLastPageLoaded: function() {
                return lastPageLoaded;
            },
            
            /**
             * Sets the number of the last page loaded if the argument is not undefined
             * @name setLastPageLoaded
             * @function
             * @param {Number} Number of the last page loaded
             * @memberOf accedo.data.ds#
             * @public
             */
            setLastPageLoaded: function(n) {
                if (typeof n !== 'undefined') {
                    if (accedo.utils.object.isString(n)){
                        lastPageLoaded = parseInt(n, 10);
                    }else{
                        lastPageLoaded = n;
                    }
                }
            },
            
            /**
             * Among the data that was already loaded, returns the index of the 1st that passes the iterator test
             * function successfully (meaning that applying testFn to it returns true), or -1
             * @name search
             * @function
             * @memberOf accedo.data.ds#
             * @param {Function} iterator Iterator function to use for testing (returns true for pass, false for fail)
             * @returns {Number} index of the first data to pass the iterator test, or -1
             * @public
             * @example var result = ds2.search(function(data, index) {
             *              return data.movieId == 'a01z';
             *          });
             * @example var result = ds2.search(function(data, index) {
             *              return i == 2;
             *          });
             */
            search: function(iterator) {
                var self = this,
                    result = -1;
                accedo.utils.array.each(loadedData, function(data, i) {
                    if (iterator.call(self, data, i)) {
                        result = i;
                        throw accedo.$break;
                    }
                });
                return result;
            },
            
            /**
             * Removes all of the previously loaded data
             * @name purgeData
             * @function
             * @memberOf accedo.data.ds#
             */
            purgeData: function() {
                loadedData = [];
                
                varReset();
            }
        });
    };
});