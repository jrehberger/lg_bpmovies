/**
 * @fileOverview Base functionality for XDK
 * @author <a href="mailto:alexej.kubarev@accedobroadband.com">Alexej Kubarev</a>
 */


if (!accedo) {
    var accedo = (function() {
    
        var loadedScripts = {},
        
        /**
         * Loads dependencies, internal function for lazy-loading
         * 
         * @private
         * @function
         * @memberOf accedo
         * @ignore
         */
        require = (function() {
                
            var loadCounter = 0,
            REQ_CONTEXT_PREFIX = "req-context-",
            contextList = {},
            head = document.getElementsByTagName("head")[0],
            objectCheckTimeout = 100,
            objectCheckMaxWaitTime = 10000,

            /**
             * Gets context by dependency name
             * @private
             * @ignore
             */
            getContextsByDependency = function(name) {
                var deps, i, context, contexts = [];
                for(context in contextList) {
                    if(contextList.hasOwnProperty(context)) {
                        deps = contextList[context].dependencies;
                        i = deps.length;

                        while(i--) {
                            if(deps[i] == name) {
                                contexts.push(context);
                                break;
                            }
                        }
                    }
                }

                return contexts;
            },
            /**
             * @ignore
             */
            checkObjForContext = function(contextName) {
                
                if(!contextList[contextName]) {
                    return;
                }

                var context = contextList[contextName],
                deps = context.dependencies,
                scope = context.scope,
                i = deps.length,
                dep;

                context.checkTimes++;

                if(context.checkTimes * objectCheckTimeout > objectCheckMaxWaitTime) {
                    accedo.console.error("### Missing one or more dependency out of the list", deps);
                    throw new Error("Required dependancies are missing and cannot be loaded, wait timeout reached");
                    return;    
                }
                
                while(i--) {
                    dep = deps[i];

                    if(/\.js$/.test(dep)) {
                        continue; //Skip JS files
                    }

                    if(!accedo.isNamespaceDefined(dep, scope)) {
                        setTimeout(function() {
                            checkObjForContext(contextName);
                        }, objectCheckTimeout);
                        return;
                    }
                }

                
                //console.log("\tCalling CB for context with dependencies: ", deps);
                context.callback();

                delete contextList[contextName];
            },
            /**
             * @ignore
             */
            completeLoad = function(name) {
                loadedScripts[name] = "complete";
               
                var contexts = getContextsByDependency(name),
                j = contexts.length,
                contextName, context, deps, i, scope, fullyLoaded;


                //console.log("DEPENDENCTY: ", name, "just LOADED", contexts);

                while(j--) {
                    contextName = contexts[j];
                    context = contextList[contextName];
                    deps = context.dependencies;
                    i = deps.length;
                    scope = context.scope;
                    fullyLoaded = true;

                    while(i--) {
                        if(loadedScripts[deps[i]] != "complete") {
                            //console.log("\t Dependency FILE", deps[i], "not loaded yet");
                            fullyLoaded = false;
                            break;
                        }
                    }

                    if(fullyLoaded) {
                        checkObjForContext(contextName);
                    }
                }


            },


            /**
             * @ignore
             */
            load = function(name, url, contextID) {
                /**
                 * @ignore
                 */
                var node = document.createElement("script");
                node.type = "text/javascript";
                node.async = true;
                /**
                 * @ignore
                 */
                 node.onreadystatechange = function() {
                    if(!node.readyState || /loaded|complete/.test(node.readyState )) {
                        /**
                         * @ignore
                         */
                        node.onload = node.onreadystatechange = null;
                        
                        completeLoad(name);
                        //Prevent memory leaks in Samsung devices using a left-hand operand
                        var child = head.removeChild(node);
                        child = null;
                        node = null;
                    }
                };
                node.onload = node.onreadystatechange;
                node.src = url;
                head.appendChild(node);

            };

            return function(dependencies, callback, scope) {

                var context = {
                    callback: callback,
                    scope: scope,
                    dependencies: dependencies,
                    checkTimes: 0
                },

                contextID = REQ_CONTEXT_PREFIX + loadCounter++,
                i = 0, 
                len = dependencies.length, 
                dep, 
                scriptPath, 
                hasWork = false;
                
                contextList[contextID] = context;

                for(;i<len;i++) {
                    dep = dependencies[i];


                    if(loadedScripts[dep] == "loading" || loadedScripts[dep] == "complete") {
                        continue;
                    } else {
                        loadedScripts[dep] = "loading";
                    }
                   
                    if(/\.js$/.test(dep)) {
                        scriptPath = dep;
                    } else {
                        if(accedo.isNamespaceDefined(dep)) {
                            continue;
                        }
                        scriptPath = 'js/' + dep.replace(/\./g, '/') + '.js';
                    }

                    hasWork = true;

                    load(dep, scriptPath, contextID);
                }

                if(!hasWork) {
                    checkObjForContext(contextID);
                }
            }
        })();
      
        //beginning of the PUBLIC object returned
        return {
            /**
             * Root scope.
             * @field
             * @name rootScope
             * @memberOf accedo
             */
            rootScope : (function() {
                return this; // || (1,eval)('this')
            })(), // Compatible with ES3, ES5 . To make even ES5-strict compatible, uncomment the ending of the above line (and remove ;)

            /**
             * A Special symbol that could be "thrown" to break from iterator loops (as "return" will not help for iterator functions).
             * @field
             * @name $break
             * @memberOf accedo
             */
            $break : {},

            /**
             * Defines a namespace module.
             * @param {String} namespace - Namespace identifier
             * @param {Array} dependencies - Array of string dependencies
             * @param {Function} constructor - Constructor function
             * @param {Object} [scope] - Optional scope to define in.
             * @name define
             * @memberOf accedo
             * @function
             */
            define: function(namespace, dependencies, constructor, scope) {
                scope = scope || accedo.rootScope;

                /**
                 * @private
                 * @function
                 */
                var internalCallback = function() {
                    var i, len, entities, root;

                    //Prepare namespace
                    if (namespace) {
                        entities = namespace.split(".");
                        root = scope;
                        len = entities.length;
                        for (i = 0; i < len; i++) {
                            root[entities[i]] = root[entities[i]] || {};
                            root = root[entities[i]];
                        }
                    }

                    try {
                        if (namespace) {
                            (new Function("constructor", namespace + " = constructor();"))(constructor);
                        } else {
                            constructor();
                        }
                    } catch(ex) {
                        throw new Error("### Unable to prepare namespace " + namespace + " dependency (missing dependency?), " + ex);
                    }
                };

                if(dependencies) {
                    require(dependencies, internalCallback, scope);
                } else {
                    internalCallback();
                }
            },

            /**
             * Tests properties in loadedScripts against a RegExp, removes the ones that match the RE
             * from the loadedScripts object.
             * This is useful if you delete an object corresponding to a loaded script and, later on,
             * want them to be loaded again.
             * Note this doesn't unload the objects that were created by scripts! Do it yourself before or after
             * calling this function.
             * @param {RegExp} regularExp RE that will be tested against every lazily loaded script
             * @name undefine
             * @memberOf accedo
             * @public
             * @function
             */
            undefine: function(regularExp) {
                if (!regularExp) {
                    return;
                }

                var script;
                
                for (script in loadedScripts) {
                    if (regularExp.test(script)) {
                        delete loadedScripts[script];
                    }
                }
            },
            
            /**
             * Gets the namespace reference in the given scope.
             * @param {String} namespace - String namespace identifier
             * @param {Object} [scope] - Optional scope. If none given - root scope used.
             * @return Reference or undefined
             * @name getNamespaceReference
             * @memberOf accedo
             * @function
             */
            getNamespaceReference: function(namespace, scope) {
                scope = scope || accedo.rootScope;

                return (new Function("scope","try { return scope." + namespace + "; } catch(ex) { return undefined; }"))(scope);
            },

            /**
             * Checks if the namespace if available in the scope.
             * @param {String} namespace - String namespace identifier
             * @param {Object} [scope] - Optional scope. If none given - root scope used.
             * @return true or false
             * @name isNamespaceDefined
             * @memberOf accedo
             * @function
             */
            isNamespaceDefined: function(namespace, scope) {
                return !!accedo.getNamespaceReference(namespace, scope);
            },


            /**
             * Gets configuation parameters for library.
             * Parameters currently do not support key-value pairs, but only set/not set type of definition.
             * @param {String} param Parameter string to look for.
             * @return {Boolean} 'true' if parameter was set on accedo.js querystring, 'false' otherwise.
             * @name getConfigParameter
             * @memberOf accedo
             * @function
             */
            getConfigParameter : function(param) {
                var i, j, p, plen, kv, file, col, len;
                //If no scripts were found, read through DOM and find them.
                if (!accedo.configParameters) {
                    accedo.configParameters = {}; //Init config parameters, this will prevent this part from running again next time

                    col = document.getElementsByTagName('script');
                    len = col.length;

                    //Loop through all script references
                    for (i = 0; i < len; i++) {
                        if (file = col[i].src.match(/accedo\.js(\?.*)?$/)) {
                            if (file[1]) {
                                p = file[1].substr(1, file[1].length).split(",");
                                plen = p.length;
                                for (j = 0; j < plen; j++) {
                                    kv = p[j].split("="); // Key-Value pair, 0 = key, 1 = value
                                    accedo.configParameters[kv[0]] = true;
                                }
                            }
                            break;
                        }
                    }
                }

                return !!accedo.configParameters[param];
            },

            /**
             * Provides a simple idiom for trying out blocks of code in sequence.
             * Such a sequence of attempts usually represents a downgrading approach to obtaining a given feature.
             * @return The first succeeded functions result
             * @name Try
             * @memberOf accedo
             * @function
             */
            Try : function() {
                var returnValue, lambda, i = 0, length = arguments.length;

                for (; i < length; i++) {
                    lambda = arguments[i];
                    try {
                        returnValue = lambda();
                        break;
                    } catch (e) {
                    }
                }

                return returnValue;
            },

            /**
             * @namespace The designated namespace for application lifecycle management.
             */
            app : undefined,

            /**
             * @namespace The designated namespace for focus.
             */
            focus : {},

            /**
             * @namespace The designated namespace for utility functions.
             */
            utils : {},

            /**
             * @namespace The designated namespace for UI functions.
             */
            ui : {
                /**
                 * @namespace The designated namespace for UI layouts.
                 */
                layout : {}
            },

            /**
             * @namespace The designated namespace for event handling functions.
             */
            events : {},
            
            /**
             * Console object for logging
             * @name console
             * @memberOf accedo
             * @namespace
             */
            console: {
                //@TODO implement remote debugging
             /**
             * Send a log message
             * @name log
             * @memberOf accedo.console
             * @function
             * @param {Mixed} string 1st log message
             * @param {Mixed} [...] nth log message
             * 
             */
                log: function(){
                    var args = Array.prototype.slice.call(arguments);
                    accedo.device.manager.console.log.apply(null,args);
                },
             /**
             * Send a info message
             * @name info
             * @memberOf accedo.console
             * @function
             * @param {Mixed} string log message
             * @param {Mixed} [...] nth log message
             * 
             */
                info: function(){
                    var args = Array.prototype.slice.call(arguments);
                    accedo.device.manager.console.info.apply(null,args); 
                },
             /**
             * Send an error message
             * @name error
             * @memberOf accedo.console
             * @function
             * @param {Mixed} string log message
             * @param {Mixed} [...] nth log message
             * @public
             */
                error: function(){
                    var args = Array.prototype.slice.call(arguments);
                    accedo.device.manager.console.error.apply(null,args); 
                },
             /**
             * Send a debug message
             * @name debug
             * @memberOf accedo.console
             * @function
             * @param {Mixed} string log message
             * @param {Mixed} [...] nth log message
             * @public
             */
                debug: function(){
                    var args = Array.prototype.slice.call(arguments);
                    accedo.device.manager.console.debug.apply(null,args); 
                },
             /**
             * Send a warning message
             * @name warn
             * @memberOf accedo.console
             * @function
             * @param {Mixed} string log message
             * @param {Mixed} [...] nth log message
             * @public
             */
                warn: function(){
                    var args = Array.prototype.slice.call(arguments);
                    accedo.device.manager.console.warn.apply(null, args);
                }
            }
        }; // End of the 'accedo' public object returned
    })();
}

