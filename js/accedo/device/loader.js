/**
 * @fileOverview Device loader - loads the proper device configuration
 * @author <a href="mailto:victor.leung@acedo.tv">Victor Leung</a>
 */
/**
 * @name loader
 * @memberOf accedo.device
 * @class
 */

accedo.define(
    'accedo.device.loader',
    [navigator && (/lg/i.test(navigator.userAgent)) ? 'accedo.device.lg' : 'accedo.device.lg'],
    function(){ return {} }
);