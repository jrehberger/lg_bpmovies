/**
 * @fileOverview Device Manager
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
 */

//= require <accedo/focus/manager>
//= require <accedo/device/manager>

accedo.define("accedo.device.manager", ["accedo.focus.manager", "accedo.utils.object", "accedo.events.dispatcher"],

    /**
     * Meta class for device handling
     * @name manager
     * @memberOf accedo.device
     * @class Device manager is reponsible handling device specific issue like key handling and media playback
     * Only one device manager is avaialble during whole lifecycle.
     * @constructor
     */
    function(){

        /**
         * Current device manager.
         * @field
         * @private
         */
        var currentDevice,
        blockAllKey = false,
        /**
         * Private direction and enter keys for reset back to default mapping
         * @author <a href="mailto:ming.hsieh@accedobroadband.com">Ming Hsieh</a>
         * @private
         * @ignore
         */
        _default_left = function(){
            return accedo.focus.manager.focusDirectionChange(accedo.focus.manager.FOCUS_LEFT);
        },
        /**
         *@ignore
         */
        _default_right = function(){
            return accedo.focus.manager.focusDirectionChange(accedo.focus.manager.FOCUS_RIGHT);
        },
        /**
         *@ignore
         */
        _default_up = function(){
            return accedo.focus.manager.focusDirectionChange(accedo.focus.manager.FOCUS_UP);
        },
        /**
         *@ignore
         */
        _default_down = function(){
            return accedo.focus.manager.focusDirectionChange(accedo.focus.manager.FOCUS_DOWN);
        },
        /**
         *@ignore
         */
        _default_enter = function(){
            var currentFocus = accedo.focus.manager.getCurrentFocus();
            if(currentFocus != null && currentFocus.dispatchEvent){
                return currentFocus.dispatchEvent("click");
            }else{
                return false;
            }
        },
        /**
         *@ignore
         */
        _default_exit = function(){
            if(currentDevice && currentDevice.system && currentDevice.system.exit){
                currentDevice.system.exit();
            }
        },
        /**
         *@ignore
         */
        _default_power = function(){
            if(currentDevice && currentDevice.system && currentDevice.system.powerOff){
                currentDevice.system.powerOff();
            }
        },
        /**
         * A hash storing the registered key function & set basic navigation
         * function for up down left right enter
         * @hash
         * @private
         * @ignore
         */
        keyFunction = {
            "left":  _default_left,
            "right": _default_right,
            "up":    _default_up,
            "down":  _default_down,
            "enter": _default_enter,
            "power" : _default_power,
            "exit" : _default_exit
        },
    
        /**
         * Object associating keys to functions (user-defined by using setAppDefaultKeys)
         * @Object
         * @private
         * @memberOf accedo.device.manager
         * @ignore
         */
        appDefaultKeys = {},
    
        keyFunctionStack = [],    /* for storing the current KeyEvent */

        keyEventDispatcher = accedo.events.dispatcher();

        return {

            getKeyEventDispatcher : function(){
                return keyEventDispatcher;
            },

            /**
         * Register a device with configuration
         * @name registerDevice
         * @param {Object} JSON for the config of the device
         * @function registerDevice
         * @public
         * @memberOf accedo.device.manager#
         */
            registerDevice : function(opts){
                currentDevice = opts;
            },
                 
            /**
         * Map the key to a specific function
         * @name registerKey
         * @param key {String} the key to map with
         * @param func {Function} the function to map with
         * @function registerKey
         * @public
         * @return {Boolean} true for successful registration, false for registration failure
         * @memberOf accedo.device.manager#
         */
            registerKey : function(key, func){
                if(!accedo.utils.object.isString(key)){
                    throw Error("accedo.device.manager.registerKey error: Wrong parameter type for 'key': "+ typeof key);
                }
                if(!accedo.utils.object.isFunction(func)){
                    throw Error("accedo.device.manager.registerKey error: Wrong parameter type for 'func' " + typeof func + ', key '+key+' was not registered successfully');
                }

                keyFunction[key] = func;
                return true;

            },
        
            /**
         * Returns the function currently associated with a key, or null
         * @param key {String} the key maped to a function
         * @returns {Function}
         * @public
         */
            getKeyFunction: function(key) {
                return keyFunction[key] || null;
            },
        
            /**
         * get the key detached from a specific function
         * @name unregisterKey
         * @function unregisterKey
         * @param key {String} the key to get unregistered
         * @public
         * @memberOf accedo.device.manager#
         */    
            unregisterKey : function(key){

                var func = keyFunction[key];
                if(accedo.utils.object.isUndefined(func) || func == null){
                    accedo.console.log("unregisterKey: no function has been registered");
                    return false;
                }else{
                    // Wire direction and enter keys back to default settings
                    switch(key) {
                        case "left":
                            keyFunction[key] = _default_left;
                            break;
                        case "right":
                            keyFunction[key] = _default_right;
                            break;
                        case "up":
                            keyFunction[key] = _default_up;
                            break;
                        case "down":
                            keyFunction[key] = _default_down;
                            break;
                        case "enter":
                            keyFunction[key] = _default_enter;
                            break;
                        case "exit":
                            keyFunction[key] = _default_exit;
                            break;
                        case "power":
                            keyFunction[key] = _default_power;
                            break;
                        default:
                            keyFunction[key] = null;
                            break;
                    }
                    return true;
                }
            },

        
            /**
         *   store all the Current Key Event Handling and reset them to default
         *   @name pushAllKeyEvent
         *   @public
         *   @function pushAllKeyEvent
         *   @memberOf accedo.device.manager#
         */
            pushAllKeyEvent : function()
            {
                keyFunctionStack.push(keyFunction);
                this.resetKeyEvent();
            },
        
            /**
         *  Restore all the Current Key Event Handling to last store status 
         *  @name popAllKeyEvent
         *   @public
         *   @function popAllKeyEvent
         *   @memberOf accedo.device.manager#
         */
            popAllKeyEvent : function()
            {
                var oldKeyFunction = keyFunctionStack.pop();
                if (!accedo.utils.object.isUndefined(oldKeyFunction) && oldKeyFunction!=null )
                {
                    keyFunction = null;
                    keyFunction = oldKeyFunction;
                }else
                {
                    this.resetKeyEvent();
                }
            },
        
            /**
         *   Block or unblock all the key event
         *   @name blockAllKeyEvent
         *   @public
         *   @function blockAllKeyEvent
         *   @memberOf accedo.device.manager#
         *   @param (boolean) OnOff
         */
            blockAllKeyEvent : function (OnOff)
            {
                blockAllKey = OnOff;
            },
        
            /**
         *   Reset the KeyEvent to default status
         *   @name resetKeyEvent
         *   @public
         *   @function resetKeyEvent
         *   @memberOf accedo.device.manager#
         */
            resetKeyEvent: function ()
            {
                keyFunction = null;
                keyFunction = accedo.utils.object.extend({
                    "left":  _default_left,
                    "right": _default_right,
                    "up":    _default_up,
                    "down":  _default_down,
                    "enter": _default_enter,
                    "power": _default_power,
                    "exit": _default_exit
                }, appDefaultKeys);
            },
        
            /**
         * Sets the app's default keys to be set when resetting all the key events, additionnally to the XDK default ones
         * @name setAppDefaultKeys
         * @param {Object} keyDefinitions Object associating keys with functions
         * @public
         * @example accedo.focus.manager.setAppDefaultKeys({'blue': doThis, 'red': doThat});
         * @function
         * @memberOf accedo.device.manager#
         */
            setAppDefaultKeys: function(keyDefinitions) {
                appDefaultKeys = keyDefinitions;
            },
        
            /**
         * fire the key function
         * @name dispatchKey
         * @function dispatchKey
         * @param key {String} name of the key function to fire
         * @public
         * @memberOf accedo.device.manager#
         */   
            dispatchKey : function(key){
                accedo.console.log("dispatchKey: " + key);
                keyEventDispatcher.dispatchEvent("accedo:keyEventDispatcher:dispatchKey", key);
                
                //accedo.console.log("keyFunction: " + keyFunction[key]);
                var func = keyFunction[key];
                if(accedo.utils.object.isUndefined(func) || func == null){
                    accedo.console.log("dispatchKey: no function has been registered");
                    return false;
                }else if (blockAllKey != true || key == "power" ){ //always be able to get key handled for power
                    return func();
                }else{
                    accedo.console.log("Key Blocked");
                    return false;
                }
            },


            /**
         * get the key detached from a specific function
         * @name keyHandling
         * @function keyHandling
         * @public
         * @memberOf accedo.device.manager#
         */   
            keyHandling : function(keyCode){
                var keyOption = currentDevice.keySet[keyCode];
                if(keyOption != null){
                    if(accedo.utils.object.isFunction(keyOption)){
                        //if it is the function, run the function
                        var val = keyOption();
                        if(val != false && accedo.utils.object.isString(val) ){
                            return this.dispatchKey(val);
                        }
                    }else{
                        return this.dispatchKey(keyOption);
                    }

                }else
                {
                    accedo.console.log("Unknown KeyCode:"+keyCode);
                    return false;
                }
            },
        
            /**
         * Console component for the device
         * @object console
         * @public
         * @memberOf accedo.device.manager#
         */
            console: {
                /**
             * Send a log message
             * @param {string} message the message to send
             * @public
             */
                log: function(message){
                    if(currentDevice){
                        if(currentDevice.deviceConsole && currentDevice.deviceConsole.log){
                            var args = Array.prototype.slice.call(arguments);
                            currentDevice.deviceConsole.log.apply(null,args);
                        }
                    }
                },
                /**
             * Send a info message
             * @param {string} message the message to send
             * @public
             */
                info: function(){
                    if(currentDevice){
                        if(currentDevice.deviceConsole && currentDevice.deviceConsole.info){
                            var args = Array.prototype.slice.call(arguments);
                            currentDevice.deviceConsole.info.apply(null,args);
                        }
                    }
                },
                /**
             * Send an error message
             * @param {string} message the message to send
             * @public
             */
                error: function(message){
                    if(currentDevice){
                        if(currentDevice.deviceConsole && currentDevice.deviceConsole.error){
                            var args = Array.prototype.slice.call(arguments);
                            currentDevice.deviceConsole.error.apply(null,args);
                        }
                    }
                },
                /**
             * Send a debug message
             * @param {string} message the message to send
             * @public
             */
                debug: function(message){
                    if(currentDevice){
                        if(currentDevice.deviceConsole && currentDevice.deviceConsole.debug){
                            var args = Array.prototype.slice.call(arguments);
                            currentDevice.deviceConsole.debug.apply(null,args);
                        }
                    }
                },
                /**
             * Send a warning message
             * @param {string} message the message to send
             * @public
             */
                warn: function(message){
                    if(currentDevice){
                        if(currentDevice.deviceConsole && currentDevice.deviceConsole.warn){
                            var args = Array.prototype.slice.call(arguments);
                            currentDevice.deviceConsole.warn.apply(null,args);
                        }
                    }
                }
            },
            /**
         * Media component for the device
         * @name media
         * @class media
         * @public
         * @memberOf accedo.device.manager
         */
            media : {
                
                initialized : false,
            
                /**
             * initialize the media player
             * @name init
             * @function init
             * @param params {Object} OPTIONAL parameter needed
             * @public
             * @memberOf accedo.device.manager.media#
             */
                init : function(params){
                    accedo.console.log(":::::::::::: media.initialized = " + this.initialized);
                    if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.init){
                        var success = currentDevice.mediaPlayer.init(params);
                        this.initialized = success;
                        return success;
                    }else{
                        return false;
                    }
                },

                /**
             * de-initialize the media player. Some of the device would need to have the plugin player turned off
             * @name deinit
             * @function deinit
             * @param params {Object} OPTIONAL parameter needed
             * @public
             * @memberOf accedo.device.manager.media#
             */
                deinit : function(params){
                    if(!this.initialized){
                        return false;
                    }
                    if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.deinit){
                        var success = currentDevice.mediaPlayer.deinit(params);
                        this.initialized = !success;
                        return success;
                    }else{
                        return false;
                    }
                },

                /**
             * set the mediaURL to the plugin Player
             * @name setMediaURL
             * @function setMediaURL
             * @public
             * @param mediaUrl { String } url string to be run
             * @param opts { Object } Optional - extra prarameter needed for the URL
             * @memberOf accedo.device.manager.media#
             */
                setMediaURL : function(url, opts){
                    if(!this.initialized){
                        return false;
                    }
                    if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.setMediaURL){
                        return currentDevice.mediaPlayer.setMediaURL(url, opts);
                    }else{
                        return false;
                    }
                },

                /**
             * get the mediaURL of the plugin Player
             * @name getMediaURL
             * @function getMediaURL
             * @public
			 * @return {String} url
             * @memberOf accedo.device.manager.media#
             */
                getMediaURL : function(){
                    if(!this.initialized){
                        return false;
                    }
                    if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.getMediaURL){
                        return currentDevice.mediaPlayer.getMediaURL();
                    }else{
                        return false;
                    }
                },

                /**
             * set the size of the video window
             * @name setWindowSize
             * @function setWindowSize
             * @param obj {Object} size json of object
             * @public
             * @memberOf accedo.device.manager.media#
             */
                setWindowSize : function(obj){
                    if(!this.initialized){
                        return false;
                    }
                    if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.setWindowSize){
                        return currentDevice.mediaPlayer.setWindowSize(obj);
                    }else{
                        return false;
                    }
                },
            
                /**
             * set the size as fullscreen
             * @name setFullscreen
             * @function setMediaURL
             * @public
             * @memberOf accedo.device.manager.media#
             */            
                setFullscreen : function(xPos, yPos){
                    if(!this.initialized){
                        return false;
                    }
                    if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.setFullscreen){
                        return currentDevice.mediaPlayer.setFullscreen(xPos, yPos);
                    }else{
                        return false;
                    }
                },

                /**
             * play the media
             * @name play
             * @function play
             * @param { Object } JSON object specifying the options
             * @public
             * @memberOf accedo.device.manager.media#
             */
                play : function(params){
                    if(!this.initialized){
                        return false;
                    }
                    if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.play){
                        return currentDevice.mediaPlayer.play(params);
                    }else{
                        return false;
                    }
                },

                /**
             * pause the media
             * @name pause
             * @function pause
             * @public
             * @memberOf accedo.device.manager.media#
             */
                pause : function(params){
                    if(!this.initialized){
                        return false;
                    }
                    if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.pause){
                        return currentDevice.mediaPlayer.pause(params);
                    }else{
                        return false;
                    }
                },

                /**
             * stop the media
             * @name stop
             * @function stop
             * @public
             * @memberOf accedo.device.manager.media#
             */
                stop : function(params){
                    if(!this.initialized){
                        return false;
                    }
                    if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.stop){
                        return currentDevice.mediaPlayer.stop(params);
                    }else{
                        return false;
                    }
                },

                /**
             * skip forward or backward to a specific position
             * @name skip
             * @function skip
             * @public
             * @memberOf accedo.device.manager.media#
             */
                skip : function(sec){
                    if(!this.initialized){
                        return false;
                    }
                    if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.skip){
                        return currentDevice.mediaPlayer.skip(sec);
                    }else{
                        return false;
                    }
                },

                /**
             * skip forward or backward to a specific position
             * @name speed
             * @function skip
             * @public
             * @memberOf accedo.device.manager.media#
             */
                speed : function(times){
                    if(!this.initialized){
                        return false;
                    }
                    if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.speed){
                        return currentDevice.mediaPlayer.speed(times);
                    }else{
                        return false;
                    }
                },

                /**
             * get bitrate and available bitrates
             * @name getBitrates
             * @function getBitrates
             * @return {Object} bitrate and available bitrate
             * @memberOf accedo.device.manager.media#
             * @public
             */
                getBitrates : function(){
                    if(!this.initialized){
                        return false;
                    }
                    if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.getBitrates){
                        return currentDevice.mediaPlayer.getBitrates();
                    }else{
                        return false;
                    }
                },

                /**
             * get and return playbackSpeed
             * @name getPlaybackSpeed
             * @function getPlaybackSpeed
             * @return {Integer} speed
             * @public
             * @memberOf accedo.device.manager.media#
             */
                getPlaybackSpeed : function(){
                    if(!this.initialized){
                        return false;
                    }
                    if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.getPlaybackSpeed){
                        return currentDevice.mediaPlayer.getPlaybackSpeed();
                    }else{
                        return false;
                    }
                },
                
                
                getBufferingProgress: function(){
                	accedo.console.log("manager - getBufferingProgress");
                	if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.getBufferingProgress){
                		accedo.console.log("success");
                        return currentDevice.mediaPlayer.getBufferingProgress();
                    }else{
                        return 0;
                    }
                },

                /**
             * Checks if a given state is the current one.
             * @name isState
             * @function
             * @returns {boolean}
             * @public
             * @memberOf accedo.device.manager.media#
             */
                isState : function(myState){
                    if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.isState) {
                        return currentDevice.mediaPlayer.isState(myState);
                    } else {
                        //False by default
                        return false;
                    }
                },
            
                /**
             * Returns true when the player is busy and may crash if successively requested to take action
             * @name isBusy
             * @function
             * @returns {boolean}
             * @public
             * @memberOf accedo.device.manager.media#
             */
                isBusy : function() {
                    if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.isBusy) {
                        return currentDevice.mediaPlayer.isBusy();
                    } else {
                        //Not busy if the function is not implemented
                        return false;
                    }
                },
            
                /**
             * get total time of the media
             * @name getTotalTime
             * @function getTotalTime
             * @public
             * @memberOf accedo.device.manager.media#
             */
                getTotalTime : function(){
                    if(!this.initialized){
                        return false;
                    }
                    if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.getTotalTime){
                   		return currentDevice.mediaPlayer.getTotalTime();
                    }else{
                        return false;
                    }
                },

                /**
             * Returns the current time (0 if never played)
             * @name getCurrentTime
             * @param {boolean} seconds When true, the return value is in seconds
             * @returns {Integer|false} current time in milliseconds (or seconds according to the 'seconds' param)
             * @function
             * @public
             * @memberOf accedo.device.manager.media#
             */
                getCurrentTime : function(seconds) {
                    if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.getCurrentTime){
                		return currentDevice.mediaPlayer.getCurrentTime(seconds);
                    } else {
                        return false;
                    }
                },
                
                setPlayerTime : function(seconds) {
                    if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.setPlayerTime){
                		return currentDevice.mediaPlayer.setPlayerTime(seconds);
                    } else {
                        return false;
                    }
                },
            
                /**
             * register the "on current time" event callback function
             * @name registerCurrentTimeCallback
             * @function registerCurrentTimeCallback
             * @param { Function } callback function to be registered
             * @public
             * @memberOf accedo.device.manager.media#
             */
                registerCurrentTimeCallback : function(func){
                    if(!this.initialized){
                        return false;
                    }
                    if(currentDevice.mediaPlayer){
                        currentDevice.mediaPlayer.onSetCurTime = func;
                        return true;
                    }else{
                        return false;
                    }
                },

                /**
             * unregister the "on current time" event callback function

             * @name unregisterCurrentTimeCallback
             * @function unregisterCurrentTimeCallback
             * @public
             * @memberOf accedo.device.manager.media#
             */
                unregisterCurrentTimeCallback : function(){
                    if(!this.initialized){
                        return false;
                    }
                    if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.onSetCurTime){
                        currentDevice.mediaPlayer.onSetCurTime = function(){};
                        return true;
                    }else{
                        return false;
                    }
                },

                /**
             * register the event callback function
             * @name registerEventCallback
             * @function registerEventCallback
             * @param { Function } callback function to be registered
             * @public
             * @memberOf accedo.device.manager.media#
             */
                registerEventCallback : function(func){
                    if(!this.initialized){
                        return false;
                    }
                    if(currentDevice.mediaPlayer){
                        currentDevice.mediaPlayer.eventHandler = func;
                        return true;
                    }else{
                        return false;
                    }
                },

                /**
             * unregister the event callback function
             * @name unregisterEventCallback
             * @function unregisterCurrentTimeCallback
             * @public
             * @memberOf accedo.device.manager.media#
             */
                unregisterEventCallback : function(){
                    if(!this.initialized){
                        return false;
                    }
                    if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.eventHandler){
                        currentDevice.mediaPlayer.eventHandler = function(){};
                        return true;
                    }else{
                        return false;
                    }
                },

                /**
             * register the status change event callback function
             * @name registerStatusChangeCallback
             * @function registerStatusChangeCallback
             * @param { Function } callback function to be registered
             * @public
             * @memberOf accedo.device.manager.media#
             */
                registerStatusChangeCallback : function(func){
                    if(!this.initialized){
                        return false;
                    }
                    if(currentDevice.mediaPlayer){
                        currentDevice.mediaPlayer.statusChangeCallback = func;
                        return true;
                    }else{
                        return false;
                    }
                },

                /**
             * unregister the status change event callback function
             * @name unregisterStatusChangeCallback
             * @function unregisterStatusChangeCallback
             * @public
             * @memberOf accedo.device.manager.media#
             */
                unregisterStatusChangeCallback : function(){
                    if(!this.initialized){
                        return false;
                    }
                    if(currentDevice.mediaPlayer && currentDevice.mediaPlayer.statusChangeCallback){
                        currentDevice.mediaPlayer.statusChangeCallback = function(){};
                        return true;
                    }else{
                        return false;
                    }
                },
            },
            /**
         * @name identification
         * @class
         * @memberOf accedo.device.manager
         */
            identification : {
                /**
             * Returns current device id
             * @name getDeviceId
             * @function
             * @memberOf accedo.device.manager.identification#
             */
	            getDeviceId : function(){
	                if (currentDevice.deviceId) {
	                    return currentDevice.deviceId;
	                }
	                return null;
	            },
	            /**
             * Returns current device type
             * @name getDeviceType
             * @function
             * @memberOf accedo.device.manager.identification#
             */

                getDeviceType : function(){
                    if(currentDevice.identification && currentDevice.identification.getDeviceType) {
                        return currentDevice.identification.getDeviceType();
                    }else{
                        return false;
                    }
                },
                
                getModelName : function(){
                    if(currentDevice.identification && currentDevice.identification.getModelName) {
                        return currentDevice.identification.getModelName();
                    }else{
                        return false;
                    }
                },

                getFirmwareYear : function(){
                    if(currentDevice.identification && currentDevice.identification.getFirmwareYear){
                        return currentDevice.identification.getFirmwareYear();
                    }else{
                        return false;
                    }
                },

                /**
             * Returns current device firmware
             * @name getFirmware
             * @function
             * @memberOf accedo.device.manager.identification#
             */
                getFirmware : function(){
                    if(currentDevice.identification && currentDevice.identification.getFirmware){
                        return currentDevice.identification.getFirmware();
                    }else{
                        return false;
                    }
                },

                /**
             * Returns current device MAC
             * @name getMac
             * @function
             * @memberOf accedo.device.manager.identification#
             *
             */
                getMac : function(){
                    if(currentDevice.identification && currentDevice.identification.getMac){
                        return currentDevice.identification.getMac();
                    }else{
                        return false;
                    }
                },

                /**
             * Returns current device IP address
             * @name getIP
             * @function
             * @memberOf accedo.device.manager.identification#
             *
             */
                getIP : function(){
                    if(currentDevice.identification && currentDevice.identification.getIP){
                        return currentDevice.identification.getIP();
                    }else{
                        return false;
                    }
                },

                /**
             * Returns current device unique id
             * @name getUniqueID
             * @function
             * @memberOf accedo.device.manager.identification#
             */
                getUniqueID : function(){
                    if(currentDevice.identification && currentDevice.identification.getUniqueID){
                        return currentDevice.identification.getUniqueID();
                    }else{
                        return false;
                    }
                },
                /**
             * Checks connectivity
             * @name checkConnection
             * @function
             * @memberOf accedo.device.manager.identification#
             */
                checkConnection : function(failureCallback){
                    if(currentDevice.identification && currentDevice.identification.checkConnection){
                        return currentDevice.identification.checkConnection(failureCallback);
                    }else{
                        return false;
                    }
                },
                /**
             * getWidevineESN
             * @name getWidevineESN
             * @function
             * @memberOf accedo.device.manager.identification#
             */
                getWidevineESN: function(){
                    if(currentDevice.identification && currentDevice.identification.getWidevineESN){
                        return currentDevice.identification.getWidevineESN();
                    }else{
                        return false;
                    }
                },
                /**
             * getWidevineSDI_ID
             * @name getWidevineSDI_ID
             * @function
             * @memberOf accedo.device.manager.identification#
             */
                getWidevineSDI_ID: function(){
                	//accedo.console.log("currentDevice.identification.getWidevineSDI_ID "+typeof(currentDevice.identification.getWidevineSDI_ID));
                    if(currentDevice.identification && currentDevice.identification.getWidevineSDI_ID){
                        return currentDevice.identification.getWidevineSDI_ID();
                    }else{
                        return false;
                    }
                },
                /**
             * getLanguage
             * @name getLanguage
             * @function
             * @memberOf accedo.device.manager.identification#
             */
                getLanguage : function(){
                    if(currentDevice.identification && currentDevice.identification.getLanguage){
                        return currentDevice.identification.getLanguage();
                    }else{
                        return false;
                    }
                },
                /**
             * support_ssl
             * @name getLanguage
             * @function
             * @memberOf accedo.device.manager.identification#
             */
                supportsSSL : function(){
                    if(currentDevice.identification && currentDevice.identification.supportsSSL){
                        return currentDevice.identification.supportsSSL();
                    }else{
                        return false;
                    }
                }

            },
           
            /**
         * @name system
         * @class
         * @memberOf accedo.device.manager
         */
            system : {
                /**
             * setSystemMute
             * @name setSystemMute
             * @function 
             * @public
             * @memberOf accedo.device.manager.system#
             */ 
                setSystemMute : function(){
                    if(currentDevice.system && currentDevice.system.setSystemMute){
                        return currentDevice.system.setSystemMute();
                    }else{
                        return false;
                    }
                },
                /**
             * setSystemUnMute
             * @name setSystemUnMute
             * @function 
             * @public
             * @memberOf accedo.device.manager.system#
             */ 
                setSystemUnMute : function(){
                    if(currentDevice.system && currentDevice.system.setSystemUnMute){
                        return currentDevice.system.setSystemUnMute();
                    }else{
                        return false;
                    }
                },
                /**
             * powerOff
             * @name powerOff
             * @function 
             * @public
             * @memberOf accedo.device.manager.system#
             */ 
                powerOff : function(param){
                    if(currentDevice.system && currentDevice.system.powerOff){
                        return currentDevice.system.powerOff(param);
                    }else{
                        return false;
                    }
                },
                /**
             * exit
             * @name exit
             * @function 
             * @public
             * @memberOf accedo.device.manager.system#
             */ 
                exit : function(param){
                    if(currentDevice.system && currentDevice.system.exit){
                        return currentDevice.system.exit(param);
                    }else{
                        return false;
                    }
                },
                /**
             * storage
             * @name storage
             * @function 
             * @public
             * @memberOf accedo.device.manager.system#
             */ 
                storage : function(){
                    if(currentDevice.system && currentDevice.system.storage){
                        return currentDevice.system.storage();
                    }else{
                        return false;
                    }
                },

                passKey : function(param){
                    if(currentDevice.system && currentDevice.system.passKey){
                        return currentDevice.system.passKey(param);
                    }else{
                        return false;
                    }
                },

                showToolsMenu : function(){
                    if(currentDevice.system && currentDevice.system.showToolsMenu){
                        return currentDevice.system.showToolsMenu();
                    }else{
                        return false;
                    }
                },

                setOnShowHandler : function(func){
                    if(currentDevice.system && currentDevice.system.setOnShowHandler){
                        return currentDevice.system.setOnShowHandler(func);
                    }else{
                        return false;
                    }
                },

                getStartingParameters : function(){
                    if(currentDevice.system && currentDevice.system.getStartingParamters){
                        return currentDevice.system.getStartingParamters();
                    }else{
                        return false;
                    }
                },
                
                onmouseon: function () {
			        if(currentDevice.system && currentDevice.system._onmouseon){
			        	return currentDevice.system._onmouseon();
                    }else{
                        return false;
                    }
			    },
			    
			    onmouseoff: function () {
			        if(currentDevice.system && currentDevice.system._onmouseoff){
			        	return currentDevice.system._onmouseoff();
                    }else{
                        return false;
                    }
			    },
            },
        
            /* check if the function exist in the abstract layer.
         * if yes, fire the function with argument. if not, return false.
         * This could be useful as we do not need to extend the manager.js to cater 
         * different device's specific functions for different platforms
         * 
         * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
         * @name dispatchFunction
         * @function dispatchFunction
         * @param path { String } path to the functino in abstract layer
         * @memberOf accedo.device.manager#
         *
         */
            dispatchFunction : function(path){
            
                //accedo.console.log("accedo.device.manager.dispatchFunction(" + path +")");
                if(typeof path == "string"){
                
                    var pathArr = path.split(".");
                    var currentLayer = currentDevice;
                    var currentParent = currentLayer;
                    var valid = true;
                    for(var i = 0; i < pathArr.length; i ++){
                        if(currentLayer[pathArr[i]]){
                            currentParent = currentLayer;
                            currentLayer = currentLayer[pathArr[i]];
                        }else{
                            valid = false;
                            break;
                        }
                    }
                    if(valid){
                        if(typeof currentLayer != "function"){
                            return currentLayer;
                        }else{
                            Array.prototype.shift.call(arguments);
                            return currentLayer.apply(currentParent, arguments);
                        }
                    }else{
                        return false;
                    }
                
                }
            
                return false;
            }
        };
    });