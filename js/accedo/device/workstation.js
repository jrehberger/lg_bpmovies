/**
 * @fileOverview Device Manager file for workstation platforms
 * @author <a href="mailto:cheung.chunho@accedobroadband.com">Cheung Chun Ho</a>
 */

//= require <accedo/device/manager>
accedo.define('accedo.device.workstation', ["accedo.device.manager"], function() {
    /**
      * Workstation device manager, should never be called directly, instead use {@link accedo.device.manager}.
     * @name workstation
     * @memberOf accedo.device
     * @class
     * @constructor
     */
    /**
     * @object A config JSON specifying each aspect on the platforms
     */
    accedo.device.manager.registerDevice((function() {

        /**
         * identification of the device manager
         * @field
         * @ignore
         */
        var DEVICEID = "workstation",

        /**
         * array of key action to be looked
         * @field
         * @ignore
         */
        keys = {},
        initiateKeyHandler, mediaPlayer, deviceIdentification, appengine, deviceConsole, exitHandling, init;
        
        // set key value
        keys[48]  = '0';
        keys[49]  = '1';  // the left-side key 1
        keys[50]  = '2';
        keys[51]  = '3';
        keys[52]  = '4';
        keys[53]  = '5';
        keys[54]  = '6';
        keys[55]  = '7';
        keys[56]  = '8';
        keys[57]  = '9'; // the left-side key 9
        
        keys[38] = "up";
        keys[40] = "down";
        keys[37] = "left";
        keys[39] = "right";
        keys[13] = "enter";
        keys[190] = "search"; //The key .
        keys[220] = "option"; //The key \

        keys[81] = "play";  //the key q
        keys[87] = "pause"; //the key w
        keys[69] = "stop"; //the key e
        keys[65] = "ff"; //the key a
        keys[83] = "rw"; //the key s

        keys[80] = "back"; //the key p
        keys[107] = "exit"; //The key +
        
        /**
         * Function to be called when a device is registered
         * @function
         * @private
         * @ignore
         */
        initiateKeyHandler = function() {

            if (document.layers) {
                document.captureEvents(Event.KEYDOWN);
            }

            var _dom = document.all ? 3 : (document.getElementById ? 1 : (document.layers ? 2 : 0));
            /**
             * @ignore
             */
            document.onkeydown = function(evt) {
                var key;
                switch (_dom) {
                    case 1:
                        key = evt.keyCode;
                        break;
                    case 2:
                        key = evt.which;
                        break;
                    case 3:
                        key = window.event.keyCode;
                        break;
                }
                key && accedo.device.manager.keyHandling(key);
                evt = null;
                key = null;
            };
        //document.onkeyup = keyHandler;
        //document.onkeypress = keyHandler;

        };

        /**
         * Create and return HTML5 player object
         * @Class mediaPlayer
         * @private
         * @ignore
         */
        mediaPlayer = (function(){

            return {

                STOPPED : 0,
                PLAYING : 1,
                PAUSED : 2,
                SKIPPING : 3,
                SPEED : 4,
                BUFFERING : 5,
                CONNECTING : 6,
                ERROR : 7,
                FINISHED : 8,

                fullscreenWidth : null,
                fullscreenHeight : null,
                url : null,
                type :"video",
                state : -1,
                loop : false,

                CONTAINER_NAME : "playerContainer",
                _parentNode : null,
                _playerContainer : null,
                _playerObject : null,
                _inited:false,
                _totalTime : 0,
                _connectionTimeLimit : 60, // in sec
                _connectionTimeOut : null,
                _connected : false,
                _pausedVideo : false,
                _support : true,
                _seek : false,
                _seekTime : 0,

                init : function(param){
                    var object;
                    this.state = this.STOPPED;

                    //read in the parameter
                    if(param){
                        this._playerParam = param;
                        this._parentNode = param.parentNode.getRoot() || document.getElementsByTagName("body")[0];
                    }else{
                        this._parentNode = document.getElementsByTagName("body")[0];
                    }

                    //create div tag
                    this._playerContainer = document.createElement("div");
                    this._playerContainer.id = this.CONTAINER_NAME;

                    this._playerContainer.style.backgroundColor = "#000000";
                    this._playerContainer.style.position = "absolute";

                    //create the player object
                    try{
                        object = document.createElement("video");
                        object.setAttribute("id","playerObject");

                        this._playerObject = object;
                        this._playerContainer.appendChild(this._playerObject);
                        this.postinit();
                    } catch(e) {
                        accedo.console.error("VideoPlayer error: while creating the player object: " + e);
                        return false;
                    }
                    //this.hide();
                    return true;
                },

                deinit : function(){
                    if(!this._inited){
                        return true;
                    }
                    this._inited = false;

                    this._playerObject.removeEventListener("loadedmetadata",accedo.utils.fn.bind(this.onLoad,this),false);
                    this._playerObject.removeEventListener("timeupdate",accedo.utils.fn.bind(this.onTimeUpdate,this),false);
                    this._playerObject.removeEventListener("waiting",accedo.utils.fn.bind(this.onWaiting,this),false);
                    this._playerObject.removeEventListener("seeking",accedo.utils.fn.bind(this.onWaiting,this),false);
                    this._playerObject.removeEventListener("seeked",accedo.utils.fn.bind(this.onPlaying,this),false);
                    this._playerObject.removeEventListener("playing",accedo.utils.fn.bind(this.onPlaying,this),false);
                    this._playerObject.removeEventListener("ended",accedo.utils.fn.bind(this.onFinished,this),false);
                    this._playerObject.removeEventListener("error",accedo.utils.fn.bind(this.onError,this),false);

                    if (this._playerObject) {
                        this._parentNode.removeChild(this._playerContainer);
                    }

                    accedo.console.log("Video player deinit success");
                    return true;
                },

                postinit : function(){
                    if(this._inited){
                        return;
                    }
                    //append to the html
                    try{
                        this._parentNode.appendChild(this._playerContainer);

                        //set the event listener
                        this._playerObject.addEventListener("loadedmetadata",accedo.utils.fn.bind(this.onLoad,this),false);
                        this._playerObject.addEventListener("timeupdate",accedo.utils.fn.bind(this.onTimeUpdate,this),false);
                        this._playerObject.addEventListener("waiting",accedo.utils.fn.bind(this.onWaiting,this),false);
                        this._playerObject.addEventListener("seeking",accedo.utils.fn.bind(this.onWaiting,this),false);
                        this._playerObject.addEventListener("seeked",accedo.utils.fn.bind(this.onPlaying,this),false);
                        this._playerObject.addEventListener("playing",accedo.utils.fn.bind(this.onPlaying,this),false);
                        this._playerObject.addEventListener("ended",accedo.utils.fn.bind(this.onFinished,this),false);
                        this._playerObject.addEventListener("error",accedo.utils.fn.bind(this.onError,this),false);

                        this._inited = true;
                    }catch(e){
                        accedo.console.info("VideoPlayer error: postinit error " + e);
                    }
                },

                setMediaURL : function(mediaUrl, opts){
                    if(mediaUrl){
                        if(!this.fullscreenWidth || !this.fullscreenHeight){
                            var resolution = appengine.getResolution();
                            this.setFullscreenSize(resolution.width, resolution.height);
                        }
                        this.url = mediaUrl;
                        accedo.console.log("set video url: " + mediaUrl);

                        if(this._playerObject){
                            //this._playerObject.setAttribute("src", mediaUrl);
                            //this._playerObject.load();

                            if(!accedo.utils.object.isUndefined(opts)){
                                if(opts.hls || opts.WMDRM || opts.widevine || opts.playReady || opts.has){
                                    this._support = false;
                                }else if(opts.type === "audio"){
                                    this.type = "audio";
                                    this._playerContainer.style.width = "0px";
                                    this._playerContainer.style.height = "0px";
                                    if(this._playerObject){
                                        this._playerObject.setAttribute("width",0);
                                        this._playerObject.setAttribute("height",0);
                                    }
                                    this._support = true;
                                }else{
                                    this._support = true;
                                }
                            }else{
                                this._support = true;
                            }
                        }

                        return mediaUrl;
                    }else{
                        return false;
                    }
                },

                getMediaURL: function(){
                    if(this.url){
                        return this.url;
                    }else{
                        return false;
                    }
                },

                setWindowSize : function(obj){
                    if(obj && !accedo.utils.object.isUndefined(obj.top) && !accedo.utils.object.isUndefined(obj.left) &&
                        !accedo.utils.object.isUndefined(obj.width)  && !accedo.utils.object.isUndefined(obj.height)){
                        accedo.console.log("top: " + obj.top + " left: " + obj.left + " width: " + obj.width + " height: " + obj.height);
                        this._playerContainer.style.width = obj.width +"px";
                        this._playerContainer.style.height = obj.height +"px";
                        this._playerContainer.style.left = obj.left +"px";
                        this._playerContainer.style.top = obj.top +"px";

                        if(obj.width !== this.fullscreenWidth || obj.height !== this.fullscreenHeight){
                            this._playerObject.className = "";
                        } else {
                            this._playerObject.className = "fullscreen";
                        }
                        if(this._playerObject){
                            this._playerObject.setAttribute("left",obj.left);
                            this._playerObject.setAttribute("top",obj.top);
                            this._playerObject.setAttribute("width",obj.width);
                            this._playerObject.setAttribute("height",obj.height);
                        }

                        this._currentWindowSize = obj;
                        return true;
                    }
                    return false;
                },

                setFullscreenSize:function(width, height){
                    this.fullscreenWidth = width;
                    this.fullscreenHeight = height;
                },

                setFullscreen : function(){
                    this.setWindowSize({
                        left:0,	//JUERGEN: CHANGED FROM -55
                        top:0,	//JUERGEN: CHANGED FROM -35
                        width:1280,
                        height:720
                    });
                },

                show : function(){
                    this._playerContainer.style.visibility = "visible";
                },

                hide : function(){
                    this._playerContainer.style.visibility = "hidden";
                },

                play: function(param){
                    if(!this._support){
                        this.statusChange(this.ERROR);
                        accedo.console.info("[xdk html5] unsupport format");
                        this.eventHandler("onRenderError", "unsupport format in the html5 video player");
                        this.stopAction();
                        return;
                    }

                    this.show();

                    if(!this._connected){
                        this._connected = true;
                        this.statusChange(this.CONNECTING);
                        this.removeConnectionTimeOut();
                        this._connectionTimeOut = setTimeout(accedo.utils.fn.bind(this.onConnectionTimeout,this),this._connectionTimeLimit*1000);
                    }

                    //if stop the video and press play, it will play the previous video
                    if(!this._playerObject.getAttribute("src") && this.url ){
                        this._playerObject.setAttribute("src", this.url);
                        this._playerObject.load();
                    }

                    this._seek = false;

                    if(param && param.sec){
                        this._seek = true;
                        this._seekTime = param.sec;
                    }

                    this._pausedVideo = false;

                    this._playerObject.play();
                },

                pause: function(){
                    if(this.state === this.PAUSED || this.state === this.STOPPED){
                        return;
                    }
                    this._pausedVideo = true;
                    this.statusChange(this.PAUSED);
                    this._playerObject.pause();
                },

                stop: function(){
                    if(this.state === this.STOPPED || this.state === this.ERROR){
                        accedo.console.log("return the stop function");
                        return;
                    }

                    this._pausedVideo = false;
                    this._playerObject.pause();
                    //if load() again in huawei, the STB will crash so need set empty src
                    this._playerObject.setAttribute("src","");
                    this._playerObject.load();
                    this._connected = false;
                    this.statusChange(this.STOPPED);
                    this.stopAction();
                },

                stopAction: function(){
                    this._playerObject.pause();
                    this.removeConnectionTimeOut();
                },

                resume: function(){
                    this._playerObject.play();
                },

                skip: function(param){
                    if(this.state === this.STOPPED ){
                        return;
                    }
                    var sec = 10,targetTime;

                    if(typeof param === "number"){
                        sec = param;
                    }else{
                        if(param && param.sec){
                            sec = param.sec;
                        }
                    }
                    targetTime = this.getCurrentTime() + sec;

                    if(targetTime > this.getTotalTime() === false && (targetTime < 0)=== false){
                        this.setPlayerTime(targetTime);
                        this.statusChange(this.SKIPPING);
                    }
                },

                speed: function(){
                    return false;
                },

                removeConnectionTimeOut:function(){
                    if(this._connectionTimeOut !== null){
                        clearTimeout(this._connectionTimeOut);
                        this._connectionTimeOut = null;
                    }
                },

                setLoop : function(on){
                    this.loop = on;
                },

                getLoop:function(){
                    return this.loop;
                },

                getBitrates : function(){
                    accedo.console.log("no bitrates information in html 5 video");
                    return false;
                },

                getPlaybackSpeed: function(){
                    return this._playerObject.playbackRate;
                },

                setPlayerTime: function(sec){
                    if(this.state === this.FINISHED){
                        //restart the player
                        this.play({
                            sec:sec
                        });
                        return true;
                    }

                    if(sec < 0 || sec > this.getTotalTime(true)){
                        return false;
                    }

                    if(this._inited && this.url && sec >= 0){
                        accedo.console.log("start at " + sec + "sec");
                        try{
                            this._playerObject.pause();
                            this._playerObject.currentTime = sec;
                            this._playerObject.play();
                            this.setTotalTime();
                        }catch(ex){
                            accedo.console.log(ex);
                        }
                    }
                },

                getCurrentTime: function(seconds){
                    var type = accedo.utils.object.isUndefined(seconds)?true:seconds;
                    return type?this._playerObject.currentTime:this._playerObject.currentTime*1000;
                },

                setTotalTime: function(){
                    this._totalTime = this._playerObject.duration;
                    accedo.console.log("duration: " + this._totalTime);
                },

                getTotalTime: function(seconds){
                    var type = accedo.utils.object.isUndefined(seconds)?true:seconds;
                    return type?this._totalTime:this._totalTime*1000;
                },

                setCurTime:function(time){
                    var total_time = this.getTotalTime(true),
                    timePercent, timeHTML, timeHour, timeMinute, timeSecond, totalTimeHour, totalTimeMinute, totalTimeSecond;

                    if(time > total_time){
                        time = total_time;
                    }else if(time < 0 ){
                        time = 0;
                    }

                    //to form the timeHTML
                    timePercent = (100 * time) / total_time;
                    timeHTML = "";
                    timeHour = 0;
                    timeMinute = 0;
                    timeSecond = 0;
                    totalTimeHour = 0;
                    totalTimeMinute = 0;
                    totalTimeSecond = 0;

                    //Current time
                    if (!time) {
                        timeHTML = '0:00:00 / ';
                    }else{

                        timeHour = Math.floor(time/3600);
                        timeMinute = Math.floor((time%3600)/60);
                        timeSecond = Math.floor(time%60);
                        timeHTML = timeHour + ":";

                        if(timeMinute === 0){
                            timeHTML += "00:";
                        }else if(timeMinute <10){
                            timeHTML += "0" + timeMinute + ":";
                        }else{
                            timeHTML += timeMinute + ":";
                        }

                        if(timeSecond === 0){
                            timeHTML += "00 / ";
                        }else if(timeSecond <10){
                            timeHTML += "0" + timeSecond + " / ";
                        }else{
                            timeHTML += timeSecond + " / ";
                        }
                    }

                    //Total time
                    if (!total_time) {
                        timeHTML += '0:00:00';
                    }else{
                        totalTimeHour = Math.floor(total_time/3600);
                        totalTimeMinute = Math.floor((total_time%3600)/60);
                        totalTimeSecond = Math.floor(total_time%60);
                        timeHTML += totalTimeHour + ":";

                        if(totalTimeMinute === 0){
                            timeHTML += "00:";
                        }else if(totalTimeMinute <10){
                            timeHTML += "0" + totalTimeMinute+":";
                        }else{
                            timeHTML += totalTimeMinute+":";
                        }

                        if(totalTimeSecond === 0){
                            timeHTML += "00";
                        }else if(totalTimeSecond <10){
                            timeHTML += "0" + totalTimeSecond;
                        }else{
                            timeHTML += totalTimeSecond;
                        }
                    }

                    try{
                        this.onSetCurTime(time,timeHTML,timePercent); // call callback function
                    //accedo.console.log(timeHTML);
                    }catch (e){
                        accedo.console.info(e.message);
                    }
                },

                statusChange : function(stateToChange){
                    this.state = stateToChange;
                    switch(stateToChange){
                        case this.STOPPED: //Stopped
                            this.statusChangeCallback("stopped");
                            break;
                        case this.FINISHED: //Stopped
                            this.statusChangeCallback("finished");
                            break;
                        case this.PLAYING : //Playing
                            this.statusChangeCallback("playing");
                            break;
                        case this.PAUSED: //Paused
                            this.statusChangeCallback("paused");
                            break;
                        case this.SKIPPING: //Skip
                            this.statusChangeCallback("skip");
                            break;
                        case this.SPEED : //Speed
                            this.statusChangeCallback("speed");
                            break;
                        case this.BUFFERING: //Buffering
                            this.statusChangeCallback("buffering");
                            break;
                        case this.CONNECTING: //connecting
                            this.statusChangeCallback("connecting");
                            break;
                        case this.ERROR ://error
                            this.statusChangeCallback("error");
                            break;
                    }
                },

                getCurrentState:function(){
                    return this.state;
                },

                statusChangeCallback : function(status){
                    accedo.console.log("statusChangeCallback" + status);
                },

                eventHandler :function(evt){
                    accedo.console.log("video eventHandler: " + evt);
                },

                onSetCurTime : function(time,formattedTime,percentage){
                    accedo.console.log("onSetCurTime: " + time+ " " + formattedTime+ " " + percentage);
                },

                onLoad:function(){
                    accedo.console.log("finish connecting");
                    this.setTotalTime();
                    this.removeConnectionTimeOut();

                    this.eventHandler("onBufferingStart");
                    this.eventHandler("onBufferingProgress");
                    this.statusChange(this.BUFFERING);

                    if(this._seek && this._seekTime>0){
                        this.setPlayerTime(this._seekTime);
                    }
                },

                onTimeUpdate:function(){
                    this.setCurTime(this.getCurrentTime(true));
                },

                onWaiting:function(){
                    if(this.state === this.CONNECTING){
                        return;
                    }

                    accedo.console.log("waiting");
                    this.eventHandler("onBufferingStart");
                    this.eventHandler("onBufferingProgress");
                    this.statusChange(this.BUFFERING);
                },

                onPlaying:function(){
                    accedo.console.log("playing");
                    this.removeConnectionTimeOut();
                    if(this.state === this.BUFFERING){
                        this.eventHandler("onBufferingComplete");
                    }

                    if(!this._pausedVideo){
                        this.statusChange(this.PLAYING);
                    }
                },

                onFinished:function(){
                    accedo.console.log("finished");
                    this.stop();
                    this.statusChange(this.FINISHED);
                    this.eventHandler("onRenderingComplete");
                    if(this.getLoop()){
                        this.play();
                    }
                },

                onError:function(e){
                    // to avoid throw second error when it is alreay in error state or it is already finished
                    if(this.state === this.FINISHED || this.state === this.ERROR || this.state === this.STOPPED){
                        return;
                    }
                    var msg = "";
                    switch (e.target.error.code) {
                        case e.target.error.MEDIA_ERR_ABORTED:
                            msg = 'You aborted the video playback.';
                            break;
                        case e.target.error.MEDIA_ERR_NETWORK:
                            msg = 'A network error caused the video download to fail part-way.';
                            break;

                        case e.target.error.MEDIA_ERR_DECODE:
                            msg = 'The video playback was aborted due to a corruption problem or because the video used features your browser did not support.';
                            break;

                        case e.target.error.MEDIA_ERR_SRC_NOT_SUPPORTED:
                            msg = 'The video could not be loaded, either because the server or network failed or because the format is not supported.';
                            break;

                        default:
                            msg = 'An unknown error occurred.';
                            break;
                    }

                    this.statusChange(this.ERROR);
                    accedo.console.info("[xdk HTML5] onRenderError : "+ msg);
                    this.eventHandler("onRenderError",msg);
                    this.stopAction();
                },

                onConnectionTimeout:function(){
                    this.statusChange(this.ERROR);
                    accedo.console.info("[xdk html5] Connection error");
                    this.eventHandler("onConnectionFailed");
                    this.stopAction();
                },

                isBusy: function() {
                    switch (this.state) {
                        case this.BUFFERING :
                        case this.CONNECTING :
                            return true;
                        default :
                            return false;
                    }
                }
            }
        })();

        /**
         *@ignore
         */
        deviceIdentification = (function(){

            return {
                
                getDeviceType : function() {
                    return "Workstation";
                },

                getMac : function(){
                    return "AACCEEDD0012";
                },

                getFirmwareYear : function() {
                    return "2011"
                },
            
                getFirmware : function() {
                    return "2.5";
                },
            
                getWidevineESN : function() {
                    return "<CLIENTINFO><CLIENTID>AgAAAO0hfaUDYP4sq1gdaL9xIzV2UWNMtMsT70*a4XLUk2VHZ20MHaFc5hd!Whr5xEstedqjYIKYgJBpMXyzdWJ9PV!Xl!TFKYweL0q26uYEpuU9ctDRsQlTgsJUREpTsfc61kPbMO1OWP1XuUuc30BGoMetbWrHu6Tw5T!1WZNqqZqtIzIHgH2IPGTwXGZGFOWCHuVbEVFtYzarAI0OT*Xppjxx1YIN</CLIENTID><CLIENTVERSION>9.00.00.2778</CLIENTVERSION><SECURITYVERSION>2.4.117.25</SECURITYVERSION><APPSECURITY>2000</APPSECURITY><SUBJECTID1>4219</SUBJECTID1><SUBJECTID2></SUBJECTID2><DRMKVERSION></DRMKVERSION><DEVCERT>bWFwmm4FPHL5aZuEQ6EtDaUX+jlZbbnrqgeKx00I5KLbMRU96uw9G4ROKtn3f880IJ8A+AxXeCN2sAUBZNVcc+fXGHTi8jfIn5KB/LepYTOyaW6wNfNR/G0N1it+VlwRzO2VPciGpXDtSBfghHdZyxElaaWJu1ewOp6uCtE5vkFMhV5f811HzQciTXLLc//Of4E4bzNrxReO0480Bb4ZEyQoMAVkgubGqCIdibtyzNu1MhsO0bq7xuszg7IXLxkJHN1FajiDq4i+NU8HZIZ7RFrncEVgsMCTDGz1zFaQGXV5lKBx5meRjYoVqN4Fg3x/40wNWc7cXcldChGxzWHy9NdQFwbYoDqN/9I6CdhGPUSpXtTgRDB8RS0C4E9Nmi1c89DPW2lA6tZx+TYT4K1IWsfVYKTrmNdxe9zuEGf9B3JeC6RaQnUDlL/WsAYLCgySHWBNqlVJRND3zKUcTQMpS01YT4O9ncdDB1UEzqDYCbmdkqkzy0ew6L7a2ka0oatpC2G9xopJEXFvbWVRTgXL0T5CIkM4z+qYujUkkjzxVa40q4ELV6x0gDjH3ZtQwhCO4FV5nwkuryDAGIFHB0FFAwlXE3/gUS+YKjTXgU2WlfNfqqun6+2znjBdz2JioL67Wy99LUOPX+G59lfZQMZ/Rj3NTnvSbGm1Z4GoIgX5EN9aq5vK1Krn/p+xoIxsXi1TctDxbQhzOAJbIhcf844Sg1UtQDM1RjuXt1ikaoqmhkzekUjBXxhrkWqCvMUCohDVU6yHjhzAc5nFwt71dCQHX7/dLWbHqles6O9AfWDUNTK7g938aM6g9IOp4/ZbD8ByAzFBWWHqvoMEqwkmG+Hb2U8J1kdwiKnCP7CUQgbd7cBgGJyrE5TEPCHn4Zu+S9eW5tX345C2EQzK2Mb2NEXPFhLVVvUk1V7drpzKIXJcm1kcMibDyRyJnUOwm8m+M6mpKubkyJTA9VCxmQN2LXofF9jdL6yw8VkSu+nCOgO1S1x2/v9hdnP6OhaEO40n0nMMwxR+yDdPgMLeV2uMI0ABUW7e0pC1+Rf1zbSEOn5MY6CWzcsUO7KYfhIZ4VNF6j8K/V88tdTY/7P2orMn6E+VC346w3KdL+ZF7E5IdWL6FRDvhPTVSoRI6KNq+Ds3S72uGbtayQaCSAEUtk1sKHYNeAUlFv0K0FnhOdRvhI/WcGyQFsCGeI9Fl0TmO+/HncLW6ssSVpj//bs75V1SNwvyFTXbOi7jIbOaMgNCiRyY58FfOBd8fI1EyBjJddRrrIpl42N/eHuX5Hf2oyfF7TL/qScZPyo/Hoq86OAojZoKkClx3FDl/LIXnOwDdZv5ltzOXPgzGtTLUSMHksOrfw4X/9g26ALaT312PnyarU8S1jWKXAzstFsdN7wmXX6MosyTU0oz2hYwO0Vs9gvdbaCWmD6i+3Stle5XBgViuE5HLGVPxEHEoieqv2QS9qsR+TRr3VKHVKe2yUJuKLPxqzw2m1YeWrtOmiyhA0kAPOJj+722o01xTERBUWd3PS8ROQyJkyQ7rajD/pxn54ip0MFk8iJ94948V2akN0WRccMjHovug2PE0D9csseqRBJNfFNKlcYiUnuoUTz5owYkzo/ctq9+uy8aF2cygVijQL26Xhg6hW/T2/IUmOG0IW6HNAXn4BPwY7lndhTLB2nbZwxHvdH79PU7qafssHgQewYRAzc72ItlzA+JBpC1D8L+Njnd+5uQXCENhNzuU1LFVlnyYJORxYJnCmhmsI1KS192/AqUxI8m47jBs36pwYbMuKZIT8+R02DZnYfvsDZf9+gnGlqBoiQPgL9rVZPtAdGq3kGXuVJdAWvjb5gnzQo6cVXHwgfK5lvxUIEbKz1Pfg4crcNSTtIVeylAYyge61AHRnslQOt5h0IQlt+cXE/ECJOYEVCM4Qql0ZlM3gIAJNcM6nNthXPjlYZ8yh9FsLATvpzsW/GHD5XIy9f8Q2y6faALABVBgeqnFCcWr7RjeJVAbPgGYbnv5IBggwrfP/UXGALNwhuwoEA2UThsORGm8dFabxTMpuUwnoPS1+VMpbjxeqDUVcuYN2/bj9y9w8j4BYMMT/GGZSHBs73YngwcBGqr9DSsmXoKI3UQyGOqzMw8YOFuDrTplFXtpntE1DZIWRGCBOungkrGix5kkdfnL5+P1HLzCGen8DdMqJJV7k6z0Vm9FhhzprQOubQpEloXoGar8xKwKi5w/zZZgokNM+BJoorhYmonGNbkSSUnwQZv58r05nvxzK+s8e/EJeszvZLlXIId3hZEmMWO5exp/vMHFGyR+g75ioH9PvLGwqqG4e6Q0SXZLbimgLGkGszr5TC2uUO+eJKpR/6rTBq1s9iUexv59z4NSp7sxobgzwm4Oe1ncz2LJoDne0KvVoGDnqWCJWVfky49E6bL7hTVBWtdTe1TvPLJq5pWK4LWTZRAQyypXBZ47pGaXp+GLrWk2AHpQPUleXUgz9EM4xAe6t2mbjS8bBUdvVTNsazZqHnyNqxZIL61f4hgkLYmN+itONX9yNbcpmkDxDuUTLFqpO+uueMS+XURZ6hh7252wd7Kb8H7xTO+I4ScUecf9sIkWe7c3zDlEyvHsR8nnxEiKMCf9JD2PcTUMXEbvJj5qKswCNRCV36GQJbGq7n0nFmm2K4cjeOtR2oUt8vUPWbH3VW2EMk0wqC39w/xZoYS/yPP4xk6/1Jas7LuhOz1crpsc+rHzuvCl5u2o9LA8KPXSlVh90FJTiRSoEwADI1SuH69ziois31HwhKjHMXgik2449bCrOy96cZSGxE2AAqaSRELxYGJXfSiDJWbi08VAlgF74TnRFGmNDRY2f45zMu1aj+pQL0RWMD6mceSinE4LrafvzRSKxcgtZ+bCvmiAxDSr8wJzTB8wNcxG0pMbxCo11uK7O+4Rm7mchQNyNC4Rb1ENi/Um3H4M9pfNm4LD8i//ZPiCCfpEs1anKZXH2SZq+Xl/HdwGfOUXTGhGwrb5y5G7HC79BabOP2G+YhU/KIM4fGWrWppUq+/rscFS3AshY5DC+M4/ISVZt7YIf954UUbn9c+D45dms4Xr64XACuYUMIksF3JH31qgIH/qvgOSQ69OkhjsFrkILqjMtw2QJNiVCIHO+SNddTrmEBX5enmmdAuc82FwJsbGVBwEWrAZAqoL+CzxDiXc5glBHnYLmg9ZMwH+/pP7RjLwN7CJ1+OmThG4fqVhgkSwHBFN7jc7Jm3hwDCLMCMksmwxv6JP6ejG7BE/WqOlVvmd16GTQ+0NT9z5NEIzvZYUj5rQKgejTgq76uT4X99WEXH77Xn5VvsP99GzLoK0BndnxYLQg2kD8n6ttCD56o2O0eCJ4ZlNNL1swQ7RuDIH3zp0RLeqKr7G/Nm73VSd6l3weMsvAtK6SbqUSbZdNRMisS4R2zvWAjBXxmaitY4uHEZeRaSbLKWwqKS2fkD+QlZyNrGdoexptGZXQwF9Sf39mASkGZzJRpNvs+JZNx+pmb0fIEpgc2+QZXYwhCUirIVsqfnMiCIEClhdlqkh6pPaluBoXwyGVNxHvqNynKa2D1EZ0NrUpt2VCn3dRG6nF5/OaXWEkWrWHNRA60Qkc69wILOxoV83lh6Naqd++XRoe3usTDbFDMvMblIZJCdcabWnf9pRCgOQQIHjvN6iHuQjehO0T1Nm6g7EjeIyi/Qni3REDnlyOplIzi3leUEbwednM2UWbqIYf2U7E+efnh2y6RSVH2S7DpTmm+xVENx+dTf6xkbrdIBiJqEiD8EMK1XtOBZB4gism6OEKUmm4C7UkQmF8ptEr5l+flCFe1kZ+CXNMu+0nm7UIVqNQLoK7oc5Yn7N6Bd+r90NC37AvHsDPrVKZzwgIT1tC4dzbra0+4qfuM/nA/YSlJc7lCDFgPc+3Ce+Hja4CToCmO5WQ2ljaFS5qO4ktbO+mzke/zxuIwZ2oOMz3R1kzIzfA/gyfMiPJjhwHu8XklopSDv8GxLuW8PmEDFerYZBXK8feqo3fhT7Ub01GVE9hT1LRKl8w9rd9RM5Sg/8U1INCmEQhQS5xYFXVMeKwprwx+9y3vgvksq6z/k03EqrGBTAjCI1ExPcn5mDX4BMiLw/doFz31YnvthqnDyBkCXWwojryA7s3dxzV8aoHgJvS/Dk5ALu5Ki0HfwD1v4IUb5Y0FL5bKwstbJ+LKXDWeoKxMSIfMnel7YsuRBWyRm4v9foU+iiQHRfSth5rUd94/gMbo77NUIVVa4Ypl/wnd/48sYIsi5ECCBpMermiHxcKqsg33y+u7Sp1IDLmKbX4aNWQM5cZo7+WCLsYEYNMJxro02mrZDtyw4LjALiWeqlnFQEZIcuv0G5/rx6SeWBkQxy8Fz+ldrjsTwg56gH0huNuZoyFM/uTIYg2msEbGFwqHF2itSSVsb1yxU3yI78YwmRl8lqlY1rS4XUQM/2PsknVW2w4ncoay1NFM4e5tW53pNtOHLeMRxen46ybuA69dOBDhviT77+QEH/qZcded+jsdRY36X8sgYOAqF2ZGfzeFjDVIRIR3VaBKArERiuEXRDayB8h4psLbluG8nXXQFSMqbqTwItEk5PVZPTVC9YbO+/BSAGSU2RrBiPpG9ciImd3vaxOy4GHoLjvDwssDlsiqBodH+1hqlL2BYTxj3ZY8Hn7OYUpS42j92Htbd3Io5nVjbkF+BHBjaC292jX5uVcVl7ca5g5hI6HjPLRwS+Nyb/IXwZRBscHZi4Jofya08fra/pe44iShHFqMnICoBS3cJxvJek63db2KEdyzparLV90SnHjT4fJ8kQU+Oq27faD+NEbm9O6oagImSByjuHWwm5K1RfwDyq7lpRYuTVDpuKkKP2YPvBGs0hkbbEuMh2GuWlpuZHlPXLlHwGEHfnMcFh3cQoea1oQskV+HovhepJ5RYFGl4toKN7hlvirJAPB2i7TvXDdYh86hpiBxc3Xk+RSJLx7Ghlt7kQObiXkXdYWDo97RmzgWYepjhOF8EgX9TR1/hcY0u878bEplZUWPypu9e0Y0YaThgORxE1HqLu9oESba/p1ErkbBUfscnN4mQdMqGosQVyT32pRR3US583DKsZsQ6nvajVd8DnJKi1NJysX6uOnstxUTpbxyrQU+FSFYhrEQO3Vwzl+TVLxSTU/rrsfy9a09qCqBSkfr4mnAfCuv96kc3oQ7O4dYgvi3B9r/yBOcA9UyUEeJpW+OUF+a6HamJmN0RdX51msRkgOY0wPIrs57qcV7PbRLTdtB3kCixnoggaZqlA3cAKeaTh5RiH1hoxCJiRvl4jLNiam2m5lERmOIT1LO6hacehDHzAV07mTYbZDvhHx6pxa52tDiI8YaF4s4u52w3sXOad6NUlN4aaaVo4sBBsF4Ts4c0ynwXvLNhMwTNxRZ+hA2QkTj7DxZhAljcXrjVYxU50UdYEpN7sJAi892Iizsw6mnsKYxnDBmJvMxQADNRaBRcFMXNvLDBX/EMfG5cm1SQ5clieNcMkleDrOlJo3B+BdeKfFWkl4vNS+mqLbDGMAxtgLB7OC1yvozIXu0HSEUuQlsAzChCMLZBy55msED2dJw75N7mQpe2Pc/L+mJxsqpjav+PVY8rftp0Am5hlNwiFLJe8ehwoiSDxlzzIIt9vm0McNn8AIhPS9Hdfzyqd2AP3aIHF34raZOR9tWlbh2cPw5qc+F3hbpxZhx1LNscGRiP2xqXTHtsHCfiyrrBAZELG+Lkvqu/yXYZi2c6Gm+rkVYBEj/azw+BkqB51izhkVzLY3ye+fGRzLntWfze9gxtz2sN5NZbba9XiFLs7NDNQ+ekSSkzPtxWnCiHxGJKUvEJNcepBMJyFkHNNDygOTryB3kDEfkqKB6C3e0OW4BcHt+RDLhp9pzqbkUnNICcPkSzBlfS+/s+TncFhcopX9JWc3TPPYuzC9Yjocdpb1bs6QFCEfYHDbRLEXYE2KzXqgLb6+mGcfTprze6s7syHcoou1opErZN8IIRrc3BlFyVzNSBCj9Kz0ui85kJDpNW5IbrrmBEEtUSj1YKV8jfjzQN++/8RCUPha7yrBM1m8JJKY1zjwYC2/JwFru0El/XxztmxaPFMVufebsbA3/ZmtuHojRB+xi0JQd0O2ODp/eT3qQlHXM+sFOP93nL+UwcReVtz946A8BNmPiH8HN3yHDRycIQWxE93J1kRlp9EQDDSFGb000ZdflSlKt4EUdn4KHOsS0Zn7uj4OU2zeFfclGmYmHxujd9kSmDK8E5KWC4Wg84Bw72UXSv/M4IJTa+wVT78q6Hhh9nYa8phf5iziJ9KfiTFYM+1U2IKSOOyaP8KatTyoZCJ2syosvdYnhcks6fALu+JtdzrGs+Qmv80HYwaS5xrns7KEBbQBGRCXrO9HiSPdakHnxcwXuK5EnsK5041Kfe9Q4ZxpFQn2z237YrCHqLjGadDQZEhRpeDASnw11oUdbDAdfFt1mo8VY99cXK1cdff5b+TEAN2MjDBcZdqVziUjiJlFuAAIQbIAApS3eY+tN6/b6NtJpnvm2jjTZHSAf+ZKKW3YVyRBZE8PLLi+am0De1dEQUz5vAmoPg0Jhmcydju1rR4Chy0Hz0CAmzh+OB0JRm7rYyF+aDEKbA+8f7rGQfK/HBoQYv+NnF1V/N9u9zajbZp9XfstTBden/+dem32kG+T35/RfaNQhtMAoVjNFfG6LfU7Sv7zL38NxfFZYRZkQdjRd+3llUVlibSilZ9y7g41j/TT7VFiBplUzWp/rh0pFVcvghTadJiLEAGe5LlzYED9kaXreN5upUoMv137bMvTVnbRRJQz12iXW8liz6by6HOA2Rr0p+YkqxVK3/vK7r7sb//PJG4GBcsiaJVgzkW6j/qaK5XKqKZhVE9ps///Li66p5IUe5j4JXrNic0NV6oiZ3MwOfiKMN1YEc5ufFVhKq5zca0OUjYdxv+/H8Z5UX9xYze+hzEwUD+pNdYN09kB9nw0i2hDWzP3ipOVwVPO1SkgpIvPrqBXzu76B83pRub9olN/bNPAH3eyfppTfSS53GGA+wufowS+LyTXUwU0Buy9CTlHP5hXH4Iw7BmlxHgTjEdNU/ALUI41AI7ODC1wr8Gnz+L3bT5VLM+6FvKc3LU+gHDTux8ks=</DEVCERT></CLIENTINFO>";
                },
            
                checkConnection : function() {
                    return true;
                }
            }
        })();

        appengine = (function(){

            return {

                getResolution: function() {
                    return {
                        width: (window.innerWidth !== null? window.innerWidth: document.body !== null? document.body.clientWidth:null),
                        height: (window.innerHeight !== null? window.innerHeight: document.body !== null? document.body.clientHeight:null)
                    };
                }
            }
        })();
        
        /**
         * @ignore
         */
        deviceConsole = {
            /**
                *@ignore
                */
            log: function(){
                var args = Array.prototype.slice.call(arguments);
                console.log.apply(console, args);
            },
            /**
                *@ignore
                */
            info: function(){
                var args = Array.prototype.slice.call(arguments);
                console.info.apply(console, args);
            },
            /**
                 *@ignore
                 */
            error: function(){
                var args = Array.prototype.slice.call(arguments);
                console.error.apply(console, args);
            },
            /**
                 *@ignore
                 */
            debug: function(){
                var args = Array.prototype.slice.call(arguments);
                console.debug.apply(console, args);
            },
            /**
                 *@ignore
                 */
            warn: function(){
                var args = Array.prototype.slice.call(arguments);
                console.warn.apply(console, args);
            }
        };

        /*
         * demo function to handle the exit event
         *
         */
        /**
         * @ignore
         */
        exitHandling = function(){
            accedo.console.log("exit application");
        };

        /**
         * Function to be called when a device is registered
         * @function
         * @private
         * @ignore
         */
        init = function() {
            //initiate the keyHandler
            initiateKeyHandler();
        };
        
        // initiate function
        /**
         * @ignore
         */
        init();

        return {
            /**
             * identification of the device manager
             * @constant
             * @public
             */
            deviceId : DEVICEID,
            /**
             * array of key action to be looked
             * @public
             */
            keySet : keys,
            mediaPlayer : mediaPlayer,
            identification : deviceIdentification,
            appengine : appengine,
            deviceConsole: deviceConsole,
            exitHandling :exitHandling          
        }

    })());
    
    //Value set to notify the lazy loader that this file has been loaded
    return {};
});