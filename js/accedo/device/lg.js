/**
 * @fileOverview Device Manager file for LG platforms
 * @author <a href="mailto:victor.leung@accedo.tv">Victor Leung</a>
 */

accedo.define('accedo.device.lg', ["accedo.device.manager", "accedo.utils.object",'accedo.utils.querystring','accedo.events.dispatcher', 'accedo.utils.hash'], function() {
    /**
     * Device manager for lg, should never be called directly, instead use {@link accedo.device.manager}.
     * @name lg
     * @memberOf accedo.device
     * @class
     * @constructor
     */
    
    /**
     * @object A config JSON specifying each aspect on the platforms
     */
    accedo.device.manager.registerDevice((function(){
 
        /**
         * identification of the device manager
         * @constant DEVICEID
         * @public
         * @memberOf accedo.device.lg
         * @ignore
         */
        var DEVICEID = "lg",

        /**
         * to Store the widevine plugin ESN
         * @field widevineESN
         * @private
         * @memberOf accedo.device.lg
         * @ignore
         */
        widevineESN = null,

        /**
         * to Store the widevine plugin SDI_ID
         * @field widevineESN
         * @private
         * @memberOf accedo.device.lg
         * @ignore
         */
        widevineSDI_ID = null,

        /**
         * tell if it is running on Emulator or not
         * @field isEmulator
         * @private
         * @memberOf accedo.device.samsung
         * @ignore
         */
        isEmulator = false,

        /**
         * array of key action to be looked
         * @field keys
         * @ignore
         */
        keys = {},

        initiateKeyHandler, mediaPlayer, deviceIdentification, appengine, 
        system, deviceConsole, init, initiateMouseHandler;

        // set key value
        keys[48]  = '0';
        keys[49]  = '1';  // the left-side key 1
        keys[50]  = '2';
        keys[51]  = '3';
        keys[52]  = '4';
        keys[53]  = '5';
        keys[54]  = '6';
        keys[55]  = '7';
        keys[56]  = '8';
        keys[57]  = '9'; // the left-side key 9

        keys[38] = "up";
        keys[40] = "down";
        keys[37] = "left";
        keys[39] = "right";
        keys[13] = "enter";
        keys[47] = "option";
        keys[134] = "search";

        keys[81] = "play";
        keys[415] = "play";
        keys[19] = "pause";
        keys[413] = "stop";
        keys[417] = "ff";
        keys[412] = "rw";
    
        keys[27] = "back";
        keys[461] = "back";

        keys[457] = "exit";
		
		keys[403] = "red";
		keys[404] = "green";
		keys[405] = "yellow";
		keys[406] = "blue";
		
		keys[9] = "red";	//emulator spits out 9 for red

        /**
         * Function to be called when a device is registered
         * @function initiateKeyHandler
         * @private
         * @ignore
         */
        initiateKeyHandler = function(){
            /**
             * @ignore
             */
            document.onkeydown = function(key){
                var keynum = key.keyCode,
                handled = accedo.device.manager.keyHandling(keynum);
                if(handled == false){
                    return;
                }
            };
        };

        initiateMouseHandler = function(){
            window.onmouseon = accedo.utils.fn.bind(accedo.device.manager.system.onmouseon, accedo.device.manager.system);
	        window.onmouseoff = accedo.utils.fn.bind(accedo.device.manager.system.onmouseoff, accedo.device.manager.system);
        };

        /**
         * Create and LG player object
         * @Class mediaPlayer
         * @private
         * @ignore
         */
        mediaPlayer = (function(){

            return {

                /*
                 * PlayState of the video Object
                 * @name STOPPED
                 * @private
                 * @constant
                 * @memberOf accedo.device.lg
                 */
                STOPPED: 0,
                /*
                 * PlayState of the video Object
                 * @name PLAYING
                 * @private
                 * @constant
                 * @memberOf accedo.device.lg
                 */
                PLAYING: 1,
                /*
                 * PlayState of the video Object
                 * @name PAUSED
                 * @private
                 * @constant
                 * @memberOf accedo.device.lg
                 */
                PAUSED: 2,
                /*
                 * PlayState of the video Object
                 * @name SKIPPING
                 * @private
                 * @constant
                 * @memberOf accedo.device.lg
                 */
                SKIPPING: 3,
                /*
                 * PlayState of the video Object
                 * @name SPEED
                 * @private
                 * @constant
                 * @memberOf accedo.device.lg
                 */
                SPEED: 4,
                /*
                 * PlayState of the video Object
                 * @name BUFFERING
                 * @private
                 * @constant
                 * @memberOf accedo.device.lg
                 */
                BUFFERING: 5,
                /*
                 * PlayState of the video Object
                 * @name CONNECTING
                 * @private
                 * @constant
                 * @memberOf accedo.device.lg
                 */
                CONNECTING: 6,
                /*
                 * PlayState of the video Object
                 * @name ERROR
                 * @private
                 * @constant
                 * @memberOf accedo.device.lg
                 */
                ERROR: 7,
                /*
                 * PlayState of the video Object
                 * @name FINISHED
                 * @private
                 * @constant
                 * @memberOf accedo.device.lg
                 */
                FINISHED: 8,
                /*
                 * playerdiv
                 * @name PLAYER_DIV
                 * @constant
                 * @memberOf accedo.device.lg
                 */
                PLAYER_DIV: 'playerObject',
                /*
                 * FULLSCREEN WIDTH
                 * @name fullscreenWidth
                 * @private
                 * @memberOf accedo.device.lg
                 */
                fullscreenWidth: null,
                /*
                 * FULLSCREEN HEIGHT
                 * @name fullscreenHeight
                 * @private
                 * @memberOf accedo.device.lg
                 */
                fullscreenHeight: null,
                /*
                 * Url of the video
                 * @name url
                 * @private
                 * @memberOf accedo.device.lg
                 */
                url: null,
                /*
                 * type of video player, it can be video or audio
                 * @name type
                 * @private
                 * @memberOf accedo.device.lg
                 */
                type: "video",
                /*
                 * currentState of video player
                 * @name state
                 * @private
                 * @memberOf accedo.device.lg
                 */
                state: -1,
                /*
                 * loop of video player
                 * @name loop
                 * @private
                 * @memberOf accedo.device.lg
                 */
                loop: false,
                /*
                 * reference to the plugin object
                 * @name plugin
                 * @private
                 * @memberof accedo.device.lg
                 */
                _plugin: null,
                /*
                 * the current time
                 * @name _curTime
                 * @private
                 * @memberof accedo.device.lg
                 */
                _curTime: 0,
                /*
                 * JSON containing current window size
                 * @name _currentWindowSize
                 * @private
                 * @memberof accedo.device.lg
                 */
                _currentWindowSize: null,
                /*
                 * total time of the video
                 * @name _totalTime
                 * @private
                 * @memberof accedo.device.lg
                 */
                _totalTime: 0,
                /*
                 * has the media player be initialized
                 * @name _inited
                 * @private
                 * @memberof accedo.device.lg
                 */
                _inited: false,
                /*the current playback speed
                 * @name _playbackSpeed
                 * @private
                 * @memberof accedo.device.lg
                 */
                _playbackSpeed: 1,
                /*is there any pause function with holding?
                 * @name _withHoldingPause
                 * @private
                 * @memberof accedo.device.lg
                 */
                _withHoldingPause: false,
                /*is there any stop function with holding?
                 * @name _withHoldingStop
                 * @private
                 * @memberof accedo.device.lg
                 */
                _withHoldingStop: false,
                /*object storing the setTimeoutEvent
                 * @name _skipDelayFire
                 * @private
                 * @memberof accedo.device.lg
                 */
                _skipDelayFire: null,
                /* direction for skipping - ff or bw
                 * @name _isFF
                 * @private
                 * @memberof accedo.device.lg
                 */
                _isFF: null,
                /* the time when first press skip
                 * @name _accumulatedArrivalTime
                 * @private
                 * @memberof accedo.device.lg
                 */
                _accumulatedArrivalTime: null,
                /* boolean, indicates whether to progress jumping
                 * @name _jumpProgress
                 * @private
                 * @memberof accedo.device.lg
                 */
                _jumpProgress: false,
                /* accumated seconds to skip
                 * @name _accumulatedSec
                 * @private
                 * @memberof accedo.device.lg
                 */
                _accumulatedSec: null,
                /* current bit rate of the video
                 * @name _currentBitrate
                 * @private
                 * @memberof accedo.device.lg
                 */
                _currentBitrate: null,
                /* available bitrate of the video
                 * @name _availableBitrates
                 * @private
                 * @memberof accedo.device.lg
                 */
                _availableBitrates: null,
                /* store parameter of the player when it is started up
                 * @name _playerParam
                 * @private
                 * @memberof accedo.device.lg
                 */
                _playerParam: null,
                /* indicate the current video type
                 * @name _WMDRM
                 * @private
                 * @memberof accedo.device.lg
                 */
                _WMDRM:	false,
                /* iframe stores the wmdrm validate website
                 * @name _iframe
                 * @private
                 * @memberof accedo.device.lg
                 */
                _iframe: null,
                /* parent node of the container
                 * @name _parentNode
                 * @private
                 * @memberof accedo.device.lg
                 */
                _parentNode: null,
                /* container store the player
                 * @name _playerContainer
                 * @private
                 * @memberof accedo.device.lg
                 */
                _playerContainer: null,
                /* store the player object
                 * @name _playerObject
                 * @private
                 * @memberof accedo.device.lg
                 */
                _playerObject: null,
                /* the status of video progress checking
                 * @name _checkingProgres
                 * @private
                 * @memberof accedo.device.lg
                 */
                _checkingProgress: false,
                /* the interval time (in ms)of the video progress checking
                 * @name _checkingInterval
                 * @private
                 * @memberof accedo.device.lg
                 */
                _checkingInterval: 500,
                /* the interval of the video progress checking
                 * @name _progressCheckFuncId
                 * @private
                 * @memberof accedo.device.lg
                 */
                _progressCheckFuncId : null,
                /* the time limit of the connection is second
                 * @name _connectionTimeLimit
                 * @private
                 * @memberof accedo.device.lg
                 */
                _connectionTimeLimit: 90,
                /*
                 * @name _videoDownloadable
                 * @private
                 * @memberof accedo.device.lg
                 */
                _videoDownloadable: true,
                /*
                 * @name _connectionTimer
                 * @private
                 * @memberof accedo.device.lg
                 */
                _connectionTimer: null,
                /*  to determine if it is resume play
                 * @name _resumePlay
                 * @private
                 * @memberof accedo.device.lg
                 */
                _resumePlay: false,
                /* the second start to play when it is resume play cases.
                 * @name _resumePlaySec
                 * @private
                 * @memberof accedo.device.lg
                 */
                _resumePlaySec: 0,
                /*
                 * container name of the player
                 * @name CONTAINER_NAME
                 * @private
                 * @constant
                 * @memberof accedo.device.lg
                 */
                CONTAINER_NAME: "lgVideoPlayer",
                
                /**
                 * To initialize the plugin player
                 * @function init
                 * @public
                 * @ignore
                 */
                init: function(param){
                	//accedo.console.log('MediaPlayer init');
                    this.state = this.STOPPED;

                    //read in the parameter
                	/*
                    if(param){
                        this._playerParam = param;
                        this._parentNode = param.parentNode.getRoot() || document.getElementsByTagName("body")[0];
                    }else{
                        this._parentNode = document.getElementsByTagName("body")[0];
                    }
                    */

                    if(param){
                        this._playerParam = param;
                    }
                    
                    if(param && param.parentNode){
                    	this._parentNode = param.parentNode.getRoot() || document.getElementsByTagName("body")[0];
                    }else{
                        this._parentNode = document.getElementsByTagName("body")[0];
                    }
					
                    if(this._inited){
                        accedo.console.log(":::::::::: player is already initialized");
                        return true;
                    }
                    accedo.console.log("....... player start initializing");
                    
                    this.eventHandler = this.defaultEventHandler;

                    //create div tag
                    this._playerContainer = document.createElement("div");
                    this._playerContainer.id = this.CONTAINER_NAME;

                    this._playerContainer.style.backgroundColor = "#000000";
                    this._playerContainer.style.position = "absolute";
                    this._playerContainer.style.zIndex = "2";

                    this.setFullscreen();
                    this._playerObject = null;

                    //append to the canvas
                    try{
                        this._parentNode.appendChild(this._playerContainer);
                    } catch(e) {
                        accedo.console.error("lgVideoPlayer error: while creating the player object: " + e);
                        return false;
                    }
                    return true;
                },
				
                postinit: function(){
                	//accedo.console.log(":::::::::: postinit; this._inited: " + this._inited);

                    this._playerContainer.style.display = "block";
                    this.show();
	
                    if(this._playerObject){
                        this._playerObject.stop();
                        this._playerObject.onPlayStateChange = null;
                        this._playerObject.onReadyStateChange = null;
                        this._playerObject.onBuffering = null;
                        this._playerObject.onError = null;
                        this._playerObject = null;
                    }

                    this._inited = false;

                    if(this._progressCheckFuncId){
                        window.clearInterval(this._progressCheckFuncId);
                    }

                    this._checkingProgress = false;

                    accedo.utils.fn.delay(accedo.utils.fn.bind(function(){
                    	if(this.opts)
                			accedo.console.log("delay this.opts.widevine:"+this.opts.widevine);
                		
                    	if(this.opts && this.opts.speedtest){
                    		document.getElementById(this.CONTAINER_NAME).innerHTML = '<object type="application/x-netcast-av" id="playerObject" preBufferingTime="10"></object>';
                    	}else if(this.opts && this.opts.widevine){
                            document.getElementById(this.CONTAINER_NAME).innerHTML = '<object type="application/x-netcast-av" drm_type ="widevine" id="playerObject" preBufferingTime="10"></object>';
                        }else{
                            document.getElementById(this.CONTAINER_NAME).innerHTML = '<object type="application/x-netcast-av" id="playerObject" preBufferingTime="10"></object>';
                        }

                        this._playerObject = document.getElementById("playerObject");
                        this.setWindowSize(this._currentWindowSize);
                   		
                   		accedo.console.log("this._playerObject:"+this._playerObject);

                        //wait for checkCondition to become true, then execute callback
                        function waitFor(checkCondition, callback, checkInterval, checkCountLimit, onLimitExceedCallback) {
                            if (typeof(checkInterval) !== 'number') {
                                checkInterval = 500;
                            }
                            if (typeof(checkCountLimit) !== 'number') {
                                checkCountLimit = -1;
                            }

                            if (checkCountLimit === 0) { // count limit reached
                                if(onLimitExceedCallback){
                                    onLimitExceedCallback();
                                }
                                return;
                            } else if (!checkCondition()) {
                                if (checkCountLimit > 0) {
                                    checkCountLimit--;
                                }
                                setTimeout(function(){
                                    waitFor(checkCondition, callback, checkInterval, checkCountLimit, onLimitExceedCallback);
                                }, checkInterval);
                                return;
                            }
                            callback();
                        }

                        waitFor( // wait for the object to be ready
                            accedo.utils.fn.bind(function(){ // make sure the context is correct
                                accedo.console.info("Waiting for video player object init...");
                                return !!document.getElementById("playerObject");
                            }, this),
                            accedo.utils.fn.bind(function(){
                                accedo.console.info("Video player object ready, proceeding...");
                                this._playerObject = document.getElementById("playerObject");

                                this._playerObject.autoStart = true;
                                this._playerObject.onPlayStateChange = accedo.utils.fn.bind(this.onPlayStateChange, this);
                                this._playerObject.onReadyStateChange = accedo.utils.fn.bind(this.onReadyStateChange, this);
                                this._playerObject.onBuffering = accedo.utils.fn.bind(this.onBuffering, this);
                                this._playerObject.onError = accedo.utils.fn.bind(this.onError, this);

                                this.onLoadCallback();
                                this._inited = true;
                            }, this),
                            500 // check interval in milliseconds
                            );
                    }, this), 1);
                },

                show: function(){
                    this._playerContainer.style.visibility = "visible";
                },

                hide: function(){
                    this._playerContainer.style.visibility = "hidden";
                },

                onLoadCallback: function(){
                    accedo.console.log("on load callback executed! (nothing to do for now...)");
                },

                deinit: function(){
                    accedo.console.log("////// lg media deinit");

                    if(!this._inited){
                        return true;
                    }
                    
                    //reset the value
                    this.onLoadCallback = null;
                    this.url = null;
                    this.onSetCurTime = null;
                    this.eventHandler = this.defaultEventHandler;
                    this.statusChangeCallback = null;

                    this._playerObject.onPlayStateChange = null;
                    this._playerObject.onReadyStateChange = null;
                    this._playerObject.onBuffering = null;
                    this._playerObject.onError = null;

                    if(this._progressCheckFuncId){
                        window.clearInterval(this._progressCheckFuncId);
                    }

                    this._inited = false;
                    this._playerContainer.removeChild(this._playerObject);
                    this._parentNode.removeChild(this._playerContainer);

                    accedo.console.log("Video player deinit success");
                    return true;
                },

                setWindowSize: function(obj){
                    if(obj && !accedo.utils.object.isUndefined(obj.top) && !accedo.utils.object.isUndefined(obj.left) &&
                        !accedo.utils.object.isUndefined(obj.width)  && !accedo.utils.object.isUndefined(obj.height)){
                        accedo.console.log("top: " + obj.top + " left: " + obj.left + " width: " + obj.width + " height: " + obj.height);
                        this._playerContainer.style.width = obj.width +"px";
                        this._playerContainer.style.height = obj.height +"px";
                        this._playerContainer.style.left = obj.left +"px";
                        this._playerContainer.style.top = obj.top +"px";

                        if(this._playerObject){
                            this._playerObject.setAttribute("width",obj.width);
                            this._playerObject.setAttribute("height",obj.height);
                
                            if (parseInt(obj.top,10) === 0 && parseInt(obj.left,10) === 0 &&
                                parseInt(obj.width,10) === this.fullscreenWidth && parseInt(obj.height,10) === this.fullscreenHeight) {
                                this._playerObject.className = "fullscreen";
                            }else {
                                this._playerObject.className = "" ;
                            }
                        }

                        this._currentWindowSize = obj;
                        return true;
                    }
                    return false;
                },

                setFullscreenSize: function(width, height){
                    this.fullscreenWidth = width;
                    this.fullscreenHeight = height;
                },

                setFullscreen: function(xPos, yPos){
                    //to ensure there is fullscreen width/height
                    if((!this.fullscreenWidth || !this.fullscreenHeight) && appengine){
                        var resolution = appengine.getResolution();
                        this.setFullscreenSize(resolution.width, resolution.height);
                    }
                    var fullscreenXPos = xPos ? xPos : 0;
                    var fullscreenYPos = yPos ? yPos : 0;
                    if(this.setWindowSize){
                        this.setWindowSize({
                            left:fullscreenXPos,
                            top:fullscreenYPos,
                            width:this.fullscreenWidth,
                            height:this.fullscreenHeight
                        });
                    }
                },

                setMediaURL: function(mediaUrl, opts){
                    accedo.console.log("setMediaURL " + mediaUrl);
                    if(mediaUrl){
                        this.url = mediaUrl;
                        
                        this.opts = opts;
                        this._hls = false;
                        this._has = false;
                        this._WMDRM = false;
                        this._widevine = false;
                        this.postinit();
                        this.onLoadCallback = accedo.utils.fn.bind(function() {
                        	accedo.console.log("onLoadCallback");
                        	
                            this._playerObject.data = mediaUrl;
	
                            if(opts){
                            	accedo.console.log("widevine "+opts.widevine);
                                if(opts.widevine){
                                    this._widevine = true;
                                    accedo.console.log("opts:"+JSON.stringify(opts));
                                    accedo.console.log("set widevine device_id: "+opts.device_id); 
                                    accedo.console.log("set widevine DRM url: " + opts.drm_url); 
                                    accedo.console.log("set widewine portal: "+opts.portal);
                                    
                                    if(opts.speedtest === true){
                                    	accedo.console.log("no drm");
                                    	this._playerObject.removeAttribute("drm_type");
                                    }else{
                                    	accedo.console.log("yes drm");
                                        this._playerObject.setAttribute("drm_type", "widevine");
                                        if (typeof(this._playerObject.setWidevineDeviceID) != "undefined"){
                                        	this._playerObject.setWidevineDeviceID(opts.device_id);                                 
	                                        this._playerObject.setWidevineDrmURL(opts.drm_url);
	                                        this._playerObject.setWidevinePortalID(opts.portal);
                                        }   
                                    }
                                    
                                }else if(opts.WMDRM && opts.authCookiesUrl){
                                    //to open an iframe and set cookie so that it will have a cookie when play at checking license stage
                                    this._WMDRM = true;
                                    if(!this._iframe){
                                        this._iframe = accedo.Element.create("iframe");
                                        this._iframe.setStyle({
                                            "width":"0px",
                                            "height":"0px",
                                            "visibility":"hidden"
                                        });
                                        this._playerContainer.appendChild(this._iframe.getHTMLElement());
                                        accedo.console.log("create the iframe to set-cookie");
                                    }
                                    this._iframe.setAttributes({
                                        src:opts.authCookiesUrl
                                    });
                                    accedo.console.log("set the url to the iframe"+opts.authCookiesUrl);
                                }

                                if(!accedo.utils.object.isUndefined(opts.type) && opts.type === "audio"){
                                    this.type = "audio";
                                    this._currentWindowSize = this.AUDIO_SIZE;
                                    this.setWindowSize(this._currentWindowSize);
                                }
                            }
                        	accedo.console.log("return "+mediaUrl);
                            return mediaUrl;
                        }, this);
                    }else{
                        return false;
                    }
                },

                getMediaURL: function(){
                    if(this.url){
                        return this.url;
                    }else{
                        return false;
                    }
                },

                play: function(param){
                	if(this._skipDelayFire !== null){
			            this.stopSkipping();
			        }
                	
                	if (!this._checkingProgress){
                        accedo.console.info("Play progress checking started!");
                    	this._progressCheckFuncId = window.setInterval(accedo.utils.fn.bind(function(){
                    		if(this._playerObject){
                    			if(this._playerObject.hasOwnProperty("mediaPlayInfo") !== false){
	                    			if(this._playerObject.mediaPlayInfo().currentPosition){
	                    				this.setCurTime(this._playerObject.mediaPlayInfo().currentPosition);
	                    			}
	                    		}	
                    		}
                        }, this), this._checkingInterval);
                        this._checkingProgress = true;
                    }
                    this._playerContainer.style.display = "block";
                    accedo.console.log("LG media play called. current playing state: " + this.state);

                    var sec = 0, __reset, __doPlay;
                    if(!this._inited){
                        accedo.console.log("media not ready! ignoring the play command...");
                        return false;
                    }
                    if(param && param.sec){
                        sec = param.sec;
                    }
                    //accedo.console.warn("switch state from:" + this.state);
                    
                    __reset = accedo.utils.fn.bind(function(){
                        this._withHoldingPause = false;
                        this._withHoldingStop = false;
                        this._playbackSpeed = 1;
                        this._curTime = 0;
                        this._totalTime = 0;
                    },this);

                    __doPlay = accedo.utils.fn.bind(function(sec){
                        if(sec){
                            this._resumePlay = true;
                            this._resumePlaySec = sec;
                            if (typeof(this._playerObject.play) != "undefined"){
                            	this._playerObject.play(this._playbackSpeed);
                            }
                        }else{
                            if (typeof(this._playerObject.play) != "undefined"){
                            	this._playerObject.play(this._playbackSpeed);
                            }
                        }
                    },this);

					switch (this.state){
                        case this.FINISHED:
                        case this.STOPPED:
                        case this.ERROR:
                        case this.CONNECTING:
                            __reset();
                            __doPlay(sec);
                            return true;
                        // keep going
                        case this.PLAYING:
                        case this.BUFFERING:
                        case this.SPEED:
                        case this.PAUSED:
                            __doPlay(sec);
                            return true;
                        default:
                            return false;
                    }
                    //accedo.console.info("play done");
                },

                pause: function(){
                    accedo.console.log("pause; this.state: " + this.state);
                    if(this._skipDelayFire !== null){
			            this.stopSkipping();
			        }
                    if(this.state === this.STOPPED || this.state === this.ERROR){
                        return false;
                    }
                    if(this.state === this.BUFFERING || this.state === this.CONNECTING){
                        accedo.console.log("pending pause");
                        this._withHoldingPause = true;
                        return false;
                    }

                    this._playerObject.play(0);
                    return true;
                },

                stop: function(){
                    accedo.console.log("stop");
                    if(this._skipDelayFire !== null){
			            this.stopSkipping();
			        }
                    if(this.state === this.CONNECTING){
                        //stopping a widevine video during the connection stage is FATAL.
                        accedo.console.log("pending Stop");
                        this._withHoldingStop = true;
                        return false;
                    }else if(this.state !== this.STOPPED){
                        //if it is ERROR, it should have been stopped previously or not appropriate to stop (e.g. connection timeout)
                        if(this.state !== this.ERROR){
                            this.stopAction();
                        }
                        // start regular progress checking
                        if (this._checkingProgress){
                            accedo.console.info("Play progress checking stopped!");
                            window.clearInterval(this._progressCheckFuncId);
                            this._checkingProgress = false;
                        }
                        return true;
                    }
                    return false; // has not stopped
                },

                stopAction: function(){
                    accedo.console.log("stop action enforced");
                    if(this._playerObject){
                        this._playerObject.stop();
                    }
                    accedo.console.log("stop action done");
                },

                resume: function(){
                    accedo.console.log("resume");
                    if(this._skipDelayFire !== null){
			            this.stopSkipping();
			        }
                    if(this.state === this.PAUSED){
                        this.statusChange(this.PLAYING);
                        this._playbackSpeed = 1; // start playing with speed 1
                        this._playerObject.play(this._playbackSpeed);
                        return true;
                    }else{
                        return false;
                    }
                },
			   
				// new skip function - support continuous skipping
				skip: function(param) {
			        //Note: haven't been test for non progressive usage.
			        var sec = 10,			//number of second to skip
			        progressive = false,	//state whether it is progressive or not
			        multipleFactor = 1,		//for progressive, multiple of increase in jump distance for each detection
			        delaySec = 2,			//for progressive, seconds of delay before the skip is fired
			        progressiveCap = 300,	//for progressive, maximum amount of second in each increase
			        self = this,
			        jumpFunction;
			        //to be reverse compatible
			        if(typeof param === "number"){
			            sec = param;
			        }else{
			            if(param && param.sec){
			                sec = param.sec;
			            }
			            if(param && param.progressive){
			                progressive = param.progressive;
			            }
			            //Progressive-specific params
			            if (progressive){
			                if(param && param.multipleFactor){
			                    multipleFactor = param.multipleFactor;
			                    if(multipleFactor < 1){
			                        multipleFactor = 1; //couldn't be smaller and smaller, right?
			                    }
			                }
			                if(param && param.delaySec){
			                    delaySec = param.delaySec;
			                }
			                if(param && param.progressiveCap){
			                    progressiveCap = param.progressiveCap;
			                }
			                this._jumpProgress = true;
			            }
			        }
			
			        if(!sec || sec === 0){
			            accedo.console.warn("Cannot skip 0 seconds!");
			            return false;
			        }
			        
			        if (this._isFF == null){
		        		// first skipping - define the direction of skipping
		        		this._isFF = (sec > 0);
		        	}
			        if(this.state === this.SKIPPING){
			        	// check if it was a request for skipping in other direction
			        	if (sec > 0 && !this._isFF){
			        		// it was backward skipping; now should change to fast-forward
			        		accedo.console.warn("Change skipping direction to FF");
			        		this.stopSkipping(true);
			        		this._isFF = true;
			        	}
			        	else if (sec < 0 && this._isFF){
			        		// it was fast-forward skipping; now should change to backward
			        		accedo.console.warn("Change skipping direction to BW");
			        		this.stopSkipping(true);
			        		this._isFF = false;
			        	}
			        	else{
			        		// skipping in the same direction => ignore
			        		accedo.console.warn("Ignore following ff/bw press - continue skipping till play/pause pressed!");
				            return false;
			        	}   
			        }
			        else if(this.state !== this.PLAYING &&
			        	//this.state !== this.SKIPPING && 
			            this.state !== this.PAUSED){  //paused and then skip seem would bring about error
			            accedo.console.warn("Cannot skip while video is neither playing nor paused!");
			            return false;
			        }
			        if(this.state === this.PAUSED){
			            this.resume();
			        }
			        
			        jumpFunction = (function(){
			            return function(){
			                var timeStr, h = 0, m = 0, s = 0, timeSec = Math.round(self._accumulatedArrivalTime/1000);
			                
			                h = Math.floor(timeSec/3600);
	                        m = Math.floor((timeSec%3600)/60);
	                        s = Math.floor(timeSec%60);
	                        timeFormatted = h + ":" + m + ":" + s;
			                
			                accedo.console.log(">>>> start jumping, _accumulatedArrivalTime: " + self._accumulatedArrivalTime + " = " + timeFormatted);
			                self._playerObject.seek(self._accumulatedArrivalTime);		
			                
			                //self.statusChange(self.getCorrectPlayState(self._playerObject.playState));
			                self.statusChange(self.SKIPPING);
			                
			                clearTimeout(self._skipDelayFire);
			                self._skipDelayFire = null;
			                
			                if(progressive && self._jumpProgress){
				                //calculate next step for skipping
				                //accedo.console.log("calculate next step for skipping");
				                self.findSkipStep(sec, progressive, multipleFactor, progressiveCap);
				            	self._skipDelayFire = accedo.utils.fn.delay(jumpFunction, delaySec);
				            	//accedo.console.log("skip again");
				            }
				            else{
				            	self.stopSkipping();
				            }
			            };
			        }());
			        
			        this.statusChange(this.SKIPPING);
			        accedo.console.log("[skip params]: " + sec + ", " + progressive + ", " + multipleFactor + ", " + progressiveCap);
			        this.findSkipStep(sec, progressive, multipleFactor, progressiveCap);
			
			        if(!progressive){
			            jumpFunction();
			        }else{
			            this._skipDelayFire = accedo.utils.fn.delay(jumpFunction, delaySec);
			            //this.statusChange(this.SKIPPING);
			        }
			        //accedo.console.log("skip action taken");
			        return true;
			    },
			    
			    findSkipStep: function(sec, progressive, multipleFactor, progressiveCap){
			    	if(typeof this._accumulatedArrivalTime !== "number"){
			            this._accumulatedArrivalTime = this._curTime*1000;
			        }
			        //accedo.console.log("[findSkipStep] initially... this._accumulatedArrivalTime " + this._accumulatedArrivalTime);
			        
			        if(this._accumulatedSec === null || this._accumulatedSec * sec < 0){
			            this._accumulatedSec = sec;
			        }else{
			            this._accumulatedSec = Math.round(this._accumulatedSec * multipleFactor);
			        }
			        accedo.console.log("accumulatedSec: " + this._accumulatedSec);
			
			        if(progressive && this._accumulatedSec > progressiveCap){  // cap the step
			            this._accumulatedSec = progressiveCap;
			        }else if(progressive && this._accumulatedSec < -progressiveCap){
			            this._accumulatedSec = -progressiveCap;
			        }
			
			        //accedo.console.debug("calling \"setCurTime(targetCurTime, true)\" to indicate skipping...");
			        if(sec >= progressiveCap || sec <= -progressiveCap){  // requesting a huge skip, so forget about the progressive step
			            this._accumulatedArrivalTime += sec*1000;
			        }else{ // skip a step
			            this._accumulatedArrivalTime += this._accumulatedSec*1000;
			        }
			        
			        if(this._accumulatedArrivalTime > this._totalTime*1000 - 1000){  // make sure do not skip outside of boundary
			            this._accumulatedArrivalTime =  this._totalTime*1000 - 5*1000;
			            accedo.console.log("too far away... set time 5 sec before end: _accumulatedArrivalTime: " + this._accumulatedArrivalTime);
			            this._jumpProgress = false;
			        }else if(this._accumulatedArrivalTime <= 0){
			            this._accumulatedArrivalTime = 5*1000;//0;
			            accedo.console.log("too early... set time 5 sec before start: _accumulatedArrivalTime: " + this._accumulatedArrivalTime);
			            this._jumpProgress = false;
			        }
			        this.setCurTime(this._accumulatedArrivalTime, true);
			
			        //accedo.console.info("skip -  accumulatedArrivalTime: " + this._accumulatedArrivalTime);
			    },
			    
			    stopSkipping: function(doNotChangeStatus){
			    	if(this._skipDelayFire !== null){
			            clearTimeout(this._skipDelayFire);
			            this._skipDelayFire = null;
			        }
			        this._isFF = null;
			    	this._accumulatedArrivalTime = null;
			    	this._accumulatedSec = null;
		        	if (!doNotChangeStatus){
		        		this.statusChange(this.getCorrectPlayState(this._playerObject.playState));	
		        	}
		        	accedo.console.warn("stop skipping!!!!");
			    },

                speed: function(speed){
                    if(this.state !== this.PLAYING && this.state !== this.SPEED){
                        return false;
                    }

                    speed = Math.floor(speed);

                    if(speed > 16){
                        speed = 16;
                    }
                    if(speed < -16){
                        speed = -16;
                    }

                    this._playbackSpeed = speed;
                    //warning. Not all device and media type would support this one function
                    //this._plugin.SetPlaybackSpeed(1);
                    this._playerObject.SetPlaybackSpeed(1);

                    if (speed === 1) {
                        if (this.state !== this.PLAYING) {
                            this.statusChange(this.PLAYING);
                        }
                    } else {
                        //this._plugin.SetPlaybackSpeed(speed);
                        this._playerObject.SetPlaybackSpeed(speed);

                        if (this.state !== this.SPEED) {
                            this.statusChange(this.SPEED);
                        }
                    }
                    return true;
                },

                isState: function(myState){
                    myState = myState.toUpperCase();
                    switch (myState) {
                        case 'STOPPED' :
                        case 'PLAYING' :
                        case 'PAUSED' :
                        case 'SKIPPING' :
                        case 'SPEED' :
                        case 'BUFFERING' :
                        case 'CONNECTING' :
                        case 'ERROR' :
                            return this.state === this[myState];
                        default:
                            return false;
                    }
                },

                setLoop: function(on){
                    this.loop = on;
                },

                getLoop: function(){
                    return this.loop;
                },

                getBitrates: function(){
                    if(this._currentBitrate === null || this._availableBitrates === null){
                        return false;
                    }else{
                        return {
                            currentBitrate : this._currentBitrate,
                            availableBitrates : this._availableBitrates
                        };
                    }
                },

                getPlaybackSpeed: function(){
                	accedo.console.log("lg - getPlaybackSpeed: " + this._playbackSpeed);
                	return this._playbackSpeed;
                	//accedo.console.log("lg - getPlaybackSpeed: " + this._playerObject.playbackRate);
                    //return this._playerObject.playbackRate;
                },
                
                getBufferingProgress: function(){
                	var bufferingProgress = this._playerObject.bufferingProgress;
                	return bufferingProgress;
                },

                setPlayerTime: function(sec){
                    if(this.state === this.FINISHED){
                        //restart the player
                        this.play({
                            sec:sec
                        });
                        return true;
                    }

                    if(sec < 0 || sec > this.getTotalTime()){
                        return false;
                    }

                    if(this._inited && this.url && sec >= 0){
                        this.setCurTime(sec*1000, true);
                        if (this.state === this.PLAYING){
                        	this._playerObject.seek(sec*1000);
                        }
                    }
                },

                getTotalTime: function(){
                    return this._totalTime;
                },

                setTotalTime: function(){
                    if(!this._playerObject){
                        this._totalTime = 0;
                        return;
                    }

                    var mediaInfo = this._playerObject.mediaPlayInfo();
                    if(mediaInfo && mediaInfo.duration){
                        this._totalTime = Math.floor(mediaInfo.duration/1000);	//convert to milli seconds
                    }
                    accedo.console.log("durations: " + this._totalTime + "sec");
                },

                getCurrentTime: function(seconds){
                    return seconds ? Math.floor(this._curTime / 1000) : this._curTime;
                },

                setCurTime: function(time, skipping){   
                	//accedo.console.log("setCurTime " + time + "; skipping? " + skipping);
                	//convert milliseconds to seconds
                	time = Math.floor(time/1000);
                	
                    if (skipping) {
                        //accedo.console.info("trying to skip, so setting time to: " + time);
                    }
                    if((this.state === this.SKIPPING || this.state === this.BUFFERING || this.state === this.STOPPED || this.state === this.PAUSED) && !skipping){
                        return;
                    }

                    if(!this._playerObject){
                        return;
                    }

                    if(!this.getTotalTime()){
                        this.setTotalTime();
                    }
					
					var mediaInfo = this._playerObject.mediaPlayInfo(),
                    cbitrate = mediaInfo.bitrateInstant,
                    abitrateValue = mediaInfo.bitrateTarget,
                    sortNumeric, abitrate, i,
                    timeHTML = "",
                    timePercent, timeHour, timeMinute, timeSecond, totalTimeHour, totalTimeMinute, totalTimeSecond;

                    if(!mediaInfo){
                        accedo.console.info("[3] return from setCurTime");
                        return;
                    }

                    //update bitrates
                    if(typeof cbitrate === "number"){
                        this._currentBitrate = parseInt(cbitrate,10);
                    }else{
                        this._currentBitrate = cbitrate || 0;
                    }

                    sortNumeric = function(a,b){
                        return Number(a) - Number(b);
                    };

                    if(typeof abitrateValue === "string"){
                        abitrate = abitrateValue.split("|");
                        for(i = 0; i < abitrate.length-1; i ++){
                            abitrate[i] = parseInt(abitrate[i],10);
                        }
                        this._availableBitrates = abitrate.sort(sortNumeric);
                    }else{
                        this._availableBitrates = [];
                        this._availableBitrates.push(abitrateValue || 0);
                    }

                    this._curTime = parseInt(time, 10);

                    if(this._curTime > this._totalTime){
                        this._curTime = this._totalTime;
                    }

                    if(this.state === this.PLAYING || this.state === this.SPEED || this.state === this.SKIPPING || this.state === this.PAUSED){
                        if(this._playbackSpeed === 1 && this.state === this.SPEED){
                            this.statusChange(this.PLAYING);
                        }
                        if(this.state === this.SPEED && this._curTime === 0){
                            this.statusChange(this.PLAYING);
                            this._playbackSpeed = 1;
                        }
                    	//$super(time/1000);

                    } else { // CONNECTING or FINISHED or ERROR or STOPPED
                        timeHTML = "0:00:00 / 0:00:00";
                        try{
                        //this.onSetCurTime(0,timeHTML,0); // call callback function
                        } catch (e){
                            accedo.console.info(e.message);
                        }
                    }

                    //to form the timeHTML
                    timePercent = (100 * time) / this._totalTime;
                    timeHTML = "";
                    timeHour = 0;
                    timeMinute = 0;
                    timeSecond = 0;
                    totalTimeHour = 0;
                    totalTimeMinute = 0;
                    totalTimeSecond = 0;

                    //Current time
                    if (!time) {
                        timeHTML = '0:00:00 / ';
                    }else{
                        timeHour = Math.floor(time/3600);
                        timeMinute = Math.floor((time%3600)/60);
                        timeSecond = Math.floor(time%60);
                        timeHTML = timeHour + ":";

                        if(timeMinute === 0){
                            timeHTML += "00:";
                        }else if(timeMinute <10){
                            timeHTML += "0" + timeMinute + ":";
                        }else{
                            timeHTML += timeMinute + ":";
                        }

                        if(timeSecond === 0){
                            timeHTML += "00 / ";
                        }else if(timeSecond <10){
                            timeHTML += "0" + timeSecond + " / ";
                        }else{
                            timeHTML += timeSecond + " / ";
                        }
                    }

                    //Total time
                    if (!this._totalTime) {
                        timeHTML += '0:00:00';
                    }else{
                        totalTimeHour = Math.floor(this._totalTime/3600);
                        totalTimeMinute = Math.floor((this._totalTime%3600)/60);
                        totalTimeSecond = Math.floor(this._totalTime%60);
                        timeHTML += totalTimeHour + ":";

                        if(totalTimeMinute === 0){
                            timeHTML += "00:";
                        }else if(totalTimeMinute <10){
                            timeHTML += "0" + totalTimeMinute+":";
                        }else{
                            timeHTML += totalTimeMinute+":";
                        }

                        if(totalTimeSecond === 0){
                            timeHTML += "00";
                        }else if(totalTimeSecond <10){
                            timeHTML += "0" + totalTimeSecond;
                        }else{
                            timeHTML += totalTimeSecond;
                        }
                    }
                    
                    //ERROR HERE FOR SECOND PLAYBACK
                    //accedo.console.log("ERROR IN LINE 1126");
                    //accedo.console.log("time:"+time);
                    //accedo.console.log("timeHTML:"+timeHTML);
                    //accedo.console.log("timePercent:"+timePercent);

                    try{
                        this.onSetCurTime(time,timeHTML,timePercent,skipping); // call callback function
                    }catch (e){
                        accedo.console.info(e.message);
                    }
                },

                statusChange: function(stateToChange){
                    if(this.state === stateToChange){
                        return;
                    }

                    accedo.console.log('statusChange: ' + this.state + ' > ' + stateToChange);
                    this.state = stateToChange;

                    if(stateToChange === this.STOPPED){
                        this._playerContainer.style.display = "none";
                    }else{
                        this._playerContainer.style.display = "block";
                    }

                    switch(stateToChange){
                        case this.STOPPED: //Stopped
                            this.statusChangeCallback("stopped");
                            //system.setScreenSaver(false);
                            if(this._connectionTimer !== null){
                                clearTimeout(this._connectionTimer);
                                this._connectionTimer = null;
                            }
                            break;
                        case this.FINISHED: //Stopped
                            this.statusChangeCallback("finished");
                            this.onRenderingComplete();
                            //system.setScreenSaver(false);
                            break;
                        case this.PLAYING: //Playing
                        	if(this._resumePlay){
                        		accedo.console.log("set resumePlay from " + this._resumePlay);
                                this._resumePlay = false;
                                this._playerObject.seek(this._resumePlaySec*1000);
                                this._resumePlaySec = 0;
                            }
                            this.statusChangeCallback("playing", this._isFF);
                            //system.setScreenSaver(false);
                            break;
                        case this.PAUSED: //Paused
                            this.statusChangeCallback("paused");
                            //system.setScreenSaver(false);
                            break;
                        case this.SKIPPING: //Skip
                            this.statusChangeCallback("skipping", this._isFF);
                            //system.setScreenSaver(false);
                            break;
                        case this.SPEED : //Speed
                            this.statusChangeCallback("speed");
                            //system.setScreenSaver(false);
                            break;
                        case this.BUFFERING: //Buffering
                            this.statusChangeCallback("buffering");
                            //system.setScreenSaver(false);
                            if(this._connectionTimer !== null){
                                clearTimeout(this._connectionTimer);
                                this._connectionTimer = null;
                            }
                            break;
                        case this.CONNECTING: //connecting
                            this.statusChangeCallback("connecting");
                            //system.setScreenSaver(false);
                            if(this._connectionTimer !== null){
                                clearTimeout(this._connectionTimer);
                                this._connectionTimer = null;
                            }
                            this._connectionTimer = accedo.utils.fn.delay(accedo.utils.fn.bind(function(){
                                accedo.console.log("connection timeout");
                                this.onConnectionTimeout();
                            }, this),this._connectionTimeLimit);

                            break;
                        case this.ERROR ://error
                            this.statusChangeCallback("error");
                            //system.setScreenSaver(false);
                            accedo.console.log("this.ERROR "+this._connectionTimer);
                            if(this._connectionTimer !== null){
                                clearTimeout(this._connectionTimer);
                                this._connectionTimer = null;
                            }
                            break;
                    }
                    
                    accedo.console.log('statusChange done');
                },

                getCurrentState: function(){
                    return this.state;
                },

                getCorrectPlayState: function(playState){
                    switch(playState){
                        case 0 :
                            return this.STOPPED;
                        case 1:
                            return this.PLAYING;
                        case 2:
                            return this.PAUSED;
                        case 3:
                            return this.CONNECTING;
                        case 4:
                            return this.BUFFERING;
                        case 5:
                            return this.FINISHED;
                        case 6:
                            return this.ERROR;
                    }
                },

                statusChangeCallback: function(status){
                    
                },
                
                defaultEventHandler: function(evt){
                    accedo.console.log("[default] video eventHandler: " + evt);
                },
				
                onSetCurTime: function(time,formattedTime,percentage){
                    accedo.console.log("onSetCurTime: " + time+ " " + formattedTime+ " " + percentage);
                },
				
                onPlayStateChange: function(){
                    var playState = this._playerObject.playState;
                    //switch back the playstate number into correct state
                    this.statusChange(this.getCorrectPlayState(playState));

                    if(!this.getTotalTime()){
                        this.setTotalTime();
                    }
                },

                onReadyStateChange: function(){
                    this.setTotalTime();
                },

                onBuffering: function(isStart){
                	accedo.console.log("onBuffering");
                    if(isStart){ // buffering started
                        this.eventHandler("onBufferingStart");

                        if(this.state === this.SKIPPING){
                            accedo.console.log("currently skipping");
                        }
                        else{
                            this.statusChange(this.BUFFERING);

                            //schedule next buffering progress check
                            accedo.utils.fn.delay(accedo.utils.fn.bind(this.bufferingProgressChecker, this),0.5/* delay 0.5 seconds*/);
                        }
                    } else { // buffering completed
                        this.eventHandler("onBufferingComplete");

                        if(this.state === this.STOPPED || this.state === this.ERROR){
                            return;
                        }

                        if(this.state === this.SKIPPING){
                            accedo.console.log("currently skipping");
                            return;
                        }

                        this.statusChange(this.PLAYING);

                        accedo.utils.fn.delay(accedo.utils.fn.bind(function(){
                            if(this.state === this.PAUSED || this._withHoldingPause === true){
                                this.pause();
                                this._withHoldingPause = false;
                            }
                        },this),1);
                    }
                },

                bufferingProgressChecker: function(){
                    accedo.console.log("in bufferingProgressChecker; this.state: " + this.state);
                    if(this.state === this.SKIPPING){
                        accedo.console.log("currently skipping");
                    }else if(this.state === this.BUFFERING){
                        if (this.eventHandler){
                        	this.eventHandler("onBufferingProgress");	
                        }
                        else {
                        	accedo.console.log("this.eventHandler not defined?!");
                        }

                        if(this.state === this.STOPPED || this._withHoldingStop === true){
                            accedo.console.log("carry out withholding stop action");
                            this.stop();
                            this._withHoldingPause = false;
                            this._withHoldingStop = false;
                            return;
                        }

                        //schedule next buffering progress check
                        accedo.utils.fn.delay(accedo.utils.fn.bind(this.bufferingProgressChecker, this),0.5/* delay 0.5 seconds*/);
                    }
                },

                onRenderingComplete: function(){
                    accedo.console.log("onRenderingComplete event");
                    
                    this.stop();
                    this.eventHandler("onRenderingComplete");

                    if(this.loop){
                        this.play();
                    }
                },

                onError: function(){
                    /*
                        0 A/V format not supported
                        1 Cannot connect to server or connection lost
                        2 Unidentified error
                        3/1000 File is not found
                        4/1001 Invalid protocol
                        5/1002 DRM failure
                        6/1003 Play list is empty
                        7/1004 Unrecognized play list
                        8/1005 Invalid ASX format
                        9/1006 Error in downloading play list
                        10/1007 Out of memory
                        11/1008 Invalid URL list format
                        12/1009 Not playable in play list
                        1100 Unidentified WM-DRM error
                        1101 Incorrect license in local license store
                        1102 Fail in receiving correct license from server
                        1103 Stored license is expired
                     */
                    var errorCode = this._playerObject.error, explain;

                    switch(errorCode){
                        case 0:
                            explain = "A/V format not supported";
                            break;
                        case 1:
                            explain = "Cannot connect to server or connection lost";
                            break;
                        case 2:
                            explain = "Unidentified error";
                            break;
                        case 3:
                        case 1000:
                            explain = "File is not found";
                            break;
                        case 4:
                        case 1001:
                            explain = "Invalid protocol";
                            break;
                        case 5:
                        case 1002:
                            explain = "DRM failure";
                            break;
                        case 6:
                        case 1003:
                            explain = "Play list is empty";
                            break;
                        case 7:
                        case 1004:
                            explain = "Unrecognized play list";
                            break;
                        case 8:
                        case 1005:
                            explain = "Invalid ASX format";
                            break;
                        case 9:
                        case 1006:
                            explain = "Error in downloading play list";
                            break;
                        case 10:
                        case 1007:
                            explain = "Out of memory";
                            break;
                        case 11:
                        case 1008:
                            explain = "Invalid URL list format";
                            break;
                        case 12:
                        case 1009:
                            explain = "Not playable in play list";
                            break;
                        case 1100:
                            explain = "Unidentified WM-DRM error";
                            break;
                        case 1101:
                            explain = "Incorrect license in local license store";
                            break;
                        case 1102:
                            explain = "Fail in receiving correct license from server";
                            break;
                        case 1103:
                            explain = "Stored license is expired";
                            break;
                    }

                    accedo.console.info("[xdk LG] on error occured! " + explain + "; error code:" + errorCode);
                    this.statusChange(this.ERROR);
                    this.stopAction();
                    //this.eventHandler("error_" + errorCode);	//Juergen there was no case for this
                    this.eventHandler("videoError");
                },

                onConnectionTimeout: function(){
                    accedo.console.info("[xdk LG] on connection timeout occured");
                    this.stopAction();
                    this.statusChange(this.ERROR);
                    this.eventHandler("onConnectionFailed");
                },

                removeConnectionTimeOut: function(){
                    if(this._connectionTimeOut !== null){
                        clearTimeout(this._connectionTimeOut);
                        this._connectionTimeOut = null;
                    }
                },

                isBusy: function() {
                    switch (this.state) {
                        case this.BUFFERING :
                        case this.CONNECTING :
                            return true;
                        default :
                            return false;
                    }
                }
            }
        })();

        deviceIdentification = (function(){

            //parse user agent to get all info
			var agentStr = navigator.userAgent,
            agetnArr = agentStr.split(";"),
            netCastStr = agetnArr ? agetnArr[agetnArr.length-1] : "",
            netcastPos = netCastStr.indexOf("NetCast."),
            deviceStr = (netcastPos > 0) ? netCastStr.substring(netcastPos+8) : "Browser or Emulator",
            remainingStr = (deviceStr.split("-").length >= 2) ?  deviceStr.split("-")[1] : "2015";
        
			deviceType = deviceStr.split("-")?  deviceStr.split("-")[0] : deviceStr;
            firmwareYear = remainingStr.split(" ")? remainingStr.split(" ")[0] : remainingStr;
            //accedo.console.log("deviceType: " + deviceType + "; firmwareYear: " + firmwareYear);
                        
            var deviceObject = document.getElementById("deviceInfo"), //get device object
            xTmDevice = null,
            firmware = null,
            modelName = null,
            mac = null,
            ip = null,
            serial = null;

            return {

                getDeviceType: function(fullname) {
                    if (deviceType !== null){
                        return deviceType;
                    }
					
                    fullname = true;

                    if (navigator.userAgent.indexOf("NetCast.Media") !== -1){
                        deviceType = "bd";
                        return (fullname) ? "Blu-ray" : "bd";
                    } else {
                        deviceType = "tv";
                        return (fullname) ? "TV" : "tv";
                    }
                },
                
                getModelName: function() {
                    if (modelName !== null){
                        return modelName;
                    }

                    modelName = (deviceObject.modelName !== undefined) ? deviceObject.modelName : 'AACCEEDDOOMODELNAME';

                    return modelName;
                },

                getFirmware: function(){
                    if (firmware !== null){
                        return firmware;
                    }

                    firmware = (deviceObject.swVersion !== undefined) ? deviceObject.swVersion : 'AACCEEDDOOFIRMWARE';

                    return firmware;
                },
				
                getFirmwareYear: function(){
                    if (firmwareYear !== null){
                        return firmwareYear;
                    }

                    firmwareYear = "2011";

                    return firmwareYear;
                },

                getMac: function(){
                    if (mac !== null){
                        return mac;
                    }

                    //mac = deviceObject.net_macAddress;
                    //mac = mac.replace(/:/g, "").toUpperCase();
						
                    //return mac;
                    return "AACCEEDD0012";
                },

                getUniqueID: function(){
                    if(serial !== null){
                        return serial;
                    }
 
                    serial = (deviceObject.serialNumber !== undefined) ? deviceObject.serialNumber : 'AACCEEDDOOSERIAL';
					
                    return serial;
                },

                checkConnection: function(failureCallback){
                    var isBrowser = false;
                    if (navigator.userAgent.indexOf('Firefox') != -1 || navigator.userAgent.indexOf('Chrome') != -1){
                    	isBrowser = true;
                    }
                    if(deviceObject.net_isConnected){
                        return true;
                    }else if(isBrowser){
                    	return true;
                    }
                    else{
                        failureCallback && failureCallback();
                        return false;
                    }
                },

                getWidevineESN: function(){
                    if(widevineESN != null){
                        return widevineESN;
                    }else{
                        return false;
                    }
                },

                /**
                 * get and return widevine SDI_ID
                 * @function getWidevineSDI_ID
                 * @public
                 * @ignore
                 */
                getWidevineSDI_ID: function(){
                    if(widevineSDI_ID !== null){
                        return widevineSDI_ID;
                    }

                    var deviceObject = document.getElementById("deviceInfo");
                	widevineSDI_ID = (deviceObject.secureSerialNumber !== undefined) ? deviceObject.secureSerialNumber : 'AACCEEDDOOSECURESERIAL';
					
                    return widevineSDI_ID;
                }
            }
        })();
        
        appengine = (function(){

            return {

                getResolution: function() {
                    return {
                        width: (window.innerWidth !== null? window.innerWidth: document.body !== null? document.body.clientWidth:null),
                        height: (window.innerHeight !== null? window.innerHeight: document.body !== null? document.body.clientHeight:null)
                    };
                }
            }
        })();

        /**
         * Create and return device system handling (e.g. power off, exit)
         * @Class system
         * @private
         * @ignore
         */
        system = (function(){

            return {

                /**
			     * to capture the event when mouseon
			     * @name _onmouseon
			     * @function
			     * @private
			     * @memberof accedo.device.lg.System#
			     */
			    // LG only event
			    _onmouseon: function () {
			        //accedo.console.log("env-onmouseon");
			    },
			    /**
			     * to capture the event when mouseoff
			     * @name _onmouseoff
			     * @function
			     * @private
			     * @memberof accedo.device.lg.System#
			     */
			    _onmouseoff: function () {
			        //accedo.console.log("env-onmouseoff");
			        accedo.focus.managerMouse.releaseFocus();
			    },
			    
			    getMouseOnOff: function(){
			        if(window.NetCastGetMouseOnOff){
			            accedo.console.info("LG system: current mouse status is:" + window.NetCastGetMouseOnOff());
			            return (window.NetCastGetMouseOnOff().toUpperCase() === "ON");
			        }
			        accedo.console.info("LG system: detecting mouse on/off on a non-NetCast platform, returning false.");
			        return false;
			    },
			    hasMouse: function(){
			        return true;
			    },
                
                exit: function(){
                    accedo.console.log("Exit app");
                    window.NetCastReturn(461);
                },

                storage: function() {
                    return {
                        data: {},

                        uniquePrefix: "AccedoXDK",
                        key: "AccedoXDK",

                        initialized: false,

                        /**
                         * get intialize the storage object
                         * @function initialize
                         * @public
                         * @param filename {String} filename to be saved inside the LG device
                         * @param uniquePrefix {String} cookie prefix to be used in the file
                         */
                        initialize: function(uniquePrefix, key) {
                            if(key){
                                this.key = key;
                            }

                            if(uniquePrefix){
                                this.uniquePrefix = uniquePrefix;
                            }
                            
                            this.initialized = true;
                        },

                        /**
                         * Set a hash object inside the storage (save to storage is not yet done)
                         * @function set
                         * @public
                         * @param k {String} key of the hash pair
                         * @param v {Object} value of the hash pair
                         */
                        set: function(k, v) {
                            if(!this.initialized){
                                return;
                            }
                            this.data[k] = v;
                        },

                        /**
                         * get a hash object inside the storage (save to storage is not yet done)
                         * @function get
                         * @public
                         * @param k {String} key of the hash pair
                         */
                        get: function(k) {
                            if(!this.initialized){
                                return;
                            }
                            return this.data[k];
                        },

                        /**
                         * remove a hash object inside the storage (save to storage is not yet done)
                         * @function unset
                         * @public
                         * @param k {String} key of the hash pair
                         */
                        unset: function(k) {
                            if(!this.initialized){
                                return;
                            }
                            delete this.data[k];
                        },

                        /**
                         * save the hash object to storage
                         * @function save
                         * @public
                         */
                        save: function() {
                            if(!this.initialized){
                                return;
                            }

                            var date = new Date();
                            date.setTime(date.getTime() + (365*24*60*60*1000));

                            var expires = "; expires=" + date.toGMTString();
                            var ck = this.uniquePrefix + "=" + escape(JSON.stringify(this.data)) + expires + "; path=/";

                            accedo.console.log(ck);
					        //save to cookie
					        document.cookie = ck;
                        },

                        /**
                         * load the hash object from the file
                         * @function load
                         * @public
                         */
                        load: function() {
                            if(!this.initialized){
                                return false;
                            }
                            
                            var cookieArray = document.cookie.split(";"), cookieString, cookieObject, i;
        
					        if (cookieArray.length === 0) {
					            this.data = {};
					            return false;
					        }
					        
					        var strip = function(text) {
                                return text.replace(/^\s+/, '').replace(/\s+$/, '');
                            };
                            var startsWith = function(text, pattern) {
                                return text.indexOf(pattern) === 0;
                            };
					        
					        for (i = 0; i < cookieArray.length; i++) {
					            
					            var item = cookieArray[i];
                                item = strip(item);

                                if(startsWith(item, this.uniquePrefix + "=")) {
                                    item = item.replace(this.uniquePrefix + "=", "");
                                    this.data = JSON.parse(unescape(item));
                                }
					        }
					        return true;
                        },

                        /**
                         * remove all the hash object
                         * @function clearAll
                         * @public
                         */
                        clearAll: function(){
                            if(!this.initialized){
                                return;
                            }
                            this.data = {};
                            this.save();
                        }
                    }
                }
            }
        })();

        /**
         *@ignore
         */
        deviceConsole = (function(){

            return {

                log: function(){
                    var args = Array.prototype.slice.call(arguments);
                    console.log.apply(console, args);
                },

                info: function(){
                    var args = Array.prototype.slice.call(arguments);
                    console.info.apply(console, args);
                },

                error: function(){
                    var args = Array.prototype.slice.call(arguments);
                    console.error.apply(console, args);
                },

                debug: function(){
                    var args = Array.prototype.slice.call(arguments);
                    console.debug.apply(console, args);
                },
                
                warn: function(){
                    var args = Array.prototype.slice.call(arguments);
                    console.warn.apply(console, args);
                }
            }
        })();
        
        /**
         * Function to be called when a device is registered
         * @function init
         * @private
         * @ignore
         */
        init = function() {
            accedo.console.log("------------------- [INIT] >> LG Abstract Layer Initialize--------------");

            //emulator check
            isEmulator = !!(window.location.search.indexOf("product=") == -1);
            
            //set dummy value for widevineESN
            //widevineESN = "<CLIENTINFO><CLIENTID>AgAAAO0hfaUDYP4sq1gdaL9xIzV2UWNMtMsT70*a4XLUk2VHZ20MHaFc5hd!Whr5xEstedqjYIKYgJBpMXyzdWJ9PV!Xl!TFKYweL0q26uYEpuU9ctDRsQlTgsJUREpTsfc61kPbMO1OWP1XuUuc30BGoMetbWrHu6Tw5T!1WZNqqZqtIzIHgH2IPGTwXGZGFOWCHuVbEVFtYzarAI0OT*Xppjxx1YIN</CLIENTID><CLIENTVERSION>9.00.00.2778</CLIENTVERSION><SECURITYVERSION>2.4.117.25</SECURITYVERSION><APPSECURITY>2000</APPSECURITY><SUBJECTID1>4219</SUBJECTID1><SUBJECTID2></SUBJECTID2><DRMKVERSION></DRMKVERSION><DEVCERT>bWFwmm4FPHL5aZuEQ6EtDaUX+jlZbbnrqgeKx00I5KLbMRU96uw9G4ROKtn3f880IJ8A+AxXeCN2sAUBZNVcc+fXGHTi8jfIn5KB/LepYTOyaW6wNfNR/G0N1it+VlwRzO2VPciGpXDtSBfghHdZyxElaaWJu1ewOp6uCtE5vkFMhV5f811HzQciTXLLc//Of4E4bzNrxReO0480Bb4ZEyQoMAVkgubGqCIdibtyzNu1MhsO0bq7xuszg7IXLxkJHN1FajiDq4i+NU8HZIZ7RFrncEVgsMCTDGz1zFaQGXV5lKBx5meRjYoVqN4Fg3x/40wNWc7cXcldChGxzWHy9NdQFwbYoDqN/9I6CdhGPUSpXtTgRDB8RS0C4E9Nmi1c89DPW2lA6tZx+TYT4K1IWsfVYKTrmNdxe9zuEGf9B3JeC6RaQnUDlL/WsAYLCgySHWBNqlVJRND3zKUcTQMpS01YT4O9ncdDB1UEzqDYCbmdkqkzy0ew6L7a2ka0oatpC2G9xopJEXFvbWVRTgXL0T5CIkM4z+qYujUkkjzxVa40q4ELV6x0gDjH3ZtQwhCO4FV5nwkuryDAGIFHB0FFAwlXE3/gUS+YKjTXgU2WlfNfqqun6+2znjBdz2JioL67Wy99LUOPX+G59lfZQMZ/Rj3NTnvSbGm1Z4GoIgX5EN9aq5vK1Krn/p+xoIxsXi1TctDxbQhzOAJbIhcf844Sg1UtQDM1RjuXt1ikaoqmhkzekUjBXxhrkWqCvMUCohDVU6yHjhzAc5nFwt71dCQHX7/dLWbHqles6O9AfWDUNTK7g938aM6g9IOp4/ZbD8ByAzFBWWHqvoMEqwkmG+Hb2U8J1kdwiKnCP7CUQgbd7cBgGJyrE5TEPCHn4Zu+S9eW5tX345C2EQzK2Mb2NEXPFhLVVvUk1V7drpzKIXJcm1kcMibDyRyJnUOwm8m+M6mpKubkyJTA9VCxmQN2LXofF9jdL6yw8VkSu+nCOgO1S1x2/v9hdnP6OhaEO40n0nMMwxR+yDdPgMLeV2uMI0ABUW7e0pC1+Rf1zbSEOn5MY6CWzcsUO7KYfhIZ4VNF6j8K/V88tdTY/7P2orMn6E+VC346w3KdL+ZF7E5IdWL6FRDvhPTVSoRI6KNq+Ds3S72uGbtayQaCSAEUtk1sKHYNeAUlFv0K0FnhOdRvhI/WcGyQFsCGeI9Fl0TmO+/HncLW6ssSVpj//bs75V1SNwvyFTXbOi7jIbOaMgNCiRyY58FfOBd8fI1EyBjJddRrrIpl42N/eHuX5Hf2oyfF7TL/qScZPyo/Hoq86OAojZoKkClx3FDl/LIXnOwDdZv5ltzOXPgzGtTLUSMHksOrfw4X/9g26ALaT312PnyarU8S1jWKXAzstFsdN7wmXX6MosyTU0oz2hYwO0Vs9gvdbaCWmD6i+3Stle5XBgViuE5HLGVPxEHEoieqv2QS9qsR+TRr3VKHVKe2yUJuKLPxqzw2m1YeWrtOmiyhA0kAPOJj+722o01xTERBUWd3PS8ROQyJkyQ7rajD/pxn54ip0MFk8iJ94948V2akN0WRccMjHovug2PE0D9csseqRBJNfFNKlcYiUnuoUTz5owYkzo/ctq9+uy8aF2cygVijQL26Xhg6hW/T2/IUmOG0IW6HNAXn4BPwY7lndhTLB2nbZwxHvdH79PU7qafssHgQewYRAzc72ItlzA+JBpC1D8L+Njnd+5uQXCENhNzuU1LFVlnyYJORxYJnCmhmsI1KS192/AqUxI8m47jBs36pwYbMuKZIT8+R02DZnYfvsDZf9+gnGlqBoiQPgL9rVZPtAdGq3kGXuVJdAWvjb5gnzQo6cVXHwgfK5lvxUIEbKz1Pfg4crcNSTtIVeylAYyge61AHRnslQOt5h0IQlt+cXE/ECJOYEVCM4Qql0ZlM3gIAJNcM6nNthXPjlYZ8yh9FsLATvpzsW/GHD5XIy9f8Q2y6faALABVBgeqnFCcWr7RjeJVAbPgGYbnv5IBggwrfP/UXGALNwhuwoEA2UThsORGm8dFabxTMpuUwnoPS1+VMpbjxeqDUVcuYN2/bj9y9w8j4BYMMT/GGZSHBs73YngwcBGqr9DSsmXoKI3UQyGOqzMw8YOFuDrTplFXtpntE1DZIWRGCBOungkrGix5kkdfnL5+P1HLzCGen8DdMqJJV7k6z0Vm9FhhzprQOubQpEloXoGar8xKwKi5w/zZZgokNM+BJoorhYmonGNbkSSUnwQZv58r05nvxzK+s8e/EJeszvZLlXIId3hZEmMWO5exp/vMHFGyR+g75ioH9PvLGwqqG4e6Q0SXZLbimgLGkGszr5TC2uUO+eJKpR/6rTBq1s9iUexv59z4NSp7sxobgzwm4Oe1ncz2LJoDne0KvVoGDnqWCJWVfky49E6bL7hTVBWtdTe1TvPLJq5pWK4LWTZRAQyypXBZ47pGaXp+GLrWk2AHpQPUleXUgz9EM4xAe6t2mbjS8bBUdvVTNsazZqHnyNqxZIL61f4hgkLYmN+itONX9yNbcpmkDxDuUTLFqpO+uueMS+XURZ6hh7252wd7Kb8H7xTO+I4ScUecf9sIkWe7c3zDlEyvHsR8nnxEiKMCf9JD2PcTUMXEbvJj5qKswCNRCV36GQJbGq7n0nFmm2K4cjeOtR2oUt8vUPWbH3VW2EMk0wqC39w/xZoYS/yPP4xk6/1Jas7LuhOz1crpsc+rHzuvCl5u2o9LA8KPXSlVh90FJTiRSoEwADI1SuH69ziois31HwhKjHMXgik2449bCrOy96cZSGxE2AAqaSRELxYGJXfSiDJWbi08VAlgF74TnRFGmNDRY2f45zMu1aj+pQL0RWMD6mceSinE4LrafvzRSKxcgtZ+bCvmiAxDSr8wJzTB8wNcxG0pMbxCo11uK7O+4Rm7mchQNyNC4Rb1ENi/Um3H4M9pfNm4LD8i//ZPiCCfpEs1anKZXH2SZq+Xl/HdwGfOUXTGhGwrb5y5G7HC79BabOP2G+YhU/KIM4fGWrWppUq+/rscFS3AshY5DC+M4/ISVZt7YIf954UUbn9c+D45dms4Xr64XACuYUMIksF3JH31qgIH/qvgOSQ69OkhjsFrkILqjMtw2QJNiVCIHO+SNddTrmEBX5enmmdAuc82FwJsbGVBwEWrAZAqoL+CzxDiXc5glBHnYLmg9ZMwH+/pP7RjLwN7CJ1+OmThG4fqVhgkSwHBFN7jc7Jm3hwDCLMCMksmwxv6JP6ejG7BE/WqOlVvmd16GTQ+0NT9z5NEIzvZYUj5rQKgejTgq76uT4X99WEXH77Xn5VvsP99GzLoK0BndnxYLQg2kD8n6ttCD56o2O0eCJ4ZlNNL1swQ7RuDIH3zp0RLeqKr7G/Nm73VSd6l3weMsvAtK6SbqUSbZdNRMisS4R2zvWAjBXxmaitY4uHEZeRaSbLKWwqKS2fkD+QlZyNrGdoexptGZXQwF9Sf39mASkGZzJRpNvs+JZNx+pmb0fIEpgc2+QZXYwhCUirIVsqfnMiCIEClhdlqkh6pPaluBoXwyGVNxHvqNynKa2D1EZ0NrUpt2VCn3dRG6nF5/OaXWEkWrWHNRA60Qkc69wILOxoV83lh6Naqd++XRoe3usTDbFDMvMblIZJCdcabWnf9pRCgOQQIHjvN6iHuQjehO0T1Nm6g7EjeIyi/Qni3REDnlyOplIzi3leUEbwednM2UWbqIYf2U7E+efnh2y6RSVH2S7DpTmm+xVENx+dTf6xkbrdIBiJqEiD8EMK1XtOBZB4gism6OEKUmm4C7UkQmF8ptEr5l+flCFe1kZ+CXNMu+0nm7UIVqNQLoK7oc5Yn7N6Bd+r90NC37AvHsDPrVKZzwgIT1tC4dzbra0+4qfuM/nA/YSlJc7lCDFgPc+3Ce+Hja4CToCmO5WQ2ljaFS5qO4ktbO+mzke/zxuIwZ2oOMz3R1kzIzfA/gyfMiPJjhwHu8XklopSDv8GxLuW8PmEDFerYZBXK8feqo3fhT7Ub01GVE9hT1LRKl8w9rd9RM5Sg/8U1INCmEQhQS5xYFXVMeKwprwx+9y3vgvksq6z/k03EqrGBTAjCI1ExPcn5mDX4BMiLw/doFz31YnvthqnDyBkCXWwojryA7s3dxzV8aoHgJvS/Dk5ALu5Ki0HfwD1v4IUb5Y0FL5bKwstbJ+LKXDWeoKxMSIfMnel7YsuRBWyRm4v9foU+iiQHRfSth5rUd94/gMbo77NUIVVa4Ypl/wnd/48sYIsi5ECCBpMermiHxcKqsg33y+u7Sp1IDLmKbX4aNWQM5cZo7+WCLsYEYNMJxro02mrZDtyw4LjALiWeqlnFQEZIcuv0G5/rx6SeWBkQxy8Fz+ldrjsTwg56gH0huNuZoyFM/uTIYg2msEbGFwqHF2itSSVsb1yxU3yI78YwmRl8lqlY1rS4XUQM/2PsknVW2w4ncoay1NFM4e5tW53pNtOHLeMRxen46ybuA69dOBDhviT77+QEH/qZcded+jsdRY36X8sgYOAqF2ZGfzeFjDVIRIR3VaBKArERiuEXRDayB8h4psLbluG8nXXQFSMqbqTwItEk5PVZPTVC9YbO+/BSAGSU2RrBiPpG9ciImd3vaxOy4GHoLjvDwssDlsiqBodH+1hqlL2BYTxj3ZY8Hn7OYUpS42j92Htbd3Io5nVjbkF+BHBjaC292jX5uVcVl7ca5g5hI6HjPLRwS+Nyb/IXwZRBscHZi4Jofya08fra/pe44iShHFqMnICoBS3cJxvJek63db2KEdyzparLV90SnHjT4fJ8kQU+Oq27faD+NEbm9O6oagImSByjuHWwm5K1RfwDyq7lpRYuTVDpuKkKP2YPvBGs0hkbbEuMh2GuWlpuZHlPXLlHwGEHfnMcFh3cQoea1oQskV+HovhepJ5RYFGl4toKN7hlvirJAPB2i7TvXDdYh86hpiBxc3Xk+RSJLx7Ghlt7kQObiXkXdYWDo97RmzgWYepjhOF8EgX9TR1/hcY0u878bEplZUWPypu9e0Y0YaThgORxE1HqLu9oESba/p1ErkbBUfscnN4mQdMqGosQVyT32pRR3US583DKsZsQ6nvajVd8DnJKi1NJysX6uOnstxUTpbxyrQU+FSFYhrEQO3Vwzl+TVLxSTU/rrsfy9a09qCqBSkfr4mnAfCuv96kc3oQ7O4dYgvi3B9r/yBOcA9UyUEeJpW+OUF+a6HamJmN0RdX51msRkgOY0wPIrs57qcV7PbRLTdtB3kCixnoggaZqlA3cAKeaTh5RiH1hoxCJiRvl4jLNiam2m5lERmOIT1LO6hacehDHzAV07mTYbZDvhHx6pxa52tDiI8YaF4s4u52w3sXOad6NUlN4aaaVo4sBBsF4Ts4c0ynwXvLNhMwTNxRZ+hA2QkTj7DxZhAljcXrjVYxU50UdYEpN7sJAi892Iizsw6mnsKYxnDBmJvMxQADNRaBRcFMXNvLDBX/EMfG5cm1SQ5clieNcMkleDrOlJo3B+BdeKfFWkl4vNS+mqLbDGMAxtgLB7OC1yvozIXu0HSEUuQlsAzChCMLZBy55msED2dJw75N7mQpe2Pc/L+mJxsqpjav+PVY8rftp0Am5hlNwiFLJe8ehwoiSDxlzzIIt9vm0McNn8AIhPS9Hdfzyqd2AP3aIHF34raZOR9tWlbh2cPw5qc+F3hbpxZhx1LNscGRiP2xqXTHtsHCfiyrrBAZELG+Lkvqu/yXYZi2c6Gm+rkVYBEj/azw+BkqB51izhkVzLY3ye+fGRzLntWfze9gxtz2sN5NZbba9XiFLs7NDNQ+ekSSkzPtxWnCiHxGJKUvEJNcepBMJyFkHNNDygOTryB3kDEfkqKB6C3e0OW4BcHt+RDLhp9pzqbkUnNICcPkSzBlfS+/s+TncFhcopX9JWc3TPPYuzC9Yjocdpb1bs6QFCEfYHDbRLEXYE2KzXqgLb6+mGcfTprze6s7syHcoou1opErZN8IIRrc3BlFyVzNSBCj9Kz0ui85kJDpNW5IbrrmBEEtUSj1YKV8jfjzQN++/8RCUPha7yrBM1m8JJKY1zjwYC2/JwFru0El/XxztmxaPFMVufebsbA3/ZmtuHojRB+xi0JQd0O2ODp/eT3qQlHXM+sFOP93nL+UwcReVtz946A8BNmPiH8HN3yHDRycIQWxE93J1kRlp9EQDDSFGb000ZdflSlKt4EUdn4KHOsS0Zn7uj4OU2zeFfclGmYmHxujd9kSmDK8E5KWC4Wg84Bw72UXSv/M4IJTa+wVT78q6Hhh9nYa8phf5iziJ9KfiTFYM+1U2IKSOOyaP8KatTyoZCJ2syosvdYnhcks6fALu+JtdzrGs+Qmv80HYwaS5xrns7KEBbQBGRCXrO9HiSPdakHnxcwXuK5EnsK5041Kfe9Q4ZxpFQn2z237YrCHqLjGadDQZEhRpeDASnw11oUdbDAdfFt1mo8VY99cXK1cdff5b+TEAN2MjDBcZdqVziUjiJlFuAAIQbIAApS3eY+tN6/b6NtJpnvm2jjTZHSAf+ZKKW3YVyRBZE8PLLi+am0De1dEQUz5vAmoPg0Jhmcydju1rR4Chy0Hz0CAmzh+OB0JRm7rYyF+aDEKbA+8f7rGQfK/HBoQYv+NnF1V/N9u9zajbZp9XfstTBden/+dem32kG+T35/RfaNQhtMAoVjNFfG6LfU7Sv7zL38NxfFZYRZkQdjRd+3llUVlibSilZ9y7g41j/TT7VFiBplUzWp/rh0pFVcvghTadJiLEAGe5LlzYED9kaXreN5upUoMv137bMvTVnbRRJQz12iXW8liz6by6HOA2Rr0p+YkqxVK3/vK7r7sb//PJG4GBcsiaJVgzkW6j/qaK5XKqKZhVE9ps///Li66p5IUe5j4JXrNic0NV6oiZ3MwOfiKMN1YEc5ufFVhKq5zca0OUjYdxv+/H8Z5UX9xYze+hzEwUD+pNdYN09kB9nw0i2hDWzP3ipOVwVPO1SkgpIvPrqBXzu76B83pRub9olN/bNPAH3eyfppTfSS53GGA+wufowS+LyTXUwU0Buy9CTlHP5hXH4Iw7BmlxHgTjEdNU/ALUI41AI7ODC1wr8Gnz+L3bT5VLM+6FvKc3LU+gHDTux8ks=</DEVCERT></CLIENTINFO>";
			widevineESN = deviceIdentification.getUniqueID();
			
            //initiate the keyHandler
            initiateKeyHandler();
            
            //initiate the mouseHandler
            initiateMouseHandler();
        };

        // initiate the class
        init();

        return {
            deviceId: DEVICEID,
            keySet: keys,
            mediaPlayer: mediaPlayer,
            identification:  deviceIdentification,
            appengine: appengine,
            system: system,
            deviceConsole: deviceConsole
        }
    })());

    //Value set to notify the lazy loader that this file has been loaded
    return {};
});
