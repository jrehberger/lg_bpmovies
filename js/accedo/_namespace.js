/**
 * @fileOverview define the accedo namespace
 * 
 * 
 * @author Calle Gustafsson <calle.gustafsson@accedobroadband.com>
 */
/**
 * Base namespace for the Accedo library.
 * @const
 * @name accedo
 * @namespace This is the base namespace for the XDK library
 */