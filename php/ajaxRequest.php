<?php
header('Content-Type: application/xml');
$method = $_SERVER["REQUEST_METHOD"];
$url = $_GET["link"];

$url = str_replace('@?@', '?', $url);
$url = str_replace('@|@', '&', $url);

$headers = getallheaders();
$requestHeaders = array();

foreach ($headers as $key => $value) {
    //echo "$key: ". $value."\n";
    if ($key == "Host") {
        $requestHeaders[] = "$key: " . substr($url, strpos($url, "//") + 2, strpos($url, ".com/") - strpos($url, "//") + 2);
        //echo substr($url, strpos($url, "//") + 2, strpos($url, ".com/") - strpos($url, "//") + 2);
    }
    else if ($key == "Accept-Encoding") {
        //echo $value;
        $requestHeaders[] = "$key: " . "utf-8";
    }
    else
        $requestHeaders[] = "$key: " . $value;
}

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, FALSE);

if (count($requestHeaders) > 0) {
    curl_setopt($ch, CURLOPT_HTTPHEADER, $requestHeaders);
}

if ($method == "POST") {
    $post_body = file_get_contents("php://input");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_body);
}

if ($method == "PUT") {
    $put_body = file_get_contents("php://input");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $put_body);
}

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

$response = curl_exec($ch);
$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 

//Add error-handling for this on the client side
if ($httpCode == 400) {
	header('HTTP/1.1 400: Bad Request');
}
if ($httpCode == 401) {
	header('HTTP/1.1 401: Unauthorized');
}
if ($httpCode == 403) {
	header('HTTP/1.1 403: Forbidden');
}
if ($httpCode == 404) {
	header('HTTP/1.1 404: Not Found');
}
if ($httpCode == 405) {
	header('HTTP/1.1 405: Method Not Allowed');
}
if ($httpCode == 408) {
	header('HTTP/1.1 408: Request Timeout');
}
if ($httpCode == 500) {
	header("HTTP/1.1 500: Internal Server Error");
}

// close cURL resource, and free up system resources
curl_close($ch);

echo $response;
?>
